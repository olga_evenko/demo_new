from django.db import models
from projects.models import *
from workers.models import *
from django.core.cache import cache
from django.conf import settings
from django.contrib.gis.db import models
from django.db.models.signals import post_save, pre_save
from channels import Channel
import datetime
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from simple_history.models import HistoricalRecords
from django.utils import timezone
# Телефоны и почта###################


def create(sender, instance, created, **kwargs):
    if instance and created:
        if sender == Client and created:
            cache.incr('count_clients'+settings.MEMCACHED_NAME, 1)
        elif sender == Contact and created:
            cache.incr('count_contacts'+settings.MEMCACHED_NAME, 1)
        elif sender == ApplicationExpo or sender == ApplicationAd or sender == ApplicationLeaseKVS and created:
            cache.incr('count_applications'+settings.MEMCACHED_NAME, 1)
        Channel('update_counters').send({})


def check_main_org(sender, instance, **kwargs):
    if instance.main and sender.objects.filter(individual_id=instance.individual_id, main=True).exists():
        sender.objects.filter(individual_id=instance.individual_id).update(main=False)


class PhoneItem(models.Model):
    phone = models.CharField(max_length=25, verbose_name='Номер телефона')
    main = models.BooleanField(default=False)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    phone_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = (("phone", "content_type"),)


class EmailItem(models.Model):
    email = models.CharField(max_length=45, verbose_name='E-mail')
    main = models.BooleanField(default=False)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    email_object = GenericForeignKey('content_type', 'object_id')


####################################

class Individual(models.Model):
    SEX_CHOICE = (
        ('m', 'Мужской'),
        ('w', 'Женский')
    )
    surname = models.CharField(verbose_name='Фамилия', max_length=64)
    name = models.CharField(verbose_name='Имя', max_length=64)
    middle_name = models.CharField(verbose_name='Отчество', max_length=64, blank=True)
    sex = models.CharField(choices=SEX_CHOICE, verbose_name='Пол', max_length=4)
    date_of_birth = models.DateField(verbose_name='Дата рождения', blank=True, null=True)
    # address = models.CharField(max_length=64, verbose_name='')
    email = models.CharField(verbose_name='E-mail', max_length=64, blank=True)
    # site = models.CharField(verbose_name='Сайт', max_length=64)
    phone = models.CharField(verbose_name='Телефон', max_length=32, blank=True)
    phones = GenericRelation(PhoneItem)
    emails = GenericRelation(EmailItem)
    address = models.CharField(verbose_name='Адрес', max_length=128)
    date_created = models.DateTimeField(verbose_name='Дата создания')
    date_edited = models.DateTimeField(verbose_name='Дата изменения', auto_now=True, blank=True, null=True)
    add_info = models.CharField(verbose_name='Доп. информация', max_length=256, blank=True)
    created_user = models.ForeignKey(User, null=True, blank=True)
    # entity = models.OneToOneField('Entity', verbose_name='Главный объект', related_name='individual',
                                  # blank=True, null=True)
    entityprivate = models.ManyToManyField('Entity', verbose_name='Юр. лица', blank=True, through='IndEntPrShip',
                                           through_fields=('individual', 'entity_id'), related_name='individual')
    visitor_manager = models.ForeignKey('workers.Manager', related_name='individuals_visitors', null=True, blank=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Физическое лицо'
        verbose_name_plural = 'Физические лица'

    def __str__(self):
        return self.name + ' ' + self.surname


class IndEntPrShip(models.Model):
    individual = models.ForeignKey('Individual')
    entity_id = models.ForeignKey('Entity')
    main = models.BooleanField(default=False)
    type_position = models.CharField(max_length=64, verbose_name='Тип должности')
    position = models.CharField(max_length=64, verbose_name='Должность')

pre_save.connect(check_main_org, sender=IndEntPrShip)


# class EntityPrivate(models.Model):
#     full_name = models.CharField(verbose_name='Полное наименование', max_length=64)
#     address = models.ForeignKey('Address', verbose_name='Адрес')
#     email = models.CharField(verbose_name='E-mail', max_length=64)
#     site = models.CharField(verbose_name='Сайт', max_length=64)
#     phone = models.CharField(verbose_name='Телефон', max_length=32)
#     auth_person = models.CharField(verbose_name='Уполномоченное лицо', max_length=64)
#     entity = models.OneToOneField('Entity', verbose_name='Главный объект', related_name='entity_private',
#                                   blank=True, null=True)
#
#     class Meta:
#         verbose_name = 'Юридическое лицо'
#         verbose_name_plural = 'Юридические лица'
#
#     def __str__(self):
#         return self.full_name


class Entity(models.Model):
    client = models.ForeignKey('Client', related_name='entitys', blank=True, null=True)
    activities = models.ManyToManyField('projects.Activities', related_name='entitys', blank=True)
    full_name = models.CharField(verbose_name='Полное наименование', max_length=256, db_index=True)
    address = models.CharField(verbose_name='Адрес', max_length=128)
    email = models.CharField(verbose_name='E-mail', max_length=64, blank=True)
    other = models.BooleanField(verbose_name='Иное', default=False)
    site = models.CharField(verbose_name='Сайт', max_length=64, blank=True)
    phone = models.CharField(verbose_name='Телефон', max_length=32, blank=True)
    phones = GenericRelation(PhoneItem)
    emails = GenericRelation(EmailItem)
    auth_person = models.CharField(verbose_name='Уполномоченное лицо', max_length=350, blank=True)
    sign = models.CharField(verbose_name='Расшифровка подписи', max_length=64, blank=True)
    based = models.CharField(verbose_name='На основании', max_length=350, blank=True)
    date_created = models.DateTimeField(verbose_name='Дата создания')
    date_edited = models.DateTimeField(verbose_name='Дата изменения', auto_now=True, blank=True, null=True)
    one_c_guid = models.CharField(verbose_name='guid 1c', max_length=64, null=True, unique=True)
    add_info = models.CharField(verbose_name='Доп. информация', max_length=256, blank=True)
    post_address = models.CharField(verbose_name='Почтовый адрес', max_length=256, blank=True)
    post_address_info = models.CharField(verbose_name='Примечание к почтовому адресу', max_length=128, blank=True)
    created_user = models.ForeignKey(User, null=True, blank=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Юридическое лицо'
        verbose_name_plural = 'Юридические лица'

    def __str__(self):
        return self.full_name






class BankAccount(models.Model):
    INN = models.CharField(verbose_name='ИНН', max_length=12)


class Address(models.Model):
    ADDR_CHOICES = (
        ('fact', 'Фактический'),
        ('legal', 'Юридический'),
        ('mail', 'Почтовый')
    )
    type_addr = models.CharField(choices=ADDR_CHOICES, max_length=8, verbose_name='Тип адреса')


class ClientActivity(models.Model):
    activity = models.CharField(max_length=32, verbose_name='Название')


class Client(models.Model):
    GROUP_CHOICES = (
        ('vip', 'VIP'),
        ('a', 'A'),
        ('b', 'B'),
        ('c', 'C'),
        ('n', 'N')
    )
    STATUS_CHOICES = (
        ('key', 'Ключевой клиент'),
        ('black_list', 'Черный список'),
        ('inactive', 'Неактивный'),
        ('active', 'Активный')
    )
    annotation_activity = models.TextField(verbose_name='Аннотация деятельности', blank=True)
    client_activitys = models.ManyToManyField(ClientActivity)
    created_user = models.ForeignKey(User, related_name='clients_created', blank=True)
    edited_user = models.ForeignKey(User, blank=True, null=True, related_name='clients_edited')
    name = models.CharField(verbose_name='Наименование', max_length=256, db_index=True)
    description = models.TextField(verbose_name='Дополнительная информация', blank=True)
    date_created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    date_to_not_active = models.DateField(blank=True, null=True, verbose_name='Дата перевда в неактивные')
    address = models.CharField(blank=True, db_index=True, max_length=150, verbose_name='Адрес')
    site = models.CharField(verbose_name='Сайт', max_length=128, blank=True, null=True)
    index = models.CharField(max_length=8, verbose_name='Почтовый индекс', blank=True)
    # group = models.CharField(verbose_name='Группа', choices=GROUP_CHOICES, max_length=3, blank=True)
    change_activities = models.DateTimeField(verbose_name='Изменен вид деятельности', null=True, blank=True)
    date_last_contact = models.DateTimeField(verbose_name='Дата последнего контакта', null=True, blank=True)
    part_exhibit = models.TextField(verbose_name='Участник сторонних проектов', blank=True)
    source_info = models.TextField(verbose_name='Источник информации', blank=True)
    status = models.CharField(verbose_name='Статус', max_length=10, choices=STATUS_CHOICES)
    personal_manager_type_one = models.ForeignKey('workers.Manager', blank=True, null=True,
                                                  related_name='clients_type_one')
    visitor_manager = models.ForeignKey('workers.Manager', blank=True, null=True,
                                        related_name='clients_visitors')
    old_mainid = models.PositiveIntegerField(unique=True, null=True, blank=True)
    city = models.ForeignKey('City', blank=True, null=True)
    point = models.PointField(blank=True, null=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return self.name

post_save.connect(create, sender=Client)
# Группа клиента в разрезе тематики проекта


class ClientGroup(models.Model):
    GROUP_CHOICES = (
        ('vip', 'VIP'),
        ('a', 'A'),
        ('b', 'B'),
        ('c', 'C')
    )
    client = models.ForeignKey(Client, verbose_name='Клиент', related_name='client_group')
    group = models.CharField(max_length=3, verbose_name='Группа', choices=GROUP_CHOICES)
    subjects = models.ForeignKey('projects.Subjects', verbose_name='Тематика мероприятия')
    failure = models.CharField(verbose_name='Отказы в проектах', max_length=32)
    history = HistoricalRecords()

# Контакты, история и.т.п


class Contact(models.Model):
    TYPE_CONTACT_CHOICES = (
        ('in_phone', 'Входящий звонок'),
        ('out_phone', 'Исходящий звонок'),
        ('email', 'Электронная переписка'),
        ('meet', 'Встреча')
    )
    date = models.DateTimeField(verbose_name='Дата и время', db_index=True)
    type_contact = models.CharField(max_length=32, choices=TYPE_CONTACT_CHOICES, verbose_name='Вид взаимодействия',
                                    db_index=True)
    contact_person = models.ForeignKey('ContactPerson', verbose_name='Контактное лицо',
                                       null=True, blank=True, on_delete=models.SET_NULL)
    contact_person_name = models.CharField(verbose_name='Имя контактного лица', max_length=200, blank=True)
    client = models.ForeignKey(Client, verbose_name='Клиент', related_name='contact', db_index=True)
    manager = models.ForeignKey('workers.Manager', verbose_name='Менеджер', related_name='contacts')
    project = models.ForeignKey(Project, verbose_name='Проект', blank=True, null=True, db_index=True)
    comment = models.TextField(verbose_name='Комментарий')
    contact = models.CharField(verbose_name='Контакт', max_length=100, blank=True, null=True)
    failure = models.CharField(verbose_name='Причина отказа', max_length=500, blank=True)
    guid_audio = models.CharField(max_length=32, null=True, blank=True)
    failure_bool = models.BooleanField(default=False)
    result = models.CharField(verbose_name='Результат', max_length=64)
    interest_other = models.BooleanField(default=False)
    interest = models.BooleanField(default=False)
    app = models.BooleanField(default=False)

post_save.connect(create, sender=Contact)


class ContactOld(models.Model):
    date = models.DateTimeField()
    result = models.CharField(max_length=2000, verbose_name='Результат')
    failure = models.CharField(max_length=2000, verbose_name='Отказ')
    created_user = models.ForeignKey(User)
    client = models.ForeignKey(Client)
    type_contact = models.CharField(max_length=256, verbose_name='Тип контакта', blank=True)


class NewContact(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    date_new_contact = models.DateField(verbose_name='Дата след. контакта')
    goal_contact = models.CharField(max_length=256, verbose_name='Цель контакта')
    client = models.ForeignKey(Client, related_name='new_contacts')
    project = models.ForeignKey(Project, blank=True, null=True)
    create_user = models.ForeignKey(User, verbose_name='Менеджер который назначил', blank=True, null=True)


class IndividualContact(models.Model):
    TYPE_CONTACT_CHOICES = (
        ('in_phone', 'Входящий звонок'),
        ('out_phone', 'Исходящий звонок'),
        ('email', 'Электронная переписка'),
        ('meet', 'Встреча')
    )
    source_of_information_choices = (
        ('press', 'Печатная пресса'),
        ('video', 'Видеоэкраны'),
        ('radio', 'Радио'),
        ('street_ad', 'Уличные рекламные конструкции'),
        ('internet', 'Интернет-порталы'),
        ('internet_ad', 'Реклама в интернете'),
        ('social_networks', 'Социальные сети'),
        ('recommendation', 'Рекомендация коллег/знакомых'),
        ('poster_sport', 'Афиши в фитнес-центрах'),
        ('poster_cafe', 'Афиши/флаеры в кафе'),
        ('poster_market', 'Афиши/флаеры в магазинах'),
        ('poster_university', 'Афиши в ВУЗах'),
        ('elevator_ad', 'Реклама в лифте'),
        ('email', 'E-mail рассылка'),
        ('sms', 'СМС-рассылка'),
        ('poster_street', 'Раздача флаеров на улице'),
        ('tv_ad', 'Реклама на ТВ'),
        ('app_site', 'Заявка с сайта'),
        ('other_project', 'Участник другого проекта'),
        ('public_transport_ad', 'Реклама в общественном транспорте'),
    )
    source_of_information = models.CharField(max_length=256, choices=source_of_information_choices,
                                             verbose_name='Вид источников информации', blank=True)
    date = models.DateTimeField(verbose_name='Дата и время')
    type_contact = models.CharField(max_length=32, choices=TYPE_CONTACT_CHOICES, verbose_name='Вид взаимодействия')
    individual = models.ForeignKey(Individual, related_name='individual_contacts')
    manager = models.ForeignKey('workers.Manager', verbose_name='Менеджер', related_name='individual_contacts')
    project = models.ForeignKey('projects.Project', verbose_name='Проект', related_name='individual_contacts')
    comment = models.TextField(verbose_name='Комментарий')
    contact = models.CharField(verbose_name='Контакт', max_length=32)
    failure = models.CharField(verbose_name='Причина отказа', max_length=64, blank=True)
    failure_bool = models.BooleanField(default=False)
    result = models.CharField(verbose_name='Результат', max_length=64)
    # interest_other = models.BooleanField(default=False)
    interest = models.BooleanField(default=False)
    waiting = models.BooleanField(default=False)


class NewIndividualContact(models.Model):
    date_new_contact = models.DateField(verbose_name='Дата след. контакта')
    goal_contact = models.CharField(max_length=64, verbose_name='Цель контакта')
    individual = models.ForeignKey(Individual, related_name='new_contacts')
    project = models.ForeignKey(Project, blank=True, null=True)
    create_user = models.ForeignKey(User, verbose_name='Менеджер который назначил', blank=True, null=True)


class OtherInterest(models.Model):
    new_interest = models.ForeignKey('projects.Project_type')
    com = models.CharField(max_length=400, verbose_name='Комментарий')
    contact_person = models.CharField(max_length=32, verbose_name='Контактное лицо')
    contact_data = models.CharField(max_length=32, verbose_name='Контактные данные')
    client = models.ForeignKey(Client, related_name='other_interest')

# Заявки


class Application(models.Model):
    CHOICES_STATUS = (
        ('working', 'В работе'),
        ('finished', 'Выполнен'),
        ('canceled', 'Отменен')
    )
    date = models.DateTimeField(verbose_name='Дата')
    main = models.ForeignKey('self', related_name='child_apps', null=True, blank=True)
    client = models.ForeignKey(Client, verbose_name='Клиент')
    project = models.ForeignKey(Project, verbose_name='Проект')
    manager = models.ForeignKey(Manager, verbose_name='Менеджер')
    entity = models.ForeignKey(Entity, verbose_name='Юр. лицо', null=True, blank=True)
    prepayment = models.BooleanField(verbose_name='Предоплата')
    installmen_plan = models.BooleanField(verbose_name='Рассрочка')
    barter = models.BooleanField(verbose_name='Бартер')
    document = models.BooleanField(verbose_name='Запрет на формирование документов', default=False)
    barter_founding = models.TextField(verbose_name='Обоснование бартера', blank=True)
    target_audience = models.TextField(verbose_name='Целевая аудитория', blank=True)
    production = models.TextField(verbose_name='Продукция', blank=True)
    novelties = models.TextField(verbose_name='Новинки', blank=True)
    brands = models.TextField(verbose_name='Бренды', blank=True)
    barter_sum = models.FloatField(verbose_name='Бартер на сумму', null=True, blank=True)
    status = models.CharField(verbose_name='Статус', choices=CHOICES_STATUS, max_length=16)
    bill = models.CharField(verbose_name='Номер счета в 1с', max_length=36, blank=True)
    organization = models.CharField(max_length=36, verbose_name='Организация в 1с ключ', blank=True)
    person = models.CharField(max_length=36, verbose_name='1C в лице', blank=True)

    class Meta:
        abstract = True


class ApplicationExpo(Application):
    CHOICES_COLOR = (
        ('color', 'Цветной'),
        ('bw', 'Черно-белые')
    )
    frieze_inc = models.CharField(max_length=400, verbose_name="Надпись на фриз", blank=True)
    count_symbol_frieze = models.SmallIntegerField(blank=True, null=True)
    number_stand = models.CharField(blank=True, max_length=64)
    count_frieze = models.SmallIntegerField(blank=True, null=True)
    color_frieze = models.CharField(max_length=7, blank=True)
    logo_path_frieze = models.CharField(max_length=100, verbose_name="Логотип на фриз", blank=True)
    catalog_inc = models.CharField(max_length=256, verbose_name="Наименование компании в каталоге", blank=True)
    contact_data_catalog = models.CharField(max_length=2048, verbose_name="Контактный данные в каталоге", blank=True)
    activities_catalog = models.TextField(verbose_name="Деятельность для каталога", blank=True)
    layout_path_catalog = models.CharField(max_length=256, verbose_name="Путь к макету каталога", blank=True)
    logo_path_catalog = models.CharField(max_length=256, verbose_name="Путь к лого в каталоге", blank=True)
    # color_catalog = models.CharField(max_length=5, choices=CHOICES_COLOR, blank=True)
    color_catalog_bw = models.BooleanField(verbose_name='Ч/б', default=False)
    color_catalog_color = models.BooleanField(verbose_name='Цветной', default=False)
    special_requests = models.CharField(max_length=400, verbose_name="Особые пожелания", blank=True)
    diploma_nomination = models.CharField(max_length=512, verbose_name='Номинация диплома', blank=True)
    name_org_diploma = models.CharField(max_length=512, verbose_name='Наименование организации для Диплома', blank=True)
    city_name = models.CharField(max_length=512, verbose_name='Город для диплома', blank=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Заявка экспонента'
        verbose_name_plural = 'Заявки экспонентов'

post_save.connect(create, sender=ApplicationExpo)


class ApplicationAd(Application):
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Заявка на рекламную услугу'
        verbose_name_plural = 'Заявки на рекламные услуги'


class ApplicationLeaseKVS(Application):
    date_start = models.DateField(verbose_name='Дата начала', blank=True, null=True)
    date_end = models.DateField(verbose_name='Дата окончания', blank=True, null=True)
    date_pay = models.DateField(verbose_name='Оплата до', blank=True, null=True)
    count_people = models.IntegerField(verbose_name='Количество гостей', default=0)
    sites = models.ManyToManyField(Sites, verbose_name='Площадки', blank=True)
    bill_number = models.CharField(max_length=32, verbose_name='Номер счета', blank=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Заявка на ареду площадей КВС'
        verbose_name_plural = 'Заявки на ареду площадей КВС'


# Планирование платежей

class PayApp(models.Model):
    sum = models.FloatField()
    date = models.DateField()
    percent = models.FloatField(null=True, blank=True)
    expo = models.ForeignKey(ApplicationExpo, related_name='pays', blank=True, null=True)
    lease_kvs = models.ForeignKey(ApplicationLeaseKVS, related_name='pays', blank=True, null=True)
    ad = models.ForeignKey(ApplicationAd, related_name='pays', blank=True, null=True)


# Заказанные услуги


class OrderedService(models.Model):
    nomenclature = models.ForeignKey('one_c.Nomenclature', blank=True, null=True)
    nomenclature_name = models.CharField(max_length=100, verbose_name='Название номенклатуры')
    nomenclature_guid = models.CharField(max_length=36, verbose_name='GUID Номенклатуры')
    discount_rate = models.SmallIntegerField(verbose_name='Процент скидки', blank=True, null=True)
    markup = models.SmallIntegerField(verbose_name='Наценка', blank=True, null=True)
    count = models.FloatField(verbose_name='Количество')
    count_days = models.PositiveSmallIntegerField(verbose_name='Количество дней', default=1)
    units = models.CharField(verbose_name='Ед. изм.', max_length=8)
    price = models.IntegerField(verbose_name='Стоимость')
    comment = models.CharField(max_length=250, verbose_name='Описание', blank=True, null=True)

    class Meta:
        abstract = True


class OrderedServiceExpo(OrderedService):
    app = models.ForeignKey(ApplicationExpo, related_name='ordered_service')
    history = HistoricalRecords()


class OrderedServiceAd(OrderedService):
    app = models.ForeignKey(ApplicationAd, related_name='ordered_service')
    history = HistoricalRecords()


class OrderedServiceLeaseKVS(OrderedService):
    app = models.ForeignKey(ApplicationLeaseKVS, related_name='ordered_service')
    history = HistoricalRecords()


# Контактные лица которые привязываются к физ. лицам, и к клиенту
class ContactPerson(models.Model):
    name = models.CharField(verbose_name="Обращение/ Имя контактного лица", max_length=128)
    client = models.ForeignKey(Client, verbose_name='Клиент', related_name='contacts_person')
    active = models.BooleanField(default=True)
    individual = models.ForeignKey(Individual, verbose_name="Физ. лицо", null=True, blank=True)
    phones = GenericRelation(PhoneItem)
    emails = GenericRelation(EmailItem)
    fax = models.CharField(max_length=32, verbose_name='Факс', blank=True)
    history = HistoricalRecords()

    class Meta:
        ordering = ('name',)


class Country(models.Model):
    name = models.CharField(max_length=1024)


class Region(models.Model):
    name = models.CharField(max_length=1024)
    country = models.ForeignKey(Country, blank=True, null=True)


class City(models.Model):
    name = models.CharField(max_length=2048, verbose_name='Название города')
    region = models.ForeignKey(Region, blank=True, null=True)
    country = models.ForeignKey(Country, blank=True, null=True)


class DiplomaNomination(models.Model):
    name = models.CharField(max_length=512, verbose_name='Номинация диплома')


# Create your models here.
