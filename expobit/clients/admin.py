from django.contrib import admin
from django.apps import apps
from .models import *

for model in apps.get_app_config('clients').models.values():
    admin.site.register(model)
from django.contrib import admin

# Register your models here.
