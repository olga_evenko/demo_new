define(['text!clients/html/clients_view.html', 'utils', 'selectize', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var AddEntitysPrivateView = Backbone.Marionette.View.extend({
		template:$(html).filter('#addEntitysPrivate')[0].outerHTML,
		events: {
			'click #createEntityButton': 'createEntity'
		},
		initialize: function(){
		},
		templateContext: function(){
			if(!this.model){
				return {other_model: true};
			};
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		createEntity: function(e){
			this.triggerMethod('newEntity');
		},
		onRender: function(){
			var _view = this;
			this.$el.find('#SelectEntitysPrivate').selectize({
				valueField: 'id',
				labelField: 'full_name',
				searchField: ['full_name'],
				onInitialize: function(){
					var _this=this;
					$.get({
						url: '/apiclients/entity_private/?data=min&no_client=True',
						success: function(response){
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i]);
							};
							if(_view.model){
								_this.addOption(_view.model.attributes);
								_this.setValue(_view.model.get('id'));
							};
						}
					});
				},
				onChange: function(value){
					$.ajax({
						type: 'GET',
						url: '/apiclients/entity_private/'+value+'/?data=max',
						success: function(response){
							_view.$el.find('#EntityInfo').html(Handlebars.compile($(html).filter('#EntityDataView')[0].innerHTML)(response));
							_view.$el.find('.collapse').collapse("show");
							response.activities_text = '';
							for(i in response.activities){
								if(i==response.activities.length-1){
									response.activities_text += response.activities[i].subcategories;
								}
								else{
									response.activities_text += response.activities[i].subcategories+', ';
								};
							}
							_view.EntityPrivate = response;
						}
					});
				}
			});
		}
	});
	return AddEntitysPrivateView
})
