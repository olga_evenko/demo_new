define(['text!clients/html/main_view.html', 'utils', 'selectize', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var AddEntitysPrivateView = Backbone.Marionette.View.extend({
		template:$(html).filter('#addEntitysPrivate')[0].outerHTML,
		events: {
			// 'click #SaveButton': 'save'
		},
		initialize: function(){
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		onRender: function(){
			var _view = this;
			this.$el.find('#SelectEntitysPrivate').selectize({
				valueField: 'id',
				labelField: 'full_name',
				searchField: ['full_name'],
				onInitialize: function(){
					var _this=this;
					this.disable();
					$.get({
						url: '/apiclients/entity_private/?data=min',
						success: function(response){
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i]);
							};
							if(_view.model){
								_this.setValue(_view.model.get('entity_id'));
							};
							if(_view.options.entity_model){
								_this.setValue(_view.options.entity_model.id);	
							};
							_this.enable();
						}
					});
				},
				// render:{
				// 	option: function(item, escape){	
				// 		return Handlebars.compile($(html).filter('#selectizeTemplate')[0].innerHTML)(item);
				// 	}
				// },
				onChange: function(value){
					$.ajax({
						type: 'GET',
						url: '/apiclients/entity_private/'+value+'/?data=max',
						success: function(response){
							_view.$el.find('#EntityInfo').html(Handlebars.compile($(html).filter('#EntityDataView')[0].innerHTML)(response));
							_view.$el.find('.collapse').collapse("show");
							response.activities_text = '';
							for(i in response.activities){
								if(i==response.activities.length-1){
									response.activities_text += response.activities[i].subcategories;
								}
								else{
									response.activities_text += response.activities[i].subcategories+', ';
								};
							}
							_view.EntityPrivate = response;
						}
					});
				}
			});
			this.$el.find('#selectTypePosition').selectize({
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('type_position'))
					}
				},
				options:[
				{
					text: 'Представитель',
					value:  'Представитель'
				},
				{
					text: 'Ген. директор/Владелец бизнеса/ИП',
					value:  'Ген. директор/Владелец бизнеса/ИП'
				},
				{
					text: 'Зам. директора/Комм. директор/Фин. Директор',
					value:  'Зам. директора/Комм. директор/Фин. Директор'
				},
				{
					text: 'Руководитель отдела/подразделения',
					value:  'Руководитель отдела/подразделения'
				},
				{
					text: 'Руководитель проекта/группы',
					value:  'Руководитель проекта/группы'
				},
				{
					text: 'Ведущий специалист/младший управляющий',
					value:  'Ведущий специалист/младший управляющий'
				},
				{
					text: 'Специалист/Менеджер',
					value:  'Специалист/Менеджер'
				},
				{
					text: 'Государственный служащий',
					value:  'Государственный служащий'
				},
				{
					text: 'Научный сотрудник',
					value:  'Научный сотрудник'
				},
				{
					text: 'Студент/учащийся',
					value:  'Студент/учащийся'
				},
				{
					text: 'Обслуживающий персонал/неквалифицированный рабочий',
					value:  'Обслуживающий персонал/неквалифицированный рабочий'
				},
				{
					text: 'Пенсионер',
					value:  'Пенсионер'
				}
				]
			});
		}
	});
	return AddEntitysPrivateView
})
