define(['text!clients/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var ItemIndividualView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_individual_view')[0].outerHTML,
		events: {
			'click #ClientView': 'openClientView',
			'click #HistoryContactsView': 'openHistoryContactsView'
		},
		regions: {
			navMenu: "#navMenu",
			MenuContent: "#MenuContent"
		},
		// className: 'displayNone',
		initialize: function(){
		},
		onRender: function(e){
			$('.loading').fadeOut();
			var _this = this;
			// this.$el.find(document).ready(function($) {
			// 	_this.$el.fadeIn(1000);
			// });
			this.menu = this.$el.find('li').toArray();
			require(['clients/js/tabIndividual'], function(IndividualView) {
				_this.showChildView('MenuContent', new IndividualView({model:_this.model}))
			});
		},
		openClientView: function(e){
			var _this = this;
			this.changeMenuActive(e);
			require(['clients/js/tabIndividual'], function(IndividualView) {
				_this.showChildView('MenuContent', new IndividualView({model:_this.model}))
			});
		},
		changeMenuActive: function(e){
			for(i in this.menu){
				$(this.menu[i]).removeClass('active');
			};
			$(e.currentTarget).addClass('active');
		},
		openHistoryContactsView: function(e){
			var _this = this;
			this.changeMenuActive(e);
			require(['clients/js/tabHistoryContactIndividualView'], function(tabHistoryContactView) {
				_this.showChildView('MenuContent', new tabHistoryContactView({model:_this.model}))
			});
		}

	});
	return ItemIndividualView
})
