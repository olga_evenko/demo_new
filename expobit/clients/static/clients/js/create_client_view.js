define(['text!clients/html/clients_view.html', 'utils', 'selectize', 'bootstrap-table-editable', 'bootstrap-table-locale', 'autocomplete'], function(html, utils){

	var CreateClientView = Backbone.Marionette.View.extend({
		template:$(html).filter('#create_client')[0].outerHTML,
		events: {
			'click #SaveButton': 'save',
			'click .trash': 'removeEntityPrivateAsk',
			'click .change': 'changeEntity',
			'click #addEntityPrivate': 'addEntityPrivate',
			'click #CancelButton': 'cancel',
			'click .edit': 'editEntity',
			'click #addClientInProject': 'addClientInProject',
			'click #ExitEditButton': 'ExitEditButton'
		},
		regions:{
			individualTable: "#individualTable"
		},
		className: 'displayNone',
		cancel: function(e){
			history.back();
			// mainView.GetClientsView();
		},
		templateContext: function(){
			var show = false;
			if(user.isSuperuser || user.groups.indexOf('project_managers')!=-1){
				show = true;
			}
			if(user.groups.indexOf('managers')!=-1){
				if(!this.model){
					show = true;
				}
				if(this.model && (this.model.get('created_user') == parseInt(user.id))){
					show = true;
				}
			}
			return {show: show};
		},
		checkAddress: function(place_id){
			var _view = this;
			this.$el.find('#checkAddress').collapse('show');
			$.ajax({
				type: 'POST',
				url: 'apiclients/searchaddresssearchaddress/',
				contentType: 'application/json',
				data: JSON.stringify({place_id: place_id}),
				success: function(response){
					_view.$el.find('.glyphicon-ok').remove();
					_view.$el.find('form').append($('<input type="hidden" name="lat" value='+response.geometry.location.lat+'>'));
					_view.$el.find('form').append($('<input type="hidden" name="lng" value='+response.geometry.location.lng+'>'));
					_view.$el.find('input[name="city"]').val(response.city);
					_view.$el.find('#labelAddress').append($('<span class="glyphicon glyphicon-ok" style="color: green;"></span> '));
					setTimeout(function(){
						_view.$el.find('#checkAddress').collapse('hide');
					}, 500);
					_view.$el.find('.glyphicon-remove').remove();
				},
				error: function(response, xhr){
					_view.$el.find('.glyphicon-remove').remove();
					_view.$el.find('.glyphicon-ok').remove();
					_view.$el.find('#checkAddress').collapse('hide');
					_view.$el.find('#labelAddress').append($('<span class="glyphicon glyphicon-remove" style="color: red;"></span> '));
				}
			});
			// this.$el.find('#labelAddress').text('some text');
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		copyData: function(){
			sessionStorage.setItem('client', JSON.stringify(this.$el.find('form').serializeArray()));
		},
		save: function(){
			if(this.validate()){
				var _this = this;
				$('.loading').fadeIn();
				var type = 'POST';
				var url = '/apiclients/clients/'
				if(this.model){
					type = 'PATCH';
					url = this.model.url;
				};
				var data = new FormData(this.$el.find('form')[0]);
				var client_activitiys = this.SelectActivityClient[0].selectize.getValue();
				data.delete('client_activitys');
				client_activitys = client_activitiys.split(',');
				for(i in client_activitys){
					data.append('client_activitys', client_activitys[i]);
				};
				$.ajax({
					url: url,
					type: type,
					data: data,
					contentType: false,
					processData: false,
					success: function(response){
						$('.loading').fadeOut();
						utils.notySuccess().show();
						if(_this.model){
						// _this.model.set(response);
					}
					else{
						router.navigate('client/'+response.id+'/edit', {replace: true});
						_this.model = new Backbone.Model(response);
						_this.model.url = this.url+response.id+'/';
						_this.firstSave = true;
						_this.$el.find('#addEntityPrivate').removeClass('disabled');
						_this.$el.find('#addEntityPrivate').tooltip('destroy');
						_this.$el.find('#addClientInProject').removeClass('disabled');
						_this.$el.find('#addClientInProject').tooltip('destroy');
					}
					_this.copyData();
				},
				error: function(response){
					$('.loading').fadeOut();
					utils.notyError().show();
				}
			});
			};
		},
		initialize: function(){
			this.edit = false;
		},
		ExitEditButton: function(e){
			if(this.model){
				router.navigate('client/'+this.model.id, {trigger: true});
			}
		},
		addClientInProject: function(e){
			var _view = this;
			if(!$(e.currentTarget).hasClass('disabled')){
				require(['clients/js/addClientInProject', 'bootstrap-modal'], function(AddClientInProjectView){
						var modal = new Backbone.BootstrapModal({ 
							content: new AddClientInProjectView({
								activities: _view.model.get('activities'), 
								exclude_projects: _view.model.get('projects')}),
							title: 'Выбрать проект и назначить менеджера',
							okText: 'Сохранить',
							cancelText: 'Отмена',
							okCloses: false,
							animate: true, 
						}).open();
						var ModalView = modal.options.content;
						modal.on('ok', function(){
							$('.loading').fadeIn();
							$.ajax({
								type: 'PUT',
								url: '/apiworkers/managers/replace/',
								dataType: 'json',
								data: {
									old_manager:'',
									new_manager:ModalView.selectManager[0].selectize.getValue(), 
									client: _view.model.id, 
									project: ModalView.selectProject[0].selectize.getValue()},
								success: function(response){
									_view.model.fetch({
										success: function(response){
											_view.tableProject.bootstrapTable('load', _view.model.get('projects'));
											$('.loading').fadeOut();
											modal.close();
										}
									});
								}
							});
						});
				});
			};
		},
		removeEntityPrivateAsk: function(e, bool){
			var _this = this;
			var modal = new Backbone.BootstrapModal({ 
				content: 'Изменение или удаление юр. лица может отразиться на данных проектов. Вы хотите удалить юр. лицо?',
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				_this.$el.find('#individualTable').collapse('hide');
				_this.removeEntityPrivate(e, bool);
			});
		},
		removeEntityPrivate: function(e, bool, ModalView, data_save){
			var _this = this;
			
			$('.loading').fadeIn();
			$.ajax({
				url: '/apiclients/clients/check_activities/',
				data: JSON.stringify({client: this.model.id, entity:$(e.currentTarget).data().id}),
				contentType: 'application/json',
				processData: false,
				type: 'PUT',
				success: function(response){
					var data = new FormData();
					data.append('client', '');
					$.ajax({
						url: '/apiclients/entity_private/'+$(e.currentTarget).data().id+'/',
						type: 'PATCH',
						data: data,
						contentType: false,
						processData: false,
						success: function(){
							if(!bool){
								$('.loading').fadeOut();
								utils.notySuccess().show();
								_this.Table.bootstrapTable('refresh', {
									url:'/apiclients/entity_private/?data=mid&client='+_this.model.id,
									silent: true
								});
								_this.model.fetch({
									success: function(response){
										_this.tableProject.bootstrapTable('load', _this.model.get('projects'));
									}
								});

							}
							else{
								_this.ajaxSave(ModalView, data_save);
							}
						},
						error: function(){
							if(!bool){
								$('.loading').fadeOut();
								utils.notySuccess().show();
							};
						}
					});
				},
				error: function(response){
					$('.loading').fadeOut();
					utils.notyError().show();
				}
			});
			
		},
		editEntity: function(e){
			sessionStorage.setItem('annotation_client', this.$el.find('textarea[name="annotation_activity"]').val());
			router.navigate('entity/'+$(e.currentTarget).data().id, {trigger: true});
		},
		changeEntity: function(e){
			this.edit = true;
			this.addEntityPrivate(e, new Backbone.Model(this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id)));
		},
		addEntityPrivate: function(e, model){
			if(!$(e.currentTarget).hasClass('disabled')){
				var _this = this;
				require(['clients/js/addEntitysPrivateViewForClient', 'bootstrap-modal'], function(EntitysPrivateView){
					var modal = new Backbone.BootstrapModal({ 
						content: new EntitysPrivateView({model:model}),
						title: 'Выбрать юридическое лицо',
						okText: 'Сохранить',
						cancelText: 'Отмена',
						okCloses: false,
						animate: true, 
					}).open();
					var ModalView = modal.options.content;
					modal.on('ok', function(){
						if(ModalView.validate()){
							this.close();
							$('.loading').fadeIn();
							var data = new FormData();
							data.append('client', _this.model.get('id'));
							if(_this.edit){
								_this.removeEntityPrivate(e, true, ModalView, data);
								_this.edit = false;
							}
							else{
								_this.ajaxSave(ModalView, data);
							}

						}
					});
					ModalView.on('newEntity',function(){
						modal.close();
						require(['clients/js/create_entity_view'], function(CreateEntityView) {
							mainView.showChildView('MainContent', new CreateEntityView({other_model:_this.model}));
						});
					});
				});
			};
		},
		ajaxSave:function(ModalView, data){
			var _this = this;
			$.ajax({
				url: '/apiclients/entity_private/'+ModalView.EntityPrivate.id+'/',
				type: 'PATCH',
				contentType: false,
				processData: false,
				data: data,
				success: function(response){
					_this.Table.bootstrapTable('refresh', {
						url:'/apiclients/entity_private/?data=mid&client='+_this.model.id,
						silent: true
					});
					$('.loading').fadeOut();
					utils.notySuccess().show();
					require(['clients/js/modal_individual_in_client_view', 'bootstrap-modal'], function(modalIndividualsView){
						_this.showChildView('individualTable', new modalIndividualsView({model: new Backbone.Model(response), client_id: _this.model.id, show_button: true}))
					});
				},
				error: function(xhr){
					$('.loading').fadeOut();
					utils.notyError().show();	
				}
			});
		},
		onRender: function(){
			$('.loading').fadeOut();
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
				if(!_view.model){
					_view.$el.find('#addEntityPrivate').tooltip({title: "Сохраните для добавления"});
					_view.$el.find('#addEntityPrivate').addClass('disabled');
					_view.$el.find('#addClientInProject').tooltip({title: "Сохраните для добавления"});
					_view.$el.find('#addClientInProject').addClass('disabled');
				};
			});
			var options = [
				{
					field: 'project_type',
					title: 'Вид проекта'
				},
				{
					field: 'project.name',
					title: 'Проект'
				},
				{
					field: 'group',
					title: 'Группа',
					editable: {
						source: [{text: 'A', value: 'a'},{text: 'B', value: 'b'},
						{text: 'C', value: 'c'},{text: 'VIP', value: 'vip'}],
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
						    beforeSend: function(jqXHR,settings){
							}
						},
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						emptytext: 'Назначить',
						url: '/apiclients/clients_group/set_group/',
						params: function(params){
							return params
						},
						success: function(response){
							_view.$el.find('#tableProject').bootstrapTable('refresh', {silent: true, 
								url: '/apiclients/project_table/?client='+_this.model.id});
						}
					}
				}];
			if(user.isSuperuser || user.groups.indexOf('project_managers')!=-1){
				options = [
				{
					field: 'project_type',
					title: 'Вид проекта'
				},
				{
					field: 'project.name',
					title: 'Проект'
				},
				{
					field: 'group',
					title: 'Группа',
					editable: {
						source: [{text: 'A', value: 'a'},{text: 'B', value: 'b'},
						{text: 'C', value: 'c'},{text: 'VIP', value: 'vip'}],
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
						    beforeSend: function(jqXHR,settings){
							}
						},
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						emptytext: 'Назначить',
						url: '/apiclients/clients_group/set_group/',
						params: function(params){
							return params
						},
						success: function(response){
							_view.$el.find('#tableProject').bootstrapTable('refresh', {silent: true, 
								url: '/apiclients/project_table/?client='+_view.model.id});
						}
					}
				},
				{
					field: 'manager.id',
					title: 'Менеджер',
					cellStyle: function(index, row) {
						if(row.project.user_id != user.id && !user.isSuperuser){
							return{
								classes: 'no-editble',
								css: {'font-weight': 'bold'}
							};
						}
						return {classes:''};
					},
					editable: {
						source: '/apiworkers/managers/forselect',
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
							// contentType: false,
						    // processData: false,
						    beforeSend: function(jqXHR,settings){
						    	settings.url += 'replace/';
								// debugger;
							}
						},
						sourceOptions: {
							type: 'get',
							dataType: 'json'
						},
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						prepend: '',
						emptytext: 'Назначить',
						url: '/apiworkers/managers/',
						params: function(params){
							var obj = {};
							var arr_manager = [];
							var arr_client = [];
							var check_row = _view.tableProject.bootstrapTable('getSelections');
							_view.check_row = check_row;
							var tableRow = _view.tableProject.bootstrapTable('getRowByUniqueId', params.pk);
							if(check_row.length>0){
								for(i in check_row){
									if(typeof(check_row[i].manager)=='object'){
										check_row[i].manager='';
									}
									arr_manager.push(check_row[i].manager);
									arr_client.push(check_row[i].id)
								}
								return {old_manager:arr_manager,
									new_manager:params.value, client: arr_client, project: tableRow.project.id}
								}
								if(tableRow.manager.id!=params.value){
									// if(typeof(tableRow.manager)=='object'){
									// 	tableRow.manager='';
									// }
									return {old_manager:tableRow.manager.id,
										new_manager:params.value, client: _view.model.id, project: tableRow.project.id}
									};
									return params
								},
								success: function(response){
									_view.model.fetch({
										success: function(response){
											_view.tableProject.bootstrapTable('load', _view.model.get('projects'));
										}
									});
								}
							}
						},
				]
			};
			this.tableProject = this.$el.find('#tableProject').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				data: this.model? this.model.get('projects'): [],
				columns: options,
				onPostBody: function () {
					var no_edit = _view.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					}
				}
			});
			// this.BtBtableProject = this.$el.find('#b2btableProject').bootstrapTable({
			// 	idField: 'id',
			// 	uniqueId: 'id',
			// 	url: '/apiprojects/projects_in_client/?client='+this.model.id,
			// 	pagination: true,
			// 	totalField: 'count',
			// 	sidePagination: 'server',
			// 	classes: 'table table-hover table-bordered',
			// 	dataField: 'results',
			// 	columns: [
			// 		{
			// 			field: 'project_type',
			// 			title: 'Вид проекта'
			// 		},
			// 		{
			// 			field: 'name',
			// 			title: 'Проект'
			// 		},
			// 		{
			// 			field: 'status',
			// 			title: 'Статус'
			// 		},
			// 		{
			// 			field: 'manager',
			// 			title: 'Менеджер',
			// 		},
			// 		{
			// 			field: 'contentment',
			// 			title: 'Удовлетворенность клиента'
			// 		}
			// 	]
			// });
			this.SelectStatus = this.$el.find('#SelectStatus').selectize({
				options: [
					{value: 'key', text: 'Ключевой'},
					{value: 'inactive', text: 'Неактивный'},
					{value: 'black_list', text: 'Черный список'},
					{value: 'active', text: 'Активный'}
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('status'));
					}
					else{
						this.setValue('active');
					};
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				data: this.model? this.model.get('entitys'): [],
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// rowStyle: function(){}
				// url: '/apiclients/individuals/',
				columns: [
				{
					field: 'full_name',
					title: 'Юр. лицо / Физ. лицо',
					sortable: true,
				},
				{
					field: 'address',
					title: 'Адрес',
					sortable: true,
				},
				{
					field: 'activities',
					title: 'Виды деятельности'
				},
				{
					field: '',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;">'+'<span class="glyphicon glyphicon-pencil pencil edit" data-id ='+
						row.id+'></span>'+'<span class="glyphicon glyphicon-transfer pencil change" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
						row.id+'></span></span></div>'
					},
					width: '30px'
				}
				],
				onClickRow: function(row, e, col){
					if(col != ''){
						$('.loading').fadeIn();
						var model = new Backbone.Model();
						model.url = '/apiclients/entity_private/'+row.id+'/';
						model.fetch({
							success: function(response){
								$('.loading').fadeOut();
								require(['clients/js/modal_individual_in_client_view', 'bootstrap-modal'], function(modalIndividualsView){
									_view.showChildView('individualTable', new modalIndividualsView({model: model, client_id: _view.model.id, show_button: true}))
								});
							}
						});
					};
				},
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				}
			});
			this.SelectActivityClient = this.$el.find('#SelectActivityClient').selectize({
				valueField: 'id',
				labelField: 'activity',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiclients/client_activity/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							};
							if(_view.model){
								_this.addItems(_view.model.get('client_activitys'));
							};
						},
					});
				}
			});
			this.autocomplite = this.$el.find('input[name="address"]').autocomplete({
				serviceUrl: '/apiclients/searchaddress/',
				transformResult: function(response) {
					var result = JSON.parse(response);
					var index = '';
			        return {
			            suggestions: $.map(result.predictions, function(dataItem) {
			            	// for(i in dataItem.address_components){
			            	// 	// if(dataItem.address_components[i].types[0]=='postal_code'){
			            	// 	// 	index = dataItem.address_components[i].long_name;
			            	// 	// };
			            	// };
			                return { 
			                	value: dataItem.description, 
			                	data: dataItem.description, 
			                	place_id: dataItem.place_id 
			                };
			            })
			        };
			    },
			    onSelect: function(suggestions){
			    	_view.checkAddress(suggestions.place_id);
			    	_view.$el.find('input[name="index"]').val(suggestions.index);
			    }
			});
			this.autocomplite = this.$el.find('input[name="name"]').autocomplete({
				serviceUrl: '/apiclients/clients/?data=mid&offset=0&limit=15',
				paramName: 'search',
				minChars: 2,
				formatResult: function(suggestions, value){
					if (!value) {
			            return suggestion.value;
			        }
					return '<div class="client_search" data-id='
					+suggestions.data+'><strong>'
					+suggestions.value+'</strong><p>'+suggestions.phones+'</p></div>';
				},
				transformResult: function(response) {
					var result = JSON.parse(response);
			        return {
			            suggestions: $.map(result.results, function(dataItem) {
			                return { value: dataItem.name + ' ', data: dataItem.id, phones: dataItem.phones};
			            })
			        };
			    },
			    // onSelect: function(suggestions){
			    // 	// router.navigate('client/'+suggestions.data+'/edit', {trigger: true});
			    // }
			});
			$(document).on('click', '.client_search', function(){
				router.navigate('client/'+$(this).data().id+'/edit', {trigger: true});
			});
			this.copyData();
		}
	});
return CreateClientView
})
