define(['text!clients/html/clients_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var OldContactView = Backbone.Marionette.View.extend({
		template:$(html).filter('#old_contacts_view')[0].outerHTML,
		// events: {
		// 	'click #createIndividualButton': 'createIndividual'
		// },
		templateContext: function(){
			// return {show_button: this.options.show_button}
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				// search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/contacts_old/?client='+this.model.id,
				columns: [
				{
					field: 'date',
					title: 'Дата',
					// sortable: true,
				},
				{
					field: 'result',
					title: 'Резульат',
					// sortable: true,
				},
				{
					field: 'failure',
					title: 'Отказ',
					// sortable: true,
				},
				{
					field: 'created_user',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'type_contact',
					title: 'Тип контакта'
				}
				]
			});
		}
	});
	return OldContactView;
});
