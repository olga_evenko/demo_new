define(['text!clients/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var IndividualsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_individual')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .trash': 'removeIndividual'
		},
		className: 'displayNone',
		removeIndividual: function(e){
			var id = $(e.currentTarget).data().id;
			var _view = this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить физ. лицо?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$.ajax({
					type: 'DELETE',
					url: '/apiclients/individuals/'+id+'/',
					success: function(response){
						utils.notySuccess('Удалено').show();
						_view.Table.bootstrapTable('refresh');
					},
					error: function(){
						utils.notyError().show();
					}
				});
			});
		},
		addIndividual: function(){
			router.navigate('individual/create', {trigger: true});
			// require(['clients/js/create_individual_view'], function(CreateIndividualView){
			// 	window.mainView.showChildView('MainContent', new CreateIndividualView({}));
			// })
		},
		editIndividual: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('individual/'+row.id+'/edit', {trigger: true});
			// var model = new Backbone.Model();
			// model.url = '/apiclients/individuals/'+row.id+'/';
			// $('.loading').fadeIn();
			// model.fetch({
			// 	success: function(){
			// 		require(['clients/js/create_individual_view'], function(CreateIndividualView){
			// 			window.mainView.showChildView('MainContent', new CreateIndividualView({model:model}));
			// 		})	
			// 	}
			// });
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/individuals/',
				onPageChange: function(num, size){
					router.navigate('individuals/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				columns: [
				// {
				// 	checkbox: true
				// },
				{
					field: 'surname',
					title: 'Фамилия',
					sortable: true,
				},
				{
					field: 'name',
					title: 'Имя',
					sortable: true,
				},
				{
					field: 'middle_name',
					title: 'Отчество',
					sortable: true,
				},
				{
					field: 'sex',
					title: 'Пол',
					sortable: true,
				},
				{
					field: 'date_of_birth',
					title: 'Дата рождения',
					sortable: true,
				},
				{
					field: 'email',
					title: 'E-mail',
				},
				{
					field: 'phone',
					title: 'Телефон',
				},
				{
					field: 'address',
					title: 'Адрес',
				},
				{
					field: 'date_created',
					title: 'Дата создания',
					sortable: true,
					// formatter: function(value, row, index){
					// 	return utils.dateFormatter(value);
					// }
				},
				{
					field: 'date_edited',
					title: 'Дата изменения',
					sortable: true,
					// formatter: function(value, row, index){
					// 	return utils.dateFormatter(value);
					// }
				},
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
						row.id+'></span></span></div>'
					},
					width: '89px'
				}],
				responseHandler: function(response){
					for(i in response.results){
						if(response.results[i].sex=='m'){
							response.results[i].sex='М';
						}
						else{
							response.results[i].sex='Ж';	
						}
					}
					return response
				},
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('individual/'+row.id, {trigger: true});
					}
				},
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		}
	});
	return IndividualsView
})
