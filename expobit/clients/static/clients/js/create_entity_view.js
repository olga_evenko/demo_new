define(['text!clients/html/entitys_view.html', 'utils', 'selectize', 'bootstrap-table-editable', 'bootstrap-table-locale', 'autocomplete'], function(html, utils){

	var CreateEntityView = Backbone.Marionette.View.extend({
		template:$(html).filter('#create_entity')[0].outerHTML,
		events: {
			'click #SaveButton': 'save',
			'click #SaveReturnButton': 'saveAndReturn',
			'click #CancelButton': 'cancel',
			'click #createIndividualButton': 'createIndividual',
			'click #openClientCard': 'openClientCard',
			'click #addSubSection': 'addSubSection',
			'click .delete_contact': 'destroyInput',
			'click #create_contragent': 'createContragent',
			'click .rmv': 'removeEntityPrivate',
			'click #copyAddressToPost': 'copyAddress',
			'click #update_contragent': 'updateContragent',
			'click #pasteButton': 'pasteData',
			'click #addBank': 'addBank'
		},
		className: 'displayNone',
		// блок для работы с добавлением и редактированием телефонов и прочее
		removePhones: [],
		removeEmails: [],
		flag_change_activities: false,
		removeEntityPrivate: function(e){
			var _this = this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить Физ. лицо?',
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true,
			}).open();
			modal.on('ok', function(){
				$('.loading').fadeIn();
				$.ajax({
					type: 'DELETE',
					url: '/apiclients/individual_member/'+$(e.currentTarget).data().id+'/',
					success: function(response){
						_this.Table.bootstrapTable('removeByUniqueId', $(e.currentTarget).data().id);
						$('.loading').fadeOut();
					}
				});
			});
		},
		addBank: function(e, model){
			var _view = this;
			if(this.SelectContrAgent[0].selectize.getValue()==''){
				utils.notyAlert('Выберете контрагента').show();
				return;
			}
			require(['clients/js/modal_bank_view', 'bootstrap-modal'], function(View){
				var modal = new Backbone.BootstrapModal({
					content: new View({
						model: model,
						owner: _view.SelectContrAgent[0].selectize.getValue(),
						guid_contr_agent: _view.SelectContrAgent[0].selectize.options[_view.SelectContrAgent[0].selectize.getValue()]['ОсновнойБанковскийСчет_Key']}),
					title: 'Реквизиты',
					okText: model?'Сохранить':'Создать',
					cancelText: 'Отмена',
					okCloses: false,
					animate: true,
				}).open();
				var ModalView = modal.options.content;
				modal.on('ok', function(){
					if(ModalView.validate()){
						$.ajax({
							url: '/one_c/bank/',
							type: model?'PATCH':'POST',
							contentType: false,
							processData: false,
							data: new FormData(ModalView.$el.find('form')[0]),
							success: function(response){
								utils.notySuccess().show();
								_view.BankTable.bootstrapTable('refresh');
								modal.close();
								if(response.main){
									_view.SelectContrAgent[0].selectize.options[_view.SelectContrAgent[0].selectize.getValue()]['ОсновнойБанковскийСчет_Key'] = response['Ref_key'];
								}
							},
							error: function(response){
								utils.notyError().show();
							}
						});
					}
				});
			});
		},
		pasteData: function(e){
			var data = JSON.parse(sessionStorage.getItem('client'));
			for(var i in data){
				if(data[i].name == 'name'){
					this.$el.find('input[name="full_name"]').val(data[i].value);
				}
				if(data[i].name == 'address'){
					this.$el.find('input[name="post_address"]').val(data[i].value);
				}
				this.$el.find('input[name="'+data[i].name+'"]').val(data[i].value);
			}
			sessionStorage.removeItem('client');
			$(e.currentTarget).remove();
		},
		updateContragent: function(e){
			var _this = this;
			var model = new Backbone.Model(this.SelectContrAgent[0].selectize.options[this.SelectContrAgent[0].selectize.getValue()]);
			var address_ent = this.$el.find('input[name="address"]').val();
				require(['clients/js/check_contragent_view', 'bootstrap-modal'], function(CreateContrAgentView){
					var modal = new Backbone.BootstrapModal({
						content: new CreateContrAgentView({model: model, address: address_ent}),
						title: 'Проверить контрагента',
						okText: 'Сохранить',
						cancelText: 'Отмена',
						okCloses: false,
						animate: true,
					}).open();
					var ModalView = modal.options.content;
					modal.on('ok', function(){
						if(ModalView.validate()){
							this.close();
							$('.loading').fadeIn();
							var form = ModalView.$el.find('form');
							var data = new FormData(form[0]);
							data.append('Ref_Key', model.get('Ref_Key'));
							$.ajax({
								type: 'PATCH',
								contentType: false,
								processData: false,
								url: '/apiclients/entity_private/one_c/',
								data: data,
								success: function(response){
									$('.loading').fadeOut();
									if(_this.$el.find('input[name="full_name"]').val() != modal.options.content.model.get('new_name')){
										_this.$el.find('input[name="full_name"]').parent().addClass('has-warning has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>');
										$('body').animate({scrollTop: 0}, 500, function(){});
										_this.$el.find('input[name="full_name"]').parent().find('label').text(_this.$el.find('input[name="full_name"]').parent().find('label').text()+' ('+_this.$el.find('input[name="full_name"]').val()+')');
										_this.$el.find('input[name="full_name"]').val(modal.options.content.model.get('new_name'));
									}
									utils.notySuccess('Контрагент обновлен').show();
									_this.SelectContrAgent[0].selectize.removeOption(response.Ref_Key);
									_this.SelectContrAgent[0].selectize.addOption(response);
									_this.SelectContrAgent[0].selectize.setValue(response.Ref_Key);
								},
								error: function(response){
									utils.notyError().show();
								}
							});


						}
					});
					ModalView.on('newEntity',function(){
						modal.close();
						require(['clients/js/create_entity_view'], function(CreateEntityView) {
							mainView.showChildView('MainContent', new CreateEntityView({other_model:_this.model}));
						});
					});
				});
		},
		createContragent: function(e, change){
			var _this = this;
			var address_ent = this.$el.find('input[name="address"]').val();
				require(['clients/js/modal_create_contragen_view', 'bootstrap-modal'], function(CreateContrAgentView){
					var model;
					if(_this.SelectContrAgent[0].selectize.getValue()!=''){
						model = new Backbone.Model(_this.SelectContrAgent[0].selectize.options[_this.SelectContrAgent[0].selectize.getValue()]);
						model.set('СвидетельствоДатаВыдачи', model.get('СвидетельствоДатаВыдачи').split('T')[0]);
						if(model.get('КонтактнаяИнформация')){
							model.set('Адрес', model.get('КонтактнаяИнформация')[0]['Представление']);
						}
					}
					var modal = new Backbone.BootstrapModal({
						content: new CreateContrAgentView({
							address: address_ent,
							model: model,
							other: _this.$el.find('#SelectType')[0].selectize.getValue()}),
						title: model?'Изменить контрагента': 'Создать контрагента',
						okText: model?'Сохранить': 'Создать',
						cancelText: 'Отмена',
						okCloses: false,
						animate: true,
					}).open();
					var ModalView = modal.options.content;
					modal.on('ok', function(){
						if(ModalView.validate()){
							this.close();
							$('.loading').fadeIn();
							var form = ModalView.$el.find('form');
							var data = new FormData(form[0]);
							$.ajax({
								type: model?'PATCH':'POST',
								contentType: false,
								processData: false,
								url: '/apiclients/entity_private/one_c/',
								data: data,
								success: function(response){
									$('.loading').fadeOut();
									utils.notySuccess(model?'Контрагент изменен':'Контрагент создан').show();
									_this.SelectContrAgent[0].selectize.addOption(response);
									_this.SelectContrAgent[0].selectize.setValue(response.Ref_Key);
									_this.$el.find('#update_contragent').attr('disabled', false);
									if(model){
										_this.SelectContrAgent[0].selectize.options[response['Ref_Key']] = response;
									}
								},
								error: function(response){
									utils.notyError().show();
								}
							});


						}
					});
					ModalView.on('newEntity',function(){
						modal.close();
						require(['clients/js/create_entity_view'], function(CreateEntityView) {
							mainView.showChildView('MainContent', new CreateEntityView({other_model:_this.model}));
						});
					});
				});
		},
		destroyInput: function(e){
			var input = $(e.currentTarget).parent().parent();
			if(input.find('input[type="text"]').attr('name')=='phone_'+input[0].id && input[0].id != ''){
				this.removePhones.push(input[0].id);
			}
			if(input.find('input[type="text"]').attr('name')=='email_'+input[0].id && input[0].id != ''){
				this.removeEmails.push(input[0].id);
			}
			input.remove();
		},
		addSubSection: function(e){
			var type = $(e.currentTarget).data().type;
			var valid_class = '';
			if(type == 'phones'){
				valid_class = 'valid_phone ';
			}
			this.$el.find('#subsection_'+$(e.currentTarget).data().type).append('<div class="input-group inputWithButton">\
				<span class="input-group-addon input-sm"><input type="radio" name="main_'+$(e.currentTarget).data().type+'"></span>\
				<input type="text" class="'+valid_class+'form-control input-sm" name="'+
				$(e.currentTarget).data().type+
				'" data-validate="True">\
				<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-trash trash delete_contact"></span></span></div>');
		},
		// конец
		cancel: function(e){
			history.back();
			// mainView.GetEntitysView();
		},
		templateContext: function(){
			var pasteButton = false;
			if(sessionStorage.getItem('client')){
				pasteButton = true;
			}
			if(this.options.other_model){
				return {other_model: true, pasteButton: pasteButton};
			}
			return {pasteButton: pasteButton};
		},
		createIndividual: function(e){
			var _this = this;
			if(!$(e.currentTarget).hasClass('disabled')){
				require(['clients/js/create_individual_view'], function(CreateIndividualView){
					window.mainView.showChildView('MainContent', new CreateIndividualView({entity_model: _this.model, redirect: 'clients/js/create_entity_view'}));
				});
			};
		},
		openClientCard: function(e){
			var _this = this;
			debugger
			router.navigate('client/'+_this.model.attributes.client,  {trigger: true});
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
				if($(inputs[i]).attr('name') && $(inputs[i]).attr('name').match('email')){
					if(!re.test($(inputs[i]).val())){
						$(inputs[i]).parent().addClass('has-error');
						valid = false;
					};
				};
			};			
			var radio_count_phones = this.$el.find('input[type="radio"][name="main_phones"]').length;
			var radio_checked_count_phones = this.$el.find( 'input:checked[name="main_phones"]' ).length;
			if(radio_count_phones>0 && radio_checked_count_phones!=1){
				utils.notyAlert('Выберете основной телефон').show();
				valid = false;
			};
			var radio_count_emails = this.$el.find('input[type="radio"][name="main_emails"]').length;
			var radio_checked_count_emails = this.$el.find( 'input:checked[name="main_emails"]' ).length;
			if(radio_count_emails>0 && radio_checked_count_emails!=1){
				utils.notyAlert('Выберете основной email').show();
				valid = false;
			};
			return valid;
		},
		check_client_activities: function(){
			if(this.model.get('client') && this.flag_change_activities){
				$.ajax({
					url: '/apiclients/clients/check_activities/',
					data: JSON.stringify({client: this.model.get('client'), entity:this.model.id}),
					contentType: 'application/json',
					processData: false,
					type: 'PUT',
					success: function(response){}
				});
			};
		},
		saveAndReturn: function(e){
			this.save(e, true);
		},
		copyAddress: function(e){
			this.$el.find('input[name="post_address"]').val(this.$el.find('input[name="address"]').val());
		},
		save: function(e, ret){
			var _view = this;
			if(this.validate()){
				var _this = this;
				$('.loading').fadeIn();
				var type = 'POST';
				var url = '/apiclients/entity_private/'
				if(this.model){
					type = 'PATCH';
					url = this.model.url;
				};
				var data = new FormData(this.$el.find('form')[0]);
				for(i in this.removePhones){
					data.append('removePhones', this.removePhones[i]);
				};
				for(i in this.removeEmails){
					data.append('removeEmails', this.removeEmails[i]);
				};
				var activities = this.SelectActivities[0].selectize.getValue().split(',')
				for (i in activities){
					data.append('activities', activities[i]);
				};
				if(this.options.other_model){
					data.append('client', this.options.other_model.id);
				};
				// if(this.flag_change_activities && this.model.get('client')){
				// 	data.append('chnge', this.model.get('client'));
				// };
				$.ajax({
					url: url,
					type: type,
					data: data,
					contentType: false,
					processData: false,
					success: function(response){
						_this.removePhones = [];
						_this.removeEmails = [];
						_this.$el.find('#createIndividualButton').removeClass('disabled');
						_this.$el.find('#createIndividualButton').tooltip('destroy');
						utils.notySuccess().show();
						_this.$el.find('input[name="full_name"]').parent().find('label').text('Полное наименование:');
						_this.$el.find('input[name="full_name"]').parent().removeClass('has-warning has-feedback');
						_this.$el.find('.glyphicon-warning-sign').remove();
						var phone_obj = {};
						var email_obj = {};
						var arr_phone_forms = _view.$el.find('#subsection_phones input[type="text"]').toArray();
						var arr_email_forms = _view.$el.find('#subsection_emails input[type="text"]').toArray();
						for(var i in response.phones){
							phone_obj[response.phones[i].phone] = response.phones[i].id;
						}
						for(var i in response.emails){
							email_obj[response.emails[i].email] = response.emails[i].id;
						}
						for(var i in arr_phone_forms){
							arr_phone_forms[i].name = 'phone_' + phone_obj[arr_phone_forms[i].value];
						}
						for(var i in arr_email_forms){
							arr_email_forms[i].name = 'email_' + email_obj[arr_email_forms[i].value];
						}

						if(_this.model){
							_this.model.set(response);
							// _this.check_client_activities();
						}
						else{
							router.navigate('entity/'+response.id, {replace: true});
							_this.model = new Backbone.Model(response);
							_this.model.url = this.url+response.id+'/';
							_this.firstSave = true;
						};
						if(ret){
							history.back();
							// _this.options.other_model.fetch({
							// 	success: function(response){
							// 		require(['clients/js/create_client_view'], function(CreateClientView){
							// 			window.mainView.showChildView('MainContent', new CreateClientView({model:_this.options.other_model}));
							// 		});
							// 		$('.loading').fadeOut();
							// 	}
							// });
						}
						else{
							$('.loading').fadeOut();
						};
					},
					error: function(response){
						// utils.notyError().show();
						$('.loading').fadeOut();
						if(response.responseJSON.one_c_guid){
							utils.notyError(response.responseJSON.one_c_guid[0]).show();
							$('.loading').fadeOut();
							return;
						}
						if(response.responseJSON.error_name){
							if(type == 'POST'){
								utils.notyError(response.responseJSON.error_name).show();
								_this.model = new Backbone.Model();
								_this.model.url = this.url + response.responseJSON.id + '/';
								_this.model.fetch();
								router.navigate('entity/' + response.responseJSON.id, {replace: true});
								_this.$el.find('#createIndividualButton').removeClass('disabled');
								_this.$el.find('#createIndividualButton').tooltip('destroy');
								$('.loading').fadeOut();
								return;
							}
							if(type == 'PATCH'){
								utils.notyError(response.responseJSON.error_name).show();
								return;
							}
						}
						utils.notyError().show();
						// $('.loading').fadeOut();
					}
				});
			};
		},
		initialize: function(){
			this.removePhones = [];
			this.removeEmails = [];
			if(this.options.other_model){
				router.navigate('entity/create');
			}
		},
		onRender: function(){
			$('.loading').fadeOut();
			if(!this.model){
				this.$el.find('#createIndividualButton').addClass('disabled');
			};
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
				if(!_view.model){
					_view.$el.find('#createIndividualButton').tooltip({title: 'Сохраните для добавлнеия'});
				};
			});
			this.$el.on('click', 'input[type="radio"]', function(e) {
				var _this = this;
				$(this.parentElement.parentElement.parentElement).find('input[type="radio"]').val('')
				$(this.parentElement.parentElement.getElementsByClassName('form-control')[0]).change(function(event) {
					_this.value = _this.parentElement.parentElement.getElementsByClassName('form-control')[0].value;
				});
				this.value = _this.parentElement.parentElement.getElementsByClassName('form-control')[0].value;
			});
			this.SelectContrAgent = this.$el.find('#SelectContrAgent').selectize({
				valueField: 'Ref_Key',
				labelField: 'НаименованиеПолное',
				searchField: ['ИНН','Description'],
				// options:[{'Ref_Key':'678', 'ИНН':'afa'}],
				loadingClass: 'loader',
				onInitialize: function(){
					var _this = this;
					if(_view.model){
						if(_view.model.get('one_c_guid')){
							$.get({
								url: '/apiclients/entity_private/one_c/?guid='+_view.model.get('one_c_guid'),
								async: true,
								success: function(response) {
	                        	// _this.clearOptions();
	                        	_view.$el.find('#update_contragent').attr('disabled', false);
	                        	_this.addOption(response);
	                        	_this.setValue(_view.model.get('one_c_guid'));
	                        	}
							});
						};
					};
				},
				load: function(input) {
					var _this = this;
					if(input!='' && input.length>3){
						// this.clearOptions();
						clearTimeout(this.timeout_id);
						this.timeout_id = setTimeout(function(){
							$.get({
								url: '/apiclients/entity_private/one_c/?inn='+input,
								success: function(response){
									for(i in response){
										_this.addOption(response[i]);
									}
									_this.open();
								}
							});		
						}, 500);
					}
                    // var _this=this;
                    // var response = $.get({
                    //     url: '/apiclients/entity_private/one_c/?inn='+input,
                    //     async: true,
                    //     success: function(response) {
                    //     	// _this.clearOptions();
                    //     	for(i in response){
                    //     		_this.addOption(response[i]);
                    //     	};
                    //         _this.open();
                    //         return response
                    //     }
                    // });
                },
                onChange: function(value){
                	_view.$el.find('#update_contragent').attr('disabled', false);
                	_view.BankTable.bootstrapTable('refresh', {url:'/one_c/bank/?guid='+value});
                },
                render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeTemplate')[0].innerHTML)(item);
					}
				}
                // onChange: function(){}
			});
			
			this.SelectAuthorizedPersons = this.$el.find('#SelectAuthorizedPersons').selectize({
				valueField: 'guid',
				labelField: 'description',
				searchField: 'description',
				onInitialize: function(){
					var _this=this;
					$.ajax({
						url: '/one_c/authorized_persons/',
						type: 'GET',
						cache: true,
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
							if(_view.model){
								_this.setValue(_view.model.get('auth_person'));
							}
						}
					});
				},
				onItemRemove: function(value){
					_view.flag_change_activities = true;
				},
				onItemAdd: function(value){
					if(this.settings.flag){
						_view.flag_change_activities = true;
					};
				}
			});
			this.SelectType = this.$el.find('#SelectType').selectize({
				options: [{text: 'Юр. лицо / ИП', value: 'false'}, {text: 'Физ. лицо', value: 'true'}],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('other').toString());
					}
					else{
						this.setValue('false');
					};
				}
			});
			this.SelectActivities = this.$el.find('#SelectActivities').selectize({
				valueField: 'id',
				labelField: 'subcategories',
				searchField: ['subcategories', 'value'],
				optgroupField: 'category',
				optgroupLabelField: 'display_name',
				flag: false,
				onInitialize: function(){
					var _this=this;
					$.ajax({
						url: '/apiprojects/activities/',
						type: 'OPTIONS',
						cache: true,
						success: function(response){
							var optGr = response.actions.POST.category.choices;
							for (i in optGr){
								_this.addOptionGroup(optGr[i].value, optGr[i]);
							}
							$.get({
								url: '/apiprojects/activities/',
								async: true,
								success: function(response) {
									for(var i=0; i<response.length; i++){
										_this.addOption(response[i]);
									};
									if(_view.model){
										_this.addItems(_view.model.get('activities'));
										_this.settings.flag = true;
									};
									return response;
								}
							});
						}
					})
				},
				onItemRemove: function(value){
					_view.flag_change_activities = true;
				},
				onItemAdd: function(value){
					if(this.settings.flag){
						_view.flag_change_activities = true;
					};
				}
			});
			this.autocomplite = this.$el.find('input[name="address"]').autocomplete({
				serviceUrl: '/apiclients/searchaddress/',
				transformResult: function(response) {
					var result = JSON.parse(response);
			        return {
			            suggestions: $.map(result.predictions, function(dataItem) {
			                return { value: dataItem.description, data: dataItem.description};
			            })
			        };
			    }
			});

			this.BankTable = this.$el.find('#bankTable').bootstrapTable({
				idField: 'Ref_Key',
				uniqueId: 'Ref_Key',
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				toolbar: $(html).filter('#toolbarBank')[0].innerHTML,
				// url: '/one_c/bank/',
				columns: [
				{
					field: 'Description',
					title: 'Описание',
				},
				{
					field: 'НомерСчета',
					title: 'Номер счета',
				}
				],
				responseHandler: function(response){
					return response.value;
				},
				onClickRow: function(row, index){
					_view.addBank(undefined, new Backbone.Model(row));
				},
				rowStyle: function(){
					return {classes: '', css: {cursor: 'pointer'}};
				}
			});


			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				data: this.model?this.model.get('entity_private'):[],
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// rowStyle: function(){}
				// url: '/apiclients/individuals/',
				columns: [
				{
					field: 'individual',
					title: 'ФИО',
					sortable: true,
					cellStyle: function(){
						return {
							classes: '',
							css: {
								cursor: 'pointer'
							}
						};
					},
				},
				{
					field: 'type_position',
					title: 'Тип должности',
					sortable: true,
				},
				{
					field: 'position',
					title: 'Должность',
					sortable: true,
				},
				{
					field: 'phone',
					title: 'Телефон',
					sortable: false
				},
				{
					field: '',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash rmv" data-id ='+
						row.id+'></span></span></div>'
					},
				}
				],
				onClickRow: function(row, element, index){
					if(index == ''){
						return;
					}
					router.navigate('individual/'+row.individual_id, {trigger: true});
				}
			});
			this.$el.find('#annotation_client').text(sessionStorage.getItem('annotation_client'));
			sessionStorage.removeItem('annotation_client');
		}
	});
return CreateEntityView;
});
