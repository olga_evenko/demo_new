define(['text!clients/html/main_view.html', 'utils', 
	'clients/js/applications/ApplicationExpoView', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize', 'datetimepicker'], function(html, utils, ApplicationExpoView){

	var TabHistoryContactView = Backbone.Marionette.View.extend({
		template:$(html).filter('#tabHistoryContacts')[0].outerHTML,
		className: 'displayNone',
		events:{
			'click #failure': 'failure_check',
			'click #saveContact': 'validate',
			'click #addProject': 'addProject'
		},
		results:{
			'failure':{
				res: false,
				title: 'Отказ'
			},
			'waiting':{
				res: false,
				title: 'Ожидание оплаты'
			},
			'goal_contact':{
				res: false,
				title: 'Запланирован контакт'
			},
			// 'new_interest':{
			// 	res: false,
			// 	title: 'Интерес на др. услуги'
			// },
			'interest':{
				res: false, 
				title: 'Интерес'
			}
		},
		templateContext: function(){
			// if(user.groups.indexOf('kvs')!=-1){
			// 	return {kvs: true}
			// }
		},
		getResults: function(){
			// var inputs = this.$el.find('.results input').toArray();
			var textarr = [];
			for(i in this.results){
				if(this.$el.find('[name="'+i+'"]').val() != '' && this.$el.find('[name="'+i+'"]').val() != undefined){
					if(this.$el.find('[name="'+i+'"]').attr('type')=='checkbox'){
						if(this.$el.find('[name="'+i+'"]').is(':checked')){
							this.results[i].res = true;
							textarr.push(this.results[i].title);
						};
					}
					else{
						this.results[i].res = true;
						textarr.push(this.results[i].title);
					};
				};
			};
			// for(i in inputs){
			// 	if($(inputs[i]).val() != ''){
			// 		if(this.results[$(inputs[i]).attr('name')])
			// 			this.results[$(inputs[i]).attr('name')].res = true;
			// 	};
			// };
			// for(i in this.results){
			// 	if(this.results[i].res){
			// 		textarr.push(this.results[i].title);
			// 	};
			// };
			return textarr.join(', ');
		},
		mainSave: function(){},
		saveApplication: function(){},
		// addProject: function(e){
		// 	var _view = this;
		// 	require(['projects/js/project_form_view', 'projects/js/modal_template'], function(ProjectView, tmp){
		// 		var modal = new Backbone.BootstrapModal({ 
		// 			content: new ProjectView({client: {id:_view.model.id, name: _view.model.get('name')}}),
		// 			title: '',
		// 			okText: 'Сохранить',
		// 			cancelText: 'Отмена',
		// 			animate: true, 
		// 			template: tmp
		// 		}).open();
		// 		// modal.options.content.$el.fadeOut();
		// 		modal.on('ok', function(){
		// 			modal.options.content.save();
		// 		});
		// 		modal.options.content.on('closeModal', function(option){
		// 			_view.SelectProject[0].selectize.addOption(option);
		// 			_view.SelectProject[0].selectize.setValue(option.id);
		// 			modal.close();
		// 		});
		// 	});
		// },
		// saveOtherService: function(){
		// 	var _this = this;
		// 	var data = new FormData();
		// 	var inputs = $('.other_services input').toArray();
		// 	for(i in inputs){
		// 		data.append($(inputs[i]).attr('name'),$(inputs[i]).val());
		// 	};
		// 	data.append('new_interest', _this.SelectizeBuisnessUnit[0].selectize.getValue());
		// 	data.append('client', _this.model.id);
		// 	data.append('contact_person', this.SelectIndividual[0].selectize.options[this.SelectIndividual[0].selectize.getValue()].name);
		// 	data.append('contact_data', this.selectContact[0].selectize.options[this.selectContact[0].selectize.getValue()].text);
		// 	$.ajax({
		// 		type: 'POST',
		// 		url: '/apiclients/other_interest/',
		// 		data: data,
		// 		contentType: false,
		// 		processData: false,
		// 		success: function(response){
		// 			utils.notySuccess('Передан интерес на др. услуги').show();
		// 			_this.resetForm();
		// 		},
		// 		error: function(response){
		// 			utils.notyError('Ошибка передчи интереса на др. услуги').show();
		// 		}
		// 	});
		// },
		saveNewContact: function(){
			var _this = this;
			var data = new FormData();
			var inputs = $('.followContact input').toArray();
			for(i in inputs){
				data.append($(inputs[i]).attr('name'),$(inputs[i]).val());
			};
			data.append('project', _this.SelectProject[0].selectize.getValue());
			data.append('individual', _this.model.id);
			$.ajax({
				type: 'POST',
				url: '/apiclients/individual_new_contacts/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					utils.notySuccess('Новый след. контакт сохранен').show();
					_this.resetForm();
				},
				error: function(response){
					utils.notyError('Ошибка сохранения след. контакта').show();
				}
			});

		},
		validate: function(e){
			var valid = false;
			var inputs = this.$el.find('.results input').toArray();
			for(i in inputs){
				if($(inputs[i]).val()!=''){
					if($(inputs[i]).attr('type')=='checkbox'){
						if(!valid){
							valid = $(inputs[i]).is(':checked');
						};
					}
					else{
						valid = true;
					};
				}
			};
			if(this.$el.find('.results select').val()!=''){
				valid = true;
			};
			if(valid){
				this.save();
			}
			else{
				utils.notyAlert('Заполните результат').show();
			};
		},
		save: function(e){
			var _this = this;
			var data = new FormData(this.$el.find('form')[0]);
			data.append('result', this.getResults());
			data.append('individual', this.model.id);
			var notInContact = this.$el.find('.notInContact input').toArray();
			for(i in notInContact){
				data.delete($(notInContact[i]).attr('name'));
			};
			if(this.results.goal_contact.res){
				var inputs = $('.followContact input').toArray();
				for(i in inputs){
					data.append($(inputs[i]).attr('name'),$(inputs[i]).val());
				}
				data.append('save_new_contact', true);
				// this.saveNewContact();
			};
			// if(this.results.new_interest.res){
			// 	this.saveOtherService();
			// };
			data.set('date', this.$el.find('#datetimepicker2').data("DateTimePicker").date().format());
			$.ajax({
				type: 'POST',
				url: '/apiclients/individual_contacts/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					utils.notySuccess().show();
					_this.model.fetch();
					// if(!_this.results.goal_contact.res){
					// 	if(_this.results.application.res){
					// 		window.mainView.showChildView('MainContent', new ApplicationExpoView({
					// 			client_model: _this.model,
					// 			app_url:_this.$el.find('#select_application').val(),
					// 			selectizeOptions:{
					// 				options: _this.SelectProject[0].selectize.options,
					// 				item: _this.SelectProject[0].selectize.items
					// 			},
					// 			project_id: _this.$el.find('#selectProject').val(),
					// 			redirect: 'clients/js/applications/ApplicationExpoView'
					// 		}));
					// 	}
						// else{
							_this.resetForm();
						// }
					// };
					_this.$el.find('#table').bootstrapTable('refresh', {silent: true})
				},
				error: function(response){
					if(response.responseJSON && response.responseJSON.manager_error){
						utils.notyError(response.responseJSON.manager_error).show();
					}
					else{
						utils.notyError().show();
					}
				}
			});
		},
		failure_check: function(e){
			if(this.$el.find('input[name="failure"]').attr('disabled')){
				this.$el.find('input[name="failure"]').attr('disabled', false);
			}
			else{
				this.$el.find('input[name="failure"]').attr('disabled', true);
			};
		},
		resetForm: function(){
			this.$el.find('form')[0].reset();
			this.$el.find('input[name="failure"]').attr('disabled', true);
			var selectized = this.$el.find('.selectized').toArray();
			for(i in selectized){
				selectized[i].selectize.clear();
			};
			this.$el.find('#datetimepicker2').data("DateTimePicker").date(new Date());
			for(i in this.results){
				this.results[i].res = false;
			};
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.$el.find('#datetimepicker2').datetimepicker({
                    locale: 'ru',
                    defaultDate: new Date()
            });
			this.$el.find('#selectTypeContact').selectize({
				labelField: 'display_name',
				searchField: 'display_name',
				onInitialize: function(){
					var _this = this;
					$.get({
						type: 'OPTIONS',
						url: '/apiclients/contacts/',
						success: function(response){
							var choices = response.actions.POST.type_contact;
							for(i in choices){
								_this.addOption(choices[i]);
							};
						}
					});
				}
			});
			this.$el.find('#selectSourceInformation').selectize({
				labelField: 'display_name',
				searchField: 'display_name',
				onInitialize: function(){
					var _this = this;
					$.get({
						type: 'OPTIONS',
						url: '/apiclients/individual_contacts/',
						success: function(response){
							var choices = response.actions.POST.source_of_information;
							for(i in choices){
								_this.addOption(choices[i]);
							};
						}
					});
				}
			});
			this.selectContact = this.$el.find('#selectContact').selectize({
				valueField: 'text',
				onInitialize: function(){
					var phones = _view.model.get('phones');
					var emails = _view.model.get('emails');
					var contacts = [];
					for(var i in phones){
						contacts.push({text: phones[i].phone, phone: true, value: phones[i].phone, main: phones[i].main});
					}
					for(var i in emails){
						contacts.push({text: emails[i].email, email: true, value: emails[i].emails, main: emails[i].main});
					}
					for(var i in contacts){
						this.addOption(contacts[i]);
					}
				},
				onChange: function(value){
					_view.$el.find('#contact_parametrs a').remove();
					_view.$el.find('#email_copy').remove();
					if(value != ""){
						if(this.options[value].phone){
							_view.$el.find('#contact_parametrs').append('<a style="padding-left: 10px;" href="callto:'+value+'"><span class="glyphicon glyphicon-earphone"></span></a>');
						}
						if(this.options[value].email){
							_view.$el.find('#contact_parametrs').parent().append('<input id="email_copy" class="form-control" type="text" value="'+value+'">');
							_view.$el.find('#email_copy').select();
							document.execCommand("copy");
							_view.$el.find('#contact_parametrs').append('<a target="_blank" style="padding-left: 10px;" href="http://mail.donexpocentre.ru"><span class="glyphicon glyphicon-envelope"></span></a>');
						}
					}
				},
				render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeTemplateContact')[0].innerHTML)(item);
					}
				}
			});
			this.SelectProject = this.$el.find('#selectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						type: 'GET',
						url: '/apiprojects/projects/?data=min&status=working',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
			});
			var options_apps = [
				{text:'Заявка экспонента', value:'expo'},
				{text:'Заявка на рекламные услуги/ застройку и брендирование', value:'ad'},
				{text:'Заявка на аренду площадей КВС',value:'lease_kvs'}];
			if(user.groups.indexOf('kvs')!=-1){
				options_apps = [
					{text:'Заявка на аренду площадей КВС',value:'lease_kvs'}];
			}
			// this.$el.find('#select_application').selectize({
			// 	options:options_apps
			// });
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/individual_contacts/?individual=' + this.model.id,
				columns: [
				// {
				// 	checkbox: true
				// },
				{
					field: 'date',
					title: 'Дата',
					sortable: true,
				},
				{
					field: 'type_contact',
					title: 'Вид взаимодействия',
					sortable: true,
				},
				{
					field: 'source_of_information',
					title: 'Вид источников информации',
					sortable: true
				},
				{
					field: 'contact',
					title: 'Контакт',
					sortable: true,
				},
				{
					field: 'project',
					title: 'Проект',
					sortable: true,
				},
				{
					field: 'manager',
					title: 'Менеджер',
					sortable: true,
				},
				{
					field: 'comment',
					title: 'Комментарии',
					// sortable: true,
				},
				{
					field: 'result',
					title: 'Результат'
				}
				],
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'manager'){
						params.sort = 'manager__user__last_name';
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		}
	});
	return TabHistoryContactView
})
