define(['text!clients/html/clients_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var ContactPersonView = Backbone.Marionette.View.extend({
		template:$(html).filter('#contact_person_view')[0].outerHTML,
		events: {
			'click #createIndividualButton': 'createIndividual',
			'click #addSubSection': 'addSubSection',
			'click .glyphicon-trash': 'destroyInput'
		},
		removePhones: [],
		removeEmails: [],
		destroyInput: function(e){
			var input = $(e.currentTarget).parent().parent();
			if(input.find('input[type="text"]').attr('name')=='phone_'+input[0].id && input[0].id != ''){
				this.removePhones.push(input[0].id);
			};
			if(input.find('input[type="text"]').attr('name')=='email_'+input[0].id && input[0].id != ''){
				this.removeEmails.push(input[0].id);
			};
			input.remove();
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				}
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				}
				if($(inputs[i]).attr('name') && $(inputs[i]).attr('name').match('email')){
					if(!re.test($(inputs[i]).val())){
						$(inputs[i]).parent().addClass('has-error');
						valid = false;
					}
				}
			}
			var radio_count_phones = this.$el.find('input[type="radio"][name="main_phones"]').length;
			var radio_checked_count_phones = this.$el.find( 'input:checked[name="main_phones"]' ).length;
			if(radio_count_phones>0 && radio_checked_count_phones!=1){
				utils.notyAlert('Выберете основной телефон').show();
				valid = false;
			}
			var radio_count_emails = this.$el.find('input[type="radio"][name="main_emails"]').length;
			var radio_checked_count_emails = this.$el.find( 'input:checked[name="main_emails"]' ).length;
			if(radio_count_emails>0 && radio_checked_count_emails!=1){
				utils.notyAlert('Выберете основной email').show();
				valid = false;
			}
			return valid;
		},
		// addSubSection: function(e){
		// 	this.$el.find('#subsection_'+$(e.currentTarget).data().type).append('<div class="input-group inputWithButtonModal">\
		// 		<input type="text" class="valid_phone form-control input-sm" name="'+
		// 		$(e.currentTarget).data().type+
		// 		'" data-validate="True">\
		// 		<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-trash trash"></span></span></div>');
		// },
		addSubSection: function(e){
			var type = $(e.currentTarget).data().type;
			var valid_class = '';
			if(type == 'phones'){
				valid_class = 'valid_phone ';
			}
			this.$el.find('#subsection_'+$(e.currentTarget).data().type).append('<div class="input-group inputWithButtonModal">\
				<span class="input-group-addon input-sm"><input type="radio" name="main_'+$(e.currentTarget).data().type+'"></span>\
				<input type="text" class="'+valid_class+'form-control input-sm" name="'+
				$(e.currentTarget).data().type+
				'" data-validate="True">\
				<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-trash trash delete_contact"></span></span></div>');
		},
		// templateContext: function(){
		// 	return {show_button: this.options.show_button}
		// },
		// createIndividual: function(){
		// 	var _this = this;
		// 	require(['clients/js/create_individual_view'], function(CreateIndividualView){
		// 		_this.model.url = 'apiclients/clients/'+_this.model.get('client')+'/';
		// 		window.mainView.showChildView('MainContent', new CreateIndividualView({entity_model: _this.model, redirect: 'clients/js/create_client_view'}));
		// 	})
		// },
		initialize: function(){
			this.removePhones = [];
			this.removeEmails = [];
		},
		onRender: function(){
			var _view = this;
			this.$el.on('click', 'input[type="radio"]', function(e) {
				var _this = this;
				$(this.parentElement.parentElement.parentElement).find('input[type="radio"]').val('')
				$(this.parentElement.parentElement.getElementsByClassName('form-control')[0]).change(function(event) {
					_this.value = _this.parentElement.parentElement.getElementsByClassName('form-control')[0].value;
				});
				this.value = _this.parentElement.parentElement.getElementsByClassName('form-control')[0].value;	
			});
		}
	});
	return ContactPersonView
})
