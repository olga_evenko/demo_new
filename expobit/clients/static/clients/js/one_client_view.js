define(['text!clients/html/clients_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var ItemClientView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_client_view')[0].outerHTML,
		events: {
			'click #ClientView': 'openClientView',
			'click #HistoryContactsView': 'openHistoryContactsView',
			'click #Contract': 'openContract',
			'click #OneCBill': 'openOneCBill'
		},
		regions: {
			navMenu: "#navMenu",
			MenuContent: "#MenuContent"
		},
		// className: 'displayNone',
		initialize: function(){
		},
		onRender: function(){
			$('.loading').fadeOut();
			var _this = this;
			// this.$el.find(document).ready(function($) {
			// 	_this.$el.fadeIn(1000);
			// });
			this.menu = this.$el.find('li').toArray();
			require(['clients/js/tabClient'], function(tabClientView) {
				_this.showChildView('MenuContent', new tabClientView({model:_this.model}))
			});
		},
		openOneCBill: function(e){
			var _this = this;
			this.changeMenuActive(e);
			require(['clients/js/tab_one_c_bill_view'], function(View) {
				_this.showChildView('MenuContent', new View({model:_this.model}))
			});
		},
		openContract: function(e){
			var _this = this;
			this.changeMenuActive(e);
			require(['clients/js/applications/applicationInClientView'], function(tabClientView) {
				_this.showChildView('MenuContent', new tabClientView({model:_this.model}))
			});
		},
		openClientView: function(e){
			var _this = this;
			this.changeMenuActive(e);
			require(['clients/js/tabClient'], function(tabClientView) {
				_this.showChildView('MenuContent', new tabClientView({model:_this.model}))
			});
		},
		changeMenuActive: function(e){
			for(i in this.menu){
				$(this.menu[i]).removeClass('active');
			};
			$(e.currentTarget).addClass('active');
		},
		openHistoryContactsView: function(e){
			var _this = this;
			this.changeMenuActive(e);
			require(['clients/js/tabHistoryContactView'], function(tabHistoryContactView) {
				_this.showChildView('MenuContent', new tabHistoryContactView({model:_this.model}))
			});
		}

	});
	return ItemClientView
})
