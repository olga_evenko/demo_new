define(['text!clients/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal'], function(html, utils){

	var TabIndividualView = Backbone.Marionette.View.extend({
		template:$(html).filter('#tabIndividual')[0].outerHTML,
		className: 'displayNone',
		regions:{
			
		},
		events:{
			'click .edit': 'editIndividual',
		},
		initialize: function(){
		},
		templateContext: function(){
			var sex = {
				'm': 'Мужской',
				'w': 'Женский'
			};
			var parse_date = utils.dateFormatter(this.model.get('date_of_birth'));
			return {view_sex: sex[this.model.get('sex')], view_date: parse_date};
		},
		editIndividual: function(e){
			var id = $(e.currentTarget).data().id;
			router.navigate('individual/'+id+'/edit', {trigger: true});
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn();
			});
			// this.tableProject = this.$el.find('#tableProject').bootstrapTable({
			// 	idField: 'id',
			// 	uniqueId: 'id',
			// 	classes: 'table table-hover table-bordered',
			// 	data: this.model.get('projects'),
			// 	columns: [
			// 	{
			// 		field: 'project_type',
			// 		title: 'Вид проекта'
			// 	},
			// 	{
			// 		field: 'project.name',
			// 		title: 'Проект'
			// 	},
			// 	{
			// 		field: 'group',
			// 		title: 'Группа',
			// 		editable: {
			// 			source: [{text: 'A', value: 'a'},{text: 'B', value: 'b'},
			// 			{text: 'C', value: 'c'},{text: 'VIP', value: 'vip'}],
			// 			ajaxOptions: {
			// 				type: 'PUT',
			// 				dataType: 'json', 
			// 			    beforeSend: function(jqXHR,settings){
			// 				}
			// 			},
			// 			inputclass: 'input-sm',
			// 			type: 'select',
			// 			mode: 'inline',
			// 			emptytext: 'Назначить',
			// 			url: '/apiclients/clients_group/set_group/',
			// 			params: function(params){
			// 				return params
			// 			},
			// 			success: function(response){
			// 				_this.$el.find('#tableProject').bootstrapTable('refresh', {silent: true, 
			// 					url: '/apiclients/project_table/?client='+_this.model.id});
			// 			}
			// 		}
			// 	},
			// 	{
			// 		title: 'Менеджер',
			// 		field: 'manager.name'
			// 	}
			// 	]
			// });
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				data: this.model?this.model.get('entity_private'):[],
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// rowStyle: function(){}
				// url: '/apiclients/individuals/',
				columns: [
				{
					field: 'full_name',
					title: 'Юр. лицо',
					sortable: true,
				},
				{
					field: 'type_position',
					title: 'Тип должности',
					sortable: true,
				},
				{
					field: 'position',
					title: 'Должность',
					sortable: true,
				},
				{
					field: 'activities',
					title: 'Виды деятельности',
					sortable: false
				},

				]
			});
			
		}
	});
return TabIndividualView
})
