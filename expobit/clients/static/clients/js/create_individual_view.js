define(['text!clients/html/main_view.html', 'utils', 'selectize', 'bootstrap-table-editable', 'bootstrap-table-locale', 'autocomplete'], function(html, utils){

	var CreateIndividualView = Backbone.Marionette.View.extend({
		template:$(html).filter('#create_individual')[0].outerHTML,
		events: {
			'click #SaveButton': 'save',
			'click #addEntityPrivate': 'addEntityPrivate',
			'click .plus': 'addActivities',
			'click .rmv': 'removeEntityPrivate',
			'click .pencil': 'editEntity',
			'click #CancelButton': 'cancel',
			'click #SaveAndReturnButton': 'SaveAndReturn',
			'click #addSubSection': 'addSubSection',
			'click .delete_contact': 'destroyInput',
			'click #ExitEditButton': 'ExitEditButton',
			'click #create_client': 'createClient'
		},
		className: 'displayNone',
		removePhones: [],
		removeEmails: [],
		createClient: function(e){
			if(!this.model){
				return;
			}
			$.ajax({
				type: 'POST',
				url: '/apiclients/individuals/create_client/',
				contentType: 'application/json',
				data: JSON.stringify({id: this.model.id}),
				success: function(response){
					if(response.exists){
						utils.notyAlert('Клиент существует').show();
						return;	
					}
					utils.notySuccess('Клиент создан').show();
					utils.notySuccess('Переход в юр. лицо').show();
					router.navigate('entity/'+response.entity, {trigger: true});
				},
				error: function(response){
					utils.notyError().show();
				}
			});
		},
		destroyInput: function(e){
			var input = $(e.currentTarget).parent().parent();
			if(input.find('input[type="text"]').attr('name')=='phone_'+input[0].id && input[0].id != ''){
				this.removePhones.push(input[0].id);
			};
			if(input.find('input[type="text"]').attr('name')=='email_'+input[0].id && input[0].id != ''){
				this.removeEmails.push(input[0].id);
			};
			input.remove();
		},
		ExitEditButton: function(e){
			if(this.model){
				router.navigate('individual/'+this.model.id, {trigger: true});
			}
		},
		cancel: function(e){
			var _this = this;
			if(this.options.redirect){
				history.back();
				// require([_this.options.redirect], function(CreateEntityView){
				// 	_this.options.entity_model.fetch({
				// 		success: function(response){
				// 			window.mainView.showChildView('MainContent', new CreateEntityView({model:_this.options.entity_model}));
				// 		}
				// 	});
				// })
			}
			else{
				history.back();
				// mainView.GetIndividualsView();
			};
		},
		addSubSection: function(e){
			var type = $(e.currentTarget).data().type;
			var valid_class = '';
			if(type == 'phones'){
				valid_class = 'valid_phone ';
			}
			this.$el.find('#subsection_'+$(e.currentTarget).data().type).append('<div class="input-group inputWithButton">\
				<span class="input-group-addon input-sm"><input type="radio" name="main_'+$(e.currentTarget).data().type+'"></span>\
				<input type="text" class="'+valid_class+'form-control input-sm" name="'+
				$(e.currentTarget).data().type+
				'" data-validate="True">\
				<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-trash trash delete_contact"></span></span></div>');
		},
		templateContext: function(){
			if(this.options.entity_model){
				return {other_model: true};
			};
		},
		check: false,
		validate: function(){
			var valid = true;
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
				if($(inputs[i]).attr('name').match('email')){
					if(!re.test($(inputs[i]).val())){
						$(inputs[i]).parent().addClass('has-error');
						valid = false;
					};
				};
			};
			var radio_count_phones = this.$el.find('input[type="radio"][name="main_phones"]').length;
			var radio_checked_count_phones = this.$el.find( 'input:checked[name="main_phones"]' ).length;
			if(radio_count_phones>0 && radio_checked_count_phones!=1){
				utils.notyAlert('Выберете основной телефон').show();
				valid = false;
			};
			var radio_count_emails = this.$el.find('input[type="radio"][name="main_emails"]').length;
			var radio_checked_count_emails = this.$el.find( 'input:checked[name="main_emails"]' ).length;
			if(radio_count_emails>0 && radio_checked_count_emails!=1){
				utils.notyAlert('Выберете основной email').show();
				valid = false;
			};
			return valid;
		},
		SaveAndReturn: function(e){
			this.save(e, true);
		},
		removeEntityPrivate: function(e){
			var _this = this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить Юр. лицо?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$('.loading').fadeIn();
				$.ajax({
					type: 'DELETE',
					url: '/apiclients/individual_member/'+$(e.currentTarget).data().id+'/',
					success: function(response){
						_this.Table.bootstrapTable('removeByUniqueId', $(e.currentTarget).data().id);
						$('.loading').fadeOut();
					}
				});
			});
		},
		editEntity: function(e){
			var model = new Backbone.Model(this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id));
			model.url = '/apiclients/individual_member/'+model.id+'/';
			this.addEntityPrivate(e, model); 
		},
		addActivities: function(e){
			var activities = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id).entity.activities;
			for(i in activities){
				this.SelectActivities[0].selectize.addItem(activities[i].id);
			}
		},
		addEntityPrivate: function(e, model, entity_model, redirect){
			var _this = this;
			if(e==undefined || !$(e.currentTarget).hasClass('disabled')){
				require(['clients/js/addEntitysPrivateView', 'bootstrap-modal'], function(EntitysPrivateView){
				var modal = new Backbone.BootstrapModal({ 
					content: new EntitysPrivateView({model:model, entity_model:entity_model}),
					title: 'Выбрать юридическое лицо',
					okText: 'Сохранить',
					cancelText: 'Отмена',
					okCloses: false,
					animate: true, 
				}).open();
				var ModalView = modal.options.content;
				modal.on('ok', function(){
					if(ModalView.validate()){
						ModalView.EntityPrivate.type_position = ModalView.$el.find('input[name="type_position"]').val();
						ModalView.EntityPrivate.position = ModalView.$el.find('input[name="position"]').val();
						$('.loading').fadeIn();
						var data = new FormData(ModalView.$el.find('form')[0]);
						data.append('individual', _this.model.id);
						data.append('main', ModalView.$el.find('input[name="main"]')[0].checked);
						$.ajax({
							type: this.options.content.model ? 'PATCH' : 'POST',
							url: this.options.content.model ? this.options.content.model.url : '/apiclients/individual_member/',
							data: data,
							contentType: false,
							processData: false,
							success: function(response){
								response.activities = ModalView.EntityPrivate.activities_text;
								response.full_name = ModalView.EntityPrivate.full_name;
								if(model){
									_this.Table.bootstrapTable('refresh', {url: '/apiclients/individual_member/?individual='+_this.model.id})
									// _this.Table.bootstrapTable('updateByUniqueId',{
									// 	id: ModalView.model.id,
									// 	row:response 
									// });
								}
								else{
									_this.Table.bootstrapTable('append', response);
								}
								$('.loading').fadeOut();
								utils.notySuccess().show();
								_this.check = true;
								if(entity_model && redirect){
									history.back();
									// entity_model.fetch({
									// 	success: function(response){
									// 		require([_this.options.redirect], function(CreateEntityView){
									// 			window.mainView.showChildView('MainContent', new CreateEntityView({model:entity_model}));
									// 		})	
									// 	},
									// });
								};
							},
							error: function(response){
								$('.loading').fadeOut();
								utils.notyError().show();
							}
						});
						this.close();
					};
					
				});
			});
			};			
		},
		save: function(e, redirect){
			var _view = this;
			if(this.validate()){
				// this.$el.find('#addEntityPrivate').removeClass('disabled');
				// this.$el.find('#addEntityPrivate').tooltip('destroy');
				var _this = this;
				$('.loading').fadeIn();
				var type = 'POST';
				var url = '/apiclients/individuals/'
				if(this.model){
					type = 'PATCH';
					url = this.model.url;
				};
				var data = new FormData(this.$el.find('form')[0]);
				for(i in this.removePhones){
					data.append('removePhones', this.removePhones[i]);
				};
				for(i in this.removeEmails){
					data.append('removeEmails', this.removeEmails[i]);
				};
				$.ajax({
					url: url,
					type: type,
					data: data,
					contentType: false,
					processData: false,
					success: function(response){
						$('.loading').fadeOut();
						_this.removePhones = [];
						_this.removeEmails = [];
						utils.notySuccess().show();
						_this.$el.find('#addEntityPrivate').removeClass('disabled');
						_this.$el.find('#addEntityPrivate').tooltip('destroy');
						var arr_phone_forms = _view.$el.find('#subsection_phones input[type="text"]').toArray();
						var arr_email_forms = _view.$el.find('#subsection_emails input[type="text"]').toArray();
						for(var i in arr_phone_forms){
							arr_phone_forms[i].name = 'phone_' + response.phones[i].id
						}
						for(var i in arr_email_forms){
							arr_email_forms[i].name = 'email_' + response.emails[i].id
						}

						if(_this.model){
							_this.model.set(response);
						}
						else{
							router.navigate('individual/'+response.id+'/edit', {replace: true});
							_this.model = new Backbone.Model(response);
							_this.model.url = this.url+response.id+'/';
							_this.firstSave = true;
							_this.Table.bootstrapTable('refresh', {url: '/apiclients/individual_member/?individual='+_this.model.id});
						};
						if(_this.options.entity_model && !_this.check){
							_this.addEntityPrivate(undefined, undefined, _this.options.entity_model, redirect);
						};
						if(_this.check && redirect){
							history.back();
							// _this.options.entity_model.fetch({
							// 	success: function(response){
							// 		require([_this.options.redirect], function(CreateEntityView){
							// 			window.mainView.showChildView('MainContent', new CreateEntityView({model:_this.options.entity_model}));
							// 		});	
							// 	},
							// });
						}
					},
					error: function(response){
						$('.loading').fadeOut();
						if(response.responseJSON || response.responseJSON.error_name){
							if(type == 'POST'){
								utils.notyError(response.responseJSON.error_name).show();
								_this.model = new Backbone.Model();
								_this.model.url = this.url + response.responseJSON.id + '/';
								_this.model.fetch();
								router.navigate('individual/'+response.responseJSON.id, {replace: true});
								$('.loading').fadeOut();
								_this.$el.find('#addEntityPrivate').removeClass('disabled');
								_this.$el.find('#addEntityPrivate').tooltip('destroy');
								return;
							}
							if(type == 'PATCH'){
								utils.notyError(response.responseJSON.error_name).show();
								return;
							}
						}
						utils.notyError().show();
					}
				});
			};
		},
		initialize: function(){
			if(this.model) {
				this.firstSave = true;
			}
			else {
				this.firstSave = false;
			};
			if(this.options.entity_model){
				router.navigate('individual/create');
			};
		},
		onRender: function(){
			$('.loading').fadeOut();
			var entity_private = [];
			var entityprivate;
			if(!this.model){
				this.$el.find('#addEntityPrivate').addClass('disabled');
			}
			this.$el.on('click', 'input[type="radio"]', function(e) {
				var _this = this;
				$(this.parentElement.parentElement.parentElement).find('input[type="radio"]').val('')
				$(this.parentElement.parentElement.getElementsByClassName('form-control')[0]).change(function(event) {
					_this.value = _this.parentElement.parentElement.getElementsByClassName('form-control')[0].value;
				});
				this.value = _this.parentElement.parentElement.getElementsByClassName('form-control')[0].value;	
			});
			// else{
			// 	entity_private = this.model.get('entity_private');
			// 	entityprivate = this.model.get('entityprivate');
			// 	for(i in entity_private){
			// 		entity_private[i].activities_text = '';
			// 		for(j in entityprivate[i].activities){
			// 			if(j==entityprivate[i].activities.length-1){
			// 				entity_private[i].activities_text += entityprivate[i].activities[j].subcategories;
			// 			}
			// 			else{
			// 				entity_private[i].activities_text += entityprivate[i].activities[j].subcategories+', ';
			// 			};
			// 		};
			// 	};
			// 	this.model.set('entity_private', entity_private);
			// }
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
				if(!_view.model){
					_view.$el.find('#addEntityPrivate').tooltip({title: 'Сохраните для добавления'});
				};
			});
			this.$el.find('#SelectSex').selectize({
				options:[{text:'Мужской', value: 'm'},{text:'Женский', value: 'w'}],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('sex'));
					}
				}
			});
			this.autocomplite = this.$el.find('input[name="address"]').autocomplete({
				serviceUrl: '/apiclients/searchaddress/',
				transformResult: function(response) {
					var result = JSON.parse(response);
			        return {
			            suggestions: $.map(result.predictions, function(dataItem) {
			                return { value: dataItem.description, data: dataItem.description};
			            })
			        };
			    }
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// data: this.model?this.model.get('entity_private'):[],
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// rowStyle: function(){}
				url: '',
				columns: [
				{
					field: 'full_name',
					title: 'Юр. лицо',
					sortable: true,
					cellStyle: function(){
						return {
							classes: '',
							css: {
								cursor: 'pointer'
							}
						};
					}
				},
				{
					field: 'type_position',
					title: 'Тип должности',
					sortable: true,
				},
				{
					field: 'position',
					title: 'Должность',
					sortable: true,
				},
				{
					field: 'activities',
					title: 'Виды деятельности',
					sortable: false
				},
				{
					field: 'main',
					title: 'Осн. орг.',
					formatter: function(value, row, index){
						if(value){
							return 'Да';
						}
						return 'Нет';
					}
				},
				{
					field: '',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash rmv" data-id ='+
						row.id+'></span></span></div>'
					},
				}

				],
				onClickRow: function(row, element, index){
					if(index == ''){
						return;
					}
					router.navigate('entity/'+row.entity_id, {trigger: true});
				}
			});
			if(this.model){
				this.Table.bootstrapTable('refresh', {url: '/apiclients/individual_member/?individual='+this.model.id});
			}
		}
	});
return CreateIndividualView
})
