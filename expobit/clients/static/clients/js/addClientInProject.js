define(['text!clients/html/clients_view.html', 'utils', 'selectize', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var AddClientInProjectView = Backbone.Marionette.View.extend({
		template:$(html).filter('#addProjectView')[0].outerHTML,
		events: {
			// 'click #SaveButton': 'save'
		},
		initialize: function(){
		},
		validate: function(){
			// var valid = true;
			// var inputs = this.$el.find('[data-validate="True"]').toArray();
			// for(i in inputs){
			// 	if($(inputs[i]).parent().hasClass('has-error')){
			// 		$(inputs[i]).parent().removeClass('has-error')
			// 	};
			// 	if($(inputs[i]).val()==""){
			// 		$(inputs[i]).parent().addClass('has-error');
			// 		valid = false;
			// 	};
			// };
			// return valid;
		},
		onRender: function(){
			var _view = this;
			this.selectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				onInitialize: function(){
					var _this = this;
					var exclude_projects = [];
					var projects = _view.options.exclude_projects; 
					for(i in projects){
						exclude_projects.push(projects[i].project.id);
					}
					var url = '/apiprojects/projects/?data=min&section_activities='+_view.options.activities.join(',');
					if(user.groups.indexOf('managers')!=-1){
						url = url + '&manager=' + user.manager_id;
					}
					$.get({
						url: url,
						success: function(response){
							for(i in response){
								if(exclude_projects.indexOf(response[i].id)==-1){
									_this.addOption(response[i]);
								}
							};
						}
					});
				},
				
			});
			this.selectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselect/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							};
						}
					});
				},
				
			});
		}
	});
	return AddClientInProjectView
})
