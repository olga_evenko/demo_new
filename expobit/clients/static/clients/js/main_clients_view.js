define(['text!clients/html/clients_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_client')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .eye': 'openClient',
			'click .trash': 'removeClient',
			'click #export': 'export'
		},
		className: 'displayNone',
		removeClient: function(e){
			var id = $(e.currentTarget).data().id;
			var _view = this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить клиента?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$.ajax({
					type: 'DELETE',
					url: '/apiclients/clients/'+id+'/',
					success: function(response){
						utils.notySuccess('Удалено').show();
						_view.Table.bootstrapTable('refresh');
					},
					error: function(){
						utils.notyError().show();
					}
				});
			});
		},
		export: function(e){
			this.SelectProject[0].selectize.getValue()
			$.ajax({
				url: '/report/model_to_xls',
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify({
					model_name: 'client',
					app_label: 'clients',
					fields_list: ['id', 'name', 'address'],
					filters: this.query,
				}),
				success: function(response){
					window.open('/report/model_to_xls/?guid='+response.guid);
				},
				error: function(response){
					utils.notyError().show();
				}
			});
		},
		openClient: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			// router.navigate('client/'+row.id, {trigger: true});
			// var model = new Backbone.Model();
			// model.url = '/apiclients/clients/'+row.id+'/';
			// $('.loading').fadeIn();
			// model.fetch({
			// 	success: function(){
			// 		require(['clients/js/one_client_view'], function(OneClientView){
			// 			window.mainView.showChildView('MainContent', new OneClientView({model:model}));
			// 		})	
			// 	}
			// });
		},
		addIndividual: function(){
			router.navigate('client/create', {trigger: true});
			// require(['clients/js/create_client_view'], function(CreateClientView){
			// 	window.mainView.showChildView('MainContent', new CreateClientView({}));
			// })

		},
		editIndividual: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('client/'+row.id+'/edit', {trigger: true});
			// var model = new Backbone.Model();
			// model.url = '/apiclients/clients/'+row.id+'/';
			// $('.loading').fadeIn();
			// model.fetch({
			// 	success: function(){
			// 		require(['clients/js/create_client_view'], function(CreateClientView){
			// 			window.mainView.showChildView('MainContent', new CreateClientView({model:model}));
			// 		})	
			// 	}
			// });
		},
		initialize: function(){
			this.query = {};
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				toolbar: $(html).filter('#toolbar_export')[0].innerHTML,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/clients/',
				onPageChange: function(num, size){
					router.navigate('clients/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('client/'+row.id, {trigger: true});
						// var model = new Backbone.Model();
						// model.url = '/apiclients/clients/'+row.id+'/';
						// $('.loading').fadeIn();
						// model.fetch({
						// 	success: function(){
						// 		require(['clients/js/one_client_view'], function(OneClientView){
						// 			window.mainView.showChildView('MainContent', new OneClientView({model:model}));
						// 		})	
						// 	}
						// });
					}
				},
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				columns: [
				// {
				// 	checkbox: true
				// },
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'date_created',
					title: 'Дата создания',
					sortable: true,
					// formatter: function(value, row, index){
					// 	return utils.dateFormatter(value);
					// }
				},
				{
					field: 'date_updated',
					title: 'Дата изменения',
					sortable: true,
					// formatter: function(value, row, index){
					// 	return utils.dateFormatter(value);
					// }
				},
				{
					field: 'created_user_name',
					title: 'Создал',
					sortable: true,
				},
				{
					field: 'edited_user_name',
					title: 'Изменил',
					sortable: true,
				},
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
						row.id+'></span></div>';
					},
					width: '89px'
				}],
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, _view.query);
					}
					return Object.assign(params, _view.query);
				},
				// queryParams: function(params){
				// 	if(params.order == 'desc'){
				// 		params.sort = '-' + params.sort;
				// 		return params
				// 	}
				// 	return params
				// },
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
			this.SelectAct = this.$el.find('#SelectAct').selectize({
				valueField: 'id',
				labelField: 'subcategories',
				searchField: ['subcategories', 'value'],
				optgroupField: 'category',
				optgroupLabelField: 'display_name',
				onInitialize: function(){
					var _this = this;
					$.ajax({
						url: '/apiprojects/activities/',
						type: 'OPTIONS',
						cache: true,
						success: function(response){
							var optGr = response.actions.POST.category.choices;
							for (i in optGr){
								_this.addOptionGroup(optGr[i].value, optGr[i]);
							}
							$.get({
								url: '/apiprojects/activities/',
								async: true,
								success: function(response) {
									for(var i=0; i<response.length; i++){
										_this.addOption(response[i]);
									}
									return response;
								}
							});
						}
					})
				},
				onChange: function(value){
					// if(value != ''){
						_view.query = {
							membership__project: _view.SelectProject[0].selectize.getValue(),
							client_activitys__id__in: _view.SelectActivities[0].selectize.getValue(),
							entitys__activities__id__in: value
						};
						_view.Table.bootstrapTable('refresh', {url: '/apiclients/clients/'});
						if(_view.SelectProject[0].selectize.getValue() != ''){
							_view.Table.bootstrapTable('refresh', {url: '/apiclients/clients/'});
						}		
					// }
				},
			});
			this.SelectActivities = this.$el.find('#SelectActivities').selectize({
				valueField: 'id',
				labelField: 'activity',
				onInitialize: function(){
					var _this = this;
					
					$.get({
						url: '/apiclients/client_activity/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					if(value != ''){
						_view.query = {
							membership__project: _view.SelectProject[0].selectize.getValue(),
							client_activitys__id__in: value,
							entitys__activities__id__in: _view.SelectAct[0].selectize.getValue(),
						};
						_view.Table.bootstrapTable('refresh', {url: '/apiclients/clients/'});
						if(_view.SelectProject[0].selectize.getValue() != ''){
							_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
						}		
					}

				},
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					if(value == '' && _view.SelectVisitors[0].selectize.getValue() != ''){
						return;
					}
					_view.SelectVisitors[0].selectize.clear();
					_view.SelectAct[0].selectize.clearOptions();
					_view.SelectAct[0].selectize.clearOptionGroups();
					$.get({
						url: '/apiprojects/activities/?project_clients='+value,
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_view.SelectAct[0].selectize.addOption(response[i]);
							}
							return response;
						}
					});
					
					_view.SelectActivities[0].selectize.enable();
					_view.query = {
						membership__project__in: value,
						entitys__activities__id__in: _view.SelectAct[0].selectize.getValue(),
						client_activitys__id__in: _view.SelectActivities[0].selectize.getValue(),
					};	
					_view.Table.bootstrapTable('refresh', {url: '/apiclients/clients/'});
				},
			});
			this.SelectVisitors = this.$el.find('#SelectPriv').selectize({
				labelField: 'project_name',
				valueField: 'project',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apivisitors/attr_visit/?data=min',
						success: function(response){
							for (var i in response) {
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					if(value == ''){
						return;
					}
					_view.SelectProject[0].selectize.clear();
					_view.SelectAct[0].selectize.clear();
					// _view.SelectVisitors[0].selectize.clear();
					_view.SelectActivities[0].selectize.clear();
					_view.query = {};
					_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients_type_2/?' 
						+ this.options[value].client_filter});
				}
			});
		}
	});
	return ClientsView
})
