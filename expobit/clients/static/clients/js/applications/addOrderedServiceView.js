define(['text!clients/html/applications.html', 'selectize', 'spinner'], function(html){

	var AddOrderedServiceView = Backbone.Marionette.View.extend({
		template:$(html).filter('#addOrderedServiceView')[0].outerHTML,
		events: {
			'input input[name="count"]': 'changeInput',
			'input input[name="count_days"]': 'changeInputDays',
			'input input[name="price"]': 'changePrice',
			'input input[name="discount_rate"]': 'changeInputDiscount',
			'input input[name="markup"]': 'changeInputMarkup',
		},
		initialize: function(){
		},
		templateContext: function(){
			return{barter_app: this.options.barter, kvs: this.options.type=='lease_kvs'};
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		changePrice: function(e){
			if(this.options.barter){
				var summ = this.$el.find('input[name="count"]').val()*$(e.currentTarget).val(); 
				this.$el.find('#allSumm').text('Итого: ' + summ);
			}
		},
		changeInput: function(e){
			if(this.options.barter){
				var summ = this.$el.find('input[name="price"]').val()*$(e.currentTarget).val()*this.$el.find('input[name="count_days"]').val(); 
				this.$el.find('#allSumm').text('Итого: ' + summ);
				return;
			}
			var summ = this.SelectOreders[0].selectize.options[this.SelectOreders[0].selectize.getValue()]['Цена']*$(e.currentTarget).val()*this.$el.find('input[name="count_days"]').val(); 
			this.$el.find('#allSumm').text('Итого: ' + summ);
		},
		changeInputDays: function(e){
			if(this.options.barter){
				var summ = this.$el.find('input[name="price"]').val()*$(e.currentTarget).val()*this.$el.find('input[name="count"]').val(); 
				this.$el.find('#allSumm').text('Итого: ' + summ);
				return;
			}
			var summ = this.SelectOreders[0].selectize.options[this.SelectOreders[0].selectize.getValue()]['Цена']*$(e.currentTarget).val()*this.$el.find('input[name="count"]').val(); 
			this.$el.find('#allSumm').text('Итого: ' + summ);
		},
		changeInputMarkup: function(e){
			if($(e.currentTarget).val() != ''){
				var summ = this.SelectOreders[0].selectize.options[this.SelectOreders[0].selectize.getValue()]['Цена']*this.$el.find('input[name="count"]').val();
				this.$el.find('#allSumm').text('Итого: ' + (summ*((100+parseInt($(e.currentTarget).val()))/100)*((100-this.$el.find('input[name="discount_rate"]').val())/100)).toFixed());
			}
		},
		changeInputDiscount: function(e){
			var summ = this.SelectOreders[0].selectize.options[this.SelectOreders[0].selectize.getValue()]['Цена']*this.$el.find('input[name="count"]').val();
			// var percent = parseInt(this.$el.find('#allSumm').text().match(/\d+/)[0]); 
			this.$el.find('#allSumm').text('Итого: ' + summ*((100-$(e.currentTarget).val())/100)*((100 + +this.$el.find('input[name="markup"]').val())/100));
		},
		onRender: function(){
			var _view = this;
			// this.$el.find('input[name="count"]').on('change', function(e){
			// 	var summ = _view.SelectOreders[0].selectize.options[_view.SelectOreders[0].selectize.getValue()].price*$(e).val(); 
			// 	_view.$el.find('#allSumm').text('Итого: '+_view.SelectOreders[0].selectize.getValue());
			// });
			if(this.options.barter){
				this.$el.find('#amount_pay').css('display', 'block');
			}
			this.NomenclatureParents = {};
			this.guidParents = {};
			var max_discount = 100;
			if(this.options.max_discount){
				max_discount = this.options.max_discount;
			}
			this.$el.find('#discount_label').text('Скидка % (доступно '+max_discount+')');
			this.$el.find('#spinner').spinner({ min: 0, max: max_discount, step: 1, precision:0}).spinner('changing', function(e, newVal, oldVal) {
			    var summ = _view.SelectOreders[0].selectize.options[_view.SelectOreders[0].selectize.getValue()]['Цена']*_view.$el.find('input[name="count"]').val();
				_view.$el.find('#allSumm').text('Итого: ' + summ*((100-newVal)/100)*((100 + +_view.$el.find('input[name="markup"]').val())/100));
			});
			this.$el.find('#spinner_markup').spinner({ min: 0, max: 100, step: 1, precision:0}).spinner('changing', function(e, newVal, oldVal) {
			    var summ = _view.SelectOreders[0].selectize.options[_view.SelectOreders[0].selectize.getValue()]['Цена']*_view.$el.find('input[name="count"]').val();
				_view.$el.find('#allSumm').text('Итого: ' + (summ*((100 + +newVal)/100)*((100-_view.$el.find('input[name="discount_rate"]').val())/100)).toFixed());
			});
			this.SelectNomenclatureGroup = this.$el.find('#SelectNomenclatureGroup').selectize({
				onChange: function(value){
					_view.SelectOreders[0].selectize.clearOptions();
					var sel = _view.NomenclatureParents[value];
					for(var i in sel){
						_view.SelectOreders[0].selectize.addOption(sel[i]);
					}
					if(_view.model){
						_view.SelectOreders[0].selectize.setValue(_view.model.get('nomenclature_guid'));
					}
				},
			});
			this.SelectOreders = this.$el.find('#SelectNomenclature').selectize({
				valueField: 'Guid',
				labelField: 'Номенклатура',
				searchField: 'Номенклатура',
				onInitialize: function(){
					var _this = this;
					var url = '/one_c/get_nomenclature/?date='+_view.options.date_app+'&project='+_view.options.project+'&barter='+_view.options.barter;
					if(_view.options.type == 'ad'){
						url = '/one_c/get_nomenclature/?date='+_view.options.date_app+'&type=ad&project='+_view.options.project+'&barter='+_view.options.barter
					}
					if(_view.options.type == 'lease_kvs'){
						url = '/one_c/get_nomenclature/?date='+_view.options.date_app+'&type=kvs&project='+_view.options.project+'&barter='+_view.options.barter
					}
					$.get({
						url: url,
						success: function(response){
							for(i in response){
								_view.guidParents[response[i].Guid] = response[i]['Родитель'];
								if(!_view.NomenclatureParents[response[i]['Родитель']]){
									_view.NomenclatureParents[response[i]['Родитель']] = [response[i]];
									_view.SelectNomenclatureGroup[0].selectize.addOption({
										text: response[i]['Родитель'],
										value: response[i]['Родитель']
									});
								}
								else{
									_view.NomenclatureParents[response[i]['Родитель']].push(response[i]);
								}
								// _this.addOption(response[i]);
							}
							if(_view.model){
								_view.SelectNomenclatureGroup[0].selectize.setValue(_view.guidParents[_view.model.get('nomenclature_guid')]);
								// _this.setValue(_view.model.get('nomenclature_guid'));
							}
						}
					});
				},
				render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeTemplate')[0].innerHTML)(item);
					}
				},
				onChange: function(value){
					if(this.options[value]['Площадь']){
						_view.$el.find('#discount').collapse('show');
					}
					else{
						_view.$el.find('#discount').collapse('hide');	
					}
					if(this.options[value]['СвободнаяЦена']){
						_view.$el.find('#amount_pay').css('display', 'block');
					}
					else{
						_view.$el.find('#amount_pay').css('display', 'none');
					}
					_view.$el.find('input[name="units"]').val(this.options[value]['ЕдИзм']);
					_view.$el.find('input[name="price"]').val(this.options[value]['Цена']);
					_view.$el.find('#allSumm').text('Итого: ' + this.options[value]['Цена']*_view.$el.find('input[name="count"]').val()*((100-_view.$el.find('input[name="discount_rate"]').val())/100)*((100 + +_view.$el.find('input[name="markup"]').val())/100));
				},
			});
		}
	});
	return AddOrderedServiceView
})
