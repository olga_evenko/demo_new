define(['text!clients/html/clients_view.html', 'utils', 'js/filters_view',
 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils, FilterView){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_client')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .eye': 'openClient',
			'click #filterButton': 'setFilter',
			'click #resetfilterButton': 'resetFilter',
			'click .trash': 'removeApp'
		},
		regions: {
			filter: '#filterModal'
		},
		defaultUrl: true,
		setFilter: function(){
			var _this = this;
			if(!viewscache.filters.apps){
				viewscache.filters.apps = new Backbone.BootstrapModal({
					content: new FilterView({
						filter_fields:[
							{
								date: {
									name: 'date',
									gr: true,
									ls: true,
									cn: true,
									label: 'Дата'
								}
							},
							{
								select: {
									name: 'status',
									label: 'Статус',
									url: '/apiclients/application_expo/',
									type_get: 'OPTIONS',
									labelField: 'display_name'
								}
							},
							{
								multiselect: {
									name: 'project',
									label: 'Проекты',
									url: '/apiprojects/projects/?data=min',
									type_get: 'GET',
									labelField: 'name',
									searchField: 'name'
								}
							},
							{
								select: {
									name: 'client',
									label: 'Клиент',
									url: '/apiclients/clients/?',
									type_get: 'GET',
									labelField: 'name',
									searchField: 'name',
									load: true,
									render:{
										option: function(item, escape){	
											return Handlebars.compile($(html).filter('#selectizeClientFilter')[0].innerHTML)(item);
										}
									}
								},
							},
						]
					}), 
					title: 'Фильтр',
					okText: 'Применить',
					cancelText: 'Сбросить',
					animate: true,
					okCloses: false
				}).open();
			viewscache.filters.apps.on('ok', function(){
				this.preventClose();
				_this.defaultUrl = false;
				$('#table').bootstrapTable('refresh', {
					silent: true, 
					query: this.options.content.getQuery(),
					pageNumber: 1
				});
				this.$el.modal('hide');
			});
			viewscache.filters.apps.preventClose();
			viewscache.filters.apps.on('cancel', function(e){
				this.preventClose();
				if(e){
					this.options.content.resetForm();
				}
				else{
					this.$el.modal('hide');
				}
			});
		}
		else{
			viewscache.filters.apps.$el.modal('show');
		};
		},
		resetFilter: function(){
			if(viewscache.filters.apps){
				viewscache.filters.apps.options.content.resetForm();
			};
			this.$el.find('#table').bootstrapTable('refresh', {
				silent: true,
			}); 
		},
		removeApp: function(e){
			var id = $(e.currentTarget).data().id;
			var _view = this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить заявку?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$.ajax({
					type: 'DELETE',
					url: '/apiclients/application_'+_view.options.app_url+'/'+id+'/',
					success: function(response){
						_view.Table.bootstrapTable('refresh');
						utils.notySuccess('Удалено').show();
					},
					error: function(){
						utils.notyError().show();	
					}
			});
			});
		},
		className: 'displayNone',
		openClient: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			var model = new Backbone.Model();
			model.url = '/apiclients/application_'+this.options.app_url+'/'+row.id+'/';
			$('.loading').fadeIn();
			model.fetch({
				success: function(){
					require(['clients/js/one_client_view'], function(OneClientView){
						window.mainView.showChildView('MainContent', new OneClientView({model:model}));
					})	
				}
			});
		},
		addIndividual: function(){
			// var _this = this;
			router.navigate('app/'+this.options.app_url+'/create',  {trigger: true});
			// require(['clients/js/applications/ApplicationExpoView'], function(ApplicationExpoView){
			// 	window.mainView.showChildView('MainContent', new ApplicationExpoView({app_url:_this.options.app_url}));
			// })

		},
		editIndividual: function(e){
			var _this = this;
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('app/'+this.options.app_url+'/'+row.id+'/edit',  {trigger: true});
			// var model = new Backbone.Model();
			// model.url = '/apiclients/application_'+this.options.app_url+'/'+row.id+'/';
			// $('.loading').fadeIn();
			// model.fetch({
			// 	success: function(){
			// 		require(['clients/js/applications/ApplicationExpoView'], function(ApplicationExpoView){
			// 			window.mainView.showChildView('MainContent', new ApplicationExpoView({model:model, app_url:_this.options.app_url}));
			// 		})	
			// 	}
			// });
		},
		initialize: function(){
			// var _this = this;
			// this.filterView = new FilterView({
			// 	filter_fields:[
			// 		{
			// 			date: {
			// 				name: 'date',
			// 				gr: true,
			// 				ls: true,
			// 				cn: true,
			// 				label: 'Дата'
			// 			}
			// 		},
			// 		{
			// 			select: {
			// 				name: 'status',
			// 				label: 'Статус',
			// 				url: '/apiclients/application_expo/',
			// 				type_get: 'OPTIONS',
			// 				labelField: 'display_name'
			// 			}
			// 		},
			// 		{
			// 			multiselect: {
			// 				name: 'project',
			// 				label: 'Проекты',
			// 				url: '/apiprojects/projects/?data=min',
			// 				type_get: 'GET',
			// 				labelField: 'name'
			// 			}
			// 		},
			// 		{
			// 			select: {
			// 				name: 'client',
			// 				label: 'Клиент',
			// 				url: '/apiclients/clients/?data=min',
			// 				type_get: 'GET',
			// 				labelField: 'name'
			// 			}
			// 		},
			// 	]
			// });
			// this.filterView.on('filtered', function(){
			// 	_this.defaultUrl = false;
			// 	_this.$el.find('#table').bootstrapTable('refresh', {
			// 		silent: true, 
			// 		query: _this.filterView.getQuery(),
			// 		pageNumber: 1
			// 	});
			// });
			// this.showChildView('filter', this.filterView);
		},
		onRender: function(){
		    this.$el.find('#FilterClient').remove()

			var col = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
				formatter: function(value, row, index){
                    if(value){
                        value = value.toString().replace(/ /g,'');
                        var number = value;
                        value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                    }
                    return value;
                },
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
					row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
					row.id+'></span></span></div>'
				}
			}
			];
			if(this.options.app_url=='lease_kvs'){
				col.splice(1,0, {
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
					formatter: function(value, row, index){
						return utils.dateFormatter(value);
					}
				},{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
					formatter: function(value, row, index){
						return utils.dateFormatter(value);
					}
				});
			};
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				toolbar: $(html).filter('#toolbar')[0].innerHTML,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_'+this.options.app_url+'/',
				newQuery: {},
				onRefresh: function(options){
					if(options && options.query){
						this.newQuery = options.query;	
					}
					else{
						this.newQuery = {};
					};
				},
				onPageChange: function(num, size){
					router.navigate('apps/'+_view.options.app_url+'/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				columns: col,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address'
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					};
					return Object.assign(params, viewscache.filters.apps?viewscache.filters.apps.options.content.getQuery():{});
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
		}
	});
	return ClientsView
})
