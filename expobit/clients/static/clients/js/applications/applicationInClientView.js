define(['text!clients/html/clients_view.html', 'utils', 'js/filters_view',
 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils, FilterView){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#client_applications_view')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .eye': 'openClient',
			'click #filterButton': 'setFilter',
			'click #resetfilterButton': 'resetFilter'
		},
		regions: {
			filter: '#filterModal'
		},
		defaultUrl: true,
		className: 'displayNone',
		editIndividual: function(e){
			var _this = this;
			var type = $(e.currentTarget).data().type;
			var table_mame = `Table${(type=='ad'? type: (type.split('_')[1]) ? type.split('_')[1] : '').toUpperCase()}`
			var row = this[table_mame].bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('app/'+type+'/'+row.id+'/edit',  {trigger: true});
		},
		initialize: function(){
		},
		onRender: function(){
			var col = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-type="expo" data-id ='+
					row.id+'></span></div>'
				}
			}
			];
			var col_ad = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-type="ad" data-id ='+
					row.id+'></span></div>'
				}
			}
			];
			var col_kvs = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'date_start',
				title: 'Дата начала',
				sortable: true,
				formatter: function(value, row, index){
					return utils.dateFormatter(value);
				}
			},{
				field: 'date_end',
				title: 'Дата окончания',
				sortable: true,
				formatter: function(value, row, index){
					return utils.dateFormatter(value);
				}
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-type="lease_kvs" data-id ='+
					row.id+'></span></div>'
				}
			},
			];
			
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_expo/?client='+this.model.id,
				columns: col,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address';
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					}
					return params;
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
			this.TableKVS = this.$el.find('#table_kvs').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_lease_kvs/?client='+this.model.id,
				columns: col_kvs,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address';
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					}
					return params;
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
			this.TableAD = this.$el.find('#table_ad').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_ad/?client='+this.model.id,
				columns: col_ad,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address'
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					}
					return params;
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
		}
	});
	return ClientsView;
});
