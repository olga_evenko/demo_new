define(['text!clients/html/applications.html', 'selectize', 'spinner'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#edit_pay')[0].outerHTML,
		events: {
		},
		initialize: function(){
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		onRender: function(){
			var _view = this;
			
		}
	});
	return View;
})
