define(['text!clients/html/applications.html', 'utils', 'selectize'], function(html, utils){

	var OrderOneCView = Backbone.Marionette.View.extend({
		template:$(html).filter('#order_one_c_view')[0].outerHTML,
		events:{
			'click #PrintingBill': 'PrintingBill',
			'click #PrintingAct': 'PrintingAct',
			'click #PrintingContract': 'PrintingContract',
			'click #PrintingBillXLS': 'PrintingBillXLS',
			'click #PrintingBillDOCX': 'PrintingBillDOCX',
			'click #PrintingActXLS': 'PrintingActXLS',
			'click #PrintingContractXLS': 'PrintingContractXLS',
			'click #PrintingContractDOCX': 'PrintingContractDOCX',
			'click #PrintingContractTraning': 'PrintingContractTraning',
			'click #PrintingContractXLSTraning': 'PrintingContractXLSTraning',
			'click #PrintingContractDOCXTraning': 'PrintingContractDOCXTraning',
			'click #PrintContractAdd': 'PrintContractAdd',
			//'click #PrintContractAdd': 'PrintContractAdd',
			'click #PrintContractAddDOC': 'PrintContractAddDOC',
			'click #getAct': 'getAct',
			'click .print_button': 'send_print'
		},
		initialize: function(){
		},
		templateContext: function(){
			var printing = false;
			var ad = false;
			if(this.options.type == 'ad'){
				ad = true;
			}
			if(this.options.print){
				printing = true;
			};
			return {print: printing, ad: ad, 
				kvs: this.options.type == 'lease_kvs', 
				sharm: this.options.subject_name == 'Шарм', 
				guid: this.options.print, 
				contracts: this.options.contracts};
		},
		send_print: function(e){
			var id = e.currentTarget.id;
			var format = $(e.currentTarget).data().format;
			window.open('/one_c/print/?formatfile='+format+'&type='+id+'&guid='+this.options.print);
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		PrintingContractDOCXTraning: function(e){
			window.open('/one_c/print/?type=contract_traning_docx&guid='+this.options.print);
		},
		PrintingContractXLSTraning: function(e){
			window.open('/one_c/print/?type=contract_traning_xls&guid='+this.options.print);
		},
		PrintingContractTraning: function(e){
			window.open('/one_c/print/?type=contract_traning&guid='+this.options.print);
		},
		PrintingBillDOCX: function(e){
			window.open('/one_c/print/?type=bill_docx&guid='+this.options.print);
		},
		PrintingContractDOCX: function(e){
			window.open('/one_c/print/?type=contract_docx&guid='+this.options.print);
		},
		PrintContractAddDOC: function(e){
			window.open('/one_c/print/?type=contract_add_docx&guid='+this.options.print);
		},
		PrintContractAdd: function(e){
			window.open('/one_c/print/?type=contract_add&guid='+this.options.print);
		},
		getAct: function(e){
			var _view = this;
			$.ajax({
				type: 'POST',
				url: '/one_c/get_act/?guid='+this.options.print,
				success: function(response){
					// _view.$el.find('#progress-indicator').fadeOut();
					utils.notySuccess('Реализация сформирована').show();
					_view.$el.find('#messageAct').css('color', 'green');
					_view.$el.find('#messageAct').text('Реализация сформирована');
					_view.$el.find('#PrintingAct').attr('disabled', false);
					_view.$el.find('#PrintingActXLS').attr('disabled', false);
					_view.$el.find('#getAct').attr('disabled', true);
				},
				error: function(response){
					// _view.$el.find('#progress-indicator').fadeOut();
					utils.notyError('Ошибка формирования реализации').show();
				}
			});
		},
		PrintingAct: function(e){
			// $('.loading').fadeIn();
			window.open('/one_c/print/?type=act&guid='+this.options.print);
		},
		PrintingContract: function(e){
			// $('.loading').fadeIn();
			window.open('/one_c/print/?type=contract&guid='+this.options.print);
		},
		PrintingBill: function(e){
			// $('.loading').fadeIn();
			window.open('/one_c/print/?type=bill&guid='+this.options.print);
		},
		PrintingActXLS: function(e){
			// $('.loading').fadeIn();
			window.open('/one_c/print/?type=act_xls&guid='+this.options.print);
		},
		PrintingContractXLS: function(e){
			// $('.loading').fadeIn();
			window.open('/one_c/print/?type=contract_xls&guid='+this.options.print);
		},
		PrintingBillXLS: function(e){
			// $('.loading').fadeIn();
			window.open('/one_c/print/?type=bill_xls&guid='+this.options.print);
		},
		onRender: function(){
			var _view = this;
			_view.$el.find('#progress-indicator').fadeIn();
			this.SelectOreders = this.$el.find('#SelectOrganization').selectize({
				valueField: 'guid',
				labelField: 'description',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/one_c/organizations',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							if(_view.options.organization){
								_this.setValue(_view.options.organization);
							}
						}
					});
					$.get({
						url: '/one_c/checking_act/?guid='+_view.options.print,
						success: function(response){
							_view.$el.find('#progress-indicator').fadeOut();
							if(response.act){
								_view.$el.find('#messageAct').css('color', 'green');
								_view.$el.find('#messageAct').text('Реализация сформирована');
								_view.$el.find('#getAct').attr('disabled', true);
								_view.$el.find('#PrintingAct').attr('disabled', false);
								_view.$el.find('#PrintingActXLS').attr('disabled', false);
							}
							else{
								_view.$el.find('#messageAct').css('color', 'red');
								_view.$el.find('#getAct').attr('disabled', false);
								_view.$el.find('#messageAct').text('Реализация не сформирована');
							}
							_view.$el.find('#blockPrintAct').collapse('show');
						},
						error: function(response){
							_view.$el.find('#progress-indicator').fadeOut();
							_view.$el.find('#messageAct').css('color', 'red');
							_view.$el.find('#messageAct').text('Ошибка получения данных об акте');
							_view.$el.find('#getAct').attr('disabled', true);
							_view.$el.find('#blockPrintAct').collapse('show');
						}
					});
				},
				onChange: function(value){
					var selectAuthOpt = _view.authorized_persons_cache;
					if(selectAuthOpt){
						_view.SelectAuthorizedPersons[0].selectize.clearOptions();
						for(var i in selectAuthOpt){
							if(selectAuthOpt[i].organization == value){
								_view.SelectAuthorizedPersons[0].selectize.addOption(selectAuthOpt[i]);
							}
						}
					}
				}
			});
			this.SelectAuthorizedPersons = this.$el.find('#SelectAuthorizedPersons').selectize({
				valueField: 'guid',
				labelField: 'description',
				searchField: 'description',
				onInitialize: function(){
					var _this=this;
					$.ajax({
						url: '/one_c/authorized_persons/',
						type: 'GET',
						cache: true,
						success: function(response){
							_view.authorized_persons_cache = response;
							for(var i in response){
								_this.addOption(response[i]);
							}
							if(_view.options.person){
								_this.setValue(_view.options.person);
							}
						}
					});
					if(_view.SelectOreders[0].selectize.getValue() != ''){
						var selectAuthOpt = _view.authorized_persons_cache;
						if(selectAuthOpt){
							_view.SelectAuthorizedPersons[0].selectize.clearOptions();
							for(var i in selectAuthOpt){
								if(selectAuthOpt[i].organization == value){
									_view.SelectAuthorizedPersons[0].selectize.addOption(selectAuthOpt[i]);
								}
							}
							_this.setValue(_view.options.person);
						}
					}
				},
				onItemRemove: function(value){
					_view.flag_change_activities = true;
				},
				onItemAdd: function(value){
					if(this.settings.flag){
						_view.flag_change_activities = true;
					};
				}
			});
		}
	});
	return OrderOneCView
})
