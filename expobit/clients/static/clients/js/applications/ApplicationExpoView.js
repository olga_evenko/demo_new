define(['text!clients/html/applications.html', 'utils', 'bootstrap-table-editable', 
	'bootstrap-table-locale', 'selectize', 'datetimepicker', 'colorpicker'], function(html, utils){
	var ApplicationExpoView = Backbone.Marionette.View.extend({
		className: 'displayNone',
		events:{
			'click #saveButton': 'save',
			'click #addOrderedService': 'addOrderedService',
			'click #saveRedirectButton': 'saveAndRedirect',
			'click #CancelButton': 'cancel',
			'click .trash': 'removeOrderedService',
			'click .pencil': 'editOrderedService',
			'click #getOrderOneC': 'getOrderOneC',
			'input textarea[name="activities_catalog"]': 'changeCatalog',
			'input input[name="frieze_inc"]': 'changeInput',
			'input input[name="barter_sum"]': 'changeBarter',
			'input textarea[name="barter_founding"]': 'changeBarterFound',
			'click #addApp': 'addApp',
			'click #barter': 'barterCheck',
			'load': 'loadedWindow'
		},
		redirect: false,
		barterCheck(){
		    debugger
		    var _view = this;
		    if(this.$el.find('input[name="barter"]')[0].checked){
		        if(this.$el.find("#document")[0]){
		            this.$el.find("#document")[0].style.display = "contents";
		        }
		    }else{
		        if(this.$el.find("#document")[0]){
		            this.$el.find("#document")[0].style.display = "none";
		            this.$el.find('input[name="document"]')[0].checked = false
		        }

		    }
		},
		addApp: function(){
			var _view = this;
			$.ajax({
				type: 'POST',
				url: '/apiclients/application_'+this.options.app_url+'/copy_app/',
				data: {id: this.model.id},
				success: function(response){
					router.navigate('app/'+_view.options.app_url+'/'+response.id, {trigger: true});
				},
			});
		},
		cancel: function(e){
			history.back();
		},
		changeCatalog: function(e){
			if(this.options.app_url != 'expo'){
				return;
			}
			var f;
			if(!e){
				f = this.$el.find('textarea[name="activities_catalog"]');
				f.parent().find('label').text('Деятельность для каталога ('+f.val().length+'):');
				if(f.val().length >= 400){
					f.parent().addClass('has-warning');
					return;
				}
				return;
			}
			else{
				f = $(e.currentTarget);
			}
			f.parent().find('label').text('Деятельность для каталога ('+e.currentTarget.textLength+'):');
			if(e.currentTarget.textLength >= 400){
				f.parent().addClass('has-warning');
				return;
			}
			f.parent().removeClass('has-warning');
		},
		changeBarter: function(e){
			this.$el.find('#total_bill').val(this.total_sum - e.currentTarget.value);
			this.change_bill = true;
		},
		changeBarterFound: function(e){
			this.change_bill = true;
		},
		countChars: function(s){
			var str = s.replace(/\s+/g, '');
			return str.length
		},
		changeInput: function(e){
			this.$el.find('input[name="count_symbol_frieze"]').val(this.countChars(e.currentTarget.value));
		},
		templateContext: function(){
			if(this.options.client_model){
				return {redirect: true, type: this.options.app_url};
			}
			return {type: this.options.app_url};
		},
		initialize: function(){
			if(this.options.client_model){
				router.navigate('app/'+this.options.app_url+'/create');
			}
			this.total_sum = 0;
			this.total_bill = 0;
			this.change_bill = false;

		},
		getTemplate: function(){
			return $(html).filter('#Application'+this.options.app_url+'CreateView')[0].outerHTML;
		},
		getOrderOneC: function(e, title){
			var _view = this;

			if(_view.$el.find('input[name="document"]')[0] && _view.$el.find('input[name="document"]')[0].checked && _view.$el.find('input[name="barter"]')[0].checked){
			    utils.notyError('Для данной заявки установлен запрет на формирование документа').show();
			    return
			}
			if(this.model && this.model.get('entity_guid_one_c')){
				require(['clients/js/applications/order_one_c_view', 'bootstrap-modal'], function(OrderOneCView){
					var modal = new Backbone.BootstrapModal({ 
						content: new OrderOneCView({print:_view.model.get('bill'), 
							organization: _view.model.get('organization'),
							person: _view.model.get('person'),
							type: _view.options.app_url,
							contracts: _view.model.get('contracts')
						}),
						title: title?title:'Сформировать счет',
						okText: _view.model.get('bill')?'Сохранить':'Создать',
						cancelText: 'Отмена',
						okCloses: false,
						animate: true, 
					}).open();
					var ModalView = modal.options.content;
					modal.on('ok', function(){
					    debugger
					    if( _view.options.app_url == 'lease_kvs' &&  !_view.model.attributes.count_people){
					        utils.notyError().show('Поле количество гостей обязательно');
					        return
					    }
					    if( _view.options.app_url == 'lease_kvs' &&  !_view.model.attributes.date_pay){
					        utils.notyError().show('Поле "оплата по договору до" обязательно');
					        return
					    }
						if(ModalView.validate()){
							$('.loading').fadeIn();
							// var data = new FormData(ModalView.$el.find('form')[0]);
							// data.append('app', _view.model.id);
							var data = utils.arraytoJson(ModalView.$el.find('form').serializeArray());
							var table_data = _view.Table.bootstrapTable('getData');
							data['Товары'] = [];
							data['app_id'] = _view.model.id;
							data['app_type'] = _view.options.app_url;
							data['barter_founding'] = _view.$el.find('textarea[name="barter_founding"]').val();
							data['entity_id'] = _view.SelectEntity[0].selectize.getValue();
							data['project_id'] = _view.SelectProject[0].selectize.getValue();
							data['client_id'] = _view.SelectClient[0].selectize.getValue();
							data['barter_sum'] = _view.$el.find('input[name="barter_sum"]').val();
							if(this.$el.find('input[name="document"]')[0]){
							    data['document'] =this.$el.find('input[name="document"]')[0].checked
							}
							data['date'] = _view.$el.find('#datetimepicker3').data("DateTimePicker").date().format().split('T')[0];
							data['Отказ'] = _view.$el.find('input[name="prepayment"]')[0].checked;
							data['type'] = _view.options.app_url;
							data['МеткаБартер'] = _view.$el.find('input[name="barter"]')[0].checked;
							data['manager_id'] = _view.SelectManager[0].selectize.getValue();
							data['СчетОтменен'] = _view.$el.find('#selectStatus')[0].selectize.getValue();
							data['Менеджер'] = _view.SelectManager[0].selectize.options[data['manager_id']].text;
							if( _view.options.app_url == 'lease_kvs'){
							debugger
                                data['count_people'] = _view.model.attributes.count_people;
                                data['date_pay'] = _view.model.attributes.date_pay;
                            }

							for(i in table_data){
								data['Товары'].push(table_data[i]);
								data['Товары'][i]['LineNumber'] = i;
							};
							if(_view.PayTable.bootstrapTable('getData').length != 0){
								data['plan_pay'] = _view.PayTable.bootstrapTable('getData');
							}
							else{
								data['plan_pay'] = [];
							}
							data['ОрганизацияПолучатель_Key'] = data['Организация_Key'];
							data['Контрагент_Key'] = _view.model.get('entity_guid_one_c');
							if(_view.model.get('bill')){
								data['Ref_Key'] = _view.model.get('bill');
							};
							$.ajax({
								type: _view.model.get('bill')?'PATCH':'POST',
								url: '/one_c/bill/',
								contentType: 'application/json',
								data: JSON.stringify(data),
								success: function(response, xhr){
									utils.notySuccess().show('Счет добавлен в 1С');
									_view.$el.find('input[name="bill"]').val(response['Ref_Key']);
									_view.$el.find('input[name="organization"]').val(data['Организация_Key']);
									_view.$el.find('input[name="person"]').val(data['ВЛице']);
									_view.save();
									$('.loading').fadeOut();
									modal.close();
								},
								error: function(response){
									$('.loading').fadeOut();
									utils.notyError().show('Ошибка формирования');
								}
							})
						}
					});
				})
			}
			else{
				utils.notyError('Нет данных 1С у юр. лица').show();
			}
		},
		removeOrderedService: function(e){
			var _view = this;
			var id = $(e.currentTarget).data().id;
			$.ajax({
				url: '/apiclients/ordered_services_'+_view.options.app_url+'/'+id+'/',
				type: 'DELETE',
				success: function(response){
					utils.notySuccess('Удалено').show();
					_view.total_sum = 0;
					_view.total_bill = 0;
					_view.Table.bootstrapTable('refresh', {slice: true});
					_view.change_bill = true;
				},
				error: function(response){
					utils.notyError().show();
				}
			});
		},
		editPay: function(e, model){
			var _view = this;
			if(this.model){
				require(['clients/js/applications/addPayView', 'bootstrap-modal'], function(View){
					var modal = new Backbone.BootstrapModal({ 
						content: new View({
							model: model, 
						}),
						title: 'Поcтупление',
						okText: 'Сохранить',
						cancelText: 'Отмена',
						okCloses: false,
						animate: true, 
					}).open();
					var ModalView = modal.options.content;
					modal.on('ok', function(){
						if(ModalView.validate()){
							var type = model?'PATCH':'POST';
							var url;
							var data = new FormData(ModalView.$el.find('form')[0]);
							if(model){
								url = '/apiclients/pay_app/'+model.id+'/'; 
							}
							else{
								data.append(_view.options.app_url, _view.model.id);
								url = '/apiclients/pay_app/'
							}
							$.ajax({
								type: type,
								url: url,
								contentType: false,
								processData: false,
								data: data,
								success: function(response){
									utils.notySuccess.show();
									_view.tablePay.bootstrapTable('refresh');
								},
							});
						}
					});
				});
			}
		},
		editOrderedService: function(e){
			var _view = this;
			var id = $(e.currentTarget).data().id;
			var row = this.Table.bootstrapTable('getRowByUniqueId', id);
			this.addOrderedService(e, new Backbone.Model(row));
		},
		saveAndRedirect: function(e){
			this.redirect = true;
			this.save(e);
		},
		save: function(e){
			var _this = this;
			$('.loading').fadeIn();
			var data = new FormData(this.$el.find('form')[0]);
			var checkbox = this.$el.find('[type=checkbox]').toArray();
			for(i in checkbox){
				data.set($(checkbox[i]).attr('name'), $(checkbox[i]).is(':checked'));
			};
			if(this.options.app_url=='lease_kvs'){
				data.set('date_start', this.$el.find('#datetimepicker1').data("DateTimePicker").date().format('YYYY-MM-DD'));
				data.set('date_end', this.$el.find('#datetimepicker2').data("DateTimePicker").date().format('YYYY-MM-DD'));
				if(!this.$el.find('#datetimepicker4').data("DateTimePicker").date()){
				    utils.notyError('Поле "Оплата по договору до" обязательно').show();
				    $('.loading').fadeOut();
				    return
				}
				data.set('date_pay', this.$el.find('#datetimepicker4').data("DateTimePicker").date().format('YYYY-MM-DD'));
			};
			data.set('date', this.$el.find('#datetimepicker3').data("DateTimePicker").date().format());
			data.append('app_type', _this.options.app_url);
			var ajaxtype = 'POST';
			if(this.model)
				ajaxtype = 'PATCH';
			$.ajax({
				type: ajaxtype,
				url: this.model?'/apiclients/application_'+this.options.app_url+'/'+this.model.id+'/':'/apiclients/application_'+this.options.app_url+'/',
				contentType: false,
				processData: false,
				data: data,
				success: function(response){
					_this.PayTable.bootstrapTable('refresh');
					if(_this.redirect){
						history.back();
					}
					else{
						router.navigate('app/'+_this.options.app_url+'/'+response.id+'/edit', {replace: true});
						_this.$el.find('#addOrderedService').removeClass('disabled');
						_this.$el.find('#addOrderedService').tooltip('destroy');
						utils.notySuccess().show();
						_this.model = new Backbone.Model(response);
						// _this.showMap();
						$('.loading').fadeOut();
					}
					if(_this.change_bill){
						_this.getOrderOneC(null, 'Сохраните изменения в счете!');
						_this.change_bill = false;
					}
				},
				error: function(response){
					utils.notyError().show();
					$('.loading').fadeOut();
				}
			});
		},
		addOrderedService: function(e, model){
			var _view = this;
			if(this.model){
				require(['clients/js/applications/addOrderedServiceView', 'bootstrap-modal'], function(addOrderedServiceView){
					var modal = new Backbone.BootstrapModal({ 
						content: new addOrderedServiceView({
							model: model, 
							date_app: _view.model.get('current_date'),
							project: _view.model.get('project'), 
							barter: _view.$el.find('input[name="barter"]')[0].checked,
							type: _view.options.app_url,
							max_discount: _view.model.get('max_discount')
						}),
						title: 'Добавить услугу',
						okText: 'Сохранить',
						cancelText: 'Отмена',
						okCloses: false,
						animate: true, 
					}).open();
					var ModalView = modal.options.content;
					modal.on('ok', function(){
						if(ModalView.validate()){
							var data = new FormData(ModalView.$el.find('form')[0]);
							data.append('nomenclature_name', ModalView.SelectOreders[0].selectize.options[ModalView.SelectOreders[0].selectize.getValue()]['Номенклатура']);
							data.append('app', _view.model.id);
							$.ajax({
								type: ModalView.model?'PATCH':'POST',
								url: ModalView.model?'/apiclients/ordered_services_'+_view.options.app_url+
								'/'+ModalView.model.id+'/':'/apiclients/ordered_services_'+
								_view.options.app_url+'/',
								contentType: false,
								processData: false,
								data: data,
								success: function(response){
									utils.notySuccess().show();
									_view.total_sum = 0;
									_view.total_bill = 0;
									_view.Table.bootstrapTable('refresh', {
										silent: true,
										url: '/apiclients/ordered_services_'+_view.options.app_url+'/?app='+_view.model.id
									});
									_view.change_bill = true;
									modal.close();
								},
								error: function(response){
									utils.notyError().show();
								}
							});
						}
					});
				})
			};
		},
		showMap: function(){
			var _this = this;
			var sites = this.model.get('plannings');
			require(['buildingmap/js/map_view_test'], function(MapView){
				for(i in sites){
					_this.$el.append('<div class="form" id="map'+ sites[i].id +'"></div>');
					if(!_this.hasRegion('map'+sites[i].id)){
						_this.addRegion('map'+sites[i].id, '#map'+sites[i].id);
					}
					sites[i].client = _this.model.get('client');
					sites[i].project = _this.model.get('project');
					sites[i].app = _this.model.id;
					// sites[i].planning = []
					_this.showChildView('map'+sites[i].id, new MapView({model: new Backbone.Model(sites[i])}));
				}
			});
		},
		onRender: function(){
			var _view = this;
            if(_view.$el.find("#document")[0]){
                if(_view.$el.find('input[name="barter"]')[0].checked){
                  _view.$el.find("#document")[0].style.display = "contents";
                }else{
                    _view.$el.find("#document")[0].style.display = "none";
                }
            }

			$('.loading').fadeOut();
			this.changeCatalog();
			if(!this.model){
				this.$el.find('#addOrderedService').addClass('disabled');
			};
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
				if(!_view.model){
					_view.$el.find('#addOrderedService').tooltip({title: "Сохраните для добавления"});
				}
			});
			this.$el.find('#datetimepicker3').datetimepicker({
                    locale: 'ru'
            });
            if(this.model){
            	this.$el.find('#datetimepicker3').data("DateTimePicker").date(this.model.get('current_date')?new Date(this.model.get('current_date')):new Date());
            }
			if(this.options.app_url=='lease_kvs'){
				this.$el.find('#datetimepicker4').datetimepicker({
					locale: 'ru',
					format: 'DD.MM.YYYY'
				});
				this.$el.find('#datetimepicker1').datetimepicker({
					locale: 'ru',
					format: 'DD.MM.YYYY'
				});
				this.$el.find('#datetimepicker2').datetimepicker({
					useCurrent: false,
					locale: 'ru',
					format: 'DD.MM.YYYY'
				});
				this.$el.find("#datetimepicker1").on("dp.change", function (e) {
					_view.$el.find('#datetimepicker2').data("DateTimePicker").minDate(e.date);
				});
				this.$el.find("#datetimepicker2").on("dp.change", function (e) {
					_view.$el.find('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
				});
				if(this.model){
				    if(this.model.get('date_pay')){
				        this.$el.find('#datetimepicker4').data("DateTimePicker").date(new Date(this.model.get('date_pay')));
				    }
					this.$el.find('#datetimepicker1').data("DateTimePicker").date(new Date(this.model.get('date_start')));
					this.$el.find('#datetimepicker2').data("DateTimePicker").date(new Date(this.model.get('date_end')));
				}
			}
			this.$el.find('#selectStatus').selectize({
				options:[
				{text:'В работе', value: 'working'},
				{text:'Выполнена', value:'finished'},
				{text:'Отменен', value: 'canceled'}],
				items:['working'],
				onInitialize: function(){
					if(_view.model)
						this.setValue(_view.model.get('status'));
				}
			});
			this.SelectColorCatalog = this.$el.find('#SelectColorCatalog').selectize({
				options: [{text: 'Цветной', value: 'color'}, {text: 'Ч/б', value: 'bw'}],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('color_catalog'));
					};
				}
			});
			this.$el.find('#color_picker').colorpicker();
			this.SelectProject = this.$el.find('#selectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					
				},
				getData: function(id){
					var _this = this;
					
				},
				onChange: function(value){
					try{
						_view.$el.find('#datetimepicker4').data("DateTimePicker").date(new Date(this.options[value].date_pay));
						_view.$el.find('#datetimepicker2').data("DateTimePicker").date(new Date(this.options[value].date_end));
						_view.$el.find('#datetimepicker1').data("DateTimePicker").date(new Date(this.options[value].date_start));
						if(_view.$el.find('input[name="count_people"]').val()=="" || _view.$el.find('input[name="count_people"]').val()=="0"){
							_view.$el.find('input[name="count_people"]').val(this.options[value].count_guests);	
						}	
					}catch(err){
						console.log('no date');
					}
				}
			});
			this.SelectClient = this.$el.find('#selectClient').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					var url = '/apiclients/clients/?data=min';
					if(_view.model && _view.model.get('client')){
						url = '/apiclients/clients/?id__in='+_view.model.get('client');
					}
					$.get({
						url: url,
						success: function(response){
							if(_view.model && _view.model.get('client')){
								_this.addOption(response);
							}
							else{
								for(i in response){
									_this.addOption(response[i]);
								}
							}
							if(_view.options.client_model){
								_this.setValue(_view.options.client_model.id);
							}
							if(_view.model){
								_this.setValue(_view.model.get('client'));
							}
						}
					});
				},
				onChange: function(value){
					// _this.SelectProject[0].selectize.settings.getData(value);
					_view.$el.find('#href_client').attr('href', '/#client/'+value);
					var url = '/apiclients/clients/forselectproject/?client_id='+value;
					if(_view.options.app_url=='lease_kvs'){
						url = '/apiprojects/projects/?data=min&client='+value;
					}
					if(_view.options.app_url=='lease_kvs'){
						url = '/apiprojects/projects/?data=min';
					}
					if(_view.options.app_url == 'ad'){
						url = '/apiprojects/projects/?data=min';
					}
					$.get({
						type: 'GET',
						url: url,
						success: function(response){
							_view.SelectProject[0].selectize.clearOptions();
							for(i in response){
								_view.SelectProject[0].selectize.addOption(response[i]);
							}
							if(_view.options.project_id){
								_view.SelectProject[0].selectize.setValue(_view.options.project_id);
							}
							if(_view.model){
								_view.SelectProject[0].selectize.setValue(_view.model.get('project'))
							}
						}
					});
					$.get({
						type: 'GET',
						url: '/apiclients/entity_private/?data=min&client='+value,
						success: function(response){
							_view.SelectEntity[0].selectize.clearOptions();
							for(i in response){
								_view.SelectEntity[0].selectize.addOption(response[i]);
							}
							if(_view.model){
								_view.SelectEntity[0].selectize.setValue(_view.model.get('entity'));
							}
						}
					});
				}

			});
			this.SelectManager = this.$el.find('#selectManager').selectize({
				searchField: 'text',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselect/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							_this.setValue(user.manager_id);
							if(!_this.options[_view.model.get('manager')]){
								_this.addOption({text: 'Неизвестный пользователь', value: _view.model.get('manager')});
							}
							if(_view.model){
								_this.setValue(_view.model.get('manager'));
							}
						},
					});	
				}
			});

			this.SelectNomination = this.$el.find('#SelectNomination').selectize({
				valueField: 'name',
				labelField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiclients/diploma_nomination/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							if(_view.model){
								_this.setValue(_view.model.get('diploma_nomination'));
							}
						},
					});	
				},
				onChange: function(value){
					_view.$el.find('input[name="diploma_nomination"]').val(value);
				}
			});

			this.SelectEntity = this.$el.find('#selectEntity').selectize({
				valueField: 'id',
				labelField: 'full_name',
				searchField: ['full_name'],
				onInitialize: function(){
					_this = this;
				},
				onChange: function(value){
					_view.$el.find('#href_entity').attr('href', '/#entity/'+value);
				}
			});

			this.PayTable = this.$el.find('#tablePay').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				checkbox: true,
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: _view.model?'/apiclients/pay_app/?'+_view.options.app_url+'='+_view.model.id:'',
				columns: [
					{
						field: 'date',
						title: 'Дата поступления',
					},
					{
						field: 'percent',
						title: 'Процент',
						formatter: function(value){
							if(value){
								return (value*100).toString() + ' %';
							}
							return null;
						}
					},
					{
						field: 'sum',
						title: 'Сумма',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toString().replace(/ /g,'');
                                var number = value;
                                value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                            }
                            return value;
                        },
					},
				],
				onClickRow: function(row){
					_view.editPay(undefined, new Backbone.Model(row));
				},
				rowStyle: function(row){
					return {
						classes: '',
						css: {
							cursor: 'pointer'
						}
					};
				}
			});

			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: _view.model?'/apiclients/ordered_services_'+_view.options.app_url+'/?app='+_view.model.id:'',
				columns: [
				{
					field: 'nomenclature_name',
					title: 'Наименование',
					sortable: true,
				},
				{
					field: 'count',
					title: 'Количество',
				},
				{
					field: 'units',
					title: 'Ед. изм.'
				},
				{
					field: 'discount_rate',
					title: 'Скидка %'
				},
				{
					field: 'markup',
					title: 'Наценка %'
				},
				{
					field: 'price',
					title: 'Стоимость',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'total',
					title: 'Итого',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: '',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
						row.id+'></span></span></div>';
					}
				}
				],
				responseHandler: function(response){
				    debugger
//					if(response.length > 0 && _view.model.get('main')){
//						_view.$el.find('input[name="barter"]').attr('disabled', true);
//						_view.$el.find('input[name="barter"]').parent().tooltip({title: 'Удалите все заказанные услуги чтобы использовать бартер'});
//					}
					if(_view.model.get('main')){
						_view.$el.find('input[name="barter"]').attr('disabled', false);
						_view.$el.find('input[name="barter"]').parent().tooltip('destroy');
					}
					if(!_view.model.get('main')){
						_view.$el.find('input[name="barter"]').parent().tooltip({title: 'Использовать только при 100% бартере'});
					}
					return response;
				},
				rowStyle: function(row, index) {
					// var disc = _view.$el.find('input[name="barter_sum"').val();
					if(_view.disc == undefined){
						_view.disc = _view.$el.find('input[name="barter_sum"]').val();
						_view.total_bill = _view.total_bill + row.total - _view.$el.find('input[name="barter_sum"]').val();
					}
					else{
						_view.total_bill += row.total;
					}
					_view.total_sum = _view.total_sum + row.total;
					_view.$el.find('#total_bill').val(_view.total_bill);
					_view.$el.find('#total_sum').text('Итого сумма: ' + _view.total_sum);
					return {classes: ''};
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params;
					}
					return params;
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
			if(this.model){
				this.showMap();
			};
			// require(['buildingmap/js/map_view'], function(MapView){

			// 	_view.showChildView('map', new MapView());
			// });
		}
	});
return ApplicationExpoView
})
