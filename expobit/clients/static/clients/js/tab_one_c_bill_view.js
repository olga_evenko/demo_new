define(['text!clients/html/clients_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#one_c_client_view')[0].outerHTML,
		events: {
		// 	'click #createIndividualButton': 'createIndividual'
		},
		className: 'displayNone',
		templateContext: function(){
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'ГУИД_счета',
				uniqueId: 'ГУИД_счета',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/clients/one_c_data/?client_id='+this.model.id,
				columns: [
				{
					field: 'Дата',
					title: 'Дата',
				},
				{
					field: 'Номер',
					title: '№',
				},
				{
					field: 'project_name',
					title: 'Проект',
				},
				{
					field: 'ОплаченнаяСумма',
					title: 'Оплаченная сумма',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'НеоплаченнаяСумма',
					title: 'Неоплаченная сумма',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'СуммаСчета',
					title: 'Сумма',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				}
				]
			});
		}
	});
	return View;
});
