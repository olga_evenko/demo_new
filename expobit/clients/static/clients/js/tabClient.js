define(['text!clients/html/clients_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal'], function(html, utils){

	var TabClientView = Backbone.Marionette.View.extend({
		template:$(html).filter('#tabClient')[0].outerHTML,
		className: 'displayNone',
		regions:{
			individualTable: "#individualTable"
		},
		events:{
			'click .edit': 'editClient',
			'click #addContactPerson': 'addContactPerson',
			'click .editRow': 'editContactPerson',
			'click .offContactPerson': 'offContactPerson',
			'click .removeContact': 'removeContact',
			'click #workfilterButton': 'workfilterButton',
			'click #allfilterButton': 'allfilterButton',
			'click #planfilterButton': 'planfilterButton'
		},
		initialize: function(){
		},
		allfilterButton: function(){
			this.BtBtableProject.bootstrapTable('refresh', {url: '/apiprojects/projects_in_client/?client='+this.model.id,})
		},
		workfilterButton: function(){
			this.BtBtableProject.bootstrapTable('refresh', {url: '/apiprojects/projects_in_client/?status=working&client='+this.model.id,})
		},
		planfilterButton: function(){
			this.BtBtableProject.bootstrapTable('refresh', {url: '/apiprojects/projects_in_client/?status=planning&client='+this.model.id,})
		},
		templateContext: function(){
			var status = {
				'active': 'Активный',
				'inactive': 'Неактивный',
				'black_list': 'Черный список',
				'key': 'Ключевой'
			};
			return {status_text: status[this.model.get('status')]};
		},
		removeContact: function(e){
			var _view = this;
			var id = $(e.currentTarget).data().id;
			var modal = new Backbone.BootstrapModal({ 
				content: 'Вы хотите удалить контактное лицо?',
				title: 'Удалить контактное лицо',
				okText: 'Удалить',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$.ajax({
					type: 'DELETE',
					url: '/apiclients/contact_person/'+id+'/',
					success: function(response){
						_view.tableContactsPerson.bootstrapTable('refresh', {
							url:'/apiclients/contact_person/?client='+_view.model.id,
							silent: true
						});
						utils.notySuccess('Удалено').show();
					},
					error: function(response){
						utils.notyError('Ошибка удаления').show();
					}
				});
			});
		},
		editContactPerson: function(e){
			this.addContactPerson(e, new Backbone.Model(this.tableContactsPerson.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id)))
		},
		offContactPerson: function(e){
			var _view = this;
			var data = new FormData();
			if(this.tableContactsPerson.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id).active){
				data.append('active', false);
			}
			else{
				data.append('active', true);
			};
			$.ajax({
				type: 'PATCH',
				url: '/apiclients/contact_person/' + $(e.currentTarget).data().id + '/',
				processData: false,
				contentType: false,
				data: data,
				success: function(response){
					_view.tableContactsPerson.bootstrapTable('refresh', {
						url:'/apiclients/contact_person/?client='+_view.model.id,
						silent: true
					});
					utils.notySuccess('Статус изменен').show();
					_view.model.fetch();
				},
				error: function(response){
					utils.notyError().show();
				}
			});
		},
		editClient: function(e){
			var id = $(e.currentTarget).data().id;
			router.navigate('client/'+id+'/edit', {trigger: true});
			// var model = new Backbone.Model();
			// model.url = '/apiclients/clients/'+id+'/';
			// $('.loading').fadeIn();
			// model.fetch({
			// 	success: function(){
			// 		require(['clients/js/create_client_view'], function(CreateClientView){
			// 			window.mainView.showChildView('MainContent', new CreateClientView({model:model}));
			// 		})	
			// 	}
			// });
		},
		addContactPerson: function(e, model){
			var _view = this;
			require(['clients/js/contacts_person/contact_person_view'], function(ContactPersonView){
				var modal = new Backbone.BootstrapModal({ 
					content: new ContactPersonView({model:model}),
					title: 'Контактное лицо',
					okText: 'Сохранить',
					cancelText: 'Отмена',
					okCloses: false,
					animate: true, 
				}).open();
				var ModalView = modal.options.content;
				modal.on('ok', function(){
					if(modal.options.content.validate()){
						var _this = this;
						var data = new FormData(this.$content.find('form')[0]);
						data.append('client', _view.model.id);
						for(i in ModalView.removePhones){
							data.append('removePhones', ModalView.removePhones[i]);
						};
						for(i in ModalView.removeEmails){
							data.append('removeEmails', ModalView.removeEmails[i]);
						};
						$.ajax({
							url: model?'/apiclients/contact_person/'+model.id+'/':'/apiclients/contact_person/',
							type: model?'PATCH':'POST',
							contentType: false,
							processData: false, 
							data: data,
							success: function(response){
								_this.close();
								utils.notySuccess().show();
								_view.tableContactsPerson.bootstrapTable('refresh', {
									url:'/apiclients/contact_person/?client='+_view.model.id,
									silent: true
								});
								_view.model.fetch();
							},
							error: function(response){
								if(response.responseJSON.error_name){
									utils.notyError(response.responseJSON.error_name).show();
									return;
								}
								utils.notyError().show();
							}
						});
					};
				});
			})
		},
		onRender: function(){
		    //	для сайтов
            var sites = this.model.get('site').split(' ')
            var html_sites = ''
            for(var i in sites){
                html_sites += `<a target="_blank" href="http://${sites[i].replace(/[,;]/g, '')}">${sites[i]}</a> `
            }
		    this.$el.find('#siteSplit').html(html_sites)

			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn();
			});
			this.tableProject = this.$el.find('#tableProject').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				data: this.model.get('projects'),
				columns: [
				{
					field: 'project_type',
					title: 'Вид проекта'
				},
				{
					field: 'project.name',
					title: 'Проект'
				},
				{
					field: 'group',
					title: 'Группа',
					editable: {
						source: [{text: 'A', value: 'a'},{text: 'B', value: 'b'},
						{text: 'C', value: 'c'},{text: 'VIP', value: 'vip'}],
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
						    beforeSend: function(jqXHR,settings){
							}
						},
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						emptytext: 'Назначить',
						url: '/apiclients/clients_group/set_group/',
						params: function(params){
							return params
						},
						success: function(response){
							_this.$el.find('#tableProject').bootstrapTable('refresh', {silent: true, 
								url: '/apiclients/project_table/?client='+_this.model.id});
						}
					}
				},
				{
					title: 'Менеджер',
					field: 'manager.name'
				}
				]
			});
			this.BtBtableProject = this.$el.find('#b2btableProject').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				url: '/apiprojects/projects_in_client/?client='+this.model.id,
				pagination: true,
				totalField: 'count',
				sidePagination: 'server',
				classes: 'table table-hover table-bordered',
				dataField: 'results',
				toolbar: $(html).filter('#toolbar_b2b_project')[0].innerHTML,
				columns: [
					{
						field: 'date',
						title: 'Даты'
					},
					{
						field: 'user',
						title: 'Создал'
					},
					{
						field: 'project_type',
						title: 'Вид проекта'
					},
					{
						field: 'name',
						title: 'Проект'
					},
					{
						field: 'status',
						title: 'Статус'
					},
					// {
					// 	field: 'manager',
					// 	title: 'Менеджер',
					// },
					{
						field: 'contentment',
						title: 'Удовл. клиента',
					}
				],
				onClickRow: function(row, e, index){
					if(row.user_id == user.id || user.isSuperuser){
						router.navigate('project/'+row.id+'/edit', {trigger: true});
					}
				},
				rowStyle: function(row, index){
					if(row.user_id == user.id || user.isSuperuser){
						return {
							classes: '',
							css: {
								cursor: 'pointer'
							}
						}
					}
					return {
							classes: '',
							css: {}
						}
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				data: this.model.get('entitys'),
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// rowStyle: function(){}
				// url: '/apiclients/individuals/',
				columns: [
				{
					field: 'full_name',
					title: 'Юр. лицо',
					sortable: true,
				},
				{
					field: 'address',
					title: 'Адрес',
					sortable: true,
				},
				{
					field: 'activities',
					title: 'Виды деятельности'
				}
				],
				onClickRow: function(row, e, index){
					$('.loading').fadeIn();
					var model = new Backbone.Model();
					model.url = '/apiclients/entity_private/'+row.id+'/';
					model.fetch({
						success: function(response){
							$('.loading').fadeOut();
							require(['clients/js/modal_individual_in_client_view', 'bootstrap-modal'], function(modalIndividualsView){
								_this.showChildView('individualTable', new modalIndividualsView({model: model, client_id: _this.model.id, show_button:false}))
							});
						}
					});
				},
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				}
			});
			this.tableContactsPerson = this.$el.find('#tableContactsPerson').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				data: this.model.get('contacts_person'),
				columns: [
				{
					field: 'name',
					title: 'Контактное лицо'
				},
				{
					field: 'emails_string',
					title: 'Email',
				},
				{
					field: 'phones_string',
					title: 'Телефоны',
				},
				{
					field: '',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "text-align: center;"><span style="margin-right: 10px; margin-left: 0px;" class="glyphicon glyphicon-pencil pencil editRow" data-id ='+
						row.id+'></span><span style="margin-right: 10px; margin-left: 0px;" data-toggle="tooltip" title="Активный/Неактивный контакт" class="glyphicon glyphicon-off pencil offContactPerson" data-id ='+
						row.id+'></span></span><span style="margin-right: 0px; margin-left: 0px;" data-toggle="tooltip" title="Удалить контакт" class="glyphicon glyphicon-trash pencil removeContact" data-id ='+
						row.id+'></span></span></div>';
					},
					width: '89px',
				}
				],
				rowStyle: function(row, index){
					var cls = '';
					if(!row.active){
						cls = 'danger';
					};
					return {
						css: {"cursor": "pointer"},
						classes: cls
					};
				},
				rowAttributes: function(row, index, e){
					var phones = [];
					var emails = [];
					for(i in row.phones){
						phones.push(row.phones[i].phone); 
					};
					for(i in row.emails){
						emails.push(row.emails[i].email); 
					};
					row.emails_string = emails.join(', ');
					row.phones_string = phones.join(', ');	
				}
			});
			
		}
	});
return TabClientView
})
