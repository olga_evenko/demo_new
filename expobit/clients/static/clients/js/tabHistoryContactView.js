define(['text!clients/html/clients_view.html', 'utils', 
	'clients/js/applications/ApplicationExpoView', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize', 'datetimepicker'], function(html, utils, ApplicationExpoView){

	var TabHistoryContactView = Backbone.Marionette.View.extend({
		template:$(html).filter('#tabHistoryContacts')[0].outerHTML,
		className: 'displayNone',
		events:{
			'click #failure': 'failure_check',
			'click #saveContact': 'validate',
			'click #addProject': 'addProject',
			'click #openHistory': 'openHistory',
			'click #saveNewContact': 'saveNewContact',
			'click #showContactPerson': 'showContactPerson',
			'click .listen': 'listenAudio'
		},
		results:{
			'failure':{
				res: false,
				title: 'Отказ'
			},
			'application':{
				res: false,
				title: 'Заявка'
			},
			'goal_contact':{
				res: false,
				title: 'Запланирован контакт'
			},
			'new_interest':{
				res: false,
				title: 'Интерес на др. услуги'
			},
			'interest':{
				res: false, 
				title: 'Интерес'
			}
		},
		templateContext: function(){
			if(user.groups.indexOf('kvs')!=-1){
				return {kvs: true, tableContext: window.appData.clients[this.model.id]};
			}
			return {tableContext: window.appData.clients[this.model.id]};
		},
		showContactPerson: function(e){
			if(!this.showContactPersonBool){
				this.$el.find('.tableContactsPersons').collapse('show');
				this.showContactPersonBool = true;
				return;
			}
			this.showContactPersonBool = false;
			this.$el.find('.tableContactsPersons').collapse('hide');
		},
		openHistory: function(e){
			var _view = this;
			require(['clients/js/old_contact_view', 'projects/js/modal_template'], function(View, tmp){
				var modal = new Backbone.BootstrapModal({ 
					content: new View({model: _view.model}),
					title: 'История контактов СОУВ',
					okText: 'Закрыть',
					cancelText: '',
					animate: true, 
					template: tmp
				}).open();
			});
		},
		listenAudio: function(e){
			var link = $(e.currentTarget).data('link');
			var modal = new Backbone.BootstrapModal({ 
					content: '<audio controls><source src="'+link+'" type="audio/ogg">Браузер не поддерживает</audio>',
					title: 'Слушать',
					okText: 'Закрыть',
					cancelText: '',
					animate: true, 
					// template: tmp
				}).open();
		},
		getResults: function(){
			// var inputs = this.$el.find('.results input').toArray();
			var textarr = [];
			for(i in this.results){
				if(this.$el.find('[name="'+i+'"]').val() != '' && this.$el.find('[name="'+i+'"]').val() != undefined){
					if(this.$el.find('[name="'+i+'"]').attr('type')=='checkbox'){
						if(this.$el.find('[name="'+i+'"]').is(':checked')){
							this.results[i].res = true;
							textarr.push(this.results[i].title);
						};
					}
					else{
						this.results[i].res = true;
						textarr.push(this.results[i].title);
					};
				};
			};
			// for(i in inputs){
			// 	if($(inputs[i]).val() != ''){
			// 		if(this.results[$(inputs[i]).attr('name')])
			// 			this.results[$(inputs[i]).attr('name')].res = true;
			// 	};
			// };
			// for(i in this.results){
			// 	if(this.results[i].res){
			// 		textarr.push(this.results[i].title);
			// 	};
			// };
			return textarr.join(', ');
		},
		mainSave: function(){},
		saveApplication: function(){},
		addProject: function(e){
			var _view = this;
			require(['projects/js/project_form_view', 'projects/js/modal_template'], function(ProjectView, tmp){
				var modal = new Backbone.BootstrapModal({ 
					content: new ProjectView({client: {id:_view.model.id, name: _view.model.get('name')}}),
					title: '',
					okText: 'Сохранить',
					cancelText: 'Отмена',
					animate: true, 
					escape: false,
					modalOptions: {
						backdrop: 'static',
						keyboard: false
					},
					template: tmp
				}).open();
				setTimeout(function(){$('input[name="name"]').focus()}, 500)
				// modal.options.content.$el.fadeOut();
				modal.on('ok', function(){
					modal.options.content.save();
				});
				modal.options.content.on('closeModal', function(option){
					_view.SelectProject[0].selectize.addOption(option);
					_view.SelectProject[0].selectize.setValue(option.id);
					modal.close();
				});
			});
		},
		saveOtherService: function(){
			var _this = this;
			var data = new FormData();
			var inputs = $('.other_services input').toArray();
			for(i in inputs){
				data.append($(inputs[i]).attr('name'),$(inputs[i]).val());
			};
			data.append('new_interest', _this.SelectizeBuisnessUnit[0].selectize.getValue());
			data.append('client', _this.model.id);
			data.append('contact_person', this.SelectIndividual[0].selectize.options[this.SelectIndividual[0].selectize.getValue()].name);
			data.append('contact_data', this.selectContact[0].selectize.options[this.selectContact[0].selectize.getValue()].text);
			$.ajax({
				type: 'POST',
				url: '/apiclients/other_interest/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					utils.notySuccess('Передан интерес на др. услуги').show();
					_this.resetForm();
				},
				error: function(response){
					utils.notyError('Ошибка передчи интереса на др. услуги').show();
				}
			});
		},
		saveNewContact: function(e){
			var _this = this;
			var data = new FormData();
			var inputs = $('.followContact input').toArray();
			for(i in inputs){
				data.append($(inputs[i]).attr('name'),$(inputs[i]).val());
			};
			data.append('project', _this.SelectProject[0].selectize.getValue());
			data.append('client', _this.model.id);
			$.ajax({
				type: 'POST',
				url: '/apiclients/new_contacts/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					utils.notySuccess('Новый след. контакт сохранен').show();
					if(e){
						_this.resetForm();
					}
				},
				error: function(response){
					utils.notyError('Ошибка сохранения след. контакта').show();
				}
			});

		},
		validate: function(e){
			var valid = false;
			var inputs = this.$el.find('.results input').toArray();
			for(i in inputs){
				if($(inputs[i]).val()!=''){
					if($(inputs[i]).attr('type')=='checkbox'){
						if(!valid){
							valid = $(inputs[i]).is(':checked');
						}
					}
					else{
						valid = true;
					}
				}
			}
			if(this.$el.find('.results select').val()!=''){
				valid = true;
			}
			if(valid){
				this.save($(e.currentTarget).data().notresetform);
			}
			else{
				utils.notyAlert('Заполните результат').show();
			}
		},
		createUrl: function(){
			$('.loading').fadeIn();
			$.ajax({
				type: 'POST',
				url: '/apiclients/contacts/create_url/',
				contentType: 'application/json',
				data: JSON.stringify({hostname: 'expoapp'}),
				success: function(response){
					$('.loading').fadeOut();
					utils.notySuccess('Ссылка на заявку создана').show();
					var modal = new Backbone.BootstrapModal({ 
						content: '<input class="form-control" type="text" value='+ response.url +'>',
						title: 'Ссылка на заявку',
						okText: 'Закрыть',
						cancelText: '',
						animate: true, 
					}).open();
					// modal.on('ok', function(){
						
					// });
				},
				error: function(response){
					utils.notyError().show();
					$('.loading').fadeOut();
				}
			});
		},
		setGuidField: function(){
			this.$el.find('input[name="guid_audio"]').val(this.genUuid().replace(/-/g, ''));
		},
		save: function(e){
			var _this = this;
			var data = new FormData(this.$el.find('form')[0]);
			data.append('result', this.getResults());
			data.append('client', this.model.id);
			// data.append('guid_audio', this.genUuid().replace());
			var notInContact = this.$el.find('.notInContact input').toArray();
			for(i in notInContact){
				data.delete($(notInContact[i]).attr('name'));
			}
			if(this.$el.find('input[name="date_new_contact"]').val() != '' && this.$el.find('input[name="goal_contact"]').val() == ''){
				utils.notyAlert('Заполните цель контакта').show();
				return;	
			}
			if(this.results.goal_contact.res){
				var inputs = $('.followContact input').toArray();
				for(i in inputs){
					data.append($(inputs[i]).attr('name'), $(inputs[i]).val());
				}
				data.append('save_new_contact', true);
				// this.saveNewContact();
			}
			if(this.results.new_interest.res){
				this.saveOtherService();
			}
			data.set('contact_person', data.get('contact_person').split('-')[0]); 
			data.set('date', this.$el.find('#datetimepicker2').data("DateTimePicker").date().format());
			$.ajax({
				type: 'POST',
				url: '/apiclients/contacts/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					utils.notySuccess().show();
					_this.model.fetch();
					// if(!_this.results.goal_contact.res){
						if(_this.results.application.res && _this.$el.find('#select_application').val() != 'create_url'){
							// window.mainView.showChildView('MainContent', new ApplicationExpoView({
							// 	client_model: _this.model,
							// 	app_url:_this.$el.find('#select_application').val(),
							// 	selectizeOptions:{
							// 		options: _this.SelectProject[0].selectize.options,
							// 		item: _this.SelectProject[0].selectize.items
							// 	},
							// 	project_id: _this.$el.find('#selectProject').val(),
							// 	redirect: 'clients/js/applications/ApplicationExpoView'
							// }));
							router.navigate('app/'+response.app_type+'/'+response.app_id, {trigger: true});
						}
						else{
							if(_this.$el.find('#select_application').val() == 'create_url'){
								_this.createUrl();
							}
							_this.resetForm(e);
						}
					// }
					_this.$el.find('#table').bootstrapTable('refresh', {silent: true})
				},
				error: function(response){
					if(response.responseJSON){
						utils.notyError(response.responseJSON.manager_error).show();
						return;	
					}
					utils.notyError().show();
				}
			});
		},
		failure_check: function(e){
			if(this.$el.find('input[name="failure"]').attr('disabled')){
				this.$el.find('input[name="failure"]').attr('disabled', false);
			}
			else{
				this.$el.find('input[name="failure"]').attr('disabled', true);
			};
		},
		resetForm: function(e){
			this.$el.find('form')[0].reset();
			this.$el.find('input[name="failure"]').attr('disabled', true);
			this.setGuidField();
			if(!e){
				var selectized = this.$el.find('.selectized').toArray();
				for(i in selectized){
					selectized[i].selectize.clear();
				}
			}
			this.$el.find('#datetimepicker2').data("DateTimePicker").date(new Date());
			for(i in this.results){
				this.results[i].res = false;
			}
			this.SelectProject[0].selectize.setValue(sessionStorage.getItem('client_project_select'));
		},
		initialize: function(){
			this.genUuid = ()=>([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,c=>(c^crypto.getRandomValues(new Uint8Array(1))[0]&15 >> c/4).toString(16));
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.$el.find('#datetimepicker2').datetimepicker({
                    locale: 'ru',
                    defaultDate: new Date()
            });
            this.setGuidField();
			this.$el.find('#selectTypeContact').selectize({
				labelField: 'display_name',
				searchField: 'display_name',
				onInitialize: function(){
					var _this = this;
					$.get({
						type: 'OPTIONS',
						url: '/apiclients/contacts/',
						success: function(response){
							var choices = response.actions.POST.type_contact;
							for(i in choices){
								_this.addOption(choices[i]);
							};
						}
					});
				}
			});
			this.selectContact = this.$el.find('#selectContact').selectize({
				valueField: 'text',
				render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeTemplateContact')[0].innerHTML)(item);
					}
				},
				onChange: function(value){
					_view.$el.find('#contact_parametrs a').remove();
					_view.$el.find('#email_copy').remove();
					if(value != ""){
						if(this.options[value].phone){
							_view.$el.find('#contact_parametrs').append('<a style="padding-left: 10px;" href="callto:'+value+':'+_view.$el.find('input[name="guid_audio"]').val()+'"><span class="glyphicon glyphicon-earphone"></span></a>');
						}
						if(this.options[value].email){
							// try {
							// 	window.copy(this.options[value].email);
							// } catch(e) {
							// 	utils.notyAlert('Ваш браузер не поддерживает копирование, скопируйте почту вручную');
							// 	console.log(e);
							// }
							_view.$el.find('#contact_parametrs').parent().append('<input id="email_copy" class="form-control" type="text" value="'+value+'">');
							_view.$el.find('#email_copy').select();
							document.execCommand("copy");
							_view.$el.find('#contact_parametrs').append('<a target="_blank" style="padding-left: 10px;" href="http://mail.donexpocentre.ru"><span class="glyphicon glyphicon-envelope"></span></a>');
						}
					}
				}
			});
			this.SelectIndividual = this.$el.find('#selectIndividual').selectize({
				valueField: 'id',
				labelField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						type: 'GET',
						url: '/apiclients/contact_person/?client='+_view.model.id+'&active=true',
						success: function(response){
							for(i in response){
								_this.addOption(response);
							};
						}
					});
				},
				onItemAdd: function(value, $item){
					_view.selectContact[0].selectize.clearOptions();
					for(i in this.options[value].emails){
						this.options[value].emails[i].text = this.options[value].emails[i].email;
						_view.selectContact[0].selectize.addOption(this.options[value].emails[i]);
					};
					for(i in this.options[value].phones){
						this.options[value].phones[i].text = this.options[value].phones[i].phone;
						_view.selectContact[0].selectize.addOption(this.options[value].phones[i]);
					};
					if(this.options[value].fax != ''){
						_view.selectContact[0].selectize.addOption({text:this.options[value].fax, fax:this.options[value].fax});
					};
				}
			});
			this.SelectProject = this.$el.find('#selectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					if(user.isSuperuser){
						$.get({
							type: 'GET',
							url: '/apiprojects/projects/?data=min',
							success: function(response){
								for(i in response){
									_this.addOption(response[i]);
								}
								_this.setValue(sessionStorage.getItem('client_project_select'));
							}
						});
						return;
					}
					if(user.groups.indexOf('ad')!=-1 || user.groups.indexOf('head_ad')!=-1){
						$.get({
							type: 'GET',
							url: '/apiprojects/projects/?attr_visitors_isnull=False&data=min&status=working',
							success: function(response){
								for(i in response){
									_this.addOption(response[i]);
								}
								_this.setValue(sessionStorage.getItem('client_project_select'));
							}
						});
						return;
					}
					if(user.groups.indexOf('kvs')==-1){
						$.get({
							type: 'GET',
							url: '/apiclients/clients/forselectproject/?status=working&client_id='+_view.model.id,
							success: function(response){
								for(i in response){
									_this.addOption(response[i]);
								}
								_this.setValue(sessionStorage.getItem('client_project_select'));
							}
						});
						return;
					}
					if(user.groups.indexOf('kvs')!=-1){
						$.get({
							type: 'GET',
							url: '/apiprojects/projects/?client='+_view.model.id,
							success: function(response){
								for(i in response){
									_this.addOption(response[i]);
								}
								_this.setValue(sessionStorage.getItem('client_project_select'));
							}
						});
						return;
					}
				},
			});
			var options_apps = [
				{text:'Заявка экспонента', value:'expo'},
				{text:'Заявка на рекламные услуги/ застройку и брендирование', value:'ad'},
				{text:'Заявка на Услуги (БХ, ДП, БД)',value:'lease_kvs'}];
			if(user.groups.indexOf('kvs')!=-1){
				options_apps = [
					{text:'Заявка на аренду площадей КВС',value:'lease_kvs'}];
			}
			if(user.groups.indexOf('ad')!=-1 || user.groups.indexOf('head_ad')!=-1){
				options_apps = [
					{text:'Заявка на рекламные услуги/ застройку и брендирование', value:'ad'}
					];
			}
			if(user.groups.indexOf('managers') != -1 || user.groups.indexOf('project_managers') != -1)
			{	
				options_apps = [
					{text:'Заявка экспонента', value:'expo'}
				];
				// options_apps.push({text:'Сгенерировать ссылку', value: 'create_url'});
			}
			if(user.isSuperuser){
				options_apps.push({text:'Сгенерировать ссылку', value: 'create_url'});
			}
			this.$el.find('#select_application').selectize({
				options:options_apps
			});
			this.SelectizeBuisnessUnit = this.$el.find('#select_direction').selectize({
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/projects_types/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption({
									value:response[i].id,
									text:response[i].type});
							}
							return response;
						}
					});
				},
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/contacts/?client=' + this.model.id,
				columns: [
					// {
					// 	checkbox: true
					// },
					{
						field: 'date',
						title: 'Дата',
						sortable: true,
					},
					{
						field: 'type_contact',
						title: 'Вид взаимодействия',
						sortable: true,
					},
					{
						field: 'contact',
						title: 'Контакт',
						sortable: true,
					},
					{
						field: 'contact_person',
						title: 'Контактное лицо',
						sortable: true,
					},
					{
						field: 'project',
						title: 'Проект',
						sortable: true,
					},
					{
						field: 'manager',
						title: 'Менеджер',
						sortable: true,
					},
					{
						field: 'comment',
						title: 'Комментарии',
						sortable: true,
					},
					{
						field: 'failure',
						title: 'Причина отказа',
					},
					{
						field: 'result',
						title: 'Результат'
					},
					{
						field: 'audio_url',
						title: 'Аудио-запись',
						// halign: 'center',
						formatter: function(value, row, index){
							if(row.type_contact == 'Встреча'){
								return null;
							}
							if(value){
								return '<span style="cursor: pointer;" class="glyphicon glyphicon-headphones listen" data-link="'+value+'"></span></p>';
								// return '<audio controls><source src="'+value+'" type="audio/ogg">Браузер не поддерживает</audio>'
							}
							return null;
						}
					}
				],
				// onClickCell: function(field, value, row, $element){
				// 	debugger
				// },
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
			this.tableContactsPerson = this.$el.find('#tableContactsPerson').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				data: this.model.get('contacts_person'),
				columns: [
				{
					field: 'name',
					title: 'Контактное лицо'
				},
				{
					field: 'emails_string',
					title: 'Email',
				},
				{
					field: 'phones_string',
					title: 'Телефоны',
				},
				// {
				// 	field: '',
				// 	title: '',
				// 	formatter: function(value, row, index){
				// 		return '<div style = "text-align: center;"><span style="margin-right: 10px; margin-left: 0px;" class="glyphicon glyphicon-pencil pencil editRow" data-id ='+
				// 		row.id+'></span><span style="margin-right: 10px; margin-left: 0px;" data-toggle="tooltip" title="Активный/Неактивный контакт" class="glyphicon glyphicon-off pencil offContactPerson" data-id ='+
				// 		row.id+'></span></span><span style="margin-right: 0px; margin-left: 0px;" data-toggle="tooltip" title="Удалить контакт" class="glyphicon glyphicon-trash pencil removeContact" data-id ='+
				// 		row.id+'></span></span></div>';
				// 	},
				// 	width: '89px',
				// }
				],
				rowStyle: function(row, index){
					var cls = '';
					if(!row.active){
						cls = 'danger';
					};
					return {
						css: {"cursor": "pointer"},
						classes: cls
					};
				},
				rowAttributes: function(row, index, e){
					var phones = [];
					var emails = [];
					for(i in row.phones){
						phones.push(row.phones[i].phone); 
					};
					for(i in row.emails){
						emails.push(row.emails[i].email); 
					};
					row.emails_string = emails.join(', ');
					row.phones_string = phones.join(', ');	
				}
			});
		}
	});
	return TabHistoryContactView
})
