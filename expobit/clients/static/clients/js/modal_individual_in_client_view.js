define(['text!clients/html/clients_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var IndividualsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#modal_individual_in_client_view')[0].outerHTML,
		events: {
			'click #createIndividualButton': 'createIndividual'
		},
		templateContext: function(){
			return {show_button: this.options.show_button}
		},
		createIndividual: function(){
			var _this = this;
			require(['clients/js/create_individual_view'], function(CreateIndividualView){
				_this.model.url = '/apiclients/clients/'+_this.model.get('client')+'/';
				window.mainView.showChildView('MainContent', new CreateIndividualView({entity_model: _this.model, redirect: 'clients/js/create_client_view'}));
			})
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				$('#individualTable').collapse('show');
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				data: this.model?this.model.get('entity_private'):[],
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// rowStyle: function(){}
				// url: '/apiclients/individuals/',
				columns: [
				{
					field: 'individual',
					title: 'ФИО',
					sortable: true,
				},
				{
					field: 'phone',
					title: 'Телефон',
					sortable: true,
				},
				{
					field: 'type_position',
					title: 'Тип должности',
					sortable: true,
				},
				{
					field: 'position',
					title: 'Должность',
					sortable: true,
				}
				],
				onClickRow: function(row, element, index){
					router.navigate('individual/'+row.individual_id, {trigger: true});
				}
			});
		}
	});
	return IndividualsView
})
