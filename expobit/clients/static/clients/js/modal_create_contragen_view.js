define(['text!clients/html/entitys_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var CreateContragentView = Backbone.Marionette.View.extend({
		template:$(html).filter('#modal_contragent_view')[0].outerHTML,
		events: {
			'click #CopyAddress': 'copyAddress',
			'change input[name="ИНН"]': 'checkInn',
			'input input[name="СвидетельствоСерияНомер"]': 'addSymbol'
		},
		templateContext: function(){
			return {other: this.options.other == 'true'};
		},
		addSymbol: function(e){
			var val = e.currentTarget.value;
			val = val.replace(/ /g, '');
			if(val.length > 2){
				$(e.currentTarget).val(val.substr(0,2)+' '+(val.substr(2,100)));
			}
		},
		checkInn: function(e){
			var inn = e.currentTarget?e.currentTarget.value:e;
			var _view = this;
			$.get({
				url: '/apiclients/entity_private/one_c_check/?inn=' + inn,
				success: function(response){
					if(response.length > 0){
						var html_error;
						if(response.length == 1){
							html_error = '<p style="color: red; font-size: 15px; margin-top: 15px;">Контрагент с таким ИНН существует (' + response[0]['НаименованиеПолное'] + ')</p>';
						}
						else{
							html_error = '<p style="color:red; font-size: 15px; margin-top: 15px;">Контрагенты с таким ИНН существуют (' + response[0]['НаименованиеПолное'];
							for(var i=1; i<response.length; i++){
								html_error += ', ' + response[i]['НаименованиеПолное'];
							}
							html_error += ')</p>';
						}
						_view.$el.find('#inn_error').html(html_error);
						_view.$el.find('#inn_error').collapse('show');
						return;
					}
					_view.$el.find('#inn_error').collapse('hide');
				}
			});
		},
		copyAddress: function(){
			this.$el.find('input[name="Адрес"]').val(this.options.address);
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.SelectContrAgent = this.$el.find('#SelectContrAgent').selectize({
				valueField: 'hid',
				labelField: 'value',
				searchField: ['value', 'inn', 'value_address'],
				// options:[{'Ref_Key':'678', 'ИНН':'afa'}],
				loadingClass: 'loader',
				onInitialize: function(){
				},
				load: function(input) {
                    var _this=this;
                    var response = $.get({
                        url: '/apiclients/searchtax/?query='+input,
                        async: true,
                        success: function(response) {
                        	_this.clearOptions();
                        	var data = response.suggestions;
                        	for(i in data){
                        		data[i].data.value = data[i].value;
                        		data[i].data.value_address = data[i].data.address.value;
                        		_this.addOption(data[i].data);
                        	};
                            _this.open();
                            // return response
                        }
                    });
                },
                render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeTemplateNalog')[0].innerHTML)(item);
					}
				},
                onChange: function(value){
                	var data = this.options[value];
                	_view.$el.find('input[name="ИНН"]').val(data.inn);
                	_view.$el.find('input[name="НаименованиеПолное"]').val(data.value);
                	_view.$el.find('input[name="Адрес"]').val(data.address.value);
                	_view.$el.find('input[name="КПП"]').val(data.kpp);
                	_view.checkInn(data.inn);
                }
			});
			// this.autocomplite = this.$el.find('input[name="НаименованиеПолное"]').autocomplete({
			// 	serviceUrl: '/apiclients/searchtax/',
			// 	// transformResult: function(response) {
			// 	// 	var result = JSON.parse(response);
			// 	// 	debugger;
			//  //        return {
			//  //            suggestions: $.map(result.predictions, function(dataItem) {
			//  //                return { value: dataItem.description, data: dataItem.description };
			//  //            })
			//  //        };
			//  //    }
			//  onSelect: function(suggestion){
			//  	debugger;
			//  }
			// });
		}
	});
	return CreateContragentView
})
