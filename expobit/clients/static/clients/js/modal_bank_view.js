define(['text!clients/html/entitys_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#modal_bank_view')[0].outerHTML,
		events: {
		},
		templateContext: function(){
			if(this.model){
				return {main: this.model.get('Ref_Key') == this.options.guid_contr_agent, owner: this.options.owner};
			}
			return {owner: this.options.owner};
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.SelectBank = this.$el.find('#SelectBank').selectize({
				valueField: 'Ref_Key',
				labelField: 'Description',
				searchField: ['Description', 'Code'],
				// options:[{'Ref_Key':'678', 'ИНН':'afa'}],
				loadingClass: 'loader',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/one_c/banks_org',
						success: function(response){
							var res = response.value;
							for(var i in res){
								_this.addOption(res[i]);
							}
							if(_view.model){
								_this.setValue(_view.model.get('Банк_Key'));
							}
						}
					});
				},
                render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeTemplateBank')[0].innerHTML)(item);
					}
				},
                onChange: function(value){
                }
			});
		}
	});
	return View;
});
