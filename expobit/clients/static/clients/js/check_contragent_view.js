define(['text!clients/html/entitys_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var CreateContragentView = Backbone.Marionette.View.extend({
		template:$(html).filter('#check_contragent_view')[0].outerHTML,
		events: {
			'click #CopyAddress': 'copyAddress'
		},
		templateContext: function(){
			if(this.model.get('load')){

			}
			// return {show_button: this.options.show_button}
		},
		copyAddress: function(){
			this.$el.find('input[name="Адрес"]').val(this.options.address);
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error');
				}
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				}
			}
			return valid;
		},
		initialize: function(){
		},
		load: function(input) {
                    var _view=this;
                    var response = $.get({
                        url: '/apiclients/searchtax/?query='+input,
                        async: true,
                        success: function(response) {
                        	var data = response.suggestions;
                        	for(i in data){
                        		data[i].data.value = data[i].value;
                        		data[i].data.value_address = data[i].data.address.value;
                        		// _this.addOption(data[i].data);
                        		// _this.setValue(data[i].value);
                        	}
                        	// _view.$el.find('input[name="ИНН"]').val(data[0].inn);
                	_view.model.set({
                		'update_inn': data[0].data.inn,
                		inn_error: data[0].data.inn != _view.model.get('ИНН'),
                		new_inn: data[0].data.inn
                	});
                	// _view.$el.find('input[name="НаименованиеПолное"]').val(data[0].value);
                	_view.model.set({
                		'update_value': data[0].value,
                		name_error: data[0].value != _view.model.get('НаименованиеПолное'),
                		new_name: data[0].value
                	});
                	// _view.$el.find('input[name="Адрес"]').val(data[0].data.address.value);
                	for(var i in _view.model.attributes['КонтактнаяИнформация']){
                		_view.model.attributes['КонтактнаяИнформация'][i]['address_error'] = data[0].data.value_address != _view.model.attributes['КонтактнаяИнформация'][i]['Представление'];
                		_view.model.attributes['КонтактнаяИнформация'][i]['new_address'] = data[0].data.address.value;
                	}
                	// _view.model.set({
                	// 	'update_address': data[0].data.address.value,
                	// 	address_error: false,
                	// 	new_address: data[0].data.value_address
                	// });
                	// _view.$el.find('input[name="КПП"]').val(data[0].data.kpp);
                	_view.model.set({
                		'update_kpp': data[0].data.kpp,
                		kpp_error: data[0].data.kpp != _view.model.get('КПП'),
                		new_kpp: data[0].data.kpp
                	});
                	_view.model.set('load', true);
                	_view.render();
                            // _this.open();
                            // return response
                        }
                    });
                },
		onRender: function(){
			var _view = this;
			if(!this.model.get('load')){
				this.load(this.model.get('ИНН'));	
			}
			// this.SelectContrAgent = this.$el.find('#SelectContrAgent').selectize({
			// 	valueField: 'hid',
			// 	labelField: 'value',
			// 	searchField: ['value', 'inn', 'value_address'],
			// 	// options:[{'Ref_Key':'678', 'ИНН':'afa'}],
			// 	loadingClass: 'loader',
			// 	onInitialize: function(){
			// 	},
			// 	load: function(input) {
   //                  var _this=this;
   //                  var response = $.get({
   //                      url: '/apiclients/searchtax/?query='+input,
   //                      async: true,
   //                      success: function(response) {
   //                      	_this.clearOptions();
   //                      	var data = response.suggestions;
   //                      	for(i in data){
   //                      		data[i].data.value = data[i].value;
   //                      		data[i].data.value_address = data[i].data.address.value;
   //                      		_this.addOption(data[i].data);
   //                      		_this.setValue(data[i].value);
   //                      	}
   //                          // _this.open();
   //                          // return response
   //                      }
   //                  });
   //              },
   //              render:{
			// 		option: function(item, escape){	
			// 			return Handlebars.compile($(html).filter('#selectizeTemplateNalog')[0].innerHTML)(item);
			// 		}
			// 	},
   //              onChange: function(value){
   //              	var data = this.options[value];
   //              	_view.$el.find('input[name="ИНН"]').val(data.inn);
   //              	_view.model.set({
   //              		'update_inn': data.inn,
   //              		inn_error: data.inn == _view.model.get('ИНН')
   //              	});
   //              	_view.$el.find('input[name="НаименованиеПолное"]').val(data.value);
   //              	_view.model.set({
   //              		'update_value': data.value,
   //              		name_error: data.value == model.get('НаименованиеПолное')
   //              	});
   //              	_view.$el.find('input[name="Адрес"]').val(data.address.value);
   //              	_view.model.set({
   //              		'update_address': data.address.value,
   //              		address_error: false
   //              	});
   //              	_view.$el.find('input[name="КПП"]').val(data.kpp);
   //              	_view.model.set({
   //              		'update_kpp': data.kpp,
   //              		kpp_error: data.kpp == _view.model.get('КПП')
   //              	});
   //              	_view.mode.set('load', true);
   //              	_view.render();
   //              }
			// });
			// this.$el.find('#SelectContrAgent-selectized').val(this.model.get('ИНН'));
		}
	});
	return CreateContragentView
})
