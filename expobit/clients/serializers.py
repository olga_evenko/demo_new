from rest_framework import serializers
from .models import *
from workers.serializers import *
from workers.models import *
from projects.serializers import *
from .client_project_serializers import *
from django.utils import timezone
from django.conf import settings
from one_c.serializers import NomenclatureSerializer


class PhonesSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        field1 = attrs.get('phone', self.object.phone)
        field2 = attrs.get('content_type', self.object.content_type)

        try:
            obj = PhoneItem.objects.get(phone=field1, content_type=field2)
        except:
            return attrs
        if self.object and obj.id == self.object.id:
            return attrs
        else:
            raise serializers.ValidationError('field1 with field2 already exists')
    class Meta:
        model = PhoneItem
        fields = '__all__'


class EmailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailItem
        fields = '__all__'


# Контактные лица
class ContactPersonSerializer(serializers.ModelSerializer):
    phones = PhonesSerializer(many=True, read_only=True)
    emails = EmailsSerializer(many=True, read_only=True)

    class Meta:
        model = ContactPerson
        fields = '__all__'


# class EntitySerializer(serializers.ModelSerializer):
#     activities = ActivitiesSerializer(many=True, read_only=True)
#
#     class Meta:
#         model = Entity
#         fields = '__all__'


# class EntitySerializerForEntityPrivate(serializers.ModelSerializer):
#     activities = ActivitiesSerializer(many=True)
#
#     class Meta:
#         model = Entity
#         fields = '__all__'
class CustomDateTimeField(serializers.DateTimeField):
    def to_representation(self, value):
        tz = timezone.get_default_timezone()
        # timezone.localtime() defaults to the current tz, you only
        # need the `tz` arg if the current tz != default tz
        value = timezone.localtime(value, timezone=tz)
        # py3 notation below, for py2 do:
        # return super(CustomDateTimeField, self).to_representation(value)
        return super().to_representation(value)


class ClientActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientActivity
        fields = "__all__"


class ClientsSerializer(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField('get_managers')
    change_activities = CustomDateTimeField(default=None)
    date_last_contact = CustomDateTimeField(default=None)
    date_created = CustomDateTimeField(read_only=True)
    date_updated = CustomDateTimeField(read_only=True)
    group = serializers.SerializerMethodField()
    created_user_name = serializers.SerializerMethodField(read_only=True)
    edited_user_name = serializers.SerializerMethodField(read_only=True)
    emails = serializers.SerializerMethodField(read_only=True)

    def get_emails(self, obj):
        return ','.join([x for x in obj.contacts_person.all().values_list('emails__email', flat=True) if x])

    def get_created_user_name(self, obj):
        return '%s %s' % (obj.created_user.last_name, obj.created_user.first_name)

    def get_edited_user_name(self, obj):
        if obj.edited_user:
            return '%s %s' % (obj.edited_user.last_name, obj.edited_user.first_name)
        return None

    def get_group(self, obj):
        if self.context['request'].GET.get('project_id'):
            if not self.__dict__.get('subject_id'):
                project = Project.objects.get(pk=self.context['request'].GET.get('project_id'))
                self.__dict__['subject_id'] = project.subjects_id
            qs = ClientGroup.objects.filter(subjects=self.__dict__.get('subject_id'), client=obj)[:1]
            if qs:
                return qs[0].group
            return None
        else:
            None

    def get_managers(self, client):
        project_id = self.context.get('request').GET.get('project_id')
        qs = Membership.objects.filter(project=project_id, client=client)
        serializer = MemberShipSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = Client
        fields = "__all__"
        # fields = ['id', 'manager', 'group', 'date_last_contact', 'change_activities', 'name', 'address']


class ClientProjectSectionSerializer(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField('get_managers')
    change_activities = CustomDateTimeField(default=None)
    date_last_contact = CustomDateTimeField(default=None)
    group = serializers.SerializerMethodField()

    def get_group(self, obj):
        if hasattr(obj, 'group_name'):
            return obj.group_name
        if self.context['request'].GET.get('project_id'):
            if not self.__dict__.get('subject_id'):
                project = Project.objects.get(pk=self.context['request'].GET.get('project_id'))
                self.__dict__['subject_id'] = project.subjects_id
            qs = ClientGroup.objects.filter(subjects=self.__dict__.get('subject_id'), client=obj)[:1]
            if qs:
                return qs[0].group
            return None
        else:
            None

    def get_managers(self, client):
        if hasattr(client, 'manager'):
            if len(client.manager) > 0:
                return [MemberShipSerializer(instance=client.manager[0]).data]
            return []
        project_id = self.context.get('request').GET.get('project_id')
        qs = Membership.objects.filter(project=project_id, client=client)
        serializer = MemberShipSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = Client
        fields = ['id', 'manager', 'group', 'date_last_contact', 'change_activities', 'name', 'address']


class ClientsSerializerMin(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'name']

# Dodelat'
class ClientsSerializerSearch(serializers.ModelSerializer):
    phones = serializers.SerializerMethodField()

    def get_phones(self, obj):
        phones_list = obj.contacts_person.filter(active=True)
        arr = []
        for i in phones_list:
            phones = i.phones.all().values_list('phone')
            for j in phones:
                arr.append(j[0])
        return ', '.join(arr)

    class Meta:
        model = Client
        fields = ['id', 'name', 'phones']


class EntityPrivateSerializerForClients(serializers.ModelSerializer):
    activities = serializers.SerializerMethodField()

    def get_activities(self, entity):
        qs = entity.activities.all()
        return ", ".join(str(item.subcategories) for item in qs)

    class Meta:
        model = Entity
        fields = ['full_name', 'id', 'address', 'activities']


class ClientsSerializerForPk(serializers.ModelSerializer):
    edit = serializers.SerializerMethodField(read_only=True)
    projects = serializers.SerializerMethodField()
    entitys = EntityPrivateSerializerForClients(many=True, read_only=True)
    contacts_person = ContactPersonSerializer(many=True, read_only=True)
    activities = serializers.SerializerMethodField()
    client_activities_list = serializers.SerializerMethodField(read_only=True)

    def get_edit(self, obj):
        # TODO Если что добавить сортировку еще по статутсу проекта, например кроме завершенных(или в работе)
        # self.__dict__['manager_id'] = None
        # if not self.__dict__.get('group_names'):
        groups = self.context.get('request').user.groups.all().values('name')
        groups = [x['name'] for x in groups]
        # self.__dict__['group_names'] = [x['name'] for x in groups]
        if 'managers' in groups:
            manager_id = self.context.get('request').user.managers.id
            return Membership.objects.filter(manager_id=manager_id, client_id=obj.id).exists() or \
                   self.context.get('request').user.id == obj.created_user_id
        # if 'kvs' in groups and 'kvs_head' not in groups:
        #     manager_id = self.context.get('request').user.managers.id
        #     return obj.personal_manager_type_one_id == manager_id
        if 'ad' in groups or 'head_ad' in groups:
            manager_id = self.context.get('request').user.managers.id
            return obj.visitor_manager_id == manager_id
        return True

    def get_client_activities_list(self, obj):
        act = obj.client_activitys.all().values('activity')
        act = [x['activity'] for x in act]
        return ', '.join(act)

    def get_activities(self, obj):
        qs = Activities.objects.filter(entitys__client=obj).values('id')
        arr = []
        for i in qs:
            arr.append(i['id'])
        return arr
    # individuals = serializers.SerializerMethodField()
    #
    # def get_individuals(self, client):
    #     arr = []
    #     qs = client.entitys.all()
    #     for i in qs:
    #         for j in i.individual.all():
    #             arr.append(j.id)
    #     return IndividualsSerializer(instance=Individual.objects.filter(id__in=arr), many=True).data

    def get_projects(self, client):
        return ProjectTableSerializer(many=True, instance=Membership.objects.filter(client=client),
                                      context={'client': client}).data

    class Meta:
        model = Client
        fields = "__all__"


class IndEntPrShipSerializerForChange(serializers.ModelSerializer):
    class Meta:
        model = IndEntPrShip
        fields = "__all__"


class IndEntPrShipSerializer(serializers.HyperlinkedModelSerializer):
    individual = serializers.SerializerMethodField()
    individual_id = serializers.SerializerMethodField()
    phone = serializers.ReadOnlyField(source='individual.phone')
    entity_id = serializers.ReadOnlyField(source='entity_id.id')
    full_name = serializers.ReadOnlyField(source='entity_id.full_name')
    activities = serializers.SerializerMethodField(read_only=True)

    def get_individual_id(self, obj):
        return obj.individual.id

    def get_individual(self, obj):
        return obj.individual.name+' '+obj.individual.surname

    def get_activities(self, obj):
        qs = obj.entity_id.activities.all()
        actArr = []
        for i in qs:
            actArr.append(i.subcategories)
        return ', '.join(actArr)

    class Meta:
        model = IndEntPrShip
        fields = ['id', 'entity_id', 'full_name', 'type_position', 'position', 'activities', 'individual',
                  'phone', 'main', 'individual_id']


class EntityPrivateSerializerMax(serializers.ModelSerializer):
    activities = ActivitiesSerializer(many=True)

    class Meta:
        model = Entity
        fields = "__all__"


class IndividualsSerializer(serializers.ModelSerializer):
    entity_private = IndEntPrShipSerializer(source='indentprship_set', many=True, read_only=True)
    phones = PhonesSerializer(many=True, read_only=True)
    emails = EmailsSerializer(many=True, read_only=True)
    # entityprivate = EntityPrivateSerializerMax(many=True, read_only=True)

    class Meta:
        model = Individual
        fields = "__all__"


class IndividualSerializerMin(serializers.ModelSerializer):
    date_created = CustomDateTimeField(read_only=True)
    date_edited = CustomDateTimeField(read_only=True)
    phones = PhonesSerializer(many=True, read_only=True)
    emails = EmailsSerializer(many=True, read_only=True)
    main_entity = serializers.SerializerMethodField()

    def get_main_entity(self, obj):
        qs = getattr(obj, 'position', None)
        if qs:
            return {'position': qs[0].position, 'ent_name': qs[0].ent_name}
        return None

    class Meta:
        model = Individual
        fields = "__all__"


class EntityPrivateSerializer(serializers.ModelSerializer):
    date_created = CustomDateTimeField(read_only=True)
    date_edited = CustomDateTimeField(read_only=True)
    phones = PhonesSerializer(many=True, read_only=True)
    emails = EmailsSerializer(many=True, read_only=True)

    class Meta:
        model = Entity
        fields = "__all__"


class EntityPrivateSerializerForPk(serializers.ModelSerializer):
    entity_private = IndEntPrShipSerializer(source='indentprship_set', many=True, read_only=True)
    phones = PhonesSerializer(many=True, read_only=True)
    emails = EmailsSerializer(many=True, read_only=True)

    class Meta:
        model = Entity
        fields = "__all__"


class EntityPrivateSerializerMin(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ['full_name', 'id']

# Контакты ################################################





class ContactsSerializerRO(serializers.ModelSerializer):
    contact_person = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()
    type_contact = serializers.SerializerMethodField()
    date = CustomDateTimeField()
    audio_url = serializers.SerializerMethodField()

    def get_audio_url(self, contact):
        if not self.context.get('request').user.is_superuser:
            if contact.manager_id not in self.context.get('manager_list'):
                return None
        if not hasattr(settings, 'AUDIO_URL') or '@' in contact.contact:
            return None
        if contact.guid_audio:
            return '%s%s.ogg' % (settings.AUDIO_URL, contact.guid_audio)

    def get_manager(self, contact):
        user = contact.manager.user
        return user.first_name+' '+user.last_name

    def get_project(self, contact):
        if contact.project_id:
            return contact.project.name
        return 'Работа с базой клиентов'

    def get_contact_person(self, contact):
        if contact.contact_person:
            return contact.contact_person.name
        return contact.contact_person_name

    def get_type_contact(self, contact):
        return contact.get_type_contact_display()

    class Meta:
        model = Contact
        fields = '__all__'


class ContactsSerializer(serializers.ModelSerializer):
    date = CustomDateTimeField()

    def validate(self, attrs):
        if not self.context['request'].user.groups.filter(name='kvs').exists() and not attrs.get('project'):
            raise serializers.ValidationError("Поле project обязательно")
        return attrs

    class Meta:
        model = Contact
        exclude = ['manager']


class ContactOldSerializer(serializers.ModelSerializer):
    created_user = serializers.SerializerMethodField()

    def get_created_user(self, obj):
        return '%s %s' % (obj.created_user.last_name, obj.created_user.first_name)

    class Meta:
        model = ContactOld
        fields = '__all__'


class NewContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewContact
        fields = '__all__'


class IndividualContactsSerializerRO(serializers.ModelSerializer):
    project = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()
    type_contact = serializers.SerializerMethodField()
    source_of_information = serializers.SerializerMethodField()
    date = CustomDateTimeField()

    def get_source_of_information(self, contact):
        return contact.get_source_of_information_display()

    def get_manager(self, contact):
        return contact.manager.user.first_name + ' ' + contact.manager.user.last_name

    def get_project(self, contact):
        return contact.project.name

    def get_contact_person(self, contact):
        return contact.contact_person.name

    def get_type_contact(self, contact):
        return contact.get_type_contact_display()

    class Meta:
        model = IndividualContact
        fields = '__all__'


class IndividualContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndividualContact
        exclude = ['manager']


class NewIndividualContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewIndividualContact
        fields = '__all__'


# Закаанные услуги
class OrderedServiceSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField()

    def get_total(self, ordered_service):
        return ordered_service.count * ordered_service.price


class OrderedServiceExpoSerializer(OrderedServiceSerializer):
    class Meta:
        model = OrderedServiceExpo
        fields = '__all__'


class OrderedServiceAdSerializer(OrderedServiceSerializer):
    class Meta:
        model = OrderedServiceAd
        fields = '__all__'


class OrderedServiceLeaseKVSSerializer(OrderedServiceSerializer):
    class Meta:
        model = OrderedServiceLeaseKVS
        fields = '__all__'


class OrderedServiceSerializerRO(serializers.ModelSerializer):
    total = serializers.SerializerMethodField()
    # nomenclature = serializers.SerializerMethodField()
    # nomenclature_guid = serializers.SerializerMethodField()

    # def get_nomenclature(self, obj):
    #     return obj.nomenclature.nomenclature_name

    # def get_nomenclature_guid(self, obj):
    #     return obj.nomenclature.guid

    def get_total(self, ordered_service):
        if ordered_service.discount_rate:
            return (ordered_service.count * ordered_service.price * ordered_service.count_days)*(100 - ordered_service.discount_rate)/100
        if ordered_service.markup:
            return (ordered_service.count * ordered_service.price * ordered_service.count_days)*(100 + ordered_service.markup)/100
        return ordered_service.count * ordered_service.price * ordered_service.count_days


class OrderedServiceExpoSerializerRO(OrderedServiceSerializerRO):
    class Meta:
        model = OrderedServiceExpo
        fields = '__all__'


class OrderedServiceAdSerializerRO(OrderedServiceSerializerRO):
    class Meta:
        model = OrderedServiceAd
        fields = '__all__'


class OrderedServiceLeaseKVSSerializerRO(OrderedServiceSerializerRO):
    class Meta:
        model = OrderedServiceLeaseKVS
        fields = '__all__'


# Заявки на аренду
class ApplicationExpoSerializer(serializers.ModelSerializer):
    sites = serializers.SerializerMethodField(read_only=True)
    entity_guid_one_c = serializers.SerializerMethodField(read_only=True)
    current_date = serializers.SerializerMethodField(read_only=True)
    main = serializers.SerializerMethodField()
    child_apps = serializers.SerializerMethodField()
    max_discount = serializers.IntegerField(read_only=True)
    thematic = serializers.SerializerMethodField(read_only=True)
    contracts = serializers.SerializerMethodField(read_only=True)
    plannings = serializers.SerializerMethodField(read_only=True)

    def get_plannings(self, obj):
        return [{'planning': x['planning'], 'planning_project': x['id'], 'id': x['id']}
                for x in obj.project.plannings.all().values('id', 'planning')]

    def get_contracts(self, obj):
        if obj.project:
            arr = []
            c = obj.project.subjects.contracts.all()
            if not c:
                c = obj.project.buisness_unit.contracts.all()
            if c:
                for i in c:
                    arr.append({
                        'id': i.id,
                        'name': i.name,
                        'subject': i.subject_id,
                        'business_unit': i.business_unit_id,
                        'type_contracts': [x for x in i.type_contracts.all().values()]
                    })
                return arr
            return None

    def get_thematic(self, obj):
        if obj.project:
            return obj.project.subjects.subject_name
        return None

    def get_child_apps(self, obj):
        return [{'id': x['id'], 'date': x['date'].strftime("%d.%m.%Y %H:%M:%S")} for x in obj.child_apps.all().values('id', 'date')]

    def get_main(self, obj):
        if obj.main:
            return {'date': obj.main.date.strftime("%d.%m.%Y %H:%M:%S"), 'id': obj.main_id}
        return None

    def get_current_date(self, obj):
        tz = timezone.get_default_timezone()
        if not obj.date.tzinfo:
            value = tz.localize(obj.date)
            return value
        value = timezone.localtime(obj.date, timezone=tz)
        return value

    def get_entity_guid_one_c(self, obj):
        if obj.entity_id:
            return obj.entity.one_c_guid
        return None

    def get_sites(self, app):
        sites = app.project.sites.all()
        arr = []
        for i in sites:
            arr.append({'id': i.id, 'name': i.name})
        return arr

    class Meta:
        model = ApplicationExpo
        fields = '__all__'


class ApplicationAdSerializer(serializers.ModelSerializer):
    entity_guid_one_c = serializers.SerializerMethodField(read_only=True)
    current_date = serializers.SerializerMethodField(read_only=True)
    main = serializers.SerializerMethodField()
    child_apps = serializers.SerializerMethodField()

    def get_child_apps(self, obj):
        return [{'id': x['id'], 'date': x['date'].strftime("%d.%m.%Y %H:%M:%S")} for x in
                obj.child_apps.all().values('id', 'date')]

    def get_main(self, obj):
        if obj.main:
            return {'date': obj.main.date.strftime("%d.%m.%Y %H:%M:%S"), 'id': obj.main_id}
        return None

    def get_current_date(self, obj):
        tz = timezone.get_default_timezone()
        if not obj.date.tzinfo:
            value = tz.localize(obj.date)
            return value
        value = timezone.localtime(obj.date, timezone=tz)
        return value

    def get_entity_guid_one_c(self, obj):
        if obj.entity_id:
            return obj.entity.one_c_guid
        return None

    class Meta:
        model = ApplicationAd
        fields = '__all__'


class ApplicationLeaseKVSSerializer(serializers.ModelSerializer):
    entity_guid_one_c = serializers.SerializerMethodField(read_only=True)
    current_date = serializers.SerializerMethodField(read_only=True)
    main = serializers.SerializerMethodField()
    child_apps = serializers.SerializerMethodField()

    def get_child_apps(self, obj):
        return [{'id': x['id'], 'date': x['date'].strftime("%d.%m.%Y %H:%M:%S")} for x in
                obj.child_apps.all().values('id', 'date')]

    def get_main(self, obj):
        if obj.main:
            return {'date': obj.main.date.strftime("%d.%m.%Y %H:%M:%S"), 'id': obj.main_id}
        return None

    def get_current_date(self, obj):
        tz = timezone.get_default_timezone()
        value = timezone.localtime(obj.date, timezone=tz)
        return value

    def get_entity_guid_one_c(self, obj):
        if obj.entity_id:
            return obj.entity.one_c_guid
        return None

    class Meta:
        model = ApplicationLeaseKVS
        fields = '__all__'


class ApplicationExpoSerializerTable(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    date = CustomDateTimeField()
    status = serializers.SerializerMethodField()
    entity = serializers.SerializerMethodField()
    business_unit = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()
    max_discount = serializers.IntegerField(read_only=True)

    def get_address(self, obj):
        return obj.client.address

    def get_total(self, app):
        qs = app.ordered_service.all()
        s = 0
        barter_sum = 0
        if app.barter_sum:
            barter_sum = app.barter_sum
        for i in qs:
            s += (i.count * i.price)*(1+(i.markup/100))*(1-(i.discount_rate/100))
        return round(s - barter_sum, 2)

    def get_business_unit(self, app):
        if app.project.buisness_unit:
            return app.project.buisness_unit.name
        else:
            return ''

    def get_entity(self, app):
        if app.entity_id:
            return app.entity.full_name
        return None

    def get_status(self, app):
        return app.get_status_display()

    def get_project(self, app):
        return app.project.name

    def get_manager(self, app):
        return app.manager.user.first_name+' '+app.manager.user.last_name

    def get_client(self, app):
        return app.client.name

    class Meta:
        model = ApplicationExpo
        fields = '__all__'


class ApplicationAdSerializerTable(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    date = CustomDateTimeField()
    status = serializers.SerializerMethodField()
    entity = serializers.SerializerMethodField()
    business_unit = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()

    def get_address(self, obj):
        return obj.client.address

    def get_total(self, app):
        qs = app.ordered_service.all()
        s = 0
        barter_sum = 0
        if app.barter_sum:
            barter_sum = app.barter_sum
        for i in qs:
            s += (i.count * i.price)*(1+(i.markup/100))*(1-(i.discount_rate/100))
        return round(s - barter_sum, 2)

    def get_business_unit(self, app):
        return app.project.buisness_unit.name

    def get_entity(self, app):
        if app.entity:
            return app.entity.full_name
        return None

    def get_status(self, app):
        return app.get_status_display()

    def get_project(self, app):
        return app.project.name

    def get_manager(self, app):
        return app.manager.user.first_name + ' ' + app.manager.user.last_name

    def get_client(self, app):
        return app.client.name

    class Meta:
        model = ApplicationAd
        fields = '__all__'


class PayAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayApp
        fields = "__all__"


class ApplicationLeaseKVSSerializerTable(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    date = CustomDateTimeField()
    status = serializers.SerializerMethodField()
    entity = serializers.SerializerMethodField()
    business_unit = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()

    def get_address(self, obj):
        return obj.client.address

    def get_total(self, app):
        barter_sum = app.barter_sum
        if not barter_sum:
            barter_sum = 0
        qs = app.ordered_service.all()
        s = 0
        for i in qs:
            s += (i.count * i.price * i.count_days) * (1 + (i.markup / 100)) * (1 - (i.discount_rate / 100))
        return round(s - barter_sum)

    def get_business_unit(self, app):
        return app.project.buisness_unit.name

    def get_entity(self, app):
        if app.entity:
            return app.entity.full_name
        return None

    def get_status(self, app):
        return app.get_status_display()

    def get_project(self, app):
        return app.project.name

    def get_manager(self, app):
        return app.manager.user.first_name + ' ' + app.manager.user.last_name

    def get_client(self, app):
        return app.client.name

    class Meta:
        model = ApplicationLeaseKVS
        fields = '__all__'


class ClientsGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientGroup
        fields = '__all__'


class OtherInterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = OtherInterest
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class DiplomaNominationSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiplomaNomination
        fields = '__all__'


class RegionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'