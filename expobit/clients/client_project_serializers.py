from projects.serializers import *
from workers.models import *
from .models import ClientGroup


class ProjectTableSerializer(serializers.ModelSerializer):
    project_type = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    group = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()

    def get_manager(self, membership):
        return {'id': membership.manager.id,
                'name': '%s %s' % (membership.manager.user.last_name, membership.manager.user.first_name)}

    def get_group(self, membership):
        qs = ClientGroup.objects.filter(subjects=membership.project.subjects, client=membership.client)
        if qs:
            return qs[0].group
        return None

    def get_project(self, membership):
        return {'name': membership.project.name, 'id': membership.project.id, 'user_id': membership.project.user_id}

    def get_project_type(self, membership):
        if membership.project.project_type:
            return membership.project.project_type.type
        else:
            return ''

    class Meta:
        model = Membership
        fields = ['id', 'project_type', 'project', 'group', 'manager']