import django_filters
from .models import *


class EntityFilter(django_filters.FilterSet):
    client = django_filters.AllValuesFilter(name='client')
    no_client = django_filters.BooleanFilter(name='client', lookup_expr='isnull')

    class Meta:
        model = Entity
        fields = ['client']


class ListFilter(django_filters.Filter):
    def filter(self, qs, value):
        if not value:
            return qs
        self.lookup_expr = 'in'
        values = value.split(',')
        return super(ListFilter, self).filter(qs, values)


class AppFilter(django_filters.FilterSet):
    project = ListFilter(name='project')
    date__lt = django_filters.DateFilter(name='date', lookup_expr='lt')
    date__gt = django_filters.DateFilter(name='date', lookup_expr='gt')
    date__contain = django_filters.DateFilter(name='date')
    status = django_filters.CharFilter(name='status')
    # total__lt = django_filters.Filter(name='total', lookup_expr='lt')
    # total__gt = django_filters.Filter(name='total', lookup_expr='gt')
    # total__contain = django_filters.Filter(name='total')
    project__user = django_filters.Filter(name='project__user')
    project__project_type__director = django_filters.Filter(name='project__project_type__director')
    client = django_filters.Filter(name='client')
    manager = django_filters.Filter(name='manager')

    class Meta:
        model = Application
        fields = ['project', 'date__lt', 'date__gt', 'status', 'client']


# def get_all_subordinates(user):
#     qs = user.my_managers.all()
#     if len(qs) < 0:
#         return []
#     result = []
#     for i in qs:
