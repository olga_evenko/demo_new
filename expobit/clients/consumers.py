from channels import Group
from channels.sessions import channel_session
import datetime
from urllib.parse import parse_qs
from django.conf import settings
from channels.auth import channel_session_user, channel_session_user_from_http
from workers.models import PushMessage, Dialog, Notification
from django.contrib.auth.models import User
from django.db.models import Q
from workers.serializers import PushMessageSerializer
from projects.models import Project
import json
from django.core.cache import cache
cache.set('count_users'+settings.MEMCACHED_NAME, 0, None)
cache.set('count_projects'+settings.MEMCACHED_NAME,
          Project.objects.filter(status='working', date_start__lte=datetime.date.today(),
                                 date_end__gte=datetime.date.today()).count(), None)
cache.set('count_clients'+settings.MEMCACHED_NAME, 0, None)
cache.set('count_applications'+settings.MEMCACHED_NAME, 0, None)
cache.set('count_contacts'+settings.MEMCACHED_NAME, 0, None)
cache.set('max_users'+settings.MEMCACHED_NAME, 0, None)
cache.set('active_users'+settings.MEMCACHED_NAME, {}, None)
cache.set('block_working'+settings.MEMCACHED_NAME, False, None)
#
#


def send_email(message):
    data = message.content
    user = data['user']
    user = User.objects.get(pk=user)
    user.email_user(subject=data['subject'], message='', html_message=data['message'])


def stop_block(message):
    data = message.content
    if data.get('clear'):
        Group('all_users').send(
            {'text': json.dumps({'clear': True, 'function': 'stop_block'})})
        return
    Group('all_users').send(
        {'text': json.dumps({'function': 'stop_block'})})


def send_notification(message):
    data = message.content
    if data.get('block_working'):
        cache.set('block_working' + settings.MEMCACHED_NAME, True, None)
        Group('all_users').send(
            {'text': json.dumps({'time': data['time'],
                                 'message': data['message'],
                                 'function': 'set_block'})})
    else:
        if data['users'] == 'all':
            create_notifications = [Notification(user=x, message=data['message']) for x in User.objects.all()]
            Notification.objects.bulk_create(create_notifications)
            Group('all_users').send(
                {'text': json.dumps({'message': data['message'],
                                     'function': 'show_notification'})})
        else:
            create_notifications = [Notification(user=x, message=data['message']) for x in User.objects.filter(id__in=data['users'].split(','))]
            Notification.objects.bulk_create(create_notifications)
            for i in data['users'].split(','):
                Group("user_%s" % User.objects.get(pk=i).username).send({
                    "text": json.dumps({
                        'message': data['message'],
                        'function': 'show_notification'
                    }),
                })


def update_counters(message):
    Group('all_users').send({'text': json.dumps({'count_projects': cache.get('count_projects'+settings.MEMCACHED_NAME),
                                                 'count_clients': cache.get('count_clients'+settings.MEMCACHED_NAME),
                                                 'count_applications': cache.get('count_applications'+settings.MEMCACHED_NAME),
                                                 'count_contacts': cache.get('count_contacts'+settings.MEMCACHED_NAME),
                                                 'function': 'update_counters'})})


def msg_consumer(message):
    # Save to model
    user = message.content['user']
    sender = message.content['sender']
    username = message.content['username']
    sender = User.objects.get(pk=sender)
    user = User.objects.get(pk=user)
    dialog = message.content.get('dialog')
    qs_dialog = Dialog.objects.filter(
        Q(user_one=user.id, user_two=sender.id) | Q(user_one=sender.id, user_two=user.id))[:1]
    if not qs_dialog:
        try:
            qs_dialog = Dialog.objects.create(user_one_id=user.id, user_two_id=sender.id)
        except:
            qs_dialog = Dialog.objects.filter(
                Q(user_one=user.id, user_two=sender.id) | Q(user_one=sender.id, user_two=user.id))[:1][0]
        qs_dialog = [qs_dialog]
    qs = PushMessage.objects.create(
        user=user,
        push=message.content['push'],
        message=message.content['message'],
        sender=sender,
        dialog=qs_dialog[0]
    )

    message = PushMessageSerializer(instance=qs).data
    # Broadcast to listening sockets
    if dialog:
        message['sender_return'] = False
        message['function'] = 'message'
        Group("user_%s" % username).send({
            "text": json.dumps(message),

        })
        # sender_return = PushMessageSerializer(instance=qs).data
        message['sender_return'] = True
        Group("user_%s" % sender.username).send({
            "text": json.dumps(message),

        })
    else:
        message = PushMessageSerializer(instance=qs).data
        message['function'] = 'message'
        message['sender_return'] = False
        Group("user_%s" % username).send({
            "text": json.dumps(message),

        })
        # Group("user_%s" % username).send({
        #     "text": json.dumps({'message': message.content['push']})
        # })

@channel_session_user_from_http
def ws_connect(message):
    print('conn')
    if not message.user.is_authenticated:
        message.reply_channel.send({"close": True})
        return
    # with open('log.txt', 'a') as f:
    #     f.write(str(message.content['client'])+ ' '+ str(message.user.is_authenticated)+'\n')
    # print(message.content['client'], ' ', message.user.is_authenticated)
    cache.set(message.reply_channel.name + settings.MEMCACHED_NAME, message.user.id, None)
    message.reply_channel.send({"accept": True})
    params = parse_qs(message.content["query_string"])
    cache.incr('count_users' + settings.MEMCACHED_NAME, 1, None)
    active_users = cache.get('active_users'+settings.MEMCACHED_NAME)
    if active_users.get(message.user.id):
        active_users[message.user.id]['counter'] += 1
        active_users[message.user.id]['reply_channel'][message.reply_channel.name] = {
                    'open_tab': True
                }
    else:
        active_users[message.user.id] = {
            'counter': 1,
            'reply_channel': {
                message.reply_channel.name: {
                    'open_tab': True,
                    'href': ''
                }
            }
        }
    cache.set('active_users'+settings.MEMCACHED_NAME, active_users, None)
    if message.user.is_authenticated:
        print('conn_auth')
        # message.channel_session["username"] = message.user.username
        if cache.get('max_users'+settings.MEMCACHED_NAME) < cache.get('count_users'+settings.MEMCACHED_NAME):
            cache.set('max_users'+settings.MEMCACHED_NAME, cache.get('count_users'+settings.MEMCACHED_NAME), None)
        Group('user_%s' % message.user.username).add(message.reply_channel)
        if message.user.is_superuser:
            Group('administrators').add(message.reply_channel)
        Group('administrators').send({'text': json.dumps({
            'user': active_users[message.user.id],
            'type': 'con',
            'name': '%s %s' % (message.user.last_name, message.user.first_name),
            'id': message.user.id,
            'function': 'update_user_table'
        })})
        Group('all_users').add(message.reply_channel)
        Group('all_users').send({'text': json.dumps({'count_user': len(cache.get('active_users'+settings.MEMCACHED_NAME)),
                                                     'count_projects': cache.get('count_projects'+settings.MEMCACHED_NAME),
                                                     'count_clients': cache.get('count_clients'+settings.MEMCACHED_NAME),
                                                     'count_applications': cache.get('count_applications'+settings.MEMCACHED_NAME),
                                                     'count_contacts': cache.get('count_contacts'+settings.MEMCACHED_NAME),
                                                     'function': 'real_time_data'})})
    else:
        message.reply_channel.send({"close": True})

@channel_session_user
def ws_disconnect(message):
    print('dis')
    user_id = message.user.id
    if not message.user.is_authenticated:
        if cache.get(message.reply_channel.name + settings.MEMCACHED_NAME):
            user_id = cache.get(message.reply_channel.name + settings.MEMCACHED_NAME)
        else:
            return
    if message.user.is_superuser:
        Group('administrators').discard(message.reply_channel)
    Group('user_%s' % message.user.username).discard(message.reply_channel)
    Group('all_users').discard(message.reply_channel)
    active_users = cache.get('active_users' + settings.MEMCACHED_NAME)
    if active_users.get(user_id):
        if active_users[user_id]['counter'] == 1:
            del active_users[user_id]
        else:
            active_users[user_id]['counter'] -= 1
            del active_users[user_id]['reply_channel'][message.reply_channel.name]
    cache.set('active_users' + settings.MEMCACHED_NAME, active_users, None)
    cache.incr('count_users'+settings.MEMCACHED_NAME, -1, None)
    Group('administrators').send({'text': json.dumps({
        'user': active_users.get(user_id),
        'id': user_id,
        'type': 'dis',
        'function': 'update_user_table'
    })})
    Group('all_users').send({'text': json.dumps({'count_user': len(cache.get('active_users' + settings.MEMCACHED_NAME)),
                                                 'function': 'real_time_data'})})
    # Group('users').discard(message.reply_channel)

@channel_session_user
def ws_receive(message):
    if not message.user.is_authenticated:
        return
    data = json.loads(message.content['text'])
    if data.get('href'):
        active_users = cache.get('active_users' + settings.MEMCACHED_NAME)
        try:
            active_users[message.user.id]['reply_channel'][message.reply_channel.name]['href'] = data.get('href')
        except:
            print(1)
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['active_time'] = data.get('active_time')
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['counter_hash'] = data.get('counter_hash')
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['deactive_time'] = data.get('deactive_time')
    if data.get('active_tab') == False:
        active_users = cache.get('active_users'+settings.MEMCACHED_NAME)
        try:
            active_users[message.user.id]['reply_channel'][message.reply_channel.name]['open_tab'] = False
        except:
            print(1)
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['active_time'] = data.get('active_time')
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['counter_hash'] = data.get('counter_hash')
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['deactive_time'] = data.get('deactive_time')
    elif data.get('active_tab') == True:
        active_users = cache.get('active_users'+settings.MEMCACHED_NAME)
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['open_tab'] = True
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['active_time'] = data.get('active_time')
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['counter_hash'] = data.get('counter_hash')
        active_users[message.user.id]['reply_channel'][message.reply_channel.name]['deactive_time'] = data.get('deactive_time')
    cache.set('active_users'+settings.MEMCACHED_NAME, active_users, None)
    Group('administrators').send({'text': json.dumps({
        'user': active_users[message.user.id],
        'type': 'con',
        'name': '%s %s' % (message.user.last_name, message.user.first_name),
        'id': message.user.id,
        'function': 'update_user_table'
    })})
    # Group('users').send({
    #     "text": message.content['text'],
    # })
    # message.reply_channel.send({
    #     "text": message.content['text'],
    # })
