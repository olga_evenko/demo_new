from rest_framework.permissions import BasePermission


class AppPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.method == 'POST' or request.method == 'PATCH':
            if request.data.get('entity'):
                return True
            else:
                return False
        return True


class ContactPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            if request.data.get('type_contact') != 'meet':
                if request.data.get('contact') == '':
                    return False
                else:
                    return True
            return True
        return True
