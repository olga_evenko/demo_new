from django.conf.urls import url, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'clients', ApiSetClientsView, 'Clients')
router.register(r'individuals', ApiSetIndividualsView, 'Individuals')
router.register(r'entity_private', ApiSetEntityPrivateView, 'EntityPrivate')
# router.register(r'entity', ApiSetEntityView, 'Entity')
router.register(r'individual_member', ApiSetIndEntPrShipView, 'IndEntPrShip')
router.register(r'contacts', ApiSetContactsView, 'ApiSetContactsView')
router.register(r'contacts_old', ApiSetContactsOldView, 'ApiSetContactsOldView')
router.register(r'new_contacts', ApiSetNewContactsView, 'ApiSetNewContactsView')
router.register(r'individual_contacts', ApiSetIndividualContactsView, 'ApiSetIndividualContactsView')
router.register(r'individual_new_contacts', ApiSetIndividualNewContactsView, 'ApiSetIndividualNewContactsView')
router.register(r'ordered_services_expo', ApiSetOrderedServiceExpoView, 'ApiSetOrderedServiceView')
router.register(r'ordered_services_ad', ApiSetOrderedServiceAdView, 'ApiSetOrderedServiceAdView')
router.register(r'ordered_services_lease_kvs', ApiSetOrderedServiceLeaseKVSView, 'ApiSetOrderedServiceLeaseKVSView')
router.register(r'application_expo', ApiSetApplicationExpoView, 'ApiSetApplicationExpoView')
router.register(r'application_ad', ApiSetApplicationAdView, 'ApiSetApplicationAdView')
router.register(r'application_lease_kvs', ApiSetApplicationLeaseKVSView, 'ApiSetApplicationLeaseKVSView')
router.register(r'pay_app', ApiSetPayAppView, 'ApiSetPayAppViewW')
router.register(r'contact_person', ApiSetContactPersonView, 'ApiSetContactPersonView')
router.register(r'clients_group', ApiSetClientsGroupView, 'ApiSetClientsGroupView')
router.register(r'project_table', ApiSetProjectTableView, 'ApiSetProjectTableView')
router.register(r'other_interest', ApiSetOtherInterestView, 'ApiSetOtherInterestView')
router.register(r'client_activity', ApiSetClientActivityView, 'ApiSetClientActivityView')
router.register(r'city', ApiSetCityView, 'ApiSetCityView')
router.register(r'region', ApiSetRegionView, 'ApiSetRegionView')
router.register(r'diploma_nomination', ApiSetDiplomaNominationView, 'ApiSetDiplomaNominationView')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'searchaddress', SearchAddressView.as_view(), name='SearchAddressView'),
    url(r'searchtax', SearchTaxView.as_view(), name='SearchTaxView')
]
