from django.shortcuts import render
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.gis.geos import Point
from rest_framework import viewsets, filters
from rest_framework.views import APIView
from rest_framework.permissions import *
from collections import OrderedDict
from .permissions import AppPermission, ContactPermission
from workers.filters import ClientSearchFilter
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from one_c import manage_data
from .models import *
from rest_framework.decorators import list_route, detail_route
from .serializers import *
from django.http import JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from .filters import *
from workers.models import *
from django.db.models import Q, Prefetch, F, Sum
from django.utils import timezone
from buildingmap.models import Place
from channels import Channel
import requests
from one_c import manage_data
from django.db import connections
from rest_framework import status
from dateutil.relativedelta import relativedelta
import uuid
import base64


class ModelViewSetForPhoneEmail(viewsets.ModelViewSet):
    def create_or_update_contacts(self, serializer, removed):
        phoneUpdate = []
        emailUpdate = []
        email = ''
        phone = ''
        data = self.request.data
        removePhones = data.getlist('removePhones', False)
        removeEmails = data.getlist('removeEmails', False)
        if removePhones:
            PhoneItem.objects.filter(id__in=removePhones).delete()
        if removeEmails:
            EmailItem.objects.filter(id__in=removeEmails).delete()
        for i in data:
            if len(i.split('phone_')) > 1:
                phoneUpdate.append(i.split('phone_')[1])
            elif len(i.split('email_')) > 1:
                emailUpdate.append(i.split('email_')[1])
        if len(phoneUpdate) > 0:
            qsP = PhoneItem.objects.filter(id__in=phoneUpdate)
            for i in qsP:
                i.phone = data.get('phone_' + str(i.id))
                if i.phone == data.get('main_phones'):
                    i.main = True
                    phone = i.phone
                else:
                    i.main = False
                try:
                    i.save()
                except:
                    if removed:
                        serializer.instance.delete()
                    raise serializers.ValidationError({"error_name": "Такой телефон уже существует",
                                                       "id": serializer.instance.id})

        if len(emailUpdate) > 0:
            qsE = EmailItem.objects.filter(id__in=emailUpdate)
            for j in qsE:
                j.email = data.get('email_' + str(j.id))
                if j.email == data.get('main_emails'):
                    j.main = True
                    email = j.email
                else:
                    j.main = False
                try:
                    j.save()
                except:
                    if removed:
                        serializer.instance.delete()
                    raise serializers.ValidationError({"error_name": "Такой телефон уже существует",
                                                       "id": serializer.instance.id})

        for i in data.getlist('phones', None):
            if i == data.get('main_phones'):
                try:
                    PhoneItem.objects.create(phone=i, main=True, phone_object=serializer.instance)
                except:
                    if removed:
                        serializer.instance.delete()
                    raise serializers.ValidationError({"error_name": "%s: телефон уже существует" % (i,),
                                                       "id": serializer.instance.id})
                phone = data.get('main_phones')
            else:
                try:
                    PhoneItem.objects.create(phone=i, main=False, phone_object=serializer.instance)
                except:
                    if removed:
                        serializer.instance.delete()
                    raise serializers.ValidationError({"error_name": "%s: телефон уже существует" % (i,),
                                                       "id": serializer.instance.id})
        for i in data.getlist('emails', None):
            if i == data.get('main_emails'):
                try:
                    EmailItem.objects.create(email=i, main=True, email_object=serializer.instance)
                except:
                    if removed:
                        serializer.instance.delete()
                    raise serializers.ValidationError({"error_name": "Такой email уже существует",
                                                       "id": serializer.instance.id})
                email = data.get('main_emails')
            else:
                try:
                    EmailItem.objects.create(email=i, main=False, email_object=serializer.instance)
                except:
                    if removed:
                        serializer.instance.delete()
                    raise serializers.ValidationError({"error_name": "Такой email уже существует",
                                                       "id": serializer.instance.id})
        serializer.instance.email = email
        serializer.instance.phone = phone
        serializer.instance.save()


class ApiSetClientsView(viewsets.ModelViewSet):
    serializer_class = ClientsSerializer
    queryset = Client.objects.all()
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (filters.OrderingFilter, ClientSearchFilter, DjangoFilterBackend)
    ordering_fields = '__all__'
    search_fields = ('name', 'contacts_person__phones__phone', 'contacts_person__emails__email', 'site')
    filter_fields = {'date_created': ['date__gte', 'date__lte'], 'date_updated': ['date__gte', 'date__lte'],
                     'membership__project': ['in'], 'created_user': ['in'],
                     'client_activitys__id': ['in'],
                     'entitys__activities__id': ['in'],
                     # 'membership__manager': ['exact'],
                     # 'client_group__subjects': ['in'], 'client_group__group': ['exact'],
                     'date_to_not_active': ['isnull'], 'membership__project__status': ['exact'], 'id': ['in']}

    def perform_update(self, serializer):
        valid_status = serializer.validated_data.get('status')
        geom_field = {}
        if self.request.data.get('lat'):
            geom_field['point'] = Point(float(self.request.data.get('lng')), float(self.request.data.get('lat')))
        if valid_status == 'inactive' and serializer.instance.status != valid_status:
            serializer.save(**geom_field, edited_user=self.request.user, date_to_not_active=datetime.date.today())
        else:
            serializer.save(**geom_field, edited_user=self.request.user)

    def perform_create(self, serializer):
        valid_status = serializer.validated_data.get('status')
        geom_field = {}
        if self.request.data.get('lat'):
            geom_field['point'] = Point(float(self.request.data.get('lng')), float(self.request.data.get('lat')))
        if valid_status == 'inactive':
            if self.request.user.groups.filter(~Q(name='kvs_head'), name='kvs').exists():
                serializer.save(**geom_field, created_user=self.request.user, date_to_not_active=timezone.now(),
                                personal_manager_type_one=self.request.user.managers)
            elif self.request.user.groups.filter(Q(name='head_ad') | Q(name='ad')).exists():
                serializer.save(**geom_field, created_user=self.request.user,
                                visitor_manager=self.request.user.managers)
            else:
                serializer.save(**geom_field, created_user=self.request.user, date_to_not_active=timezone.now())
        else:
            if self.request.user.groups.filter(~Q(name='kvs_head'), name='kvs').exists():
                serializer.save(**geom_field, created_user=self.request.user, personal_manager_type_one=self.request.user.managers)
            elif self.request.user.groups.filter(Q(name='head_ad') | Q(name='ad')).exists():
                serializer.save(**geom_field, created_user=self.request.user,
                                visitor_manager=self.request.user.managers)
            else:
                serializer.save(**geom_field, created_user=self.request.user)

    @list_route(methods=['get'])
    def forselectproject(self, request):
        client_id = request.GET.get('client_id')
        # qsEnt = Client.objects.filter(membership__client=client_id)
        arr = []
        if request.user.is_superuser:
            qsEnt = Membership.objects.filter(client=client_id).extra(
                                 select={'id': 'project_id'}).annotate(
                    name=F('project__name')).values('id', 'name')
        else:
            if request.user.groups.filter(name='managers').exists():
                qsEnt = Membership.objects.filter(client=client_id,
                                                  project__status__in=['working', 'finished']).extra(
                                 select={'id': 'project_id'}).annotate(
                    name=F('project__name')).values('id', 'name')
                if not qsEnt:
                    qsEnt = Membership.objects.filter(project__status__in=['working', 'finished'],
                                                      project__user=request.user.managers.head_id).extra(
                                 select={'id': 'project_id'}).annotate(
                    name=F('project__name')).values('id', 'name')
            else:
                qsEnt = Membership.objects.filter(client=client_id,
                                                  project__status__in=['working', 'finished']).extra(
                                 select={'id': 'project_id'}).annotate(
                    name=F('project__name')).values('id', 'name')
        return JsonResponse(list(qsEnt), safe=False)

    @list_route(methods=['get'])
    def count_activities(self, request):
        array_id = request.GET['id_act'].split(',')
        count = Client.objects.filter(entitys__activities__in=array_id).distinct().count()
        return JsonResponse({'count': count})

    @list_route(methods=['put'], permission_classes=(DjangoModelPermissions,))
    def check_activities(self, request):
        client_id = request.data.get('client')
        entity_id = request.data.get('entity')
        qs_now = Activities.objects.filter(entitys__client=client_id)
        qs_new = Activities.objects.filter(Q(entitys__client=client_id), ~Q(entitys=entity_id))
        if qs_new != qs_now:
            qs_mem = Membership.objects.filter(client=client_id)
            for i in qs_mem:
                check = False
                qs_test = Test.objects.filter(project=i.project)
                for j in qs_test:
                    qs_act = j.activities.all()
                    for act in qs_act:
                        if act in qs_new:
                            check = True
                            break
                    if check:
                        break
                if not check:
                    i.delete()
        return Response(status=200)
    # TODO поправить getSumTable на getSumTableFull

    @staticmethod
    def get_plan_one_c(filter_set):
        filter_date = {}
        if filter_set.get('first_month'):
            filter_date['date__gte'] = filter_set.get('first_month')
        if filter_set.get('last_month'):
            filter_date['date__lte'] = filter_set.get('last_month')
        bills = json.loads(manage_data.getSumTableBill(project_id=filter_set.get('project_id'), manager_id=filter_set.get('manager_id', '')))
        pays = json.loads(manage_data.getSumTable(**filter_set))
        plans_filter = [Q(expo__project__in=filter_set.get('project_id').split(',')) | Q(ad__project__in=filter_set.get('project_id').split(',')) | Q(lease_kvs__in=filter_set.get('project_id').split(','))]
        if filter_set.get('manager_id'):
            plans_filter.append(Q(expo__manager__in=filter_set.get('manager_id').split(',')) | Q(ad__manager__in=filter_set.get('manager_id').split(',')) | Q(lease_kvs__manager__in=filter_set.get('manager_id').split(',')))
        plan_pays = PayApp.objects.filter(*plans_filter, **filter_date).values('expo__bill',
                                                                                         'lease_kvs__bill', 'ad__bill').annotate(total=Sum('sum'))
        plan_dict = {}
        for i in plan_pays:
            plan_dict[i.get('expo__bill', i.get('lease_kvs__bill', i.get('ad__bill')))] = i.get('total')
        pay_dict = {}
        if pays != [{}]:
            for i in pays:
                if pay_dict.get(i.get('ГУИД_счета')):
                    pay_dict[i.get('ГУИД_счета')] += float(i.get('ОплаченнаяСумма'))
                else:
                    pay_dict[i.get('ГУИД_счета')] = float(i.get('ОплаченнаяСумма'))
        for i in range(len(bills)):
            bills[i]['ОплаченнаяСумма'] = pay_dict.get(bills[i].get('ГУИД_счета'), 0)
            bills[i]['СуммаСчета'] = plan_dict.get(bills[i].get('ГУИД_счета'))
        return bills

    @list_route(methods=['get'],)
    def one_c_data(self, request):

        client_id = request.GET.get('client_id')
        manager_id = request.GET.get('manager_id')
        head = request.GET.get('head')
        date_start = request.GET.get('date_start', '')
        date_end = request.GET.get('date_end', '')
        barter = request.GET.get('barter', '')
        type_data = request.GET.get('type', 'pay')
        one_c_method = {
            'bill': manage_data.getSumTableBill,
            'pay': manage_data.getSumTable
        }
        if client_id:
            projects = []
            projects_dict = {}
            one_c_data = json.loads(manage_data.getSumTableBill(client_id=client_id, barter=barter))
            if one_c_data[0] != {}:
                for i in one_c_data:
                    if i['ПроектИД'] not in projects:
                        projects.append(i['ПроектИД'])
                query_projects = Project.objects.filter(id__in=projects).values('id', 'name')
                for i in query_projects:
                    projects_dict[str(i['id'])] = i['name']
                for k, v in enumerate(one_c_data):
                    one_c_data[k]['project_name'] = projects_dict.get(v['ПроектИД'])
                return Response(data=one_c_data)
            return Response(status=200, data=[])
        if manager_id or head:
            head = False
            if request.user.groups.filter(name='kvs_head').exists() or \
                    request.user.groups.filter(name='project_managers').exists() or \
                    request.user.groups.filter(name='head_ad').exists():
                head = True
                head_id = request.user.id
                manager_id = Manager.objects.filter(
                    Q(head_id=head_id) | Q(user_id=head_id)).values_list('id', flat=True)
                manager_id = list(manager_id)
                manager_id = [str(x) for x in manager_id]
                manager_id = ','.join(manager_id)
            projects = []
            clients = []
            managers = []
            apps = []
            clients_dict = {}
            projects_dict = {}
            managers_dict = {}
            apps_dict = {}
            filter_set = {
                'manager_id': manager_id,
                'first_month': date_start,
                'last_month': date_end,
                'barter': barter
            }
            if request.GET.get('manager') or request.user.is_superuser:
                filter_set['manager_id'] = request.GET.get('manager')
            if request.GET.get('project') or request.user.is_superuser:
                filter_set['project_id'] = request.GET.get('project')
                if request.GET.get('show_all'):
                    filter_set['manager_id'] = ''
            one_c_data = None
            try:
                if type_data == 'plan':
                    one_c_data = self.get_plan_one_c(filter_set)
                else:
                    one_c_data = json.loads(one_c_method.get(type_data)(**filter_set))
            except:
                return Response(status=400, data={'error': 'error get data from 1C'})
            if one_c_data[0] != {}:
                query_managers = Manager.objects.filter(
                    Q(head_id=request.user.managers.head_id) | Q(user=request.user.managers.head_id)).select_related()
                for i in query_managers:
                    managers_dict[str(i.id)] = '%s %s' % (i.user.last_name, i.user.first_name)
                for k, v in enumerate(one_c_data):
                    one_c_data[k]['manager_name'] = managers_dict.get(v['МенеджерИД'])
                for i in one_c_data:
                    if i['ПроектИД'] not in projects:
                        projects.append(i['ПроектИД'])
                    if i['КлиентИД'] not in clients:
                        clients.append(i['КлиентИД'])
                    if i['МенеджерИД'] not in managers:
                        managers.append(i['МенеджерИД'])
                    if i['ЗаявкаИД'] not in apps:
                        apps.append(i['ЗаявкаИД'])
                query_projects = Project.objects.filter(id__in=projects).values('id', 'name')
                query_clients = Client.objects.filter(id__in=clients).values('id', 'name')
                if self.request.user.groups.filter(Q(name='head_ad') | Q(name='ad')):
                    query_apps = ApplicationAd.objects.filter(id__in=apps).values('id', 'status')
                elif self.request.user.groups.filter(Q(name='kvs')):
                    query_apps = ApplicationLeaseKVS.objects.filter(id__in=apps).values('id', 'status')
                else:
                    query_apps = ApplicationExpo.objects.filter(id__in=apps).values('id', 'status')
                if head:
                    query_managers = Manager.objects.filter(Q(head_id=head_id) | Q(user=request.user)).select_related()
                    for i in query_managers:
                        managers_dict[str(i.id)] = '%s %s' % (i.user.last_name, i.user.first_name)
                    for k, v in enumerate(one_c_data):
                        one_c_data[k]['manager_name'] = managers_dict.get(v['МенеджерИД'])
                if request.user.is_superuser or request.user.groups.filter(name='head_expo').exists():
                    if request.GET.get('manager'):
                        query_managers = Manager.objects.filter(id__in=request.GET.get('manager').split(',')).select_related()
                    else:
                        query_managers = Manager.objects.filter(
                            membership__project__in=request.GET.get('project').split(',')).select_related()
                    for i in query_managers:
                        managers_dict[str(i.id)] = '%s %s' % (i.user.last_name, i.user.first_name)
                    for k, v in enumerate(one_c_data):
                        one_c_data[k]['manager_name'] = managers_dict.get(v['МенеджерИД'])
                for i in query_projects:
                    projects_dict[str(i['id'])] = i['name']
                for k, v in enumerate(one_c_data):
                    one_c_data[k]['project_name'] = projects_dict.get(v['ПроектИД'])
                for i in query_clients:
                    clients_dict[str(i['id'])] = i['name']
                for k, v in enumerate(one_c_data):
                    one_c_data[k]['client_name'] = clients_dict.get(v['КлиентИД'])
                for i in query_apps:
                    apps_dict[str(i['id'])] = i['status']
                for k, v in enumerate(one_c_data):
                    one_c_data[k]['app_status'] = apps_dict.get(v['ЗаявкаИД'])
                return Response(data=one_c_data)
            return Response(status=200, data=[])
        return Response(status=400, data={'description': 'data not found'})

    def get_queryset(self):
        if 'id' in self.request.GET:
            project_id = self.request.GET.get('project_id')
            project = Project.objects.get(pk=project_id)
            client_group = self.request.GET.get('client_group__group')
            subject_id = self.request.GET.get('client_group__subjects')
            filter_field = {}
            Q_filter = []
            array_id = self.request.GET['id'].split(',')
            filter_field['entitys__activities__in'] = array_id
            is_null = self.request.GET.get('manager__isnull')
            not_call = self.request.GET.get('not_call')
            if is_null:
                qs_exclude_client = Client.objects.filter(membership__project=project_id,
                                                          membership__manager__isnull=False).values_list('id')
                exclude_client = [x[0] for x in qs_exclude_client]
                Q_filter.append(~Q(id__in=exclude_client))
            if not_call:
                s = set(Contact.objects.filter(type_contact='out_phone', project_id=project_id).distinct('client').values_list('client_id', flat=True))
                s1 = set(Client.objects.filter(membership__project_id=project_id).exclude(
                    status__in=['black_list', 'inactive']).distinct().values_list('id', flat=True))
                # Q_filter.append(~Q(contact__project_id=project_id) | Q(
                # Q(contact__project_id=project_id) & ~Q(contact__type_contact='out_phone')))
                Q_filter.append(Q(id__in=list(s1-s)))
            if client_group:
                filter_field['client_group__group'] = client_group
                filter_field['client_group__subjects'] = subject_id
            if self.request.GET.get('membership__manager'):
                filter_field['membership__manager'] = self.request.GET.get('membership__manager')
                filter_field['membership__project'] = project_id
            order = ''
            qs = Client.objects.filter(*Q_filter, **filter_field).prefetch_related(Prefetch(
                'membership',
                queryset=Membership.objects.filter(project=project),
                to_attr='manager'))
            if self.request.GET.get('sort') == '-group':
                order = '-'
            if self.request.GET.get('sort') == 'group' or self.request.GET.get('sort') == '-group':
                return Client.objects.filter(*Q_filter, **filter_field).extra(select={
                    'group_name': """SELECT "group" 
                                    FROM clients_clientgroup 
                                    WHERE clients_clientgroup.client_id=clients_client.id 
                                    AND subjects_id=%s  
                                    ORDER BY id DESC LIMIT 1""" % (
                    str(project.subjects_id),)
                }).order_by(order+'group_name').distinct()
            return qs.distinct()
        elif 'contacts_person__isnull' or 'created_user__managers__in' in self.request.GET:
            filters_field = {}
            if self.request.GET.get('contacts_person__isnull'):
                filters_field = {'contacts_person__isnull': bool(self.request.GET.get('contacts_person__isnull'))}
            # isnull = self.request.GET.get('contacts_person__isnull')
            q_filter = Q()
            if self.request.GET.get('created_user__managers__in'):
                #filters_field['created_user__managers__in'] = self.request.GET.get('created_user__managers__in').split(',')
                q_filter |= Q(created_user__managers__in=self.request.GET.get('created_user__managers__in').split(','))
            if self.request.GET.get('edited_user__managers__in'):
                q_filter |= Q(edited_user__managers__in=self.request.GET.get('edited_user__managers__in').split(','))

            return Client.objects.filter(q_filter, **filters_field)
        else:
            return Client.objects.all().prefetch_related('created_user', 'edited_user')

    def get_serializer_context(self):
        return {'request': self.request}

    def get_serializer_class(self):
        if 'pk' in self.kwargs:
            return ClientsSerializerForPk
        elif self.request.GET.get('data', None) == 'min':
            return ClientsSerializerMin
        elif self.request.GET.get('data', None) == 'mid':
            return ClientsSerializerSearch
        elif self.request.GET.get('id'):
            return ClientProjectSectionSerializer
        else:
            return ClientsSerializer


class ApiSetIndividualsView(ModelViewSetForPhoneEmail):
    @list_route(methods=['get'])
    def count(self, request):
        Individual_count = Individual.objects.count()
        content = {'count': Individual_count}
        return JsonResponse(content)

    @list_route(methods=['get'])
    def forselectindividual(self, request):
        client_id = request.GET.get('client_id')
        qsEnt = Entity.objects.filter(client=client_id)
        arr = []
        for i in qsEnt:
            arr.extend(list(map(lambda x: dict(list({'text':x['surname']+' '+x['name']+' '+x['middle_name']}.items())+list(x.items())), i.individual.all().values())))
        return JsonResponse(arr, safe=False)

    @list_route(methods=['post'], )
    def create_client(self, request):
        individual = Individual.objects.get(id=request.data.get('id'))
        if IndEntPrShip.objects.filter(individual=individual).exists():
            return JsonResponse({'exists': True})
        phones = list(individual.phones.all().values_list('phone', flat=True))
        emails = list(individual.emails.all().values_list('email', flat=True))
        client = Client.objects.create(
            name='%s %s' % (individual.surname, individual.name),
            address=individual.address,
            visitor_manager=individual.visitor_manager,
            created_user=request.user,
            site=''
        )
        contact_person = ContactPerson.objects.create(client=client,
                                                      name='%s %s' % (individual.surname, individual.name))
        for i in phones:
            contact_person.phones.create(phone=i)
        for i in emails:
            contact_person.emails.create(email=i)
        entity = Entity.objects.create(
            full_name='%s %s' % (individual.surname, individual.name),
            client=client,
            date_created=datetime.datetime.now(),
            address=individual.address
        )
        IndEntPrShip.objects.create(
            entity_id=entity,
            individual=individual,
            type_position='Представитель',
            position='Представитель'
        )
        qs = NewIndividualContact.objects.filter(individual=individual)
        create_new = []
        for i in qs:
            create_new.append(NewContact(
                client=client,
                date_new_contact=i.date_new_contact,
                goal_contact=i.goal_contact,
                project=i.project,
                create_user=i.create_user
            ))
        NewContact.objects.bulk_create(create_new)
        qs.delete()
        return JsonResponse({
            'client': client.id,
            'entity': entity.id
        })

    queryset = Individual.objects.all().prefetch_related(Prefetch('indentprship_set', queryset=IndEntPrShip.objects.filter(main=True).annotate(ent_name=F('entity_id__full_name')), to_attr='position'))
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = '__all__'
    search_fields = ('name', 'address', 'surname', 'sex', 'phones__phone')
    ordering = ('name',)
    filter_fields = {'visitor_manager': ['isnull']}
    serializer_class = IndividualsSerializer

    def get_serializer_class(self):
        if 'pk' in self.kwargs:
            return IndividualsSerializer
        # elif self.request.GET.get('data') == 'min':
        #     return
        else:
            return IndividualSerializerMin

    def perform_update(self, serializer):
        self.create_or_update_contacts(serializer, removed=False)
        serializer.save()

    def perform_create(self, serializer):
        save_data = {
            'date_created': timezone.now(),
            'created_user': self.request.user
        }
        if self.request.user.groups.filter(name='ad').exists():
            save_data['visitor_manager'] = self.request.user.managers
        serializer.save(**save_data)
        self.create_or_update_contacts(serializer, removed=False)


    # def perform_create(self, serializer):


class ApiSetEntityPrivateView(ModelViewSetForPhoneEmail):
    queryset = Entity.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = EntityPrivateSerializer
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_class = EntityFilter
    search_fields = ('full_name', )
    ordering = ('full_name',)
    ordering_fields = '__all__'
    # filter_fields = ('client', )

    @list_route(methods=['get'])
    def one_c_check(self, request):
        INN = request.GET.get('inn', None)
        r = requests.get("http://%s/odata/standard.odata/"
                         "Catalog_Контрагенты"
                         "?$format=json&$top=15&$filter=(ИНН eq '%s') "
                         "and DeletionMark eq false" % (settings.ONE_C_URL, INN), auth=settings.ONE_C_AUTH)
        return JsonResponse(r.json()['value'], safe=False)

    @list_route(methods=['get', 'post', 'patch'])
    def one_c(self, request):
        if request.method == 'POST':
            data = request.POST.dict()
            data['Description'] = data['НаименованиеПолное']
            data['Parent_Key'] = '73d53189-d55c-11e1-8d1c-00e052b07681'
            del data['Ref_Key']
            data['КонтактнаяИнформация'] = [{'Представление': data['Адрес'], 'Тип': 'Адрес', 'LineNumber': 1,
                                             'Вид_Key': '74300843-eff8-11e8-8ae5-00505682f556',
                                             'ВидДляСписка_Key': '74300843-eff8-11e8-8ae5-00505682f556'}]
            r = requests.post('http://%s/odata/standard.odata/'
                              'Catalog_Контрагенты?$format=json' % (settings.ONE_C_URL,),
                              auth=settings.ONE_C_AUTH,
                              json=data)
            return JsonResponse(r.json(), safe=False)
        if request.method == 'PATCH':
            data = request.POST.dict()
            data['Description'] = data['НаименованиеПолное']
            data['КонтактнаяИнформация'] = [{'Представление': data['Адрес'], 'Тип': 'Адрес', 'LineNumber': 1,
                                             'Вид_Key': '74300843-eff8-11e8-8ae5-00505682f556',
                                             'ВидДляСписка_Key': '74300843-eff8-11e8-8ae5-00505682f556'}]
            # r = requests.patch("http://%s/odata/standard.odata/"
            #                    "Document_СчетНаОплатуПокупателю"
            #                    "(guid'%s')?$format=json" % (settings.ONE_C_URL, data['Ref_Key']),
            #                    auth=settings.ONE_C_AUTH,
            #                    json=data)
            r = requests.patch("http://%s/odata/standard.odata/"
                               "Catalog_Контрагенты(guid'%s')?$format=json" % (settings.ONE_C_URL, data['Ref_Key']),
                               auth=settings.ONE_C_AUTH,
                               json=data)
            return JsonResponse(r.json(), safe=False)
        elif request.method == 'GET':
            INN = request.GET.get('inn', None)
            guid = request.GET.get('guid', False)
            nomenclature = request.GET.get('nom', False)
            if guid:
                r = requests.get("http://%s/odata/standard.odata/"
                                 "Catalog_Контрагенты(guid'%s')/?$format=json" % (settings.ONE_C_URL, guid),
                                 auth=settings.ONE_C_AUTH)
                return JsonResponse(r.json(), safe=False)
            elif nomenclature:
                r = requests.get("http://%s/odata/standard.odata/"
                                 "Catalog_Номенклатура?$format=json&$filter=Услуга eq true "
                                 "and substringof('%s',НаименованиеПолное)" % (settings.ONE_C_URL, nomenclature),
                                 auth=settings.ONE_C_AUTH)
                return JsonResponse(r.json()['value'], safe=False)
            r = requests.get("http://%s/odata/standard.odata/"
                             "Catalog_Контрагенты"
                             "?$format=json&$top=15&$filter=substringof('%s',concat(ИНН, Description)) "
                             "and DeletionMark eq false" % (settings.ONE_C_URL, INN), auth=settings.ONE_C_AUTH)
            return JsonResponse(r.json()['value'], safe=False)

    def perform_create(self, serializer):
        act_in_client = Activities.objects.filter(
            entitys__client=serializer.validated_data.get('client')
            ).distinct().values_list('id', flat=True)
        act_in_client = set(act_in_client)
        serializer.save(date_created=timezone.now(), created_user=self.request.user)
        if serializer.instance.client:
            # act_in_client = Activities.objects.filter(
            #     entitys__client=serializer.instance.client).distinct().values_list('id', flat=True)
            act_ent = serializer.instance.activities.all().values_list('id', flat=True)
            act_ent = set(act_ent)
            result = list(act_ent ^ act_in_client)
            if len(result) > 0:
                client = serializer.instance.client
                client.change_activities = timezone.now()
                client.save()
                section = Test.objects.filter(activities__in=result)
                section.update(change=True)
                arr = set()
                arr_project = set()
                for i in section:
                    arr.add(i.project.name)
                    arr_project.add(i.project)
                arr = list(arr)
                # TODO РАССЫЛКА ПО ИЗМЕНЕНИЮ ВИДА ДЕЯТЕЛЬНОСТИ, ВРЕМЕННО ОТКЛЮЧЕНА
                # if len(arr) > 0:
                #     m = 'Добавлен новый клиент в проект'
                #     if len(arr) > 1:
                #         m += 'ы'
                #     for i in arr_project:
                #         Channel('chat-messages').send({'user': i.user.id,
                #                                        'push': m,
                #                                        'username': i.user.username,
                #                                        'message': m + ': ' + ', '.join(arr),
                #                                        'sender': self.request.user.pk})
        self.create_or_update_contacts(serializer, removed=False)

    def perform_update(self, serializer):
        self.create_or_update_contacts(serializer, removed=False)
        if serializer.initial_data.get('client', None) != '' and serializer.initial_data.get('client', None) is not None:
            # serializer.save()
        # else:
            qsOld = Activities.objects.filter(Q(entitys__client=serializer.initial_data.get('client'))).distinct()
            qsNew = Activities.objects.\
                filter(Q(entitys__client=serializer.initial_data.get('client')) |
                       Q(entitys__id=serializer.instance.id)).distinct()
            if qsOld != qsNew and len(qsOld) < len(qsNew):
                diff = list(set(qsNew.values_list('id', flat=True)) - set(qsOld.values_list('id', flat=True)))
                clients = Client.objects.filter(id=serializer.initial_data.get('client'))
                clients.update(change_activities=timezone.now())
                section = Test.objects.filter(activities__in=diff)
                section.update(change=True)
                arr = set()
                arr_project = set()
                for i in section:
                    arr.add(i.project.name)
                    arr_project.add(i.project)
                arr = list(arr)
                # if len(arr) > 0:
                #     m = 'Добавлен новый клиент в проект'
                #     if len(arr) > 1:
                #         m += 'ы'
                #     for i in arr_project:
                #         Channel('chat-messages').send({'user': i.user.id,
                #                                        'push': m,
                #                                        'username': i.user.username,
                #                                        'message': m + ': ' + ', '.join(arr),
                #                                        'sender': self.request.user.pk})

        act_db = serializer.instance.activities.values_list('id', flat=True)
        act_db_id = []
        for i in act_db:
            act_db_id.append(i)
        act_initial_data = [int(x) for x in serializer.initial_data.getlist('activities')]
        serializer.save()
        # Push уведомления, потом перенести в отдельный метод
        result = list(set(act_db_id) ^ set(act_initial_data))
        diff = list(set(act_initial_data) - set(act_db_id))
        section = Test.objects.filter(activities__in=diff)
        section.update(change=True)
        arr = set()
        arr_project = set()
        for i in section:
            arr.add(i.project.name)
            arr_project.add(i.project)
        arr = list(arr)
        # if len(arr) > 0:
        #     m = 'Добавлен новый клиент в проект'
        #     if len(arr) > 1:
        #         m += 'ы'
        #     for i in arr_project:
        #         Channel('chat-messages').send({'user': i.user.id,
        #                                        'push': m,
        #                                        'username': i.user.username,
        #                                        'message': m + ': ' + ', '.join(arr),
        #                                        'sender': self.request.user.pk})
            # Конец
        if len(result) > 0 and serializer.instance.client:
            qs_new = Activities.objects.filter(entitys__client=serializer.instance.client)
            qs_mem = Membership.objects.filter(client=serializer.instance.client)
            for i in qs_mem:
                check = False
                qs_test = Test.objects.filter(project=i.project)
                for j in qs_test:
                    qs_act = j.activities.all()
                    for act in qs_act:
                        if act in qs_new:
                            check = True
                            break
                    if check:
                        break
                if not check:
                    i.delete()
            client = serializer.instance.client
            client.change_activities = timezone.now()
            client.save()

        # qs_now = Activities.objects.filter(entitys__client=client_id)
        # qs_new = Activities.objects.filter(Q(entitys__client=client_id), ~Q(entitys=entity_id))

    def get_serializer_class(self):
        if self.request.GET.get('data', None) == 'min':
            return EntityPrivateSerializerMin
        elif self.request.GET.get('data', None) == 'max':
            return EntityPrivateSerializerMax
        elif self.request.GET.get('data', None) == 'mid':
            return EntityPrivateSerializerForClients
        elif 'pk' in self.kwargs:
            return EntityPrivateSerializerForPk
        else:
            return EntityPrivateSerializer


# class ApiSetEntityView(viewsets.ModelViewSet):
#     queryset = Entity.objects.all()
#     permission_classes = (DjangoModelPermissions, )
#     serializer_class = EntitySerializer


class ApiSetIndEntPrShipView(viewsets.ModelViewSet):
    queryset = IndEntPrShip.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = IndEntPrShipSerializerForChange
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('entity_id', 'individual')

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return IndEntPrShipSerializer
        else:
            return IndEntPrShipSerializerForChange


# Контакты ##############################################
class ApiSetContactsView(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    permission_classes = (DjangoModelPermissions, ContactPermission)
    serializer_class = ContactsSerializer
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('client',)
    search_fields = ('comment', 'contact_person__name')
    ordering = ('-date',)
    ordering_fields = '__all__'

    @list_route(methods=['post'])
    def create_url(self, request):
        hostname = request.data.get('hostname')
        guid = str(uuid.uuid4())
        encode_param = 'guid=%s' % (base64.b64encode(guid.encode()).decode('ascii'),)
        url = '%s/api/guid/?%s' % (hostname, encode_param)
        cursor = connections['expoApp'].cursor()
        # connections['expoApp'].commit()
        try:
            cursor.execute('INSERT INTO app_bid (guid, url, date_create, hotel, individual_build, answer) '
                           'VALUES (%s,%s,%s,%s,%s,%s)',
                           (guid, url, str(datetime.date.today()), False, False, False))
        except:
            return Response(data={'db': 'error save in DB'}, status=400)
        connections['expoApp'].commit()
        return JsonResponse(data={'url': url}, status=200)

    def get_serializer_context(self):
        manager_list = []
        if Manager.objects.filter(head=self.request.user).exists():
            manager_list += Manager.objects.filter(head=self.request.user).values_list('id', flat=True)
        manager_list.append(self.request.user.managers.id)
        return {'request': self.request, 'manager_list': manager_list}

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return ContactsSerializerRO
        else:
            return ContactsSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        app = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        data = serializer.data
        if app.get('app'):
            if app.get('type') == 'expo':
                qs = ApplicationExpo.objects.create(
                    date=datetime.datetime.now(),
                    client=app.get('client'),
                    project=app.get('project'),
                    manager=app.get('manager'),
                    prepayment=False,
                    installmen_plan=False,
                    barter=False,
                    status='working'
                )
                data['app_id'] = qs.id
                data['app_type'] = 'expo'
            elif app.get('type') == 'kvs':
                qs = ApplicationLeaseKVS.objects.create(
                    date=datetime.datetime.now(),
                    client=app.get('client'),
                    project=app.get('project'),
                    manager=app.get('manager'),
                    prepayment=False,
                    installmen_plan=False,
                    barter=False,
                    status='working'
                )
                data['app_id'] = qs.id
                data['app_type'] = 'lease_kvs'
            elif app.get('type') == 'ad':
                qs = ApplicationAd.objects.create(
                    date=datetime.datetime.now(),
                    client=app.get('client'),
                    project=app.get('project'),
                    manager=app.get('manager'),
                    prepayment=False,
                    installmen_plan=False,
                    barter=False,
                    status='working'
                )
                data['app_id'] = qs.id
                data['app_type'] = 'ad'

        return Response(data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        # try:
        #     manager = self.request.user.managers
        # except Exception:
        #     return Response(status=403, data={'error': 'You not manager'})
        if 'Отказ' in serializer.validated_data.get('result'):
            NewContact.objects.filter(create_user=self.request.user,
                                      project=serializer.validated_data.get('project'),
                                      client=serializer.validated_data.get('client')).delete()
        else:
            NewContact.objects.filter(create_user=self.request.user,
                                      project=serializer.validated_data.get('project'),
                                      client=serializer.validated_data.get('client'),
                                      date_created__lte=serializer.validated_data.get('date')).delete()
        if self.request.data.get('save_new_contact'):
            NewContact.objects.create(create_user=self.request.user,
                                      project=serializer.validated_data.get('project'),
                                      client=serializer.validated_data.get('client'),
                                      goal_contact=self.request.data.get('goal_contact'),
                                      date_new_contact=self.request.data.get('date_new_contact'))
        client = serializer.validated_data.get('client')
        client.date_last_contact = serializer.validated_data.get('date', None)
        client.save()
        project = serializer.validated_data.get('project')
        result = serializer.validated_data.get('result')
        if project:
            subjects = project.subjects
        failure_bool = False
        app = False
        interest_other = False
        expo_user = self.request.user.groups.\
            filter(Q(name='managers') | Q(name='project_managers')).exists()
        if 'Отказ' in result:
            failure_bool = True
            if project and project.project_type.type_project_type == 'expo' and expo_user:
                subjects = project.subjects
                qs = ClientGroup.objects.filter(client=client, subjects=subjects)
                if qs:
                    qs = qs[0]
                    failure = qs.failure
                    if failure == '':
                        failure = []
                    else:
                        failure = failure.split(',')
                    if str(project.id) not in failure:
                        if len(failure) == 0:
                            qs.failure = str(project.id)
                        elif len(failure) < 2:
                            qs.failure += ',' + str(project.id)
                        elif len(failure) == 2:
                            qs.failure = ''
                            if qs.group == 'a':
                                qs.group = 'b'
                            elif qs.group == 'b':
                                qs.group = 'c'
                            elif qs.group == 'vip':
                                qs.group = 'a'
                        qs.save()
        if 'Заявка' in result:
            app = True
            try:
                if project.project_type.type_project_type == 'expo' and expo_user:
                    subjects = project.subjects
                    qs = ClientGroup.objects.filter(client=client, subjects=subjects)[:1]
                    if qs:
                        qs[0].group = 'vip'
                        qs[0].save()
                    else:
                        ClientGroup.objects.create(client=client, subjects=subjects, group='vip')
            except:
                raise ValidationError({'manager_error': 'У выбранного проекта нет тематики'})
        if 'Интерес на др. услуги' in result:
            interest_other = True

        try:
            self.request.user.managers
        except ObjectDoesNotExist:
            raise ValidationError({'manager_error': 'Нет прав менеджмента'})

        serializer.save(manager=self.request.user.managers, failure_bool=failure_bool,
                        app=app, interest_other=interest_other,
                        contact_person_name=serializer.validated_data.get('contact_person').name)
        project_type = None
        if project:
            project_type = project.project_type.type_project_type
        return {
            'app': app,
            'type': project_type,
            'client': client,
            'project': project,
            'manager': self.request.user.managers
        }


class ApiSetContactsOldView(viewsets.ModelViewSet):
    queryset = ContactOld.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = ContactOldSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('client',)
    ordering_fields = ('date',)
    ordering = ('-date',)


class ApiSetIndividualContactsView(viewsets.ModelViewSet):
    queryset = IndividualContact.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = IndividualContactsSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('individual',)
    ordering_fields = ('date', 'result', 'manager__user__last_name', 'source_of_information')
    ordering = ('-date',)

    def get_serializer_context(self):
        return {'request': self.request}

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return IndividualContactsSerializerRO
        else:
            return IndividualContactsSerializer

    def perform_create(self, serializer):
        NewIndividualContact.objects.filter(create_user=self.request.user,
                                            project=serializer.validated_data.get('project'),
                                            individual=serializer.validated_data.get('individual'),
                                            date_new_contact__lte=serializer.validated_data.get('date')).delete()
        if self.request.data.get('save_new_contact'):
            NewIndividualContact.objects.create(create_user=self.request.user,
                                                project=serializer.validated_data.get('project'),
                                                individual=serializer.validated_data.get('individual'),
                                                goal_contact=self.request.data.get('goal_contact'),
                                                date_new_contact=self.request.data.get('date_new_contact'))
        result = serializer.validated_data.get('result')
        failure_bool = False
        # app = False
        # interest_other = False
        if 'Отказ' in result:
            failure_bool = True
        # if 'Ожидание оплаты' in result:
        #     app = True
        # if 'Интерес на др. услуги' in result:
        #     interest_other = True
        try:
            self.request.user.managers
        except ObjectDoesNotExist:
            raise ValidationError({'manager_error': 'Нет прав менеджмента'})

        serializer.save(manager=self.request.user.managers, failure_bool=failure_bool)


class ApiSetNewContactsView(viewsets.ModelViewSet):
    queryset = NewContact.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = NewContactSerializer

    def perform_create(self, serializer):
        NewContact.objects.filter(client=serializer.validated_data.get('client'),
                                  project=serializer.validated_data.get('project'),
                                  create_user=self.request.user).delete()
        serializer.save(create_user=self.request.user)


class ApiSetIndividualNewContactsView(viewsets.ModelViewSet):
    queryset = NewIndividualContact.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = NewIndividualContactSerializer

    def perform_create(self, serializer):
        NewIndividualContact.objects.filter(individual=serializer.validated_data.get('individual'),
                                            project=serializer.validated_data.get('project'),
                                            create_user=self.request.user).delete()
        serializer.save(create_user=self.request.user)


# Заказанные услуги

class ApiSetOrderedServiceView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('app', )
    search_fields = ('nomenclature__name',)
    ordering = ('nomenclature__name',)
    ordering_fields = '__all__'


class ApiSetOrderedServiceExpoView(ApiSetOrderedServiceView):
    queryset = OrderedServiceExpo.objects.all()

    # serializer_class = OrderedServiceExpoSerializer

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return OrderedServiceExpoSerializerRO
        else:
            return OrderedServiceExpoSerializer


class ApiSetOrderedServiceAdView(ApiSetOrderedServiceView):
    queryset = OrderedServiceAd.objects.all()
    # serializer_class = OrderedServiceAdSerializer

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return OrderedServiceAdSerializerRO
        else:
            return OrderedServiceAdSerializer


class ApiSetOrderedServiceLeaseKVSView(ApiSetOrderedServiceView):
    queryset = OrderedServiceLeaseKVS.objects.all()
    # serializer_class = OrderedServiceLeaseKVSSerializer

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return OrderedServiceLeaseKVSSerializerRO
        else:
            return OrderedServiceLeaseKVSSerializer
# Заявки


class ApiSetApplicationView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions, AppPermission)
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_class = AppFilter
    filter_fields = ('client', 'manager')
    search_fields = ('client__name', 'entity__full_name', 'client__address')
    ordering = ('-date',)
    ordering_fields = ('client', 'client__address', 'entity', 'date', 'project')


class ApiSetApplicationExpoView(ApiSetApplicationView):
    queryset = ApplicationExpo.objects.all().annotate(max_discount=F('project__max_discount')).prefetch_related('ordered_service')
    serializer_class = ApplicationExpoSerializer

    @list_route(methods=['post'], permission_classes=[DjangoModelPermissions])
    def copy_app(self, request):
        app = ApplicationExpo.objects.get(pk=request.data.get('id'))
        app.id = None
        app.bill = ''
        app.barter_sum = 0
        app.main_id = int(request.data.get('id'))
        app.date = datetime.datetime.now()
        app.save()
        return Response(data={'id': app.id}, status=200)

    def get_serializer_class(self):
        if 'pk' in self.kwargs or self.request.method.upper() not in settings.SAFE_METHODS:
            return ApplicationExpoSerializer
        else:
            return ApplicationExpoSerializerTable

    def perform_create(self, serializer):
        serializer.save(date=datetime.datetime.now())

    def perform_update(self, serializer):
        if self.request.data.get('total_sum') and self.request.data.get('barter') == 'false':
            total_sum = self.request.data.get('total_sum')
            if PayApp.objects.filter(expo_id=serializer.instance.id).exists():
                PayApp.objects.filter(expo_id=serializer.instance.id).delete()
            thematic = serializer.validated_data.get('project').subjects.format
            project_date = serializer.validated_data.get('project').date_start
            app_date = serializer.validated_data.get('date')
            curr_date = app_date.date()
            first_term = project_date - relativedelta(months=2)
            second_term = project_date - relativedelta(days=20)
            #exist_pay = PayApp.objects.filter(expo_id=serializer.instance.id).exists()
            arr = []
            if thematic == 'B2B+B2C' or thematic == 'B2B':
                # arr = [PayApp(date=app_date+relativedelta(days=7), sum=float(total_sum)*0.3, expo_id=serializer.instance.id, percent=0.3),
                #        PayApp(date=project_date-relativedelta(months=2), sum=float(total_sum)*0.4, expo_id=serializer.instance.id, percent=0.4),
                #        PayApp(date=project_date-relativedelta(days=20), sum=float(total_sum)*0.3, expo_id=serializer.instance.id, percent=0.3)]
                if first_term < curr_date < second_term:
                    arr = [PayApp(date=app_date + relativedelta(days=5),
                                  sum=float(total_sum) * 0.7,
                                  expo_id=serializer.instance.id,
                                  percent=0.7),
                           PayApp(date=second_term,
                                  sum=float(total_sum) * 0.3,
                                  expo_id=serializer.instance.id,
                                  percent=0.3)]
                elif curr_date >= second_term:
                    arr = [
                        PayApp(date=app_date + relativedelta(days=5),
                               expo_id=serializer.instance.id,
                               sum=float(total_sum),
                               percent=1)
                    ]
                elif curr_date < first_term:
                    arr = [PayApp(date=app_date + relativedelta(days=5), sum=float(total_sum) * 0.3,
                                  expo_id=serializer.instance.id, percent=0.3),
                           PayApp(date=project_date - relativedelta(months=2), sum=float(total_sum) * 0.4,
                                  expo_id=serializer.instance.id, percent=0.4),
                           PayApp(date=project_date - relativedelta(days=20), sum=float(total_sum) * 0.3,
                                  expo_id=serializer.instance.id, percent=0.3)]
            else:
                if curr_date > second_term:
                    arr = [
                        PayApp(date=app_date + relativedelta(days=5),
                               expo_id=serializer.instance.id,
                               sum=float(total_sum),
                               percent=1)
                    ]
                else:
                    arr = [PayApp(date=app_date+relativedelta(days=5), sum=float(total_sum)*0.3, expo_id=serializer.instance.id, percent=0.3),
                       PayApp(date=serializer.validated_data.get('project').date_end, sum=float(total_sum)*0.7, expo_id=serializer.instance.id, percent=0.7)]
            if arr:
                PayApp.objects.bulk_create(arr)



            # else:
            #     thematic = serializer.validated_data.get('project').subjects.format
            #     project_date = serializer.validated_data.get('project').date_start
            #     app_date = serializer.validated_data.get('date')
            #     qs = PayApp.objects.filter(expo_id=serializer.instance.id).order_by('sum')
            #     if thematic == 'B2B+B2C' or thematic == 'B2B':
            #         m1 = qs[0]
            #         m2 = qs[2]
            #         m3 = qs[1]
            #         m1.date = app_date + relativedelta(days=7)
            #         m1.sum = float(total_sum) * 0.3
            #         m1.percent = 0.3
            #         m1.save()
            #         #m = qs[1]
            #         m2.date = project_date - relativedelta(months=2)
            #         m2.sum = float(total_sum) * 0.4
            #         m2.percent = 0.4
            #         m2.save()
            #         #m = qs[2]
            #         m3.date = project_date - relativedelta(days=20)
            #         m3.sum = float(total_sum) * 0.3
            #         m3.percent = 0.3
            #         m3.save()
            #     else:
            #         m1 = qs[0]
            #         m2 = qs[1]
            #         m1.date = app_date + relativedelta(days=7)
            #         m1.sum = float(total_sum) * 0.3
            #         m1.percent = 0.3
            #         m1.save()
            #         #m = qs[1]
            #         m2.date = serializer.validated_data.get('project').date_end
            #         m2.sum = float(total_sum) * 0.7
            #         m2.percent = 0.7
            #         m2.save()

        if serializer.validated_data.get('status') == 'canceled' and \
                        serializer.validated_data.get('status') != serializer.instance.status and \
                serializer.validated_data.get('bill'):
            r = requests.get("http://%s/hs/Expobit/AccountClosed/%s" % (
                settings.ONE_C_URL, serializer.validated_data.get('bill')),
                             cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                             auth=settings.ONE_C_AUTH)
            if r.status_code == 404:
                if manage_data.update_session() == 200:
                    r = requests.get("http://%s/hs/Expobit/AccountClosed/%s" % (
                    settings.ONE_C_URL, serializer.validated_data.get('bill')),
                                     cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                                     auth=settings.ONE_C_AUTH)
        serializer.save()


class ApiSetApplicationAdView(ApiSetApplicationView):
    queryset = ApplicationAd.objects.all()
    serializer_class = ApplicationAdSerializer

    def copy_app(self, request):
        app = ApplicationExpo.objects.get(pk=request.data.get('id'))
        app.id = None
        app.main_id = int(request.data.get('id'))
        app.date = datetime.datetime.now()
        app.save()
        return Response(data={'id': app.id}, status=200)

    def get_serializer_class(self):
        if 'pk' in self.kwargs or self.request.method.upper() not in settings.SAFE_METHODS:
            return ApplicationAdSerializer
        else:
            return ApplicationAdSerializerTable

    def perform_create(self, serializer):
        serializer.save(date=datetime.datetime.now())

    def perform_update(self, serializer):
        if serializer.validated_data.get('status') == 'canceled' and \
                        serializer.validated_data.get('status') != serializer.instance.status and \
                serializer.validated_data.get('bill'):
            r = requests.get("http://%s/hs/Expobit/AccountClosed/%s" % (
                settings.ONE_C_URL, serializer.validated_data.get('bill')),
                             cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                             auth=settings.ONE_C_AUTH)
            if r.status_code == 404:
                if manage_data.update_session() == 200:
                    r = requests.get("http://%s/hs/Expobit/AccountClosed/%s" % (
                    settings.ONE_C_URL, serializer.validated_data.get('bill')),
                                     cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                                     auth=settings.ONE_C_AUTH)
        serializer.save()


class ApiSetApplicationLeaseKVSView(ApiSetApplicationView):
    queryset = ApplicationLeaseKVS.objects.all()
    serializer_class = ApplicationLeaseKVSSerializer

    def copy_app(self, request):
        app = ApplicationExpo.objects.get(pk=request.data.get('id'))
        app.id = None
        app.main_id = int(request.data.get('id'))
        app.date = datetime.datetime.now()
        app.save()
        return Response(data={'id': app.id}, status=200)

    def get_serializer_class(self):
        if 'pk' in self.kwargs or self.request.method.upper() not in settings.SAFE_METHODS:
            return ApplicationLeaseKVSSerializer
        else:
            return ApplicationLeaseKVSSerializerTable

    def perform_create(self, serializer):
        serializer.save(date=datetime.datetime.now())

    def perform_update(self, serializer):
        if serializer.validated_data.get('status') == 'canceled' and \
                        serializer.validated_data.get('status') != serializer.instance.status and \
                serializer.validated_data.get('bill'):
            r = requests.get("http://%s/hs/Expobit/AccountClosed/%s" % (
                settings.ONE_C_URL, serializer.validated_data.get('bill')),
                             cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                             auth=settings.ONE_C_AUTH)
            if r.status_code == 404:
                if manage_data.update_session() == 200:
                    r = requests.get("http://%s/hs/Expobit/AccountClosed/%s" % (
                    settings.ONE_C_URL, serializer.validated_data.get('bill')),
                                     cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                                     auth=settings.ONE_C_AUTH)
        serializer.save()

# Контактные лица


class ApiSetContactPersonView(ModelViewSetForPhoneEmail):
    queryset = ContactPerson.objects.all()
    serializer_class = ContactPersonSerializer
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('client', 'active')
    ordering = ('name',)

    def perform_update(self, serializer):
        self.create_or_update_contacts(serializer, removed=False)
        serializer.save()

    def perform_create(self, serializer):
        serializer.save(active=True)
        self.create_or_update_contacts(serializer, removed=True)


class ApiSetClientsGroupView(viewsets.ModelViewSet):
    queryset = ClientGroup.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = ClientsGroupSerializer

    @list_route(methods=['put'])
    def set_group(self, request):
        pk = request.data.get('pk')
        group = request.data.get('value')
        obj = Membership.objects.get(pk=pk)
        qs = ClientGroup.objects.filter(client=obj.client, subjects=obj.project.subjects)
        if qs:
            qs[0].group = group
            qs[0].save()
            return Response(status=200, content_type='application/json',
                            data={'success': 'true', 'update': False})
        ClientGroup.objects.create(group=group, client=obj.client, subjects=obj.project.subjects)
        return Response(status=201, content_type='application/json',
                        data={'success': 'true', 'update': False})

    @list_route(methods=['put'])
    def set_group_from_section(self, request):
        if request.data.get('many'):
            project = Project.objects.get(pk=request.data.get('project'))
            group = request.data.get('group')
            arr_pk = request.data.getlist('values[]')
            for i in arr_pk:
                qs = ClientGroup.objects.filter(client_id=int(i), subjects=project.subjects)
                if qs:
                    qs[0].group = group
                    qs[0].save()
                else:
                    ClientGroup.objects.create(group=group, client_id=int(i), subjects=project.subjects)
            return Response(status=200, content_type='application/json',
                            data={'success': 'true', 'update': True})
        else:
            pk = request.data.get('pk')
            group = request.data.get('value')
            project = Project.objects.get(pk=request.data.get('project'))
            qs = ClientGroup.objects.filter(client_id=pk, subjects=project.subjects)
            if qs:
                qs[0].group = group
                qs[0].save()
                return Response(status=200, content_type='application/json',
                                data={'success': 'true', 'update': False})
            ClientGroup.objects.create(group=group, client_id=pk, subjects=project.subjects)
            return Response(status=201, content_type='application/json',
                            data={'success': 'true', 'update': False})


class ApiSetProjectTableView(viewsets.ModelViewSet):
    serializer_class = ProjectTableSerializer
    permission_classes = (DjangoModelPermissions, )

    def get_queryset(self):
        client = self.request.GET.get('client', None)
        return Membership.objects.filter(client=client)


class SearchAddressView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        search_string = request.GET.get('query')
        r = requests.get(url='https://maps.googleapis.com/maps/api/place/autocomplete/'
                             'json?language=ru&region=RU'
                             '&input=' + search_string + '&key=%s' % settings.API_KEY_GOOGLE)
        # r = requests.get(url='https://maps.googleapis.com/maps/api/geocode/'
        #                      'json?language=ru&'
        #                      'region=ru&address='+search_string+'&key=%s' % (settings.API_KEY_GOOGLE_ADDRESS, ))
        return Response(content_type='application/json', data=r.json(), status=200)

    def post(self, request):
        r = requests.get(url='https://maps.googleapis.com/maps/api/place/details/'
                             'json?language=ru&region=RU'
                             '&place_id=%s&key=%s' % (request.data.get('place_id'), settings.API_KEY_GOOGLE))
        result = r.json().get('result')
        address_components = result.get('address_components')
        city = None
        country = None
        region = None
        for i in address_components:
            if i['types'][0] == 'locality':
                city = i['long_name']
            if i['types'][0] == 'administrative_area_level_1':
                region = i['long_name']
            if i['types'][0] == 'country':
                country = i['long_name']
        if city and region and country:
            country_obj, created_contry = Country.objects.get_or_create(name=country)
            city_obj, created_city = City.objects.get_or_create(name=city)
            region_obj, created_region = Region.objects.get_or_create(name=region)
        elif city and country:
            country_obj, created_contry = Country.objects.get_or_create(name=country)
            city_obj, created_city = City.objects.get_or_create(name=city)
            # region_obj, created_region = Region.objects.get_or_create(name=region)
        else:
            return Response(content_type='application/json', data={
                'city': city,
                'region': region,
                'country': country
            }, status=400)
        if region:
            city_obj.region = region_obj
        city_obj.country = country_obj
        city_obj.save()
        if country and region:
            region_obj.country = country_obj
            region_obj.save()
        return Response(content_type='application/json', data={
            'city': city_obj.id,
            'geometry': result['geometry']
        }, status=200)


class SearchTaxView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        search_string = request.GET.get('query')
        headers = {
            'Authorization': 'Token %s' % settings.API_KEY_NALOG,
            'Content-Type': 'application/json',
        }
        data = {
            'query': search_string
        }
        r = requests.post(url='https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party',
                          headers=headers, json=data)
        return Response(content_type='application/json', data=r.json(), status=200)


class ApiSetOtherInterestView(viewsets.ModelViewSet):
    serializer_class = OtherInterestSerializer
    permission_classes = (DjangoModelPermissions, )
    queryset = OtherInterest.objects.all()

    def perform_create(self, serializer):
        project_type = serializer.validated_data.get('new_interest')
        client = serializer.validated_data.get('client')
        com = serializer.validated_data.get('com')
        contact_person = serializer.validated_data.get('contact_person')
        contact_data = serializer.validated_data.get('contact_data')
        Channel('chat-messages').send({'user': project_type.director_id,
                                       'push': 'Поступил интерес от клиента на ваши услуги',
                                       'username': project_type.director.username,
                                       'message': '<a href="#client/%s">%s</a><br>%s - %s<br>%s' % (client.id,
                                                                                                    client.name,
                                                                                                    contact_person,
                                                                                                    contact_data, com),
                                       'sender': self.request.user.pk})
        serializer.save()


class ApiSetClientActivityView(viewsets.ModelViewSet):
    serializer_class = ClientActivitySerializer
    permission_classes = (DjangoModelPermissions, )
    queryset = ClientActivity.objects.all()


class ApiSetCityView(viewsets.ModelViewSet):
    serializer_class = CitySerializer
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (filters.OrderingFilter,)
    ordering = ('name',)
    queryset = City.objects.all()


class ApiSetDiplomaNominationView(viewsets.ModelViewSet):
    queryset = DiplomaNomination.objects.all()
    serializer_class = DiplomaNominationSerializer
    permission_classes = (DjangoModelPermissions, )


class ApiSetRegionView(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionsSerializer
    permission_classes = (DjangoModelPermissions,)


class ApiSetPayAppView(viewsets.ModelViewSet):
    queryset = PayApp.objects.all()
    serializer_class = PayAppSerializer
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('expo', 'lease_kvs', 'ad')
    ordering = ('date',)


# Create your views here.
