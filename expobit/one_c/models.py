from django.db import models


class Nomenclature(models.Model):
    guid = models.CharField(max_length=36, verbose_name='guid', db_index=True, primary_key=True, unique=True)
    name = models.CharField(max_length=128, verbose_name='Название')
    units = models.ForeignKey('Units', blank=True, null=True)


class Organizations(models.Model):
    guid = models.CharField(max_length=36, verbose_name='guid', db_index=True, primary_key=True, unique=True)
    description = models.CharField(max_length=36, verbose_name='Описание')


class TypePrice(models.Model):
    description = models.CharField(max_length=128, verbose_name='Описание')
    # guid = models.CharField(max_length=36, verbose_name='guid', blank=True)


class Price(models.Model):
    nomenclature = models.ForeignKey(Nomenclature)
    type_price = models.ForeignKey(TypePrice)
    price = models.IntegerField(verbose_name='Цена')


class Units(models.Model):
    guid = models.CharField(max_length=36, verbose_name='guid', db_index=True, primary_key=True, unique=True)
    description = models.CharField(max_length=10, verbose_name='Описание')


class OneCSession(models.Model):
    created = models.DateTimeField(auto_now=True)
    session_key = models.CharField(max_length=36)
# Create your models here.
