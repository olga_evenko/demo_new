from django.core.cache import cache
import requests
from rest_framework.exceptions import ValidationError
from .models import OneCSession
from django.conf import settings
import time
import json
from django.db import connections


def dictfetchall(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def get_query_filter(params):
    query_string = ''
    for i in params:
        if params[i]:
            if not query_string:
                query_string += "WHERE st='p' AND "
                if '__gt' in i:
                    query_string += "%s>='%s'" % (i.split('__')[0], params[i])
                elif '__lt' in i:
                    query_string += "%s<='%s'" % (i.split('__')[0], params[i])
                else:
                    if ',' in str(params[i]):
                        query_string += '%s IN (%s)' % (i, params[i])
                    else:
                        query_string += '%s=%s' % (i, params[i])
            else:
                if '__gt' in i:
                    query_string += "AND %s>='%s'" % (i.split('__')[0], params[i])
                elif '__lt' in i:
                    query_string += "AND %s<='%s'" % (i.split('__')[0], params[i])
                else:
                    if ',' in str(params[i]):
                        query_string += 'AND %s IN (%s)' % (i, params[i])
                    else:
                        query_string += 'AND %s=%s' % (i, params[i])
                    # query_string += 'AND %s=%s' % (i, params[i])
                # query_string += 'AND %s=%s' % (i, params[i])
    return query_string


def update_session():
    if not cache.get('update_session'+settings.MEMCACHED_NAME):
        cache.set('update_session'+settings.MEMCACHED_NAME, True, None)
        rq = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
                          headers={'IBSession': 'start'})
        if rq.status_code == 200:
            qs = OneCSession.objects.create(session_key=rq.cookies.get('ibsession'))
            cache.set('one_c_session_key'+settings.MEMCACHED_NAME, rq.cookies.get('ibsession'), None)
            cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
        else:
            cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
    else:
        while cache.get('update_session'+settings.MEMCACHED_NAME):
            time.sleep(0.01)
        return 200
    return rq.status_code


def getSumTable(first_month='', last_month='', project_id='', manager_id='', client_id='', barter='0'):
    r = requests.get("http://%s/hs/Expobit/BreakThePayment/" % (settings.ONE_C_URL,),
                     auth=settings.ONE_C_AUTH, cookies={
            'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)
        },
                     json={
                         'НачДата': str(first_month),
                         'КонДата': str(last_month),
                         'project_id': project_id,
                         'manager_id': manager_id,
                         'client_id': client_id,
                         'Бартер': barter
                     })
    if r.status_code == 404:
        if update_session() == 200:
            r = requests.get(
                "http://%s/hs/Expobit/BreakThePayment/" % (settings.ONE_C_URL,),
                auth=settings.ONE_C_AUTH, cookies={'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)},
                json={
                    'НачДата': str(first_month),
                    'КонДата': str(last_month),
                    'project_id': project_id,
                    'manager_id': manager_id,
                    'client_id': client_id,
                    'Бартер': barter
                })
    json_str = r.text.replace('\xa0', '')
    d = json.loads(json_str[1:len(json_str) - 1])
    try:
        cursor = connections['trial'].cursor()
    except:
        return json.dumps(d)

    cursor.execute("SELECT * FROM log_table %s" % get_query_filter({
                    'date::date__gt': str(first_month),
                    'date::date__lt': str(last_month),
                    'p_id': project_id,
                    'm_id': manager_id,
                    'c_id': client_id
                }))
    trial_data = dictfetchall(cursor)
    trial_data = map(lambda x: {
        'ГУИД_счета': x['b'],
        'МенеджерИД': str(x['m_id']),
        'ПроектИД': str(x['p_id']),
        'ОплаченнаяСумма': x['s'],
        'КлиентИД': str(x['c_id']),
        'ЗаявкаИД': str(x['a_id']),
        'Дата': x['date'].strftime("%d.%m.%Y %H:%M:%S")
    }, trial_data)
    trial_data = list(trial_data)
    if d == [{}]:
        if trial_data:
            return json.dumps(list(trial_data))
        else:
            return '[{}]'
    return json.dumps(list(trial_data)+d)


def getSumTableBill(first_month='', last_month='', project_id='', manager_id='', client_id='', barter='0'):
    r = requests.get("http://%s/hs/Expobit/GetSummaryTable/" % (settings.ONE_C_URL,),
                     auth=settings.ONE_C_AUTH, cookies={
            'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)
        },
                     json={
                         'НачДата': str(first_month),
                         'КонДата': str(last_month),
                         'project_id': project_id,
                         'manager_id': manager_id,
                         'client_id': client_id,
                         'Бартер': barter
                     })
    if r.status_code == 404:
        if update_session() == 200:
            r = requests.get(
                "http://%s/hs/Expobit/GetSummaryTable/" % (settings.ONE_C_URL,),
                auth=settings.ONE_C_AUTH, cookies={'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)},
                json={
                    'НачДата': str(first_month),
                    'КонДата': str(last_month),
                    'project_id': project_id,
                    'manager_id': manager_id,
                    'client_id': client_id,
                    'Бартер': barter
                })
    json_str = r.text.replace('\xa0', '')
    d = json.loads(json_str[1:len(json_str) - 1])
    try:
        cursor = connections['trial'].cursor()
    except:
        return json.dumps(d)

    cursor.execute("SELECT * FROM log_table %s ORDER BY date ASC" % get_query_filter({
        'date::date__gt': str(first_month),
        'date::date__lt': str(last_month),
        'p_id': project_id,
        'm_id': manager_id,
        'c_id': client_id
    }))
    trial_data = dictfetchall(cursor)
    temp_trial = {}
    for i in trial_data:
        t = temp_trial.setdefault(i['b'], {'s': []})
        t['s'].append(i['s'])
        t['date'] = i['date']
        t['last_p'] = i['s']
    for i in d:
        if i.get('ГУИД_счета') and temp_trial.get(i['ГУИД_счета']):
            i['ОплаченнаяСумма'] = float(i['ОплаченнаяСумма']) + sum(temp_trial.get(i['ГУИД_счета']).get('s'))
            i['НеоплаченнаяСумма'] = round(float(i['НеоплаченнаяСумма']) - (sum(temp_trial.get(i['ГУИД_счета']).get('s'))), 2)
            i['ДатаОплаты'] = temp_trial.get(i['ГУИД_счета']).get('date').strftime('%d.%m.%Y %H:%M:%S')
            i['СуммаПоследнейОплаты'] = temp_trial.get(i['ГУИД_счета']).get('last_p')
    return json.dumps(d)


def getProjectPerformance(project_id):
    r = requests.get("http://%s/hs/Expobit/GetProjectPerformance/%s" % (settings.ONE_C_URL, project_id),
                     auth=settings.ONE_C_AUTH, cookies={
            'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)
        })
    if r.status_code == 404:
        if update_session() == 200:
            r = requests.get(
                "http://%s/hs/Expobit/GetProjectPerformance/%s" % (settings.ONE_C_URL, project_id),
                auth=settings.ONE_C_AUTH,
                cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},)
    if r.status_code == 500:
        raise ValidationError(r.text)
    json_str = r.text.replace('\xa0', '')
    try:
        return json.loads(json_str[1:len(json_str) - 1].replace('\\', ''))
    except:
        return [{}]


def getSumTableFull(first_month='', last_month='', project_id='', manager_id='', client_id=''):
    r = requests.get("http://%s/hs/Expobit/GetSummaryTableFull/" % (settings.ONE_C_URL,),
                     auth=settings.ONE_C_AUTH, cookies={
            'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)
        },
                     json={
                         'НачДата': str(first_month),
                         'КонДата': str(last_month),
                         'project_id': project_id,
                         'manager_id': manager_id,
                         'client_id': client_id
                     })
    if r.status_code == 404:
        if update_session() == 200:
            r = requests.get(
                "http://%s/hs/Expobit/GetSummaryTable/" % (settings.ONE_C_URL,),
                auth=settings.ONE_C_AUTH, cookies={'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)},
                json={
                    'НачДата': str(first_month),
                    'КонДата': str(last_month),
                    'project_id': project_id,
                    'manager_id': manager_id,
                    'client_id': client_id
                })

    return r.json()


def getSumTableExcel(first_month='', last_month='', project_id='', manager_id='', client_id=''):
    r = requests.get("http://%s/hs/Expobit/GetSummaryTableExcel/" % (settings.ONE_C_URL,),
                     auth=settings.ONE_C_AUTH, cookies={
            'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)
        },
                     json={
                         'НачДата': str(first_month),
                         'КонДата': str(last_month),
                         'project_id': project_id,
                         'manager_id': manager_id,
                         'client_id': client_id
                     })
    if r.status_code == 404:
        if update_session() == 200:
            r = requests.get(
                "http://%s/hs/Expobit/GetSummaryTableExcel/" % (settings.ONE_C_URL,),
                auth=settings.ONE_C_AUTH, cookies={'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)},
                json={
                    'НачДата': str(first_month),
                    'КонДата': str(last_month),
                    'project_id': project_id,
                    'manager_id': manager_id,
                    'client_id': client_id
                })

    return r


def getActs(project_id='', date=''):
    r = requests.post("http://%s/hs/Customers/GetActs" % (settings.ONE_C_URL,),
                     auth=settings.ONE_C_AUTH, cookies={
            'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME),
        }, json={'project_id': project_id, 'data_project': date})
    if r.status_code == 404:
        if update_session() == 200:
            r = requests.post(
                "http://%s/hs/test/GetActs" % (settings.ONE_C_URL,),
                auth=settings.ONE_C_AUTH, cookies={'IBSession': cache.get('one_c_session_key'+settings.MEMCACHED_NAME)},
                json={'project_id': project_id, 'data_project': date})

    return r


def transliterate(name):
    dictionary = {'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e',
                  'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'i', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
                  'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
                  'ц': 'c', 'ч': 'cz', 'ш': 'sh', 'щ': 'scz', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e',
                  'ю': 'u', 'я': 'ja', 'А': 'a', 'Б': 'b', 'В': 'v', 'Г': 'g', 'Д': 'd', 'Е': 'e', 'Ё': 'e',
                  'Ж': 'zh', 'З': 'z', 'И': 'i', 'Й': 'i', 'К': 'k', 'Л': 'l', 'М': 'm', 'Н': 'n',
                  'О': 'o', 'П': 'p', 'Р': 'r', 'С': 's', 'Т': 't', 'У': 'u', 'Ф': 'Х', 'х': 'h',
                  'Ц': 'c', 'Ч': 'cz', 'Ш': 'sh', 'Щ': 'scz', 'Ъ': '', 'Ы': 'y', 'Ь': '', 'Э': 'e',
                  'Ю': 'u', 'Я': 'ja', ',': '', '?': '', ' ': '_', '~': '', '!': '', '@': '', '#': '',
                  '$': '', '%': '', '^': '', '&': '', '*': '', '(': '', ')': '', '-': '', '=': '', '+': '',
                  ':': '', ';': '', '<': '', '>': '', '\'': '', '"': '', '\\': '', '/': '', '№': '',
                  '[': '', ']': '', '{': '', '}': '', 'ґ': '', 'ї': '', 'є': '', 'Ґ': 'g', 'Ї': 'i',
                  'Є': 'e'}
    for key in dictionary:
        name = name.replace(key, dictionary[key])
    return name
