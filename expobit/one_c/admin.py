from django.contrib import admin
from django.apps import apps
from .models import *

for model in apps.get_app_config('one_c').models.values():
    admin.site.register(model)

# Register your models here.
