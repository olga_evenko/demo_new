from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from rest_framework import views
from rest_framework import viewsets
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework import filters
from io import StringIO
import requests
from django.conf import settings
from collections import OrderedDict
# from .urls import update_session as u_s
from dateutil.relativedelta import relativedelta
from .models import *
from .serializers import PriceSerializer, PriceSerializerRO, OrganizationsSerializer
from projects.models import Project
import requests
import json
import re
from workers.tasks import get_acts
from clients.models import ApplicationExpo, ApplicationAd, ApplicationLeaseKVS, Entity
from django.core.cache import cache
import time
from one_c import manage_data
from celery.result import AsyncResult
import datetime


def update_session():
    if not cache.get('update_session' + settings.MEMCACHED_NAME):
        cache.set('update_session' + settings.MEMCACHED_NAME, True, None)
        rq = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
                          headers={'IBSession': 'start'})
        if rq.status_code == 200:
            qs = OneCSession.objects.create(session_key=rq.cookies.get('ibsession'))
            cache.set('one_c_session_key' + settings.MEMCACHED_NAME, rq.cookies.get('ibsession'), None)
            cache.set('update_session' + settings.MEMCACHED_NAME, False, None)
        else:
            cache.set('update_session' + settings.MEMCACHED_NAME, False, None)
    else:
        while cache.get('update_session' + settings.MEMCACHED_NAME):
            time.sleep(0.01)
        return 200
    return rq.status_code


class SyncDBOneC(views.APIView):
    permission_classes = (IsAuthenticated,)

    def sync_organizations(self):
        r = requests.get("http://%s/odata/standard.odata/"
                         "Catalog_Организации/?$format=json" % (settings.ONE_C_URL,),
                         auth=settings.ONE_C_AUTH)
        data = r.json()
        for i in data['value']:
            Organizations.objects.update_or_create(guid=i['Ref_Key'], defaults={'description': i['Description']})

    def sync_units(self):
        r = requests.get("http://%s/odata/standard.odata/"
                         "Catalog_КлассификаторЕдиницИзмерения/?$format=json" % (settings.ONE_C_URL,),
                         auth=settings.ONE_C_AUTH)
        data = r.json()
        for i in data['value']:
            Units.objects.update_or_create(guid=i['Ref_Key'], description=i['Description'])

    def sync_nomenclature(self):
        r_nomenclature = requests.get("http://%s/odata/standard.odata/"
                                      "Catalog_Номенклатура/?$format=json" % (settings.ONE_C_URL,),
                                      auth=settings.ONE_C_AUTH)
        data_nomenclature = r_nomenclature.json()
        for i in data_nomenclature['value']:
            try:
                Nomenclature.objects.update_or_create(guid=i['Ref_Key'],
                                                      name=i['Description'],
                                                      defaults={'units_id': i['ЕдиницаИзмерения_Key']})
            except:
                print(1)

    def get(self, request):
        if request.user.is_superuser:
            if request.GET.get('units'):
                self.sync_units()
            if request.GET.get('nomenclature'):
                self.sync_nomenclature()
            if request.GET.get('organizations'):
                self.sync_organizations()
            # r_nomenclature = requests.get("http://%s/odata/standard.odata/"
            #                  "Catalog_Номенклатура/?$format=json" % (settings.ONE_C_URL,),
            #                  auth=settings.ONE_C_AUTH)
            # data_nomenclature = r_nomenclature.json()
            # for i in data_nomenclature['value']:
            #     Nomenclature.objects.update_or_create(guid=i['Ref_Key'],
            # name=i['Description'], units=i['ЕдиницаИзмерения_Key'])
            r = requests.get("http://%s/odata/standard.odata/"
                             "Document_УстановкаЦенНоменклатуры/?$format=json" % (settings.ONE_C_URL,),
                             auth=settings.ONE_C_AUTH)
            data = r.json()
            data_for_db = {}
            for i in data['value']:
                if i['Информация'] not in data_for_db:
                    data_for_db[i['Информация']] = [{'Товары': i['Товары'], 'date': i['Date']}]
                else:
                    data_for_db[i['Информация']].append({'Товары': i['Товары'], 'date': i['Date']})
                    data_for_db[i['Информация']].sort(key=lambda r: r['date'])
            for k, v in data_for_db.items():
                qs = TypePrice.objects.update_or_create(description=k)
                for i in data_for_db[k]:
                    for j in i['Товары']:
                        Price.objects.update_or_create(nomenclature_id=j['Номенклатура_Key'],
                                                       defaults={'price': j['Цена'], 'type_price': qs[0]})
            return Response(status=200, data={'success': 'start updated'})
        else:
            return Response(status=403, data={'error': 'not permissions'})


class ApiSetPriceView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions,)
    serializer_class = PriceSerializerRO

    def get_queryset(self):
        project = Project.objects.get(pk=self.request.GET.get('project'))
        if project:
            return Price.objects.filter(type_price__description=project.subjects.subject_name)
        return Price.objects.all()

        # def get_serializer_class(self):
        #     if self.request.method.upper() in settings.SAFE_METHODS:
        #         return PriceSerializerRO
        #     else:
        #         return PriceSerializer


class ApiSetOrganizationsView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions,)
    serializer_class = OrganizationsSerializer
    queryset = Organizations.objects.all()


class PrintOneC(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get_document(self, guid):
        s = requests.Session()
        auth = s.post(
            url='http://dbserv/1c8/ru_RU/e1cib/login?version=8.3.10.2639&cred=QWRtaW46MTIzNDU2&nooida&vl=ru_RU&clnId=f90c98dc-d2c6-d7de-b998-14e0829e8b01')
        resp = json.loads(auth.text.encode('utf8'))
        session_id = resp['response']['seance']
        headers = {'content-type': 'application/json; charset=UTF-8'}
        data_json = '{"root":{"key":"ОбщаяФорма.ПечатьДокументов","prms":{"prm":[{"name":"ИмяМенеджераПечати","#val":"9B6ABF8B-0173-48E5-B0A0-83B21FCF63C5","val":"Обработка.ПечатьСчетаНаОплату"},{"name":"ИменаМакетов","#val":"9B6ABF8B-0173-48E5-B0A0-83B21FCF63C5","val":"СчетЗаказ"},{"name":"ПараметрКоманды","#val":"51E7A0D2-530B-11D4-B98A-008048DA3034","val":{"#Value":["65752CB5-2914-412E-9C55-A4FE9273C207"],"Value":["' + guid + '"]}},{"name":"ПараметрыПечати","#val":"4238019D-7E49-4FC9-91DB-B6B951D5CF8E","val":{"Property":[{"name":"ЗаголовокФормы","#Value":"65752CB5-2914-412E-9C55-A4FE9273C207","Value":"' + guid + '"},{"name":"ДополнительныеПараметры","#Value":"4238019D-7E49-4FC9-91DB-B6B951D5CF8E","Value":{"Property":[{"name":"ДополнитьКомплектВнешнимиПечатнымиФормами","#Value":"5D4125AD-F6E7-4313-BE32-F71D0AB60915","Value":false}]}}]}}]},"mw":0,"mh":0,"cw":0,"ch":0,"pres":false,"trdata":false,"cf":false}}'
        query_params = s.post(url='http://dbserv/1c8/ru_RU/e1cib/logForm?cmd=query', data=data_json.encode('utf8'),
                              headers=headers,
                              cookies={'vrs_session': session_id, 'vrs_rc': '604', 'vrs_client_alive': '1',
                                       'conftitle': '%D0%91%D1%83%D1%85%D0%B3%D0%B0%D0%BB%D1%82%D0%B5%D1%80%D0%B8%D1%8F%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D1%8F%2C%20%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%203.0%20'})
        result = re.findall(r'"fid":"\w+-\w+-\w+-\w+-\w+', query_params.text)
        query_uid = result[0].replace('"', '')
        query_uid = query_uid.split(':')[1]
        result = re.findall(r'"id":"\w+:\w+:\w+:\w+:\w+-\w+-\w+-\w+-\w+', query_params.text)
        query_id = result[0].replace('"', '')
        print(query_uid, query_id)
        query_id = query_id[3:]
        fover = re.findall(r'"fover":"\w+-\w+-\w+-\w+-\w+', query_params.text)
        fover = fover[0].replace('"', '')
        fover = fover.split(':')[1]
        data_json = '{"root":{"rKey":"' + query_uid.upper() + '","id":"' + query_id + '","fid":"' + query_uid + '","fover":"' + fover + '","prm":{"name":"УстановитьТекущийТабличныйДокумент","#param":["9B6ABF8B-0173-48E5-B0A0-83B21FCF63C5"],"param":["ПечатнаяФорма1"]},"pchs":{},"prms":{"descriptions":{"remoteKey":"","trackChanges":false,"sinDe":0,"seqDe":0,"sinUo":0,"seqUo":0,"structure":[{"name":"parameters","field":[{"id":"0","name":"ИмяМенеджераПечати","type":{"length":0,"__content":"9b6abf8b-0173-48e5-b0a0-83b21fcf63c5"}},{"id":"2","name":"ПараметрКоманды","type":{"__content":""}},{"id":"3","name":"ПараметрыПечати","type":{"__content":""}},{"id":"4","name":"ИсточникДанных","type":{"__content":""}},{"id":"5","name":"ПараметрыИсточника","type":{"__content":""}}]}]},"type":"parameters","data":{"#v":[undefined,"51E7A0D2-530B-11D4-B98A-008048DA3034","4238019D-7E49-4FC9-91DB-B6B951D5CF8E",undefined,undefined],"v":["Обработка.ПечатьСчетаНаОплату",{"#Value":["65752CB5-2914-412E-9C55-A4FE9273C207"],"Value":["5719334a-eb50-11e5-8526-10c37b94ecb0"]},{"Property":[{"name":"ЗаголовокФормы","#Value":"65752CB5-2914-412E-9C55-A4FE9273C207","Value":"5719334a-eb50-11e5-8526-10c37b94ecb0"},{"name":"ДополнительныеПараметры","#Value":"4238019D-7E49-4FC9-91DB-B6B951D5CF8E","Value":{"Property":[{"name":"ДополнитьКомплектВнешнимиПечатнымиФормами","#Value":"5D4125AD-F6E7-4313-BE32-F71D0AB60915","Value":false}]}}]},undefined,undefined]}},"dd":{"m":{"ce":117,"attl":"Печать документов"},"f":[{"id":117,"ssr":0,"ssc":0,"ser":0,"sec":0}],"g":[{"id":9,"cp":"СтраницаТекущаяПечатнаяФорма"}]}}}'
        print(query_uid, query_id, fover)
        a = s.post(url='http://dbserv/1c8/ru_RU/e1cib/logForm?cmd=call', data=data_json.encode('utf8'), headers=headers,
                   cookies={'vrs_session': session_id, 'vrs_rc': '604', 'vrs_client_alive': '1',
                            'conftitle': '%D0%91%D1%83%D1%85%D0%B3%D0%B0%D0%BB%D1%82%D0%B5%D1%80%D0%B8%D1%8F%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D1%8F%2C%20%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%203.0%20'})
        a = s.post(url='http://dbserv/1c8/ru_RU/e1cib/logForm?cmd=queryLayouter', data=data_json.encode('utf8'),
                   headers=headers, cookies={'vrs_session': session_id, 'vrs_rc': '604', 'vrs_client_alive': '1',
                                             'conftitle': '%D0%91%D1%83%D1%85%D0%B3%D0%B0%D0%BB%D1%82%D0%B5%D1%80%D0%B8%D1%8F%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D1%8F%2C%20%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%203.0%20'})
        data_json = '{"root":{"rKey":"' + query_uid.upper() + '","formuuid":"' + query_uid + '","props":{},"path":"10","type":0,"begin":0,"end":55,"cur":0,"widthPix":-1,"heightPix":-1,"brType":1,"compact":false,"svgSupported":true,"flags":0,"zoom":100,"showRowAndColumnNames":false,"showCellNames":false,"pointerType":0,"wndWidthPix":0}}'
        a = s.post(url='http://dbserv/1c8/ru_RU/e1cib/moxel?cmd=getVisualContents', data=data_json.encode('utf8'),
                   headers=headers, cookies={'vrs_session': session_id, 'vrs_rc': '604', 'vrs_client_alive': '1',
                                             'conftitle': '%D0%91%D1%83%D1%85%D0%B3%D0%B0%D0%BB%D1%82%D0%B5%D1%80%D0%B8%D1%8F%20%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D1%8F%2C%20%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%203.0%20'})
        page = json.loads(a.text.encode('utf8'))
        print(page)
        doc = s.get(url='http://dbserv/1c8/ru_RU/' + page['request']['rightContentURL'])
        return doc

    def get(self, request):
        guid = self.request.GET.get('guid')
        type = self.request.GET.get('type')
        format_file = self.request.GET.get('formatfile')
        response = HttpResponse(content_type='application/pdf')
        if 'docx' in type:
            response['Content-Disposition'] = 'attachment; filename="%s.docx"' % (type,)
        elif 'xls' in type:
            response['Content-Disposition'] = 'attachment; filename="%s.xls"' % (type,)
        else:
            response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % (type,)
        if format_file == 'pdf':
            response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % (type,)
        elif format_file == 'docx':
            response['Content-Disposition'] = 'attachment; filename="%s.docx"' % (type,)
        elif format_file == 'xls':
            response['Content-Disposition'] = 'attachment; filename="%s.xls"' % (type,)
        response['Content-Transfer-Encoding'] = 'binary'
        if type == 'bill_docx':
            type = 'PrintAccountDOC'
        elif type == 'act_xls':
            type = 'GetPrintActXLS'
        elif type == 'contract_docx':
            type = 'PrintContractDOC'
        elif type == 'bill_xls':
            type = 'PrintAccountXLS'
        elif type == 'contract_xls':
            type = 'PrintContractXLS'
        elif type == 'bill':
            type = 'PrintAccount'
        elif type == 'act':
            type = 'PrintAct'
        elif type == 'contract':
            type = 'PrintContract'
        elif type == 'contract_add':
            type = 'PrintContractAdd'
        elif type == 'contract_add_docx':
            type = 'PrintContractAddDOC'
        elif type == 'contract_traning':
            type = 'PrintContractConsalting'
        elif type == 'contract_traning_xls':
            type = 'PrintContractConsaltingXLS'
        elif type == 'contract_traning_docx':
            type = 'PrintContractConsaltingDOC'
        r = requests.get("http://%s/hs/Expobit/%s/%s" % (settings.ONE_C_URL, type, guid),
                         cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                         auth=settings.ONE_C_AUTH)
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.get("http://%s/hs/Expobit/%s/%s" % (settings.ONE_C_URL, type, guid),
                                 cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                                 auth=settings.ONE_C_AUTH)
        response.write(r.content)
        return response


class Reports(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        project_id = request.GET.get('project')
        project = Project.objects.get(pk=project_id)
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="%s.xls"' % (manage_data.transliterate(project.name),)
        response['Content-Transfer-Encoding'] = 'binary'
        response.write(manage_data.getSumTableExcel(project_id=project_id).content)
        return response


class GetActs(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        if not request.GET.get('work'):
            project_id = request.GET.get('project')
            date = request.GET.get('date_end')
            work = get_acts.delay(project_id, date)
            return JsonResponse(data={'work_id': work.id}, safe=False)
        work = AsyncResult(request.GET.get('work'))
        if request.GET.get('load'):
            return work.result
        return JsonResponse(data={'status': work.status}, safe=False)

        # project_id = request.GET.get('project')
        # date = request.GET.get('date_start')
        # get_acts.delay(request)
        # project = Project.objects.get(pk=project_id)
        # response = HttpResponse(content_type='application/zip')
        # response['Content-Disposition'] = 'attachment; filename="%s.zip"' % (manage_data.transliterate(project.name),)
        # response['Content-Transfer-Encoding'] = 'binary'
        # response.write(manage_data.getActs(project_id=project_id, date=date).content)
        return JsonResponse(data={}, safe=False)


class SetApp(views.APIView):
    permission_classes = (IsAuthenticated,)

    def patch(self, request):
        data = request.data
        try:
            app_id = int(data.get('app_id'))
        except:
            return Response(status=400, data={'error': 'unknown type'})
        if data.get('app_type') == 'expo' and data.get('Status') == 'Оплачен':
            ApplicationExpo.objects.filter(id=app_id).update(status='finished')
        elif data.get('app_type') == 'ad' and data.get('Status') == 'Оплачен':
            ApplicationAd.objects.filter(id=app_id).update(status='finished')
        elif data.get('app_type') == 'lease_kvs' and data.get('Status') == 'Оплачен':
            ApplicationLeaseKVS.objects.filter(id=app_id).update(status='finished')
        return Response(status=200, data={'status': 'ok'})


class BankOrgOneC(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        r = requests.get("http://%s/odata/standard.odata/"
                         "Catalog_Банки?$format=json" % (settings.ONE_C_URL,),
                         auth=settings.ONE_C_AUTH, )
        return Response(data=r.json(), status=r.status_code)


class BankOneC(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        guid = request.GET.get('guid')
        r = requests.get("http://%s/odata/standard.odata/"
                         "Catalog_БанковскиеСчета?$format=json&"
                         "$filter=Owner eq cast(guid'%s','Catalog_Контрагенты')" % (settings.ONE_C_URL, guid),
                         auth=settings.ONE_C_AUTH,)
        return Response(data=r.json(), status=r.status_code)

    def post(self, request):
        data = request.data
        r = requests.post("http://%s/odata/standard.odata/"
                            "Catalog_БанковскиеСчета"
                            "?$format=json" % (settings.ONE_C_URL, ),
                            auth=settings.ONE_C_AUTH,
                            json=data)
        resp_data = r.json()
        if data.get('main') and r.status_code == 201:
            rr = requests.patch("http://%s/odata/standard.odata/"
                               "Catalog_Контрагенты"
                               "(guid'%s')?$format=json" % (settings.ONE_C_URL, resp_data['Owner']),
                               auth=settings.ONE_C_AUTH,
                               json={'ОсновнойБанковскийСчет_Key': resp_data['Ref_Key']})
            if rr.status_code == 200:
                resp_data['main'] = True
        return Response(data=resp_data, status=r.status_code)

    def patch(self, request):
        data = request.data
        r = requests.patch("http://%s/odata/standard.odata/"
                          "Catalog_БанковскиеСчета"
                          "(guid'%s')?$format=json" % (settings.ONE_C_URL, data['Ref_Key']),
                          auth=settings.ONE_C_AUTH,
                          json=data)
        resp_data = r.json()
        if data.get('main') and r.status_code == 200:
            rr = requests.patch("http://%s/odata/standard.odata/"
                               "Catalog_Контрагенты"
                               "(guid'%s')?$format=json" % (settings.ONE_C_URL, resp_data['Owner']),
                               auth=settings.ONE_C_AUTH,
                               json={'ОсновнойБанковскийСчет_Key': resp_data['Ref_Key']})
            if rr.status_code == 200:
                resp_data['main'] = True
        return Response(data=resp_data, status=r.status_code)


class BillOneC(views.APIView):
    permission_classes = (IsAuthenticated,)

    def patch(self, request):
        data = request.data
        s=0
        data['Товары'] = list(map(lambda x: {'LineNumber': int(x['LineNumber']) + 1,
                                             'ПроцентСкидки': x['discount_rate'],
                                             'Наценка': x['markup'],
                                             'Номенклатура_Key': x['nomenclature_guid'],
                                             'Сумма': x['total'],
                                             'count_days': x['count_days'],
                                             'Цена': x['price'],
                                             'Содержание': x['comment'],
                                             'Количество': x['count']}, data['Товары'],))
        for i in data['Товары']:
            s += i['Сумма']
        data['СуммаДокумента'] = s
        entity = Entity.objects.get(pk=request.data.get('entity_id'))
        project = Project.objects.get(pk=request.data.get('project_id'))
        # data['ВЛице'] = entity.auth_person
        data['based'] = entity.based
        data['НаОсновании'] = entity.auth_person
        data['Проект'] = project.name
        data['АдресПроведенияМероприятия'] = project.address
        if data.get('type') == 'ad':
            data['ТипЦен'] = 'businessproject'
        elif data.get('type') == 'lease_kvs':
            data['ТипЦен'] = 'kvs'
        else:
            data['ТипЦен'] = project.subjects.subject_name
        # data['ТипЦен'] = project.subjects.subject_name
        data['РасшифровкаПодписиКонтрагента'] = entity.sign
        data['ДатаС'] = str(project.date_start)
        data['ДатаПо'] = str(project.date_end)
        data['ВремяЗаезда'] = project.date_start - relativedelta(days=1, hour=18, minute=0, second=0)
        data['ВремяЗаезда'] = data['ВремяЗаезда'].strftime("%Y-%m-%dT%H:%M:%S")
        data['ВремяВыезда'] = project.date_end + relativedelta(hour=project.time_last.hour,
                                                               minute=project.time_last.minute)
        data['ВремяВыезда'] = data['ВремяВыезда'].strftime("%Y-%m-%dT%H:%M:%S")
        r = requests.patch("http://%s/hs/Expobit/RewritingAccount/%s" % (settings.ONE_C_URL, data['Ref_Key']),
                           auth=settings.ONE_C_AUTH,
                           cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                           json=data)
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.patch(
                    "http://%s/hs/Expobit/RewritingAccount/%s" % (settings.ONE_C_URL, data['Ref_Key']),
                    auth=settings.ONE_C_AUTH,
                    cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                    json=data)
        if r.status_code == 500:
            return Response(content_type='application/json', data=r.text, status=r.status_code)
        return Response(content_type='application/json', data=r.json(), status=r.status_code)

    def post(self, request):
        data = request.data
        s = 0
        data['Товары'] = list(map(lambda x: {'LineNumber': int(x['LineNumber']) + 1,
                                             'Номенклатура_Key': x['nomenclature_guid'],
                                             'ПроцентСкидки': int(x['discount_rate']),
                                             'Наценка': x['markup'],
                                             'count_days': x['count_days'],
                                             'Сумма': x['total'],
                                             'Цена': x['price'],
                                             'Содержание': x['comment'],
                                             'Количество': x['count']}, data['Товары']))
        for i in data['Товары']:
            s += i['Сумма']
        data['СуммаДокумента'] = s
        entity = Entity.objects.get(pk=request.data.get('entity_id'))
        project = Project.objects.get(pk=request.data.get('project_id'))
        # data['ВЛице'] = entity.auth_person
        data['based'] = entity.based
        data['Проект'] = project.name
        # data['data_project'] = project.date_end
        data['АдресПроведенияМероприятия'] = project.address
        if data.get('type') == 'ad':
            data['ТипЦен'] = 'businessproject'
        elif data.get('type') == 'lease_kvs':
            data['ТипЦен'] = 'kvs'
        else:
            data['ТипЦен'] = project.subjects.subject_name
        # data['ТипЦен'] = project.subjects.subject_name
        data['НаОсновании'] = entity.auth_person
        data['РасшифровкаПодписиКонтрагента'] = entity.sign
        data['ДатаС'] = str(project.date_start)
        data['ДатаПо'] = str(project.date_end)
        data['ВремяЗаезда'] = project.date_start - relativedelta(days=1, hour=18, minute=0, second=0)
        data['ВремяЗаезда'] = data['ВремяЗаезда'].strftime("%Y-%m-%dT%H:%M:%S")
        data['ВремяВыезда'] = project.date_end + relativedelta(hour=project.time_last.hour,
                                                               minute=project.time_last.minute)
        data['ВремяВыезда'] = data['ВремяВыезда'].strftime("%Y-%m-%dT%H:%M:%S")
        r = requests.post('http://%s/hs/Expobit/GetAccount' % (settings.ONE_C_URL,),
                          auth=settings.ONE_C_AUTH,
                          cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                          json=data)
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.post('http://%s/hs/Expobit/GetAccount' % (settings.ONE_C_URL,),
                                  auth=settings.ONE_C_AUTH,
                                  cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)},
                                  json=data)
        if r.status_code == 500:
            return Response(content_type='application/json', data=r.text, status=r.status_code)
        return Response(content_type='application/json', data=r.json(), status=r.status_code)


class GetFromOdata(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, odata_url):
        r = requests.get("http://%s/odata/standard.odata/"
                         "%s?$format=json" % (settings.ONE_C_URL, odata_url),
                         auth=settings.ONE_C_AUTH)
        return Response(content_type='application/json', data=r.json(), status=200)


class CheckAct(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        guid = request.GET.get('guid')
        if not guid:
            return Response(data={'error': 'not GUID'}, status=400)
        r = requests.get("http://%s/hs/Expobit/CheckAct/%s" % (settings.ONE_C_URL, guid),
                         auth=settings.ONE_C_AUTH,
                         cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.get("http://%s/hs/Expobit/CheckAct/%s" % (settings.ONE_C_URL, guid),
                                 auth=settings.ONE_C_AUTH,
                                 cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        return Response(content_type='application/json', data=r.json(), status=200)


class AuthorizedPersons(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        r = requests.get("http://%s/hs/Expobit/GetAuthorizedPerson" % (settings.ONE_C_URL,),
                         auth=settings.ONE_C_AUTH,
                         cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.get("http://%s/hs/Expobit/GetAuthorizedPerson" % (settings.ONE_C_URL, ),
                                 auth=settings.ONE_C_AUTH,
                                 cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        return Response(content_type='application/json', data=json.loads(r.text[1:-1]), status=200)


class GetAct(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        guid = request.GET.get('guid')
        r = requests.get("http://%s/hs/Expobit/GetAct/%s" % (settings.ONE_C_URL, guid),
                         auth=settings.ONE_C_AUTH,
                         cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.get("http://%s/hs/Expobit/GetAct/%s" % (settings.ONE_C_URL, guid),
                                 auth=settings.ONE_C_AUTH,
                                 cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        return Response(content_type='application/json', data={}, status=r.status_code)


class GetNomenclature(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        project = Project.objects.get(pk=request.GET.get('project'))
        barter = request.GET.get('barter', 'false')
        date = request.GET.get('date')[:10]
        if request.GET.get('type') == 'ad':
            type_price = 'businessproject'
        elif request.GET.get('type') == 'kvs':
            #type_price = 'kvs'
            type_price = project.project_type.type
        else:
            type_price = project.subjects.subject_name
        r = requests.get("http://%s/hs/Expobit/GetNomenclature/%s/%s/%s" % (settings.ONE_C_URL,
                                                                            type_price, barter, date),
                         auth=settings.ONE_C_AUTH,
                         cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.get("http://%s/hs/Expobit/GetNomenclature/%s/%s/%s" % (settings.ONE_C_URL,
                                                                                    type_price, barter, date),
                                 auth=settings.ONE_C_AUTH,
                                 cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        return Response(content_type='application/json', data=json.loads(r.json()), status=200)


class Organizations(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        r = requests.get("http://%s/hs/Expobit/GetOrganization/" % (settings.ONE_C_URL,),
                         auth=settings.ONE_C_AUTH,
                         cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        if r.status_code == 404:
            if update_session() == 200:
                r = requests.get("http://%s/hs/Expobit/GetOrganization/" % (settings.ONE_C_URL,),
                                 auth=settings.ONE_C_AUTH,
                                 cookies={'IBSession': cache.get('one_c_session_key' + settings.MEMCACHED_NAME)})
        return Response(content_type='application/json', data=json.loads(r.json()), status=200)

# Create your views here.
