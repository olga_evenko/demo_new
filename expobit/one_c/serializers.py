from rest_framework import serializers
from .models import Price, Nomenclature, Organizations


class NomenclatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nomenclature
        fields = ['guid', 'name']


class PriceSerializerRO(serializers.ModelSerializer):
    nomenclature_name = serializers.SerializerMethodField()
    units = serializers.SerializerMethodField()

    def get_nomenclature_name(self, obj):
        return obj.nomenclature.name

    class Meta:
        model = Price
        fields = "__all__"

    def get_units(self, obj):
        return obj.nomenclature.units.description


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = "__all__"


class OrganizationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organizations
        fields = '__all__'