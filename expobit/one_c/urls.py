from django.conf.urls import url, include
from .views import SyncDBOneC, ApiSetPriceView, ApiSetOrganizationsView, BillOneC, PrintOneC, \
    GetFromOdata, CheckAct, GetAct, GetNomenclature, Organizations, Reports, SetApp, \
    AuthorizedPersons, GetActs, BankOneC, BankOrgOneC
from rest_framework import routers
from .models import OneCSession
import requests
from django.conf import settings
from django.core.cache import cache
router = routers.DefaultRouter()
router.register(r'prices', ApiSetPriceView, 'ApiSetPriceView')
# router.register(r'organizations', ApiSetOrganizationsView, 'ApiSetOrganizationsView')

urlpatterns = [
    url(r'sync_db', SyncDBOneC.as_view(), name='sync_db'),
    url(r'bill', BillOneC.as_view(), name='bill'),
    url(r'banks_org', BankOrgOneC.as_view(), name='bank_org'),
    url(r'bank', BankOneC.as_view(), name='bank'),
    url(r'checking_act', CheckAct.as_view(), name='CheckAct'),
    url(r'reports', Reports.as_view(), name='CheckAct'),
    url(r'get_acts', GetActs.as_view(), name='GetActs'),
    url(r'organizations', Organizations.as_view(), name='Organizations'),
    url(r'get_act', GetAct.as_view(), name='GetAct'),
    url(r'get_nomenclature', GetNomenclature.as_view(), name='GetNomenclature'),
    url(r'print', PrintOneC.as_view(), name='print'),
    url(r'authorized_persons', AuthorizedPersons.as_view(), name='authorized_persons'),
    url(r'setApp', SetApp.as_view(), name='setApp'),
    url(r'^', include(router.urls)),
    url(r'^getodata/(?P<odata_url>[^/]+)', GetFromOdata.as_view(), name='getodata')
]

# def update_session():
#     rq = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
#                      headers={'IBSession': 'start'})
#     if rq.status_code == 200:
#         qs = OneCSession.objects.create(session_key=rq.cookies.get('ibsession'))
#         cache.set('one_c_session_key'+settings.MEMCACHED_NAME, rq.cookies.get('ibsession', None))
#     return rq.status_code
#
#
# last_session = OneCSession.objects.order_by('id').last()
# cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
# if not last_session:
#     r = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
#                      headers={'IBSession': 'start'})
#     if r.status_code == 200:
#         qs = OneCSession.objects.create(session_key=r.cookies.get('ibsession'))
#         cache.set('one_c_session_key'+settings.MEMCACHED_NAME, qs.session_key, None)
# else:
#     r = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
#                      cookies={'IBSession': last_session.session_key})
#     if r.status_code != 200:
#         r = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
#                          headers={'IBSession': 'start'})
#         if r.status_code == 200:
#             qs = OneCSession.objects.create(session_key=r.cookies.get('ibsession'))
#             cache.set('one_c_session_key'+settings.MEMCACHED_NAME, r.cookies.get('ibsession'), None)
#     else:
#         cache.set('one_c_session_key'+settings.MEMCACHED_NAME, last_session.session_key, None)