from celery.task import periodic_task, task
from django.http import HttpResponse
from one_c import manage_data
from celery.schedules import crontab
from django.core.cache import cache
from workers.models import StartAlert, Statistic
from django.conf import settings
from projects.models import Project
import datetime
from dateutil.relativedelta import relativedelta
from channels import Channel


# @periodic_task(run_every=crontab(hour='0'))
# def check_area_project():
#

@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def reset_counters():
    Statistic.objects.create(count_clients=cache.get('count_clients'+settings.MEMCACHED_NAME),
                             count_projects=cache.get('count_projects'+settings.MEMCACHED_NAME),
                             count_contacts=cache.get('count_contacts'+settings.MEMCACHED_NAME),
                             count_users=cache.get('max_users'+settings.MEMCACHED_NAME),
                             count_app=cache.get('count_applications' + settings.MEMCACHED_NAME))
    cache.set('count_projects'+settings.MEMCACHED_NAME,
              Project.objects.filter(status='working',
                                     date_start__lte=datetime.date.today(),
                                     date_end__gte=datetime.date.today()).count(), None)
    cache.set('count_clients'+settings.MEMCACHED_NAME, 0, None)
    # cache.set('count_users'+settings.MEMCACHED_NAME, 0, None)
    cache.set('count_contacts'+settings.MEMCACHED_NAME, 0, None)
    cache.set('max_users'+settings.MEMCACHED_NAME, 0, None)
    cache.set('count_applications' + settings.MEMCACHED_NAME, 0, None)


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def check_area_project():
    now = datetime.datetime.now()
    current_date = now + relativedelta(months=3)
    qs = Project.objects.filter(date_start__gt=now, date_start__lt=current_date, status='planning')
    user = None
    users = []
    for i in qs:
        pr_t = i.project_type
        if pr_t and pr_t.type_project_type == 'expo' and i.user_id not in users:
            StartAlert.objects.create(user_id=i.user_id, viewed=False, director=False)
            users.append(i.user_id)
        elif pr_t:
            if pr_t.director and pr_t.director_id not in users:
                StartAlert.objects.create(user_id=pr_t.director_id, viewed=False, director=True)
                users.append(pr_t.director_id)
            elif i.user_id not in users:
                StartAlert.objects.create(user_id=i.user_id, viewed=False, director=False)
                users.append(i.user_id)
        else:
            if i.user_id not in users:
                StartAlert.objects.create(user_id=i.user_id, viewed=False, director=False)
                users.append(i.user_id)
    return print('Send check project')


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def check_project_status():
    now = datetime.datetime.now()
    current_date = now - relativedelta(months=1)
    qs = Project.objects.filter(created_date__lt=current_date, status='planning')
    for i in qs:
        Channel('send_notification').send({
            'users': str(i.user_id),
            'message': 'Истек период планирования, переведите проект в '
                       'статус «В работе» или он будет отменен <a href="#project/%s/edit">%s</a>' % (str(i.id), i.name),
            'block_working': None
        })

    return print('Send check project')


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def canceled_projects():
    now = datetime.date.today()
    Project.objects.filter(date_end__lt=now, status='planning').update(status='canceled')
    # Project.objects.filter(date_end__lt=now, status='working').update(status='finished')
    qs = Project.objects.filter(date_end__lt=now, status='working')
    if not hasattr(settings, 'GOOGLE_FORM_LINK'):
        Project.objects.filter(date_end__lt=now, status='working').update(status='finished')
        return print('canceled projects')
    for i in qs:
        #if i.project_type.type_project_type == 'expo':
        Channel('send_notification').send({
            'users': str(i.user_id),
            'message': 'Заполните форму о проекте '
                       '<a href="%s">%s</a>' % (
                       settings.GOOGLE_FORM_LINK, i.name),
            'block_working': None
        })
    Project.objects.filter(date_end__lt=now, status='working').update(status='finished')
    return print('canceled projects')

@task
def get_acts(project_id, date):
    # project_id = request.GET.get('project')
    # date = request.GET.get('date_start')
    project = Project.objects.get(pk=project_id)
    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename="%s.zip"' % (manage_data.transliterate(project.name),)
    response['Content-Transfer-Encoding'] = 'binary'
    response.write(manage_data.getActs(project_id=project_id, date=date).content)
    return response