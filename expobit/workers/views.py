from django.shortcuts import render
from rest_framework import viewsets, filters
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import *
from .models import *
from django.contrib.gis.geos import GEOSGeometry
from django.db.models import Count, Max, Prefetch, Sum
from rest_framework.decorators import list_route
from .serializers import *
import calendar
from django.http import JsonResponse
from rest_framework.serializers import ValidationError
from projects.models import *
from clients.models import *
from rest_framework import status
from django_filters.rest_framework import DjangoFilterBackend
from projects.serializers import ProjectSerializerROMin
from django.db.models import Q, F
from django.db.models.functions.datetime import TruncBase
from channels import Channel
from .filters import ClientFilter, ClientFilterTypeTwo, ClientSearchFilter
import collections
from django.conf import settings
import requests
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from one_c.models import OneCSession
from one_c import manage_data
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import ContentType
from django.forms.models import model_to_dict
import csv
from django.db.models.query import QuerySet

def update_session():
    if not cache.get('update_session'+settings.MEMCACHED_NAME):
        cache.set('update_session'+settings.MEMCACHED_NAME, True, None)
        rq = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
                          headers={'IBSession': 'start'})
        if rq.status_code == 200:
            qs = OneCSession.objects.create(session_key=rq.cookies.get('ibsession'))
            cache.set('one_c_session_key'+settings.MEMCACHED_NAME, rq.cookies.get('ibsession'), None)
            cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
        else:
            cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
    else:
        while cache.get('update_session'+settings.MEMCACHED_NAME):
            time.sleep(0.01)
        return 200
    return rq.status_code


class ApiSetManagersView(viewsets.ModelViewSet):
    serializer_class = ManagersSerializer
    queryset = Manager.objects.all().order_by('user__last_name')

    @list_route(methods=['post'],)
    def remove_related(self, request):
        project_id = request.POST.get('project_id')
        q1 = Client.objects.filter(entitys__activities__in=request.POST.get('removed_id').split(',')).distinct()
        q2 = Client.objects.filter(entitys__activities__in=Activities.objects.
                                   filter(project_section__project=project_id)).distinct()

        for obj in q1:
            if obj not in q2:
                Membership.objects.filter(client=obj, project=project_id).delete()
        return JsonResponse({'success': True})

    @list_route(methods=['get'], )
    def count(self, request):
        array_id = request.GET.get('client_id').split(',')
        query_client = Client.objects.filter(entitys__activities__in=array_id).distinct()
        count = Membership.objects.filter(project=request.GET.get('project'), client__in=query_client).count()
        return JsonResponse(data={'count': count})

    @list_route(methods=['put'], )
    def replace(self, request):
        data = self.request.data
        s = status.HTTP_200_OK
        old_manager = data.getlist('old_manager', data.getlist('old_manager[]'))
        clients = data.getlist('client', data.getlist('client[]'))
        # update
        for manager, client in zip(old_manager, clients):
            if manager != '':
                obj = Membership.objects.filter(client=client,
                                                manager=manager,
                                                project=data.get('project'))
                if data.get('new_manager') == '':
                    obj.delete()
                    old_user = Manager.objects.get(pk=manager).user
                    NewContact.objects.filter(project_id=data.get('project'), create_user=old_user, client=client).delete()
                else:
                    obj.update(manager=data.get('new_manager'))
                    old_user = Manager.objects.get(pk=manager).user
                    new_user = Manager.objects.get(pk=data.get('new_manager')).user
                    NewContact.objects.filter(project_id=data.get('project'), create_user=old_user, client=client).update(
                        create_user=new_user)
            elif manager == '':
                if data.get('new_manager') != '':
                    Membership.objects.create(client=Client.objects.get(pk=client),
                                              manager=Manager.objects.get(pk=data.get('new_manager')),
                                              project=Project.objects.get(pk=data.get('project')))
                    s = status.HTTP_201_CREATED
        return Response(status=s, content_type='application/json',
                        data={'success': 'true', 'update': True if len(clients) > 1 else False})

    @list_route(methods=['get'], )
    def forselect(self, request):
        serilizeData = []
        # TODO Изменил на поиск по руководителю
        qs = []
        if request.user.is_superuser:
            qs = Manager.objects.all().order_by('user__last_name')
        elif request.user.my_managers.all().exists():
            if request.user.groups.filter(name='head_expo').exists():
                if request.GET.get('get_headers_group'):
                    qs = Manager.objects.filter(head=request.user).order_by('user__last_name')
                else:
                    qs_project_managers = request.user.my_managers.all().values('user__id')
                    qs_project_managers = [x['user__id'] for x in qs_project_managers]
                    qs = Manager.objects.filter(
                        Q(head__in=qs_project_managers) | Q(user__in=qs_project_managers)).order_by('user__last_name')
            else:
                qs = Manager.objects.filter(head=request.user).order_by('user__last_name')
        for i in qs:
            user = i.user
            name = user.last_name + ' ' + user.first_name
            manager_id = i.id
            serilizeData.append({
                'text': name,
                'value': manager_id,
                'user_id': user.id
            })
        user_manager = Manager.objects.filter(user=request.user)[:1]
        if user_manager:
            serilizeData.append({
                'text': '%s %s' % (request.user.last_name, request.user.first_name),
                'value': user_manager[0].id,
                'user_id': request.user.id
            })
        return JsonResponse(serilizeData, safe=False)

    @list_route(methods=['get'], )
    def forselectmessage(self, request):
        serilizeData = []
        # TODO Изменил на поиск по руководителю
        qs = []
        if request.user.is_superuser:
            qs = Manager.objects.all().order_by('user__last_name')
        elif request.user.my_managers.all().exists():
            if request.user.groups.filter(name='head_expo').exists():
                if request.GET.get('get_headers_group'):
                    qs = Manager.objects.filter(head=request.user).order_by('user__last_name')
                else:
                    qs_project_managers = request.user.my_managers.all().values('user__id')
                    qs_project_managers = [x['user__id'] for x in qs_project_managers]
                    qs = Manager.objects.filter(
                        Q(head__in=qs_project_managers) | Q(user__in=qs_project_managers)).order_by('user__last_name')
            else:
                qs = Manager.objects.filter(head=request.user).order_by('user__last_name')
        for i in qs:
            user = i.user
            name = user.last_name + ' ' + user.first_name
            manager_id = i.id
            serilizeData.append({
                'text': name,
                'value': manager_id,
                'user_id': user.id
            })
        user_manager = Manager.objects.filter(user=request.user)[:1]
        if user_manager and user_manager[0].head_id:
            qs = User.objects.filter(Q(id=user_manager[0].head_id) | Q(managers__head=user_manager[0].head_id))
            for i in qs:
                name = i.last_name + ' ' + i.first_name
                serilizeData.append({
                    'text': name,
                    'user_id': i.id
                })
        return JsonResponse(serilizeData, safe=False)

    @list_route(methods=['put'], )
    def processing_manager_client(self, request):
        try:
            data = self.request.data
            new_data = data.getlist('client', data.getlist('client[]'))
            manager = data.get('new_manager')
            user = Manager.objects.get(pk=manager).user_id
            old_managers = Client.objects.filter(id__in=new_data).values_list('personal_manager_type_one__user',
                                                                              flat=True).distinct()
            Client.objects.filter(id__in=new_data).update(personal_manager_type_one=manager)
            NewContact.objects.filter(create_user__in=old_managers, client__in=new_data).update(create_user=user)

            if request.user.groups.filter(name='kvs_head') or request.user.is_superuser:
                project_type = Project_type.objects.values_list('id', flat=True).filter(addit_parameter = 'update_man')
                projects = Project.objects.filter(project_type__in = project_type, status__in = ['planning', 'working'],
                                                  client__id__in=new_data)
                ApplicationLeaseKVS.objects.filter(project_id__in = projects.values_list('id')).update(
                    manager=Manager.objects.get(user_id=user))
                projects.update(user=user)

            return JsonResponse(safe=False, data={'success': True, 'update': True},
                                content_type='application/json', status=200)
        except:
            return JsonResponse(data='Ошибка сохранения',
                                content_type='application/json', status=500)

    @list_route(methods=['put'], )
    def visitor_manager_client(self, request):
        data = self.request.data
        new_data = data.getlist('client', data.getlist('client[]'))
        manager = data.get('new_manager')
        user = None
        if manager:
            user = Manager.objects.get(pk=manager).user_id
        old_managers = Client.objects.filter(id__in=new_data).values_list('visitor_manager__user', flat=True).distinct()
        Client.objects.filter(id__in=new_data).update(visitor_manager=manager)
        if manager:
            NewContact.objects.filter(create_user__in=old_managers, client__in=new_data).update(create_user=user)
        else:
            NewContact.objects.filter(create_user__in=old_managers, client__in=new_data).delete()
        return JsonResponse(safe=False, data={'success': True, 'update': True},
                            content_type='application/json', status=200)

    @list_route(methods=['put'], )
    def visitor_manager_individual(self, request):
        data = self.request.data
        new_data = data.getlist('individual', data.getlist('individual[]'))
        manager = data.get('new_manager')
        user = Manager.objects.get(pk=manager).user_id
        old_managers = Individual.objects.filter(id__in=new_data).values_list('visitor_manager__user',
                                                                              flat=True).distinct()
        Individual.objects.filter(id__in=new_data).update(visitor_manager=manager)
        NewIndividualContact.objects.filter(create_user__in=old_managers, individual__in=new_data).update(create_user=user)
        return JsonResponse(safe=False, data={'success': True, 'update': True},
                            content_type='application/json', status=200)

    permission_classes = (DjangoModelPermissions,)


class ApiSetPushMessageView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions, IsAuthenticated)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('show',)
    ordering_fields = ('date_created',)
    serializer_class = PushMessageSerializer
    queryset = PushMessage.objects.all()

    def get_html_string(self, clients, message):
        html = '<p>%s</p>' % (message,)
        for i in clients:
            html += '<a href="/#client/%s">%s</a><br>' % (i['id'], i['name'])
        return html

    def get_html_attachments(self, attachments, message):
        html = '<p>%s</p>' % (message,)
        for i in attachments:
            html += '<br>%s' % (i,)
        return html


    @list_route(methods=['post'])
    def manager_report(self, request):
        manager = request.data.get('manager')
        message = request.data.get('message')
        clients = Client.objects.filter(**request.data.get('filter')).values('id', 'name')
        user = User.objects.get(managers__id=manager)
        Channel('chat-messages').send({'user': user.id,
                                       'push': 'Проверить данные',
                                       'username': user.username,
                                       'message': self.get_html_string(clients, message),
                                       'sender': request.user.pk})
        return Response(status=200, content_type='applications/json', data={'send': True})

    @list_route(methods=['post'])
    def send_message(self, request):
        user_id = request.data.get('user_id')
        message = request.data.get('message')
        attachments = request.data.getlist('attachments')
        user = User.objects.get(pk=int(user_id))
        Channel('chat-messages').send({'user': user.id,
                                       'push': 'Сообщение',
                                       'username': user.username,
                                       'message': self.get_html_attachments(attachments, message),
                                       'sender': request.user.pk})
        return Response(status=200, content_type='applications/json', data={'send': True})

    @list_route(methods=['patch'])
    def read_messages(self, request):
        sender_id = request.data.get('sender_id')
        PushMessage.objects.filter(user_id=request.user.id, sender_id=sender_id).update(show=True)
        return Response(status=200, content_type='applications/json', data={'read': True})

    @list_route(methods=['post'])
    def dialog_message(self, request):
        user_id = request.data.get('user_id')
        message = request.data.get('message')
        user = User.objects.get(pk=int(user_id))
        Channel('chat-messages').send({'user': user.id,
                                       'push': 'Сообщение',
                                       'username': user.username,
                                       'message': message,
                                       'sender': request.user.pk,
                                       'dialog': True})
        return Response(status=200, content_type='applications/json', data={'send': True})

    @list_route(methods=['get'], )
    def count(self, request):
        user = self.request.user
        count = PushMessage.objects.filter(Q(user_id=user.id) | Q(sender_id=user.id)).order_by('dialog').\
            distinct('dialog').values_list('id', flat=True).count()
        return JsonResponse(data={'count': count})

    @list_route(methods=['get'], )
    def count_new(self, request):
        user = self.request.user
        count = PushMessage.objects.filter(user=user, show=False).count()
        return JsonResponse(data={'count': count})

    def get_queryset(self):
        user = self.request.user
        if self.request.GET.get('open_dialog'):
            user_id = self.request.GET.get('user')
            sender_id = self.request.GET.get('sender')
            return PushMessage.objects.filter(user__in=[user.id, user_id],
                                              sender__in=[sender_id, user.id]).order_by('-date_created')
        messages_id = PushMessage.objects.filter(Q(user_id=user.id) | Q(sender_id=user.id)).order_by('dialog',
                                                                                                     '-date_created').\
            distinct('dialog').values_list('id', flat=True)
        return PushMessage.objects.filter(id__in=messages_id).order_by('-date_created')


class ApiSetManagerPlanView(viewsets.ModelViewSet):
    serializer_class = ManagerPlanSerializer
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('manager',)

    def get_serializer_context(self):
        manager_id = self.request.GET.get('manager')
        first_month = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month, 1)
        last_month = first_month + relativedelta(months=1) - relativedelta(days=1)
        month = self.request.GET.get('month')
        if month:
            y = month.split('-')[0]
            m = month.split('-')[1]
            first_month = datetime.date(int(y), int(m), 1)
            last_month = first_month + relativedelta(months=1) - relativedelta(days=1)
        one_c_context = {}
        # TODO Подсчет расхождения с фин планом доходов
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, manager_id=manager_id))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                if bill['ПроектИД'] in one_c_context:
                    one_c_context[bill['ПроектИД']].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    one_c_context[bill['ПроектИД']] = [int(float(bill['ОплаченнаяСумма']))]
        return one_c_context

    def get_queryset(self):
        month = datetime.datetime.now().month
        year = datetime.datetime.now().year
        get_month = self.request.GET.get('month')
        status_project = ['working']
        if self.request.GET.get('all'):
            status_project.append('finished')
        if get_month:
            year = int(get_month.split('-')[0])
            month = int(get_month.split('-')[1])
        return ProjectReportManager.objects.filter(project_report__month_date__month=month,
                                                   project_report__month_date__year=year,
                                                   project_report__project_indicators__project__status__in=status_project)


class ApiSetManagersPlanTypeTwoView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('manager',)
    serializer_class = ManagerPlanTypeTwoSerializer

    def get_serializer_context(self):
        manager_id = self.request.GET.get('manager')
        first_month = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month, 1)
        last_month = first_month + relativedelta(months=1)
        one_c_context = {}
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, manager_id=manager_id))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                if bill['МенеджерИД'] in one_c_context:
                    one_c_context[bill['МенеджерИД']].append(int(bill['ОплаченнаяСумма']))
                else:
                    one_c_context[bill['МенеджерИД']] = [int(bill['ОплаченнаяСумма'])]
        return one_c_context

    def get_queryset(self):
        date = datetime.datetime.now()
        month = date.month
        year = date.year
        if self.request.GET.get('month'):
            year = self.request.GET.get('month').split('-')[0]
            month = self.request.GET.get('month').split('-')[1]
        return ProjectReportTypeTwoManager.objects.filter(project_report__month_date__month=month,
                                                          project_report__month_date__year=year,
                                                          project_report__project_indicators__project__status='working')


class ApiSetClientsManagerView(viewsets.ModelViewSet):
    serializer_class = ClientsForManagerSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissions, )
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, ClientSearchFilter)
    search_fields = ('name', )
    filter_fields = ('contact__project', 'membership__project', 'contact__failure_bool', 'status')
    ordering_fields = ('client_group__group', 'name', 'status', 'result_last_contact', 'date_last_contact_proj', 'actual_date_new_contact')
    filter_class = ClientFilter

    def get_serializer_context(self):
        return {'project': self.request.GET.get('membership__project')}


    def get_serializer_class(self):
        if self.request.GET.get('all'):
            return ClientsForAllProjectSerializer
        else:
            return ClientsForManagerSerializer

    def get_queryset(self):
        if self.request.GET.get('order') == 'asc':
            q_s = ''
        else:
            q_s = '-'
        if self.request.user.groups.filter(name='project_managers').exists():
            if self.request.GET.get('all'):
                filter_fields = {}
                if self.request.user.groups.filter(name='head_expo').exists():
                    filter_fields['membership__project__user__in'] = self.request.user.my_managers.all().\
                        values_list('user', flat=True)
                else:
                    filter_fields['membership__project__user'] = self.request.user
                return Client.objects.filter(**filter_fields). \
                    prefetch_related().distinct()
            if self.request.GET.get('call_soon') and Manager.objects.filter(user=self.request.user).exists():
                return Client.objects.filter(  # new_contacts__date_new_contact__gte=datetime.datetime.now(),
                    # new_contacts__date_new_contact__lte=end_date,
                    new_contacts__project=self.request.GET.get('membership__project'),
                    membership__manager=self.request.user.managers,
                    new_contacts__create_user=self.request.user). \
                    extra(select={
                    'actual_date_new_contact': """SELECT date_new_contact 
                                    FROM clients_newcontact 
                                    WHERE clients_newcontact.client_id=clients_client.id 
                                    AND create_user_id=%s AND project_id=%s 
                                    ORDER BY id DESC LIMIT 1""" % (
                        str(self.request.user.id), self.request.GET.get('membership__project')),
                    'result_last_contact': """SELECT result 
                                           FROM clients_contact 
                                           WHERE clients_contact.client_id=clients_client.id 
                                           AND project_id=%s 
                                           ORDER BY id DESC LIMIT 1""" % (
                        self.request.GET.get('membership__project'),),
                    'date_last_contact_proj': """SELECT date 
                                              FROM clients_contact 
                                              WHERE clients_contact.client_id=clients_client.id 
                                              AND project_id=%s 
                                              ORDER BY id DESC LIMIT 1""" % (
                        self.request.GET.get('membership__project'),)
                }). \
                    order_by('actual_date_new_contact'). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).distinct()
            if self.request.GET.get('not_call'):
                s = set(Contact.objects.filter(type_contact='out_phone', project_id=self.request.GET.get(
                                                      'membership__project')).distinct(
                    'client').values_list('client_id', flat=True))
                s1 = set(Client.objects.filter(membership__project_id=self.request.GET.get(
                                                      'membership__project')).exclude(
                    status__in=['black_list', 'inactive']).distinct().values_list('id', flat=True))
                qs_fitlers = {
                    'id__in': list(s1 - s),
                    'membership__project': self.request.GET.get('membership__project')
                }
                if self.request.GET.get('membership_manager'):
                    qs_fitlers['membership_manager'] = self.request.GET.get('membership_manager')
                return Client.objects.filter(**qs_fitlers). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
            if self.request.GET.get('sort') and 'contact__date' in self.request.GET.get('sort'):
                return Client.objects.filter(membership__project=self.request.GET.get('membership__project')). \
                    annotate(latest_call=Max('contact__date')).order_by(q_s + 'latest_call'). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
            elif self.request.GET.get('sort') and 'new_contacts__date_new_contact' in self.request.GET.get('sort'):
                return Client.objects.filter(membership__project=self.request.GET.get('membership__project')). \
                    extra(select={
                                  'actual_date_new_contact': """SELECT date_new_contact 
                                                    FROM clients_newcontact 
                                                    WHERE clients_newcontact.client_id=clients_client.id 
                                                    AND project_id=%s 
                                                    ORDER BY id DESC LIMIT 1""" % (
                        self.request.GET.get('membership__project'),),
                    'result_last_contact': """SELECT result 
                                                                                FROM clients_contact 
                                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                                AND project_id=%s 
                                                                                ORDER BY id DESC LIMIT 1""" % (
                    self.request.GET.get('membership__project'),),
                    'date_last_contact_proj': """SELECT date 
                                                                                            FROM clients_contact 
                                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                                            AND project_id=%s 
                                                                                            ORDER BY id DESC LIMIT 1""" % (
                    self.request.GET.get('membership__project'),)
                }). \
                    order_by(q_s + 'actual_date_new_contact'). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
            return Client.objects.filter(membership__project=self.request.GET.get('membership__project')).\
                prefetch_related(Prefetch('new_contacts',
                                                  queryset=NewContact.objects.filter(
                                                      project=self.request.GET.get('membership__project')).order_by(
                                                      'client', '-id').distinct('client'),
                                                  to_attr='menu'),
                                         Prefetch('client_group',
                                                  queryset=ClientGroup.objects.filter(
                                                      subjects__projects=self.request.GET.get(
                                                          'membership__project')).distinct(
                                                      'client'),
                                                  to_attr='group')
                                         ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
        elif self.request.GET.get('call_soon'):
            return Client.objects.filter(#new_contacts__date_new_contact__gte=datetime.datetime.now(),
                                         # new_contacts__date_new_contact__lte=end_date,
                                         new_contacts__project=self.request.GET.get('membership__project'),
                                         membership__manager=self.request.user.managers,
                                         new_contacts__create_user=self.request.user). \
                extra(select={
                'actual_date_new_contact':"""SELECT date_new_contact 
                FROM clients_newcontact 
                WHERE clients_newcontact.client_id=clients_client.id 
                AND create_user_id=%s AND project_id=%s 
                ORDER BY id DESC LIMIT 1""" % (str(self.request.user.id), self.request.GET.get('membership__project')),
                'result_last_contact': """SELECT result 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (
                self.request.GET.get('membership__project'),),
                'date_last_contact_proj': """SELECT date 
                                                                                        FROM clients_contact 
                                                                                        WHERE clients_contact.client_id=clients_client.id 
                                                                                        AND project_id=%s 
                                                                                        ORDER BY id DESC LIMIT 1""" % (
                self.request.GET.get('membership__project'),)

            }).\
                order_by('actual_date_new_contact'). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  create_user=self.request.user,
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).distinct()
        else:
            if self.request.GET.get('sort') and 'contact__date' in self.request.GET.get('sort'):
                return Client.objects.filter(membership__manager=self.request.user.managers,
                                             membership__project=self.request.GET.get('membership__project')). \
                        annotate(latest_call=Max('contact__date')).order_by(q_s+'latest_call'). \
                        prefetch_related(Prefetch('new_contacts',
                                                  queryset=NewContact.objects.filter(
                                                      create_user=self.request.user,
                                                      project=self.request.GET.get('membership__project')).order_by(
                                                      'client', '-id').distinct('client'),
                                                  to_attr='menu'),
                                         Prefetch('client_group',
                                                  queryset=ClientGroup.objects.filter(
                                                      subjects__projects=self.request.GET.get(
                                                          'membership__project')).distinct(
                                                      'client'),
                                                  to_attr='group')
                                         ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
            elif self.request.GET.get('sort') and 'new_contacts__date_new_contact' in self.request.GET.get('sort'):
                return Client.objects.filter(membership__manager=self.request.user.managers,
                    membership__project=self.request.GET.get('membership__project')). \
                    extra(select={
                    'actual_date_new_contact': """SELECT date_new_contact 
                                FROM clients_newcontact 
                                WHERE clients_newcontact.client_id=clients_client.id 
                                AND create_user_id=%s AND project_id=%s 
                                ORDER BY id DESC LIMIT 1""" % (
                    str(self.request.user.id), self.request.GET.get('membership__project'))
                }). \
                    order_by(q_s + 'actual_date_new_contact'). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  create_user=self.request.user,
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
            elif self.request.GET.get('not_call'):
                s = set(Contact.objects.filter(type_contact='out_phone', project_id=self.request.GET.get(
                                                      'membership__project')).distinct(
                    'client').values_list('client_id', flat=True))
                s1 = set(Client.objects.filter(membership__manager_id=self.request.user.managers.id, membership__project_id=self.request.GET.get(
                                                      'membership__project')).exclude(
                    status__in=['black_list', 'inactive']).distinct().values_list('id', flat=True))
                return Client.objects.filter(id__in=list(s1-s), membership__manager=self.request.user.managers,
                                      membership__project=self.request.GET.get('membership__project')). \
                    prefetch_related(Prefetch('new_contacts',
                                              queryset=NewContact.objects.filter(
                                                  project=self.request.GET.get('membership__project')).order_by(
                                                  'client', '-id').distinct('client'),
                                              to_attr='menu'),
                                     Prefetch('client_group',
                                              queryset=ClientGroup.objects.filter(
                                                  subjects__projects=self.request.GET.get(
                                                      'membership__project')).distinct(
                                                  'client'),
                                              to_attr='group')
                                     ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()
            return Client.objects.filter(membership__manager=self.request.user.managers,
                                         membership__project=self.request.GET.get('membership__project')).\
                prefetch_related(Prefetch('new_contacts',
                                          queryset=NewContact.objects.filter(
                                              project=self.request.GET.get('membership__project')).order_by(
                                              'client', '-id').distinct('client'),
                                          to_attr='menu'),
                                 Prefetch('client_group',
                                          queryset=ClientGroup.objects.filter(
                                              subjects__projects=self.request.GET.get('membership__project')).distinct(
                                              'client'),
                                          to_attr='group')
                                 ).extra(select={
                                                    'result_last_contact': """SELECT result 
                                                                FROM clients_contact 
                                                                WHERE clients_contact.client_id=clients_client.id 
                                                                AND project_id=%s 
                                                                ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),),
                                                    'date_last_contact_proj': """SELECT date 
                                                                            FROM clients_contact 
                                                                            WHERE clients_contact.client_id=clients_client.id 
                                                                            AND project_id=%s 
                                                                            ORDER BY id DESC LIMIT 1""" % (self.request.GET.get('membership__project'),)
                                                    }).distinct()


class ApiSetClientsTypeOneView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = ClientTypeOneSerializer
    filter_backends = (filters.OrderingFilter, ClientSearchFilter, DjangoFilterBackend)
    ordering_fields = ('name', 'date_last_contact')
    # ordering = ('name', )
    search_fields = ('name', 'personal_manager_type_one__user__first_name',
                     'personal_manager_type_one__user__last_name', 'address', 'contacts_person__phones__phone')
    filter_fields = {
        'personal_manager_type_one': ['isnull', 'exact'],
        'status': ['exact'],
        'contact__manager': ['exact']
    }

    def get_serializer_class(self):
        if not self.request.user.groups.filter(name='kvs_head').exists():
            return ClientForManagerTypeOneSerializer
        return ClientTypeOneSerializer

    def get_queryset(self):
        if self.request.GET.get('order') == 'asc':
            q_s = ''
        else:
            q_s = '-'
        if self.request.user.groups.filter(name='kvs_head').exists():
            users = [str(x.user_id) for x in Manager.objects.filter(Q(head=self.request.user) | Q(user=self.request.user))]
        else:
            users = [str(self.request.user.id)]
        filter_query_ad = {}
        filter_query_expo = {}
        filter_query_lease_kvs = {}
        filter_query = {}
        projects = None
        activities = None
        if self.request.GET.get('client_activitys') and self.request.GET.get('client_activitys') != '':
            filter_query = {
                'client_activitys__in': self.request.GET.get('client_activitys').split(',')
            }
        if self.request.GET.get('projects') and self.request.GET.get('projects') != '':
            projects = self.request.GET.get('projects').split(',')
            filter_query_ad['applicationad__project__in'] = projects
            filter_query_expo['applicationexpo__project__in'] = projects
            filter_query_lease_kvs['applicationleasekvs__project__in'] = projects
        if self.request.GET.get('activities') and self.request.GET.get('activities') != '':
            activities = self.request.GET.get('activities').split(',')
            filter_query_ad['entitys__activities__in'] = activities
            filter_query_expo['entitys__activities__in'] = activities
            filter_query_lease_kvs['entitys__activities__in'] = activities
        if self.request.GET.get('sort') and 'date_new_contact.date' in self.request.GET.get('sort'):
                return Client.objects.filter(Q(**filter_query_ad) |
                                         Q(**filter_query_expo) |
                                         Q(**filter_query_lease_kvs), **filter_query). \
                    extra(select={
                        'actual_date_new_contact': """SELECT date_new_contact 
                                                        FROM clients_newcontact 
                                                        WHERE clients_newcontact.client_id=clients_client.id 
                                                        AND create_user_id IN (%s) 
                                                        ORDER BY id DESC LIMIT 1""" % (','.join(users),),
                        'goal_date_new_contact': """SELECT goal_contact 
                                                                            FROM clients_newcontact 
                                                                            WHERE clients_newcontact.client_id=clients_client.id 
                                                                            AND create_user_id IN (%s) 
                                                                            ORDER BY id DESC LIMIT 1""" % (
                            ','.join(users),)
                    }). \
                    order_by(q_s + 'actual_date_new_contact').distinct()
        if projects or activities:
            return Client.objects.filter(Q(**filter_query_ad) |
                                         Q(**filter_query_expo) |
                                         Q(**filter_query_lease_kvs), **filter_query).distinct()

        return Client.objects.filter(**filter_query).extra(select={
                        'actual_date_new_contact': """SELECT date_new_contact 
                                                        FROM clients_newcontact 
                                                        WHERE clients_newcontact.client_id=clients_client.id 
                                                        AND create_user_id IN (%s) 
                                                        ORDER BY id DESC LIMIT 1""" % (','.join(users),),
                        'goal_date_new_contact': """SELECT goal_contact 
                                                                            FROM clients_newcontact 
                                                                            WHERE clients_newcontact.client_id=clients_client.id 
                                                                            AND create_user_id IN (%s) 
                                                                            ORDER BY id DESC LIMIT 1""" % (
                            ','.join(users),)
                    }).distinct()


class ApiSetClientsTypeTwoView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    permission_classes = (DjangoModelPermissions, )
    serializer_class = ClientTypeTwoSerializer
    filter_backends = (filters.OrderingFilter, ClientSearchFilter, DjangoFilterBackend)
    ordering_fields = ('name', 'date_last_contact', 'visitor_manager', 'visitor_manager__user__last_name')
    # ordering = ('name',)
    search_fields = ('name', 'personal_manager_type_one__user__first_name',
                     'personal_manager_type_one__user__last_name', 'address', 'contacts_person__phones__phone')
    filter_fields = {
        'visitor_manager': ['isnull', 'exact'],
        'status': ['exact'],
        'city': ['in'],
        'applicationad__status': ['exact'],
        'contact__failure_bool': ['exact'],
        'contact__interest': ['exact'],
        'contact__app': ['exact']
        # 'personal_manager_type_one': ['in']
    }
    filter_class = ClientFilterTypeTwo

    def get_serializer_class(self):
        if not self.request.user.groups.filter(name='head_ad').exists():
            return ClientForManagerTypeTwoSerializer
        return ClientTypeTwoSerializer

    def get_queryset(self):
        filter_query_ad = {}
        filter_query_expo = {}
        filter_query_lease_kvs = {}
        projects = None
        activities = None
        manager = self.request.GET.get('visitor_manager')
        project = self.request.GET.get('project')
        if self.request.GET.get('order') == 'asc':
            q_s = ''
        else:
            q_s = '-'
        if manager:
            users = [str(Manager.objects.get(pk=int(manager)).user_id)]
        elif self.request.user.groups.filter(name='head_ad').exists():
            users = [str(x.user_id) for x in Manager.objects.filter(Q(head=self.request.user) | Q(user=self.request.user))]
        else:
            users = [str(self.request.user.id)]
        if self.request.GET.get('projects') and self.request.GET.get('projects') != '':
            projects = self.request.GET.get('projects').split(',')
            filter_query_ad['applicationad__project__in'] = projects
            filter_query_expo['applicationexpo__project__in'] = projects
            filter_query_lease_kvs['applicationleasekvs__project__in'] = projects
        if self.request.GET.get('activities') and self.request.GET.get('activities') != '':
            activities = self.request.GET.get('activities').split(',')
            filter_query_ad['entitys__activities__in'] = activities
            filter_query_expo['entitys__activities__in'] = activities
            filter_query_lease_kvs['entitys__activities__in'] = activities
        s = []
        if self.request.GET.get('not_call'):
            s = list(Contact.objects.filter(type_contact='out_phone', project_id=self.request.GET.get(
                'project')).distinct(
                'client').values_list('client_id', flat=True))
        if project and users:

            qs = Client.objects.filter(Q(**filter_query_ad) |
                                         Q(**filter_query_expo) |
                                         Q(**filter_query_lease_kvs)).extra(select={
                        'actual_date_new_contact': """SELECT date_new_contact 
                                                        FROM clients_newcontact 
                                                        WHERE clients_newcontact.client_id=clients_client.id 
                                                        AND create_user_id IN (%s) AND project_id = %s
                                                        ORDER BY id DESC LIMIT 1""" % (','.join(users), project),
                        'goal_date_new_contact': """SELECT goal_contact 
                                                    FROM clients_newcontact 
                                                    WHERE clients_newcontact.client_id=clients_client.id 
                                                    AND create_user_id IN (%s) AND project_id = %s
                                                    ORDER BY id DESC LIMIT 1""" % (','.join(users), project)
                        }).exclude(id__in=s).distinct()
            if self.request.GET.get('sort') and 'date_new_contact.date' in self.request.GET.get('sort'):
                qs = Client.objects.filter(Q(**filter_query_ad) |
                                           Q(**filter_query_expo) |
                                           Q(**filter_query_lease_kvs)).extra(select={
                    'actual_date_new_contact': """SELECT date_new_contact 
                                                                        FROM clients_newcontact 
                                                                        WHERE clients_newcontact.client_id=clients_client.id 
                                                                        AND create_user_id IN (%s) AND project_id = %s
                                                                        ORDER BY id DESC LIMIT 1""" % (','.join(users), project),
                    'goal_date_new_contact': """SELECT goal_contact 
                                                                    FROM clients_newcontact 
                                                                    WHERE clients_newcontact.client_id=clients_client.id 
                                                                    AND create_user_id IN (%s) AND project_id = %s
                                                                    ORDER BY id DESC LIMIT 1""" % (','.join(users), project)
                }).order_by(q_s + 'actual_date_new_contact').exclude(id__in=s).distinct()
            return qs
        return Client.objects.filter(**filter_query_ad)


class ApiSetFinPlanTypeOneView(viewsets.ModelViewSet):
    queryset = FinPlanTypeOne.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = FinPlanTypeOneSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ('month', )
    ordering = ('month',)
    filter_fields = {'month': ['gt', 'lt']}

    def get_serializer_context(self):
        managers_id = Manager.objects.filter(head=self.request.user).values('id')
        managers_id = [str(x['id']) for x in managers_id]
        if Manager.objects.filter(user=self.request.user).exists():
            managers_id.append(str(self.request.user.managers.id))
        first_month = datetime.date(datetime.datetime.now().year, 1, 1)
        last_month = first_month + relativedelta(years=1)
        one_c_context = {}
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, manager_id=','.join(managers_id)))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
                key = '%s.%s' % (str(date.year), str(date.month))
                if Project_type.objects.filter(director_id=self.request.user.id).count()>0:
                    for type in Project_type.objects.filter(director_id=self.request.user.id):
                        key_local = key +'.%s' % (str(type.id))
                        if type.id == Project.objects.get(id = bill['ПроектИД']).project_type.id:
                            if key_local in one_c_context:
                                one_c_context[key_local].append(int(float(bill['ОплаченнаяСумма'])))
                            else:
                                one_c_context[key_local] = [int(float(bill['ОплаченнаяСумма']))]
                else:
                    if key in one_c_context:
                        one_c_context[key].append(int(float(bill['ОплаченнаяСумма'])))
                    else:
                        one_c_context[key] = [int(float(bill['ОплаченнаяСумма']))]
        return one_c_context

    def get_queryset(self):
        if Project_type.objects.filter(director_id = self.request.user.id).count():
            project_type = Project_type.objects.filter(director_id = self.request.user.id)
            for type in project_type:
                qs = FinPlanTypeOne.objects.filter(project_type=type.id, head=self.request.user, month__year=datetime.datetime.now().year)
                if not qs:
                    year = datetime.datetime.now().year
                    for i in range(1, 13):
                        FinPlanTypeOne.objects.create(project_type=type, head=self.request.user, month=datetime.datetime(year, i, 1))
                qs = FinPlanTypeOne.objects.filter(head=self.request.user, month__year=datetime.datetime.now().year)
        else:
            qs = FinPlanTypeOne.objects.filter(head=self.request.user, month__year=datetime.datetime.now().year)
            if not qs:
                year = datetime.datetime.now().year
                for i in range(1, 13):
                    FinPlanTypeOne.objects.create(head=self.request.user, month=datetime.datetime(year, i, 1))
                qs = FinPlanTypeOne.objects.filter(head=self.request.user, month__year=datetime.datetime.now().year)
        return qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset()).order_by('project_type', 'month')
        summ_fields = {'fin_plan': 0, 'month_plan': 0, 'fact_money': 0}
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        for i in data:
            for k, v in i.items():
                if k in summ_fields and v:
                    summ_fields[k] += v
        try:
            summ_fields['percent'] = str(round(summ_fields['fact_money'] / summ_fields['month_plan'] * 100, 2)) + ' %'
        except:
            summ_fields['percent'] = None
        try:
            summ_fields['diff'] = summ_fields['fact_money'] - summ_fields['fin_plan']
        except:
            summ_fields['diff'] = None
        summ_fields['month'] = None
        summ_fields['id'] = None
        summ_fields['head'] = None
        data.insert(0, collections.OrderedDict(summ_fields))
        return Response(data)


class ApiSetManagersPlanTypeOneView(viewsets.ModelViewSet):
    queryset = ManagersPlanTypeOne.objects.all()
    serializer_class = ManagersPlanTypeOneSerializerRO
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('fin_plan', 'manager')

    def get_serializer_context(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            if self.__dict__['get_all_managers']:
                manager_id = Manager.objects.filter(head=self.request.user).values('id')
                manager_id = [str(x['id']) for x in manager_id]
                if Manager.objects.filter(user=self.request.user).exists():
                    manager_id.append(str(self.request.user.id))
                f_p = FinPlanTypeOne.objects.get(pk=int(self.request.GET.get('fin_plan')))
                self.__dict__['f_p'] = f_p
                first_month = f_p.month
                last_month = f_p.month + relativedelta(months=1)
            else:
                manager_id = [str(self.request.user.managers.id)]
                first_month = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month, 1)
                last_month = first_month + relativedelta(months=1)
            one_c_context = {}
            one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, manager_id=','.join(manager_id)))
            if one_c_data[0] != {}:
                for bill in one_c_data:
                    if bill['МенеджерИД'] in one_c_context:
                        one_c_context[bill['МенеджерИД']].append(float(bill['ОплаченнаяСумма']))
                    else:
                        one_c_context[bill['МенеджерИД']] = [float(bill['ОплаченнаяСумма'])]
            return one_c_context

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if self.__dict__['get_all_managers']:
            qs = FinPlanTypeOne.objects.filter(head=request.user, month__year=datetime.datetime.now().year)
            summ_fields = {'month_plan': 0}
            for i in qs:
                if i.month_plan:
                    summ_fields['month_plan'] += i.month_plan
            summ_fields['manager'] = None
            summ_fields['month'] = None
            summ_fields['plan_app_count'] = None
            summ_fields['plan_processing_db'] = None
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        if self.__dict__['get_all_managers']:
            f_p = self.__dict__.get('f_p')
            if f_p:
                summ_fields['month_plan'] = f_p.month_plan
            data.insert(0, collections.OrderedDict(summ_fields))
        return Response(data)

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return ManagersPlanTypeOneSerializerRO
        return ManagersPlanTypeOneSerializer

    def get_queryset(self):
        self.__dict__['get_all_managers'] = True
        if not self.request.user.groups.filter(name='kvs_head').exists():
            self.__dict__['get_all_managers'] = False
            date = datetime.datetime.now()
            return ManagersPlanTypeOne.objects.filter(manager=self.request.user.managers,
                                                      fin_plan__month__month=date.month,
                                                      fin_plan__month__year=date.year)
        return ManagersPlanTypeOne.objects.all()


class ApiSetManagerContactReportTypeOne(viewsets.ModelViewSet):
    serializer_class = ManagersReportContactTypeOneSerializer
    permission_classes = (DjangoModelPermissions,)

    def get_queryset(self):
        if self.request.user.groups.filter(name='kvs_head').exists():
            return Manager.objects.filter(head=self.request.user)
        return Manager.objects.filter(user=self.request.user)

    def get_serializer_context(self):
        filters_context = {}
        one_c_context = {}
        date_lt = self.request.GET.get('date_lt')
        date_gt = self.request.GET.get('date_gt')
        if not self.request.GET.get('report'):
            if date_gt:
                filters_context['first_month'] = datetime.datetime.strptime(date_gt, '%Y-%m-%d').date()
            if date_lt:
                filters_context['last_month'] = datetime.datetime.strptime(date_lt, '%Y-%m-%d').date()
                manager_list = Manager.objects.filter(head=self.request.user).values_list('id', flat=True)
                manager_list = [str(x) for x in manager_list]
            filters_context['manager_id'] = ','.join(manager_list)
            one_c_data = json.loads(manage_data.getSumTable(**filters_context))
            if one_c_data[0] != {}:
                for bill in one_c_data:
                    if bill['МенеджерИД'] in one_c_context:
                        one_c_context[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                    else:
                        one_c_context[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
        return {'date_lt': date_lt, 'date_gt': date_gt, 'one_c_context': one_c_context}


class ApiSetManagerContactReportTypeTwo(viewsets.ModelViewSet):
    serializer_class = ManagersReportContactTypeTwoSerializer
    permission_classes = (DjangoModelPermissions,)

    def list(self, request, *args, **kwargs):
        instance = self.get_queryset()
        serializer = self.get_serializer(instance, many=True)
        data = serializer.data

        def get_key(key_dict):
            sum_data = {}
            for key in key_dict.keys():
                if key != 'name':
                    sum_data[key] = 0
                if type(key_dict[key]) == dict:
                    sum_data[key] = get_key(key_dict[key])
            return sum_data

        if data:
            sum_data = get_key(data[0])
            for i in data:
                for key, value in i.items():
                    if sum_data.get(key) is not None and type(sum_data.get(key)) != dict and value is not None:
                        sum_data[key] += value
                    if type(sum_data.get(key)) == dict:
                        for key_ch, value_ch in i.get(key).items():
                            if sum_data[key].get(key_ch) is not None:
                                sum_data[key][key_ch] += value_ch
            last_row = collections.OrderedDict(sum_data)
            last_row['name'] = 'Итого'
            data.append(last_row)
        return Response(data)

    def get_queryset(self):
        if self.request.user.groups.filter(name='head_ad').exists():
            return Manager.objects.filter(Q(head=self.request.user) | Q(user=self.request.user))
        return Manager.objects.filter(user=self.request.user)

    def get_serializer_context(self):
        filters_context = {}
        one_c_context = {}
        date_lt = self.request.GET.get('date_lt')
        date_gt = self.request.GET.get('date_gt')
        project = self.request.GET.get('membership__project__in').split(',')
        if not self.request.GET.get('report'):
            if date_gt:
                filters_context['first_month'] = datetime.datetime.strptime(date_gt, '%Y-%m-%d').date()
            if date_lt:
                filters_context['last_month'] = datetime.datetime.strptime(date_lt, '%Y-%m-%d').date()
            manager_list = Manager.objects.filter(Q(head=self.request.user) | Q(user=self.request.user)).values_list('id', flat=True)
            manager_list = [str(x) for x in manager_list]
            filters_context['manager_id'] = ','.join(manager_list)
            one_c_data = json.loads(manage_data.getSumTable(**filters_context))
            if one_c_data[0] != {}:
                for bill in one_c_data:
                    if bill['МенеджерИД'] in one_c_context:
                        one_c_context[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                    else:
                        one_c_context[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
        return {'date_lt': date_lt, 'date_gt': date_gt, 'one_c_context': one_c_context, 'project': project}


class ApiSetManagerContactReportIndividualSOI(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    filter_backends = (DjangoFilterBackend,)
    serializer_class = ManagerContactReportIndividualSOISerializer
    filter_fields = {
        'date': ['date__gte', 'date__lte'],
        'project': ['in']
    }

    def get_queryset(self):
        return IndividualContact.objects.values('source_of_information').\
            annotate(sof_count=Count('source_of_information'))


class ApiSetManagerContactReportIndividualTypeTwo(viewsets.ModelViewSet):
    serializer_class = ManagersReportContactIndividualTypeTwoSerializer
    permission_classes = (DjangoModelPermissions,)

    def list(self, request, *args, **kwargs):
        instance = self.get_queryset()
        serializer = self.get_serializer(instance, many=True)
        data = serializer.data

        def get_key(key_dict):
            sum_data = {}
            for key in key_dict.keys():
                if key != 'name':
                    sum_data[key] = 0
                if type(key_dict[key]) == dict:
                    sum_data[key] = get_key(key_dict[key])
            return sum_data

        if data:
            sum_data = get_key(data[0])
            for i in data:
                for key, value in i.items():
                    if sum_data.get(key) is not None and type(sum_data.get(key)) != dict and value is not None:
                        sum_data[key] += value
                    if type(sum_data.get(key)) == dict:
                        for key_ch, value_ch in i.get(key).items():
                            if sum_data[key].get(key_ch) is not None:
                                sum_data[key][key_ch] += value_ch
            last_row = collections.OrderedDict(sum_data)
            last_row['name'] = 'Итого'
            data.append(last_row)
        return Response(data)

    def get_queryset(self):
        if self.request.user.groups.filter(name='head_ad').exists():
            return Manager.objects.filter(Q(head=self.request.user) | Q(user=self.request.user))
        return Manager.objects.filter(user=self.request.user)

    def get_serializer_context(self):
        filters_context = {}
        one_c_context = {}
        date_lt = self.request.GET.get('date_lt')
        date_gt = self.request.GET.get('date_gt')
        project = self.request.GET.get('membership__project__in').split(',')
        if not self.request.GET.get('report'):
            if date_gt:
                filters_context['first_month'] = datetime.datetime.strptime(date_gt, '%Y-%m-%d').date()
            if date_lt:
                filters_context['last_month'] = datetime.datetime.strptime(date_lt, '%Y-%m-%d').date()
            manager_list = Manager.objects.filter(Q(head=self.request.user) | Q(user=self.request.user)).values_list('id', flat=True)
            manager_list = [str(x) for x in manager_list]
            filters_context['manager_id'] = ','.join(manager_list)
            one_c_data = json.loads(manage_data.getSumTable(**filters_context))
            if one_c_data[0] != {}:
                for bill in one_c_data:
                    if bill['МенеджерИД'] in one_c_context:
                        one_c_context[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                    else:
                        one_c_context[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
        return {'date_lt': date_lt, 'date_gt': date_gt, 'one_c_context': one_c_context, 'project': project}


class ApiSetProjectsView(viewsets.ModelViewSet):
    serializer_class = ProjectSerializerROMin
    permission_classes = (IsAuthenticated, DjangoModelPermissions, )

    def get_queryset(self):
        status_project = ['working', 'planning']
        if self.request.GET.get('all'):
            status_project.append('finished')
        if self.request.user.is_superuser:
            return Project.objects.filter(status__in=status_project)
        if self.request.user.groups.filter(name='project_managers').exists():
            if Project_type.objects.filter(director=self.request.user).exists():
                return Project.objects.filter(project_type__director=self.request.user,
                                              status__in=status_project).distinct()
            return Project.objects.filter(user=self.request.user,
                                          status__in=status_project)
        if self.request.user.groups.filter(name='head_ad').exists():
            return Project.objects.filter(Q(user=self.request.user) | Q(attr_visitors__user=self.request.user),
                                          status__in=status_project).distinct()
        if self.request.user.groups.filter(name='kvs_head').exists():
            return Project.objects.filter(Q(user=self.request.user) | Q(project_type__director=self.request.user),
                                          status__in=status_project).distinct()
        elif self.request.user.groups.filter(Q(name='kvs') & ~Q(name='kvs_head')).exists():
            return Project.objects.filter(Q(user=self.request.user),
                                          status__in=status_project).distinct()
        return Project.objects.filter(membership__manager=self.request.user.managers,
                                      status__in=status_project).distinct()


class ApiSetManagersReportContactView(viewsets.ModelViewSet):
    permission_classes = (DjangoModelPermissions, )
    serializer_class = ManagersReportContactSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = {'membership__project': ['in']}

    def list(self, request, *args, **kwargs):
        instance = self.get_queryset()
        serializer = self.get_serializer(instance, many=True)
        data = serializer.data
        if self.request.GET.get('report'):
            result = {'vip': 0, 'a': 0, 'b': 0, 'c': 0, 'vip_in': 0, 'a_in': 0, 'b_in': 0, 'c_in': 0,
                      'all': 0, 'first_in': 0}
            for i in data:
                for key, value in result.items():
                    if key == 'all' or key == 'first_in':
                        result[key] += i.get('all_clients')[key]
                    else:
                        result[key] += i.get('groups')[key]
            last_row = data[0].copy()
            for i in last_row:
                last_row[i] = None
            last_row_perc = last_row.copy()
            last_row['all_clients'] = {}
            last_row_perc['groups'] = {}
            last_row_perc['all_clients'] = {}
            last_row['groups'] = {}
            last_row['manager'] = 'Итого'
            for key, value in result.items():
                if key == 'all' or key == 'first_in':
                    last_row['all_clients'][key] = value
                else:
                    last_row['groups'][key] = value
            if last_row['all_clients']['all'] != 0:
                last_row_perc['all_clients']['first_in'] = \
                    str(round((last_row['all_clients']['first_in']/last_row['all_clients']['all']) * 100, 2)) + ' %'
            if last_row['groups']['vip'] != 0:
                last_row_perc['groups']['vip_in'] = \
                    str(round((last_row['groups']['vip_in'] / last_row['groups']['vip']) * 100, 2)) + ' %'
            if last_row['groups']['a'] != 0:
                last_row_perc['groups']['a_in'] = \
                    str(round((last_row['groups']['a_in'] / last_row['groups']['a']) * 100, 2)) + ' %'
            if last_row['groups']['b'] != 0:
                last_row_perc['groups']['b_in'] = \
                    str(round((last_row['groups']['b_in'] / last_row['groups']['b']) * 100, 2)) + ' %'
            if last_row['groups']['c'] != 0:
                last_row_perc['groups']['c_in'] = \
                    str(round((last_row['groups']['c_in'] / last_row['groups']['c']) * 100, 2)) + ' %'
            last_row_perc['manager'] = 'Итого %'
            data.append(last_row)
            data.append(last_row_perc)
        else:
            def get_key(key_dict):
                sum_data = {}
                for key in key_dict.keys():
                    if key != 'name':
                        sum_data[key] = 0
                    if type(key_dict[key]) == dict:
                        sum_data[key] = get_key(key_dict[key])
                return sum_data
            # sum_data = {}
            if data:
                # for key in data[0].keys():
                #     if key != 'name':
                #         sum_data[key] = 0
                sum_data = get_key(data[0])
                # last_row = data[0].copy()
                for i in data:
                    for key, value in i.items():
                        if sum_data.get(key) is not None and type(sum_data.get(key)) != dict and value is not None:
                            sum_data[key] += value
                            # last_row[key] = sum_data.get(key)
                        if type(sum_data.get(key)) == dict:
                            for key_ch, value_ch in i.get(key).items():
                                if sum_data[key].get(key_ch) is not None:
                                    sum_data[key][key_ch] += value_ch
                                    # last_row[key][key_ch] = sum_data.get(key).get(key_ch)
                last_row = collections.OrderedDict(sum_data)
                last_row['name'] = 'Итого'
                data.append(last_row)
        # here you can manipulate your data response
        return Response(data)

    def get_serializer_context(self):
        filters_context = {}
        one_c_context = {}
        project = self.request.GET.get('membership__project__in')
        if not project:
            raise serializers.ValidationError({'error': 'project is required'})
        date_lt = self.request.GET.get('date_lt')
        date_gt = self.request.GET.get('date_gt')
        if not self.request.GET.get('report'):
            if date_gt:
                filters_context['first_month'] = datetime.datetime.strptime(date_gt, '%Y-%m-%d').date()
            if date_lt:
                filters_context['last_month'] = datetime.datetime.strptime(date_lt, '%Y-%m-%d').date()
            filters_context['project_id'] = project
            one_c_data = json.loads(manage_data.getSumTable(**filters_context))
            if one_c_data[0] != {}:
                for bill in one_c_data:
                    if bill['МенеджерИД'] in one_c_context:
                        one_c_context[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                    else:
                        one_c_context[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
        return {'project': project.split(','), 'date_lt': date_lt, 'date_gt': date_gt, 'one_c_context': one_c_context}

    def get_serializer_class(self):
        if self.request.GET.get('report'):
            return ReportWorkGroupClientsSerializer
        else:
            return ManagersReportContactSerializer

    def get_queryset(self):
        if self.request.user.groups.filter(name='managers').exists():
            return Manager.objects.filter(membership__manager_id=self.request.user.managers.id,
                                          membership__project__status__in=['working', 'planning']).distinct()
        if self.request.user.groups.filter(name='head_expo').exists():
            return Manager.objects.filter(membership__project__user__in=list(self.request.user.my_managers.all().values_list('user', flat=True)),
                                          membership__project__status__in=['working', 'planning']).distinct()
        if self.request.user.is_superuser:
            return Manager.objects.filter(membership__project__user__in=Project.objects.filter(
                id__in=self.request.GET.get('membership__project__in').split(',')).values_list('user', flat=True),
                                          membership__project__status__in=['working', 'planning']).distinct()
        return Manager.objects.filter(membership__project__user=self.request.user,
                                      membership__project__status__in=['working', 'planning']).distinct()


class ApiSetClientsReportContactView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientContactReportSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', )

    def list(self, request, *args, **kwargs):
        if not self.request.GET.get('manager__in'):
            return JsonResponse(status=400, data={'error': 'no_manager'})
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_filter_dict(self):
        date_lt = self.request.GET.get('date_lt')
        date_gt = self.request.GET.get('date_gt')
        filter_params = {}
        exclude_params = ('order', 'offset', 'limit', 'search', 'sort')
        for k, v in self.request.GET.items():
            if k not in exclude_params and v != '':
                if ',' in v:
                    filter_params[k] = v.split(',')
                else:
                    filter_params[k] = v
        if type(filter_params['project__in']) != list:
            filter_params['project__in'] = filter_params['project__in'].split(',')

        if type(filter_params['manager__in']) != list:
            filter_params['manager__in'] = filter_params['manager__in'].split(',')
        if filter_params.get('result'):
            filter_params[filter_params.get('result')] = True
            filter_params.pop('result')
        return filter_params

    def get_serializer_context(self):
        date_lt = self.request.GET.get('date_lt')
        date_gt = self.request.GET.get('date_gt')
        filter_params = {}
        exclude_params = ('order', 'offset', 'limit', 'search')
        for k, v in self.request.GET.items():
            if k not in exclude_params and v != '':
                if ',' in v:
                    filter_params[k] = v.split(',')
                else:
                    filter_params[k] = v
        if type(filter_params['project__in']) != list:
            filter_params['project__in'] = filter_params['project__in'].split(',')

        if type(filter_params['manager__in']) != list:
            filter_params['manager__in'] = filter_params['manager__in'].split(',')
        if filter_params.get('result'):
            filter_params[filter_params.get('result')] = True
            filter_params.pop('result')
        return {'filter': filter_params}

    def get_queryset(self):
        filter_f = {}

        if self.request.GET.get('date__date__gte'):
            filter_f['contact__date__date__gte'] = self.request.GET.get('date__date__gte')
        if self.request.GET.get('date__date__lte'):
            filter_f['contact__date__date__lte'] = self.request.GET.get('date__date__lte')
        if self.request.GET.get('result'):
            res = self.request.GET.get('result')
            res = res.split(',')
            res = {'contact__%s' % x: True for x in res}
            filter_f = {**filter_f, **res}
        if self.request.user.groups.filter(Q(name='head_ad') | Q(name='ad')):
            return Client.objects.filter(**filter_f, contact__isnull=False,
                                     contact__project__in=self.request.GET.get('project__in').split(',')).\
            prefetch_related(Prefetch(queryset=Contact.objects.filter(**self.get_filter_dict()).prefetch_related('manager__user'), to_attr='contacts', lookup='contact')).distinct()
        if len(self.request.GET.get('project__in').split(',')) == 1:
            subject = Project.objects.get(pk=self.request.GET.get('project__in').split(',')[0]).subjects_id
            return Client.objects.filter(**filter_f, membership__project__in=self.request.GET.get('project__in').split(','),
                                     contact__isnull=False,
                                     contact__project__in=self.request.GET.get('project__in').split(',')).\
            prefetch_related(Prefetch(queryset=Contact.objects.filter(**self.get_filter_dict()).prefetch_related('manager__user'), to_attr='contacts', lookup='contact')).extra(select={
            'group_contact': """SELECT "group"
                FROM clients_clientgroup
                WHERE clients_clientgroup.client_id=clients_client.id AND clients_clientgroup.subjects_id=%s ORDER BY id DESC LIMIT 1""" % subject
        }).distinct()
        return Client.objects.filter(**filter_f, membership__project__in=self.request.GET.get('project__in').split(','),
                                     contact__isnull=False,
                                     contact__project__in=self.request.GET.get('project__in').split(',')).\
            prefetch_related(Prefetch(queryset=Contact.objects.filter(**self.get_filter_dict()).prefetch_related('manager__user'), to_attr='contacts', lookup='contact')).distinct()


class ApiSetClientsReportUpdatedView(viewsets.ViewSet):
    queryset = Client.objects.all()
    permission_classes = (DjangoModelPermissions, )

    def get_url_params(self, filter_dict, isnull):
        if isnull:
            filter_dict['contacts_person__isnull'] = True
        url = '/apiclients/clients/?'
        for k, v in filter_dict.items():
            if url[-1] != '?':
                url += '&'
            if type(v) == list:
                url += '%s=%s' % (k, ','.join(v))
            else:
                url += '%s=%s' % (k, v)

        return url


    def get_filter_params(self):
        exclude_params = ['order']
        filters_field = {}
        filters_field_args = []
        response_data = {}
        temp = {}
        temp_inactive = {'date_to_not_active__isnull': False, 'status': 'inactive'}
        for k, v in self.request.GET.items():
            if k not in exclude_params and v != '':
                if ',' in v:
                    if 'created_user' in k:
                        filters_field_args.\
                            append(Q(**{k: v.split(',')}) | Q(**{'edited_user__managers__in': v.split(',')}))
                        temp['edited_user__managers__in'] = v.split(',')
                        temp[k] = v.split(',')
                    else:
                        filters_field[k] = v.split(',')
                elif 'date__lte' in k or 'date__gte' in k:
                    filters_field['date_created__date__%s' % (k.split('__')[1],)] = v
                    temp['date_created__date__%s' % (k.split('__')[1],)] = v
                    filters_field['date_updated__date__%s' % (k.split('__')[1],)] = v
                    temp['date_updated__date__%s' % (k.split('__')[1],)] = v
                    temp_inactive['date_to_not_active__%s' % (k.split('__')[1],)] = v
                    filters_field_args. \
                        append(Q(**{'date_created__date__%s' % (k.split('__')[1],): v})
                               | Q(**{'date_updated__date__%s' % (k.split('__')[1],): v}))
                else:
                    if 'created_user' in k:
                        filters_field_args.\
                            append(Q(**{k: v}) | Q(**{'edited_user__managers__in': v}))
                        temp['edited_user__managers__in'] = v
                        temp[k] = v
                    else:
                        filters_field[k] = v.split(',')
        return filters_field_args, filters_field, temp, temp_inactive

    def list(self, request):
        delete_val = ['date_created__date__lte', 'date_created__date__gte',
                      'date_updated__date__lte', 'date_updated__date__gte',
                      'created_user__managers__in']
        response_data = {'no_contact': {'filter': {}}, 'created': {'filter': {}}, 'to_inactive': {'filter': {}}}
        filters_field_args, filters_field, temp, temp_inactive = self.get_filter_params()
        [filters_field.pop(i, None) for i in delete_val]
        response_data['all'] = Client.objects.filter(*filters_field_args, **filters_field).distinct().count()
        filters_field.update(temp)
        filters_field.pop('created_user__managers__in', None)
        filters_field.pop('edited_user__managers__in', None)
        response_data['no_contact']['value'] = Client.objects.filter(**filters_field,
                                                                     contacts_person__isnull=True).count()
        response_data['no_contact']['url'] = self.get_url_params(filters_field, True)
        response_data['no_contact']['filter'] = filters_field.copy()
        filters_field.update(temp)
        filters_field.pop('contacts_person__isnull', None)
        filters_field.pop('date_created__date__lte', None)
        filters_field.pop('date_created__date__gte', None)
        filters_field.pop('created_user__managers__in', None)
        response_data['edited'] = Client.objects.filter(**filters_field).count()
        filters_field.update(temp)
        filters_field.pop('date_updated__date__lte', None)
        filters_field.pop('date_updated__date__gte', None)
        filters_field.pop('edited_user__managers__in', None)
        response_data['created']['value'] = Client.objects.filter(**filters_field).count()
        response_data['created']['url'] = self.get_url_params(filters_field, False)
        response_data['created']['filter'] = filters_field.copy()
        filters_field.update(temp_inactive)
        response_data['to_inactive']['value'] = Client.objects.filter(**filters_field).count()
        response_data['to_inactive']['url'] = self.get_url_params(filters_field, False)
        response_data['to_inactive']['filter'] = filters_field.copy()
        data_array = [response_data]
        return JsonResponse(data_array, safe=False)


class ApiMessageView(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    @list_route(methods=['post'])
    def manager(self, request):
        return 1


class ApiSetClientsReportUpdatedTypeOneView(viewsets.ViewSet):
    queryset = Client.objects.all()
    permission_classes = (DjangoModelPermissions, )

    def get_url_params(self, filter_dict, isnull):
        if isnull:
            filter_dict['contacts_person__isnull'] = True
        url = '/apiclients/clients/?'
        for k, v in filter_dict.items():
            if url[-1] != '?':
                url += '&'
            if type(v) == list:
                url += '%s=%s' % (k, ','.join(v))
            else:
                url += '%s=%s' % (k, v)

        return url

    def get_filter_params(self):
        exclude_params = ['order']
        filters_field = {}
        filters_field_args = []
        response_data = {}
        temp = {}
        temp_inactive = {'date_to_not_active__isnull': False, 'status': 'inactive'}
        for k, v in self.request.GET.items():
            if k not in exclude_params and v != '':
                if ',' in v:
                    if 'created_user' in k:
                        filters_field_args.\
                            append(Q(**{k: v.split(',')}) | Q(**{'edited_user__managers__in': v.split(',')}))
                        temp['edited_user__managers__in'] = v.split(',')
                        temp[k] = v.split(',')
                    else:
                        filters_field[k] = v.split(',')
                elif 'date__lte' in k or 'date__gte' in k:
                    filters_field['date_created__date__%s' % (k.split('__')[1],)] = v
                    temp['date_created__date__%s' % (k.split('__')[1],)] = v
                    filters_field['date_updated__date__%s' % (k.split('__')[1],)] = v
                    temp['date_updated__%s' % (k.split('__')[1],)] = v
                    temp_inactive['date_to_not_active__%s' % (k.split('__')[1],)] = v
                    filters_field_args. \
                        append(Q(**{'date_created__date__%s' % (k.split('__')[1],): v})
                               | Q(**{'date_updated__date__%s' % (k.split('__')[1],): v}))
                else:
                    if 'created_user' in k:
                        filters_field_args.\
                            append(Q(**{k: v}) | Q(**{'edited_user__managers__in': v}))
                        temp['edited_user__managers__in'] = v.split(',')
                        temp[k] = v.split(',')
                    else:
                        filters_field[k] = v
        return filters_field_args, filters_field, temp, temp_inactive

    def list(self, request):

        delete_val = ['date_created__date__lt', 'date_created__date__gt',
                      'date_updated__date__lt', 'date_updated__date__gt',
                      'created_user__managers__in', 'client_change_db']
        response_data = {'no_contact': {'filter': {}}, 'created': {'filter': {}}, 'to_inactive': {'filter': {}}}

        filters_field_args, filters_field, temp, temp_inactive = self.get_filter_params()
        [filters_field.pop(i, None) for i in delete_val]
        response_data['all'] = {}
        response_data['all']['value'] = Client.objects.filter(*filters_field_args, **filters_field).distinct().count()
        response_data['all']['url'] = self.get_url_params({i[0]: i[1] for i in filters_field_args[0].children}, False)
        response_data['all']['filter'] = filters_field
        filters_field.update(temp)
        # Думать тут над отчетом
        filters_field.pop('created_user__managers__in', None)
        filters_field.pop('edited_user__managers__in', None)
        filters_field.pop('date_updated__lte', None)
        filters_field.pop('date_updated__gte', None)
        response_data['no_contact']['value'] = Client.objects.filter(**filters_field,
                                                                     contacts_person__isnull=True).count()
        response_data['no_contact']['url'] = self.get_url_params(filters_field, True)
        response_data['no_contact']['filter'] = filters_field.copy()
        filters_field.update(temp)
        filters_field.pop('contacts_person__isnull', None)
        filters_field.pop('date_created__date__lt', None)
        filters_field.pop('date_created__date__gt', None)
        filters_field.pop('created_user__managers__in', None)
        response_data['edited'] = Client.objects.filter(**filters_field).count()
        filters_field.update(temp)
        filters_field.pop('date_updated__date__lt', None)
        filters_field.pop('date_updated__date__gt', None)
        filters_field.pop('edited_user__managers__in', None)
        filters_field.pop('date_updated__lte', None)
        filters_field.pop('date_updated__gte', None)
        response_data['created']['value'] = Client.objects.filter(**filters_field).count()
        response_data['created']['url'] = self.get_url_params(filters_field, False)
        response_data['created']['filter'] = filters_field.copy()
        filters_field.update(temp_inactive)
        response_data['to_inactive']['value'] = Client.objects.filter(**filters_field).count()
        response_data['to_inactive']['url'] = self.get_url_params(filters_field, False)
        response_data['to_inactive']['filter'] = filters_field.copy()
        data_array = [response_data]
        return JsonResponse(data_array, safe=False)


class ApiSetStartAlertView(viewsets.ModelViewSet):
    queryset = StartAlert.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = StartAlertSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('viewed', 'user')


class ApiSetUserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('direction__type_project_type', 'managers__head')
    serializer_class = UserSerializer


class NotificationView(APIView):
    permission_classes = (IsAdminUser, IsAuthenticated)

    def post(self, request):
        Channel('send_notification').send({
            'users': request.data.get('user_id'),
            'block_working': request.data.get('block_working'),
            'message': request.data.get('message'),
            'time': request.data.get('time')
        })
        return Response(status=200, data={'status': 'ok'})

    def patch(self, request):
        cache.set('block_working'+settings.MEMCACHED_NAME, False, None)
        data = request.data
        socket_data = {}
        if data.get('clear'):
            socket_data['clear'] = True
        Channel('stop_block').send(socket_data)
        return Response(status=200, data={'status': 'ok'})


# class ApiSetEntityMergeView(viewsets.ModelViewSet):
#     queryset = Entity.objects.all()
#     serializer_class = EntityMergeSerializer
#     permission_classes = (DjangoModelPermissions, )
#     filter_backends = (DjangoFilterBackend, filters.SearchFilter)
#     search_fields = ('full_name', )
#     filter_fields = ('applicationleasekvs__project', 'applicationexpo__project')
#
#     def get_queryset(self):
#         if self.request.GET.get('project') != 'all':
#             project_list = [self.request.GET.get('project')]
#         else:
#             project_list = Project.objects.filter(project_type=self.request.GET.get('project_type')).\
#                 values_list('id', flat=True)
#         project_type = Project_type.objects.get(pk=self.request.GET.get('project_type'))
#         p_t = project_type.type_project_type
#         c = {
#             'kvs': 'applicationleasekvs__project__in',
#             'ad': 'applicationad__project__in',
#             'expo': 'applicationexpo__project__in'
#         }
#         return Entity.objects.filter(**{c[p_t]: project_list})
#
#     def get_serializer_context(self):
#         if self.request.GET.get('project') != 'all':
#             project_list = [self.request.GET.get('project')]
#         else:
#             project_list = Project.objects.filter(project_type=self.request.GET.get('project_type')).\
#                 values_list('id', flat=True)
#         project_type = Project_type.objects.get(pk=self.request.GET.get('project_type'))
#         return {'project': project_list, 'project_type': project_type.type_project_type}


class ApiSetEntityMergeView(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = EntityMergeSerializer
    permission_classes = (DjangoModelPermissions, )
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('full_name', )
    filter_fields = ('applicationleasekvs__project', 'applicationexpo__project')

    def get_queryset(self):
        if self.request.GET.get('project') != 'all':
            project_list = [self.request.GET.get('project')]
        else:
            project_list = Project.objects.filter(project_type=self.request.GET.get('project_type')).\
                values_list('id', flat=True)
        project_type = Project_type.objects.get(pk=self.request.GET.get('project_type'))
        p_t = project_type.type_project_type
        c_ent = {
            'kvs': 'applicationleasekvs',
            'ad': 'applicationad',
            'expo': 'applicationexpo'
        }
        params = ['id', 'name', c_ent[p_t]+'__id', c_ent[p_t]+'__entity_id', c_ent[p_t]+'__client_id', c_ent[p_t]+'__manager_id', c_ent[p_t]+'__entity__full_name']
        if p_t == 'kvs':
            params.append(c_ent[p_t]+'__bill_number')
        return Project.objects.filter(id__in=project_list).values(*params).order_by(c_ent[p_t]+'__entity__full_name')

    def get_serializer_context(self):
        if self.request.GET.get('project') != 'all':
            project_list = [self.request.GET.get('project')]
        else:
            project_list = Project.objects.filter(project_type=self.request.GET.get('project_type')).\
                values_list('id', flat=True)
        project_type = Project_type.objects.get(pk=self.request.GET.get('project_type'))
        return {'project': project_list, 'project_type': project_type.type_project_type}


class ApiDvpReportView(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ('date_start',)
    ordering = ('date_start',)
    filter_fields = {'date_start': ['year'], 'user': ['exact'], 'project_type__director': ['exact'],
                     'status': ['exact', 'in']}
    serializer_class = DvpReportSerializer

    def get_queryset(self):
        return Project.objects.prefetch_related(Prefetch('project_indicators', to_attr='plan')).\
            filter(project_type__type_project_type='expo').extra(select={
            'applicationexpo__count': """SELECT COUNT(*)
            FROM clients_applicationexpo
            WHERE clients_applicationexpo.project_id=projects_project.id
            AND status != 'canceled' AND main_id is null""",
            'on_approval': """exists(SELECT 1 
            FROM projects_projectreport, projects_projectindicators
            WHERE approval_status='on_approval'
            AND projects_projectreport.project_indicators_id=projects_projectindicators.id 
            AND projects_projectindicators.project_id=projects_project.id)""",
            'return_val': """exists(SELECT 1 
                    FROM projects_projectreport, projects_projectindicators
                    WHERE approval_status='return'
                    AND projects_projectreport.project_indicators_id=projects_projectindicators.id 
                    AND projects_projectindicators.project_id=projects_project.id)""",
            'count_vis_fact': """SELECT COUNT(*)
                    FROM "TICKETS_Tickets", "PROJECT_projects", "TICKETS_Order"
                    WHERE "TICKETS_Tickets".order_id="TICKETS_Order".id
                    AND "TICKETS_Order".project_id="PROJECT_projects".id
                    AND "PROJECT_projects".p_expobit=projects_project.id::text 
                    AND "TICKETS_Tickets".state=true""",
            'sum_amount_fact': """SELECT SUM(amount)
                    FROM "TICKETS_Order", "PROJECT_projects"
                    WHERE "TICKETS_Order".project_id="PROJECT_projects".id
                    AND "PROJECT_projects".p_expobit=projects_project.id::text 
                    AND "TICKETS_Order".state=1"""
        })

    def get_serializer_context(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            end_date = datetime.date.today()
            if self.request.GET.get('date_start__year'):
                end_date = datetime.date(int(self.request.GET.get('date_start__year')), 12, 31)
            start_date = datetime.date(end_date.year-1, 1, 1)
            qs = self.filter_queryset(self.get_queryset())
            projects_id = [str(x) for x in qs.values_list('id', flat=True)]
            if len(projects_id) != 0:
                one_c_data = json.loads(manage_data.getSumTableBill(start_date, end_date, ','.join(projects_id)))
                one_c_context = {'pay': {}, 'not_pay': {}, 'sum': {}}
                if one_c_data[0] != {}:
                    for bill in one_c_data:
                        if bill['ПроектИД'] in one_c_context['pay']:
                            one_c_context['pay'][bill['ПроектИД']].append(int(float(bill['ОплаченнаяСумма'])))
                            one_c_context['not_pay'][bill['ПроектИД']].append(int(float(bill['НеоплаченнаяСумма'])))
                            one_c_context['sum'][bill['ПроектИД']].append(int(float(bill['СуммаСчета'])))
                        else:
                            one_c_context['pay'][bill['ПроектИД']] = [int(float(bill['ОплаченнаяСумма']))]
                            one_c_context['not_pay'][bill['ПроектИД']] = [int(float(bill['НеоплаченнаяСумма']))]
                            one_c_context['sum'][bill['ПроектИД']] = [int(float(bill['СуммаСчета']))]
                return one_c_context
        return {}


class UserControlView(APIView):
    permission_classes = (IsAdminUser, IsAuthenticated)

    def get(self, request):
        active_users = cache.get('active_users' + settings.MEMCACHED_NAME)
        qs = User.objects.filter(id__in=list(active_users)).values('id', 'last_name', 'first_name')
        qs = {x['id']: x for x in qs}
        json_array = []
        for i in active_users:
            active_users[i]['id'] = i
            active_users[i]['user'] = '%s %s' % (qs[i]['last_name'], qs[i]['first_name'])
            json_array.append(active_users[i])
        return JsonResponse(data=json_array, safe=False)


models_dict = {
    'project_type': Project_type.objects.filter(type_project_type='kvs'),
    'buisness_unit': Buisness_unit.objects.filter(project__project_type__type_project_type='kvs'),
    'manager': Manager.objects.filter(head__direction__type_project_type='kvs'),

}

one_c_instance = {
    'user': 'МенеджерИД'
}

serializers_dict = {

}


class ApiReportByProjectView(APIView):
    permission_classes = (IsAdminUser, IsAuthenticated)

    def get(self, request):
        date_start = request.GET.get('date__gt')
        date_start = datetime.date(int(date_start.split('-')[0]), int(date_start.split('-')[1]), 1)
        date_end = request.GET.get('date__lt')
        project_type = request.GET.get('project_type')
        filter_fields = {
            'project_type__type_project_type': 'kvs',
            'status__in': ['working', 'finished'],
            'date_start__year': date_start.year
        }
        if project_type:
            filter_fields['project_type'] = project_type
        date_end = datetime.date(int(date_end.split('-')[0]), int(date_end.split('-')[1]),
                                 calendar.monthrange(int(date_end.split('-')[0]), int(date_end.split('-')[1]))[1])
        category = request.GET.get('category_select')
        instance = models_dict.get(category)
        qs = Project.objects.filter(**filter_fields).values('id', category)
        qs_dict = {str(x['id']): x[category] for x in qs}
        qs_sum = Project.objects.filter(**filter_fields).values(category).annotate(sum_guests=Sum('count_guests'))
        qs_sum_dict = {str(x[category]): x['sum_guests'] for x in qs_sum}
        # if not instance:

        # if len(projects_id) != 0:
        one_c_data = json.loads(manage_data.getSumTable(date_start, date_end, ','.join(list(qs_dict))))
        one_c_context = {}
        sum_types = {}
        sum_types_months = {}
        one_c_context_months = {}
        if one_c_data[0] != {}:
            for bill in one_c_data:
                if not qs_dict.get(bill[one_c_instance.get(category, 'ПроектИД')]):
                    continue
                date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
                key = '%s.%s' % (str(date.year), str(date.month - 1))
                one_c_context_months.setdefault(qs_dict[bill[one_c_instance.get(category, 'ПроектИД')]], {}).setdefault(
                    '%s.%s' % (key, 'fact'), []).append(int(float(bill['ОплаченнаяСумма'])))
                one_c_context.setdefault(qs_dict[bill[one_c_instance.get(category, 'ПроектИД')]], []).append(
                    int(float(bill['ОплаченнаяСумма'])))
                sum_types_months.setdefault(qs_dict[bill[one_c_instance.get(category, 'ПроектИД')]], {}).setdefault('%s.%s' % (key, 'project'),
                                                                                                                    set()).add(
                    bill[one_c_instance.get(category, 'ПроектИД')])
                sum_types.setdefault(qs_dict[bill[one_c_instance.get(category, 'ПроектИД')]], set()).add(
                    bill[one_c_instance.get(category, 'ПроектИД')])
        result = []
        result_months = []
        ranger_dict = {
            'count_project': sum_types_months
        }
        for i in one_c_context:
            temp = {'fact': sum(one_c_context[i]), 'name': i, 'count_projects': len(sum_types[i]),
                    'guest': qs_sum_dict[i]}
            result.append(temp)
        for i in one_c_context_months:
            temp = {'name': i, 'count_projects': len(sum_types[i])}
            if 'fact' in request.GET.get('show_by'):
                for j in one_c_context_months[i]:
                    one_c_context_months[i][j] = sum(one_c_context_months[i][j])
                temp = {**temp, **one_c_context_months[i]}
            if 'project' in request.GET.get('show_by'):
                for j in sum_types_months[i]:
                    sum_types_months[i][j] = len(sum_types_months[i][j])
                temp = {**temp, **sum_types_months[i]}
            result_months.append(temp)
        return JsonResponse(data={
            'plan_table': result,
            'dynamic_plan_table': result_months
        }, safe=False)


# Класс для группировки по неделям в БД queryset filter
class TruncWeek(TruncBase):
    kind = 'week'


class ApiSetReportByWeekView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        report = request.GET.get('report')
        report = ProjectReport.objects.get(pk=report)
        project_id = request.GET.get('project')
        date_start = report.month_date
        date_end = report.month_date + relativedelta(months=1)
        year = report.month_date.year
        month = report.month_date.month
        arr = []
        weeks = calendar.monthcalendar(report.month_date.year, report.month_date.month)
        qs = ApplicationExpo.objects.filter(project_id=project_id,
                                            date__gte=date_start, date__lt=date_end).exclude(status='canceled').\
            annotate(week=TruncWeek('date')).values('week').annotate(c=Count('id')).values('week', 'c')
        for i in range(len(weeks)):
            if i == 0:
                try:
                    apps = (item for item in qs if item['week'].date() <= datetime.date(year, month, weeks[i][-1])).__next__()
                    arr.append({
                        'week_number': i + 1,
                        'week': date_start.isocalendar()[1],
                        's_d': date_start,
                        'e_d': datetime.date(year, month, weeks[i][-1]),
                        'count': apps['c']
                    })
                except StopIteration:
                    arr.append({
                        'week_number': i + 1,
                        'week': date_start.isocalendar()[1],
                        's_d': date_start,
                        'e_d': datetime.date(year, month, weeks[i][-1]),
                        'count': 0
                    })
            elif i >= 1 and i < len(weeks)-1:
                try:
                    apps = (item for item in qs if datetime.date(year, month, weeks[i][0]) <= item['week'].date() and item['week'].date() <= datetime.date(year, month, weeks[i][-1])).__next__()
                    arr.append({
                        'week_number': i + 1,
                        'week': datetime.date(year, month, weeks[i][0]).isocalendar()[1],
                        's_d': datetime.date(year, month, weeks[i][0]),
                        'e_d': datetime.date(year, month, weeks[i][-1]),
                        'count': apps['c']
                    })
                except StopIteration:
                    arr.append({
                        'week_number': i + 1,
                        'week': datetime.date(year, month, weeks[i][0]).isocalendar()[1],
                        's_d': datetime.date(year, month, weeks[i][0]),
                        'e_d': datetime.date(year, month, weeks[i][-1]),
                        'count': 0
                    })
            elif i == len(weeks)-1:
                try:
                    apps = (item for item in qs if item['week'].date() >= datetime.date(year, month, weeks[i][0])).__next__()
                    arr.append({
                        'week_number': i + 1,
                        'week': datetime.date(year, month, weeks[i][0]).isocalendar()[1],
                        's_d':  datetime.date(year, month, weeks[i][0]),
                        'e_d': datetime.date(year, month, calendar.monthrange(year,month)[1]),
                        'count': apps['c']
                    })
                except StopIteration:
                    arr.append({
                        'week_number': i + 1,
                        's_d':  datetime.date(year, month, weeks[i][0]),
                        'week': datetime.date(year, month, weeks[i][0]).isocalendar()[1],
                        'e_d': datetime.date(year, month, calendar.monthrange(year,month)[1]),
                        'count': 0
                    })
        one_c_context = {}
        one_c_data_bill = json.loads(manage_data.getSumTableBill(date_start, date_end-relativedelta(days=1), project_id))
        # one_c_context = {'pay': {}, 'not_pay': {}, 'sum': {}}
        one_c_context_bill = {}
        if one_c_data_bill[0] != {}:
            for bill in one_c_data_bill:
                date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
                key = date.isocalendar()[1]
                if key in one_c_context_bill:
                    one_c_context_bill[key].append(int(float(bill['СуммаСчета'])))
                else:
                    one_c_context_bill[key] = [int(float(bill['СуммаСчета']))]
        one_c_data = json.loads(manage_data.getSumTable(date_start, date_end-relativedelta(days=1), project_id))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
                key = date.isocalendar()[1]
                if key in one_c_context:
                    one_c_context[key].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    one_c_context[key] = [int(float(bill['ОплаченнаяСумма']))]
        return JsonResponse(
            data=ReportByWeekSerializer(arr, many=True,
                                        context={
                                            'one_c': one_c_context,
                                            'one_c_bill': one_c_context_bill}).data, safe=False)


class ApiSetManagersReportContactByGroupView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        project = Project.objects.get(pk=request.GET.get('project'))
        client_all = Client.objects.filter(membership__project_id=project.id).exclude(
            status__in=['black_list', 'inactive']).values_list('id', flat=True)
        all_null = set(client_all.distinct()) - set(Client.objects.filter(
            Q(client_group__subjects_id=project.subjects_id) &
            Q(membership__project_id=project.id)).exclude(
            status__in=['black_list', 'inactive']).values_list('id', flat=True).distinct())
        dict_groups = {
            'all': len(client_all),
            'vip': Contact.objects.filter(
                Q(client__client_group__group='vip') & Q(client__client_group__subjects_id=project.subjects_id),
                type_contact='out_phone', project_id=project.id, client__in=client_all).distinct('client').count(),
            'all_vip': Client.objects.filter(
                Q(client_group__group='vip') & Q(
                    client_group__subjects_id=project.subjects_id), membership__project_id=project.id).exclude(
            status__in=['black_list', 'inactive']).distinct().count(),
            'a': Contact.objects.filter(
                Q(client__client_group__group='a') & Q(
                    client__client_group__subjects_id=project.subjects_id),
                type_contact='out_phone', project_id=project.id, client__in=client_all).distinct('client').count(),
            'all_a': Client.objects.filter(
                Q(client_group__group='a') & Q(
                    client_group__subjects_id=project.subjects_id), membership__project_id=project.id).exclude(
            status__in=['black_list', 'inactive']).distinct().count(),
            'b': Contact.objects.filter(
                Q(client__client_group__group='b') & Q(
                    client__client_group__subjects_id=project.subjects_id),
                type_contact='out_phone', project_id=project.id, client__in=client_all).distinct('client').count(),
            'all_b': Client.objects.filter(
                Q(client_group__group='b') & Q(
                    client_group__subjects_id=project.subjects_id), membership__project_id=project.id).exclude(
            status__in=['black_list', 'inactive']).distinct().count(),
            'c': Contact.objects.filter(
                Q(client__client_group__group='c') & Q(
                    client__client_group__subjects_id=project.subjects_id), project_id=project.id,
                type_contact='out_phone', client__in=client_all).distinct('client').count(),
            'all_c': Client.objects.filter(
                Q(client_group__group='c') & Q(
                    client_group__subjects_id=project.subjects_id), membership__project_id=project.id).exclude(
            status__in=['black_list', 'inactive']).distinct().count(),
            'null': Contact.objects.filter(
                client__in=list(all_null), project_id=project.id,
                type_contact='out_phone').distinct('client').count(),
            'all_null': len(all_null),
        }

        return JsonResponse(data=dict_groups, safe=False)


class ApiSetNotificationsView(viewsets.ModelViewSet):
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissions)

    def get_queryset(self):
        return Notification.objects.filter(user=self.request.user).order_by('-date_created')

    @list_route(methods=['post'],  permission_classes=[IsAuthenticated])
    def read_noty(self, request):
        Notification.objects.filter(user=request.user, show=False).update(show=True)
        return Response(status=200, data={})

    @list_route(methods=['get'], )
    def count_new(self, request):
        return Response(data={'count': Notification.objects.filter(user=request.user, show=False).count()})


class UpdateCacheView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        UpdateCache.objects.filter(user=request.user).update(clear=True)
        return Response(data={'clear': 'ok'}, status=200)


class ApiSetContentTypeView(viewsets.ModelViewSet):
    queryset = ContentType.objects.all()
    permission_classes = (IsAuthenticated, IsAdminUser)
    serializer_class = ContentTypeSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering = ('app_label',)


class LoadCsvView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def update_non_rel(self, model, data, field):
        for row in data:
            if row.get(field):
                model.objects.filter(**{field: row.get(field)}).update(**dict(row))

    def update_with_rel(self, model, data, field, rel_field, fields):
        error = []
        error_rel = []
        for row in data:
            if row.get(field):
                try:
                    obj = model.objects.get(**{field: row.get(field)})
                except ObjectDoesNotExist:
                    error.append(row.get(field))
                    continue
                if obj.id == 361:
                    print('')
                for j in fields:
                    setattr(obj, j, row.get(j))
                obj.save()
                # last = rel_field[-1]
                for j in rel_field:
                    last = j[-1]
                    for f in j:
                        if f != last:
                            temp = getattr(obj, f)
                            continue
                        setattr(temp, f, row.get('__'.join(j)))
                        try:
                            temp.save()
                        except IntegrityError:
                            error_rel.append(temp.id)
        return error, error_rel

    def post(self, request):
        csvfile = request.data.get('file')
        encode = request.data.get('encode')
        obj = ContentType.objects.get(pk=request.data.get('model'))
        field = request.data.get('field')
        model = apps.get_model(app_label=obj.app_label, model_name=obj.model)
        spamreader = csv.DictReader(csvfile.file.read().decode(encode).split("\n"),
                                    delimiter=request.data.get('delimiter'))
        fieldnames = spamreader.fieldnames
        rel_field = []
        fields = []
        for i in fieldnames:
            if len(i.split('__')) > 1:
                rel_field.append(i.split('__'))
                continue
            fields.append(i)
        if rel_field:
            er, er_rel = self.update_with_rel(model=model, data=spamreader, field=field, rel_field=rel_field, fields=fields)
            return Response(data={'error': er, 'error_rel': er_rel}, status=200)
        self.update_non_rel(model=model, data=spamreader, field=field)
        return Response(data={}, status=200)


class DuplicatesDataView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get(self, request):
        field = request.GET.get('field')
        obj = ContentType.objects.get(pk=request.GET.get('model'))
        model = apps.get_model(app_label=obj.app_label, model_name=obj.model)
        duplicates = model.objects.values(field).annotate(name_count=Count(field)).filter(name_count__gt=1)
        duplicates = [{'object': x[field], 'count': x['name_count']} for x in duplicates]
        return JsonResponse(data=duplicates, safe=False, status=200)


class MergeDataView(APIView):
    permission_classes = (IsAuthenticated, IsAdminUser)

    def post(self, request):
        data = request.data
        model = data.get('model')
        models_dict = data.get('models_dict')
        exclude_field = data.get('exclude_field')
        add_field = data.get('add_field')
        del data['add_field']
        del data['exclude_field']
        del data['model']
        del data['field']
        del data['models_dict']
        obj = ContentType.objects.get(pk=model)
        model = apps.get_model(app_label=obj.app_label, model_name=obj.model)
        main_model = model.objects.get(pk=data.get('id'))
        delete_models = []
        for i in models_dict:
            if i != data.get('id'):
                try:
                    delete_models.append(model.objects.get(pk=i))
                except:
                    return JsonResponse(data={'error': 'client not found'}, status=400)
        related = model._meta.related_objects
        all_fileds = model._meta.get_fields()
        valnames = list()
        rel_field_names = dict()
        rel_name_fields = dict()
        for r in related:
            rel_field_names.setdefault(r.field.model, r.field.name)
            rel_name_fields.setdefault(r.name, r.related_model)
            valnames.append(r.name)
        for i in delete_models:
            temp = models_dict[str(i.id)]['add']
            for j in temp:
                if rel_name_fields.get(j):
                    temp_model = rel_name_fields.get(j)
                    temp_model.objects.filter(**{rel_field_names[temp_model]: i.id}).\
                        update(**{rel_field_names[temp_model]: main_model.id})
            i.delete()
        if data.get('point') and data.get('point') != 'None':
            data['point'] = GEOSGeometry(data['point'])
        elif data.get('point') == 'None':
            data['point'] = None

        a = json.dumps(data).replace('true', 'True').replace('false', 'False')
        data = json.loads(a)

        model.objects.filter(id=main_model.id).update(**data)
        return JsonResponse(data={}, status=200)

    def check_related(self, qs_dict):
        for i in qs_dict:
            if type(qs_dict[i]) == QuerySet:
                qs_dict[i] = ', '.join([str(x) for x in qs_dict[i].values_list('id', flat=True)])
        return qs_dict

    def get(self, request):
        field = request.GET.get('field')
        obj = ContentType.objects.get(pk=request.GET.get('model'))
        val = request.GET.get('val')
        model = apps.get_model(app_label=obj.app_label, model_name=obj.model)
        related = model._meta.related_objects
        all_fileds = model._meta.get_fields()
        valnames = list()
        rel_field_names = dict()
        for r in related:
            rel_field_names.setdefault(r.field.model, r.field.name)
            valnames.append(r.name)
        qs = model.objects.filter(**{field: val})
        temp_list = []
        for i in qs:
            temp = model_to_dict(i)
            for j in all_fileds:
                if j.get_internal_type() == 'ForeignKey' and j.name not in valnames:
                    if getattr(i, j.name):
                        if j.related_model == User:
                            usr = getattr(i, j.name)
                            temp['user_name_%s' % (j.name,)] = '%s %s' % (usr.last_name, usr.first_name)
                            temp[j.name] = usr.id
                        else:
                            # temp[j.name] = model_to_dict(getattr(i, j.name))
                            # temp[j.name] = self.check_related(temp[j.name])
                            temp[j.name] = getattr(i, j.name).id
                    else:
                        temp[j.name] = None
                elif j.get_internal_type() == 'ForeignKey':
                    if hasattr(i, j.name):
                        temp[j.name] = list(getattr(i, j.name).all().values())
                    else:
                        temp[j.name] = list(getattr(i, '%s_set' % (j.name,)).all().values())
                elif j.get_internal_type() == 'ManyToManyField':
                    temp[j.name] = list(getattr(i, j.name).all().values())
                elif j.get_internal_type() == 'PointField':
                    temp[j.name] = str(getattr(i, j.name))
            temp_list.append(temp)
        return JsonResponse(data=list(temp_list), safe=False, status=200)
# Create your views here.



class ReductionFactorView(viewsets.ModelViewSet):
    queryset = ReductionFactor.objects.all()
    serializer_class = ReductionFactorSerializer
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)

