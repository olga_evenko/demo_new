from django.contrib import admin
from django.apps import apps
from .models import *


for model in apps.get_app_config('workers').models.values():
    admin.site.register(model)