from django.db import models
from django.contrib.auth.models import User
from projects.models import Project_type


class Manager(models.Model):
    user = models.OneToOneField(User, verbose_name='Пользователь', related_name='managers')
    head = models.ForeignKey(User, verbose_name='Руководитель', related_name='my_managers', blank=True, null=True)

    def __str__(self):
        user = self.user
        return '%s %s (%s)' % (user.last_name, user.first_name, user.username)
    # clients = models.ManyToManyField('clients.Client', verbose_name='Клиенты',
    #                                  related_name='manager_client', through='ClientManagerShip')
    # projects = models.ManyToManyField('projects.Project', verbose_name='Проекты')


# class ClientManagerShip(models.Model):
#     manager = models.ForeignKey(Manager)
#     client = models.OneToOneField('clients.Client')
#     date_created = models.DateTimeField(auto_now_add=True)
# test

class Membership(models.Model):
    project = models.ForeignKey('projects.Project')
    client = models.ForeignKey('clients.Client', related_name='membership')
    manager = models.ForeignKey(Manager, related_name='membership')

    class Meta:
        unique_together = (("project", "client", ),)


class Dialog(models.Model):
    user_one = models.ForeignKey(User, related_name='dialog_user_one')
    user_two = models.ForeignKey(User, related_name='dialog_user_two')

    class Meta:
        unique_together = (("user_one", "user_two"),)


class PushMessage(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    push = models.CharField(verbose_name='Сообщение', max_length=64)
    message = models.TextField()
    user = models.ForeignKey(User, verbose_name='Пользователь', related_name='push_message')
    show = models.BooleanField(default=False)
    sender = models.ForeignKey(User, verbose_name='Отправитель', related_name='sender_push_messages')
    dialog = models.ForeignKey(Dialog)


class FinPlanTypeOne(models.Model):
    head = models.ForeignKey(User, blank=True, null=True)
    month = models.DateField(verbose_name='Месяц')
    fin_plan = models.IntegerField(verbose_name='Финансовый план', blank=True, null=True)
    month_plan = models.IntegerField(verbose_name='Ежемесячный план', blank=True, null=True)
    project_type = models.ForeignKey(Project_type, verbose_name='Тип проекта', blank=True, null=True)


class ManagersPlanTypeOne(models.Model):
    fin_plan = models.ForeignKey(FinPlanTypeOne)
    manager = models.ForeignKey(Manager, related_name='manager_plan_type_one')
    month_plan = models.IntegerField(verbose_name='Ежемесячный план')
    plan_app_count = models.SmallIntegerField(verbose_name='План получения заявок')
    plan_processing_db = models.SmallIntegerField(verbose_name='План обработки БД')


class StartAlert(models.Model):
    date_created = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User)
    viewed = models.BooleanField()
    director = models.BooleanField()


class Statistic(models.Model):
    date = models.DateField(auto_now=True)
    count_clients = models.PositiveIntegerField()
    count_projects = models.PositiveIntegerField()
    count_contacts = models.PositiveIntegerField()
    count_users = models.PositiveSmallIntegerField()
    count_app = models.PositiveSmallIntegerField()


class Domain(models.Model):
    domain = models.CharField(max_length=256)

    def __str__(self):
        return self.domain


class UserDomainPermission(models.Model):
    user = models.ForeignKey(User)
    domain = models.ForeignKey(Domain)

    def __str__(self):
        return '%s (%s)' % (self.user.username, self.domain)


class Notification(models.Model):
    user = models.ForeignKey(User)
    date_created = models.DateTimeField(auto_now_add=True)
    message = models.TextField(blank=True)
    show = models.BooleanField(default=False)


class UpdateCache(models.Model):
    user = models.ForeignKey(User)
    clear = models.BooleanField(default=False)

class ReductionFactor(models.Model):
    name = models.CharField(max_length=20, null=True, blank=True, verbose_name='Наименование')
    min_value = models.FloatField(default=0, verbose_name='Минимальное значение', null=True, blank=True)
    max_value = models.FloatField(default=0, verbose_name='Максимальное значение', null=True, blank=True)
    m1 = models.FloatField(default=0, verbose_name='µ инд (М1)')
    m2 = models.FloatField(default=0, verbose_name='µ груп (М2)')

    def __str__(self):
        return self.name

# Create your models here.
