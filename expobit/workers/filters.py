import django_filters
from clients.models import Client
from django.db.models import Q
from rest_framework import filters
import re



class ClientFilter(django_filters.FilterSet):
    contact__project = django_filters.NumberFilter(name='contact__project')
    membership__project = django_filters.NumberFilter(name='membership__project')
    # membership__manager = django_filters.NumberFilter(name='membership__manager')
    membership__manager = django_filters.Filter(name='membership__manager', method='get_manager')
    contact__failure_bool = django_filters.BooleanFilter(name='contact__failure_bool')
    contact__interest = django_filters.BooleanFilter(name='contact__interest')
    contact__app = django_filters.BooleanFilter(name='contact__app')
    status = django_filters.CharFilter(name='status')
    status_exclude = django_filters.CharFilter(name='status', exclude=True)
    no_app = django_filters.BooleanFilter(name='applicationexpo', lookup_expr='isnull')
    app__status = django_filters.CharFilter(name='applicationexpo__status')
    client_activitys = django_filters.Filter(name='client_activitys')
    entitys__activities = django_filters.Filter(name='entitys__activities')
    # client_group__group = django_filters.Filter(name='client_group__group')
    client_group__subjects = django_filters.Filter(name='client_group__subjects', method='get_group')
    app__isnull = django_filters.Filter(name='applicationexpo', method='get_appisnull')
    client_change_db = django_filters.Filter(name='client_group__subjects', method='get_client_change_db')
    result_last_contact = django_filters.Filter(name='result_last_contact')
    date_last_contact_proj = django_filters.Filter(name='date_last_contact_proj')
    # exc_client_group__group = django_filters.Filter(name='client_group__group', exclude=True)
    # client_group__subjects__projects = django_filters.Filter(name='client_group__subjects__projects')

    def get_appisnull(self, queryset, name, value):
        filters = Q()
        if self.request.GET.get('membership__project'):
            filters |= Q(applicationexpo__project=self.request.GET.get('membership__project'))
        if self.request.GET.get('membership__manager'):
            filters |= Q(applicationexpo__manager=self.request.GET.get('membership__manager'))
        return queryset.filter(filters, applicationexpo__isnull=int(value))

    def get_group(self, queryset, name, value):
        return queryset.filter(Q(client_group__group=self.request.GET.get('client_group__group')) &
                               Q(client_group__subjects=value))

    def get_manager(self, queryset, name, value):
        return queryset.filter(Q(membership__project=self.request.GET.get('membership__project')) &
                               Q(membership__manager=value))

    def get_client_change_db(self, queryset, name, value):
        filter_set = Q(created_user__managers__in=self.request.GET.get('created_user__managers__in').split(','))
        if self.request.GET.get('date_created__gt'):
            filter_set &= Q(date_created__gt=self.request.GET.get('date_created__gt'))
        if self.request.GET.get('date_created__lt'):
            filter_set &= Q(date_created__gt=self.request.GET.get('date_created__lt'))
        return queryset.filter(filter_set)

    class Meta:
        model = Client
        fields = ['contact__project', 'membership__project', 'contact__failure_bool', 'status', 'status_exclude']


class ClientFilterTypeTwo(django_filters.FilterSet):
    visitor_manager__isnull = django_filters.BooleanFilter(name='visitor_manager', lookup_expr='isnull')
    contact__project = django_filters.Filter(name='contact__project')
    visitor_manager = django_filters.Filter(name='visitor_manager')
    status = django_filters.Filter(name='status')
    city__in = django_filters.Filter(name='city', lookup_expr='in', method='get_city_in')
    applicationad__status = django_filters.Filter(name='applicationad__status')
    contact__failure_bool = django_filters.BooleanFilter(name='contact__failure_bool')
    contact__interest = django_filters.BooleanFilter(name='contact__interest')
    contact__interest__not__app = django_filters.Filter(name='contact__interest', method='get_contact_interest')
    contact__not__app = django_filters.Filter(name='contact__app', method='get_contact_app')
    contact__project = django_filters.Filter(name='contact__project')
    client_activitys__in = django_filters.Filter(name='client_activitys', lookup_expr='in',
                                                 method='get_client_activitys_in')
    city__region__in = django_filters.Filter(name='city__region', lookup_expr='in',
                                                 method='get_city__region_in')

    def get_city__region_in(self, queryset, name, value):
        return queryset.filter(city__region__in=value.split(','))

    def get_client_activitys_in(self, queryset, name, value):
        return queryset.filter(client_activitys__in=value.split(','))

    def get_city_in(self, queryset, name, value):
        return queryset.filter(city__in=value.split(','))

    def get_contact_interest(self, queryset, name, value):
        return queryset.filter(Q(contact__interest=True) & Q(contact__app=False) & Q(contact__project=self.request.GET.get('project')))

    def get_contact_app(self, queryset, name, value):
        project = self.request.GET.get('project')
        return queryset.filter(Q(contact__interest=True) & Q(contact__app=False) & Q(contact__project=self.request.GET.get('project'))).exclude(
            id__in=list(Client.objects.filter(applicationad__project=project).values_list('id', flat=True)))

    class Meta:
        model = Client
        fields = ['visitor_manager__isnull', 'visitor_manager', 'status',
                  'applicationad__status', 'contact__failure_bool', 'contact__interest',
                  'contact__not__app', 'contact__project']


class ClientFilterTypeOne(django_filters.FilterSet):
    personal_manager_type_one = django_filters.Filter(name='personal_manager_type_one')
    personal_manager_type_one__isnull = django_filters.Filter(name='personal_manager_type_one', lookup_expr='isnull')
    status = django_filters.Filter(name='status')
    do_contact = django_filters.Filter(name='do_contact', method='get_do_contact')

    def get_do_contact(self, queryset, name, value):
        manager = self.request.GET.get('personal_manager_type_one', self.request.user.managers.id)
        return queryset.filter(Q(contact__manager=manager))


class ClientSearchFilter(filters.SearchFilter):

    #def get_search_terms(self, request):

        # if '`' in request.query_params.get(self.search_param, ''):
        #     params = request.query_params.get(self.search_param, '')
        #     return params.replace('`', '')
        # params = request.query_params.get(self.search_param, '')
        # return params.replace(',', ' ').split()

    def filter_queryset(self, request, queryset, view):
        if re.match(r'%.+%', request.query_params.get('search', '')):
            return queryset.filter(name=request.query_params.get('search').replace('%', ''))
        return super(ClientSearchFilter, self).filter_queryset(request, queryset, view)

    # def get_search_fields(self, view, request):
    #     if re.match(r'`.+`', request.query_params.get('search')):
    #         return ('^name',)
    #     return super(ClientSearchFilter, self).get_search_fields(view, request)