from django.conf.urls import url, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'managers', ApiSetManagersView, 'Managers')
router.register(r'content_type', ApiSetContentTypeView, 'ContentType')
router.register(r'messages', ApiSetPushMessageView, 'ApiSetPushMessageView')
router.register(r'notifications', ApiSetNotificationsView, 'ApiSetNotificationsView')
router.register(r'users', ApiSetUserView, 'ApiSetUserView')
router.register(r'plans_manager', ApiSetManagerPlanView, 'ApiSetManagerPlanView')
router.register(r'full_report_dvp', ApiDvpReportView, 'ApiDvpReportView')
# router.register(r'report_by_project', ApiReportByProjectView, 'ApiReportByProjectView')
router.register(r'clients', ApiSetClientsManagerView, 'ApiSetClientsManagerView')
router.register(r'entity_private_merge', ApiSetEntityMergeView, 'ApiSetEntityMergeView')
router.register(r'start_alert', ApiSetStartAlertView, 'ApiSetStartAlertView')
router.register(r'clients_type_1', ApiSetClientsTypeOneView, 'ApiSetClientsTypeOneView')
router.register(r'clients_type_2', ApiSetClientsTypeTwoView, 'ApiSetClientsTypeTwoView')
router.register(r'plan_type_1', ApiSetFinPlanTypeOneView, 'ApiSetFinPlanTypeOneView')
router.register(r'plan_managers_type_1', ApiSetManagersPlanTypeOneView, 'ApiSetClientsManagerView')
router.register(r'plan_managers_type_2', ApiSetManagersPlanTypeTwoView, 'ApiSetClientsManagerView')
router.register(r'projects', ApiSetProjectsView, 'ApiSetProjectsView')
router.register(r'report_contact', ApiSetManagersReportContactView, 'ApiSetManagersReportContactView')
# router.register(r'report_contact_by_group', ApiSetManagersReportContactByGroupView, 'ApiSetManagersReportContactByGroupView')
# router.register(r'report_by_week', ApiSetReportByWeekView.as_view(), 'ApiSetReportByWeekView')
router.register(r'report_contact_type_1', ApiSetManagerContactReportTypeOne, 'ApiSetManagerContactReportTypeOne')
router.register(r'report_contact_type_2', ApiSetManagerContactReportTypeTwo, 'ApiSetManagerContactReportTypeTwo')
router.register(r'report_contact_individual_type_2', ApiSetManagerContactReportIndividualTypeTwo,
                'ApiSetManagerContactReportIndividualTypeTwo')
router.register(r'report_contact_individual_soi', ApiSetManagerContactReportIndividualSOI,
                'ApiSetManagerContactReportIndividualTypeTwo')
router.register(r'report_contact_client', ApiSetClientsReportContactView, 'ApiSetClientsReportContactView')
router.register(r'report_updated_client', ApiSetClientsReportUpdatedView, 'ApiSetClientsReportUpdatedView')
router.register(r'report_updated_client_type_1', ApiSetClientsReportUpdatedTypeOneView,
                'ApiSetClientsReportUpdatedTypeOneView')
router.register(r'reduction_factor', ReductionFactorView, 'ReductionFactor')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'send_noty', NotificationView.as_view(), name='NotificationView'),
    url(r'duplicates_data', DuplicatesDataView.as_view(), name='DuplicatesDataView'),
    url(r'merge_data', MergeDataView.as_view(), name='MergeDataView'),
    url(r'report_contact_by_group', ApiSetManagersReportContactByGroupView.as_view(),
        name='ApiSetManagersReportContactByGroupView'),
    url(r'report_by_week', ApiSetReportByWeekView.as_view(), name='ApiSetReportByWeekView'),
    url(r'report_by_project', ApiReportByProjectView.as_view(), name='ApiReportByProjectView'),
    url(r'user_control', UserControlView.as_view(), name='UserControlView'),
    url(r'update_cache', UpdateCacheView.as_view(), name='UpdateCacheView'),
    url(r'load_csv', LoadCsvView.as_view(), name='LoadCsvView')
]

# from .models import UpdateCache
# from django.contrib.auth.models import User
#
#
# def update_cache():
#     users = User.objects.all().values('id')
#     for i in users:
#         UpdateCache.objects.update_or_create(user_id=i['id'], clear=False)
#
# update_cache()
