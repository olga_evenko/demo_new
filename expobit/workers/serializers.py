from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User
from restExt.serializers import CustomDateTimeField
from projects.models import ProjectReportManager, Activities, ProjectReportTypeTwoManager
from clients.models import ApplicationAd, ApplicationLeaseKVS, ApplicationExpo, Contact, NewContact
import datetime
from clients.models import Client, ClientGroup, Individual, IndividualContact, Entity
from projects.models import Project
from django.utils import timezone
from projects.views import MONTH_NAME
from django.db.models import Q, Count
import time
from dateutil import parser
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import ContentType
from django.apps import apps


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username']


class ManagersSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Manager
        fields = "__all__"


class MemberShipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membership
        fields = "__all__"


# class PushMessageSerializer(serializers.ModelSerializer):
#     date_created = CustomDateTimeField()
#     sender = serializers.SerializerMethodField()
#
#     def get_sender(self, obj):
#         return '%s %s' % (obj.sender.last_name, obj.sender.first_name)
#
#     class Meta:
#         model = PushMessage
#         fields = "__all__"
# TODO переделать под диалоги 11.05.2018 начало
class PushMessageSerializer(serializers.ModelSerializer):
    date_created = CustomDateTimeField()
    sender = serializers.SerializerMethodField()
    sender_id = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()

    def get_user_name(self, obj):
        return '%s %s' % (obj.user.last_name, obj.user.first_name)

    def get_sender_id(self, obj):
        return obj.sender_id

    def get_sender(self, obj):
        # User.objects.get()
        return '%s %s' % (obj.sender.last_name, obj.sender.first_name)

    class Meta:
        model = PushMessage
        fields = "__all__"


class ManagerPlanSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField()
    project_id = serializers.SerializerMethodField()
    project_subject = serializers.SerializerMethodField()
    fact_app = serializers.SerializerMethodField()
    perc_comp = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    perc_comp_db = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.project_report.project_indicators.project_id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        key = str(obj.manager_id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        return 0

    def get_perc(self, report):
        if report.fin_plan_month:
            return str(round((self.__dict__['fact'] / report.fin_plan_month) * 100)) + ' %'
        return None

    def get_perc_comp_db(self, obj):
        return str(round(self.percent_db, 2)) + ' %'

    def get_fact_processing_db(self, obj):
        project_id = obj.project_report.project_indicators.project_id
        manager_id = obj.manager_id
        qs_client = Membership.objects.filter(project=project_id,
                                              manager=manager_id).values('client_id')
        clients_id = [x['client_id'] for x in qs_client]
        date = obj.project_report.month_date
        if date.month == 12:
            date_end = datetime.date(date.year + 1, 1, date.day)
        else:
            date_end = datetime.date(date.year, date.month + 1, date.day)
        s = 0
        # for i in qs_client:
        #     qs = Contact.objects.filter(project=project_id,
        #                                 manager=manager_id, type_contact='out_phone',
        #                                 client=i.client_id, date__gt=date, date__lt=date_end)
        #     if qs:
        #         s += 1
        # qs = Contact.objects.filter(project=project_id,
        #                             manager=obj.manager_id, type_contact='out_phone',
        #                             client__in=clients_id, date__gt=date,
        #                             date__lt=date_end).values('client_id').distinct()

        qs_old = Contact.objects.filter(type_contact='out_phone',
                                        client__in=clients_id, project=project_id,
                                        manager=obj.manager_id,
                                        date__lt=date).values_list('client_id', flat=True).distinct()
        qs = Contact.objects.filter(type_contact='out_phone',
                                    manager=obj.manager_id,
                                    client__in=clients_id, project=project_id,
                                    date__gt=date, date__lt=date_end).values_list('client_id', flat=True).distinct()
        s = len(set(qs) - set(qs_old))
        self.__dict__['percent_db'] = 0
        # s = len(qs)
        if obj.plan_processing_db:
            self.percent_db = (s / obj.plan_processing_db) * 100
        return s

    def get_perc_comp(self, obj):
        try:
            return str(round((self.s / obj.app_plan_month) * 100, 2)) + ' %'
        except:
            return None

    def get_fact_app(self, obj):
        date = obj.project_report.month_date
        # if date.month == 12:
        #     date_end = datetime.date(date.year + 1, 1, date.day)
        # else:
        #     date_end = datetime.date(date.year, date.month + 1, date.day)
        date_end = date + relativedelta(months=1)
        project_id = obj.project_report.project_indicators.project_id
        manager_id = obj.manager_id
        app_expo = ApplicationExpo.objects.filter(date__gt=date, date__lt=date_end,
                                                  project=project_id,
                                                  manager=manager_id, main__isnull=True).exclude(status='canceled').count()
        app_kvs = ApplicationLeaseKVS.objects.filter(date__gt=date, date__lt=date_end,
                                                     project=project_id,
                                                     manager=manager_id, main__isnull=True).exclude(status='canceled').count()
        app_ad = ApplicationAd.objects.filter(date__gt=date, date__lt=date_end,
                                              project=project_id,
                                              manager=manager_id, main__isnull=True).exclude(status='canceled').count()
        self.__dict__['s'] = 0
        self.s = app_expo + app_ad + app_kvs
        return app_expo + app_ad + app_kvs

    def get_manager(self, obj):
        self.__dict__['count_client_manager'] = 0
        return {'manager': '%s %s' % (obj.manager.user.last_name, obj.manager.user.first_name),
                'id': obj.manager.id}

    def get_project(self, obj):
        project = obj.project_report.project_indicators.project
        self.__dict__['project_id'] = project.id
        return project.name

    def get_project_id(self, obj):
        return self.__dict__['project_id']

    def get_project_subject(self, obj):
        if obj.project_report.project_indicators.project.subjects:
            return obj.project_report.project_indicators.project.subjects.id
        return ''

    class Meta:
        model = ProjectReportManager
        fields = "__all__"


class ManagerPlanTypeTwoSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    perc_proccessing_db = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = obj.project_report.project_indicators.project_id
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        key = str(obj.manager_id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        return 0

    def get_perc(self, report):
        if report.fin_plan_month:
            return str(round((self.__dict__['fact'] / report.fin_plan_month) * 100)) + ' %'
        return None

    def get_project(self, obj):
        start = time.time()
        name = Project.objects.get(project_indicators_type_two__project_report__project_report_manager=obj.id).name
        # name = obj.project_report.project_indicators.project.name
        return name

    def get_fact_processing_db(self, obj):
        # start = time.time()
        month = obj.project_report.month_date.month
        manager = obj.manager_id
        qs_clients = Client.objects.filter(visitor_manager=manager).values_list('id')
        project = obj.project_report.project_indicators.project_id
        clients_id = [x[0] for x in qs_clients]
        qs = Contact.objects.filter(client__in=clients_id, date__month=month, type_contact='out_phone',
                                    project=project,
                                    manager=manager)
        count = 0
        for i in qs:
            c = i.client_id
            if c in clients_id:
                count += 1
                clients_id.remove(c)
        # Еще дописать проверку на Физ лица
        qs_individuals = Individual.objects.filter(visitor_manager=manager).values_list('id')
        individuals_id = [x[0] for x in qs_individuals]
        qs_ind = IndividualContact.objects.filter(individual__in=individuals_id, date__month=month,
                                                  type_contact='out_phone', project=project, manager=manager)
        for i in qs_ind:
            c = i.individual_id
            if c in individuals_id:
                count += 1
                individuals_id.remove(c)
        self.__dict__['count'] = count
        return count

    def get_perc_proccessing_db(self, obj):
        return str(round((self.count / obj.plan_processing_db) * 100, 2)) + ' %'

    class Meta:
        model = ProjectReportTypeTwoManager
        fields = '__all__'


class ClientsForManagerSerializer(serializers.ModelSerializer):
    group = serializers.SerializerMethodField()
    # date_last_contact = serializers.SerializerMethodField()
    new_contact = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()
    result_last_contact = serializers.CharField()
    date_last_contact_proj = serializers.DateTimeField()

    def get_new_contact(self, obj):
        # qs = NewContact.objects.filter(project_id=self.context['project'], client_id=obj.id,
        #                                date_new_contact__gte=datetime.datetime.now()).order_by('-id')\
        #     .values('date_new_contact', 'goal_contact')
        # qs = NewContact.objects.filter(project_id=self.context['project'], client_id=obj.id).order_by('-id') \
        #     .values('date_new_contact', 'goal_contact')
        # qs = qs[:1]
        # if qs:
        #     return {'date': qs[0]['date_new_contact'], 'goal': qs[0]['goal_contact']}
        qs = obj.menu
        if qs:
            return {'date': qs[0].date_new_contact, 'goal': qs[0].goal_contact}
        return {'date': None, 'goal': None}

    # def get_date_last_contact(self, obj):
    #     # qs = Contact.objects.filter(client_id=obj.id, project_id=self.context['project']).order_by('-date').\
    #     #     values('result', 'date')
    #     # qs = qs[:1]
    #     # if qs:
    #     #     tz = timezone.get_default_timezone()
    #     #     return {'result': qs[0]['result'],
    #     #             'date': timezone.localtime(qs[0]['date'], timezone=tz).strftime("%d-%m-%Y %H:%M")}
    #     qs = obj.last_contact
    #     if qs:
    #         tz = timezone.get_default_timezone()
    #         return {'result': qs[0].result,
    #                 'date': timezone.localtime(qs[0].date, timezone=tz).strftime("%d-%m-%Y %H:%M")}
    #     else:
    #         return {'result': None, 'date': None}

    def get_group(self, obj):

        # project = Project.objects.filter(id=self.context['project']).values('subjects')[0]
        # qs_group = ClientGroup.objects.filter(client_id=obj.id, subjects_id=project['subjects'])
        # qs_group = qs_group[:1]
        qs_group = obj.group
        if qs_group:
            return qs_group[0].get_group_display()
        else:
            return None

    def get_manager(self, obj):
        manager = User.objects.filter(managers__membership__client_id=obj.id,
                                      managers__membership__project_id=self.context['project']).values('last_name',
                                                                                                       'first_name')[:1]
        if manager:
            return '%s %s' % (manager[0]['last_name'], manager[0]['first_name'])
        return None

    class Meta:
        model = Client
        fields = "__all__"


class ClientsForAllProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'name', 'status', 'address']


# TODO Переделал алгоритм построения отчета, проверять надо, если что откатиться ManagersReportContactSerializer
class ManagersReportContactSerializer(serializers.ModelSerializer):
    count_contact = serializers.SerializerMethodField()
    in_contact = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    result = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()

    # out_contact = serializers.SerializerMethodField()
    # count_meet = serializers.SerializerMethodField()
    # count_emails = serializers.SerializerMethodField()
    # in_work = serializers.SerializerMethodField()
    # interest = serializers.SerializerMethodField()
    # app = serializers.SerializerMethodField()
    # failure = serializers.SerializerMethodField()
    # interest_on_other_project = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        one_c_data = self.context.get('one_c_context')
        key = str(obj.id)
        if key in one_c_data:
            self.__dict__['fact'] = sum(one_c_data[key])
            return sum(one_c_data[key])
        return 0

    def get_name(self, obj):
        return '%s %s' % (obj.user.first_name, obj.user.last_name)

    def get_count_contact(self, obj):
        qs_client = Client.objects.filter(membership__manager_id=obj.id,
                                          membership__project_id__in=self.context['project']).values_list('id').distinct()
        filter_field = {
            'manager': obj,
            'project_id__in': self.context['project'],
            'client__in': qs_client
        }
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        self.__dict__['count'] = Contact.objects.filter(**filter_field).count()
        return self.__dict__['count']

    def get_in_contact(self, obj):
        qs_client = Client.objects.filter(membership__manager_id=obj.id,
                                          membership__project_id__in=self.context['project']).values('id').distinct()
        clients_id = [x['id'] for x in qs_client]
        # qs_client = obj.membership.filter(project=self.context['project']).distinct()
        result = {'first_in': 0, 'repeat_in': 0, 'first_out': 0, 'repeat_out': 0,
                  'meet': 0, 'email': 0, 'interest': 0, 'fail': 0, 'app': 0}
        filter_field = {
            'manager': obj.id,
            'project_id__in': self.context['project']
        }
        filter_field_inversion = {
            'manager': obj.id,
            'project_id__in': self.context['project']
        }
        check_old_data =False
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
            filter_field_inversion['date__date__lt'] = parser.parse(self.context['date_gt'])
            check_old_data = True
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])

        # if self.context['date_lt'] != '':
        #     filter_field_inversion['date__gt'] = parser.parse(self.context['date_lt'])
        # for i in qs_client:
        #     filter_field['client'] = i
        temp = Contact.objects.filter(**filter_field, client__in=clients_id).values('type_contact',
                                                                                    'interest',
                                                                                    'failure_bool',
                                                                                    'app',
                                                                                    'client')
        temp_all_project_phone_in = []
        temp_all_project_phone_out = []
        if check_old_data:
            temp_all_project_phone_in = list(Contact.objects.filter(**filter_field_inversion, client__in=clients_id,
                                                                    type_contact='1'). \
                                             values_list('client', flat=True).distinct('client'))
            temp_all_project_phone_out = list(Contact.objects.filter(**filter_field_inversion, client__in=clients_id,
                                                                     type_contact='out_phone'). \
                                              values_list('client', flat=True).distinct('client'))
        # processed_client_phone_in = []
        # processed_client_phone_out = []
        # phone_in_flag = False
        # phone_out_flag = False
        for j in temp:
            if j['type_contact'] == 'in_phone' and not j['client'] in temp_all_project_phone_in:
                result['first_in'] += 1
                temp_all_project_phone_in.append(j['client'])
                # phone_in_flag = True
            elif j['type_contact'] == 'in_phone':
                result['repeat_in'] += 1
            if j['type_contact'] == 'out_phone' and not j['client'] in temp_all_project_phone_out:
                result['first_out'] += 1
                temp_all_project_phone_out.append(j['client'])
                # phone_out_flag = True
            elif j['type_contact'] == 'out_phone':
                result['repeat_out'] += 1
            if j['type_contact'] == 'meet':
                result['meet'] += 1
            if j['type_contact'] == 'email':
                result['email'] += 1
                # if j['interest']:
                #     result['interest'] += 1
                # if j['failure_bool']:
                #     result['fail'] += 1
                # if j['app']:
                #     result['app'] += 1
                # result['work'] = self.__dict__['count'] - result['fail']
        return result

    def get_result(self, obj):
        qs_client = Client.objects.filter(membership__manager_id=obj.id,
                                          membership__project_id__in=self.context['project']).values('id').distinct()
        clients_id = [x['id'] for x in qs_client]
        result = {'interest': 0, 'fail': 0, 'app': 0, 'work': 0, 'interest_other': 0}
        filter_field = {
            'manager_id': obj.id,
            'project_id__in': self.context['project']
        }
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        # for i in qs_client:
        #     filter_field['client'] = i['id']
        qs = Contact.objects.filter(**filter_field, client__in=clients_id).order_by('-date')
        for i in qs:
            if qs.exists():
                if i.failure_bool:
                    result['fail'] += 1
                elif i.app:
                    result['app'] += 1
                elif i.interest:
                    result['interest'] += 1
                elif i.interest_other:
                    result['interest_other'] += 1
                else:
                    result['work'] += 1
        result['work'] += result['app'] + result['interest'] + result['interest_other']
        return result

    class Meta:
        model = Manager
        fields = "__all__"


# TODO Старая версия сериалайзера ReportWorkGroupClientsSerializer, новую тестировать
# class ReportWorkGroupClientsSerializer(serializers.ModelSerializer):
#     manager = serializers.SerializerMethodField()
#     all_clients = serializers.SerializerMethodField()
#     groups = serializers.SerializerMethodField()
#
#     def get_manager(self, obj):
#         return '%s %s' % (obj.user.last_name, obj.user.first_name)
#
#     def check_contact(self, obj):
#         if self.context['date_lt'] and not self.context['date_gt']:
#             if obj.date.replace(tzinfo=None) < datetime.datetime.strptime(self.context['date_lt'], '%Y-%m-%d'):
#                 return True
#             else:
#                 return False
#         elif self.context['date_gt'] and not self.context['date_lt']:
#             if obj.date.replace(tzinfo=None) > datetime.datetime.strptime(self.context['date_gt'], '%Y-%m-%d'):
#                 return True
#             else:
#                 return False
#         else:
#             if datetime.datetime.strptime(self.context['date_gt'], '%Y-%m-%d') < obj.date.replace(tzinfo=None) < \
#                     datetime.datetime.strptime(self.context['date_lt'], '%Y-%m-%d'):
#                 return True
#             else:
#                 return False
#
#     def get_all_clients(self, obj):
#         first_in = 0
#         all_client_count = obj.membership.filter(project=self.context['project']).count()
#         qs = Client.objects.filter(membership__manager=obj, membership__project=self.context['project'])
#         self.__dict__['cache'] = {'clients': qs}
#         for i in qs:
#             contact_qs = Contact.objects.filter(client=i, manager=obj,
#                                                 project=self.context['project'],
#                                                 type_contact='out_phone').order_by('date')[:1]
#             if contact_qs:
#                 if self.context['date_gt'] or self.context['date_lt']:
#                     if self.check_contact(contact_qs[0]):
#                         first_in += 1
#                 else:
#                     first_in += 1
#         return {'all': all_client_count, 'first_in': first_in}
#
#     def get_groups(self, obj):
#         # qs = Client.objects.filter(membership__manager=obj,
#         #                            membership__project=self.context['project']).values_list('id')
#         qs = self.cache['clients']
#         qs_group = ClientGroup.objects.filter(client__in=qs, subjects__projects=self.context['project'])
#         result = {'vip': 0, 'a': 0, 'b': 0, 'c': 0, 'vip_in': 0, 'a_in': 0, 'b_in': 0, 'c_in': 0}
#         for i in qs_group:
#             if i.group == 'vip':
#                 result['vip'] += 1
#                 contact_qs = Contact.objects.filter(client=i.client, manager=obj,
#                                                     project=self.context['project'],
#                                                     type_contact='out_phone').order_by('date')[:1]
#                 if contact_qs:
#                     if self.context['date_gt'] or self.context['date_lt']:
#                         if self.check_contact(contact_qs[0]):
#                             result['vip_in'] += 1
#                     else:
#                         result['vip_in'] += 1
#             elif i.group == 'a':
#                 result['a'] += 1
#                 contact_qs = Contact.objects.filter(client=i.client, manager=obj,
#                                                     project=self.context['project'],
#                                                     type_contact='out_phone').order_by('date')[:1]
#                 if contact_qs:
#                     if self.context['date_gt'] or self.context['date_lt']:
#                         if self.check_contact(contact_qs[0]):
#                             result['a_in'] += 1
#                     else:
#                         result['a_in'] += 1
#             elif i.group == 'b':
#                 result['b'] += 1
#                 contact_qs = Contact.objects.filter(client=i.client, manager=obj,
#                                                     project=self.context['project'],
#                                                     type_contact='out_phone').order_by('date')[:1]
#                 if contact_qs:
#                     if self.context['date_gt'] or self.context['date_lt']:
#                         if self.check_contact(contact_qs[0]):
#                             result['b_in'] += 1
#                     else:
#                         result['b_in'] += 1
#             elif i.group == 'c':
#                 result['c'] += 1
#                 contact_qs = Contact.objects.filter(client=i.client, manager=obj,
#                                                     project=self.context['project'],
#                                                     type_contact='out_phone').order_by('date')[:1]
#                 if contact_qs:
#                     if self.context['date_gt'] or self.context['date_lt']:
#                         if self.check_contact(contact_qs[0]):
#                             result['c_in'] += 1
#                     else:
#                         result['c_in'] += 1
#         return result
#
#     class Meta:
#         model = Manager
#         fields = "__all__"



class ReportWorkGroupClientsSerializer(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField()
    all_clients = serializers.SerializerMethodField()
    groups = serializers.SerializerMethodField()
    not_groups = serializers.SerializerMethodField()

    def get_manager(self, obj):
        return '%s %s' % (obj.user.last_name, obj.user.first_name)

    def check_contact(self, obj):
        if self.context['date_lt'] and not self.context['date_gt']:
            if obj.date.replace(tzinfo=None) < datetime.datetime.strptime(self.context['date_lt'], '%Y-%m-%d'):
                return True
            else:
                return False
        elif self.context['date_gt'] and not self.context['date_lt']:
            if obj.date.replace(tzinfo=None) > datetime.datetime.strptime(self.context['date_gt'], '%Y-%m-%d'):
                return True
            else:
                return False
        else:
            if datetime.datetime.strptime(self.context['date_gt'], '%Y-%m-%d') < obj.date.replace(tzinfo=None) < \
                    datetime.datetime.strptime(self.context['date_lt'], '%Y-%m-%d'):
                return True
            else:
                return False

    def get_all_clients(self, obj):
        first_in = 0
        all_client_count = obj.membership.filter(project_id__in=self.context['project']).count()
        qs = Client.objects.filter(membership__manager_id=obj.id,
                                   membership__project_id__in=self.context['project']).exclude(
            status__in=['black_list', 'inactive']).values('id')
        clients_id = [x['id'] for x in qs]
        self.__dict__['cache'] = {'clients': clients_id}
        filter_fields = {
            'client__in': clients_id,
            'manager_id': obj.id,
            'project_id__in': self.context['project'],
            'type_contact': 'out_phone'
        }
        if self.context['date_gt']:
            filter_fields['date__date__gte'] = self.context['date_gt']
        if self.context['date_lt']:
            filter_fields['date__date__lte'] = self.context['date_lt']
        first_in = Contact.objects.filter(**filter_fields).values('client').distinct().count()
        return {'all': all_client_count, 'first_in': first_in}

    def get_groups(self, obj):
        qs = self.cache['clients']
        filter_fields = {}
        client_filter_fields = {}
        exclude_filter_fields = {}
        flag = False
        if self.context['date_gt']:
            filter_fields['date__date__gte'] = self.context['date_gt']
            exclude_filter_fields['date__date__lt'] = self.context['date_gt']
            flag = True
            client_filter_fields['contact__date__date__gte'] = self.context['date_gt']
        if self.context['date_lt']:
            filter_fields['date__date__lte'] = self.context['date_lt']
            client_filter_fields['contact__date__date__lte'] = self.context['date_lt']
        vip_in = Contact.objects.filter(Q(client__client_group__group='vip') & Q(client__client_group__subjects__projects__in=self.context['project']), **{**{'client__in': qs, 'manager_id': obj.id,
                                              'project_id__in': self.context['project'],
                                              'type_contact': 'out_phone',
                                              **filter_fields}}).values_list(
            'client', flat=True).distinct()
        a_in = Contact.objects.filter(Q(client__client_group__group='a') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                              'project_id__in': self.context['project'],
                                              'type_contact': 'out_phone',
                                              **filter_fields}}).values_list(
            'client', flat=True).distinct()
        b_in = Contact.objects.filter(Q(client__client_group__group='b') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                              'project_id__in': self.context['project'],
                                              'type_contact': 'out_phone',
                                              **filter_fields}}).values_list(
            'client', flat=True).distinct()
        c_in = Contact.objects.filter(Q(client__client_group__group='c') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                              'project_id__in': self.context['project'],
                                              'type_contact': 'out_phone',
                                              **filter_fields}}).values_list(
            'client', flat=True).distinct()
        if flag:
            vip_in = set(vip_in)
            a_in = set(a_in)
            b_in = set(b_in)
            c_in = set(c_in)
            vip_all = set(Contact.objects.filter(Q(client__client_group__group='vip') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                                       'project_id__in': self.context['project'],
                                                       'type_contact': 'out_phone',
                                                       **exclude_filter_fields}}).values_list(
                'client', flat=True).distinct())
            a_all = set(Contact.objects.filter(Q(client__client_group__group='a') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                                       'project_id__in': self.context['project'],
                                                       'type_contact': 'out_phone',
                                                       **exclude_filter_fields}}).values_list(
                'client', flat=True).distinct())
            b_all = set(Contact.objects.filter(Q(client__client_group__group='b') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                                       'project_id__in': self.context['project'],
                                                       'type_contact': 'out_phone',
                                                       **exclude_filter_fields}}).values_list(
                'client', flat=True).distinct())
            c_all = set(Contact.objects.filter(Q(client__client_group__group='c') & Q(client__client_group__subjects__projects__in=self.context['project']),**{**{'client__in': qs, 'manager_id': obj.id,
                                                       'project_id__in': self.context['project'],
                                                       'type_contact': 'out_phone',
                                                       'client__client_group__group': 'c',
                                                       **exclude_filter_fields}}).values_list(
                'client', flat=True).distinct())

            vip_count = len(vip_in-vip_all)
            a_count = len(a_in-a_all)
            b_count = len(b_in-b_all)
            c_count = len(c_in - c_all)
        else:
            vip_count = vip_in.count()
            a_count = a_in.count()
            b_count = b_in.count()
            c_count = c_in.count()
        result = {'vip': Client.objects.filter(Q(client_group__group='vip') & Q(client_group__subjects__projects__in=self.context['project']),**{**{'id__in': qs,
                                                     },
                                                  **client_filter_fields}).distinct().count(),
                  'a': Client.objects.filter(Q(client_group__group='a') & Q(client_group__subjects__projects__in=self.context['project']),**{**{'id__in': qs,
                                                  },
                                                **client_filter_fields}).distinct().count(),
                  'b': Client.objects.filter(Q(client_group__group='b') & Q(client_group__subjects__projects__in=self.context['project']),**{**{'id__in': qs,
                                                  },
                                                **client_filter_fields}).distinct().count(),
                  'c': Client.objects.filter(Q(client_group__group='c') & Q(client_group__subjects__projects__in=self.context['project']),**{**{'id__in': qs,
                                                   },
                                                **client_filter_fields}).count(),
                  'vip_in': vip_count,
                  'a_in': a_count,
                  'b_in': b_count,
                  'c_in': c_count}

        # for i in qs_group:
        #     if i.group == 'vip':
        #         result['vip'] += 1
        #         contact_qs = Contact.objects.filter(client=i.client, manager=obj,
        #                                             project=self.context['project'],
        #                                             type_contact='out_phone').order_by('date')[:1]
        #         if contact_qs:
        #             if self.context['date_gt'] or self.context['date_lt']:
        #                 if self.check_contact(contact_qs[0]):
        #                     result['vip_in'] += 1
        #             else:
        #                 result['vip_in'] += 1
        #     elif i.group == 'a':
        #         result['a'] += 1
        #         contact_qs = Contact.objects.filter(client=i.client, manager=obj,
        #                                             project=self.context['project'],
        #                                             type_contact='out_phone').order_by('date')[:1]
        #         if contact_qs:
        #             if self.context['date_gt'] or self.context['date_lt']:
        #                 if self.check_contact(contact_qs[0]):
        #                     result['a_in'] += 1
        #             else:
        #                 result['a_in'] += 1
        #     elif i.group == 'b':
        #         result['b'] += 1
        #         contact_qs = Contact.objects.filter(client=i.client, manager=obj,
        #                                             project=self.context['project'],
        #                                             type_contact='out_phone').order_by('date')[:1]
        #         if contact_qs:
        #             if self.context['date_gt'] or self.context['date_lt']:
        #                 if self.check_contact(contact_qs[0]):
        #                     result['b_in'] += 1
        #             else:
        #                 result['b_in'] += 1
        #     elif i.group == 'c':
        #         result['c'] += 1
        #         contact_qs = Contact.objects.filter(client=i.client, manager=obj,
        #                                             project=self.context['project'],
        #                                             type_contact='out_phone').order_by('date')[:1]
        #         if contact_qs:
        #             if self.context['date_gt'] or self.context['date_lt']:
        #                 if self.check_contact(contact_qs[0]):
        #                     result['c_in'] += 1
        #             else:
        #                 result['c_in'] += 1
        return result

    def get_not_groups(self, obj):
        qs = self.cache['clients']
        client_filter_fields = {}
        if self.context['date_gt']:
            client_filter_fields['contact__date__date__gte'] = self.context['date_gt']
        if self.context['date_lt']:
            client_filter_fields['contact__date__date__lte'] = self.context['date_lt']
        full = Client.objects.filter(
            Q(client_group__subjects__projects__in=self.context['project']),
            **{**{'id__in': qs},**client_filter_fields}).values_list('id', flat=True).distinct()
        full = set(full)
        res = set(qs)
        return {'count': len(res-full), 'client_list': list(res-full)}

    class Meta:
        model = Manager
        fields = "__all__"


class ClientContactReportSerializer(serializers.ModelSerializer):
    results = serializers.SerializerMethodField()
    group = serializers.SerializerMethodField()
    manager = serializers.SerializerMethodField()

    def get_manager(self, obj):
        return ', '.join(self.__dict__['managers'])

    def get_group(self, obj):
        if hasattr(obj, 'group_contact') and obj.group_contact:
            return obj.group_contact.upper()
        return None

    # comment = serializers.SerializerMethodField()

    def get_results(self, obj):
        res = ''
        comment = ''
        self.context['filter']['client'] = obj
        self.__dict__['managers'] = []
        # self.context['filter']['project__in'] = self.context['filter']['project__in'].split(',')
        # qs = Contact.objects.filter(**self.context['filter']).values('result', 'comment', 'date')
        qs = obj.contacts
        # for i in qs:
        #     date = i.get('date') + relativedelta(hours=3)
        #     res += '%s: %s <br>' % (date.strftime('%d-%m-%Y %H:%M'), i.get('result'),)
        #     comment += '%s <br>' % (i.get('comment'),)
        for i in qs:
            date = i.date + relativedelta(hours=3)
            res += '%s: %s <br>' % (date.strftime('%d-%m-%Y %H:%M'), i.result,)
            comment += '%s' % (i.comment,)
            if i.failure != '':
                comment += ' (%s)' % i.failure
            comment += '<br>'
            if not '%s %s' % (i.manager.user.last_name, i.manager.user.first_name) in self.__dict__['managers']:
                self.__dict__['managers'].append('%s %s' % (i.manager.user.last_name, i.manager.user.first_name))
        return {'result': res, 'comment': comment}

    class Meta:
        model = Client
        fields = ('name', 'results', 'group', 'manager')


# Block serializers for type 1 KVS and other


class ClientTypeOneSerializer(serializers.ModelSerializer):
    date_new_contact = serializers.SerializerMethodField(read_only=True)

    def get_date_new_contact(self, obj):
        if hasattr(obj, 'actual_date_new_contact'):
            return {'date': obj.actual_date_new_contact, 'goal': obj.goal_date_new_contact}
        return None

    class Meta:
        fields = '__all__'
        model = Client


class ClientTypeTwoSerializer(serializers.ModelSerializer):
    activities = serializers.SerializerMethodField()
    date_new_contact = serializers.SerializerMethodField(read_only=True)

    def get_date_new_contact(self, obj):
        if hasattr(obj, 'actual_date_new_contact'):
            return {'date': obj.actual_date_new_contact, 'goal': obj.goal_date_new_contact}
        return None


    def get_activities(self, obj):
        qs = Activities.objects.filter(entitys__client=obj).distinct().values_list('subcategories')
        act = [x[0] for x in qs]
        return ', '.join(act)

    class Meta:
        # fields = '__all__'
        model = Client
        exclude = ['client_activitys']


class ClientForManagerTypeOneSerializer(serializers.ModelSerializer):
    date_new_contact = serializers.SerializerMethodField(read_only=True)

    def get_date_new_contact(self, obj):
        if obj.actual_date_new_contact:
            return {'date': obj.actual_date_new_contact, 'goal': obj.goal_date_new_contact}
        # date = datetime.datetime.now().date()
        # qs = NewContact.objects.filter(client=obj, create_user=obj.personal_manager_type_one.user,
        #                                date_new_contact__gte=date).order_by('-id')[:1]
        # if qs:
        #     return {'date': qs[0].date_new_contact, 'goal': qs[0].goal_contact}
        return None

    class Meta:
        fields = '__all__'
        model = Client


class ClientForManagerTypeTwoSerializer(serializers.ModelSerializer):
    date_new_contact = serializers.SerializerMethodField(read_only=True)

    # activities = serializers.SerializerMethodField(read_only=True)

    # def get_activities(self, obj):
    #     qs = Activities.objects.filter(entitys__client=obj).distinct().values_list()
    #     qs = [x[0] for x in qs]
    #     return ', '.join(qs)

    def get_date_new_contact(self, obj):
        if hasattr(obj, 'actual_date_new_contact'):
            return {'date': obj.actual_date_new_contact, 'goal': obj.goal_date_new_contact}
        return None

    # def get_date_new_contact(self, obj):
    #     date = datetime.datetime.now().date()
    #     qs = NewContact.objects.filter(client=obj, create_user=obj.visitor_manager.user,
    #                                    date_new_contact__gte=date).order_by('-id')[:1]
    #     if qs:
    #         return {'date': qs[0].date_new_contact, 'goal': qs[0].goal_contact}
    #     return None

    class Meta:
        fields = '__all__'
        model = Client


class FinPlanTypeOneSerializer(serializers.ModelSerializer):
    fact_money = serializers.SerializerMethodField(read_only=True)
    percent = serializers.SerializerMethodField(read_only=True)
    diff = serializers.SerializerMethodField(read_only=True)
    month_name = serializers.SerializerMethodField(read_only=True)
    project_type = serializers.SerializerMethodField(read_only=True)
    project_type_text = serializers.SerializerMethodField(read_only=True)

    def get_fact_money(self, report):
        self.__dict__['fact'] = 0
        key = '%s.%s' % (report.month.year, report.month.month)
        if report.project_type:
            key += '.%s' % (report.project_type.id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        return 0

    def get_diff(self, report):
        # if report.fin_plan:
        #     return self.__dict__['fact'] - report.fin_plan
        return None

    def get_project_type(self, report):
        if report.project_type:
            return report.project_type.id
        return None

    def get_project_type_text(self, report):
        if report.project_type:
            return report.project_type.type
        return None

    def get_percent(self, report):
        if report.month_plan:
            return str(round((self.__dict__['fact'] / report.month_plan) * 100)) + ' %'
        return None

    def get_month_name(self, obj):
        return MONTH_NAME[obj.month.month - 1]

    class Meta:
        fields = "__all__"
        model = FinPlanTypeOne


class ManagersPlanTypeOneSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = ManagersPlanTypeOne


class ManagersPlanTypeOneSerializerRO(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField()
    fact_app_count = serializers.SerializerMethodField()
    perc_app_count = serializers.SerializerMethodField()
    fact_proc_db = serializers.SerializerMethodField()
    perc_proc_db = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.manager_id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        return 0

    def get_perc(self, obj):
        # if report.fin_plan_month:
        try:
            return str(round((self.__dict__['fact'] / obj.month_plan) * 100)) + ' %'
        except Exception as e:
            return None
        # return None

    def get_fact_app_count(self, obj):
        month = obj.fin_plan.month.month
        app_ad_count = ApplicationAd.objects.filter(manager=obj.manager, date__month=month).count()
        app_lease_kvs = ApplicationLeaseKVS.objects.filter(manager=obj.manager, date__month=month).count()
        self.__dict__['count_app'] = app_ad_count + app_lease_kvs
        return app_ad_count + app_lease_kvs

    def get_perc_app_count(self, obj):
        return round((self.__dict__['count_app'] / obj.plan_app_count) * 100, 2)

    def get_fact_proc_db(self, obj):
        clients = obj.manager.clients_type_one.all().values_list('id')
        clients = [x[0] for x in clients]
        count = 0
        year = obj.fin_plan.month.year
        month = obj.fin_plan.month.month
        for i in clients:
            if Contact.objects.filter(manager=obj.manager, client=i, date__year=year, date__month=month)[:1]:
                count += 1
        self.__dict__['count_proc_db'] = count
        return count

    def get_perc_proc_db(self, obj):
        return round((self.__dict__['count_proc_db'] / obj.plan_processing_db) * 100, 2)

    def get_manager(self, obj):
        return {'manager': '%s %s' % (obj.manager.user.last_name, obj.manager.user.first_name),
                'id': obj.manager.id}

    class Meta:
        fields = "__all__"
        model = ManagersPlanTypeOne


# class ManagersReportContactTypeOneSerializer(serializers.ModelSerializer):
#     name = serializers.SerializerMethodField()
#     count_contact = serializers.SerializerMethodField()
#     first_input = serializers.SerializerMethodField()
#     repeat_input = serializers.SerializerMethodField()
#     first_out = serializers.SerializerMethodField()
#     repeat_out = serializers.SerializerMethodField()
#     meet = serializers.SerializerMethodField()
#     e_mails = serializers.SerializerMethodField()
#     in_work = serializers.SerializerMethodField()
#     interest = serializers.SerializerMethodField()
#     app = serializers.SerializerMethodField()
#     fail = serializers.SerializerMethodField()
#     other_interest = serializers.SerializerMethodField()
#     fact = serializers.SerializerMethodField()
#
#     def get_fact(self, obj):
#         self.__dict__['fact'] = 0
#         one_c_data = self.context.get('one_c_context')
#         key = str(obj.id)
#         if key in one_c_data:
#             self.__dict__['fact'] = sum(one_c_data[key])
#             return sum(one_c_data[key])
#         return 0
#
#     def get_name(self, obj):
#         return '%s %s' % (obj.user.first_name, obj.user.last_name)
#
#     def get_count_contact(self, obj):
#         date = datetime.datetime.now()
#         filter_fields = {
#             'manager': obj,
#             # 'contact__type_contact': 'in_phone'
#         }
#         if self.context.get('date_lt'):
#             filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
#                                                                              '%Y-%m-%d').date()
#         if self.context.get('date_gt'):
#             filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
#                                                                              '%Y-%m-%d').date()
#         qs = Contact.objects.filter(**filter_fields)
#         self.__dict__['qs'] = qs
#         return qs.count()
#
#     def get_first_input(self, obj):
#         date = datetime.datetime.now()
#         filter_fields = {
#             'contact__manager': obj,
#             'contact__type_contact': 'in_phone'
#         }
#         if self.context.get('date_lt'):
#             filter_fields['contact__date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
#                                                                              '%Y-%m-%d').date()
#         if self.context.get('date_gt'):
#             filter_fields['contact__date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
#                                                                              '%Y-%m-%d').date()
#         qs_clients = list(Client.objects.filter(**filter_fields).values_list('id',
#                                                                                               flat=True).distinct())
#         # qs_clients = Contact.objects.filter(**filter_fields).values('client').annotate(cl_count=Count)
#         self.__dict__['client_id_in'] = qs_clients
#         qs = list(Contact.objects.filter(date__lt=filter_fields['contact__date__date__gte'],
#                                     manager=obj, client__in=qs_clients, type_contact='in_phone').\
#             values_list('client', flat=True).\
#             distinct('client'))
#         # qs_clients = obj.clients_type_one.all()
#         self.__dict__['count_input'] = {}
#         count = 0
#         for i in qs_clients:
#             if i not in qs:
#                 count += 1
#                 qs.append(i)
#         self.__dict__['first_in_count'] = count
#         # for i in qs_clients:
#         #     self.__dict__['count_input'][i.name] = self.__dict__['qs'].filter(client=i, type_contact='in_phone').count()
#         #     if self.__dict__['count_input'][i.name] != 0:
#         #         count += 1
#         return count
#
#     def get_repeat_input(self, obj):
#         date = datetime.datetime.now()
#         filter_fields = {
#             'contact__manager': obj,
#             # 'contact__type_contact': 'in_phone'
#         }
#         if self.context.get('date_lt'):
#             filter_fields['contact__date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
#                                                                              '%Y-%m-%d').date()
#         if self.context.get('date_gt'):
#             filter_fields['contact__date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
#                                                                              '%Y-%m-%d').date()
#         # qs_clients = self.__dict__['client_id']
#         qs_clients = list(Client.objects.filter(**filter_fields).values_list('id',
#                                                                                              flat=True).distinct())
#         qs = Contact.objects.filter(date__lte=filter_fields['contact__date__date__lte'],
#                                     date__gte=filter_fields['contact__date__date__gte'],
#                                     manager=obj, client__in=qs_clients, type_contact='in_phone')
#         return qs.count() - self.__dict__['first_in_count']
#
#     def get_first_out(self, obj):
#         # qs_clients = obj.clients_type_one.all()
#         date = datetime.datetime.now()
#         filter_fields = {
#             'contact__manager': obj,
#             'contact__type_contact': 'out_phone'
#         }
#         if self.context.get('date_lt'):
#             filter_fields['contact__date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
#                                                                              '%Y-%m-%d').date()
#         if self.context.get('date_gt'):
#             filter_fields['contact__date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
#                                                                              '%Y-%m-%d').date()
#         qs_clients = Client.objects.filter(**filter_fields).distinct()
#         qs = list(Contact.objects.filter(date__lte=filter_fields['contact__date__date__gte'],
#                                     manager=obj,
#                                     client__in=qs_clients,
#                                     type_contact='out_phone').values_list('client', flat=True).distinct('client'))
#         count = 0
#         for i in qs_clients:
#             if i not in qs:
#                 count += 1
#                 qs.append(i)
#         self.__dict__['first_out_count'] = count
#         return count
#
#     def get_repeat_out(self, obj):
#         # count = 0
#         filter_fields = {
#             'contact__manager': obj,
#             # 'contact__type_contact': 'out_phone'
#         }
#         if self.context.get('date_lt'):
#             filter_fields['contact__date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
#                                                                              '%Y-%m-%d').date()
#         if self.context.get('date_gt'):
#             filter_fields['contact__date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
#                                                                              '%Y-%m-%d').date()
#         # date = datetime.datetime.now()
#         qs_clients = list(Client.objects.filter(**filter_fields).values_list('id', flat=True).distinct())
#         # qs_clients = self.__dict__['client_id']
#         qs = Contact.objects.filter(date__date__lte=filter_fields['contact__date__date__lte'],
#                                     date__date__gte=filter_fields['contact__date__date__gte'],
#                                     manager=obj, client__in=qs_clients, type_contact='out_phone')
#         return qs.count() - self.__dict__['first_out_count']
#
#     def get_meet(self, obj):
#         return self.__dict__['qs'].filter(type_contact='meet').count()
#
#     def get_e_mails(self, obj):
#         return self.__dict__['qs'].filter(type_contact='email').count()
#
#     def get_interest(self, obj):
#         return self.__dict__['qs'].filter(interest=True).count()
#
#     def get_in_work(self, obj):
#         return self.__dict__['qs'].filter(Q(interest=True) | Q(app=True) | Q(interest_other=True)).count()
#
#     def get_fail(self, obj):
#         return self.__dict__['qs'].filter(failure_bool=True).count()
#
#     def get_app(self, obj):
#         return self.__dict__['qs'].filter(app=True).count()
#
#     def get_other_interest(self, obj):
#         return self.__dict__['qs'].filter(interest_other=True).count()
#
#     class Meta:
#         model = Manager
#         fields = "__all__"


class ManagersReportContactTypeOneSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    count_contact = serializers.SerializerMethodField()
    first_input = serializers.SerializerMethodField()
    repeat_input = serializers.SerializerMethodField()
    first_out = serializers.SerializerMethodField()
    repeat_out = serializers.SerializerMethodField()
    meet = serializers.SerializerMethodField()
    e_mails = serializers.SerializerMethodField()
    in_work = serializers.SerializerMethodField()
    interest = serializers.SerializerMethodField()
    app = serializers.SerializerMethodField()
    fail = serializers.SerializerMethodField()
    other_interest = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        one_c_data = self.context.get('one_c_context')
        key = str(obj.id)
        if key in one_c_data:
            self.__dict__['fact'] = sum(one_c_data[key])
            return sum(one_c_data[key])
        return 0

    def get_name(self, obj):
        return '%s %s' % (obj.user.first_name, obj.user.last_name)

    def get_count_contact(self, obj):
        filter_fields = {
            'manager': obj,
        }
        if self.context.get('date_lt'):
            filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
                                                                          '%Y-%m-%d').date()
        if self.context.get('date_gt'):
            filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
                                                                          '%Y-%m-%d').date()
        qs = Contact.objects.filter(**filter_fields)
        self.__dict__['qs'] = qs
        return qs.count()

    def get_first_input(self, obj):
        filter_fields = {
            'manager': obj,
            'type_contact': 'in_phone'
        }
        if self.context.get('date_lt'):
            filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
                                                                          '%Y-%m-%d').date()
        if self.context.get('date_gt'):
            filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
                                                                          '%Y-%m-%d').date()
        repeat_input_count = 0
        first_input_count = 0
        qs_clients = Contact.objects.filter(**filter_fields).values('client').annotate(cl_count=Count('client'))
        client_dict = {x['client']: x['cl_count'] for x in qs_clients}
        qs_clients_old = Contact.objects.filter(date__date__lt=filter_fields['date__date__gte'],
                                                client__in=list(client_dict), manager=obj,
                                                type_contact='in_phone').values('client'). \
            annotate(cl_count=Count('client'))
        client_dict_old = {x['client']: x['cl_count'] for x in qs_clients_old}
        for i in client_dict:
            if client_dict_old.get(i):
                repeat_input_count += client_dict[i]
                continue
            if client_dict[i] == 1:
                first_input_count += 1
            else:
                first_input_count += 1
                repeat_input_count += client_dict[i] - 1
        # self.__dict__['client_id_in'] = qs_clients
        self.__dict__['repeat_input_count'] = repeat_input_count
        self.__dict__['first_in_count'] = 0
        return first_input_count

    def get_repeat_input(self, obj):
        return self.__dict__['repeat_input_count']

    def get_first_out(self, obj):
        filter_fields = {
            'manager': obj,
            'type_contact': 'out_phone'
        }
        if self.context.get('date_lt'):
            filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
                                                                          '%Y-%m-%d').date()
        if self.context.get('date_gt'):
            filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
                                                                          '%Y-%m-%d').date()
        repeat_out_count = 0
        first_out_count = 0
        qs_clients = Contact.objects.filter(**filter_fields).values('client').annotate(cl_count=Count('client'))
        client_dict = {x['client']: x['cl_count'] for x in qs_clients}
        qs_clients_old = Contact.objects.filter(date__date__lt=filter_fields['date__date__gte'],
                                                client__in=list(client_dict), manager=obj,
                                                type_contact='out_phone').values('client'). \
            annotate(cl_count=Count('client'))
        client_dict_old = {x['client']: x['cl_count'] for x in qs_clients_old}
        for i in client_dict:
            if client_dict_old.get(i):
                repeat_out_count += client_dict[i]
                continue
            if client_dict[i] == 1:
                first_out_count += 1
            else:
                first_out_count += 1
                repeat_out_count += client_dict[i] - 1
        # self.__dict__['client_id_in'] = qs_clients
        self.__dict__['repeat_out_count'] = repeat_out_count
        return first_out_count

    def get_repeat_out(self, obj):
        return self.__dict__['repeat_out_count']

    def get_meet(self, obj):
        return self.__dict__['qs'].filter(type_contact='meet').count()

    def get_e_mails(self, obj):
        return self.__dict__['qs'].filter(type_contact='email').count()

    def get_interest(self, obj):
        return self.__dict__['qs'].filter(interest=True).count()

    def get_in_work(self, obj):
        return self.__dict__['qs'].filter(Q(interest=True) | Q(app=True) | Q(interest_other=True)).count()

    def get_fail(self, obj):
        return self.__dict__['qs'].filter(failure_bool=True).count()

    def get_app(self, obj):
        return self.__dict__['qs'].filter(app=True).count()

    def get_other_interest(self, obj):
        return self.__dict__['qs'].filter(interest_other=True).count()

    class Meta:
        model = Manager
        fields = "__all__"


class ManagersReportContactTypeTwoSerializer(serializers.ModelSerializer):
    count_contact = serializers.SerializerMethodField()
    in_contact = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    result = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        one_c_data = self.context.get('one_c_context')
        key = str(obj.id)
        if key in one_c_data:
            self.__dict__['fact'] = sum(one_c_data[key])
            return sum(one_c_data[key])
        return 0

    def get_name(self, obj):
        return '%s %s' % (obj.user.first_name, obj.user.last_name)

    def get_count_contact(self, obj):
        qs_client = list(Contact.objects.filter(manager_id=obj.id,
                                            project_id__in=self.context['project']).values_list('client_id', flat=True).distinct())
        self.__dict__['qs_client'] = qs_client
        filter_field = {
            'manager': obj,
            'project_id__in': self.context['project'],
            'client__in': qs_client
        }
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        self.__dict__['count'] = Contact.objects.filter(**filter_field).count()
        return self.__dict__['count']

    def get_in_contact(self, obj):
        clients_id = Contact.objects.filter(manager_id=obj.id,
                                            project_id__in=self.context['project']).values_list('client_id', flat=True).distinct()
        # clients_id = [x['id'] for x in qs_client]
        # qs_client = obj.membership.filter(project=self.context['project']).distinct()
        result = {'first_in': 0, 'repeat_in': 0, 'first_out': 0, 'repeat_out': 0,
                  'meet': 0, 'email': 0, 'interest': 0, 'fail': 0, 'app': 0}
        filter_field = {
            'manager': obj.id,
            'project_id__in': self.context['project']
        }
        filter_field_inversion = {
            'manager': obj.id,
            'project_id__in': self.context['project']
        }
        check_old_data = False
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
            filter_field_inversion['date__date__lt'] = parser.parse(self.context['date_gt'])
            check_old_data = True
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        temp = Contact.objects.filter(**filter_field, client__in=clients_id).values('type_contact',
                                                                                    'interest',
                                                                                    'failure_bool',
                                                                                    'app',
                                                                                    'client')
        temp_all_project_phone_in = []
        temp_all_project_phone_out = []
        if check_old_data:
            temp_all_project_phone_in = list(Contact.objects.filter(**filter_field_inversion, client__in=clients_id,
                                                                    type_contact='1'). \
                                             values_list('client', flat=True).distinct('client'))
            temp_all_project_phone_out = list(Contact.objects.filter(**filter_field_inversion, client__in=clients_id,
                                                                     type_contact='out_phone'). \
                                              values_list('client', flat=True).distinct('client'))
        for j in temp:
            if j['type_contact'] == 'in_phone' and not j['client'] in temp_all_project_phone_in:
                result['first_in'] += 1
                temp_all_project_phone_in.append(j['client'])
                # phone_in_flag = True
            elif j['type_contact'] == 'in_phone':
                result['repeat_in'] += 1
            if j['type_contact'] == 'out_phone' and not j['client'] in temp_all_project_phone_out:
                result['first_out'] += 1
                temp_all_project_phone_out.append(j['client'])
                # phone_out_flag = True
            elif j['type_contact'] == 'out_phone':
                result['repeat_out'] += 1
            if j['type_contact'] == 'meet':
                result['meet'] += 1
            if j['type_contact'] == 'email':
                result['email'] += 1
        return result

    def get_result(self, obj):
        # qs_client = Client.objects.filter(membership__manager_id=obj.id,
        #                                   membership__project_id__in=self.context['project']).values('id').distinct()
        clients_id = self.__dict__['qs_client']
        result = {'interest': 0, 'fail': 0, 'app': 0, 'work': 0, 'interest_other': 0}
        filter_field = {
            'manager_id': obj.id,
            'project_id__in': self.context['project']
        }
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        # for i in qs_client:
        #     filter_field['client'] = i['id']
        qs = Contact.objects.filter(**filter_field, client__in=clients_id).order_by('-date')
        for i in qs:
            if qs.exists():
                if i.failure_bool:
                    result['fail'] += 1
                elif i.app:
                    result['app'] += 1
                elif i.interest:
                    result['interest'] += 1
                elif i.interest_other:
                    result['interest_other'] += 1
                else:
                    result['work'] += 1
        result['work'] += result['app'] + result['interest'] + result['interest_other']
        return result
    # TODO изменил проверить отчет по контактам 3 блок
    # name = serializers.SerializerMethodField()
    # count_contact = serializers.SerializerMethodField()
    # first_input = serializers.SerializerMethodField()
    # repeat_input = serializers.SerializerMethodField()
    # first_out = serializers.SerializerMethodField()
    # repeat_out = serializers.SerializerMethodField()
    # meet = serializers.SerializerMethodField()
    # e_mails = serializers.SerializerMethodField()
    # in_work = serializers.SerializerMethodField()
    # interest = serializers.SerializerMethodField()
    # app = serializers.SerializerMethodField()
    # fail = serializers.SerializerMethodField()
    # other_interest = serializers.SerializerMethodField()
    # fact = serializers.SerializerMethodField()
    #
    # def get_fact(self, obj):
    #     self.__dict__['fact'] = 0
    #     one_c_data = self.context.get('one_c_context')
    #     key = str(obj.id)
    #     if key in one_c_data:
    #         self.__dict__['fact'] = sum(one_c_data[key])
    #         return sum(one_c_data[key])
    #     return 0
    #
    # def get_name(self, obj):
    #     return '%s %s' % (obj.user.first_name, obj.user.last_name)
    #
    # def get_count_contact(self, obj):
    #     filter_fields = {
    #         'manager': obj,
    #         'project__in': self.context.get('project').split(',')
    #     }
    #     if self.context.get('date_lt'):
    #         filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
    #                                                                       '%Y-%m-%d').date()
    #     if self.context.get('date_gt'):
    #         filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
    #                                                                       '%Y-%m-%d').date()
    #     qs = Contact.objects.filter(**filter_fields)
    #     self.__dict__['qs'] = qs
    #     return qs.count()
    #
    # def get_first_input(self, obj):
    #     filter_fields = {
    #         'manager': obj,
    #         'type_contact': 'in_phone',
    #         'project__in': self.context.get('project').split(',')
    #     }
    #     if self.context.get('date_lt'):
    #         filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
    #                                                                       '%Y-%m-%d').date()
    #     if self.context.get('date_gt'):
    #         filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
    #                                                                       '%Y-%m-%d').date()
    #     repeat_input_count = 0
    #     first_input_count = 0
    #     qs_clients = Contact.objects.filter(**filter_fields).values('client').annotate(cl_count=Count('client'))
    #     client_dict = {x['client']: x['cl_count'] for x in qs_clients}
    #     qs_clients_old = Contact.objects.filter(date__date__lt=filter_fields['date__date__gte'],
    #                                             client__in=list(client_dict), manager=obj,
    #                                             type_contact='in_phone').values('client'). \
    #         annotate(cl_count=Count('client'))
    #     client_dict_old = {x['client']: x['cl_count'] for x in qs_clients_old}
    #     for i in client_dict:
    #         if client_dict_old.get(i):
    #             repeat_input_count += client_dict[i]
    #             continue
    #         if client_dict[i] == 1:
    #             first_input_count += 1
    #         else:
    #             first_input_count += 1
    #             repeat_input_count += client_dict[i] - 1
    #     # self.__dict__['client_id_in'] = qs_clients
    #     self.__dict__['repeat_input_count'] = repeat_input_count
    #     self.__dict__['first_in_count'] = 0
    #     return first_input_count
    #
    # def get_repeat_input(self, obj):
    #     return self.__dict__['repeat_input_count']
    #
    # def get_first_out(self, obj):
    #     filter_fields = {
    #         'manager': obj,
    #         'type_contact': 'out_phone',
    #         'project__in': self.context.get('project').split(',')
    #     }
    #     if self.context.get('date_lt'):
    #         filter_fields['date__date__lte'] = datetime.datetime.strptime(self.context.get('date_lt'),
    #                                                                       '%Y-%m-%d').date()
    #     if self.context.get('date_gt'):
    #         filter_fields['date__date__gte'] = datetime.datetime.strptime(self.context.get('date_gt'),
    #                                                                       '%Y-%m-%d').date()
    #     repeat_out_count = 0
    #     first_out_count = 0
    #     qs_clients = Contact.objects.filter(**filter_fields).values('client').annotate(cl_count=Count('client'))
    #     client_dict = {x['client']: x['cl_count'] for x in qs_clients}
    #     qs_clients_old = Contact.objects.filter(date__date__lt=filter_fields['date__date__gte'],
    #                                             client__in=list(client_dict), manager=obj,
    #                                             type_contact='out_phone').values('client'). \
    #         annotate(cl_count=Count('client'))
    #     client_dict_old = {x['client']: x['cl_count'] for x in qs_clients_old}
    #     for i in client_dict:
    #         if client_dict_old.get(i):
    #             repeat_out_count += client_dict[i]
    #             continue
    #         if client_dict[i] == 1:
    #             first_out_count += 1
    #         else:
    #             first_out_count += 1
    #             repeat_out_count += client_dict[i] - 1
    #     # self.__dict__['client_id_in'] = qs_clients
    #     self.__dict__['repeat_out_count'] = repeat_out_count
    #     return first_out_count
    #
    # def get_repeat_out(self, obj):
    #     return self.__dict__['repeat_out_count']
    #
    # def get_meet(self, obj):
    #     return self.__dict__['qs'].filter(type_contact='meet').count()
    #
    # def get_e_mails(self, obj):
    #     return self.__dict__['qs'].filter(type_contact='email').count()
    #
    # def get_interest(self, obj):
    #     return self.__dict__['qs'].filter(interest=True).count()
    #
    # def get_in_work(self, obj):
    #     return self.__dict__['qs'].filter(Q(interest=True) | Q(app=True) | Q(interest_other=True)).count()
    #
    # def get_fail(self, obj):
    #     return self.__dict__['qs'].filter(failure_bool=True).count()
    #
    # def get_app(self, obj):
    #     return self.__dict__['qs'].filter(app=True).count()
    #
    # def get_other_interest(self, obj):
    #     return self.__dict__['qs'].filter(interest_other=True).count()

    class Meta:
        model = Manager
        fields = "__all__"


class ManagersReportContactIndividualTypeTwoSerializer(serializers.ModelSerializer):
    count_contact = serializers.SerializerMethodField()
    in_contact = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    result = serializers.SerializerMethodField()
    # fact = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        one_c_data = self.context.get('one_c_context')
        key = str(obj.id)
        if key in one_c_data:
            self.__dict__['fact'] = sum(one_c_data[key])
            return sum(one_c_data[key])
        return 0

    def get_name(self, obj):
        return '%s %s' % (obj.user.first_name, obj.user.last_name)

    def get_count_contact(self, obj):
        qs_client = list(IndividualContact.objects.filter(manager_id=obj.id,
                                            project_id__in=self.context['project']).values_list('individual_id', flat=True).distinct())
        self.__dict__['qs_client'] = qs_client
        filter_field = {
            'manager': obj,
            'project_id__in': self.context['project'],
            'individual__in': qs_client
        }
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        self.__dict__['count'] = IndividualContact.objects.filter(**filter_field).count()
        return self.__dict__['count']

    def get_in_contact(self, obj):
        clients_id = IndividualContact.objects.filter(manager_id=obj.id,
                                            project_id__in=self.context['project']).values_list('individual_id', flat=True).distinct()
        # clients_id = [x['id'] for x in qs_client]
        # qs_client = obj.membership.filter(project=self.context['project']).distinct()
        result = {'first_in': 0, 'repeat_in': 0, 'first_out': 0, 'repeat_out': 0,
                  'meet': 0, 'email': 0, 'interest': 0, 'fail': 0, 'app': 0}
        filter_field = {
            'manager': obj.id,
            'project_id__in': self.context['project']
        }
        filter_field_inversion = {
            'manager': obj.id,
            'project_id__in': self.context['project']
        }
        check_old_data = False
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
            filter_field_inversion['date__date__lt'] = parser.parse(self.context['date_gt'])
            check_old_data = True
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        temp = IndividualContact.objects.filter(**filter_field, individual__in=clients_id).values('type_contact',
                                                                                    'interest',
                                                                                    'failure_bool',
                                                                                    'waiting',
                                                                                    'individual')
        temp_all_project_phone_in = []
        temp_all_project_phone_out = []
        if check_old_data:
            temp_all_project_phone_in = list(IndividualContact.objects.filter(**filter_field_inversion, individual__in=clients_id,
                                                                    type_contact='1'). \
                                             values_list('individual', flat=True).distinct('individual'))
            temp_all_project_phone_out = list(IndividualContact.objects.filter(**filter_field_inversion, individual__in=clients_id,
                                                                     type_contact='out_phone'). \
                                              values_list('individual', flat=True).distinct('individual'))
        for j in temp:
            if j['type_contact'] == 'in_phone' and not j['individual'] in temp_all_project_phone_in:
                result['first_in'] += 1
                temp_all_project_phone_in.append(j['individual'])
                # phone_in_flag = True
            elif j['type_contact'] == 'in_phone':
                result['repeat_in'] += 1
            if j['type_contact'] == 'out_phone' and not j['individual'] in temp_all_project_phone_out:
                result['first_out'] += 1
                temp_all_project_phone_out.append(j['individual'])
                # phone_out_flag = True
            elif j['type_contact'] == 'out_phone':
                result['repeat_out'] += 1
            if j['type_contact'] == 'meet':
                result['meet'] += 1
            if j['type_contact'] == 'email':
                result['email'] += 1
        return result

    def get_result(self, obj):
        # qs_client = Client.objects.filter(membership__manager_id=obj.id,
        #                                   membership__project_id__in=self.context['project']).values('id').distinct()
        clients_id = self.__dict__['qs_client']
        result = {'interest': 0, 'fail': 0, 'waiting': 0, 'work': 0, 'interest_other': 0}
        filter_field = {
            'manager_id': obj.id,
            'project_id__in': self.context['project']
        }
        if self.context['date_gt'] != '':
            filter_field['date__date__gte'] = parser.parse(self.context['date_gt'])
        if self.context['date_lt'] != '':
            filter_field['date__date__lte'] = parser.parse(self.context['date_lt'])
        # for i in qs_client:
        #     filter_field['client'] = i['id']
        qs = IndividualContact.objects.filter(**filter_field, individual__in=clients_id).order_by('-date')
        for i in qs:
            if qs.exists():
                if i.failure_bool:
                    result['fail'] += 1
                elif i.waiting:
                    result['waiting'] += 1
                elif i.interest:
                    result['interest'] += 1
                elif i.waiting:
                    result['waiting'] += 1
                else:
                    result['work'] += 1
        result['work'] += result['waiting'] + result['interest'] + result['waiting']
        return result

    class Meta:
        model = Manager
        fields = "__all__"


class StartAlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = StartAlert
        fields = '__all__'


class ManagerContactReportIndividualSOISerializer(serializers.ModelSerializer):
    sof_count = serializers.IntegerField(read_only=True)
    source_of_information = serializers.SerializerMethodField(read_only=True)

    def get_source_of_information(self, obj):
        for i in self.Meta.model.source_of_information_choices:
            if i[0] == obj['source_of_information']:
                return i[1]
        return None

    class Meta:
        model = IndividualContact
        fields = ['source_of_information', 'sof_count']


c_models = {
        'kvs': ApplicationLeaseKVS,
        'ad': ApplicationAd,
        'expo': ApplicationExpo
    }


# class EntityMergeSerializer(serializers.ModelSerializer):
#     project_id = serializers.SerializerMethodField()
#     manager_id = serializers.SerializerMethodField()
#     app_id = serializers.SerializerMethodField()
#
#     def get_app_id(self, obj):
#         ids = [str(x) for x in list(c_models[self.context.get('project_type')].objects.filter(entity=obj.id,
#                                                                    project__in=self.context.get('project')).values_list('id', flat=True))]
#         return ','.join(ids)
#
#     def get_project_id(self, obj):
#         app = c_models[self.context.get('project_type')].objects.filter(entity=obj.id, project__in=self.context.get('project'))
#         if app and len(app) == 1:
#             return app[0].project_id
#         return None
#
#     def get_manager_id(self, obj):
#         app = c_models[self.context.get('project_type')].objects.filter(entity=obj.id, project__in=self.context.get('project'))
#         if app:
#             return app[0].manager_id
#         return None
#
#     class Meta:
#         model = Entity
#         fields = "__all__"


class EntityMergeSerializer(serializers.ModelSerializer):
    project_id = serializers.SerializerMethodField()
    manager_id = serializers.SerializerMethodField()
    entity_id = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()
    app_id = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    bill_number = serializers.SerializerMethodField()

    def get_bill_number(self, obj):
        return obj.get('applicationleasekvs__bill_number')

    def get_project_id(self, obj):
        return obj.get('id')

    def get_manager_id(self, obj):
        if obj.get('applicationleasekvs__manager_id'):
            return obj.get('applicationleasekvs__manager_id')
        elif obj.get('applicationad__manager_id'):
            return obj.get('applicationad__manager_id')
        else:
            return obj.get('applicationexpo__manager_id')

    def get_entity_id(self, obj):
        if obj.get('applicationleasekvs__entity_id'):
            return obj.get('applicationleasekvs__entity_id')
        elif obj.get('applicationad__entity_id'):
            return obj.get('applicationad__entity_id')
        else:
            return obj.get('applicationexpo__entity_id')

    def get_client(self, obj):
        if obj.get('applicationleasekvs__client_id'):
            return obj.get('applicationleasekvs__client_id')
        elif obj.get('applicationad__client_id'):
            return obj.get('applicationad__client_id')
        else:
            return obj.get('applicationexpo__client_id')

    def get_app_id(self, obj):
        if obj.get('applicationleasekvs__id'):
            return obj.get('applicationleasekvs__id')
        elif obj.get('applicationad__id'):
            return obj.get('applicationad__id')
        else:
            return obj.get('applicationexpo__id')

    def get_full_name(self, obj):
        if obj.get('applicationleasekvs__entity__full_name'):
            return obj.get('applicationleasekvs__entity__full_name')
        elif obj.get('applicationad__entity__full_name'):
            return obj.get('applicationad__entity__full_name')
        else:
            return obj.get('applicationexpo__entity__full_name')

    class Meta:
        model = Project
        fields = ['project_id', 'manager_id', 'entity_id', 'client', 'app_id', 'full_name', 'bill_number']


class DvpReportSerializer(serializers.ModelSerializer):
    fact = serializers.SerializerMethodField()
    plan_money = serializers.SerializerMethodField()
    perc_money = serializers.SerializerMethodField()
    plan_app = serializers.SerializerMethodField()
    fact_app = serializers.SerializerMethodField()
    perc_app = serializers.SerializerMethodField()
    period = serializers.SerializerMethodField()
    not_pay = serializers.SerializerMethodField()
    sum_pay = serializers.SerializerMethodField()
    approval = serializers.SerializerMethodField()
    return_val = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()
    count_vis_fact = serializers.SerializerMethodField()
    sum_amount_fact = serializers.SerializerMethodField()

    def get_sum_amount_fact(self, obj):
        return obj.sum_amount_fact

    def get_count_vis_fact(self, obj):
        return obj.count_vis_fact

    def get_user_name(self, obj):
        user = obj.user
        return '%s %s' % (user.last_name, user.first_name)

    def get_approval(self, obj):
        return obj.on_approval

    def get_return_val(self, obj):
        return obj.return_val

    def get_period(self, obj):
        return '%s - %s' % (obj.date_start.strftime('%d-%m-%Y'), obj.date_end.strftime('%d-%m-%Y'))

    def get_perc_money(self, obj):
        if obj.plan.cash_receipts_exp:
            return str(round((self.__dict__['fact']/obj.plan.cash_receipts_exp)*100, 2)) + ' %'
        return None

    def get_perc_app(self, obj):
        if obj.plan.count_app:
            return str(round((obj.applicationexpo__count/obj.plan.count_app)*100, 2)) + ' %'
        return None

    def get_fact_app(self, obj):
        return obj.applicationexpo__count

    def get_plan_app(self, obj):
        return obj.plan.count_app

    def get_plan_money(self, obj):
        return obj.plan.cash_receipts_exp

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.id)
        if key in self.context['pay']:
            self.__dict__['fact'] = sum(self.context['pay'][key])
            return sum(self.context['pay'][key])
        return 0

    def get_not_pay(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.id)
        if key in self.context['not_pay']:
            self.__dict__['fact'] = sum(self.context['not_pay'][key])
            return sum(self.context['not_pay'][key])
        return 0

    def get_sum_pay(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.id)
        if key in self.context['sum']:
            self.__dict__['fact'] = sum(self.context['sum'][key])
            return sum(self.context['sum'][key])
        return 0

    class Meta:
        model = Project
        fields = ['id', 'name', 'fact', 'plan_money', 'plan_app', 'fact_app', 'perc_money', 'perc_app', 'period',
                  'status', 'not_pay', 'sum_pay', 'approval', 'user_name', 'count_vis_fact', 'sum_amount_fact',
                  'return_val']


class ReportByWeekSerializer(serializers.Serializer):
    s_d = serializers.DateField()
    e_d = serializers.DateField()
    count = serializers.IntegerField()
    week_number = serializers.IntegerField()
    week = serializers.IntegerField()
    date_diff = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    fact_bill = serializers.SerializerMethodField()

    def get_date_diff(self, obj):
        return '%s-%s' % (obj['s_d'].strftime("%d.%m"), obj['e_d'].strftime("%d.%m"))

    def get_fact(self, obj):
        key = obj['week']
        if key in self.context['one_c']:
            self.__dict__['fact'] = sum(self.context['one_c'][key])
            return sum(self.context['one_c'][key])
        return 0

    def get_fact_bill(self, obj):
        key = obj['week']
        if key in self.context['one_c_bill']:
            self.__dict__['fact'] = sum(self.context['one_c_bill'][key])
            return sum(self.context['one_c_bill'][key])
        return 0


class NotificationSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField()
    date_created = CustomDateTimeField()

    def get_sender(self, obj):
        return 'Системное уведомление'

    class Meta:
        model = Notification
        fields = '__all__'


class ContentTypeSerializer(serializers.ModelSerializer):
    fields_list = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.name

    def get_fields_list(self, obj):
        try:
            fields = apps.get_model(app_label=obj.app_label, model_name=obj.model)._meta.get_fields()
        except:
            return []
        return [{'value': x.name, 'name':x.verbose_name if hasattr(x, 'verbose_name') else x.name} for x in fields]

    class Meta:
        model = ContentType
        fields = '__all__'



class ReductionFactorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReductionFactor
        fields = "__all__"