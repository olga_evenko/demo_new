define(['text!workers/html/main_view.html', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize'], function(html){
	
	var PlanProjectManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#main_dvp_plan_manager_view')[0].outerHTML,
		events: {
			'click #report': 'getReport'
		},
		className: 'displayNone',
		templateContext: function(){
			var show_select = false;
			// if(user.direction.length > 0){
			// 	show_select = true;
			// }
			// if(user.groups.indexOf('project_managers')!=-1){
			// 	return {buttonCreate: true, showSelect: show_select};
			// }
		},
		initialize: function(){
		},
		getReport: function(e){
			window.open('/report/projects_report?project_indicators__project__project_type__director='+this.SelectDirection[0].selectize.getValue()+'&months='+
						this.$el.find('input[name="month"]').val());
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			head_direction = true;
			var date = new Date();
			this.$el.find('input[name="month"]').val(date.getFullYear().toString()+'-'+((date.getMonth()+1)<10?'0'+(date.getMonth()+1).toString():(date.getMonth()+1).toString()));
			this.SelectDirection = this.$el.find('#SelectDirection').selectize({
				valueField: 'id',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/users/?direction__type_project_type=expo',
						success: function(response){
							for(var i in response){
								_this.addOption({
									id: response[i].id, 
									text: response[i].last_name+' '+response[i].first_name
								});
							}
						}
					});
				},
				onChange: function(value){
					$.get({
						url: '/apiworkers/users/?managers__head='+value,
						success: function(response){
							_view.SelectProjectManager[0].selectize.clearOptions();
							for(var i in response){
								_view.SelectProjectManager[0].selectize.addOption({
									id: response[i].id, 
									text: response[i].last_name+' '+response[i].first_name
								});
							}
						}
					});
					if(_view.$el.find('input[name="month"]').val() == ''){
						_view.Table.bootstrapTable('refresh', {
							url: '/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+value+'&month=1'
						});
						return;
					}
					_view.Table.bootstrapTable('refresh', {
						url: '/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+value+'&months='+
						_view.$el.find('input[name="month"]').val()
					});
				}
			});
			this.SelectProjectManager = this.$el.find('#SelectProjectsManager').selectize({
					valueField: 'id',
					onInitialize: function(){
						var _this = this;
					},
					onChange: function(value){
						// if(value != ''){
						if(_view.$el.find('input[name="month"]').val() == ''){
							_view.Table.bootstrapTable('refresh', {
								url: '/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+
								_view.SelectDirection[0].selectize.getValue()+
								'&month=1&project_indicators__project__user='+value
							});
							return;
						}
						_view.Table.bootstrapTable('refresh', {
							url: '/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+
							_view.SelectDirection[0].selectize.getValue()+
							'&months='+_view.$el.find('input[name="month"]').val()+'&project_indicators__project__user='+value
						});
						// _view.Table.bootstrapTable('refresh', {
						// 	url: '/apiprojects/project_reports/?project_indicators__project__user='+value+'&month=1'
						// });
						// }
						// else{
						// 	_view.Table.bootstrapTable('refresh', {
						// 		url: '/apiprojects/project_reports/?project_indicators__project__project_type__director='+user.id+'&month=1'
						// 	});
						// }
					}
				});
			this.$el.find('input[name="month"]').on('change', function(){
				_view.Table.bootstrapTable('refresh', {
					url: '/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+
					_view.SelectDirection[0].selectize.getValue()+'&months='+this.value+
					'&project_indicators__project__user='+_view.SelectProjectManager[0].selectize.getValue()
				});
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiprojects/project_reports/?all=true&month=1',
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				toolbar: $(html).filter('#toolbar-report-project-dvp')[0].innerHTML,
				columns: [
				{
					field: 'project.name',
					title: 'Проекты в работе',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'perc_comp',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				],
				responseHandler: function(response){
					// var results = response.results;
					var sum_fin_plan = 0;
					var sum_fact = 0;
					var sum_app_plan = 0;
					var sum_fact_app = 0;
					var sum_fact_processing_db = 0;
					var sum_plan_processing_db = 0;
					for(i in response){
						sum_fin_plan = sum_fin_plan + response[i].fin_plan_month;
						sum_fact = sum_fact + response[i].fact;
						sum_app_plan = sum_app_plan + response[i].app_plan_month;
						sum_fact_app = sum_fact_app + response[i].fact_app;
						sum_fact_processing_db = sum_fact_processing_db + response[i].fact_processing_db;
						sum_plan_processing_db = sum_plan_processing_db + response[i].plan_processing_db;
					}
					var perc = 0, perc_comp = 0, perc_comp_db = 0;
					if(sum_fin_plan != 0){
						perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
					}
					if(sum_app_plan != 0){
						perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
					}
					if(sum_plan_processing_db != 0){
						perc_comp_db = ((sum_fact_processing_db/sum_plan_processing_db)*100).toFixed(2);
					}
					response.push({
						project: {name: 'Итого'},
						fin_plan_month: sum_fin_plan,
						perc: perc.toString()+' %',
						perc_comp: perc_comp.toString()+' %',
						perc_comp_db: perc_comp_db.toString()+' %',
						fact: sum_fact,
						app_plan_month: sum_app_plan,
						fact_app: sum_fact_app,
						fact_processing_db: sum_fact_processing_db,
						plan_processing_db: sum_plan_processing_db
					});
					return response;
				},
				onClickRow: function(row, e, index){
					require(['workers/js/director_plan_manager_by_project', 'workers/js/modals/modal_templates'], function(View, tmp){
						var modal = new Backbone.BootstrapModal({ 
							content: new View({report: row.id}),
							title: 'План по проекту '+row.project.name,
							okText: 'Закрыть',
							cancelText: '',
							animate: true, 
							template: tmp.full_screen
						}).open();
						modal.on('ok', function(){
							
						});
					});
				},
				rowStyle: function(row, index) {
					if(row.project.name == 'Итого'){
						return {
							classes: 'info',
							css: {"cursor": "pointer"}
						};
					}
					return {
							css: {"cursor": "pointer"}
						};
				}
			});
		},
	});
return PlanProjectManagerView;
});
