define(['text!workers/html/projects/projects_view.html', 'bootstrap-table-locale', 'bootstrap-modal'], function(html){

	var ProjectsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#ProjectView')[0].outerHTML,
		events: {
			'click #addProject': 'addProject',
			'click .edit': 'editProject',
			'click .copy': 'copyProject',
			'click .filter_button_client': 'setFilter',
			'click #resetfilterButton': 'resetFilter'
		},
		regions: {
		},
		className: 'displayNone',
		defaultUrl: true,
		addProject: function(){
			router.navigate('project/create', {trigger: true});
		},
		setFilter: function(e){
			var _this = this;
			if(e.currentTarget.id=='in_working_project'){
				this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?status=working&project_type='+user.direction.join(',')});	
			}
			else{
				this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/'});
			}
		},
		resetFilter: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type='+user.direction.join(',')});
		},
		initialize: function(){},
		copyProject: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			require(['projects/js/modal_extend_project_view'], function(ModalExtendProjectView){
				var _this=this;
				var modal = new Backbone.BootstrapModal({
					content: new ModalExtendProjectView({name:row.name}), 
					title: 'Создать проект по выбранному',
					okText: 'Создать',
					cancelText: 'Отмена',
					animate: true, 
				}).open();
				modal.on('ok', function(){
					$('.loading').fadeIn();
					var data = new FormData(this.$content.find('form')[0]);
					data.append('id', row.id);
					$.ajax({
						type: 'POST',
						url: 'apiprojects/projects/extend/',
						contentType: false,
						processData: false,
						data: data,
						success: function(response){
							router.navigate('project/'+response.id+'/edit', {trigger: true});
						}
					});
				});
			});
		},
		editProject: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('project/'+row.id+'/edit', {trigger: true});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
//			var head_user = false;
//			if(user.direction.length > 0){
//				head_user = true;
//			}

			var url = ''

			if(user.groups.indexOf('head_direction')>=0){
                url = '/apiprojects/projects/?user__managers__head__id='+user.id
			}else{
			    url = '/apiprojects/projects/?user='+user.id
			    if(user.direction.length > 0){
				    '/apiprojects/projects/?project_type__director='+user.id
			    }
			}

			if(_view.options.proj_type){
                url+='&project_type='+_view.options.proj_type
			}
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: url,
				newQuery: {},
				onRefresh: function(options){
					if(options.query){
						this.newQuery = options.query;	
					}
					else{
						this.newQuery = {};
					};
				},
				onClickRow: function(row, e, index){
//				    if(user.groups.indexOf('head_expo')>=0){
//				        return
//				    }
					if(index!='edit'){
						router.navigate('project/'+row.id, {trigger: true});
					}
				},
				onPageChange: function(num, size){
					router.navigate('projects_type_0/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'user',
					title: 'Руководитель проекта',
					sortable: true,
				},
				{
					field: 'project_type.type',
					title: 'Вид проекта',
					sortable: true,
				},
				{
					field: 'buisness_unit.name',
					title: 'Бизнес-единица',
					sortable: true,
				},
				{
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'sites_all',
					title: 'Место проведения',
					// sortable: true,
				},
				{
					field: 'status',
					title: 'Статус',
					sortable: true,
					formatter: function(value, row, index){
						var status = {
							'planning': 'Планирование',
							'working': 'В работе',
							'canceled': 'Отменен',
							'finished': 'Завершен'
						};
						return status[value]
					}
				},
//				(user.groups.indexOf('head_direction')>=0?{ field: 'drop_menu',title: '',}:undefined),

				],
				rowAttributes: function(row, index, e){

				},
				responseHandler: function(response){

					var results = response.results;
					var sites_arr = [];
					var s = 0;
					var sum_cap = 0;
					for(i in results){
						for(j in results[i].sites){
							 sites_arr.push(results[i].sites[j].name);
							 s += parseInt(results[i].sites[j].area);
							 sum_cap += results[i].sites[j].capacity;
						}
						results[i].sites_all = sites_arr.join(', ');
						results[i].all_area = s;
						results[i].all_capacity = sum_cap;
						sites_arr = [];
						s = 0; 
						sum_cap = 0;

//						if(user.groups.indexOf('head_direction')>=0){
//						    results[i].drop_menu = `
//                                <div class="dropdown">
//                                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Меню
//                                <span class="caret"></span></button>
//                                <ul class="dropdown-menu dropdown-menu-right">`
//
//
//                            if(user.groups.indexOf('head_expo')>=0){
//                                results[i].drop_menu +=` <li><a href="#project/${results[i].id}/edit">План прямого маркетинга</a></li>`
//                                results[i].drop_menu +=` <li><a href="#director_plan_manager_dvp">План месяца по проектам</a></li>`
//                                results[i].drop_menu +=` <li><a href="#group_plan">План месяца по менеджерам</a></li>`
//                            }
//                            else{
//                                results[i].drop_menu +=` <li><a href="#project/${results[i].id}">Список проектов</a></li>`
//                                results[i].drop_menu +=` <li><a href="#director_plan_manager_dvp">План месяца по проектам</a></li>`
//                                results[i].drop_menu +=` <li><a href="#group_plan">План месяца по менеджерам</a></li>`
//                            }
//
//                            results[i].drop_menu +=' </ul></div>'
//
//				        }






					};
					return response
				},
				queryParams: function(params){
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		},
	});
return ProjectsView
})
