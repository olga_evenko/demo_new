define(['text!workers/html/projects/projects_view.html', 'bootstrap-table-locale', 'bootstrap-modal'], function(html){

	var ProjectsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#ProjectView')[0].outerHTML,
		events: {
			'click #addProject': 'addProject',
			'click .edit': 'editProject',
			'click .filter_button_client': 'setFilter',
			'click #resetfilterButton': 'resetFilter'
		},
		regions: {
		},
		className: 'displayNone',
		defaultUrl: true,
		addProject: function(){
			router.navigate('project/create', {trigger: true});
		},
		setFilter: function(e){
			var _this = this;
			if(e.currentTarget.id=='in_work_project'){
				this.Table.bootstrapTable('refresh', {url: this.currentUrl+'&status=working,planning'});
			}
			if(e.currentTarget.id=='my_project'){
				this.currentUrl = '/apiprojects/projects/?user='+user.id+'&project_type__director='+user.header;
				this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?user='+user.id+'&project_type__director='+user.header});	
			}
			if(e.currentTarget.id=='all_project'){
				this.currentUrl = '/apiprojects/projects/?project_type__director='+user.header;
				this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type__director='+user.header});
			}
		},
		resetFilter: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type__director='+user.header});
		},
		initialize: function(){
		},
		editProject: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('project/'+row.id+'/edit', {trigger: true});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.currentUrl = '/apiprojects/projects/?project_type__director='+user.header;
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				showColumns: true,
				sortName: 'date_start',
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiprojects/projects/?project_type__director='+user.header,
				newQuery: {},
				onRefresh: function(options){
					if(options.query){
						this.newQuery = options.query;	
					}
					else{
						this.newQuery = {};
					};
				},
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('project/'+row.id, {trigger: true});
					}
				},
				onPageChange: function(num, size){
					router.navigate('my_projects_manager_type_1/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				toolbar: $(html).filter('#toolbar_manager_type1')[0].innerHTML,
				columns: [
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'user',
					title: 'Руководитель проекта',
					sortable: true,
				},
				{
					field: 'project_type.type',
					title: 'Вид проекта',
					sortable: true,
				},
				{
					field: 'buisness_unit.name',
					title: 'Бизнес-единица',
					sortable: true,
				},
				{
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'sites_all',
					title: 'Место проведения',
					// sortable: true,
				},
				{
					field: 'count_guests',
					title: 'Количество гостей',
					sortable: true
				},
				{
					field: 'status',
					title: 'Статус',
					sortable: true,
					formatter: function(value, row, index){
						var status = {
							'planning': 'Планирование',
							'working': 'В работе',
							'canceled': 'Отменен',
							'finished': 'Завершен'
						};
						return status[value]
					}
				},
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil edit" data-id ='+
						row.id+'></span></div>'
					}
				}],
				rowAttributes: function(row, index, e){

				},
				responseHandler: function(response){
					var results = response.results;
					var sites_arr = [];
					var s = 0;
					var sum_cap = 0;
					for(i in results){
						for(j in results[i].sites){
							 sites_arr.push(results[i].sites[j].name);
							 s += parseInt(results[i].sites[j].area);
							 sum_cap += results[i].sites[j].capacity;
						}
						results[i].sites_all = sites_arr.join(', ');
						results[i].all_area = s;
						results[i].all_capacity = sum_cap;
						sites_arr = [];
						s = 0; 
						sum_cap = 0;
					};
					return response
				},
				queryParams: function(params){
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		},
	});
return ProjectsView
})
