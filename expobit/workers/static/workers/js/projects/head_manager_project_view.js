define(['text!workers/html/projects/projects_view.html', 'bootstrap-table-locale', 'bootstrap-modal'], function(html){

	var ProjectsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#ProjectView')[0].outerHTML,
		events: {
			'click #addProject': 'addProject',
			'click .edit': 'editProject',
			'click .set_filter': 'setFilterFileds',
			'click .filter_button_client': 'setFilter',
			'click #resetfilterButton': 'resetFilter'
		},
		regions: {
		},
		className: 'displayNone',
		defaultUrl: true,
		addProject: function(){
			router.navigate('project/create', {trigger: true});
		},
		setFilterFileds: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type='+user.direction.join(',')+'&'+this.$el.find('form').serialize()});
		},
		setFilter: function(e){
			var _this = this;
			if(e.currentTarget.id=='in_working_project'){
				this.$el.find('input[name="status"]').val('working,planning');	
				this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type='+user.direction.join(',')+'&'+this.$el.find('form').serialize()});
			}
			else{
				this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/'});
				this.$el.find('input[name="status"]').val('');
			}
		},
		resetFilter: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type='+user.direction.join(',')});
			this.$el.find('input[name="status"]').val('');
		},
		initialize: function(){
		},
		templateContext: function(){
			if(user.groups.indexOf('kvs_head') != -1){
				return {show: true};
			}
		},
		editProject: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('project/'+row.id+'/edit', {trigger: true});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				valueField: 'user_id',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/managers/forselect/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					// _view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				}
			});
			this.SelectSites = this.$el.find('#SelectSites').selectize({
				valueField: 'id',
				labelField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiprojects/sites/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					// _view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				showColumns: true,
				sortName: 'date_start',
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiprojects/projects/?project_type='+user.direction.join(','),
				newQuery: {},
				onRefresh: function(options){
					// if(options.query){
					// 	this.newQuery = options.query;	
					// }
					// else{
					// 	this.newQuery = {};
					// };
				},
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('project/'+row.id+'/edit', {trigger: true});
					}
				},
				onPageChange: function(num, size){
					router.navigate('my_projects/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				toolbar: $(html).filter('#toolbar_head_type1')[0].innerHTML,
				columns: [
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'user',
					title: 'Руководитель проекта',
					sortable: true,
				},
				{
					field: 'project_type.type',
					title: 'Вид проекта',
					sortable: true,
				},
				{
					field: 'buisness_unit.name',
					title: 'Бизнес-единица',
					sortable: true,
				},
				{
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'sites_all',
					title: 'Место проведения',
					// sortable: true,
				},
				{
					field: 'count_guests',
					title: 'Количество гостей',
					sortable: true
				},
				{
					field: 'status',
					title: 'Статус',
					sortable: true,
					formatter: function(value, row, index){
						var status = {
							'planning': 'Планирование',
							'working': 'В работе',
							'canceled': 'Отменен',
							'finished': 'Завершен'
						};
						return status[value]
					}
				},
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil edit" data-id ='+
						row.id+'></span></div>'
					}
				}],
				rowAttributes: function(row, index, e){

				},
				responseHandler: function(response){
					var results = response.results;
					var sites_arr = [];
					var s = 0;
					var sum_cap = 0;
					for(i in results){
						for(j in results[i].sites){
							 sites_arr.push(results[i].sites[j].name);
							 s += parseInt(results[i].sites[j].area);
							 sum_cap += results[i].sites[j].capacity;
						}
						results[i].sites_all = sites_arr.join(', ');
						results[i].all_area = s;
						results[i].all_capacity = sum_cap;
						sites_arr = [];
						s = 0; 
						sum_cap = 0;
					};
					return response
				},
				queryParams: function(params){
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		},
	});
return ProjectsView
})
