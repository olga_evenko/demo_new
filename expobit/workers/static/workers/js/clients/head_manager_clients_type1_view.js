define(['text!workers/html/clients/clients_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal', 
	'selectize'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#head_manager_clients_view')[0].outerHTML,
		events: {
			'click #addClient': 'addClient',
			'click .filter_button_client': 'setFilter'
		},
		regions: {
		},
		className: 'displayNone',
		defaultUrl: true,
		addClient: function(){
			router.navigate('client/create', {trigger: true});
		},
		setFilter: function(e){
			var _this = this;
			if(e.currentTarget.id=='client_in_working'){
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients_type_1/?personal_manager_type_one__isnull=false&'+this.$el.find('form').serialize()});	
			}
			if(e.currentTarget.id=='key_client'){
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients_type_1/?status=key&'+this.$el.find('form').serialize()});	
			}
			if(e.currentTarget.id=='all_clients'){
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients_type_1/?'+this.$el.find('form').serialize()});
			}
		},
		resetFilter: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/projects/?project_type='+user.direction.join(',')});
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/managers/forselect/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
				}
			});
			this.SelectActivities = this.$el.find('#SelectActivities').selectize({
				valueField: 'id',
				labelField: 'subcategories',
				searchField: ['subcategories', 'value'],
				optgroupField: 'category',
				optgroupLabelField: 'display_name',
				onInitialize: function(){
					var _this=this;
					this.ItemsRemoved = [];
					this.isLoad = false;
					$.ajax({
						url: '/apiprojects/activities/',
						type: 'OPTIONS',
						cache: true,
						success: function(response){
							var optGr = response.actions.POST.category.choices;
							for (i in optGr){
								_this.addOptionGroup(optGr[i].value, optGr[i]);
							}
							$.get({
								url: '/apiprojects/activities/',
								async: true,
								success: function(response) {
									for(var i=0; i<response.length; i++){
										_this.addOption(response[i]);
									}
								}
							});
						}
					})
				},
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				pageList: [10,25,50,100,250],
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				checkedRow:{},
				toolbar: $(html).filter('#toolbar')[0].innerHTML,
				url: '/apiworkers/clients_type_1/',
				columns: [
				{
					checkbox: true
				},	
				{
					field: 'name',
					title: 'Клиент',
					sortable: true,
				}, 
				{
					field: 'personal_manager_type_one',
					title: 'Менеджер',
					editable: {
						source: '/apiworkers/managers/forselect',
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
							// contentType: false,
						    // processData: false,
						    beforeSend: function(jqXHR,settings){
						    	// settings.url += 'processing_manager_client/';
								// debugger;
							}
						},
						sourceOptions: {
							type: 'get',
							dataType: 'json'
						},
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						prepend: '',
						emptytext: 'Назначить',
						url: '/apiworkers/managers/processing_manager_client/',
						params: function(params){
							var obj = {};
							var arr_client = [];
							var check_row = $('#table').bootstrapTable('getSelections');
							_view.check_row = check_row;
							var tableRow = $('#table').bootstrapTable('getRowByUniqueId', params.pk);
							if(check_row.length>0){
								for(i in check_row){
									arr_client.push(check_row[i].id)
								}
								return {new_manager:params.value, client: arr_client}
								}
								return {new_manager:params.value, client: params.pk}
								},
								success: function(response){
									debugger;
									if(response.update){
										$('#table').bootstrapTable('refresh', {silent: true});
									};
									if(_view.check_row){
										_view.Table.bootstrapTable('checkBy', _view.check_row);
										_view.check_row = null;
									};
									_view.getManagerCount(_view.SelectActivities[0].selectize.getValue());
								}
							}
						},
						{
							field: 'address',
							title: 'Адрес'
						},
						{
							field: 'date_last_contact',
							title: 'Дата последнего контакта',
							sortable: true,
						},
						{
							field: 'date_new_contact.date',
							title: 'Дата след. контакта',
							sortable: true,
							formatter: function(value, row, index){
								return utils.dateFormatter(value);
							}
						},
						{
							field: 'date_new_contact.goal',
							title: 'Цель',
							// sortable: true,
						},  
						],
						rowStyle: function(row, index) {
							var classes = '';
							if(row.date_new_contact && row.date_new_contact.date){
								if(new Date(row.date_new_contact.date).toDateString() == new Date().toDateString()){
									classes = 'warning';
								}
								else{
									if(new Date(row.date_new_contact.date) < new Date()){
										classes = 'danger';
									}
								}
							}
							return {
								classes: classes,
								css: {"cursor": "pointer"}
							};
						},
						rowAttributes: function(row, index){
							if(this.checkedRow[row.id]){
								row[0] = this.checkedRow[row.id];
							};
						},
						onUncheckAll: function(rows){
							this.checkedRow = {};
						},
						onPageChange: function(num, size){
							router.navigate('my_clients_type_1/p'+num+'-'+size, {replace: true});
						},
						pageSize: _view.options.page_size,
						pageNumber: _view.options.page_num,
						onClickRow: function(row, e, index){
							if(index!='personal_manager_type_one'){
								router.navigate('client/'+row.id, {trigger: true});
								sessionStorage.setItem('client_project_select', _view.SelectProject[0].selectize.getValue());
							}
						},
						onCheck: function(row, $element){
							this.checkedRow[row.id] = true;
						},
						onUncheck: function(row, $element){
							delete this.checkedRow[row.id];
						},
						responseHandler: function(response){

							return response
						},
						queryParams: function(params){
							params.project_id = _view.options.project_id;
							if(params.order == 'desc'){
								params.sort = '-' + params.sort;
								return params
							}
							return params
						},
						// rowStyle: function(row, index) {
						// 	return {css:{
						// 		cursor: 'pointer'
						// 	}}
						// },
						onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
			this.SelectActivities = this.$el.find('#SelectClientActivities').selectize({
				valueField: 'id',
				labelField: 'activity',
				searchField: 'activity',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiclients/client_activity/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){

				},
			});
//			this.SelectProject = this.$el.find('#SelectProject').selectize({
//				valueField: 'id',
//				labelField: 'name',
//				searchField: 'name',
//				onInitialize: function(){
//					var _this = this;
//					$.get({
//						url: '/apiprojects/projects/?data=min',
//						success: function(response){
//							for(i in response){
//								_this.addOption(response[i]);
//							}
//						}
//					});
//				}
//			});
		},
	});
return ClientsView
})
