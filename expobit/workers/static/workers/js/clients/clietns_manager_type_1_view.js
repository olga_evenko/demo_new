define(['text!workers/html/clients/clients_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal', 
	'selectize'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#manager_clients_type_1_view')[0].outerHTML,
		events: {
			'click #addClient': 'addClient',
			'click .filter_button_client': 'setFilter'
		},
		regions: {
		},
		className: 'displayNone',
		defaultUrl: true,
		initialize: function(){
		},
		addClient: function(){
			router.navigate('client/create', {trigger: true});
		},
		setFilter: function(e){
			var id = $(e.currentTarget)[0].id;
			if(id == 'all_clients'){
				this.Table.bootstrapTable('refresh', {url: '/apiclients/clients/'});
			}
			if(id=='do_contact'){
				
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients_type_1/?personal_manager_type_one='+user.manager_id+'&contact__manager='+user.manager_id});	
				this.setClassActive(button);
			}
			if(id == 'resetfilterButton'){
				this.Table.bootstrapTable('refresh', {
					url: '/apiworkers/clients_type_1/?personal_manager_type_one='+user.manager_id
				});
			}
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				pageList: [10,25,50,100,250],
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				checkedRow:{},
				toolbar: $(html).filter('#toolbar_client_type_1')[0].innerHTML,
				url: '/apiworkers/clients_type_1/?personal_manager_type_one='+user.manager_id,
				columns: [
				{
					field: 'index',
					title: '№',
					formatter: function(value, row, index, field){
						if(row['Дата'] != 'Итого'){
							return index + 1;
						}
						return null;
					}
				},
				{
					field: 'name',
					title: 'Клиент',
					sortable: true,
				}, 
				{
					field: 'status',
					title: 'Статус',
					formatter: function(value, row, index){
						var groups = {
							'active': 'Активный',
							'black_list': 'Черный список',
							'inactive': 'Неактивный',
							'key': 'Ключевой'
						};
						return groups[value];
					}
				},
				{
					field: 'address',
					title: 'Адрес'
				},
				{
					field: 'date_last_contact',
					title: 'Дата последнего контакта',
					sortable: true,
				}, 
				{
					field: 'date_new_contact.date',
					title: 'Дата след. контакта',
					sortable: true,
					formatter: function(value, row, index){
						return utils.dateFormatter(value);
					}
				},
				{
					field: 'date_new_contact.goal',
					title: 'Цель'
				}
				],
				onPageChange: function(num, size){
					router.navigate('my_clients_manager_type_1/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('client/'+row.id, {trigger: true});
						// sessionStorage.setItem('client_project_select', _view.SelectProject[0].selectize.getValue());
					}
				},
				responseHandler: function(response){

					return response
				},
				queryParams: function(params){
					params.project_id = _view.options.project_id;
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				rowStyle: function(row, index) {
					var classes = '';
					if(row.date_new_contact && row.date_new_contact.date){
						if(new Date(row.date_new_contact.date).toDateString() == new Date().toDateString()){
							classes = 'warning';
						}
						else{
							if(new Date(row.date_new_contact.date) < new Date()){
								classes = 'danger';
							}
						}
					}
					return {
						classes: classes,
						css: {"cursor": "pointer"}
					};
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		},
	});
return ClientsView
})
