define(['text!workers/html/clients/clients_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#one_c_client_type_one_view')[0].outerHTML,
		events: {
			'click #getOneC': 'getOneC',
		},
		className: 'displayNone',
		templateContext: function(){
			var show_manager = false;
			var show_date = false;
			// if(user.groups.indexOf('project_managers') != -1 || user.isSuperuser){
			// 	show_manager = true;
			// }
			if(user.isSuperuser){
				show_date = true;
			}
			return {show_manager: show_manager, show_date: true};
		},
		initialize: function(){
		},
		getOneC: function(){
			var url = this.url + '&' + this.$el.find('form').serialize();
			// if(this.SelectManager[0].selectize.getValue() == ''){
				if(user.groups.indexOf('kvs_head') != -1){
					url += '&show_all=true';	
				}
				else{
					url += '&show_all=true&manager_id='+user.manager_id;
				}
			// }
			this.Table.bootstrapTable('refresh', {
				url: url
			});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.url = '/apiclients/clients/one_c_data/?manager_id='+user.manager_id;
			if(user.groups.indexOf('kvs_head') != -1){
				this.url = '/apiclients/clients/one_c_data/?head=true';
			}
			this.SelectType = this.$el.find('#SelectType').selectize({
				options: [{text: 'Счета', value: 'bill'}, {text: 'Оплаты', value: 'pay'}],
				onInitialize: function(){
					this.setValue('bill');
				}
			});
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/managers/forselect/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/projects/?all=true',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'ГУИД_счета',
				uniqueId: 'ГУИД_счета',
				// pagination: true,
				search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: this.url,
				columns: [
				{
					field: 'index',
					title: '№',
					formatter: function(value, row, index, field){
						if(row['Дата'] != 'Итого'){
							return index + 1;
						}
						return null;
					}
				},
				{
					field: 'Дата',
					title: 'Дата',
					sortable: true
				},
				{
					field: 'Номер',
					title: '№',
				},
				{
					field: 'project_name',
					title: 'Проект',
					width: '170px'
				},
				{
					field: 'client_name',
					title: 'Клиент',
				},
				{
					field: 'Контрагент',
					title: 'Контрагент',
				},
				{
					field: 'manager_name',
					title: 'Менеджер',
				},
				{
					field: 'ДатаОплаты',
					title: 'Дата посл. опл.',
					sortable: true
				},
				{
					field: 'ОплаченнаяСумма',
					title: 'Оплаченная сумма',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'НеоплаченнаяСумма',
					title: 'Неоплаченная сумма',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'СуммаСчета',
					title: 'Сумма',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				}
				],
				customSort: function(sortName, sortOrder){
					if(sortName){
						var d = this.data.splice(-1,1);
					}
					if(sortName == 'Дата' || sortName == 'ДатаОплаты'){
						this.data.sort(function(a,b){
							var arra = a['Дата'].split(' ');
							var arrb = b['Дата'].split(' ');
							a = arra[0].split('.')[2] +'-'+ arra[0].split('.')[1] +'-'+ arra[0].split('.')[0] + ' ' + arra[1];
							b = arrb[0].split('.')[2] +'-'+ arrb[0].split('.')[1] +'-'+ arrb[0].split('.')[0] + ' ' + arrb[1];
							a = new Date(a);
							b = new Date(b);
							if(sortOrder == 'asc'){
								return a>b ? -1 : a<b ? 1 : 0;
							}
							return a>b ? 1 : a<b ? -1 : 0;
						});
						this.data.push(d[0]);
						return;
					}
					
					this.data.sort(function(a,b){
						if(sortOrder == 'asc'){
							return parseInt(a[sortName])>parseInt(b[sortName]) ? -1 : parseInt(a[sortName])<parseInt(b[sortName]) ? 1 : 0;
						}
						return parseInt(a[sortName])>parseInt(b[sortName]) ? 1 : parseInt(a[sortName])<parseInt(b[sortName]) ? -1 : 0;
					});
					if(sortName){
						this.data.push(d[0]);
					}
		            // Сортируем логику здесь.
		            // Для сортировки данных вы должны использовать массив `this.data`. НЕТ использовать `this.options.data`.
		        },
				rowStyle: function(row, index){
					if(row['Дата'] == 'Итого'){
						return{
							classes: 'info'
						};
					}
					return{
						classes: ''
					};
				},
				responseHandler: function(response){
					var sum=0, pay=0, notpay=0;
					for(var i in response){
						sum = sum + parseInt(response[i]['СуммаСчета']);
						pay = pay + parseInt(response[i]['ОплаченнаяСумма']);
						notpay = notpay + parseInt(response[i]['НеоплаченнаяСумма']);
					}
					response.push({
						'ОплаченнаяСумма': pay,
						'НеоплаченнаяСумма': notpay,
						'СуммаСчета': sum,
						'Дата': 'Итого'
					});
					return response;
				}
			});
		}
	});
	return View;
});
