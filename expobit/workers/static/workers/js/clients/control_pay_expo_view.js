define(['text!workers/html/clients/clients_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#one_c_client_view')[0].outerHTML,
		events: {
			'click #getOneC': 'getOneC',
			'click #getOneCByProject': 'getOneCByProject'
		},
		className: 'displayNone',
		templateContext: function(){
			var show_manager = false;
			var show_date = false;
			if(user.groups.indexOf('project_managers') != -1 || user.isSuperuser){
				show_manager = true;
			}
			if(user.isSuperuser || user.groups.indexOf('project_managers') != -1){
				show_date = true;
			}
			return {show_manager: show_manager, show_date: show_date};
		},
		initialize: function(){
			this.arr_w = [];
		},
		getOneC: function(){
			// this.Table.bootstrapTable('refresh', {
			// 	url: this.url + '&project='+this.SelectProject[0].selectize.getValue()+'&manager='+this.SelectManager[0].selectize.getValue()
			// });
			var url = this.url + '&' + this.$el.find('form').serialize();
			if(user.groups.indexOf('head_expo') != -1 && this.SelectManager[0].selectize.getValue() == ''){
				url += '&show_all=true' ;
			}
			this.Table.bootstrapTable('refresh', {
				url: url
			});
		},
		getOneCByProject: function(){
			this.Table.bootstrapTable('refresh', {
				url: this.url + '&show_all=true' + '&project='+this.SelectProject[0].selectize.getValue()+'&manager='+this.SelectManager[0].selectize.getValue()
			});	
		},
		resizeTable: function(){
			// var arr_w = [];
			var _view = this;
			if(this.arr_w.length == 0){
				var arr = this.$el.find('tbody tr[data-index="0"]').find('td').toArray();
				for(var i in arr){
					this.arr_w.push(arr[i].offsetWidth);
				}
			}
			$(this.$el.find('thead')[0]).find('th').each(function(e){$(this).find('.fht-cell').width(_view.arr_w[e])});
			$(this.$el.find('thead')[1]).find('th').each(function(e){$(this).find('.fht-cell').width(_view.arr_w[e])});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			var resizeTimer;
			$(window).resize(function(){
				clearTimeout(resizeTimer);
				resizeTimer = setTimeout(function() {
					_view.resizeTable();
				}, 250);
			});
			this.url = '/apiclients/clients/one_c_data/?type=bill&manager_id='+user.manager_id;
			if(user.groups.indexOf('projects_manager') != -1){
				this.url = '/apiclients/clients/one_c_data/?type=bill&head=true';
			}
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/managers/forselect/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/projects/?all=true',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				}
			});
			this.SelectBarter = this.$el.find('#SelectBarter').selectize({
				options: [{text: 'Все', value: ''},{text: 'Только бартер', value: '1'}, {text: 'Без бартера', value: '0'}],
				onInitialize: function(){
					this.setValue('');
				}
			});
			this.SelectType = this.$el.find('#SelectType').selectize({
				options: [{text: 'Счета', value: 'bill'}, {text: 'Оплаты', value: 'pay'}, {text: 'Плановые поступления', value: 'plan'}],
				onInitialize: function(){
					this.setValue('bill');
				}
			});
			
			$.get({
				url: this.url,
				success: function(response){
					var sum=0, pay=0, notpay=0;
					for(var i in response){
						sum = sum + parseInt(response[i]['СуммаСчета']);
						pay = pay + parseInt(response[i]['ОплаченнаяСумма']);
						notpay = notpay + parseInt(response[i]['НеоплаченнаяСумма']);
					}
					response.push({
						'ОплаченнаяСумма': pay,
						'НеоплаченнаяСумма': notpay,
						'СуммаСчета': sum,
						'Дата': 'Итого'
					});
					_view.Table.bootstrapTable('load', response);
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'ГУИД_счета',
				uniqueId: 'ГУИД_счета',
				// pagination: true,
				search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				height: 600,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// url: this.url,
				columns: [
				{
					field: 'index',
					title: '№',
					formatter: function(value, row, index, field){
						if(row['Дата'] != 'Итого'){
							return index + 1;
						}
						return null;
					}
				},
				{
					field: 'Дата',
					title: 'Дата',
					sortable: true
				},
				{
					field: 'Номер',
					title: '№',
				},
				{
					field: 'project_name',
					title: 'Проект',
					width: '170px'
				},
				{
					field: 'client_name',
					title: 'Клиент',
				},
				{
					field: 'Контрагент',
					title: 'Контрагент',
				},
				{
					field: 'manager_name',
					title: 'Менеджер',
				},
				{
					field: 'ДатаОплаты',
					title: 'Дата посл. опл.',
					sortable: true
				},
				{
					field: 'СуммаПоследнейОплаты',
					title: 'Сумма посл. опл.',
					sortable: true
				},
				{
					field: 'ОплаченнаяСумма',
					title: 'Оплаченная сумма',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'НеоплаченнаяСумма',
					title: 'Неоплаченная сумма',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'СуммаСчета',
					title: 'Сумма',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'Площадь',
					title: 'Площадь',
					sortable: true,
				},
				{
					field: 'РегВзнос',
					title: 'Рег. Взнос',
					sortable: true,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				}
				],
				customSort: function(sortName, sortOrder){
					if(sortName){
						var d = this.data.splice(-1,1);
					}
					if(sortName == 'Дата' || sortName == 'ДатаОплаты'){
						this.data.sort(function(a,b){
							var arra = a[sortName].split(' ');
							var arrb = b[sortName].split(' ');
							a = arra[0].split('.')[2] +'-'+ arra[0].split('.')[1] +'-'+ arra[0].split('.')[0] + ' ' + arra[1];
							b = arrb[0].split('.')[2] +'-'+ arrb[0].split('.')[1] +'-'+ arrb[0].split('.')[0] + ' ' + arrb[1];
							a = new Date(a);
							b = new Date(b);
							if(sortOrder == 'asc'){
								return a>b ? -1 : a<b ? 1 : 0;
							}
							return a>b ? 1 : a<b ? -1 : 0;
						});
						this.data.push(d[0]);
						return;
					}
					
					this.data.sort(function(a,b){
						if(sortOrder == 'asc'){
							return parseInt(a[sortName])>parseInt(b[sortName]) ? -1 : parseInt(a[sortName])<parseInt(b[sortName]) ? 1 : 0;
						}
						return parseInt(a[sortName])>parseInt(b[sortName]) ? 1 : parseInt(a[sortName])<parseInt(b[sortName]) ? -1 : 0;
					});
					if(sortName){
						this.data.push(d[0]);
					}
		            // Сортируем логику здесь.
		            // Для сортировки данных вы должны использовать массив `this.data`. НЕТ использовать `this.options.data`.
		        },
				rowStyle: function(row, index){
					// if(row['МеткаБартер'] && row.app_status == 'finished' && row["НеоплаченнаяСумма"] != "0"){
					// 	row["ОплаченнаяСумма"] = row["НеоплаченнаяСумма"];
					// 	row["СуммаПоследнейОплаты"] = row["НеоплаченнаяСумма"];
					// 	row["НеоплаченнаяСумма"] = '0';
					// }
					if(row["МеткаБартер"]){
						return {
							classes: '',
							css: {
								'background-color': 'rgba(38, 185, 154, 0.34)'
							}
						};
					}
					if(row['Дата'] == 'Итого'){
						return{
							classes: 'info'
						};
					}
					return{
						classes: ''
					};
				},
				onLoadSuccess: function(data){
					setTimeout(function(){
						_view.resizeTable();
					}, 1);
				},
				onSort: function(){
					setTimeout(function(){
						_view.resizeTable();
					}, 250);
				},
				responseHandler: function(response){
					var sum=0, pay=0, notpay=0;
					for(var i in response){
						if(response[i]['МеткаБартер'] && response[i].app_status == 'finished' && response[i]["НеоплаченнаяСумма"] != "0"){
							response[i]["ОплаченнаяСумма"] = response[i]["НеоплаченнаяСумма"];
							response[i]["СуммаПоследнейОплаты"] = response[i]["НеоплаченнаяСумма"];
							response[i]["НеоплаченнаяСумма"] = '0';
						}
						sum = sum + parseInt(response[i]['СуммаСчета']);
						pay = pay + parseInt(response[i]['ОплаченнаяСумма']);
						notpay = notpay + parseInt(response[i]['НеоплаченнаяСумма']);
					}
					response.push({
						'ОплаченнаяСумма': pay,
						'НеоплаченнаяСумма': notpay,
						'СуммаСчета': sum,
						'Дата': 'Итого'
					});
					return response;
				}
			});
		// _view.$el.on('DOMSubtreeModified','tr',function(e){
		// 	debugger
		// 		_view.Table.bootstrapTable('resetView');
		// 	});
		}
	});
	return View;
});
