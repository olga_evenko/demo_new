define(['text!workers/html/main_view.html', 'utils', 
	'bootstrap-table-editable', 'bootstrap-table-locale', 
	'selectize', 'bootstrap-table-export'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#main_contact_report_view')[0].outerHTML,
		events: {
			'click #getReport': 'getReport',
			'click .load_pdf': 'loadPDF',
			'click #show_all': 'showAll',
			'click #show_not_null': 'showNotNull'
		},
		className: 'displayNone',
		initialize: function(){
		},
		getReport: function(e){
			var date_gt = this.$el.find('input[name="date_gt"]').val();
			var date_lt = this.$el.find('input[name="date_lt"]').val();
			var value = this.SelectProject[0].selectize.getValue();
			this.Table.bootstrapTable('refresh', {
				url: '/apiworkers/report_contact/?membership__project__in='+value+'&date_gt='+date_gt+'&date_lt='+date_lt
			});
			router.navigate('report_contact' + '/project' + value, {replace: true});
		},
		loadPDF: function(e){
			$.ajax({
				type: 'POST',
				url: '/report/html_to_pdf/',
				contentType: 'application/json',
				data: JSON.stringify({html: this.$el.find('#table')[0].outerHTML}),
				success: function(response){
					window.open('/report/html_to_pdf/?guid='+response.guid);
				},
				error: function(response){
					utils.notyError().show();
				}

			});
		},
		showAll: function(e){
			this.Table.bootstrapTable('load', this.all_data);
		},
		showNotNull: function(e){
			this.Table.bootstrapTable('load', this.not_null_data);
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				totalField: 'count',
				showExport: true,
				exportTypes: ['excel'],
				// sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				toolbar: $(html).filter('#toolbar-report-contact')[0].innerHTML,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// url: '/apiworkers/clients/',
				// onPageChange: function(num, size){
				// 	router.navigate('my_clients/p'+num+'-'+size+'/project'+_view.SelectProject[0].selectize.getValue(), {replace: true});
				// },
				// pageSize: _view.options.page_size,
				// pageNumber: _view.options.page_num,
				// onClickRow: function(row, e, index){
				// 	if(index!='edit'){
				// 		router.navigate('client/'+row.id, {trigger: true});
				// 	}
				// },
				// rowStyle: function(row, index) {
				// 	return {
				// 		css: {"cursor": "pointer"}
				// 	};
				// },
				columns: [
				{
					field: 'name',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'count_contact',
					title: 'Количество контактов',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'success',
						};
					}
					// sortable: true
				},
				{
					field: 'in_contact.first_in',
					title: 'Вх. зв. перв.',
				},
				{
					field: 'in_contact.repeat_in',
					title: 'Вх. зв. повт.',
				},
				{
					field: 'in_contact.first_out',
					title: 'Исх. зв. перв.',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'success',
						};
					}
				},
				{
					field: 'in_contact.repeat_out',
					title: 'Исх. зв. повт.',
				},
				{
					field: 'in_contact.meet',
					title: 'Встреча',
				},
				{
					field: 'in_contact.email',
					title: 'Электронная переписка',
				},
				{
					field: 'result.work',
					title: 'В работе',
					cellStyle: function(value, row, index, field){
						if(row.name != 'Итого'){
							return {
								css: {'background-color': '#f0ad4e'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'result.interest',
					title: 'Интерес',
				},
				{
					field: 'result.app',
					title: 'Заявка',
					cellStyle: function(value, row, index, field){
						if(row.name != 'Итого'){
							return {
								css: {'background-color': '#f0ad4e'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'result.fail',
					title: 'Отказ',
				},
				{
					field: 'result.interest_other',
					title: 'Интерес на другие проекты',
				},
				{
					field: 'fact',
					title: 'Полученные ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				],
				rowStyle: function(row, index) {
					if(row.name == 'Итого'){
						return {
							classes: 'info'
						};	
					};
					return {classes: ''}
					
				},
				responseHandler: function(response){
					var null_result = false;
					_view.all_data = response;
					_view.not_null_data = [];
					for(i in response){
						null_result = true;
						if(response[i].fact || response[i].count_contact){
							null_result = false;
						}	
						for(k in response[i].result){
							if(response[i].result[k]){
								null_result = false;
							}
						}
						for(k in response[i].in_contact){
							if(response[i].in_contact[k]){
								null_result = false;
							}
						}
						if(!null_result){
							_view.not_null_data.push(response[i]);
						}
					}
					return response
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/?all=true',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							if(_view.options.project){
								_this.setValue(_view.options.project);
							};
						}
					});
				},
				onChange: function(value){
					// var date_gt = _view.$el.find('input[name="date_gt"]').val();
					// var date_lt = _view.$el.find('input[name="date_lt"]').val();
					// _view.Table.bootstrapTable('refresh', {
					// 	url: '/apiworkers/report_contact/?membership__project='+value+'&date_gt='+date_gt+'&date_lt='+date_lt
					// });
					// router.navigate('report_contact' + '/project' + value, {replace: true});
				},
			});
		}
	});
	return ClientsView
})
