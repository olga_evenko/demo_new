define(['text!workers/html/modal_view.html'], function(html){
	
	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#modal_message_view')[0].outerHTML,
		events: {
			'click #affixed_object': 'AffixedObject',
			'click #removeButton': 'removeAffixed'
		},
		// className: 'panel panel-default form',
		initialize: function(){
			this.objects = {
				'#client': {
					name: 'Прикрепить текущего клиента',
					field: 'name'
				},
				'#entity': {
					name: 'Прикрепить текущее юр. лицо',
					field: 'name'
				},
				'#individual': {
					name: 'Прикрепить текущее физ. лицо',
					field: 'name'
				},
				'#project': {
					name: 'Прикрепить текущий проект',
					field: 'name'
				},
				'#app': {
					name: 'Прикрепить текущую заявку',
					field: 'date'
				}
			};
			this.url_type = location.hash.match(/#\w+/)[0];
			this.enableButton = false;
			if(this.objects[this.url_type]){
				this.enableButton = true;
			}
		},
		removeAffixed: function(e){
			$(e.currentTarget).parent().remove();
		},
		AffixedObject: function(e){
			var current_model = mainView.getRegion('MainContent').currentView.model;
			this.$el.find('#list_attachments ul').append('<li>\
				<a href="/'+this.url_type+'/'+current_model.id+'">'+current_model.get(this.objects[this.url_type].field)+'</a>\
				<span style="color: red;" id="removeButton" class="glyphicon glyphicon-minus"></span>\
        		</li>');
		},
		onRender: function(){
			var _this = this;
			if(!this.enableButton){
				this.$el.find('#affixed_button').attr('disabled', true);
			}
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				valueField: 'user_id',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselectmessage/',
						success: function(response){
							for(i in response){
								if(response[i].user_id != user.id || user.isSuperuser){
									_this.addOption(response[i]);
								}
							}
						}
					});
				},
				onChange: function(value){
				},
			});
		},
	});
return View;
});
