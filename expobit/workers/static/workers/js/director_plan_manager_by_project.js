define(['text!workers/html/main_view.html', 
	'utils',
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'bootstrap-modal'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#modal_managers_table_view')[0].outerHTML,
		events: {
		},
		regions: {
		},
		// className: 'displayNone',
		templateContext: function(){
			return {
				month: this.options.month
			};
		},
		initialize: function(){
		},
		onRender: function(){
			var _this = this;
			// this.$el.find(document).ready(function($) {
			// 	_this.$el.fadeIn(1000);
			// });
			this.Table = this.$el.find('#table_managers').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiprojects/report_managers/table/?report='+_this.options.report,
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },

				rowStyle: function(row, index) {
					if(row.manager == 'Итого'){
						return {
							classes: 'info'
						};	
					}
					return {classes: ''};
					
				},
				columns: [
				{
					field: 'manager.manager',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						};
					}
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок на весь период'
				},
				{
					field: 'perc_comp',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						};
					}
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						};
					}
				},
				],
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				},
				responseHandler: function(response){
					if(response){
						response[0].manager = {manager: 'Итого'};
					}
					return response;
				},
			});

		}
	});
	return ProjectReportsView
})
