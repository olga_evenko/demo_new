define(['text!workers/html/main_view.html', 'utils', 'selectize'], function(html, utils){
		
		var MessageView = Backbone.Marionette.View.extend({
			template:$(html).filter('#import_csv_view')[0].outerHTML,
			events: {
				'click #load': 'loadFile',
			},
			className: 'panel panel-default form',
			initialize: function(){
			},
			loadFile: function(){
				var _view = this;
				$.ajax({
					type: 'POST',
					url: '/apiworkers/load_csv',
					contentType: false,
					processData: false,
					data: new FormData(this.$el.find('form')[0]),
					success: function(response){
						utils.notySuccess('Данные загружены').show();
						if(response.error.length != 0){
							_view.$el.append('Ошибки основной загрузки: '+response.error.toString());
						}
						if(response.error_rel.length != 0){
							_view.$el.append(' Ошибки зависимых: '+response.error_rel.toString());
						}
					},
					error: function(response){
						utils.notySuccess('Ошибка загрузки').show();
					}
				});
			},
			onRender: function(){
				var _view = this;
				this.$el.find(document).ready(function($) {
					_view.$el.fadeIn();
				});
				this.SelectModel = this.$el.find('#SelectModel').selectize({
					labelField: 'model',
					valueField: 'id',
					searchField: 'model',
					onInitialize: function(){
						var _this = this;
						$.get({
							url: '/apiworkers/content_type/',
							success: function(response){
								for(var i in response){
									_this.addOption(response[i]);
								}
							},
						});
					},
					onChange: function(value){
						var val = this.options[value];
						for(var i in val){
							_view.SelectField[0].selectize.addOption(val[i]);
						}
					}
				});
				this.SelectField = this.$el.find('#SelectField').selectize({
					labelField: 'name',
					valueField: 'name'
				});
				this.SelectEncode = this.$el.find('#SelectEncode').selectize({
					options: [{text: 'CP-1251', value: 'cp1251'}, {text: 'UTF-8', value: 'utf8'}]
				});
			},
		});
		return MessageView;
	});
