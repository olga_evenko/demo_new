define(['text!workers/html/main_view.html', 'utils', 'bootstrap-table-editable', 
	'bootstrap-table-locale', 'selectize', 'bootstrap-modal'], function(html, utils){

	var ReportHistoryContactsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#report_updated_client_view')[0].outerHTML,
		events: {
			'click #getReport': 'getReport'
		},
		className: 'displayNone',
		initialize: function(){
		},
		getReport: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiworkers/report_updated_client/?'+this.$el.find('form').serialize()});
		},
		openModal: function(url, title, filter){
			var _this=this;
			// Пробное место, потом почистить и перенести работу с кэшем в app.js
			// window.viewscache = {modals: {}};
			require(['workers/js/modals/clients_view_for_report_updated'], function(ClientsView){
				var modal = new Backbone.BootstrapModal({
					content: new ClientsView({url: url, filter: filter}), 
					title: title,
					okText: 'Отправить',
					cancelText: 'Отмена',
					animate: true,
					okCloses: false
				}).open();
				utils.addWindow(modal, 'modals', title);
				// window.viewscache.modals[modal.cid] = modal;
				modal.on('ok', function(){
					if(this.options.content.SelectManager[0].selectize.getValue()==''){
						utils.notyAlert('Выберите получателя').show();
					}
					else{
						var serialize = this.$content.find('form').serializeArray();
						var data = utils.arraytoJson(serialize);
						data.filter = this.options.content.options.filter;
						$.ajax({
							url: '/apiworkers/messages/manager_report/',
							type: 'POST',
							data: JSON.stringify(data),
							contentType: 'application/json',
							// processData: false,
							success: function(response){
								utils.notySuccess('Сообщение отправлено').show();
								modal.close();
								utils.removeWindow(modal, 'modals');
							},
							error: function(response){
								utils.notyError('Ошибка отправления').show();
							}
						});
					};
				});
				modal.on('cancel', function(e){
					utils.removeWindow(this, 'modals');
				});
			});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				columns: [
				{
					field: 'all',
					title: 'Всего клиентов',
					// sortable: true,
				},
				{
					field: 'no_contact.value',
					title: 'Нет контактных данных',
					onClickCell: function(field, value, row, $element){
						
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'cursor': 'pointer'}
						};
					}
				},
				{
					field: 'edited',
					title: 'Редактировано',
				},
				{
					field: 'created.value',
					title: 'Создано новых',
					onClickCell: function(field, value, row, $element){
						debugger;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'cursor': 'pointer'}
						};
					}
				},
				{
					field: 'to_inactive.value',
					title: 'Переведены в неактивные',
					onClickCell: function(field, value, row, $element){
						
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: '',
							css: {'cursor': 'pointer'}
						};
					}

				}],
				onClickCell: function(field, value, row, $element){
					switch (field) {
						case 'no_contact.value':
							_view.openModal(row[field.split('.')[0]].url, 'Клиенты без контактных данных', row[field.split('.')[0]].filter);
							break;
						case 'created.value':
							_view.openModal(row[field.split('.')[0]].url, 'Новые созданные клиенты', row[field.split('.')[0]].filter);
							break;
						case 'to_inactive.value':
							_view.openModal(row[field.split('.')[0]].url, 'Клиенты переведенные в неактивные', row[field.split('.')[0]].filter);
							break;
						default:
							// statements_def
							break;
					};
				},
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							// if(_view.options.project){
							// 	_this.setValue(_view.options.project);
							// };
						}
					});
				},
				onChange: function(value){
					_view.$el.find('#getReport').attr('disabled', false);
				},
			});
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselect/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							// if(_view.options.project){
							// 	_this.setValue(_view.options.project);
							// };
						}
					});
				},
				onChange: function(value){
				},
			});
		}
	});
	return ReportHistoryContactsView
})
