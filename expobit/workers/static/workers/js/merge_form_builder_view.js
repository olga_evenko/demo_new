define(['text!workers/html/main_view.html', 'utils', 'selectize','bootstrap-table-locale'], function(html, utils){
		
		var View = Backbone.Marionette.View.extend({
			template:$(html).filter('#form_builder_view')[0].outerHTML,
			events: {
				'click #search': 'search',
				'click .addVal': 'addVal',
				'click .cancelVal': 'cancelVal',
				'input input': 'changeModel'
			},
			className: 'panel panel-default form',
			initialize: function(){
				if(this.options.region == 0){
					this.addSpan = '<span class="glyphicon glyphicon-ok addVal" style="\
	    				color: #18bc9c; top: 25px; position: absolute; right: -25px; display: none;"></span>';
	    			this.cancelSpan = '<span class="glyphicon glyphicon-remove cancelVal" style="\
	    				color: #e74c3c; position: absolute; top: 45px; right: -25px; display: none;"></span>';
				}
				else{
					this.addSpan = '<span class="glyphicon glyphicon-ok addVal" style="\
	    				color: #18bc9c; top: 25px; position: absolute; left: -25px; display: none;"></span>';
	    			this.cancelSpan = '<span class="glyphicon glyphicon-remove cancelVal" style="\
	    				color: #e74c3c; position: absolute; top: 45px; left: -25px; display: none;"></span>';
				}
				if(this.options.main){
					this.old_model = new Backbone.Model(this.model.attributes);
				}
				this.local_exclude = [];
				this.local_addition = [];
			},
			addVal: function(e){
				this.options.addition.push($(e.currentTarget).parent().data().field);
				this.local_addition.push($(e.currentTarget).parent().data().field);
				this.updateMain($(e.currentTarget).parent().data().field);
				mainView.getRegion('MainContent').currentView.updateSpanGlobal();
				// this.updateSpan();
			},
			updateMain: function(field){
				var table = this.main_view.$el.find('#'+field);
				if(table.length > 0){
					var options = table.bootstrapTable('getOptions');
					if(options.columns[0].length == 0){
						table.bootstrapTable('refreshOptions', this.$el.find('#'+field).bootstrapTable('getOptions'));
						this.main_view.model.set(field, table.bootstrapTable('getData'));
						this.$el.find('#'+field).parent().parent().parent().parent().find('span').fadeOut();
						var span = this.main_view.$el.find('div[data-field="'+field+'"]').append(this.cancelSpan);
						span.find('span').fadeIn();
						return;
					}
					table.bootstrapTable('append', this.$el.find('#'+field).bootstrapTable('getData'));
					this.main_view.model.set(field, table.bootstrapTable('getData'));
					this.$el.find('#'+field).parent().parent().parent().parent().find('span').fadeOut();
					var span = this.main_view.$el.find('div[data-field="'+field+'"]').append(this.cancelSpan);
					span.find('span').fadeIn();
					return;
				}
				this.main_view.$el.find('input[name="'+field+'"]').val(this.$el.find('input[name="'+field+'"]').val());
				this.main_view.$el.find('input[name="'+field+'"]').trigger('input');
				var span = this.main_view.$el.find('input[name="'+field+'"]').parent().append(this.cancelSpan);
				span.find('span').fadeIn();
			},
			cancelVal: function(e){
				if(this.options.main){
					var span = $(e.currentTarget);
					var index;
					if(span.parent().find('table').length > 0){
						span.parent().find('table').bootstrapTable('load', this.old_model.get(span.parent().data().field));
						this.model.set(span.parent().data().field, this.old_model.get(span.parent().data().field));
						span.fadeOut();
						index = this.options.exclude.indexOf(span.parent().data().field);
						if (index > -1) {
							this.options.exclude.splice(index, 1);
							this.local_exclude.splice(index, 1);
						}
						index = this.options.addition.indexOf(span.parent().data().field);
						if (index > -1) {
							this.options.addition.splice(index, 1);
							this.local_addition.splice(index, 1);
						}
						span.remove();
						mainView.getRegion('MainContent').currentView.updateSpanGlobal();
						return;
					}
					span.parent().find('input').val(this.old_model.get(span.parent().data().field));
					span.fadeOut();
					this.model.set(span.parent().data().field, this.old_model.get(span.parent().data().field));
					index = this.options.exclude.indexOf(span.parent().data().field);
					if (index > -1) {
						this.options.exclude.splice(index, 1);
						this.local_exclude.splice(index, 1);
					}
					index = this.options.addition.indexOf(span.parent().data().field);
					if (index > -1) {
						this.options.addition.splice(index, 1);
						this.local_addition.splice(index, 1);
					}
					span.remove();
					mainView.getRegion('MainContent').currentView.updateSpanGlobal();
					return;
				}
				this.options.exclude.push($(e.currentTarget).parent().data().field);
				this.local_exclude.push($(e.currentTarget).parent().data().field);
				this.updateSpan();
			},
			changeModel: function(e){
				if(this.options.main){
					var input = $(e.currentTarget);
					this.model.set(input.attr('name'), input.val());
				}
			},
			compareArray: function(arr1, arr2){
				if(arr1.length != arr2.length){
					return false;
				}
				for(var i in arr1){
					if(typeof(arr1[i]) == 'object'){
						if(JSON.stringify(arr1[i]) !== JSON.stringify(arr2[i]) ){
							return false;
						}
						continue;
					}
					if(arr1[i] !== arr2[i]){
						return false;
					}
				}
				return true;
			},
			updateSpan: function(){
				var attrs = this.main_view.model.attributes;
				var curr_attrs = this.model.attributes;
				for(var i in attrs){
					this.$el.find('div[data-field="'+i+'"]').removeClass('has-success');
					this.$el.find('div[data-field="'+i+'"]').css('border', '');
					if(curr_attrs[i] && curr_attrs[i].constructor == Array){
						if(this.compareArray(curr_attrs[i], attrs[i])){
							this.$el.find('div[data-field="'+i+'"]').find('span').fadeOut();
							this.$el.find('div[data-field="'+i+'"]').css('border', '2px solid #18bc9c');
							
						}
						else if(!this.compareArray(curr_attrs[i], attrs[i]) && this.options.addition.indexOf(i) != -1){
							// 2px solid #18bc9c
							this.$el.find('div[data-field="'+i+'"]').find('span').fadeOut();
							// this.$el.find('div[data-field="'+i+'"]').addClass('has-success');
						}
						else if(!this.compareArray(curr_attrs[i], attrs[i]) && this.options.exclude.indexOf(i) != -1){
							// 2px solid #18bc9c
							this.$el.find('div[data-field="'+i+'"]').find('span').fadeOut();
							// this.$el.find('div[data-field="'+i+'"]').addClass('has-success');
						}
						else{
							this.$el.find('div[data-field="'+i+'"]').find('span').fadeIn();
						}
						continue;
					}
					if(curr_attrs[i] != attrs[i] && this.options.exclude.indexOf(i) == -1 && this.options.addition.indexOf(i) == -1){

						this.$el.find('div[data-field="'+i+'"]').find('span').fadeIn();
					}
					else if(curr_attrs[i] != attrs[i] && this.options.addition.indexOf(i) != -1){
						// 2px solid #18bc9c
						this.$el.find('div[data-field="'+i+'"]').find('span').fadeOut();
						// this.$el.find('div[data-field="'+i+'"]').addClass('has-success');
					}
					else if(curr_attrs[i] != attrs[i] && this.options.exclude.indexOf(i) != -1){
						// 2px solid #18bc9c
						this.$el.find('div[data-field="'+i+'"]').find('span').fadeOut();
						// this.$el.find('div[data-field="'+i+'"]').addClass('has-success');
					}
					else{
						this.$el.find('div[data-field="'+i+'"]').find('span').fadeOut();
						this.$el.find('div[data-field="'+i+'"]').addClass('has-success');
					}
				}
			},
			templateContext: function(){
				return {region: this.options.region};
			},
			addTable: function(table_id, data){
				var options = [];
				if(data.length>0){
					for(var i in data[0]){
						options.push({
							title: i,
							field: i
						});
					}
				}
				this.$el.find(table_id).bootstrapTable({
					idField: 'id',
					uniqueId: 'id',
					classes: 'table table-hover table-bordered',
					columns: options,
					height: 250,
					data: data
				});
			},
			addHTML: function(){
				var attrs = this.model.attributes;
				var span;
				for(var i in attrs){
					if(this.options.main){
						span = '';
					}
					else{
						span = this.addSpan+this.cancelSpan;
					}
					if(attrs[i] && typeof(attrs[i]) == 'object' && attrs[i].constructor == Array){
						this.$el.find('#table_block').append('<div data-field="'+i+'" style="position: relative; margin-top: 20px; padding-bottom: 20px;"><label>'+i+'</label><table id="'+i+'"></table>'+span+'</div>');
						this.addTable('#'+i+'', attrs[i]);
						continue;
					} 
					if(i == 'id'){
						this.$el.find('form').append('<div data-field="'+i+'" class="form-group"><label>'+i+'</label><input class="form-control input-sm" type="text" name="'+i+'" readonly value="'+attrs[i]+'"></div>');
						continue;
					}
					if(i.indexOf('user_name_') != -1){
						this.$el.find('form').append('<div data-field="'+i+'" class="form-group"><label>'+i+'</label><input class="form-control input-sm" type="text" readonly value="'+attrs[i]+'"></div>');
						continue;
					}
					this.$el.find('form').append('<div data-field="'+i+'" style="position: relative;" class="form-group"><label>'+i+'</label><input class="form-control input-sm" name="'+i+'" type="text" value="'+attrs[i]+'">'+span+'</div>');
				}
			},
			onRender: function(){
				var _view = this;
				this.$el.find(document).ready(function($) {
					_view.$el.fadeIn();
				});
				this.addHTML();
				// debugger
				// this.Table = this.$el.find('#table').bootstrapTable({
				// 	idField: 'id',
				// 	uniqueId: 'id',
				// 	// pagination: true,
				// 	search: true,
				// 	// totalField: 'count',
				// 	// sidePagination: 'server',
				// 	// dataField: 'results',
				// 	checkbox: true,
				// 	sortable: true,
				// 	classes: 'table table-hover table-bordered',
				// 	iconSize: 'sm',
				// 	url: '',
				// 	onClickRow: function(row, e, index){
				// 		$.get({
				// 			url: '/apiworkers/merge_data?'+_view.$el.find('form').serialize()+'&val='+row.object,
				// 			success: function(response){
				// 				debugger
				// 			}
				// 		});
				// 	},
				// 	// rowStyle: function(row, index) {
				// 	// 	return {
				// 	// 		css: {"cursor": "pointer"}
				// 	// 	};
				// 	// },
				// 	columns: [
				// 		{
				// 			field: 'object',
				// 			title: 'Объект',
				// 			sortable: true,
				// 			cellStyle: function(row){
				// 				return {
				// 					classes: '',
				// 					css: {
				// 						cursor: 'pointer'
				// 					}
				// 				}
				// 			}
				// 		},
				// 		{
				// 			field: 'count',
				// 			title: 'Количество',
				// 		},
				// 	]
				// });
			}
		});
		return View;
	});
