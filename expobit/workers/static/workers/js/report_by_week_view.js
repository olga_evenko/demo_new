define(['text!workers/html/main_view.html', 
	'utils',
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'bootstrap-modal'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#modal_details_table_view')[0].outerHTML,
		events: {
		},
		regions: {
		},
		// className: 'displayNone',
		templateContext: function(){
			return {
				month: this.options.month
			};
		},
		initialize: function(){
		},
		onRender: function(){
			var _this = this;
			var col = [
				{
					field: 'date_diff',
					title: 'Даты',
					// editable: edit_settings,
				},
				{
					field: 'count',
					title: 'Кол. заявок',
					// editable: edit_settings,
				},
				{
					field: 'c_arr',
					title: 'Нарастающий итог',
					formatter: function(value, row, index){
						if(_this.app){
							_this.app = _this.app+row.count;
							return _this.app;
						}
						_this.app = row.count;
						return _this.app;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact_bill',
					title: 'Сумма заявок',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'f_bill_arr',
					title: 'Нарастающий итог',
					formatter: function(value, row, index){
						if(_this.fact_bill){
							_this.fact_bill = _this.fact_bill+row.fact_bill;
							// return _this.fact_bill;
						}
						else{
							_this.fact_bill = row.fact_bill;
						}
						var value = _this.fact_bill;
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					}
				},
				{
					field: 'fact',
					title: 'Оплата',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'f_arr',
					title: 'Нарастающий итог',
					formatter: function(value, row, index){
						if(_this.fact){
							_this.fact = _this.fact+row.fact;
							// return _this.fact;
						}
						else{
							_this.fact = row.fact;
						}
						var value = _this.fact;
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				}
				];
				if(!this.options.min){
					col.unshift({
						field: 'week_number',
						title: 'Неделя в месяце',
					});
					col.unshift({
						field: 'week',
						title: 'Неделя',
					});
				}
			this.Table = this.$el.find('#table_weeks').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiworkers/report_by_week/table/?report='+_this.options.report+'&project='+_this.options.project,
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				rowStyle: function(row, index) {
					if(row.manager == 'Итого'){
						return {
							classes: 'info'
						};	
					}
					return {classes: ''};
					
				},
				columns: col,
				
			});

		}
	});
	return ProjectReportsView
})
