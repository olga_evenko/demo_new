define(['text!workers/html/main_view.html', 
	'utils',
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'bootstrap-modal'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#managers_table_view')[0].outerHTML,
		events: {
		},
		regions: {
		},
		className: 'displayNone',
		templateContext: function(){
			return {
				month: this.options.month
			};
		},
		initialize: function(){
		},
		onRender: function(){
			var _this = this;
			var _view = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var date = new Date();
			this.$el.find('input[name="date"]').val(date.getFullYear().toString()+'-'+((date.getMonth()+1)<10?'0'+(date.getMonth()+1).toString():(date.getMonth()+1).toString()));
			this.SelectDirection = this.$el.find('#SelectDirection').selectize({
				valueField: 'id',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/users/?direction__type_project_type=expo',
						success: function(response){
							for(var i in response){
								_this.addOption({
									id: response[i].id, 
									text: response[i].last_name+' '+response[i].first_name
								});
							}
						}
					});
				},
				onChange: function(value){
					$.get({
						url: '/apiworkers/users/?managers__head='+value,
						success: function(response){
							_view.SelectProjectManager[0].selectize.clearOptions();
							for(var i in response){
								_view.SelectProjectManager[0].selectize.addOption({
									id: response[i].id, 
									text: response[i].last_name+' '+response[i].first_name
								});
							}
						}
					});
					if(_view.$el.find('input[name="date"]').val() == ''){
						_view.Table.bootstrapTable('refresh', {
							url: '/apiprojects/report_managers/full_table/?project__project_type__director='+value+'&month=1'
						});
						return;
					}
					_view.Table.bootstrapTable('refresh', {
						url: '/apiprojects/report_managers/full_table/?project__project_type__director='+value+'&date='+
						_view.$el.find('input[name="date"]').val()
					});
				}
			});
			this.SelectProjectManager = this.$el.find('#SelectProjectsManager').selectize({
					valueField: 'id',
					onInitialize: function(){
						var _this = this;
					},
					onChange: function(value){
						// if(value != ''){
						if(_view.$el.find('input[name="date"]').val() == ''){
							_view.Table.bootstrapTable('refresh', {
								url: '/apiprojects/report_managers/full_table/?project__project_type__director='+
								_view.SelectDirection[0].selectize.getValue()+
								'&month=1&project_indicators__project__user='+value
							});
							return;
						}
						_view.Table.bootstrapTable('refresh', {
							url: '/apiprojects/report_managers/full_table/?project__project_type__director='+
							_view.SelectDirection[0].selectize.getValue()+
							'&date='+_view.$el.find('input[name="date"]').val()+'&project_indicators__project__user='+value
						});
					}
				});
			this.$el.find('input[name="date"]').on('change', function(){
				_view.Table.bootstrapTable('refresh', {
					url: '/apiprojects/report_managers/full_table/?project__project_type__director='+
					_view.SelectDirection[0].selectize.getValue()+'&date='+this.value+
					'&project_indicators__project__user='+_view.SelectProjectManager[0].selectize.getValue()
				});
			});
			this.Table = this.$el.find('#table_managers').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiprojects/report_managers/full_table/?date='+this.$el.find('input[name="date"]').val(),
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				rowStyle: function(row, index) {
					if(row.manager.manager == 'Итого'){
						return {
							classes: 'info'
						};	
					}
					return {classes: ''};
					
				},
				columns: [
				{
					field: 'manager.manager',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'fin_plan_month_sum',
					title: 'Ежемесячный план поступлений',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'app_plan_month_sum',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок на весь период'
				},
				{
					field: 'perc_comp',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'plan_processing_db_sum',
					title: 'План обработки базы клиентов',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				],
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				}
			});

		}
	});
	return ProjectReportsView
})
