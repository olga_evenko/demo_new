define(['text!workers/html/main_view.html', 'utils', 'selectize','bootstrap-table-locale'], function(html, utils){
		
		var View = Backbone.Marionette.View.extend({
			template:$(html).filter('#merge_data_view')[0].outerHTML,
			events: {
				'click #search': 'search',
				'click .setMain': 'setMain',
				'click #sendMerge': 'checkFields'
			},
			className: 'panel panel-default form',
			initialize: function(){
				this.exclude = [];
				this.addition = [];
			},
			search: function(e){
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/duplicates_data?'+this.$el.find('form').serialize()});
			},
			checkFields: function(e){
				var content = 'Внимание! Все сторонние объекты удалятся. Продолжить?';
				var arr = this.$el.find('span.addVal').toArray();
				for(var i in arr){
					if($(arr[i]).attr('style').indexOf('display: none;') == -1){
						content = 'Остались непроверенные поля. Продолжить? \n Все сторонние объекты удалятся.';
					}
				}
				var _view = this;
				var modal = new Backbone.BootstrapModal({ 
					content: content,
					title: 'Предупреждение',
					okText: 'Продолжить',
					cancelText: 'Отмена',
					animate: true
				}).open();
				modal.on('ok', function(){
					_view.sendMerge();
				});
			},
			sendMerge: function(e){
				var send_data = {};
				var arr = this.getRegion('region_main').currentView.$el.find('form').serializeArray();
				for(var i in arr){
					send_data[arr[i].name] = arr[i].value;
					if(send_data[arr[i].name] == 'null'){
						send_data[arr[i].name] = null;
					}
				}
				arr = this.$el.find('#modelForm').serializeArray();
				for(var i in arr){
					send_data[arr[i].name] = arr[i].value;
				}
				send_data.exclude_field = this.exclude;
				send_data.add_field = this.addition;
				send_data.models_dict = {};
				var regions = this.getRegions();
				for(var i in regions){
					if(i != 'region_main'){
						send_data.models_dict[regions[i].currentView.model.id] = {
							exclude: regions[i].currentView.local_exclude,
							add: regions[i].currentView.local_addition
						};
					}
				}
				var _view = this;
				$.ajax({
					url: '/apiworkers/merge_data',
					type: 'POST',
					contentType: 'application/json',
					data: JSON.stringify(send_data),
					success: function(response){
						utils.notySuccess('Успешно').show();
						$('#merge_region').empty();
						_view.Table.bootstrapTable('refresh');
					},
					error: function(response){
						utils.notyError().show();
					} 
				});
			},
			setMain: function(e){
				var _view = this;
				require(['workers/js/merge_form_builder_view'], function(View) {
					var button = $(e.currentTarget);
					$('<div class="'+_view.class_name+'" id="region_'+'main'+'"></div>').insertAfter('#region_0');
					_view.addRegion('region_main', '#region_main');
					_view.showChildView('region_main', new View({
						model: new Backbone.Model(_view.model_regions['region_'+button.data().region].attributes), 
						main: true,
						exclude: _view.exclude, 
						addition: _view.addition
					}));
					_view.$el.find('.setMain').fadeOut();
					_view.$el.find('span').fadeIn();
					var regions = _view.getRegions();
					for(var i in regions){
						regions[i].currentView.main_model = _view.model_regions['region_'+button.data().region];
						regions[i].currentView.main_view = _view.getRegion('region_main').currentView;
						if(i != 'region_main'){
							regions[i].currentView.updateSpan();
						}
					}
				});
			},
			updateSpanGlobal: function(){
				var regions = this.getRegions();
					for(var i in regions){
						if(i != 'region_main'){
							regions[i].currentView.updateSpan();
						}
					}
			},
			onRender: function(){
				var _view = this;
				this.$el.find(document).ready(function($) {
					_view.$el.fadeIn();
				});
				this.SelectModel = this.$el.find('#SelectModel').selectize({
					labelField: 'name',
					valueField: 'id',
					searchField: 'name',
					onInitialize: function(){
						var _this = this;
						$.get({
							url: '/apiworkers/content_type/',
							success: function(response){
								for(var i in response){
									_this.addOption(response[i]);
								}
							},
						});
					},
					onChange: function(value){
						var val = this.options[value];
						_view.SelectField[0].selectize.clearOptions();
						for(var i in val){
							_view.SelectField[0].selectize.addOption(val[i]);
						}
					}
				});
				this.SelectField = this.$el.find('#SelectField').selectize({
					labelField: 'name',
					valueField: 'value',
					searchField: 'name'
				});
				this.Table = this.$el.find('#table').bootstrapTable({
					idField: 'id',
					uniqueId: 'id',
					// pagination: true,
					search: true,
					// totalField: 'count',
					// sidePagination: 'server',
					// dataField: 'results',
					checkbox: true,
					sortable: true,
					classes: 'table table-hover table-bordered',
					iconSize: 'sm',
					url: '',
					onClickRow: function(row, e, index){
						$.get({
							url: '/apiworkers/merge_data?'+_view.$el.find('form').serialize()+'&val='+row.object,
							success: function(response){
								require(['workers/js/merge_form_builder_view'], function(View) {
									_view.$el.find('#sendMerge').fadeIn();
									$('#merge_region').empty();
									_view.exclude = [];
									_view.addition = [];
									_view.class_name = 'col-md-'+Math.round((12/(response.length+1)), 0);
									_view.model_regions = {};
									var body = $("html, body");
									body.stop().animate({scrollTop:$('#merge_region').offset().top-25}, 500, 'swing', function() {});
									// _view.$el.find('#merge_region').fadeIn();
									mainView.sideLeftBar();
									for(var i in response){
										_view.$el.find('#merge_region').append('<div class="'+_view.class_name+'" id="region_'+i+'"></div>');
										_view.addRegion('region_'+i, '#region_'+i);
										_view.model_regions['region_'+i] = new Backbone.Model(response[i]);
											_view.showChildView('region_'+i, new View({model: _view.model_regions['region_'+i], region: i, exclude: _view.exclude, addition: _view.addition}));
									}
								});
							}
						});
					},
					// rowStyle: function(row, index) {
					// 	return {
					// 		css: {"cursor": "pointer"}
					// 	};
					// },
					columns: [
						{
							field: 'object',
							title: 'Объект',
							sortable: true,
							cellStyle: function(row){
								return {
									classes: '',
									css: {
										cursor: 'pointer'
									}
								}
							}
						},
						{
							field: 'count',
							title: 'Количество',
						},
					]
				});
			}
		});
		return View;
	});
