define(['text!workers/html/main_view.html','libs/backbone.paginator/js/paginator', 
	'paginator-view'], function(html, Paginator, PaginatorView){
	var Model = Backbone.Model.extend({

	});
	var Collection = Paginator.extend({
		model: Model,
		url: '/apiworkers/messages/',
		parse: function(response){
			return response.results
		},
        state: {
            firstPage: 1,
            lastPage: null,
            currentPage: 1,
            top: 0,
            pageSize: 5,
            actPageSize: 3,
            totalSize: null
        },
	});
	
	var ItemMessageView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_message')[0].outerHTML,
		templateContext: function(){
			if(this.model.get('sender_id') == user.id){
				return {show_user: true};
			}
		},
		className: function(){
			if(this.model.get('show')){
				return 'contentItems panel panel-default';
			}
			else{
				return 'contentItems panel panel-default new_message';
			};
		},
		events: {
			'click': 'openMessage'
		},
		openMessage: function(e){
			if(this.model.get('sender_id') == user.id){
				router.navigate('message/'+this.model.get('user'), {trigger: true});
			}
			else{
				router.navigate('message/'+this.model.get('sender_id'), {trigger: true});
			}

		},
	});

	var EmptyView = Backbone.Marionette.View.extend({
		template: $(html).filter('#empty_view')[0].outerHTML,
	});

	var MessageCollectionView = Backbone.Marionette.CollectionView.extend({
		childView:ItemMessageView,
		emptyView: EmptyView,
		initialize: function(){
			var _this=this;
		}
	});
	var MessageView = Backbone.Marionette.View.extend({
		template:$(html).filter('#message_view')[0].outerHTML,
		regions:{
			content: '#content',
			paginator: '#paginator'
		},
		events: {
		},
		className: 'displayNone',
		initialize: function(){
			this.collection = new Collection();
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn();
			});
			var messageCollectionView = new MessageCollectionView({collection:this.collection})
			this.showChildView('content', messageCollectionView);
			this.collection.fetch({
				success: function(){
					PaginatorView(
						_this.getRegion('paginator'),
						_this.collection, [5, 10, 25, 50]
						);
					messageCollectionView.render();
				}
			});
		},
	});
return MessageView
})
