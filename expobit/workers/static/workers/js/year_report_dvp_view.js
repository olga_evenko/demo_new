define(['text!workers/html/main_view.html', 
	'chartjs',
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize',
	'datetimepicker',
	'table-context-menu'], function(html, Chart){
	
	var PlanProjectManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#year_dvp_report')[0].outerHTML,
		events: {
			'click #getReport': 'getReport',
			'click #getReportInWork': 'getReportInWork'
		},
		className: 'displayNone',
		templateContext: function(){
			var show_select = false;
		},
		initialize: function(){
			$('.loading').fadeIn();
		},
		getReport: function(){
			$('.loading').fadeIn();
			this.Table.bootstrapTable('refresh', {
				url: 'apiworkers/full_report_dvp/?status__in=working,planning,finished&project_type__director='+this.SelectDirection[0].selectize.getValue()+'&date_start__year='+this.$el.find('input[name="year"]').val()+'&user='+this.SelectProjectManager[0].selectize.getValue()
			});
		},
		getReportInWork: function(){
			$('.loading').fadeIn();
			this.Table.bootstrapTable('refresh', {
				url: 'apiworkers/full_report_dvp/?status=working&project_type__director='+this.SelectDirection[0].selectize.getValue()+'&date_start__year='+this.$el.find('input[name="year"]').val()+'&user='+this.SelectProjectManager[0].selectize.getValue()
			});
		},
		onRender: function(){
			var _view = this;
			this.config = {
				type: 'pie',
				data: {
					datasets: [{
						data: [],
						backgroundColor: [
							window.chartColors.red,
							window.chartColors.green,
						],
						label: 'Dataset 1'
					}],
					labels: [
						'Не оплачено',
						'Оплачено',
					]
				},
				options: {
					responsive: true,
					tooltips: {
			            callbacks: {
			                label: function(tooltipItem, data) {
			                	var value = data.datasets[0].data[tooltipItem.index];
			                	value = value.toString().replace(/ /g,'');
							    var number = value;
							    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			                    return value;
			                }
			            }
			        }
				}
			};
			this.chart = new Chart(_view.$el.find('#Chart'), _view.config);
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			head_direction = true;
			// var date = new Date();
			// this.$el.find('input[name="month"]').val(date.getFullYear().toString()+'-'+((date.getMonth()+1)<10?'0'+(date.getMonth()+1).toString():(date.getMonth()+1).toString()));
			// this.SelectYear = this.$el.find('#SelectYear').selectize();
			this.SelectDirection = this.$el.find('#SelectDirection').selectize({
				valueField: 'id',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/users/?direction__type_project_type=expo',
						success: function(response){
							for(var i in response){
								_this.addOption({
									id: response[i].id, 
									text: response[i].last_name+' '+response[i].first_name
								});
							}
						}
					});
				},
				onChange: function(value){
					_view.SelectProjectManager[0].selectize.clearOptions();
					if(value != ''){
						$.get({
							url: '/apiworkers/users/?managers__head='+value,
							success: function(response){
								for(var i in response){
									_view.SelectProjectManager[0].selectize.addOption({
										id: response[i].id, 
										text: response[i].last_name+' '+response[i].first_name
									});
								}
							}
						});
					}
				}
			});
			this.SelectProjectManager = this.$el.find('#SelectProjectsManager').selectize({
					valueField: 'id',
					onInitialize: function(){
						var _this = this;
					},
					onChange: function(value){
						
					}
				});

			this.$el.find('#datetimepicker10').datetimepicker({
                viewMode: 'years',
                format: 'YYYY'
            });
            this.$el.find('#datetimepicker10').data("DateTimePicker").date(new Date());
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiworkers/full_report_dvp/?status=working&date_start__year='+this.$el.find('input[name="year"]').val(),
				contextMenu: '#context-menu',
				onContextMenuItem: function(row, $el){
					if($el.data("item") == "show_meeting"){
						require(['workers/js/modals/project_report_dvp_meeting_view', 'workers/js/modals/modal_templates'], function(View, tmp){
							var modal = new Backbone.BootstrapModal({ 
								content: new View({model: new Backbone.Model(row), status: row.status}),
								title: 'План по проекту '+row.name,
								okText: 'Закрыть',
								cancelText: '',
								animate: true, 
								template: tmp.full_screen
							}).open();
							modal.on('ok', function(){
								if(modal.options.content.updateGlobal){
									_view.Table.bootstrapTable('refresh');
								}
							});
						});
					}
				},
				onContextMenuRow: function(row, $el){
					this.right_click = true;
				},
				onClickRow: function(row, e, index){
					if(row.name != 'Итого выставки' && !this.right_click){
						require(['workers/js/modals/project_report_dvp_view', 'workers/js/modals/modal_templates'], function(View, tmp){
							var modal = new Backbone.BootstrapModal({ 
								content: new View({model: new Backbone.Model(row), status: row.status}),
								title: 'План по проекту '+row.name,
								okText: 'Закрыть',
								cancelText: '',
								animate: true, 
								template: tmp.full_screen
							}).open();
							modal.on('ok', function(){
								if(modal.options.content.updateGlobal){
									_view.Table.bootstrapTable('refresh');
								}
							});
						});
					}
					this.right_click = false;
				},
				columns: [
				{
					field: 'approval',
					title: '',
					formatter: function(value, row, index){
						if(row.name == 'Итого выставки'){
							return '';
						}
						if(value){
							return '<span class="glyphicon glyphicon-alert" style="color: #f39c12;"></span>';
						}
						if(row.return_val){
							return '<span class="glyphicon glyphicon-question-sign" style="color: #f39c12;"></span>';
						}
						return '<span class="glyphicon glyphicon-ok-sign"></span>';
					}
				},
				{
					field: 'name',
					title: 'Название мероприятия',
					cellStyle: function(value, row){
						var classes = '';
						if(row.status == 'finished'){
							classes = classes + 'success';
						}
						return {
							classes: classes
						}
					}
					// sortable: true,
				},
				{
					field: 'period',
					title: 'Дата начала - дата окончания',
					// editable: edit_settings,
				},
				{
					field: 'plan_money',
					title: 'План поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'sum_pay',
					title: 'Факт сумма заявок',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc_money',
					title: '% выполнения',
					valign: 'middle',
					// cellStyle: function(value, row, index, field){
					// 	return {
					// 		classes: 'info',
					// 	};
					// },
					formatter: function(value, row, index){
						if(!value){
							return;
						}
						var progress = parseFloat(value.split(' ')[0]).toFixed();
						var class_name;
						var style_name = '';
						if(parseFloat(value.split(' ')[0]) < 25){
							style_name = 'color: #39434e;';
							class_name = 'progress-bar progress-bar-danger progress-bar-striped active';
						}
						else if(parseFloat(value.split(' ')[0]) < 75){
							class_name = 'progress-bar progress-bar-striped active';
						}
						else{
							class_name = 'progress-bar progress-bar-success progress-bar-striped active';
						}
						return '<div class="progress" style="margin-bottom: 0px; height: 25px;"><div class="'+class_name+'" role="progressbar"aria-valuenow="'+
						progress+'" aria-valuemin="0" aria-valuemax="100" style="width:'+progress+'%; padding-top: 4px; '+style_name+'">'+value+'</div></div>';
					},
				},
				{
					field: 'plan_app',
					title: 'План поступления заявок',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'perc_app',
					title: '% выполнения',
					valign: 'middle',
					formatter: function(value, row, index){
						if(!value){
							return;
						}
						var progress = parseFloat(value.split(' ')[0]).toFixed();
						var class_name;
						var style_name = '';
						if(parseFloat(value.split(' ')[0]) < 25){
							style_name = 'color: #39434e;';
							class_name = 'progress-bar progress-bar-danger progress-bar-striped active';
						}
						else if(parseFloat(value.split(' ')[0]) < 75){
							class_name = 'progress-bar progress-bar-striped active';
						}
						else{
							class_name = 'progress-bar progress-bar-success progress-bar-striped active';
						}
						return '<div class="progress" style="margin-bottom: 0px; height: 25px;"><div class="'+class_name+'" role="progressbar"aria-valuenow="'+
						progress+'" aria-valuemin="0" aria-valuemax="100" style="width:'+progress+'%; padding-top: 4px; '+style_name+'">'+value+'</div></div>';
					},
					// cellStyle: function(value, row, index, field){
					// 	return {
					// 		classes: 'info',
					// 	};
					// }
				},
				{
					field: 'sum_amount_fact',
					title: 'ДС от посетителей',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'count_vis_fact',
					title: 'Количество посетителей'
				},
				],
				responseHandler: function(response){
					// var results = response.results;
					var sum_fin_plan = 0;
					var sum_fact = 0;
					var sum_app_plan = 0;
					var sum_fact_app = 0;
					var sum_not_pay = 0;
					var sum_sum_pay = 0;
					var sum_amount_fact = 0;
					var count_vis_fact = 0;
					for(i in response){
						sum_amount_fact = sum_amount_fact + response[i].sum_amount_fact
						count_vis_fact = count_vis_fact + response[i].count_vis_fact
						sum_fin_plan = sum_fin_plan + response[i].plan_money;
						sum_fact = sum_fact + response[i].fact;
						sum_app_plan = sum_app_plan + response[i].plan_app;
						sum_fact_app = sum_fact_app + response[i].fact_app;
						sum_not_pay = sum_not_pay + response[i].not_pay;
						sum_sum_pay = sum_sum_pay + response[i].sum_pay;
					}
					var perc = 0, perc_comp = 0, perc_comp_db = 0;
					if(sum_fin_plan != 0){
						perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
					}
					if(sum_app_plan != 0){
						perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
					}
					response.push({
						name: 'Итого выставки',
						plan_money: sum_fin_plan,
						perc_money: perc.toString()+' %',
						perc_app: perc_comp.toString()+' %',
						fact: sum_fact,
						plan_app: sum_app_plan,
						fact_app: sum_fact_app,
						not_pay: sum_not_pay,
						sum_pay: sum_sum_pay,
						sum_amount_fact: sum_amount_fact, 
						count_vis_fact: count_vis_fact
					});
					_view.config.data.datasets[0].data = [sum_not_pay, sum_fact];
					_view.config.data.labels[0] = 'Не оплачено';
					_view.config.data.labels[1] = 'Оплачено';
					_view.config.data.labels[0] = _view.config.data.labels[0]+' - '+ (sum_not_pay/(sum_not_pay+sum_fact)*100).toFixed() + ' %';
					_view.config.data.labels[1] = _view.config.data.labels[1]+' - '+ (sum_fact/(sum_not_pay+sum_fact)*100).toFixed() + ' %';
					$('.loading').fadeOut();
					_view.chart.update();
					return response;
				},
				// onClickRow: function(row, e, index){
				// 	if(row.name != 'Итого выставки'){
				// 		require(['workers/js/modals/project_report_dvp_view', 'workers/js/modals/modal_templates'], function(View, tmp){
				// 			var modal = new Backbone.BootstrapModal({ 
				// 				content: new View({model: new Backbone.Model(row), status: row.status}),
				// 				title: 'План по проекту '+row.name,
				// 				okText: 'Закрыть',
				// 				cancelText: '',
				// 				animate: true, 
				// 				template: tmp.full_screen
				// 			}).open();
				// 			modal.on('ok', function(){
				// 				if(modal.options.content.updateGlobal){
				// 					_view.Table.bootstrapTable('refresh');
				// 				}
				// 			});
				// 		});
				// 	}
				// },
				rowStyle: function(row, index) {
					var classes = '';
					// if(row.status == 'finished'){
					// 	classes = classes + 'success';
					// }
					if(row.name == 'Итого выставки'){
						return {
							classes: classes + 'info',
							css: {"cursor": "pointer"}
						};
					}
					return {
							classes: classes,
							css: {"cursor": "pointer"}
						};
				}
			});
		},
	});
return PlanProjectManagerView;
});
