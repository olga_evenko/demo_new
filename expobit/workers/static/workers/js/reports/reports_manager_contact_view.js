define(['text!workers/html/reports/reports_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#report_manager_contact_view')[0].outerHTML,
		events: {
			'input input[type="date"]': 'activeButton',
			'click #getReport': 'getReport'
		},
		className: 'displayNone',
		initialize: function(){
		},
		activeButton: function(e){
			this.$el.find('#getReport').attr('disabled', false);
		},
		getReport: function(e){
			var date_gt = this.$el.find('input[name="date_gt"]').val();
			var date_lt = this.$el.find('input[name="date_lt"]').val();
			this.Table.bootstrapTable('refresh', {
				url: '/apiworkers/report_contact_type_1/?date_gt='+date_gt+'&date_lt='+date_lt
			});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				totalField: 'count',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// url: '/apiworkers/report_contact_type_1/',
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				columns: [
				{
					field: 'name',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'count_contact',
					title: 'Количество контактов',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'success',
						};
					}
					// sortable: true
				},
				{
					field: 'first_input',
					title: 'Вх. зв. перв.',
				},
				{
					field: 'repeat_input',
					title: 'Вх. зв. повт.',
				},
				{
					field: 'first_out',
					title: 'Исх. зв. перв.',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'success',
						};
					}
				},
				{
					field: 'repeat_out',
					title: 'Исх. зв. повт.',
				},
				{
					field: 'meet',
					title: 'Встреча',
				},
				{
					field: 'e_mails',
					title: 'Электронная переписка',
				},
				{
					field: 'in_work',
					title: 'В работе',
					cellStyle: function(value, row, index, field){
						if(row.name != 'Итого'){
							return {
								css: {'background-color': '#f0ad4e'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'interest',
					title: 'Интерес',
				},
				{
					field: 'app',
					title: 'Заявка',
					cellStyle: function(value, row, index, field){
						if(row.name != 'Итого'){
							return {
								css: {'background-color': '#f0ad4e'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'fail',
					title: 'Отказ',
				},
				{
					field: 'other_interest',
					title: 'Интерес на другие проекты',
				},
				{
					field: 'fact',
					title: 'Поступление ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				],
				rowStyle: function(row, index, value){
					if(row.name == 'Итого'){
						return {
							classes: 'info' 
						};
					}
					return {
						classes: ''
					};
				},
				responseHandler: function(response){
					var exclude_fields = ['id', 'user', 'head', 'name'];
					var last_row = {name: 'Итого'};
					for(var i in response){
						for(var j in response[i]){
							if(exclude_fields.indexOf(j) == -1){
								if(last_row[j]){
									last_row[j] = last_row[j] + response[i][j];
								}
								else{
									last_row[j] = response[i][j];	
								}
							}
						}
					}
					response.push(last_row);
					return response;
				}
			});
		}
	});
	return ClientsView
})
