define(['text!workers/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#main_contact_report_view')[0].outerHTML,
		events: {
			'click #getReport': 'getReport'
		},
		className: 'displayNone',
		initialize: function(){
		},
		getReport: function(e){
			var date_gt = this.$el.find('input[name="date_gt"]').val();
			var date_lt = this.$el.find('input[name="date_lt"]').val();
			var value = this.SelectProject[0].selectize.getValue();
			this.Table.bootstrapTable('refresh', {
				url: '/apiworkers/report_contact_individual_soi/?project__in='+value+'&date__date__gte='+date_gt+'&date__date__lte='+date_lt
			});
			// router.navigate('report_contact_individual' + '/project' + value, {replace: true});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				totalField: 'count',
				// dataField: 'results',
				checkbox: true,
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				columns: [
				{
					field: 'source_of_information',
					title: 'Тип',
				
				},
				{
					field: 'sof_count',
					title: 'Количество',
				
				},
				],
				rowStyle: function(row, index) {
					if(row.name == 'Итого'){
						return {
							classes: 'info'
						};	
					};
					return {classes: ''}
					
				},
				responseHandler: function(response){
					
					return response
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/?all=true',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							if(_view.options.project){
								_this.setValue(_view.options.project);
							};
						}
					});
				},
				onChange: function(value){
				},
			});
		}
	});
	return ClientsView
})
