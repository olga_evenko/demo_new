define(['text!workers/html/reports/reports_view.html', 
	'chartjs',
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize',
	'datetimepicker'], function(html, Chart){
	
	var PlanProjectManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#report_by_project_view')[0].outerHTML,
		events: {
			'click #getReport': 'getReport'
		},
		regions: {
			accommodation_services: '#accommodation_services',
			banquet_hall: '#banquet_hall'
		},
		className: 'displayNone',
		templateContext: function(){
			var show_select = false;
		},
		initialize: function(){
			// $('.loading').fadeIn();
			this.months = [
			'Январь', 'Февраль', 
			'Март', 'Апрель', 
			'Май', 'Июнь', 
			'Июль', 'Август', 
			'Сентябрь', 'Октябрь', 
			'Ноябрь', 'Декабрь'];
			var _view = this;
			require(['projects/js/custom_report_view'], function(View) {
				
				var options = {
					type_name: 'Проживание в гостинице',
					title: 'Показатели услуг проживания',
					columns: [
						{
							title: 'Название',
							field: 'name'
						},
						{
							title: 'План',
							field: 'plan',
							editable: true,
							formatter: function(value, row, index){
								if(value){
									value = value.toString().replace(/ /g,'');
								    var number = value;
								    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
								}
								return value;
							},
						},
						{
							title: 'Факт',
							field: 'fact',
							editable: true,
							formatter: function(value, row, index){
								if(value){
									value = value.toString().replace(/ /g,'');
								    var number = value;
								    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
								}
								return value;
							},
						},
						{
							title: '%',
							field: 'perc',
						}
					]
				};
				// _view.showChildView('accommodation_services', new View(options));
				options = {
					title: 'Показатели БанкетХолла',
					type_name: 'Проект Банкет-Холла',
					columns: [
						{
							title: 'Название',
							field: 'name'
						},
						{
							title: 'План',
							field: 'plan',
							editable: true,
							formatter: function(value, row, index){
								if(value){
									value = value.toString().replace(/ /g,'');
								    var number = value;
								    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
								}
								return value;
							},
						},
						{
							title: 'Факт',
							field: 'fact',
							editable: true,
							formatter: function(value, row, index){
								if(value){
									value = value.toString().replace(/ /g,'');
								    var number = value;
								    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
								}
								return value;
							},
						},
						{
							title: '%',
							field: 'perc',
						},
						{
							title: 'Выработка, т',
							field: 'working_out',
							editable: true
						},
						{
							title: 'Количество человек',
							field: 'count_people',
							editable: true
						},
						{
							title: 'Ср. чек',
							field: 'average_check',
							editable: true
						},
						{
							title: 'Удовлетворенность',
							field: 'satisfaction',
							editable: true
						}
					]
				};
				// _view.showChildView('banquet_hall', new View(options));

			});
		},
		getReport: function(){
			// $('.loading').fadeIn();
			this.Table.bootstrapTable('refresh', {
				url: 'apiworkers/report_by_project/?'+this.$el.find('form').serialize()
			});
		},
		renderDynamicTable: function(col, data){
			this.$el.find('#dynamic_table').bootstrapTable('destroy');
			this.DynamicTable = this.$el.find('#dynamic_table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				data: data,
				columns: col,
				rowStyle: function(row, index) {
					var classes = '';
					if(row.status == 'finished'){
						classes = classes + 'success';
					}
					if(row.name == 'Итого'){
						return {
							classes: classes + 'info',
							css: {"cursor": "pointer"}
						};
					}
					return {
							classes: classes,
							css: {"cursor": "pointer"}
						};
				}
			});
		},
		onRender: function(){
			var _view = this;
			this.config = {
				type: 'pie',
				data: {
					datasets: [{
						data: [],
						backgroundColor: [
							window.chartColors.red,
							window.chartColors.green,
						],
						label: 'Dataset 1'
					}],
					labels: [
						'Не оплачено',
						'Оплачено',
					]
				},
				options: {
					responsive: true
				}
			};
			
			// this.chart = new Chart(_view.$el.find('#Chart'), _view.config);
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			var curr_date = new Date();
			this.$el.find('input[type="month"]').val(curr_date.toJSON().slice(0,7));
			
			this.SelectProjectType = this.$el.find('#SelectProjectType').selectize({
					valueField: 'id',
					labelField: 'type',
					onInitialize: function(){
						var _this = this;
						$.get({
							url: 'apiprojects/projects_types/?type_project_type=kvs',
							success: function(response){
								for(var i in response){
									_this.addOption(response[i]);
								}
							}
						});
					},
					onChange: function(value){
						
					}
				});
			this.SelectCategory = this.$el.find('#SelectCategory').selectize({
				options: [
					{text: 'Виду мероприятия',value: 'format_event'}, 
					{text: 'Виду проекта',value: 'project_type__type'}, 
					{text: 'Бизнес-единице',value: 'buisness_unit__name'}, 
					{text: 'По менеджеру', value: 'user__last_name'}
				]
			});
			this.SelectFieldShow = this.$el.find('#SelectFieldShow').selectize({
				options: [
					{text: 'ДС',value: 'fact'}, 
					{text: 'Количество проектов',value: 'project'}, 
				],
				onInitialize: function(){
					this.setValue('fact');
				}
			});
			this.FORMAT_EVENT = {
				exhibition: 'Выставка',
				forum: 'Форум',
        		corporate: 'Корпоратив',
        		competition: 'Конкурс/чемпионат',
		        training: 'Тренинг/семинар',
		        banquet: 'Банкет',
        		reception: 'Фуршет',
        		coffee_break: 'Кофе-брейк',
        		supper: 'Обед-ужин',
        		catering: 'Кейтеринг',
			};
            // this.$el.find('#datetimepicker10').data("DateTimePicker").date(new Date());
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				columns: [
				// {
				// 	field: 'approval',
				// 	title: '',
				// 	formatter: function(value, row, index){
				// 		if(row.name == 'Итого выставки'){
				// 			return '';
				// 		}
				// 		if(value){
				// 			return '<span class="glyphicon glyphicon-alert" style="color: #f39c12;"></span>';
				// 		}
				// 		return '<span class="glyphicon glyphicon-ok-sign"></span>';
				// 	}
				// },
				{
					field: 'name',
					title: 'Категории',
					formatter: function(value, row, index){
						if(_view.FORMAT_EVENT[value]){
							return _view.FORMAT_EVENT[value];
						}
						return value;
					}
				},
				{
					field: 'plan_money',
					title: 'План поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'count_projects',
					title: 'Количество проектов'
					// sortable: true,
				},
				{
					field: 'guest',
					title: 'Количество гостей',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						};
					},
					formatter: function(value, row){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'avg_bill',
					title: 'Средний чек',
					formatter: function(val, row){
						if(!row.count_projects){
							return undefined;
						}
						var value = (row.fact/row.count_projects).toFixed();
						value = value.toString().replace(/ /g,'');
						var number = value;
						value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						return value;
					},
				}
				],
				responseHandler: function(response){
					_view.summ_result = {};
					var dynamic_results = response.dynamic_plan_table;
					response = response.plan_table;
					var sum_fin_plan = 0;
					var sum_fact = 0;
					var count_projects = 0;
					var count_guests = 0;
					var sum_fact_app = 0;
					var sum_not_pay = 0;
					var sum_sum_pay = 0;
					for(i in response){
						sum_fin_plan = sum_fin_plan + response[i].plan_money;
						sum_fact = sum_fact + response[i].fact;
						count_guests += response[i].guest;
						count_projects = count_projects + response[i].count_projects;
						sum_fact_app = sum_fact_app + response[i].fact_app;
						sum_not_pay = sum_not_pay + response[i].not_pay;
						sum_sum_pay = sum_sum_pay + response[i].sum_pay;
					}
					var perc = 0, perc_comp = 0, perc_comp_db = 0;
					if(sum_fin_plan != 0){
						perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
					}
					// if(sum_app_plan != 0){
					// 	perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
					// }
					response.push({
						name: 'Итого',
						plan_money: sum_fin_plan,
						perc_money: perc.toString()+' %',
						perc_app: perc_comp.toString()+' %',
						fact: sum_fact,
						count_projects: count_projects,
						fact_app: sum_fact_app,
						not_pay: sum_not_pay,
						sum_pay: sum_sum_pay,
						guest: count_guests
					});
					_view.config.data.datasets[0].data = [sum_not_pay, sum_fact];
					_view.config.data.labels[0] = 'Не оплачено';
					_view.config.data.labels[1] = 'Оплачено';
					_view.config.data.labels[0] = _view.config.data.labels[0]+' - '+ (sum_not_pay/(sum_not_pay+sum_fact)*100).toFixed() + ' %';
					_view.config.data.labels[1] = _view.config.data.labels[1]+' - '+ (sum_fact/(sum_not_pay+sum_fact)*100).toFixed() + ' %';
					$('.loading').fadeOut();
					var start_date = _view.$el.find('input[name="date__gt"]').val();
					var end_date = _view.$el.find('input[name="date__lt"]').val();
					start_date = new Date(start_date+'-01');
					end_date = new Date(end_date+'-01');
					end_date.setMonth(end_date.getMonth() + 1);
					var col = [{
						field: 'name',
						title: 'Категории',
						formatter: function(value, row, index){
						if(_view.FORMAT_EVENT[value]){
							return _view.FORMAT_EVENT[value];
						}
						return value;
					}
					}];
					var fields = _view.SelectFieldShow[0].selectize.getValue().split(',');
					while (start_date<end_date) {
						for(var i in fields){
							col.push({
								field: start_date.getFullYear().toString()+'.'+start_date.getMonth().toString()+'.'+fields[i],
								title: _view.months[start_date.getMonth()]+' ('+_view.SelectFieldShow[0].selectize.options[fields[i]].text+')',
								formatter: function(value, row, index){
									if(value){
										value = value.toString().replace(/ /g,'');
									    var number = value;
									    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
									}
									return value;
								}
							});
						}
						start_date.setMonth(start_date.getMonth()+1);
					}
					for(var i in dynamic_results){
						for(var j in dynamic_results[i]){
							if(Number.isInteger(dynamic_results[i][j])){
								if(!_view.summ_result[j]){
									_view.summ_result[j] = dynamic_results[i][j];
								}
								else{
									_view.summ_result[j] += dynamic_results[i][j];
								}
							}
						}
					}
					_view.summ_result['name'] = 'Итого';
					dynamic_results.push(_view.summ_result);
					_view.renderDynamicTable(col, dynamic_results);
					// _view.chart.update();
					return response;
				},
				onClickRow: function(row, e, index){
					
				},
				rowStyle: function(row, index) {
					var classes = '';
					if(row.name == 'Итого'){
						classes = classes + 'info';
					}
					if(row.name == 'Итого выставки'){
						return {
							classes: classes + 'info',
							css: {"cursor": "pointer"}
						};
					}
					return {
							classes: classes,
							css: {"cursor": "pointer"}
						};
				}
			});
		},
	});
return PlanProjectManagerView;
});
