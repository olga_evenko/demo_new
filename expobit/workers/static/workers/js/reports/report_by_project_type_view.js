define(['text!workers/html/main_view.html', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize',
	'table-context-menu'], function(html){
	
	var PlanProjectManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#report_project_type_view')[0].outerHTML,
		events: {
			'change #setMonth': 'setMonth',
			'click #report': 'getReport'
		},
		className: 'displayNone',
		templateContext: function(){
			return {curr_date: new Date().toJSON().substr(0,7)};
		},
		setMonth: function(e){
			var date = $(e.currentTarget).val();
			this.TableManager.bootstrapTable('removeAll');
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/project_type_reports/?month='+date});
		},
		getReport: function(){

		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.TableManager = this.$el.find('#manager_table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',

				columns: [
					{
						field: 'name',
						title: 'Менеджер',
					},
					{
						field: 'fin_plan_month',
						title: 'План поступлений',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toString().replace(/ /g,'');
                                var number = value;
                                value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                            }
                            return value;
                        },
					},
					{
						field: 'plan_money',
						title: 'Ожидаемая сумма',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toFixed().toString().replace(/ /g,'');
                                var number = value;
                                value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                            }
                            return value;
                        },
					},
					{
						field: 'fact',
						title: 'Факт',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toString().replace(/ /g,'');
                                var number = value;
                                value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                            }
                            return value;
                        },
					},
					{
						field: 'percent',
						title: '% выполнения',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toString()+ '%'
                            }
                            return value;
                        },
                        cellStyle: function(value, row, index, field){
                            return {
                                classes: 'info',
                            }
                        }
					},
					{
						field: 'plan_app',
						title: 'План заявок',
					},
					{
						field: 'fact_app',
						title: 'Факт',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toString().replace(/ /g,'');
                                var number = value;
                                value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                            }
                            return value;
                        },
					},
					{
						field: 'percent_app',
						title: '% выполнения',
						formatter: function(value, row, index){
                            if(value){
                                value = value.toString()+ '%'
                            }
                            return value;
                        },
                        cellStyle: function(value, row, index, field){
                            return {
                                classes: 'info',
                            }
                        }
					},
				]
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/project_type_reports?month='+new Date().toJSON().substr(0,7),
				// toolbar: $(html).filter('#toolbar-report-project-dvp')[0].innerHTML,
				columns: [
				{
					field: 'type',
					title: 'Бизнес-направление',
				},
				{
					field: 'director_name',
					title: 'Руководитель'
				},
				{
					field: 'fin_plan',
					title: 'Фин. план',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'План на месяц',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// sortable: true,
				},
				{
					field: 'plan_app_money',
					title: 'Ожидание поступлений',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toFixed().toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// sortable: true,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// sortable: true,
				},
				{
					field: 'percent',
					title: '% выполнения',
					formatter: function(value, row){
						if(row.fin_plan_month){
							return (row.fact / row.fin_plan_month * 100).toFixed(2) + '%';
						}
						return;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'drop_menu',
					title: '',
				},
				
				],
				onClickRow: function(row, e, index){
				    var result = []
				    for(i in row.manager_detail){
				        if(row.manager_detail[i].fin_plan_month){
				            result.push(row.manager_detail[i])
				        }
				    }
					_view.TableManager.bootstrapTable('load', result);
				},
				responseHandler: function(response){
				    var result = []
				    var sum={
                            type:'Итого',
                            fin_plan:0,
                            fin_plan_month:0,
                            plan_app_money:0,
                            fact:0,
                            manager_detail:[],
				        }

				    for(var i in response){
				        if(response[i].fin_plan){
				            result.push(response[i])
				            sum.fin_plan +=response[i].fin_plan
				            sum.plan_app_money +=response[i].plan_app_money
				            sum.manager_detail = sum.manager_detail.concat(response[i].manager_detail)
				            if(response[i].fin_plan_month){
                                sum.fin_plan_month +=response[i].fin_plan_month
                                sum.fact +=response[i].fact
				            }

				            response[i].drop_menu = `
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Меню
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">`

                            if((user.groups.indexOf('head_expo')>=0 || user.isSuperuser) && response[i].type_project_type == 'expo' ){
                                response[i].drop_menu +=` <li><a href="#projects_type_0/p1-20/t${response[i].id}">Список проектов</a></li>`
                                response[i].drop_menu +=` <li><a href="#director_plan_manager_dvp">План месяца по проектам</a></li>`
                                response[i].drop_menu +=` <li><a href="#group_plan">План месяца по менеджерам</a></li>`
                            }

                            if((user.groups.indexOf('kvs_head')>=0 || user.groups.indexOf('head_expo')>=0 || user.isSuperuser) && response[i].type_project_type == 'kvs'){
                                response[i].drop_menu +=` <li><a href="#group_plan">План на месяц</a></li>`
                                response[i].drop_menu +=` <li><a href="#projects_type_0/p1-20/t${response[i].id}">Список проектов</a></li>`
                            }

                            if((user.groups.indexOf('head_ad')>=0 || user.isSuperuser) && response[i].type_project_type == 'ad'){
                                response[i].drop_menu +=` <li><a href="#group_plan">План на месяц</a></li>`
                                response[i].drop_menu +=` <li><a href="#projects_type_0/p1-20/t${response[i].id}">Список проектов</a></li>`
                                response[i].drop_menu +=` <li><a href="#report_by_calendar">Загрузка заллов</a></li>`
                            }

                            if((user.groups.indexOf('head_expo') >=0 || user.groups.indexOf('kvs_head')>=0 || user.isSuperuser) && response[i].type_project_type == 'kvs'){

                                if(response[i].room_reservation){
                                    response[i].drop_menu +=` <li><a href="#report_by_calendar">Загрузка заллов</a></li>`
                                }
                                if(response[i].addit_parameter == 'booking'){
                                    response[i].drop_menu +=` <li><a href="#report_by_booking">Показатели гостиницы</a></li>`
                                }
                                if(response[i].addit_parameter == 'banquet'){
                                    response[i].drop_menu +=` <li><a href="#report_by_banquet">Показатели Банкет-Холла</a></li>`
                                }
                            }
                            response[i].drop_menu +=' </ul></div>'
                        }
				    }

				    result.push(sum)
				    return result

//					 var results = response.results;
//					 var sum_fin_plan = 0;
//					 var sum_fact = 0;
//					 var sum_app_plan = 0;
//					 var sum_fact_app = 0;
//					 var sum_fact_processing_db = 0;
//					 var sum_plan_processing_db = 0;
//					 for(i in response){
//					 	sum_fin_plan = sum_fin_plan + response[i].fin_plan_month;
//					 	sum_fact = sum_fact + response[i].fact;
//					 	sum_app_plan = sum_app_plan + response[i].app_plan_month;
//					 	sum_fact_app = sum_fact_app + response[i].fact_app;
//					 	sum_fact_processing_db = sum_fact_processing_db + response[i].fact_processing_db;
//					 	sum_plan_processing_db = sum_plan_processing_db + response[i].plan_processing_db;
//					 }
//					 var perc, perc_comp = '0 %', perc_comp_db = 0;
//					 if(sum_fin_plan != 0){
//					 	perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
//					 }
//					 if(sum_app_plan != 0){
//					 	perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
//					 }
//					 if(sum_plan_processing_db != 0){
//					 	perc_comp_db = ((sum_fact_processing_db/sum_plan_processing_db)*100).toFixed(2);
//					 }
//					 response.push({
//					 	project: {name: 'Итого'},
//					 	fin_plan_month: sum_fin_plan,
//					 	perc: perc.toString()+' %',
//					 	perc_comp: perc_comp.toString()+' %',
//					 	perc_comp_db: perc_comp_db.toString()+' %',
//					 	fact: sum_fact,
//					 	app_plan_month: sum_app_plan,
//					 	fact_app: sum_fact_app,
//					 	fact_processing_db: sum_fact_processing_db,
//					 	plan_processing_db: sum_plan_processing_db
//					 });
				},
				 rowStyle: function(row, index) {
				 	if(row.type == 'Итого'){
						return {
							classes: 'info'
						};
					};
					return {classes: ''}
				 }
			});
		},
	});
return PlanProjectManagerView
})
