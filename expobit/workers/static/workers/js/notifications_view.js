define(['text!workers/html/modal_view.html'], function(html){
	
	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#notifications_view')[0].outerHTML,
		events: {
			'click :checkbox': 'check'
		},
		// className: 'panel panel-default form',
		initialize: function(){
			
		},
		check: function(e){
			if($(e.currentTarget).is(':checked')){
				this.SelectManager[0].selectize.setValue('all');
				this.SelectManager[0].selectize.disable();
				this.$el.find('input[name="time"]').attr('disabled', false);
			}
			else{
				this.$el.find('input[name="time"]').attr('disabled', true);
				this.SelectManager[0].selectize.enable();	
			}
		},
		onRender: function(){
			var _this = this;
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				valueField: 'user_id',
				options: [{user_id: 'all', text: 'Все'}],
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselectmessage/',
						success: function(response){
							for(i in response){
								if(response[i].user_id != user.id || user.isSuperuser){
									_this.addOption(response[i]);
								}
							}
						}
					});
				},
				onChange: function(value){
				},
			});
		},
	});
return View;
});
