define(['text!workers/html/main_view.html', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize'], function(html){
	
	var AdminUserControl = Backbone.Marionette.View.extend({
		template:$(html).filter('#admin_user_control_view')[0].outerHTML,
		events: {
		},
		className: 'displayNone',
		templateContext: function(){
			
		},
		initialize: function(){
		},
		updateData: function(data){
			if(data.id && !data.user){
				this.Table.bootstrapTable('removeByUniqueId', data.id);
				return;
			}
			if(!this.Table.bootstrapTable('getRowByUniqueId', data.id)){
				this.Table.bootstrapTable('append', {
					id: data.id, counter: data.user.counter, 
					user: data.name, 
					reply_channel: data.user.reply_channel
				});
			}
			this.Table.bootstrapTable('updateByUniqueId', {
				id: data.id, 
				row:{
					counter: data.user.counter,
					reply_channel: data.user.reply_channel,
					update: true,
					}
				});
		},
		formatTime: function(time){
			time = parseInt((time / 1000).toFixed());
			if(time < 60){
				return time + ' с.';
			}
			if(time >= 60 && time < 3600){
				return Math.floor(time/60) + 'м. ' + time % 60 + 'с.';
			}
			if(time >= 3600){
				return Math.floor(time/3600) + 'ч. ' + Math.floor((time%3600)/60) + 'м. ' + time % 60 + 'с.';
			}
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			
			this.Table = this.$el.find('#UserControlTable').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiworkers/user_control',
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'user',
					title: 'Пользователь',
					sortable: true,
				},
				{
					field: 'counter',
					title: 'Активных окон',
					sortable: true,
				},
				{
					field: 'tabs',
					title: 'Вкладки',
					formatter: function(value, row, index){
						var tabs = row.reply_channel;
						var html = '';
						for(var i in tabs){
							if(tabs[i].open_tab){
								html = html + '<span class="glyphicon glyphicon-eye-open"></span>'
							}
							else{
								html = html + '<span class="glyphicon glyphicon-eye-close"></span>'
							}
						}
						return html;
					}
				},
				{
					field: 'href',
					title: 'Страница',
					formatter: function(value, row, index){
						var tabs = row.reply_channel;
						var html = '';
						for(var i in tabs){
							if(tabs[i].href){
								html = html + '<a href="'+tabs[i].href+'">'+tabs[i].href+'</a><br>';
							}
						}
						return html;
					}
				},
				{
					field: 'active',
					title: 'Активность',
					formatter: function(value, row, index){
						var tabs = row.reply_channel;
						var html = '';
						var time, color;
						for(var i in tabs){
							if(tabs[i].counter_hash != 0){
								time = _view.formatTime(Math.floor(tabs[i].active_time/tabs[i].counter_hash));
								if(Math.floor(tabs[i].active_time/tabs[i].counter_hash) < 60000){
									color = 'green';
								}
								else if(Math.floor(tabs[i].active_time/tabs[i].counter_hash) >= 60000 && Math.floor(tabs[i].active_time/tabs[i].counter_hash) < 180000){
									color = '#f39c12';
								}
								else{
									color = 'red';
								}
								html = html + '<i style="color: '+color+'">'+_view.formatTime(Math.floor(tabs[i].active_time/tabs[i].counter_hash))+ '</i><br>';
							}
						}
						return html;
					}
				},
				{
					field: 'active_time',
					title: 'Акт. время',
					formatter: function(value, row, index){
						var tabs = row.reply_channel;
						var html = '';
						for(var i in tabs){
							if(tabs[i].counter_hash != 0){
								html = html + _view.formatTime(tabs[i].active_time) + '<br>';
							}
						}
						return html;
					}
				},
				{
					field: 'deactive_time',
					title: 'Пас. время',
					formatter: function(value, row, index){
						var tabs = row.reply_channel;
						var html = '';
						for(var i in tabs){
							if(tabs[i].counter_hash != 0){
								html = html + _view.formatTime(tabs[i].deactive_time) + '<br>';
							}
						}
						return html;
					}
				},
				],
				onClickRow: function(row, e, index){
					// if(index!='edit'){
					// 	router.navigate('project/'+row.project.id+'/edit', {trigger: true});
					// }
				},
				responseHandler: function(response){
					
					return response;
				},
				rowStyle: function(row, index) {
					if(row.update){
						row.update = null;
						setTimeout(function(){
							_view.$el.find('tr[data-uniqueid="'+row.id+'"]').removeClass('info');
						}, 600);
						return {
							css: {"cursor": "pointer"},
							classes: 'info'
						};
					}
					return {
						css: {"cursor": "pointer"},
					};
				}
			});
		},
	});
return AdminUserControl;
});
