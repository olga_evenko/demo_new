define(['text!workers/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize'], function(html, utils){

	var ReportHistoryContactsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#report_history_contact_view')[0].outerHTML,
		events: {
			'click #getReport': 'getReport'
		},
		className: 'displayNone',
		initialize: function(){
		},
		getReport: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiworkers/report_contact_client/?'+this.$el.find('form').serialize()});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				pageList: [10,25,50,100,'All'],
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				columns: [
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'group',
					title: 'Группа'
				},
				{
					field: 'results.result',
					title: 'Результат',
					// sortable: true,
					// escape: 
				},
				{
					field: 'results.comment',
					title: 'Коментарии',
				},
				{
					field: 'manager',
					title: 'Менеджер',
				}],
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				}
			});
			this.SelectResult = this.$el.find('#SelectResult').selectize({
				options: [
					{
						text: 'Отказ', 
						value: 'failure_bool'
					},
					{
						text: 'Заявка', 
						value: 'app'
					},
					{
						text: 'Интерес', 
						value: 'interest'
					},
				]
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/?all=true',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							// if(_view.options.project){
							// 	_this.setValue(_view.options.project);
							// };
						}
					});
				},
				onChange: function(value){
					_view.$el.find('#getReport').attr('disabled', false);
				},
			});
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselect/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							// if(_view.options.project){
							// 	_this.setValue(_view.options.project);
							// };
						}
					});
				},
				onChange: function(value){
				},
			});
		}
	});
	return ReportHistoryContactsView
})
