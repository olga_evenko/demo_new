define(['text!workers/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){
	
	var PlanManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#plan_manager_view')[0].outerHTML,
		events: {
			'change #setMonth': 'setMonth',
		},
		className: 'displayNone',
		initialize: function(){

		},
		templateContext: function(){
			return {curr_date: new Date().toJSON().substr(0,7)};
		},
		setMonth: function(e){
			var date = $(e.currentTarget).val();
			this.Table.bootstrapTable('refresh', {
				url: '/apiworkers/plans_manager/?all=true&manager='+user.manager_id+'&month='+date
			});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			$.get({
                url: '/apiprojects/subjects/',
                success: function(response){
                    _view.subjects = response
                }
            });
            $.get({
                url: '/apiworkers/reduction_factor/',
                success: function(response){
                    _view.reduction_factor = response
                }
            });
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiworkers/plans_manager/?all=true&manager='+user.manager_id,
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'project',
					title: 'Проекты в работе',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений ДС',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'bonus_100',
					title: 'Бонус план',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'warning',
						}
					},
                    formatter: function(value, row, index, field){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
					}
				},
				{
					field: 'bonus',
					title: 'Бонус на сегодня',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'warning',
						}
					},
                    formatter: function(value, row, index, field){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }

                        if(row.bonus_text){
                            value += ` <span title="${row.bonus_text}" class="glyphicon glyphicon-question-sign"></span>`
                        }

                        return value
					}
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'perc_comp',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'download',
					title: 'Отчеты',
					align: 'center',
					formatter: function(value, row, index, field){
						if(row.project != 'Итого'){
							return '<a data-toggle="tooltip" title="Создание каталога" target="_balnk" href="/report/app?type=expo&project='+
							row.project_id+'"><span class="glyphicon glyphicon-download-alt"></span></a>'+
							'<a style="padding-left:5px;" data-toggle="tooltip" title="Отчет по фризам" target="_balnk" href="/report/freeze?type=expo&project='+
							row.project_id+'"><span class="glyphicon glyphicon-download-alt"></span></a>'+
							'<a style="padding-left:5px;" data-toggle="tooltip" title="Отчет по оборудованию" target="_balnk" href="report/rig?type=expo&project='+
							row.project_id+'"><span class="glyphicon glyphicon-download-alt"></span></a>'+
							'<a style="padding-left:5px;" data-toggle="tooltip" title="Информация по экспонатам" target="_balnk" href="report/exponat?type=expo&project='+
							row.project_id+'"><span class="glyphicon glyphicon-download-alt"></span></a>'+
							'<a style="padding-left:5px;" data-toggle="tooltip" title="Информация по дипломам" target="_balnk" href="report/diploma?type=expo&project='+
							row.project_id+'"><span class="glyphicon glyphicon-download-alt"></span></a>';
						}
					}
				},
				],
				rowStyle: function(row, index){
					if(row.project == 'Итого'){
						return {
							classes: 'info',
							css: {}
						};
					}
					return {
						classes: '',
						css: {}
					};
				},
				responseHandler: function(response){
				    _view
					// var results = response.results;
					var sum_fin_plan = 0;
					var sum_fact = 0;
					var sum_app_plan = 0;
					var sum_fact_app = 0;
					var sum_fact_processing_db = 0;
					var sum_plan_processing_db = 0;
					var bonus=0
					var bonus_100=0
					for(i in response){
						sum_fin_plan = sum_fin_plan + response[i].fin_plan_month;
						sum_fact = sum_fact + response[i].fact;
						sum_app_plan = sum_app_plan + response[i].app_plan_month;
						sum_fact_app = sum_fact_app + response[i].fact_app;
						sum_fact_processing_db = sum_fact_processing_db + response[i].fact_processing_db;
						sum_plan_processing_db = sum_plan_processing_db + response[i].plan_processing_db;

                        //рассчет премии
						var coefficients = _view.subjects[_view.subjects.map(function(o) { return o.id; }).indexOf(response[i].project_subject)]
						var coefficients_reduction = {}

						var percent = +(response[i].fact /response[i].fin_plan_month*100).toFixed(1)

						_view.reduction_factor.some(function(el) {
						    coefficients_reduction = el
                            if(percent>=el.min_value && percent<=el.max_value){return true}
                            if(percent>=el.min_value && el.max_value==null){return true}
                        });
                        response[i].bonus_100 = (response[i].fin_plan_month * coefficients.per_im_plan/100).toFixed()
						if (response[i].fact /response[i].fin_plan_month>1){
                            response[i].bonus = (response[i].fin_plan_month * coefficients.per_im_plan/100  + (response[i].fact-response[i].fin_plan_month) * coefficients.per_over_plan/100).toFixed()
                            response[i].bonus_text = `${response[i].fin_plan_month} * ${coefficients.per_im_plan}% +(${response[i].fact}-${response[i].fin_plan_month})*${coefficients.per_over_plan}%`
						}else{
                            response[i].bonus = (response[i].fact * coefficients.per_im_plan/100 * coefficients_reduction.m1).toFixed()
                            response[i].bonus_text = `${response[i].fact}  * ${coefficients.per_im_plan}% * ${coefficients_reduction.m1}`
						}
						bonus += +response[i].bonus
						bonus_100 += +response[i].bonus_100

					}
					var perc = 0, perc_comp = 0, perc_comp_db = 0;
					if(sum_fin_plan != 0){
						perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
					}
					if(sum_app_plan != 0){
						perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
					}
					if(sum_plan_processing_db != 0){
						perc_comp_db = ((sum_fact_processing_db/sum_plan_processing_db)*100).toFixed(2);
					}
					response.push({
						project: 'Итого',
						fin_plan_month: sum_fin_plan,
						perc: perc.toString()+' %',
						perc_comp: perc_comp.toString()+' %',
						perc_comp_db: perc_comp_db.toString()+' %',
						fact: sum_fact,
						app_plan_month: sum_app_plan,
						fact_app: sum_fact_app,
						fact_processing_db: sum_fact_processing_db,
						plan_processing_db: sum_plan_processing_db,
						bonus:bonus,
						bonus_100:bonus_100,
					});
					return response;
				},
			});
		},
	});
return PlanManagerView
})
