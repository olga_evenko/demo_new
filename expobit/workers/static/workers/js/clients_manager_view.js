define(['text!workers/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'table-context-menu', 'selectize'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#main_client_view')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .eye': 'openClient',
			'click .filter_button_client': 'setFilter'
		},
		className: 'displayNone',
		templateContext: function(){
			if(user.groups.indexOf('project_managers')!=-1){
				return {show_manager: true};
			}
		},
		openClient: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
		},
		setClassActive: function(button){
			var buttons = this.$el.find('.filter_button_client').toArray();
			for(i in buttons){
				if($(buttons[i]).hasClass('btn-success')){
					$(buttons[i]).removeClass('btn-success');
					$(buttons[i]).addClass('btn-default');
				};
			};
			if(button){
				$(button).addClass('btn-success');
				$(button).removeClass('btn-default');
			};
		},
		setFilter: function(e){
			var _view = this;
			var button = $(e.currentTarget);
			var id = button[0].id;
			if(id=='key_client' && this.SelectProject[0].selectize.getValue()){
				this.query = {
					status: 'key',
					membership__project: _view.SelectProject[0].selectize.getValue()
				};
				if(_view.SelectProject[0].selectize.getValue()=='all'){
					this.query = {all: _view.SelectProject[0].selectize.getValue(), status: 'key'}
				}
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				this.setClassActive(button);
			}
			if(id=='not_call' && _view.SelectProject[0].selectize.getValue()){
				this.query = {
					not_call: 'true',
					membership__project: _view.SelectProject[0].selectize.getValue(),
					client_group__group: _view.SelectGroup[0].selectize.getValue(),
					client_group__subjects: _view.SelectGroup[0].selectize.getValue()?_view.SelectProject[0].selectize.options[_view.SelectProject[0].selectize.getValue()].subjects:'',
					client_activitys: _view.SelectActivities[0].selectize.getValue(),
					membership__manager: _view.SelectManager[0].selectize.getValue()
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});	
				this.setClassActive(button);
			}
			if(id=='participants' && _view.SelectProject[0].selectize.getValue()){
				this.query = {
					app__isnull: 0,
					membership__project: _view.SelectProject[0].selectize.getValue(),
					client_group__group: _view.SelectGroup[0].selectize.getValue(),
					client_group__subjects: _view.SelectGroup[0].selectize.getValue()?_view.SelectProject[0].selectize.options[_view.SelectProject[0].selectize.getValue()].subjects:'',
					client_activitys: _view.SelectActivities[0].selectize.getValue(),
					membership__manager: _view.SelectManager[0].selectize.getValue()
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});	
				this.setClassActive(button);
			}
			if(id=='key_client_fail' && this.SelectProject[0].selectize.getValue()){
				this.query = {
					status: 'key',
					contact__project: this.SelectProject[0].selectize.getValue(),
					contact__failure_bool: 2,
					membership__project: _view.SelectProject[0].selectize.getValue()
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});	
				this.setClassActive(button);
			}
			if(id=='client_phone_future' && _view.SelectProject[0].selectize.getValue()){
				this.query = {
					call_soon: 'true',
					membership__project: _view.SelectProject[0].selectize.getValue()
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				this.setClassActive(button);
			}
			if(id=='client_not_pay' && this.SelectProject[0].selectize.getValue()){
				this.query = {
					app__status: 'working',
					membership__project: _view.SelectProject[0].selectize.getValue()
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				this.setClassActive(button);
			}
			if(id=='client_interest_not_app' && this.SelectProject[0].selectize.getValue()){
				this.query = {
					no_app: '2',
					membership__project: _view.SelectProject[0].selectize.getValue(),
					contact__interest: '2'
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				this.setClassActive(button);
			}
			if(id == 'client_all'){
				this.Table.bootstrapTable('refresh', {url: '/apiclients/clients/'});
				this.setClassActive(button);
			}
			if(id=='client_exclude_black_list' && this.SelectProject[0].selectize.getValue()){
				this.query = {
					status_exclude: 'black_list',
					membership__project: _view.SelectProject[0].selectize.getValue()
				};
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				this.setClassActive(button);
			}
			if(id=='resetfilterButton'){
				this.query = {membership__project: _view.SelectProject[0].selectize.getValue()};
				_view.SelectGroup[0].selectize.clear();
				_view.SelectActivities[0].selectize.clear();
				_view.SelectManager[0].selectize.clear();
				if(_view.SelectProject[0].selectize.getValue()=='all'){
					this.query = {all: _view.SelectProject[0].selectize.getValue()}
				}
				if(_view.SelectProject[0].selectize.getValue() == ''){
					this.Table.bootstrapTable('removeAll');
					this.setClassActive();
					return;
				}
				this.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				this.setClassActive();
			}
		},
		addIndividual: function(){
			router.navigate('client/create', {trigger: true});
		},
		editIndividual: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('client/'+row.id+'/edit', {trigger: true});
		},
		initialize: function(){
			this.resetUrl = false;
			this.query = {};
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			var columns = [
				{
					field: 'index',
					title: '№',
					formatter: function(value, row, index, field){
						if(row['Дата'] != 'Итого'){
							return index + 1;
						}
						return null;
					}
				},
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'status',
					title: 'Статус',
					sortable: true,
					formatter: function(value, row, index){
						var groups = {
							'active': 'Активный',
							'black_list': 'Черный список',
							'inactive': 'Неактивный',
							'key': 'Ключевой'
						};
						return groups[value];
					}
				},
				{
					sortable: true,
					field: 'group',
					title: 'Группа',
				},
				{
					sortable: true,
					field: 'date_last_contact_proj',
					title: 'Дата последнего контакта'
				},
				{
					field: 'result_last_contact',
					title: 'Результат последнего контакта'
				},
				{
					// sortable: true,
					field: 'new_contact.date',
					title: 'Дата следующего контакта',
					sortable: true,
					formatter: function(value, row, index){
						return utils.dateFormatter(value);
					}
				},
				{
					field: 'new_contact.goal',
					title: 'Цель контакта'
				},
				// {
				// 	field: 'edit',
				// 	title: '',
				// 	formatter: function(value, row, index){
				// 		return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
				// 		row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
				// 		row.id+'></span></span></div>'
				// 	},
				// 	width: '89px'
				// }
			];
			if(user.groups.indexOf('project_managers') != -1){
				columns[5].sortable = true;
				columns.splice(columns.length-1, 0, {
					field: 'manager',
					title: 'Менеджер'
				});
				columns.splice(columns.length-1, 0, {
					field: 'address',
					title: 'Адрес'
				});
			};
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				columns: columns,
				contextMenu: '#context-menu',
				onContextMenuItem: function(row, $el){
					if($el.data("item") == "show_history"){
						require(['workers/js/modals/history_client_view', 'projects/js/modal_template'], function(View, tmp) {
							var modal = new Backbone.BootstrapModal({
								content: new View({model: new Backbone.Model(row)}), 
								title: row.name,
								template: tmp,
								okText: 'Закрыть',
								cancelText: '',
								animate: true, 
							}).open();
							modal.on('ok', function(){

							});
						});
					}
				},
				onContextMenuRow: function(row, $el){
					this.right_click = true;
				},
				// url: '/apiworkers/clients/',
				onPageChange: function(num, size){
					router.navigate('my_clients/p'+num+'-'+size+'/project'+_view.SelectProject[0].selectize.getValue(), {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				onClickRow: function(row, e, index){
					if(index!='edit' && !this.right_click){
						router.navigate('client/'+row.id, {trigger: true});
						sessionStorage.setItem('client_project_select', _view.SelectProject[0].selectize.getValue());
						window.appData.clients[row.id] = {template: row.new_contact.goal};
					}
					this.right_click = false;
				},
				rowStyle: function(row, index) {
					var classes = '';
					if(row.new_contact && row.new_contact.date){
						if(new Date(row.new_contact.date).toDateString() == new Date().toDateString()){
							classes = 'warning';
						}
						else{
							if(new Date(row.new_contact.date) < new Date()){
								classes = 'danger';
							}
						}
					}
					return {
						classes: classes,
						css: {"cursor": "pointer"}
					};
				},
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'date_last_contact.date'){
						params.sort = 'contact__date'
					};
					if(params.sort == 'new_contact.date'){
						params.sort = 'new_contacts__date_new_contact'
					};
					if(params.sort == 'group'){
						params.sort = 'client_group__group';
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, _view.query);
					}
					return Object.assign(params, _view.query);
				},
				toolbar: $(html).filter('#toolbar')[0].innerHTML
			});
			this.SelectGroup = this.$el.find('#SelectGroup').selectize({
				options: [{text:'VIP', value: 'vip'}, {text:'A', value: 'a'}, {text:'B', value: 'b'}, {text:'C', value: 'c'}],
				onChange: function(value){
					if(value != ''){
						_view.query = {
							membership__project: _view.SelectProject[0].selectize.getValue(),
							client_group__group: value,
							client_activitys: _view.SelectActivities[0].selectize.getValue(),
							membership__manager: _view.SelectManager[0].selectize.getValue(),
							entitys__activities: _view.SelectAct[0].selectize.getValue(),
							client_group__subjects: _view.SelectProject[0].selectize.options[_view.SelectProject[0].selectize.getValue()].subjects
						};
						if(_view.SelectProject[0].selectize.getValue() != ''){
							_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
						}
					}
				}
			});
			this.SelectAct = this.$el.find('#SelectAct').selectize({
				valueField: 'id',
				labelField: 'subcategories',
				searchField: ['subcategories', 'value'],
				optgroupField: 'category',
				optgroupLabelField: 'display_name',
				onInitialize: function(){
					var _this = this;
					$.ajax({
						url: '/apiprojects/activities/',
						type: 'OPTIONS',
						cache: true,
						success: function(response){
							var optGr = response.actions.POST.category.choices;
							for (i in optGr){
								_this.addOptionGroup(optGr[i].value, optGr[i]);
							}
							$.get({
								url: '/apiprojects/activities/',
								async: true,
								success: function(response) {
									for(var i=0; i<response.length; i++){
										_this.addOption(response[i]);
									}
									return response;
								}
							});
						}
					})
				},
				onChange: function(value){
					// if(value != ''){
						_view.query = {
							membership__project: _view.SelectProject[0].selectize.getValue(),
							client_group__group: _view.SelectGroup[0].selectize.getValue(),
							client_group__subjects: _view.SelectGroup[0].selectize.getValue()?_view.SelectProject[0].selectize.options[_view.SelectProject[0].selectize.getValue()].subjects:'',
							client_activitys: _view.SelectActivities[0].selectize.getValue(),
							membership__manager: _view.SelectManager[0].selectize.getValue(),
							entitys__activities: value
						};
						if(_view.SelectProject[0].selectize.getValue() != ''){
							_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
						}		
					// }
				},
			});
			this.SelectActivities = this.$el.find('#SelectActivities').selectize({
				valueField: 'id',
				labelField: 'activity',
				onInitialize: function(){
					var _this = this;
					
					$.get({
						url: '/apiclients/client_activity/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							// if(_view.options.project){
							// 	_this.setValue(_view.options.project);
							// };
						}
					});
				},
				onChange: function(value){
					if(value != ''){
						_view.query = {
							membership__project: _view.SelectProject[0].selectize.getValue(),
							client_group__group: _view.SelectGroup[0].selectize.getValue(),
							client_group__subjects: _view.SelectGroup[0].selectize.getValue()?_view.SelectProject[0].selectize.options[_view.SelectProject[0].selectize.getValue()].subjects:'',
							client_activitys: value,
							membership__manager: _view.SelectManager[0].selectize.getValue(),
							entitys__activities: _view.SelectAct[0].selectize.getValue(),
						};
						if(_view.SelectProject[0].selectize.getValue() != ''){
							_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
						}		
					}

				},
			});
			this.SelectActivities[0].selectize.disable();
			this.SelectGroup[0].selectize.disable();
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiworkers/managers/forselect/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					_view.query = {
						membership__manager: value,
						membership__project: _view.SelectProject[0].selectize.getValue(),
						client_group__group: _view.SelectGroup[0].selectize.getValue(),
						client_group__subjects: _view.SelectGroup[0].selectize.getValue()?_view.SelectProject[0].selectize.options[_view.SelectProject[0].selectize.getValue()].subjects:'',
						client_activitys: _view.SelectActivities[0].selectize.getValue(),
						entitys__activities: _view.SelectAct[0].selectize.getValue(),
					};
					_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
				}
			});
			this.SelectManager[0].selectize.disable();
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				onInitialize: function(){
					var _this = this;
					if(user.groups.indexOf('project_managers')!=-1){
						this.addOption({name: 'Все', id: 'all'});
					};
					$.get({
						url: '/apiworkers/projects/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							if(_view.options.project){
								_this.setValue(_view.options.project);
							};
						}
					});
				},
				onChange: function(value){
					_view.SelectAct[0].selectize.clearOptions();
					_view.SelectAct[0].selectize.clearOptionGroups();
					$.get({
								url: '/apiprojects/activities/?project_clients='+value,
								async: true,
								success: function(response) {
									for(var i=0; i<response.length; i++){
										_view.SelectAct[0].selectize.addOption(response[i]);
									}
									return response;
								}
							});
					if(value == ''){
						return;
					}
					
					_view.SelectActivities[0].selectize.enable();
					_view.SelectGroup[0].selectize.enable();
					_view.SelectManager[0].selectize.enable();
					if(value=='all'){
						_view.query = {all: value}
						$(".pull-left :not(#key_client,#resetfilterButton,#client_all)").attr('disabled', true)
					}
					else{
						_view.query = {membership__project: value};	
						$(".pull-left :not(#key_client,#resetfilterButton,#client_all)").attr('disabled', false)
					};
					_view.Table.bootstrapTable('refresh', {url: '/apiworkers/clients/'});
					router.navigate('my_clients/p'+_view.options.page_num+'-'+
						_view.options.page_size+'/project'+value, {replace: true});
					$('#my_clients').attr('href', '#my_clients/p'+_view.options.page_num+'-'+
						_view.options.page_size+'/project'+value);
					_view.setClassActive();
				},
			});
		}
	});
	return ClientsView
})
