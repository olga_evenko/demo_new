define(['text!workers/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize'], function(html, utils){

	var EntitysView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_entity')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .trash': 'removeEntity'
		},
		className: 'displayNone',
		addIndividual: function(){
			router.navigate('entity/create', {trigger: true});
		},
		editIndividual: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('entity/'+row.id, {trigger: true});
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			
			this.SelectProjectType = this.$el.find('#SelectProjectType').selectize({
				labelField: 'type',
				valueField: 'id',
				searchField: 'type',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: 'apiprojects/projects_types/',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					$.get({
						url: 'apiprojects/projects/?data=min&project_type='+value,
						success: function(response){
							_view.SelectProject[0].selectize.clearOptions();
							_view.SelectProject[0].selectize.addOption({name: 'Все', id: 'all'});
							for(var i in response){
								_view.SelectProject[0].selectize.addOption(response[i]);
							}
						}
					});
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				labelField: 'name',
				valueField: 'id',
				searchField: 'name',
				onInitialize: function(){
					// var _this = this;
					// $.get({
					// 	url: 'apiprojects/projects/?data=min&project_type=5',
					// 	success: function(response){
					// 		for(var i in response){
					// 			_this.addOption(response[i]);
					// 		}
					// 	}
					// });
				},
				onChange: function(value){
					if(!value){
						return;
					}
					_view.Table.bootstrapTable('refresh', {url: '/apiworkers/entity_private_merge/?project_type='+_view.SelectProjectType[0].selectize.getValue()+'&project='+value});
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// url: '/apiclients/entity_private_merge/',
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				columns: [
				// {
				// 	checkbox: true
				// },
				{
					field: 'full_name',
					title: 'Полное название',
					sortable: true,
				},
				{
					field: 'bill_number',
					title: 'Номер счета',
				},
				{
					field: 'manager_id',
					title: 'менеджер ID',
					sortable: true,
				},
				{
					field: 'project_id',
					title: 'Проект ID',
					sortable: true,
				},
				{
					field: 'app_id',
					title: 'заявка id',
					sortable: true,
				},
				{
					field: 'client',
					title: 'Клиент ID',
					sortable: true,
				},

				],
				onClickRow: function(row){
					router.navigate('entity/'+row.id, {trigger: true});
				},
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		}
	});
	return EntitysView
})
