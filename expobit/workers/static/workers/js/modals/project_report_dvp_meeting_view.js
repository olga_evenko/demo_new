define(['text!workers/html/main_view.html', 'utils', 'chartjs', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils, Chart){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_indicators_meeting_view')[0].outerHTML,
		events: {
		},
		regions: {
			managers: '#managers_region',
			details: '#details_region'
		},
		className: '',
		initialize: function(){
		},
		digitValue: function(value){
			value = value.toString().replace(/ /g,'');
		    var number = value;
		    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		    return value;
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			// $.get({
			// 	url: 'apiprojects/project_indicators/?type=expo&project='+this.model.id,
			// 	success: function(response){
			// 		_this.$el.find('#summ_app').append(_this.digitValue(parseFloat(response[0].average_cost.value).toFixed())+' / '+_this.digitValue(parseFloat(response[0].average_cost.fact).toFixed()));
			// 	}
			// });
			// this.$el.append($('<div class="row"><div class="col-md-4"></div><div class="col-md-4"><h5 style="text-align: center;">Прозвон базы по проекту</h5><br><canvas id="Chart" style="width: 100%;"></canvas></div><div class="col-md-4"></div></div>'));
			// var GROUPS = ['Всего', 'VIP', 'A', 'B', 'C'];
			// var color = Chart.helpers.color;
			// this.config = {
			// 	type: 'bar',
			// 	data:{
			// 		labels: ['Всего', 'VIP', 'A', 'B', 'C'],
			// 		datasets: [{
			// 			label: 'Процент',
			// 			backgroundColor:[
			// 						window.chartColors.red,
			// 						window.chartColors.blue,
			// 						window.chartColors.green,
			// 						window.chartColors.orange,
			// 						window.chartColors.purple,
			// 					],
			// 			borderColor: window.chartColors.red,
			// 			// borderWidth: 1,
			// 			data: []
			// 		}]
			// 	},
			// 	options: {
			// 		responsive: true,
			// 		legend: {
			// 			position: 'bottom'
			// 		},
			// 		tooltips: {
			//             callbacks: {
			//                 label: function(tooltipItem, data) {
			//                     return data.datasets[tooltipItem.datasetIndex].data[0] + ' %';
			//                 }
			//             }
			//         }
			// 	}
				

			// };
			this.index_id = {};
			// this.chart = new Chart(this.$el.find('#Chart'), this.config);
			this.GroupTable = this.$el.find('#groups_table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				columns: [
					{
						field: 'group',
						title: 'Группа',
					},
					{
						field: 'all',
						title: 'Всего',
					},
					{
						field: 'process',
						title: 'Обработано',
					},
					{
						field: 'progress',
						title: '%',
						formatter: function(value, row, index){
							if(!value){
								return;
							}
							var progress = parseFloat(value);
							var class_name;
							var style_name = '';
							if(parseFloat(value) < 25){
								style_name = 'color: #39434e;';
								class_name = 'progress-bar progress-bar-danger progress-bar-striped active';
							}
							else if(parseFloat(value) < 75){
								class_name = 'progress-bar progress-bar-striped active';
							}
							else{
								class_name = 'progress-bar progress-bar-success progress-bar-striped active';
							}
							return '<div class="progress" style="margin-bottom: 0px; height: 25px;"><div class="'+class_name+'" role="progressbar"aria-valuenow="'+
							progress+'" aria-valuemin="0" aria-valuemax="100" style="width:'+progress+'%; padding-top: 4px; '+style_name+'">'+value+' %</div></div>';
						},
					},
				]
			});
			$.get({
				url: 'apiworkers/report_contact_by_group/?project='+this.model.id,
				success: function(response){
					var percent_all = parseInt((((response.vip+response.a+response.b+response.c+response.null)/response.all)*100).toFixed());
					var vip = parseInt(((response.vip/response.all_vip)*100).toFixed());
					var a = parseInt(((response.a/response.all_a)*100).toFixed());
					var b = parseInt(((response.b/response.all_b)*100).toFixed());
					var c = parseInt(((response.c/response.all_c)*100).toFixed());
					var null_group = 0;
					if(response.all_null != 0){
						null_group = parseInt(((response.null/response.all_null)*100).toFixed());
						
					}
					var arr_table = [
						{
							'group': 'VIP',
							'all': response.all_vip,
							'process': response.vip,
							'progress': vip
						},
						{
							'group': 'A',
							'all': response.all_a,
							'process': response.a,
							'progress': a
						},
						{
							'group': 'B',
							'all': response.all_b,
							'process': response.b,
							'progress': b
						},
						{
							'group': 'C',
							'all': response.all_c,
							'process': response.c,
							'progress': c
						},
						{
							'group': 'Группа не назначена',
							'all': response.all_null,
							'process': response.null,
							'progress': null_group
						},
						{
							'group': 'Итого',
							'all': response.all_vip+response.all_a+response.all_b+response.all_c+response.all_null,
							'process': response.vip+response.a+response.b+response.c+response.null,
							'progress': percent_all
						},

					];
					_this.GroupTable.bootstrapTable('load', arr_table);
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/project_reports/table/?show_bill=true&project='+_this.model.id,
				rowStyle: function(row, index) {
					_this.index_id[index] = row.id;
					var now_date = new Date();
					var row_date = new Date(row.month_date);
					var classes = '';
					if(now_date.getMonth() == row_date.getMonth() && now_date.getFullYear() == row_date.getFullYear()){
						classes = classes + 'success';
					}
					if(index == 0){
						return{
							classes: classes + ' no-editble',
							css: {'font-weight': 'bold', 'cursor': 'pointer'}
						};
					}
					return {classes: classes, css: {'cursor': 'pointer'}};
				},
				onClickRow: function(row, e, index){
					var _table = this;
					if(index == 'approval_status' && _this.options.status == 'working'){
						require(['workers/js/modals/approval_view'], function(View){
							var modal = new Backbone.BootstrapModal({ 
								content: new View({report: row.id}),
								title: 'Выберите действие',
								okText: 'Сохранить',
								cancelText: 'Отмена',
								animate: true, 
							}).open();
							modal.on('ok', function(){
								var form = modal.$el.find('form')[0];
								$('.modal.fade.in').css('overflow-y', 'auto');
								$.ajax({
									type: 'PATCH',
									data: new FormData(form),
									url: 'apiprojects/project_reports/'+row.id+'/',
									contentType: false,
									processData: false,
									success: function(response){
										_this.updateGlobal = true;
										_this.Table.bootstrapTable('refresh');
									}
								});
							});
							modal.on('cancel', function(){
								$('.modal.fade.in').css('overflow-y', 'auto');
							});
						});
						return;
					}
					require(['workers/js/director_plan_manager_by_project'], function(TableManagersView){
						_this.showChildView('managers', new TableManagersView({
							report: row.id, 
						}));
						// $('.modal.fade.in').animate({ scrollTop: $(document).height() }, 1000);
					});
					require(['workers/js/report_by_week_view'], function(TableManagersView){
						var row_index = e.data('index');
						var sum = 0;
						for(var i=1; i<row_index; i++){
							sum = sum + _table.data[i].fact;
						}
						_this.showChildView('details', new TableManagersView({
							prev_row: sum,
							row: row,
							report: row.id, 
							project: _this.model.id,
							min: true
						}));
						$('.modal.fade.in').animate({ scrollTop: $('#details_region').offset().top-270 }, 2000);
					});
				},
				columns: [
				// {
				// 	field: 'approval_status',
				// 	title: '',
				// 	formatter: function(value, row, index){
				// 		if(!user.isSuperuser && value == ''){
				// 			return '<span class="glyphicon glyphicon-transfer" title="Отправить на согласование"></span>';
				// 		}
				// 		if(!user.isSuperuser && value == 'return'){
				// 			return '<span class="glyphicon glyphicon-question-sign" title="Возврат'+'Причина'+'"></span>';
				// 		}
				// 		if(!user.isSuperuser && value == 'on_approval'){
				// 			return '<span class="glyphicon glyphicon-question-sign" title="На рассмотрении"></span>';
				// 		}
				// 		if(!user.isSuperuser && value == 'approved_by'){
				// 			return '<span class="glyphicon glyphicon-transfer" title="Утверждено"></span>';
				// 		}
				// 		if(value == 'on_approval'){
				// 			return ' <span class="glyphicon glyphicon-exclamation-sign"></span>';
				// 		}
				// 		if(value == 'return'){
				// 			return '<span class="glyphicon glyphicon-question-sign"></span>';
				// 		}
				// 		if(value == 'approved_by'){
				// 			return '<span class="glyphicon glyphicon-ok-sign"></span>';
				// 		}
				// 		return '';
				// 	}
				// },
				{
					field: 'month_name',
					title: '',
					// sortable: true,
				},
				// {
				// 	field: 'fin_plan_month',
				// 	title: 'Финансовый план на весь период',
				// 	formatter: function(value, row, index){

				// 		if(value){
				// 			value = value.toString().replace(/ /g,'');
				// 		    var number = value;
				// 		    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				// 		}
				// 		return value;
				// 	},
				// },
				{
					field: 'fin_plan',
					title: 'План поступлений',
					// formatter: function(value, row, index){
					// 	debugger;
					// },
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: ''
						};
					}
					// sortable: true,
				},
				{
					field: 'fact',
					title: 'Факт поступлений',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				// {
				// 	field: 'diff',
				// 	title: 'Расхождение факта, с фин. планом',
				// 	formatter: function(value, row, index){
				// 		if(value){
				// 			value = value.toString().replace(/ /g,'');
				// 		    var number = value;
				// 		    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
				// 		}
				// 		return value;
				// 	},
				// 	// sortable: true,
				// },
				{
					field: 'perc',
					title: '%',
					formatter: function(value, row, index){
						if(row.fin_plan == 0){
							return undefined;
						}
						return ((row.fact / row.fin_plan) * 100).toFixed() + ' %';
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info'
						};
					}
				},
				// {
				// 	field: 'app_plan',
				// 	title: 'План поступления заявок на весь период',
				// },
				{
					field: 'app_plan_month',
					title: 'План заявки',
				},
				{
					field: 'fact_app',
					title: 'Факт заявки'
				},
				// {
				// 	field: 'app_diff',
				// 	title: 'Расхождение факта с планом'
				// },
				{
					field: 'perc_comp',
					title: '%',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info'
						};
					}
				},
				{
					field: 'fact_bill',
					title: 'Заявки в ДС',
					formatter: function(value, row, index){

						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'plan_processing_db',
					title: 'Всего клиентов',
				},
				{
					field: 'fact_processing_db',
					title: 'Обработано клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '%',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info'
						};
					}
				},
				],
				responseHandler: function(response){
					this.fact_sum = 0;
					this.sum_app = 0;
					var curr_date = new Date();
					var temp_date;
					for(var i=1; i<response.length; i++){
						temp_date = new Date(response[i].month_date);
						if(temp_date < curr_date && !(temp_date.getMonth() == curr_date.getMonth() && temp_date.getFullYear() == curr_date.getFullYear())){
							this.fact_sum += response[i].fin_plan;
							this.sum_app += response[i].app_plan_month;
						}
						if(temp_date.getMonth() == curr_date.getMonth() && temp_date.getFullYear() == curr_date.getFullYear()){
							this.fact_sum += Math.round((curr_date.getDate()/30)*response[i].fin_plan);
							this.sum_app += Math.round((curr_date.getDate()/30)*response[i].app_plan_month);
						}
					}
					_this.$el.find('#summ_app').append(_this.digitValue(this.fact_sum.toString())+' / '+_this.digitValue(response[0].fact));
					_this.$el.find('#summ_app_plan').append(_this.digitValue(this.sum_app.toString())+' / '+_this.digitValue(response[0].fact_app.toString()));
					_this.$el.find('#processin_db').append(_this.digitValue(response[0].fact_bill.toString()));
					if(response[0].fact/this.fact_sum>0.95){
						_this.$el.find('#summ_app').parent().addClass('alert-success');
					}
					if(response[0].fact/this.fact_sum<0.95 && response[0].fact/this.fact_sum>=0.8){
						_this.$el.find('#summ_app').parent().css({'background-color': '#f9e824', 'border-color': '#f9e824'});
					}
					if(response[0].fact/this.fact_sum<0.8){
						_this.$el.find('#summ_app').parent().addClass('alert-danger');
					}
					if(response[0].fact_app/this.sum_app>0.95){
						_this.$el.find('#summ_app_plan').parent().addClass('alert-success');
					}
					if(response[0].fact_app/this.sum_app<0.95 && response[0].fact_app/this.sum_app>=0.8){
						_this.$el.find('#summ_app_plan').parent().css({'background-color': '#f9e824', 'border-color': '#f9e824'});
					}
					if(response[0].fact_app/this.sum_app<0.8){
						_this.$el.find('#summ_app_plan').parent().addClass('alert-danger');
					}

					return response;
				},
			});
		}
	});
	return ProjectReportsView;
});