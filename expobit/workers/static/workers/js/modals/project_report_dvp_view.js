define(['text!workers/html/main_view.html', 'utils', 'chartjs', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils, Chart){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_indicators_view')[0].outerHTML,
		events: {
		},
		regions: {
			managers: '#managers_region',
			details: '#details_region'
		},
		className: '',
		initialize: function(){
		},
		digitValue: function(value){
			value = value.toString().replace(/ /g,'');
		    var number = value;
		    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		    return value;
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			$.get({
				url: 'apiprojects/project_indicators/?type=expo&project='+this.model.id,
				success: function(response){
					_this.$el.find('#summ_app').append(_this.digitValue(parseFloat(response[0].average_cost.value).toFixed())+' / '+_this.digitValue(parseFloat(response[0].average_cost.fact).toFixed()));
				}
			});
			this.$el.append($('<div class="row"><div class="col-md-4"></div><div class="col-md-4"><h5 style="text-align: center;">Прозвон базы по проекту</h5><br><canvas id="Chart" style="width: 100%;"></canvas></div><div class="col-md-4"></div></div>'));
			var GROUPS = ['Всего', 'VIP', 'A', 'B', 'C'];
			var color = Chart.helpers.color;
			this.config = {
				type: 'bar',
				data:{
					labels: ['Всего', 'VIP', 'A', 'B', 'C'],
					datasets: [{
						label: 'Процент',
						backgroundColor:[
									window.chartColors.red,
									window.chartColors.blue,
									window.chartColors.green,
									window.chartColors.orange,
									window.chartColors.purple,
								],
						borderColor: window.chartColors.red,
						// borderWidth: 1,
						data: []
					}]
				},
				options: {
					responsive: true,
					legend: {
						position: 'bottom'
					},
					tooltips: {
			            callbacks: {
			                label: function(tooltipItem, data) {
			                    return data.datasets[tooltipItem.datasetIndex].data[0] + ' %';
			                }
			            }
			        }
				}
				

			};
			this.index_id = {};
			this.chart = new Chart(this.$el.find('#Chart'), this.config);
			$.get({
				url: 'apiworkers/report_contact_by_group/?project='+this.model.id,
				success: function(response){
					var arr = [];
					var percent_all = parseInt((((response.vip+response.a+response.b+response.c)/response.all)*100).toFixed());
					var vip = parseInt(((response.vip/response.all_vip)*100).toFixed());
					var a = parseInt(((response.a/response.all_a)*100).toFixed());
					var b = parseInt(((response.b/response.all_b)*100).toFixed());
					var c = parseInt(((response.c/response.all_c)*100).toFixed());
					arr = [
						{
							label: percent_all+'% - Всего клиентов',
							backgroundColor:[window.chartColors.red],
							borderColor: window.chartColors.red,
							data: [percent_all]
						},
						{
							label: vip+'% - VIP клиентов',
							backgroundColor:[window.chartColors.blue],
							borderColor: window.chartColors.red,
							data: [vip]
						},
						{
							label: a+'% - A клиентов',
							backgroundColor:[window.chartColors.green],
							borderColor: window.chartColors.red,
							data: [a]
						},
						{
							label: b+'% - B клиентов',
							backgroundColor:[window.chartColors.orange],
							borderColor: window.chartColors.red,
							data: [b]
						},
						{
							label: c+'% - C клиентов',
							backgroundColor:[window.chartColors.purple],
							borderColor: window.chartColors.red,
							data: [c]
						}
					];
					var labels = [];
					// for(var i in response){
					// 	arr.push(response[i]);
					// }
					_this.config.data.labels = labels;
					_this.config.data.datasets = arr;
					_this.chart.update();
				}
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/project_reports/table/?project='+_this.model.id,
				rowStyle: function(row, index) {
					_this.index_id[index] = row.id;
					var now_date = new Date();
					var row_date = new Date(row.month_date);
					var classes = '';
					if(now_date.getMonth() == row_date.getMonth() && now_date.getFullYear() == row_date.getFullYear()){
						classes = classes + 'success';
					}
					if(index == 0){
						return{
							classes: classes + ' no-editble',
							css: {'font-weight': 'bold', 'cursor': 'pointer'}
						};
					}
					return {classes: classes, css: {'cursor': 'pointer'}};
				},
				onClickRow: function(row, e, index){
					var _table = this;
					if(index == 'approval_status' && _this.options.status == 'working'){
						require(['workers/js/modals/approval_view'], function(View){
							var modal = new Backbone.BootstrapModal({ 
								content: new View({report: row.id}),
								title: 'Выберите действие',
								okText: 'Сохранить',
								cancelText: 'Отмена',
								animate: true, 
							}).open();
							modal.on('ok', function(){
								var form = modal.$el.find('form')[0];
								$('.modal.fade.in').css('overflow-y', 'auto');
								$.ajax({
									type: 'PATCH',
									data: new FormData(form),
									url: 'apiprojects/project_reports/'+row.id+'/',
									contentType: false,
									processData: false,
									success: function(response){
										_this.updateGlobal = true;
										_this.Table.bootstrapTable('refresh');
									}
								});
							});
							modal.on('cancel', function(){
								$('.modal.fade.in').css('overflow-y', 'auto');
							});
						});
						return;
					}
					require(['workers/js/director_plan_manager_by_project'], function(TableManagersView){
						_this.showChildView('managers', new TableManagersView({
							report: row.id, 
						}));
						// $('.modal.fade.in').animate({ scrollTop: $(document).height() }, 1000);
					});
					require(['workers/js/report_by_week_view'], function(TableManagersView){
						var row_index = e.data('index');
						var sum = 0;
						for(var i=1; i<row_index; i++){
							sum = sum + _table.data[i].fact;
						}
						_this.showChildView('details', new TableManagersView({
							prev_row: sum,
							row: row,
							report: row.id, 
							project: _this.model.id
						}));
						$('.modal.fade.in').animate({ scrollTop: $('#details_region').offset().top-270 }, 2000);
					});
				},
				columns: [
				{
					field: 'approval_status',
					title: '',
					formatter: function(value, row, index){
						if(!user.isSuperuser && value == ''){
							return '<span class="glyphicon glyphicon-transfer" title="Отправить на согласование"></span>';
						}
						if(!user.isSuperuser && value == 'return'){
							return '<span class="glyphicon glyphicon-question-sign" title="Возврат'+'Причина'+'"></span>';
						}
						if(!user.isSuperuser && value == 'on_approval'){
							return '<span class="glyphicon glyphicon-question-sign" title="На рассмотрении"></span>';
						}
						if(!user.isSuperuser && value == 'approved_by'){
							return '<span class="glyphicon glyphicon-transfer" title="Утверждено"></span>';
						}
						if(value == 'on_approval'){
							return ' <span class="glyphicon glyphicon-exclamation-sign"></span>';
						}
						if(value == 'return'){
							return '<span class="glyphicon glyphicon-question-sign"></span>';
						}
						if(value == 'approved_by'){
							return '<span class="glyphicon glyphicon-ok-sign"></span>';
						}
						return '';
					}
				},
				{
					field: 'month_name',
					title: '',
					// sortable: true,
				},
				{
					field: 'fin_plan',
					title: 'Финансовый план на весь период',
					formatter: function(value, row, index){

						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений',
					// formatter: function(value, row, index){
					// 	debugger;
					// },
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: ''
						};
					}
					// sortable: true,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'diff',
					title: 'Расхождение факта, с фин. планом',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info'
						};
					}
				},
				{
					field: 'app_plan',
					title: 'План поступления заявок на весь период',
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок',
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'app_diff',
					title: 'Расхождение факта с планом'
				},
				{
					field: 'perc_comp',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info'
						};
					}
				},
				{
					field: 'plan_processing_db',
					title: 'Количество клиентов назначенных на менеджеров',
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info'
						};
					}
				},
				],
				responseHandler: function(response){
					_this.$el.find('#summ_app_plan').append(((response[0].fact_app*100)/response[0].fact_processing_db).toFixed(2)+' %');
					_this.$el.find('#processin_db').append(response[0].perc_comp_db);
					var count_fin = 0;
					var count_app = 0;
					var d, row_d;
					for(var i = 1; i < response.length; i++){
						d = new Date();
						d.setHours(3,0,0,0);
						d.setDate(1);
						row_d = new Date(response[i].month_date);
						if(row_d<=d){
							count_fin += response[i].fin_plan_month;
							count_app += response[i].app_plan_month;
						}
					}
					response[0].fin_plan_month = count_fin;
					response[0].app_plan_month = count_app;
					return response;
				},
			});
		}
	});
	return ProjectReportsView;
})
