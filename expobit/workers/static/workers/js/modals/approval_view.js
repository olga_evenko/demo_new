define(['text!workers/html/modal_view.html', 'utils', 
		'selectize'
		], function(html, utils){
	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#approval_view')[0].outerHTML,
		events: {
			'click #showHistory': 'showHistory'
		},
		initialize: function(){
		},
		showHistory: function(e){
			if(this.show){
				this.show = false;
				this.$el.find('#history').collapse('hide');
				return;
			}
			this.show = true;
			this.$el.find('#history').collapse('show');
		},
		onRender: function(){
			var _view = this;
			$.get({
				url: 'apiprojects/approval/?report='+this.options.report,
				success: function(response){
					_view.$el.find('#history').append(Handlebars.compile($(html).filter('#template_approval_history')[0].innerHTML)(response));
				},
			});
			this.SelectApproval = this.$el.find('#selectStatus').selectize({
				options: [{text: 'Утвердить', value: 'approved_by'}, {text: 'Возврат на доработку', value: 'return'}],
				onChange: function(value){
					if(value == 'return'){
						_view.$el.find('.collapse').collapse('show');
						return;
					}
					_view.$el.find('.collapse').collapse('hide');
				}
			});
		}
	});
	return View;
});