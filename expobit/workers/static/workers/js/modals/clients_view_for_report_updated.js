define(['text!workers/html/modal_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#client_view')[0].outerHTML,
		events: {
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				// checkbox: true,
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: _view.options.url,
				onClickRow: function(row, e, index){
					_view.$el.parent().parent().parent().parent().modal('hide');
					router.navigate('client/'+row.id, {trigger: true});
				},
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				columns: [
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'address',
					title: 'Адрес',
					sortable: true,
				},
				{
					field: 'emails',
					title: 'Эл. адреса',
				}
				],
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				}
			});
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselect/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							};
						}
					});
				},
				onChange: function(value){
				},
			});
		}
	});
	return ClientsView
})
