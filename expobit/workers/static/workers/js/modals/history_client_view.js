define(['text!workers/html/modal_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#history_client')[0].outerHTML,
		events: {
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/contacts/?client=' + this.model.id,
				columns: [
				// {
				// 	checkbox: true
				// },
				{
					field: 'date',
					title: 'Дата',
					sortable: true,
				},
				{
					field: 'type_contact',
					title: 'Вид связи',
					sortable: true,
					width: '20px'
				},
				{
					field: 'contact',
					title: 'Контакт',
					sortable: true,
				},
				{
					field: 'contact_person',
					title: 'Контактное лицо',
					sortable: true,
				},
				{
					field: 'project',
					title: 'Проект',
					sortable: true,
				},
				{
					field: 'manager',
					title: 'Менеджер',
					sortable: true,
				},
				{
					field: 'comment',
					title: 'Комментарии',
					sortable: true,
				},
				{
					field: 'failure',
					title: 'Причина отказа',
				},
				{
					field: 'result',
					title: 'Результат'
				}
				],
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		}
	});
	return View;
});
