define(['text!workers/html/modal_view.html', 'utils', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale'], function(html, utils){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#check_project_list_view')[0].outerHTML,
		events: {
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			var oneDay = 24*60*60*1000;
			var min_date = utils.getDateString();
			var max_date = new Date();
			max_date.setMonth(max_date.getMonth()+3);
			max_date = utils.getDateString(max_date);
			var dir = this.options.director?'&project_type__director='+user.id:'';
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				// search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				// checkbox: true,
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiprojects/projects/?data=min&status=planning&date_start__gt='+min_date+'&date_start__lt='+max_date+dir+'&user='+user.id,
				onClickRow: function(row, e, index){
					router.navigate('project/'+row.id+'/edit', {trigger: true});
				},
				rowStyle: function(row, index) {
					var firstDate = new Date(row.date_start);
					var secondDate = new Date();
					var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
					var color;
					if(diffDays <= 14){
						color = 'danger';
					}
					if(diffDays > 14 && diffDays <= 30){
						color = 'warning';
					}
					if(diffDays > 30){
						color = 'success';
					}
					return {
						classes: color,
						css: {"cursor": "pointer"}
					};
				},
				columns: [
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
				},
				{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
				},
				],
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				}
			});
		}
	});
	return View
})
