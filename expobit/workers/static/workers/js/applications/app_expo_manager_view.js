define(['text!workers/html/applications/applications_view.html', 'utils', 'js/filters_view',
 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils, FilterView){

	var AppView = Backbone.Marionette.View.extend({
		template:$(html).filter('#expo_app_manager_view')[0].outerHTML,
		events: {
			'click #addIndividual': 'addIndividual',
			'click .pencil': 'editIndividual',
			'click .eye': 'openClient',
			'click #filterButton': 'setFilter',
			'click #resetfilterButton': 'resetFilter',
			'click .filter_button_app': 'setFilter'
		},
		regions: {
			filter: '#filterModal'
		},
		className: 'displayNone',
		editIndividual: function(e){
			var _this = this;
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('app/expo/'+row.id+'/edit',  {trigger: true});
		},
		initialize: function(){
		},
		setFilter: function(e){
			var id = e.currentTarget.id;
			if(id == 'all'){
				this.Table.bootstrapTable('refresh', {url: '/apiclients/application_expo/?project__user='+this.project_user});
			}
			if(id == 'my'){
				this.Table.bootstrapTable('refresh', {url: '/apiclients/application_expo/?manager='+user.manager_id});
			}
		},
		onRender: function(){
			var col = [
			{
				field: 'index',
				title: '№',
				formatter: function(value, row, index, field){
					return index + 1;
				}
			},
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'production',
				title: 'Экспонаты',
				// sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			// {
			// 	field: 'edit',
			// 	title: '',
			// 	formatter: function(value, row, index){
			// 		return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-id ='+
			// 		row.id+'></span></div>';
			// 	}
			// }
			];
			if(this.options.app_url=='lease_kvs'){
				col.splice(1,0, {
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
					formatter: function(value, row, index){
						return utils.dateFormatter(value);
					}
				},{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
					formatter: function(value, row, index){
						return utils.dateFormatter(value);
					}
				});
			}
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/?all=true',
						success: function(response){
							for(var i in response){
								_this.addOption(response[i]);
							}
						}
					});
				},
				onChange: function(value){
					_view.query = {
						project: value
					}
					_view.Table.bootstrapTable('refresh');
				}
			});
			this.project_user = user.header;
			var url = '/apiclients/application_expo/?manager='+user.manager_id+'&project__user='+user.header;
			if(user.groups.indexOf('project_managers') != -1){
				this.project_user = user.id;
				url = '/apiclients/application_expo/?manager='+user.manager_id+'&project__user='+user.id;
			}
			if(user.groups.indexOf('head_expo') != -1){
				this.hide_toolbar = true;
				url = '/apiclients/application_expo/?project__project_type__director='+user.id;
			}
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				showColumns: true,
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				toolbar: this.hide_toolbar?'':$(html).filter('#toolbar_filter')[0].innerHTML,
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: url,
				// newQuery: {},
				// onRefresh: function(options){
				// 	if(options.query){
				// 		this.newQuery = options.query;	
				// 	}
				// 	else{
				// 		this.newQuery = {};
				// 	}
				// },
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('app/expo/'+row.id+'/edit',  {trigger: true});
					}
				},
				onPageChange: function(num, size){
					router.navigate('manager_expo_app/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				columns: col,
				responseHandler: function(response){
					return response;
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				queryParams: function(params){
					return Object.assign(params, _view.query);
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
						css:{
							cursor: 'pointer'
						}
					};
				}
			});
		}
	});
	return AppView;
});
