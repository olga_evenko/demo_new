define(['text!workers/html/main_view.html'], function(html){
	var MessageView = Backbone.Marionette.View.extend({
		template:$(html).filter('#user_notifications_view')[0].outerHTML,
		events: {
		},
		className: 'panel panel-default form massage_view',
		initialize: function(){
			var _view = this;
			$.get({
				url: '/apiworkers/notifications/?limit=10&offset=0',
				success: function(response){
					_view.count = response.count;
					_view.next = response.next;
					var html_messages = Handlebars.compile($(html).filter('#collection_messages')[0].innerHTML)(response.results.reverse());
					var message_block = _view.$el.find('#message_dialog div').toArray();
					var old_height = 0;
					for(var i in message_block){
						old_height = old_height + $(message_block[i]).height();
					}
					_view.$el.find('#message_dialog').prepend(html_messages);
					var new_height = 0;
					message_block = _view.$el.find('#message_dialog div').toArray();
					for(var i in message_block){
						new_height = new_height + $(message_block[i]).height();
					}
					_view.$el.find('#message_dialog').scrollTop(new_height-old_height);
				},
				error: function(response){

				}
			});
			this.readMessage();
			},
			readMessage: function(){
				// if(!this.model.get('show')){	
					var data = new FormData();
					data.append('sender_id', this.options.sender_id);
					$.ajax({
						url: '/apiworkers/notifications/read_noty/',
						type: 'POST',
						data: data,
						contentType: false,
						processData: false,
						success: function(){
							mainView.update_count_notifications();
						}
					});
				// }
			},
			getNextPage: function(){
				var _view = this;
				if(this.next){
					$.get({
						url: this.next,
						success: function(response){
							_view.count = response.count;
							_view.next = response.next;
							var html_messages = Handlebars.compile($(html).filter('#collection_messages')[0].innerHTML)(response.results.reverse());
							var message_block = _view.$el.find('#message_dialog div').toArray();
							var old_height = 0;
							for(var i in message_block){
								old_height = old_height + $(message_block[i]).height();
							}
							_view.$el.find('#message_dialog').prepend(html_messages);
							message_block = _view.$el.find('#message_dialog div').toArray();
							var new_height = 0;
							for(var i in message_block){
								new_height = new_height + $(message_block[i]).height();
							}
							_view.$el.find('#message_dialog').scrollTop(new_height-old_height+200);
						},
						error: function(response){

						}
					});
				}
			},
			update_chat: function(str){
				this.$el.find('#message_dialog').append(Handlebars.compile($(html).filter('#collection_messages')[0].innerHTML)([JSON.parse(str),]));
				var obj = this.$el.find('#message_dialog');
					var h = obj.get(0).scrollHeight;
					obj.animate({scrollTop: h},
						500, function() {
							/* stuff to do after animation is complete */
						});
				this.readMessage();
			},
			// Добавить у отправляющего сообщение и чистить инпут
			onRender: function(){
				var _this = this;
				$('.loading').fadeOut();
				this.$el.find(document).ready(function($) {
					_this.$el.fadeIn();
					var obj = _this.$el.find('#message_dialog');
					var h = obj.get(0).scrollHeight + 50;
					obj.animate({scrollTop: h},
						500, function() {
							/* stuff to do after animation is complete */
						});
				});
				this.$el.find('#message_dialog').scroll(function(event) {
					if(_this.$el.find('#message_dialog').scrollTop() == 0){
						_this.getNextPage();
					}
				});
			},
		});
	return MessageView;
});
