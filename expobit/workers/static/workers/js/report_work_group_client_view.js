define(['text!workers/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize'], function(html, utils){

	var ClientsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#main_contact_group_report_view')[0].outerHTML,
		events: {
			'click #getReport': 'getReport',
		},
		className: 'displayNone',
		initialize: function(){
		},
		getReport: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiworkers/report_contact/?report=true&'+this.$el.find('form').serialize()});
		},
		openModal: function(url, title, filter){
			var _this=this;
			// Пробное место, потом почистить и перенести работу с кэшем в app.js
			// window.viewscache = {modals: {}};
			require(['workers/js/modals/clients_view_for_report_updated'], function(ClientsView){
				var modal = new Backbone.BootstrapModal({
					content: new ClientsView({url: url, filter: filter}), 
					title: title,
					okText: 'Отправить',
					cancelText: 'Отмена',
					animate: true,
					okCloses: false
				}).open();
				utils.addWindow(modal, 'modals', title);
				// window.viewscache.modals[modal.cid] = modal;
				modal.on('ok', function(){
					if(this.options.content.SelectManager[0].selectize.getValue()==''){
						utils.notyAlert('Выберите получателя').show();
					}
					else{
						var serialize = this.$content.find('form').serializeArray();
						var data = utils.arraytoJson(serialize);
						data.filter = this.options.content.options.filter;
						$.ajax({
							url: '/apiworkers/messages/manager_report/',
							type: 'POST',
							data: JSON.stringify(data),
							contentType: 'application/json',
							// processData: false,
							success: function(response){
								utils.notySuccess('Сообщение отправлено').show();
								modal.close();
								utils.removeWindow(modal, 'modals');
							},
							error: function(response){
								utils.notyError('Ошибка отправления').show();
							}
						});
					};
				});
				modal.on('cancel', function(e){
					utils.removeWindow(this, 'modals');
				});
			});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				totalField: 'count',
				// sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// url: '/apiworkers/clients/',
				// onPageChange: function(num, size){
				// 	router.navigate('my_clients/p'+num+'-'+size+'/project'+_view.SelectProject[0].selectize.getValue(), {replace: true});
				// },
				// pageSize: _view.options.page_size,
				// pageNumber: _view.options.page_num,
				// onClickRow: function(row, e, index){
				// 	if(index!='edit'){
				// 		router.navigate('client/'+row.id, {trigger: true});
				// 	}
				// },
				// rowStyle: function(row, index) {
				// 	if(index == 'not_groups.count'){
				// 		return {
				// 			classes: '',
				// 			css: {"cursor": "pointer"}
				// 		};
				// 	}
				// 	return {classes: ''};
				// },
				columns: [
				{
					field: 'manager',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'all_clients.all',
					title: 'Всего клиентов',
					cellStyle: function(value, row, index, field){
						if(row.manager != 'Итого %'){
							return {
								css: {'background-color': 'rgba(52, 152, 219, 0.48)'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
					// sortable: true
				},
				{
					field: 'all_clients.first_in',
					title: 'Всего клиен. перв. исх. зв.',
				},
				{
					field: 'groups.vip',
					title: 'Клиентов VIP',
					cellStyle: function(value, row, index, field){
						if(row.manager != 'Итого %'){
							return {
								css: {'background-color': 'rgba(52, 152, 219, 0.48)'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'groups.vip_in',
					title: 'VIP перв. исх. зв.',
				},
				{
					field: 'groups.a',
					title: 'Клиентов A',
					cellStyle: function(value, row, index, field){
						if(row.manager != 'Итого %'){
							return {
								css: {'background-color': 'rgba(52, 152, 219, 0.48)'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'groups.a_in',
					title: 'A перв. исх. зв.',
				},{
					field: 'groups.b',
					title: 'Клиентов B',
					cellStyle: function(value, row, index, field){
						if(row.manager != 'Итого %'){
							return {
								css: {'background-color': 'rgba(52, 152, 219, 0.48)'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'groups.b_in',
					title: 'B перв. исх. зв.',
				},{
					field: 'groups.c',
					title: 'Клиентов C',
					cellStyle: function(value, row, index, field){
						if(row.manager != 'Итого %'){
							return {
								css: {'background-color': 'rgba(52, 152, 219, 0.48)'},
								classes: '',
							};	
						}
						return {
							classes: ''
						};
					}
				},
				{
					field: 'groups.c_in',
					title: 'C перв. исх. зв.',
				},
				{
					field: 'not_groups.count',
					title: 'Без группы',
					cellStyle: function(){
						return {css: {cursor: 'pointer'}};
					}
				}
				],
				rowStyle: function(row, index) {
					if(row.manager == 'Итого %'){
						return {
							classes: 'info'
						};	
					};
					return {classes: ''}
					
				},
				responseHandler: function(response){
					
					return response
				},
				onClickCell: function(field, value, row, $element){
					if(field == 'not_groups.count'){
						_view.openModal('apiclients/clients/?id__in='+row.not_groups.client_list.join(','), 'Клиенты без группы', {id__in: row.not_groups.client_list});
					}
				}
			});
			this.SelectProject = this.$el.find('#SelectProject').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/projects/',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							}
							if(_view.options.project){
								_this.setValue(_view.options.project);
							};
						}
					});
				},
				onChange: function(value){
					_view.$el.find('#getReport').attr('disabled', false);
					// _view.Table.bootstrapTable('refresh', {url: '/apiworkers/report_contact/?report=true&membership__project='+value});
					// router.navigate('report_group_manager' + '/project' + value, {replace: true});
				},
			});
		}
	});
	return ClientsView
})
