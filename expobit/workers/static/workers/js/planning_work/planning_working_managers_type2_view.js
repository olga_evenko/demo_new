define(['text!workers/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){
	
	var PlanManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#plan_manager_view')[0].outerHTML,
		events: {
			'change #setMonth': 'setMonth',
		},
		className: 'displayNone',
		initialize: function(){
		},
		templateContext: function(){
			return {curr_date: new Date().toJSON().substr(0,7)};
		},
		setMonth: function(e){
			var date = $(e.currentTarget).val();
			this.Table.bootstrapTable('refresh', {
				url: '/apiworkers/plan_managers_type_2/?manager='+user.manager_id+'&month='+date
			});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiworkers/plan_managers_type_2/?manager='+user.manager_id,
				columns: [
				{
					field: 'project',
					title: 'Проекты в работе',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'ticket_plan_month',
					title: 'План покупки билетов',
					// editable: edit_settings,
				},
				{
					field: '',
					title: 'Факт. покупки билетов'
				},
				{
					field: '',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_proccessing_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				]
			});
		},
	});
return PlanManagerView
})
