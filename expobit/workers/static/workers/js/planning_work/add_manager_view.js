define(['text!workers/html/planning_work/planning_work_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal', 'selectize', 'spinner'], function(html){

	var AddManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#add_manager_view')[0].outerHTML,
		events: {
		},
		regions: {
		},
		// className: 'panel panel-default panelTabs displayNone form',
		templateContext: function(){
			return {row: this.options.row}
		},
		initialize: function(){
		},
		addManager: function(){
		},
		onRender: function(){
			var _view = this; 
			this.$el.find('#spinner1').spinner({ min: 1, max: this.options.row.month_plan, step: 1, precision:0 });
			this.SelectManager = this.$el.find('#SelectManager').selectize({
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiworkers/managers/forselect/',
						// async: true,
						// labelField: 'text',
						// valueField: 'value',
						success: function(response){
							for(i in response){
								_this.addOption(response[i]);
							};
							if(_view.model){
								_this.setValue(_view.model.get('manager').id);
							}
						}
					});
				}
			});
		}
	});
	return AddManagerView
})
