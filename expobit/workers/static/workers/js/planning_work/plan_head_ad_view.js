define(['text!workers/html/main_view.html', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize',
	'table-context-menu'], function(html){
	
	var PlanProjectManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#plan_manager_view')[0].outerHTML,
		events: {
			'change #setMonth': 'setMonth',
			'click #report': 'getReport'
		},
		className: 'displayNone',
		templateContext: function(){
			var show_select = false;
			if(user.direction.length > 0){
				show_select = true;
			}
			return {buttonCreate: false, showSelect: false, curr_date: new Date().toJSON().substr(0,7)};
		},
		setMonth: function(e){
			var date = $(e.currentTarget).val();
			// if(this.SelectProjectManager && this.SelectProjectManager[0].selectize.getValue() != ''){
			// 	this.Table.bootstrapTable('refresh', {
			// 		url: '/apiprojects/project_reports_type2/?all=true&project_indicators__project__user='+this.SelectProjectManager[0].selectize.getValue()+
			// 		'&months='+date+'',
			// 	});
			// 	return;
			// }
			this.Table.bootstrapTable('refresh', {
				url: !this.head_direction?'/apiprojects/project_reports_type2/?all=true&project_indicators__project__user='+user.id+
				'&months='+date+'':'/apiprojects/project_reports_type2/?all=true&project_indicators__project__project_type__director='+user.id+
				'&months='+date+'',
			});
		},
		getReport: function(){
			if(user.groups.indexOf('head_expo') == -1 && user.groups.indexOf('project_managers') != -1){
				window.open('/report/projects_report?'+'months='+
						this.$el.find('input[name="month"]').val());
			}
			window.open('/report/projects_report?'+'months='+
						this.$el.find('input[name="month"]').val()+'&project_indicators__project__user='+this.SelectProjectManager[0].selectize.getValue());
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.head_direction = false;
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: !this.head_direction?'/apiprojects/project_reports_type2/?all=true&project_indicators__project__user='+user.id+
				'&month=1':'/apiprojects/project_reports_type2/?all=true&project_indicators__project__project_type__director='+user.id+
				'&month=1',
				// toolbar: $(html).filter('#toolbar-report-project-dvp')[0].innerHTML,
				columns: [
				// {
				// 	field: 'approval_status',
				// 	title: '',
				// 	formatter: function(value, row, index){
				// 		if(value == 'on_approval'){
				// 			return ' <span class="glyphicon glyphicon-exclamation-sign" title="На утверждении"></span>';
				// 		}
				// 		if(value == 'return'){
				// 			return '<span class="glyphicon glyphicon-question-sign" title="Возврат"></span>';
				// 		}
				// 		if(value == 'approved_by'){
				// 			return '<span class="glyphicon glyphicon-ok-sign" title="Утвержден"></span>';
				// 		}
				// 		return '';
				// 	}
				// },
				{
					field: 'project.name',
					title: 'Проекты в работе',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// editable: edit_settings,
				},
				{
					field: 'fact_money',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				// {
				// 	field: 'app_plan_month',
				// 	title: 'Ежемес. план поступления заявок',
				// 	// editable: edit_settings,
				// },
				{
					field: 'fact_ticket',
					title: 'Количество билетов'
				},
				// {
				// 	field: 'perc_comp',
				// 	title: '% выполнения',
				// 	cellStyle: function(value, row, index, field){
				// 		return {
				// 			classes: 'info',
				// 		}
				// 	}
				// },
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_processing_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				],
				contextMenu: '#context-menu',
				onContextMenuItem: function(row, $el){
					if($el.data("item") == "show_meeting"){
						require(['workers/js/modals/project_report_dvp_meeting_view', 'workers/js/modals/modal_templates'], function(View, tmp){
							var model = new Backbone.Model(row);
							model.set('id', model.get('project').id);
							model.set('name', model.get('project').name);
							model.set('user_name', model.get('project').user_name);
							var modal = new Backbone.BootstrapModal({ 
								content: new View({model: model, status: row.status}),
								title: 'План по проекту '+model.get('name'),
								okText: 'Закрыть',
								cancelText: '',
								animate: true, 
								template: tmp.full_screen
							}).open();
							modal.on('ok', function(){
								if(modal.options.content.updateGlobal){
									_view.Table.bootstrapTable('refresh');
								}
							});
						});
					}
				},
				onContextMenuRow: function(row, $el){
					this.right_click = true;
				},
				onClickRow: function(row, e, index){
					if(!this.right_click){
						if(index!='edit'){
							router.navigate('project/'+row.project.id+'/edit', {trigger: true});
						}
					}
					this.right_click = false;
				},
				responseHandler: function(response){
					if(response.length == 0){
						return response
					}
					// var results = response.results;
					var sum_fin_plan = 0;
					var sum_fact = 0;
					var sum_app_plan = 0;
					var sum_fact_app = 0;
					var sum_fact_processing_db = 0;
					var sum_plan_processing_db = 0;
					for(i in response){
						sum_fin_plan = sum_fin_plan + response[i].fin_plan_month;
						sum_fact = sum_fact + response[i].fact_money;
						sum_app_plan = sum_app_plan + response[i].app_plan_month;
						sum_fact_app = sum_fact_app + response[i].fact_app;
						sum_fact_processing_db = sum_fact_processing_db + response[i].fact_processing_db;
						sum_plan_processing_db = sum_plan_processing_db + response[i].plan_processing_db;
					}
					var perc, perc_comp = '0 %', perc_comp_db = 0;
					if(sum_fin_plan != 0){
						perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
					}
					if(sum_app_plan != 0){
						perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
					}
					if(sum_plan_processing_db != 0){
						perc_comp_db = ((sum_fact_processing_db/sum_plan_processing_db)*100).toFixed(2);
					}
					response.push({
						project: {name: 'Итого'},
						fin_plan_month: sum_fin_plan,
						perc: perc?perc.toString()+' %': null,
						perc_comp: perc_comp?perc_comp.toString()+' %': null,
						perc_processing_db: perc_comp_db?perc_comp_db.toString()+' %':null,
						fact_money: sum_fact,
						app_plan_month: sum_app_plan,
						fact_app: sum_fact_app,
						fact_processing_db: sum_fact_processing_db,
						plan_processing_db: sum_plan_processing_db
					});
					return response;
				},
				rowStyle: function(row, index) {
					if(row.project.name == 'Итого'){
						return {
							classes: 'info',
							css: {"cursor": "pointer"}
						};	
					}
					return {
						css: {"cursor": "pointer"}
					};
				}
			});
		},
	});
return PlanProjectManagerView
})
