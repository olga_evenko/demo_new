define(['text!workers/html/planning_work/planning_work_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal'], function(html){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#manager_plan_table_view')[0].outerHTML,
		events: {
		},
		regions: {
		},
		className: 'displayNone',
		templateContext: function(){

		},
		initialize: function(){
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			this.Table = this.$el.find('#table_managers').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiworkers/plan_managers_type_1/',
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'month_plan',
					title: 'Ежемесячный план поступлений',
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана'
				},
				{
					field: 'plan_app_count',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app_count',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'perc_app_count',
					title: '% выполнения'
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_proc_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_proc_db',
					title: '% выполнения'
				},
				],
			});

		}
	});
	return ProjectReportsView
})
