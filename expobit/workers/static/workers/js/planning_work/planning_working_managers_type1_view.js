define(['text!workers/html/planning_work/planning_work_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale', 'bootstrap-modal'], function(html){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#managers_table_view')[0].outerHTML,
		events: {
			'click #addManager': 'addManager',
			'click #editManager': 'editManager'
		},
		regions: {
		},
		className: 'displayNone',
		templateContext: function(){
			return {
				month: this.options.month
			};
		},
		initialize: function(){
		},
		getSumm: function(model){
			var arr = this.Table.bootstrapTable('getData');
			var s = arr[0];
			for(var i = 1; i < arr.length; i++){
				s.month_plan-=arr[i].month_plan;
			};
			if(model){
				s.month_plan += model.get('month_plan');
			};
			return s
		},
		editManager: function(e){
			var id = $(e.currentTarget).data().id;
			var row = this.Table.bootstrapTable('getRowByUniqueId', id);
			this.addManager(e, new Backbone.Model(row));
		},
		addManager: function(e, model){
			var _view = this;
			var title = 'Добавить';
			var okText = 'Добавить';
			if(model){
				title = 'Редактировать';
				var okText = 'Сохранить';
			};
			var row = this.getSumm(model);
			require(['workers/js/planning_work/add_manager_view'], function(AddManagerView){
				var modal = new Backbone.BootstrapModal({ 
					content: new AddManagerView({model: model, row: row}),
					title: title,
					okText: okText,
					cancelText: 'Отмена',
					okCloses: false,
					escape: false,
					modalOptions: {
						backdrop: 'static',
						keyboard: false
					},
					animate: true, 
				}).open();
				modal.on('ok', function(){
					var data = new FormData(this.$content.find('form')[0]);
					data.append('fin_plan', _view.options.report)
					var type = 'POST';
					var url = '/apiworkers/plan_managers_type_1/';
					if(modal.options.content.model){
						type = 'PATCH';
						url = url + modal.options.content.model.id + '/';
					};
					$.ajax({
						type: type,
						url: url,
						processData: false,
						contentType: false,
						data: data,
						success: function(response){
							_view.Table.bootstrapTable('refresh', {silent: true});
							modal.close();
						},
						error: function(response){}
					});
				});
			});
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			this.Table = this.$el.find('#table_managers').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiworkers/plan_managers_type_1/?fin_plan='+_this.options.report,
				rowStyle: function(row, index) {
					if(index == 0){
						_this.first_row = row;
						return{
							classes: 'no-editble',
						};
					};
					return {classes:''}
				},
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						if(index != 0){
							return '<span style="margin-right: 0px; margin-left: 0px;" id="editManager" class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span>';
						}
						else{
							return ''
						};
					}
				},
				{
					field: 'manager.manager',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'month_plan',
					title: 'Ежемесячный план поступлений',
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана'
				},
				{
					field: 'plan_app_count',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app_count',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'perc_app_count',
					title: '% выполнения'
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_proc_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_proc_db',
					title: '% выполнения'
				},
				],
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				}
			});

		}
	});
	return ProjectReportsView
})
