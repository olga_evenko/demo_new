define(['text!workers/html/planning_work/planning_work_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#planning_work_type1')[0].outerHTML,
		events: {
			'click .managers': 'opentableManagers',
			'change #buttonTypeProject': 'buttonTypeProject',
		},
		regions: {
			managers: '#managers_region'
		},
		className: 'displayNone',
		initialize: function(){
		    $.get({
                url: '/apiprojects/projects_types/?director='+user.id,
                success: function(response){
                    if(response.length>1){
                        $('#buttonTypeProject').removeClass('hidden');
                        for( var i in response){
                            $('#buttonTypeProject').append(
                            `<label class="btn btn-primary">
                               <input type="radio" name="options" id="buttonTypeProject${response[i].id}"> ${response[i].type}
                             </label>`)
                        }
                    }
                }
            });
		},
		validateTable: function(row){
			var valid = true;
			if(!row.month_plan){
				utils.notyAlert('Заполните ежемесячный план поступлений').show();
				valid = false;
			};
			return valid
		},
		opentableManagers: function(e){
			var _this = this;
			var id = $(e.currentTarget).data().id;
			var row = _this.Table.bootstrapTable('getRowByUniqueId', id);
			if(this.validateTable(row)){
				require(['workers/js/planning_work/planning_working_managers_type1_view'], function(TableManagersView){
					_this.showChildView('managers', new TableManagersView({
						report: id, 
						month: row.month_name,
						max_value1: row.month_plan,
					}));
				});
			};
		},
		buttonTypeProject: function(e){
		    this.select_project_type = $(e.target).button('toggle').attr('id')
		    this.Table.bootstrapTable('refresh', {silent: true});

		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var edit_settings = {
				type: 'text',
				emptytext: 'Установить',
				url: '/apiworkers/plan_type_1/',
				ajaxOptions: {
					type: 'PATCH',
					dataType: 'json',
					beforeSend: function(jqXHR,settings){
						var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
						settings.data = settings.data.replace(/\+/gi,'');
						settings.url += row.id+'/';
					}
				},
				params: function(params){
					params[params.name] = params.value
					delete params['name'];
					delete params['value'];
					return params;
				},
				// validate: function(value){
				// 	var field_name = $(this).data().name;
				// 	if(field_name!='plan_processing_db'){
				// 		var all_data = _this.Table.bootstrapTable('getData');
				// 		var count = 0;
				// 		for(var i=1; i<all_data.length; i++){
				// 			if(all_data[i][field_name]){
				// 				count += parseInt(all_data[i][field_name], 10); 
				// 			};
				// 		};
				// 		if(parseInt(all_data[0][field_name], 10)-count<parseInt(value, 10)){
				// 			return 'Введите число меньше '+ (parseInt(all_data[0][field_name], 10)-count).toString();
				// 		};
				// 	}
				// },
				success: function(response){
					_this.Table.bootstrapTable('refresh', {silent: true});
				}
			};
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiworkers/plan_type_1',
				rowStyle: function(row, index) {
					if(index == 0){
						return{
							classes: 'no-editble',
							css: {'font-weight': 'bold'}
						};
					};
					return {classes:''}
				},
				columns: [
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						if(index != 0){
							return '<span style="margin-right: 0px; margin-left: 0px;" class="managers glyphicon glyphicon-briefcase pencil" data-id ='+
						row.id+'></span>';
						}
						else{
							return ''
						};
					}
				},
				{
					field: 'month_name',
					title: '',
				},
				{
					field: 'fin_plan',
					title: 'Финансовый план поступления денежных средств',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'month_plan',
					title: 'Ежемесячный план поступлений',
					editable: edit_settings,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: ''
						};
					}
				},
				{
					field: 'fact_money',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'percent',
					title: '% выполнения ежемесячного плана',
				},
				{
					field: 'diff',
					title: 'Расхождение факта, с фин. планом, наростающим итогом',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					}
				}
				],
				onLoadSuccess: function (response) {
				    _this
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
					debugger




					if(_this.select_project_type){
					    _this.select_project_type.replace('buttonTypeProject','')
					    var type = _this.select_project_type.replace('buttonTypeProject','')
					    if(type !== 'All'){
					        var temp = []
					        for(var i in this.data){
					            if(this.data[i].project_type == type){
					                temp.push(this.data[i])
					            }
					        }
					    }else{
					        var temp = this.data
					    }
					    $('#table').bootstrapTable('load', temp);
					}

				},

			});
			this.$el.on('keyup', '.form-control.input-sm', function(){
			    this.value = this.value.replace(/ /g,'');
			    var number = this.value;
			    this.value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			});
		}
	});
	return ProjectReportsView;
});
