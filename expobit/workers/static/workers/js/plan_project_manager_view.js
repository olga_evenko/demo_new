define(['text!workers/html/main_view.html', 
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'selectize',
	'table-context-menu'], function(html){
	
	var PlanProjectManagerView = Backbone.Marionette.View.extend({
		template:$(html).filter('#plan_manager_view')[0].outerHTML,
		events: {
			'change #setMonth': 'setMonth',
			'click #report': 'getReport'
		},
		className: 'displayNone',
		templateContext: function(){
			var show_select = false;
			if(user.direction.length > 0){
				show_select = true;
			}
			if(user.groups.indexOf('project_managers')!=-1){
				return {buttonCreate: true, showSelect: show_select, curr_date: new Date().toJSON().substr(0,7)};
			};
		},
		setMonth: function(e){
			var date = $(e.currentTarget).val();
			var _view = this

//			пересчет индивидуального плана
			$.get({
                url: `apiworkers/plans_manager/?all=true&manager=${user.manager_id}&month=${date}&order=asc`,
                success: function(response){
                    _view.individual_plan = response
                }
            });

			if(this.SelectProjectManager && this.SelectProjectManager[0].selectize.getValue() != ''){
                 this.Table.bootstrapTable('refresh', {
                    url: '/apiprojects/project_reports/?all=true&project_indicators__project__user='+this.SelectProjectManager[0].selectize.getValue()+
                    '&months='+date+'',
                 });
                return;
			}
			this.Table.bootstrapTable('refresh', {
				url: !this.head_direction?'/apiprojects/project_reports/?all=true&project_indicators__project__user='+user.id+
				'&months='+date+'':'/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+user.id+
				'&months='+date+'',
			});
		},
		getReport: function(){
			if(user.groups.indexOf('head_expo') == -1 && user.groups.indexOf('project_managers') != -1){
				window.open('/report/projects_report?'+'months='+
						this.$el.find('input[name="month"]').val());
			}
			window.open('/report/projects_report?'+'months='+
						this.$el.find('input[name="month"]').val()+'&project_indicators__project__user='+this.SelectProjectManager[0].selectize.getValue());
		},
		getColumns: function(){
		    debugger
            cols =  [
				{
					field: 'approval_status',
					title: '',
					formatter: function(value, row, index){
						if(value == 'on_approval'){
							return ' <span class="glyphicon glyphicon-exclamation-sign" title="На утверждении"></span>';
						}
						if(value == 'return'){
							return '<span class="glyphicon glyphicon-question-sign" title="Возврат"></span>';
						}
						if(value == 'approved_by'){
							return '<span class="glyphicon glyphicon-ok-sign" title="Утвержден"></span>';
						}
						return '';
					}
				},
				{
					field: 'project.name',
					title: 'Проекты в работе',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений ДС',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// editable: edit_settings,
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
                    },
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'perc_comp',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
						}
					}
				},
				]

			if(user.groups.indexOf('project_managers')>=0 && user.groups.indexOf('head_expo')<0){
			    cols.splice(5, 0, {
					field: 'bonus_100',
					title: 'Бонус план',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'warning',
						}
					},
                    formatter: function(value, row, index, field){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }
                        return value;
					}
				});
				cols.splice(6, 0,{
					field: 'bonus',
					title: 'Бонус на сегодня',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'warning',
						}
					},
                    formatter: function(value, row, index, field){
                        if(value){
                            value = value.toString().replace(/ /g,'');
                            var number = value;
                            value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                        }

                        if(row.bonus_text){
                            value += ` <span title="${row.bonus_text}" class="glyphicon glyphicon-question-sign"></span>`
                        }

                        return value
					}
				})
			}
			return cols
		},
		initialize: function(){
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.head_direction = false;
			if(user.direction.length > 0){
				this.head_direction = true;
				this.SelectProjectManager = this.$el.find('#SelectProjectsManager').selectize({
					valueField: 'user_id',
					onInitialize: function(){
						var _this = this;
						$.get({
							url: '/apiworkers/managers/forselect/?get_headers_group=true',
							success: function(response){
								for(var i in response){
									_this.addOption(response[i]);
								}
							}
						});
					},
					onChange: function(value){
						var date = $('input[name="month"]').val();
						if(value != ''){
							_view.Table.bootstrapTable('refresh', {
								url: '/apiprojects/project_reports/?all=true&project_indicators__project__user='+value+'&months='+date
							});
						}
						else{
							_view.Table.bootstrapTable('refresh', {
								url: '/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+user.id+'&months='+date
							});
						}
					}
				});
			}

			$.get({
                url: '/apiprojects/subjects/',
                success: function(response){
                    _view.subjects = response
                }
            });
            $.get({
                url: '/apiworkers/reduction_factor/',
                success: function(response){
                    _view.reduction_factor = response
                }
            });

            $.get({
                url: `apiworkers/plans_manager/?all=true&manager=${user.manager_id}&order=asc`,
                success: function(response){
                    _view.individual_plan = response
                }
            });

			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: !this.head_direction?'/apiprojects/project_reports/?all=true&project_indicators__project__user='+user.id+
				'&month=1':'/apiprojects/project_reports/?all=true&project_indicators__project__project_type__director='+user.id+
				'&month=1',
				toolbar: $(html).filter('#toolbar-report-project-dvp')[0].innerHTML,
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: this.getColumns(),
				contextMenu: '#context-menu',
				onContextMenuItem: function(row, $el){
					if($el.data("item") == "show_meeting"){
						require(['workers/js/modals/project_report_dvp_meeting_view', 'workers/js/modals/modal_templates'], function(View, tmp){
							var model = new Backbone.Model(row);
							model.set('id', model.get('project').id);
							model.set('name', model.get('project').name);
							model.set('user_name', model.get('project').user_name);
							var modal = new Backbone.BootstrapModal({ 
								content: new View({model: model, status: row.status}),
								title: 'План по проекту '+model.get('name'),
								okText: 'Закрыть',
								cancelText: '',
								animate: true, 
								template: tmp.full_screen
							}).open();
							modal.on('ok', function(){
								if(modal.options.content.updateGlobal){
									_view.Table.bootstrapTable('refresh');
								}
							});
						});
					}
				},
				onContextMenuRow: function(row, $el){
					this.right_click = true;
				},
				onClickRow: function(row, e, index){
					if(!this.right_click){
						
						if(index == 'approval_status' && row.approval_status == 'on_approval' && window.user.groups.indexOf('head_expo') != -1){
						require(['workers/js/modals/approval_view'], function(View){
							var modal = new Backbone.BootstrapModal({ 
								content: new View({report: row.id}),
								title: 'Выберите действие',
								okText: 'Сохранить',
								cancelText: 'Отмена',
								animate: true, 
							}).open();
							modal.on('ok', function(){
								var form = modal.$el.find('form')[0];
								$('.modal.fade.in').css('overflow-y', 'auto');
								$.ajax({
									type: 'PATCH',
									data: new FormData(form),
									url: 'apiprojects/project_reports/'+row.id+'/',
									contentType: false,
									processData: false,
									success: function(response){
										_this.updateGlobal = true;
										_this.Table.bootstrapTable('refresh');
									}
								});
							});
							modal.on('cancel', function(){
								$('.modal.fade.in').css('overflow-y', 'auto');
							});
						});
						return;
					}
					if(index!='edit'){
						router.navigate('project/'+row.project.id+'/edit', {trigger: true});
					}
					}
					this.right_click = false;
				},
				responseHandler: function(response){

				    _view
					// var results = response.results;
					var sum_fin_plan = 0;
					var sum_fact = 0;
					var sum_app_plan = 0;
					var sum_fact_app = 0;
					var sum_fact_processing_db = 0;
					var sum_plan_processing_db = 0;
					var bonus = 0;
					var bonus_100 = 0;
					for(i in response){
						sum_fin_plan = sum_fin_plan + response[i].fin_plan_month;
						sum_fact = sum_fact + response[i].fact;
						sum_app_plan = sum_app_plan + response[i].app_plan_month;
						sum_fact_app = sum_fact_app + response[i].fact_app;
						sum_fact_processing_db = sum_fact_processing_db + response[i].fact_processing_db;
						sum_plan_processing_db = sum_plan_processing_db + response[i].plan_processing_db;

						//рассчет премии
						if(user.groups.indexOf('project_managers')>=0 && user.groups.indexOf('head_expo')<0){
						    var k = _view.subjects[_view.subjects.map(function(o) { return o.id; }).indexOf(response[i].project_subject)]
                            var coefficients_reduction_i = {}
                            var coefficients_reduction_g = {}

                            var individual_plan = _view.individual_plan[_view.individual_plan.map(function(o) { return o.project_id; }).indexOf(response[i].project.id)]

                            var perc_g = +(response[i].fact /response[i].fin_plan_month*100).toFixed(1)
                            var perc_i = individual_plan ? +(individual_plan.fact /individual_plan.fin_plan_month*100).toFixed(1) : 0

                            response[i].bonus = 0
                            response[i].bonus_text = ''

                            if(individual_plan){
                                //премия за индивидуальный план
                                response[i].bonus_100 = +(individual_plan.fin_plan_month * k.per_im_plan_head/100).toFixed()
                                if(perc_i>100){
                                    response[i].bonus += +(individual_plan.fin_plan_month * k.per_im_plan_head/100  + (individual_plan.fact-individual_plan.fin_plan_month) * k.per_over_plan_head/100).toFixed()
                                    response[i].bonus_text += `${individual_plan.fin_plan_month} * ${k.per_im_plan_head}% +(${individual_plan.fact}-${individual_plan.fin_plan_month})*${k.per_over_plan_head}%`
                                }else{
                                    _view.reduction_factor.some(function(el) {
                                        coefficients_reduction_i = el
                                        if(perc_i>=el.min_value && perc_i<=el.max_value){return true}
                                        if(perc_i>=el.min_value && el.max_value==null){return true}
                                    });

                                    response[i].bonus += +(individual_plan.fact  * k.per_im_plan_head/100 * coefficients_reduction_i.m1).toFixed()
                                    response[i].bonus_text += `${individual_plan.fact} * ${k.per_im_plan_head}% * ${coefficients_reduction_i.m1}`
                                }
                            }else{
                                response[i].bonus_text = '0'
                                response[i].bonus_100 = 0
                            }

                            //премия за групповой план
                            response[i].bonus_100 += +(response[i].fin_plan_month * k.per_im_plan_group/100).toFixed()
                            if(perc_g>100){
                                response[i].bonus += +(response[i].fin_plan_month * k.per_im_plan_group/100  + (response[i].fact-response[i].fin_plan_month) * k.per_over_plan_group/100).toFixed()
                                response[i].bonus_text += `+${response[i].fin_plan_month} * ${k.per_im_plan_group}% +(${response[i].fact}-${response[i].fin_plan_month})*${k.per_over_plan_group}%`
                            }else{
                                _view.reduction_factor.some(function(el) {
                                    coefficients_reduction_g = el
                                    if(perc_g>=el.min_value && perc_g<=el.max_value){return true}
                                    if(perc_g>=el.min_value && el.max_value==null){return true}
                                });

                                response[i].bonus += +(response[i].fact * k.per_im_plan_group/100 * coefficients_reduction_g.m2).toFixed()
                                response[i].bonus_text += `+${response[i].fact} * ${k.per_im_plan_group}% * ${coefficients_reduction_g.m2}`
                            }

                            if(isNaN(response[i].bonus)){
                                response[i].bonus = 0
                                response[i].bonus_text = ''
                            }

                            bonus += +response[i].bonus
                            bonus_100 += +response[i].bonus_100
						}

					}
					var perc, perc_comp = '0 %', perc_comp_db = 0;
					if(sum_fin_plan != 0){
						perc = ((sum_fact/sum_fin_plan)*100).toFixed(2);
					}
					if(sum_app_plan != 0){
						perc_comp = ((sum_fact_app/sum_app_plan)*100).toFixed(2);
					}
					if(sum_plan_processing_db != 0){
						perc_comp_db = ((sum_fact_processing_db/sum_plan_processing_db)*100).toFixed(2);
					}
					response.push({
						project: {name: 'Итого'},
						fin_plan_month: sum_fin_plan,
						perc: perc ? perc.toString()+' %' : '0%',
						perc_comp: perc_comp.toString()+' %',
						perc_comp_db: perc_comp_db.toString()+' %',
						fact: sum_fact,
						app_plan_month: sum_app_plan,
						fact_app: sum_fact_app,
						fact_processing_db: sum_fact_processing_db,
						plan_processing_db: sum_plan_processing_db,
						bonus:bonus,
						bonus_100:bonus_100,
					});
					return response;
				},
				rowStyle: function(row, index) {
					if(row.project.name == 'Итого'){
						return {
							classes: 'info',
							css: {"cursor": "pointer"}
						};	
					}
					return {
						css: {"cursor": "pointer"}
					};
				}
			});
		},
	});
return PlanProjectManagerView
})
