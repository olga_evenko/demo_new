define(['text!workers/html/main_view.html','libs/backbone.paginator/js/paginator', 
	'paginator-view'], function(html, Paginator, PaginatorView){
		
		var MessageView = Backbone.Marionette.View.extend({
			template:$(html).filter('#one_message')[0].outerHTML,
			events: {
			},
			className: 'panel panel-default form',
			initialize: function(){
				if(!this.model.get('show')){	
					var data = new FormData();
					data.append('show', true);
					$.ajax({
						url: '/apiworkers/messages/' + this.model.id + '/',
						type: 'PATCH',
						data: data,
						contentType: false,
						processData: false,
						success: function(){
							var count = parseInt(mainView.$el.find('.badge')[0].innerHTML);
							mainView.$el.find('.badge')[0].innerHTML = count==1?'':count - 1;
						}
					});
				}
			},
			onRender: function(){
				var _this = this;
				$('.loading').fadeOut();
				this.$el.find(document).ready(function($) {
					_this.$el.fadeIn();
				});
			},
		});
		return MessageView;
	});
