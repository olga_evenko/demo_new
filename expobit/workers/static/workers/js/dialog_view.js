define(['text!workers/html/main_view.html'], function(html){
	var MessageView = Backbone.Marionette.View.extend({
		template:$(html).filter('#dialog_view')[0].outerHTML,
		events: {
			'click #sendMessage': 'sendMessage',
			'keyup :input': 'logKey'
		},
		className: 'panel panel-default form massage_view',
		initialize: function(){
			var _view = this;
			$.get({
				url: '/apiworkers/messages/?open_dialog=true&limit=10&offset=0&sender='+this.options.sender_id+'&user='+this.options.sender_id,
				success: function(response){
					_view.count = response.count;
					_view.next = response.next;
					for(var i in response.results){
						if(response.results[i].push == 'Поступил интерес от клиента на ваши услуги'){
							response.results[i].color = 'aquamarine';
						}
						if(response.results[i].push == 'Добавлен новый клиент в проекты' || response.results[i].push == 'Добавлен новый клиент в проект'){
							response.results[i].color = 'aliceblue';
						}
					}
					var html_messages = Handlebars.compile($(html).filter('#collection_messages')[0].innerHTML)(response.results.reverse());
					var message_block = _view.$el.find('#message_dialog div').toArray();
					var old_height = 0;
					for(var i in message_block){
						old_height = old_height + $(message_block[i]).height();
					}
					_view.$el.find('#message_dialog').prepend(html_messages);
					var new_height = 0;
					message_block = _view.$el.find('#message_dialog div').toArray();
					for(var i in message_block){
						new_height = new_height + $(message_block[i]).height();
					}
					_view.$el.find('#message_dialog').scrollTop(new_height-old_height);
				},
				error: function(response){

				}
			});
			$.get({
				url: '/apiworkers/users/'+this.options.sender_id,
				success: function(response){
					_view.$el.find('#header_dialog').html('<h4>'+response.last_name+' '+response.first_name+'</h4>')
				}
			});
			this.readMessage();
				// if(!this.model.get('show')){	
				// 	var data = new FormData();
				// 	data.append('show', true);
				// 	$.ajax({
				// 		url: '/apiworkers/messages/' + this.model.id + '/',
				// 		type: 'PATCH',
				// 		data: data,
				// 		contentType: false,
				// 		processData: false,
				// 		success: function(){
				// 			var count = parseInt(mainView.$el.find('.badge')[0].innerHTML);
				// 			mainView.$el.find('.badge')[0].innerHTML = count==1?'':count - 1;
				// 		}
				// 	});
				// }
			},
			readMessage: function(){
				// if(!this.model.get('show')){	
					var data = new FormData();
					data.append('sender_id', this.options.sender_id);
					$.ajax({
						url: '/apiworkers/messages/read_messages/',
						type: 'PATCH',
						data: data,
						contentType: false,
						processData: false,
						success: function(){
							mainView.update_count_messages();
						}
					});
				// }
			},
			logKey: function(e){
				if(e.keyCode == 13){
					this.sendMessage();
				}
			},
			getNextPage: function(){
				var _view = this;
				if(this.next){
					$.get({
						url: this.next,
						success: function(response){
							_view.count = response.count;
							_view.next = response.next;
							for(var i in response.results){
								if(response.results[i].push == 'Поступил интерес от клиента на ваши услуги'){
									response.results[i].color = 'aquamarine';
								}
								if(response.results[i].push == 'Добавлен новый клиент в проекты' || response.results[i].push == 'Добавлен новый клиент в проект'){
									response.results[i].color = 'aliceblue';
								}
							}
							var html_messages = Handlebars.compile($(html).filter('#collection_messages')[0].innerHTML)(response.results.reverse());
							var message_block = _view.$el.find('#message_dialog div').toArray();
							var old_height = 0;
							for(var i in message_block){
								old_height = old_height + $(message_block[i]).height();
							}
							_view.$el.find('#message_dialog').prepend(html_messages);
							message_block = _view.$el.find('#message_dialog div').toArray();
							var new_height = 0;
							for(var i in message_block){
								new_height = new_height + $(message_block[i]).height();
							}
							_view.$el.find('#message_dialog').scrollTop(new_height-old_height+200);
						},
						error: function(response){

						}
					});
				}
			},
			sendMessage: function(){
				if(this.$el.find('textarea').val() == ''){
					return;
				}
				var data = new FormData(this.$el.find('form')[0]);
				var _view = this;
				_view.$el.find('textarea[name="message"]').attr('disabled', true);
				data.append('user_id', this.options.sender_id);
					$.ajax({
						url: '/apiworkers/messages/dialog_message/',
						type: 'POST',
						contentType: false,
						processData: false,
						data: data,
						success: function(response){
							_view.$el.find('textarea[name="message"]').val('');
							_view.$el.find('textarea[name="message"]').attr('disabled', false);
							// _view.$el.find('#message_dialog').prepend();
							// utils.notySuccess('Сообщение отправлено').show();
						},
						error: function(response){
							_view.$el.find('textarea[name="message"]').attr('disabled', false);
							utils.notyError().show();
						}
					});
			},
			update_chat: function(str){
				this.$el.find('#message_dialog').append(Handlebars.compile($(html).filter('#collection_messages')[0].innerHTML)([JSON.parse(str),]));
				var obj = this.$el.find('#message_dialog');
					var h = obj.get(0).scrollHeight;
					obj.animate({scrollTop: h},
						500, function() {
							/* stuff to do after animation is complete */
						});
				this.readMessage();
			},
			// Добавить у отправляющего сообщение и чистить инпут
			onRender: function(){
				var _this = this;
				$('.loading').fadeOut();
				this.$el.find(document).ready(function($) {
					_this.$el.fadeIn();
					var obj = _this.$el.find('#message_dialog');
					var h = obj.get(0).scrollHeight + 50;
					obj.animate({scrollTop: h},
						500, function() {
							/* stuff to do after animation is complete */
						});
				});
				this.$el.find('#message_dialog').scroll(function(event) {
					if(_this.$el.find('#message_dialog').scrollTop() == 0){
						_this.getNextPage();
					}
				});
			},
		});
	return MessageView;
});
