define(['text!projects/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#custom_report_view')[0].outerHTML,
		events: {
			'change input[name="month"]': 'setMonth'
		},
		regions: {
			
		},
		templateContext: function(){
			return {month: new Date().toJSON().substr(0,7), title: this.options.title};
		},
		className: 'panel panel-default panelTabs displayNone form',
		initialize: function(){
		},
		setMonth: function(e){
			this.Table.bootstrapTable('refresh', {url: '/apiprojects/report_values/?month='+$(e.currentTarget).val()+'&type_report__project_type__type='+this.options.type_name});
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var edit_settings = {
				type: 'text',
				emptytext: 'Установить',
				url: '/apiprojects/report_values/',
				ajaxOptions: {
					type: 'PATCH',
					dataType: 'json',
					beforeSend: function(jqXHR,settings){
						var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
						settings.data = settings.data.replace(/\+/gi,'');
						settings.url += row.id+'/';
					}
				},
				params: function(params){
					params[params.name] = params.value
					delete params['name'];
					delete params['value'];
					return params;
				},
				validate: function(value){
					var field_name = $(this).data().name;
					var curr_value = $(this).data().value;
					if(field_name!='plan_processing_db' && field_name!='fin_plan_month' && field_name!='app_plan_month'){
						var all_data = _this.Table.bootstrapTable('getData');
						var count = 0;
						for(var i=1; i<all_data.length; i++){
							if(all_data[i][field_name]){
								count += parseInt(all_data[i][field_name], 10); 
							}
						}
						if(parseInt(all_data[0][field_name], 10)<parseInt(value.replace(/\ /g, ''), 10)+count-curr_value){
							return 'Введите число меньше '+ (parseInt(all_data[0][field_name], 10)-count+curr_value).toString();
						}
					}
				},
				success: function(response){
					_this.Table.bootstrapTable('refresh', {silent: true});
				}
			};
			var columns = [];
			for(var i in this.options.columns){
				if(this.options.columns[i].editable){
					this.options.columns[i].editable = edit_settings;
				}
			}
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/report_values/?month='+new Date().toJSON().substr(0,7)+'&type_report__project_type__type='+this.options.type_name,
				
				// toolbar: $(html).filter('#toolbar_project_report')[0].innerHTML,
				columns: this.options.columns,

				responseHandler: function(response){
				    if(this.columns[0].length >= 8 && response.length>0){
				        var self = this
                        var result = {}
                        for(var i in response){
                            for(var j in response[i]){
                                if(!result[j] && j!='id'){result[j]=''}
                                if(typeof(response[i][j]) == "number" && j!='id'){
                                    if(!result[j]){result[j]=0}
                                    result[j]+=response[i][j]
                                }
                            }
                        }
                        if(result.fact && result.plan){
                            result.perc = (result.fact/result.plan*100).toFixed(2)+'%'
                        }
                        result.name = 'Итого'
                        response.push(result)
				    }
					
					return response;
				},
				rowStyle :  function(row, index) {
				if(!row.id){
				     return {
                     classes: 'no-editble',
                     css: {"background": '#f39c12'}
                     };
				}

				return {}


                 },
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					}
				}
			});
			this.$el.on('keyup', '.form-control.input-sm', function(){
			    this.value = this.value.replace(/ /g,'');
			    var number = this.value;
			    this.value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			});
		}
	});
	return View;
})
