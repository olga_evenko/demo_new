define(['text!projects/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var ProjectInfoView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_info_view')[0].outerHTML,
		events: {
			'click #download_catalog': 'DownloadCatalog',
			'click .download_emails': 'DownloadEmail',
			'click .pencil': 'editIndividual',
			'click #download_report': 'download_report',
			'click #download_acts': 'download_acts',
			'click #DownloadFreeze': 'DownloadFreeze',
			'click #DownloadRig': 'DownloadRig',
			'click #DownloadExponat': 'DownloadExponat',
			'click #DownloadDiploma': 'DownloadDiploma',
			'click .discount_client': 'DownloadDiscount'
		},
		regions: {
		},
		className: 'panel panel-default panelTabs displayNone form',
		initialize: function(){
		},
		download_report: function(){
			var _view = this;
			window.open('/one_c/reports?project='+_view.model.id);
		},
		download_acts: function(){
			var _view = this;
			$('.loading').fadeIn();
			$.get({
				url: '/one_c/get_acts?project='+_view.model.id+'&date_end='+_view.model.get('date_end'),
				success: function(response){
					_view.timeoutGet = setInterval(function(){
						var work = response.work_id;
						$.get({
							url: '/one_c/get_acts?work='+response.work_id,
							success: function(response){
								if(response.status == 'SUCCESS'){
									clearInterval(_view.timeoutGet);
									$('.loading').fadeOut();
									window.open('/one_c/get_acts?load=true&work='+work);
								}
							}
						});
					}, 5000);
				}
			});
			// window.open('/one_c/get_acts?project='+_view.model.id+'&date_start='+_view.model.get('date_start'));
		},
		editIndividual: function(e){
			var _this = this;
			var type = $(e.currentTarget).data().type;
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('app/'+type+'/'+row.id+'/edit',  {trigger: true});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			var col = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-type="expo" data-id ='+
					row.id+'></span></div>'
				}
			}
			];
			var col_ad = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-type="ad" data-id ='+
					row.id+'></span></div>'
				}
			}
			];
			var col_kvs = [
			{
				field: 'date',
				title: 'Дата',
				sortable: true,
			},
			{
				field: 'project',
				title: 'Проект',
				sortable: true	
			},
			{
				field: 'client',
				title: 'Клиент',
				sortable: true,
			},
			{
				field: 'entity',
				title: 'Юр. лицо',
				sortable: true,
			},
			{
				field: 'total',
				title: 'Стоимость',
				sortable: true,
			},
			{
				field: 'manager',
				title: 'Менеджер',
				sortable: true,
			},
			{
				field: 'address',
				title: 'Адрес',
				sortable: true
			},
			{
				field: 'business_unit',
				title: 'Бизнес-единица',
				sortable: true,
			},
			{
				field: 'status',
				title: 'Статус заявки',
				sortable: true
			},
			{
				field: 'edit',
				title: '',
				formatter: function(value, row, index){
					return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil" data-type="lease_kvs" data-id ='+
					row.id+'></span></div>'
				}
			},
			{
				field: 'date_start',
				title: 'Дата начала',
				sortable: true,
				formatter: function(value, row, index){
					return utils.dateFormatter(value);
				}
			},{
				field: 'date_end',
				title: 'Дата окончания',
				sortable: true,
				formatter: function(value, row, index){
					return utils.dateFormatter(value);
				}
			}
			];
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				// sidePagination: 'server',
				dataField: 'results',
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_expo/?project='+this.model.id,
				columns: col,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address';
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					}
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
			this.TableKVS = this.$el.find('#table_kvs').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				// sidePagination: 'server',
				dataField: 'results',
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_lease_kvs/?project='+this.model.id,
				columns: col_kvs,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address';
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					}
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
			this.TableAd = this.$el.find('#table_ad').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				// sidePagination: 'server',
				dataField: 'results',
				sortable: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiclients/application_ad/?project='+this.model.id,
				columns: col,
				responseHandler: function(response){
					
					return response
				},
				queryParams: function(params){
					if(params.sort == 'address'){
						params.sort = 'client__address'
					}
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						// return params;
					}
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				},
				rowStyle: function(row, index) {
					var status = {
						'В работе': '',
						'Выполнен': 'success',
						'Отменен': 'danger'

					};
					return {
						classes: status[row.status],
					};
				}
			});
		},
		DownloadCatalog: function(e){
			var _view = this;
			window.open('/report/app?type='+_view.model.get('project_type').type+'&project='+_view.model.id);
		},
		DownloadFreeze: function(e){
			var _view = this;
			window.open('/report/freeze?type='+_view.model.get('project_type').type_project_type+'&project='+_view.model.id);
		},
		DownloadRig: function(e){
			var _view = this;
			window.open('/report/rig?type='+_view.model.get('project_type').type_project_type+'&project='+_view.model.id);
		},
		DownloadEmail: function(e){
			var _view = this;
			window.open('/report/clients_email?project='+_view.model.id+'&group='+$(e.currentTarget).data().group);
		},
		DownloadExponat: function(e){
			var _view = this;
			window.open('/report/exponat?type='+_view.model.get('project_type').type_project_type+'&project='+_view.model.id);
		},
		DownloadDiploma: function(e){
			var _view = this;
			window.open('/report/diploma?type='+_view.model.get('project_type').type_project_type+'&project='+_view.model.id);
		},
		DownloadDiscount: function(e){
			var _view = this;
			var id = e.currentTarget.id;
			window.open('/report/discount?mode='+id+'&type='+_view.model.get('project_type').type_project_type+'&project='+_view.model.id);
		},
		templateContext: function(){
			return {expo: this.model.get('project_type').type_project_type == 'expo'}
		}
		
	});
	return ProjectInfoView;
});
