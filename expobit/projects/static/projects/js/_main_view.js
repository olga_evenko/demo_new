define(['text!projects/html/main_view.html','utils', 'selectize'], function(html){
	var Model = Backbone.Model.extend({});
	var Collection = Backbone.PageableCollection.extend({
		model: Model,
		url: 'apiprojects/projects',
		parse: function(response){
			this.totalRecords = response.count;
			this.state.lastPage = Math.ceil(this.totalRecords/this.state.pageSize)
			return response.results
		},
		state:{
			pageSize:2
		},
		initialize: function(){
			this.pagArr=[];

		},
		// setTotalPages: function(){
		// 	this.state.totalPages = Math.ceil(this.totalRecords/this.state.pageSize)
		// },
		kak: function(){
			this.pagArr=[];
			if (this.state.currentPage==1 && 1<this.state.lastPage && this.state.lastPage<3){
				for (i=1; i<this.state.lastPage+1; i++){
					this.pagArr.push(i);
				}
			}
			if (this.state.currentPage==1 && this.state.lastPage>3){
				for (i=1; i<4; i++){
					this.pagArr.push(i);
				}
			}
			if (this.state.currentPage>1 && this.hasNextPage() && 1<this.state.lastPage && this.state.lastPage>3){
				for (i=this.state.currentPage-1; this.state.currentPage+1; i++){
					this.pagArr.push(i);
				}
			}
		}
	});
	var ItemProjectView = Backbone.Marionette.View.extend({
		template:$(html).filter('#item_project')[0].outerHTML
	});

	var FormCollectionView = Backbone.Marionette.CollectionView.extend({
		childView:ItemProjectView,
		initialize: function(){
			var _this=this;
			this.collection = new Collection();
			window.collection = this.collection;
			this.collection.on('sync', function(){
				_this.collection.kak();
			})
			this.collection.fetch({
				success: function(){
					this.collection.kak();

					_this.render();
				}
			});
		}
	});

	// var LayoutView = Backbone.
	return FormCollectionView
	
})
