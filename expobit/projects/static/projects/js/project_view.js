define(['text!projects/html/main_view.html'], function(html){
	var ProjectView = Backbone.Marionette.View.extend({
		statusView:{
			'planning':'Планирование',
			'working': 'В работе',
			'finished': 'Завершен',
			'canceled': 'Отменен'
		},
		events:{
			'click .openCollapse': 'openCollapse'
		},
		className: 'dispalyNone',
		openCollapse: function(e){
			this.$el.find('selector')
		},
		onRender: function(){
			$('.loading').fadeOut();
			var _view = this;
			this.getSummRow([1,2,3,4,5]);
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
		},
		getNum: function(str){
			return parseFloat(str.replace(/\.(?=.*\.)|[^\d\.-]/g, ''));
		},
		getSummRow(RowNum){
			var cont = 0;
			var SummRow = {};
			for (i in RowNum){
				SummRow[RowNum[i]] = {count:0};
			};
			var Table = this.$el.find('tr').toArray();
			delete Table[0];
			for (col in Table){
				if(Table[col].id != 'infoRow'){
					var col = $(Table[col]).children().toArray();
					for(row in col){
						if(RowNum.indexOf(parseInt(row))!=-1){
							SummRow[row].count += this.getNum(col[row].innerHTML);
						}
					}	
				}else{
					var LastCol = $(Table[col]).children().toArray();
					for(i in SummRow){
						// LastCol[i].id=='percent_conxept_all'
						
						if($(LastCol[i]).hasClass('percent')){
							$(LastCol[i]).replaceWith('<td id="percent_conxept_all" class="infoRow percent">' + Math.round(SummRow[i].count) + ' %</td>');
						}
						else{
							$(LastCol[i]).replaceWith('<td class="infoRow">' + SummRow[i].count + '</td>');	
						}
					}

				}
				
			}
		},
		template:$(html).filter('#ItemProjectView')[0].outerHTML,
		className: 'panel panel-default displayNone',
		templateContext: function() {
			var DS = this.model.get('date_start');
			if(DS!=null){
				DS = DS.split('-');
				DS=DS[2]+'-'+DS[1]+'-'+DS[0];
			};
			var DE = this.model.get('date_end');
			if(DE!=null){
				DE = DE.split('-');
				DE=DE[2]+'-'+DE[1]+'-'+DE[0];
			};
			var show_table = false;
			if(this.model.get('project_type') && this.model.get('project_type').type == 'Выставочный проект'){
				show_table = true;
			};
			return {
				statusView: this.statusView[this.model.get('status')],
				date_start: DS,
				date_end: DE,
				s_t:show_table
			}
		},
		
	});
	return ProjectView
	
})