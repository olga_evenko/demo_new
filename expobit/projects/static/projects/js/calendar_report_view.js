define(['text!projects/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale', 'datetimepicker'], function(html, utils){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#calendar_report_view')[0].outerHTML,
		events: {
			'dp.change #calendar_yearpicker': 'setYear',
			'dp.hide #calendar_yearpicker': 'setYearView',
			'click #calendar_report_create': 'createReport',
		},
		regions: {

		},
		templateContext: function(){
		debugger
			return {
			    year:(new Date()).getYear()+1900,
			    title: this.options.title,};

		},
		className: 'panel panel-default panelTabs displayNone form',
		initialize: function(){
		},
		setYearView: function(e){
		    var self = this
		    setTimeout(function(){
                self.$el.find('#calendar_yearpicker').data('DateTimePicker').viewMode('years');
            },1);
		},

		createReport:function(e){
            if(this.Table){
                this.Table.bootstrapTable('refresh', { url: '/apiprojects/calendar/report/?year='+$('#calendar_yearpicker').find("input").val()+
                '&extend_val='+this.$el.find('#extend_val').prop('checked')+
                '&building='+this.$el.find('#building').prop('checked')})
            }else{
                var _this = this;
                this.Table = this.$el.find('#table_calendar_report').bootstrapTable({
                    idField: 'id',
                    uniqueId: 'id',
                    classes: 'table table-hover table-bordered',
                    url: '/apiprojects/calendar/report/?year='+$('#calendar_yearpicker').find("input").val()+
                          '&extend_val='+this.$el.find('#extend_val').prop('checked')+
                          '&building='+this.$el.find('#building').prop('checked'),

                    columns: [
                        { field: 'plase', title: 'Зал',},
                        { field: '1', title: 'Январь',},
                        { field: '2', title: 'Февраль',},
                        { field: '3', title: 'Март',},
                        { field: '4', title: 'Апрель',},
                        { field: '5', title: 'Май',},
                        { field: '6', title: 'Июнь',},
                        { field: '7', title: 'Июль',},
                        { field: '8', title: 'Август',},
                        { field: '9', title: 'Сентябрь',},
                        { field: '10', title: 'Октябрь',},
                        { field: '11', title: 'Ноярь',},
                        { field: '12', title: 'Декабрь',},
                    ],

                    responseHandler: function(response){
                        return response;
                    },
                    onLoadSuccess: function () {
                        var no_edit = _this.$el.find('.no-editble a');
                        no_edit = no_edit.toArray();
                        for(i in no_edit){
                            no_edit[i].outerHTML = no_edit[i].innerHTML
                        }
                    }
                });
            }
		},
		onRender: function(){

            var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});

			this.$el.find('#calendar_yearpicker').datetimepicker({
                format: 'YYYY',
                locale: 'ru',
                viewMode: 'years',
            });


		}
	});
	return View;
})
