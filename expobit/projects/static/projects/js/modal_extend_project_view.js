define(['text!projects/html/main_view.html','libs/backbone.paginator/js/paginator', 'paginator-view', 'bootstrap-table-locale'], function(html, Paginator, PaginatorView){

	var ModalView = Backbone.Marionette.View.extend({
		template:$(html).filter('#modalExtendProjectView')[0].outerHTML,
		templateContext: function(){
			return {name:this.options.name}
		},
		onRender: function(){
			var _view = this;
			this.$el.find('input').click(function(e) {
				this.select();
			});
		},
	});
	return ModalView
})
