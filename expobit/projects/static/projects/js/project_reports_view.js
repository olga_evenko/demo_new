define(['text!projects/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_indicators_view')[0].outerHTML,
		events: {
			'click .managers': 'opentableManagers',
			'click .sendApproval': 'sendApproval',
			'click #load': 'loadTable'
		},
		regions: {
			managers: '#managers_region'
		},
		className: 'panel panel-default panelTabs displayNone form',
		initialize: function(){
		},
		loadTable: function(e){
			window.open('/report/projects_report?project='+this.model.id);
		},
		sendApproval: function(e){
			var _this = this;
			var id = $(e.currentTarget).data().id;
			var row = _this.Table.bootstrapTable('getRowByUniqueId', id);
			if(!this.validateTable(row)){
				return;
			}
			var modal = new Backbone.BootstrapModal({ 
				content: 'Вы хотите отправить на утверждение?',
				title: 'Отправить?',
				okText: 'Отправить',
				cancelText: 'Отмена',
				animate: true, 
				// template: tmp.full_screen
			}).open();
			modal.on('ok', function(){	
				var span = $(e.currentTarget);
				var tr = span.parent().parent().find('a');
				if(id){
					$.ajax({
						type: 'PATCH',
						url: '/apiprojects/project_reports/'+id+'/',
						contentType: 'application/json',
						data: JSON.stringify({approval_status: 'on_approval'}),
						success: function(response){
							span.removeClass();
							span.addClass('glyphicon glyphicon-exclamation-sign');
							tr = tr.toArray();
							for(i in tr){
								tr[i].outerHTML = tr[i].innerHTML;
							}
						}
					});
				}
			});
		},
		validateTable: function(row){
			var valid = true;
			if(row.fin_plan_month === null){
				utils.notyAlert('Заполните ежемесячный план поступлений').show();
				valid = false;
			}
			if(row.app_plan_month === null){
				utils.notyAlert('Заполните ежемесячный план поступления заявок').show();
				valid = false;
			}
			if(row.plan_processing_db === null){
				utils.notyAlert('Заполните план обработки базы клиентов').show();
				valid = false;
			}
			return valid;
		},
		opentableManagers: function(e){
			var _this = this;
			var id = $(e.currentTarget).data().id;
			var row = _this.Table.bootstrapTable('getRowByUniqueId', id);
			if(this.validateTable(row)){
				require(['projects/js/managers_report_view'], function(TableManagersView){
					_this.showChildView('managers', new TableManagersView({
						report: id, 
						month: row.month_name,
						max_value1: row.fin_plan_month,
						max_value2: row.app_plan_month,
						max_value3: row.plan_processing_db
					}));
				});
			};
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var edit_settings = {
				type: 'text',
				emptytext: 'Установить',
				url: '/apiprojects/project_reports/',
				ajaxOptions: {
					type: 'PATCH',
					dataType: 'json',
					beforeSend: function(jqXHR,settings){
						var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
						settings.data = settings.data.replace(/\+/gi,'');
						settings.url += row.id+'/';
					}
				},
				params: function(params){
					params[params.name] = params.value
					delete params['name'];
					delete params['value'];
					return params;
				},
				validate: function(value){
					var field_name = $(this).data().name;
					var curr_value = $(this).data().value;
					if(field_name!='plan_processing_db' && field_name!='fin_plan_month' && field_name!='app_plan_month'){
						var all_data = _this.Table.bootstrapTable('getData');
						var count = 0;
						for(var i=1; i<all_data.length; i++){
							if(all_data[i][field_name]){
								count += parseInt(all_data[i][field_name], 10); 
							}
						}
						if(parseInt(all_data[0][field_name], 10)<parseInt(value.replace(/\ /g, ''), 10)+count-curr_value){
							return 'Введите число меньше '+ (parseInt(all_data[0][field_name], 10)-count+curr_value).toString();
						}
					}
				},
				success: function(response){
					_this.Table.bootstrapTable('refresh', {silent: true});
				}
			};
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiprojects/project_reports/table/?show_bill=true&project='+_this.model.id,
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				rowStyle: function(row, index) {
					var now_date = new Date();
					var row_date = new Date(row.month_date);
					var classes = '';
					if(now_date.getMonth() == row_date.getMonth() && now_date.getFullYear() == row_date.getFullYear()){
						classes = classes + 'success';
					}
					if((row.approval_status != 'return' && row.approval_status != '') && !user.isSuperuser){
						classes = classes + ' no-editble';
					}
					// if(now_date>row_date && Math.round(Math.abs((now_date.getTime() - row_date.getTime())/(24*60*60*1000)))>(new Date(row_date.getFullYear(), row_date.getMonth(), 0).getDate())+8 && !user.isSuperuser){	
					// 	classes = classes + ' no-editble';
					// }
					if(index == 0){
						return{
							classes: classes + ' no-editble',
							css: {'font-weight': 'bold'}
						};
					}
					return {classes: classes};
				},
				toolbar: $(html).filter('#toolbar_project_report')[0].innerHTML,
				columns: [
				// {
				// 	checkbox: true
				// },
				{
					field: 'approval_status',
					title: '',
					onClickCell: function(field, value, row, $element){
						debugger;
					},
					formatter: function(value, row, index){
						if(!user.isSuperuser && value == '' && user.groups.indexOf('project_managers') != -1){
							return '<span data-id='+row.id+' class="glyphicon glyphicon-transfer pencil sendApproval" style="margin-right: 0px; margin-left: 0px;" title="Отправить на согласование"></span>';
						}
						if(!user.isSuperuser && value == 'return'){
							return '<span data-id='+row.id+' style="margin-right: 0px; margin-left: 0px;" class="glyphicon glyphicon-question-sign pencil sendApproval" title="Возврат\n'+'Причина: \n'+row.comment_return+'"></span>';
						}
						if(!user.isSuperuser && value == 'on_approval'){
							return '<span class="glyphicon glyphicon-exclamation-sign" title="На рассмотрении"></span>';
						}
						if(!user.isSuperuser && value == 'approved_by'){
							return '<span class="glyphicon glyphicon-ok-sign" title="Утверждено"></span>';
						}
						if(value == 'on_approval'){
							return ' <span class="glyphicon glyphicon-exclamation-sign"></span>';
						}
						if(value == 'return'){
							return '<span class="glyphicon glyphicon-question-sign"></span>';
						}
						if(value == 'approved_by'){
							return '<span class="glyphicon glyphicon-ok-sign"></span>';
						}
						return '';
					}
				},
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						if(index != 0){
							return '<span style="margin-right: 0px; margin-left: 0px;" class="managers glyphicon glyphicon-briefcase pencil" data-id ='+
						row.id+'></span>';
						}
						else{
							return ''
						};
					}
				},
				{
					field: 'month_name',
					title: '',
					// sortable: true,
				},
				{
					field: 'fin_plan',
					title: 'Финансовый план на весь период',
					editable: edit_settings,
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					cellStyle: function(value, row, index, field){
						if(user.isSuperuser){
							return {
								classes: ''
							};
						}
						return {
							classes: 'no-editble'
						};
					}
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений на тек. мес.',
					// formatter: function(value, row, index){
					// 	debugger;
					// },
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					editable: edit_settings,

					cellStyle: function(value, row, index, field){
						return {
							classes: ''
						};
					}
					// sortable: true,
				},
				{
					field: 'plan_app_money',
					title: 'Сумма к поступлению (план)',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'white-space': 'nowrap'}
						};
					}
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'diff',
					title: 'Расхождение факта, с фин. планом',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'white-space': 'nowrap'}
						};
					}
				},
				{
					field: 'app_plan',
					title: 'План поступления заявок на весь период',
					editable: edit_settings
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок на тек. мес.',
					editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок'
				},
				{
					field: 'app_diff',
					title: 'Расхождение факта с планом'
				},
				{
					field: 'perc_comp',
					title: '% вып.',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'white-space': 'nowrap'}
						};
					}
				},
				{
					field: 'fact_bill',
					title: 'Заявки в ДС',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'white-space': 'nowrap'}
						};
					},
					formatter: function(value, row, index){

						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'plan_processing_db',
					title: 'Количество клиентов назначенных на менеджеров',
					editable: edit_settings,
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '% вып.',
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'white-space': 'nowrap'}
						};
					}
				},
				],
				// rowAttributes: function(row, index, e){
					
				// },
				responseHandler: function(response){
					var count_fin = 0;
					var count_app = 0;
					var app_plan = 0;
					var plan_app_money = 0;
					var d, row_d;
					for(var i = 1; i < response.length; i++){
						d = new Date();
						d.setHours(3,0,0,0);
						d.setDate(1);
						row_d = new Date(response[i].month_date);
						app_plan += response[i].app_plan;
						if(row_d<=d){
							plan_app_money += response[i].plan_app_money;
							count_fin += response[i].fin_plan_month;
							count_app += response[i].app_plan_month;
						}
					}
					response[0].plan_app_money = plan_app_money;
					response[0].fin_plan_month = count_fin;
					response[0].app_plan_month = count_app;
					response[0].app_plan = response[0].app_plan.toString()+' ('+app_plan.toString()+')';
					return response;
				},
				// onEditableShown: function(field,row,$el,editable){debugger;},
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					}
				}
			});
			this.$el.on('keyup', '.form-control.input-sm', function(){
			    this.value = this.value.replace(/ /g,'');
			    var number = this.value;
			    this.value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			});
		}
	});
	return ProjectReportsView;
})
