define(['text!projects/html/main_view.html', 'utils', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_reports_type_2_view')[0].outerHTML,
		events: {
			'click #setMonths': 'setMonths',
			'click .managers': 'opentableManagers',
		},
		regions: {
			managers: '#managers_region'
		},
		className: 'panel panel-default panelTabs displayNone form',
		initialize: function(){
			this.not_validate = ['plan_processing_db', 'fin_plan_month', 'ticket_plan'];
		},
		validateTable: function(row){
			var valid = true;
			if(!row.fin_plan_month){
				utils.notyAlert('Заполните ежемесячный план поступлений').show();
				valid = false;
			};
			if(!row.ticket_plan){
				utils.notyAlert('Заполните ежемесячный план купленных билетов').show();
				valid = false;
			};
			if(!row.plan_processing_db){
				utils.notyAlert('Заполните план обработки базы клиентов').show();
				valid = false;
			};
			return valid
		},
		setMonths: function(e){
			var _this = this;
			if(this.data_count && this.$el.find('input[name="months"]').val() != this.data_count){
				$.ajax({
					type: 'POST',
					url: '/apiprojects/project_reports_type2/create_plan/?project='+
					this.model.id+'&months='+this.$el.find('input[name="months"]').val(),
					success: function(response){
						utils.notySuccess().show();
						_this.Table.bootstrapTable('refresh');
					},
					error: function(response){
						utils.notyError().show();
					}
				});
			}
			if(this.data_count === null){
				$.ajax({
					type: 'POST',
					url: '/apiprojects/project_reports_type2/create_plan/?project='+
					this.model.id+'&months='+this.$el.find('input[name="months"]').val(),
					success: function(response){
						utils.notySuccess().show();
						_this.Table.bootstrapTable('refresh');
					},
					error: function(response){
						utils.notyError().show();
					}
				});
			}
		},
		opentableManagers: function(e){
			var _this = this;
			var id = $(e.currentTarget).data().id;
			var row = _this.Table.bootstrapTable('getRowByUniqueId', id);
			if(this.validateTable(row)){
				require(['projects/js/managers_report_type2_view'], function(TableManagersView){
					_this.showChildView('managers', new TableManagersView({
						report: id, 
						month: row.month_name,
						max_value1: row.fin_plan_month,
						max_value2: row.ticket_plan,
						max_value3: row.plan_processing_db
					}));
				});
			};
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var edit_settings = {
				type: 'text',
				emptytext: 'Установить',
				url: '/apiprojects/project_reports_type2/',
				ajaxOptions: {
					type: 'PATCH',
					dataType: 'json',
					beforeSend: function(jqXHR,settings){
						var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
						settings.url += row.id+'/';
					}
				},
				params: function(params){
					params[params.name] = params.value
					delete params['name'];
					delete params['value'];
					return params;
				},
				validate: function(value){
					var field_name = $(this).data().name;
					if(_this.not_validate.indexOf(field_name)==-1){
						var all_data = _this.Table.bootstrapTable('getData');
						var count = 0;
						for(var i=1; i<all_data.length; i++){
							if(all_data[i][field_name]){
								count += parseInt(all_data[i][field_name], 10); 
							}
						}
						if(parseInt(all_data[0][field_name], 10)-count<parseInt(value.replace(/\ /g, ''), 10)){
							return 'Введите число меньше '+ (parseInt(all_data[0][field_name], 10)-count).toString();
						}
					}
				},
				success: function(response){
					_this.Table.bootstrapTable('refresh', {silent: true});
				}
			};
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/project_reports_type2/table/?project='+_this.model.id,
				rowStyle: function(row, index) {
					if(index == 0){
						return{
							classes: 'no-editble',
							css: {'font-weight': 'bold'}
						};
					};
					return {classes:''}
				},
				columns: [
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						if(index != 0){
							return '<span style="margin-right: 0px; margin-left: 0px;" class="managers glyphicon glyphicon-briefcase pencil" data-id ='+
						row.id+'></span>';
						}
						else{
							return ''
						};
					}
				},
				{
					field: 'month_name',
					title: '',
					// sortable: true,
				},
				{
					field: 'fin_plan',
					title: 'Финансовый план на весь период',
					editable: edit_settings,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений',
					// formatter: function(value, row, index){
					// 	debugger;
					// },
					editable: edit_settings,

					cellStyle: function(value, row, index, field){
						return {
							classes: ''
						};
					}
					// sortable: true,
				},
				{
					field: 'fact_money',
					title: 'Факт поступлений ДС',
					// sortable: true,
				},
				{
					field: 'diff_money',
					title: 'Расхождение факта, с фин. планом',
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана'
				},
				{
					field: 'ticket_plan',
					title: 'План покупки билетов',
					editable: edit_settings,
				},
				{
					field: 'fact_ticket',
					title: 'Факт. покупки билетов'
				},
				{
					field: 'app_diff',
					title: 'Расхождение факта с планом'
				},
				{
					field: 'perc_comp',
					title: '% выполнения'
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов',
					editable: edit_settings,
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_processing_db',
					title: '% выполнения'
				},
				],
				onLoadSuccess: function (data) {
					_this.$el.find('input[name="months"]').val(data.length-1);
					_this.data_count = data.length - 1;
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				},
				onLoadError: function(){
					_this.data_count = null;
				}
			});

		}
	});
	return ProjectReportsView
})
