define(['text!projects/html/main_view.html', 
	'utils',
	'bootstrap-table-editable', 
	'bootstrap-table-locale', 
	'bootstrap-modal'], function(html, utils){

	var ProjectReportsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#managers_table_view')[0].outerHTML,
		events: {
			'click #addManager': 'addManager',
			'click #editManager': 'editManager',
			'click .removeManager': 'removeManager'
		},
		regions: {
		},
		className: 'displayNone',
		templateContext: function(){
			return {
				month: this.options.month
			};
		},
		initialize: function(){
		},
		getSumm: function(model){
			var arr = this.Table.bootstrapTable('getData');
			var s = $.extend({}, arr[0]);
			for(var i = 1; i < arr.length; i++){
				s.fin_plan_month-=arr[i].fin_plan_month;
				s.app_plan_month-=arr[i].app_plan_month;
				s.plan_processing_db-=arr[i].plan_processing_db;
			};
			if(model){
				s.fin_plan_month += model.get('fin_plan_month');
				s.app_plan_month += model.get('app_plan_month');
				s.plan_processing_db += model.get('plan_processing_db');
			};
			return s
		},
		removeManager: function(e){
			var _view = this;
			var id = $(e.currentTarget).data().id;
			var modal = new Backbone.BootstrapModal({ 
				content: 'Вы хотите удалить менеджера из плана?',
				title: 'Удалить?',
				okText: 'Удалить',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$.ajax({
					type: 'DELETE',
					url: '/apiprojects/report_managers/'+id+'/',
					success: function(response){
						_view.Table.bootstrapTable('refresh');
						utils.notySuccess('Удалено').show();
					},
					error: function(response){
						utils.notyError().show();	
					}
				});
			});
		},
		editManager: function(e){
			var id = $(e.currentTarget).data().id;
			var row = this.Table.bootstrapTable('getRowByUniqueId', id);
			this.addManager(e, new Backbone.Model(row));
		},
		addManager: function(e, model){
			var _view = this;
			var title = 'Добавить';
			var okText = 'Добавить';
			if(model){
				title = 'Редактировать';
				var okText = 'Сохранить';
			};
			var row = this.getSumm(model);
			require(['projects/js/add_manager_view'], function(AddManagerView){
				var modal = new Backbone.BootstrapModal({ 
					content: new AddManagerView({model: model, row: row}),
					title: title,
					okText: okText,
					cancelText: 'Отмена',
					okCloses: false,
					escape: false,
					modalOptions: {
						backdrop: 'static',
						keyboard: false
					},
					animate: true, 
				}).open();
				modal.on('ok', function(){
					var data = new FormData(this.$content.find('form')[0]);
					data.append('project_report', _view.options.report)
					var type = 'POST';
					var url = '/apiprojects/report_managers/';
					if(modal.options.content.model){
						type = 'PATCH';
						url = url + modal.options.content.model.id + '/';
					};
					$.ajax({
						type: type,
						url: url,
						processData: false,
						contentType: false,
						data: data,
						success: function(response){
							_view.Table.bootstrapTable('refresh', {silent: true});
							modal.close();
						},
						error: function(response){
							if(response.responseJSON && response.responseJSON.non_field_errors){
								utils.notyError('Этот менеджер уже назначен').show();
							}
							else{
								utils.notyError().show();	
							}
						}
					});
				});
			});
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var edit_settings = {
				type: 'text',
				emptytext: 'Установить',
				url: '/apiprojects/project_reports/',
				ajaxOptions: {
					type: 'PATCH',
					dataType: 'json',
					beforeSend: function(jqXHR,settings){
						var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
						settings.url += row.id+'/';
					}
				},
				params: function(params){
					params[params.name] = params.value;
					delete params['name'];
					delete params['value'];
					return params;
				},
				success: function(response){
					_this.Table.bootstrapTable('refresh', {silent: true});
				}
			};
			this.Table = this.$el.find('#table_managers').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				// sidePagination: 'server',
				// dataField: 'results',
				// checkbox: true,
				// sortable: true,
				// showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				// iconSize: 'sm',
				url: '/apiprojects/report_managers/table/?report='+_this.options.report,
				// onRefresh: function(options){
				// },
				// onClickRow: function(row, e, index){
				// },
				rowStyle: function(row, index) {
					if(index == 0){
						_this.first_row = row;
						return{
							classes: 'no-editble',
						};
					};
					return {classes:''}
				},
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						if(index != 0){
							return '<span style="margin-right: 0px; margin-left: 0px;" id="editManager" class="glyphicon glyphicon-pencil pencil" data-id ='+
						row.id+'></span><span style="margin-right: 0px; margin-left: 8px;" class="glyphicon glyphicon-trash trash removeManager" data-id ='+
						row.id+'></span>';
						}
						else{
							return '';
						}
					}
				},
				{
					field: 'manager.manager',
					title: 'Менеджер',
					// sortable: true,
				},
				{
					field: 'fin_plan_month',
					title: 'Ежемесячный план поступлений',
					// editable: edit_settings,
				},
				{
					field: 'plan_app_money',
					title: 'Сумма к поступлению (план)',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					cellStyle: function(value, row, index, field){
						return {
							classes: 'info',
							css: {'white-space': 'nowrap'}
						};
					}
				},
				{
					field: 'fact',
					title: 'Факт поступлений ДС',
					// sortable: true,
				},
				{
					field: 'perc',
					title: '% вып ежемес. плана'
				},
				{
					field: 'app_plan_month',
					title: 'Ежемес. план поступления заявок',
					// editable: edit_settings,
				},
				{
					field: 'fact_app',
					title: 'Факт. поступления заявок на весь период'
				},
				{
					field: 'perc_comp',
					title: '% выполнения'
				},
				{
					field: 'plan_processing_db',
					title: 'План обработки базы клиентов'
				},
				{
					field: 'fact_processing_db',
					title: 'Факт обработки базы клиентов'
				},
				{
					field: 'perc_comp_db',
					title: '% выполнения'
				},
				],
				responseHandler: function(response){
					if(response){
						response[0].manager = {manager: 'Итого'};
					}
					return response;
				},
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				}
			});

		}
	});
	return ProjectReportsView
})
