define(['text!projects/html/main_view.html','libs/backbone.paginator/js/paginator',
	'paginator-view', 'js/filters_view', 'bootstrap-table-locale', 'bootstrap-modal','datetimepicker'], function(html, Paginator, PaginatorView, FilterView){

    var DAY = {
      0     : "Вс",
      1     : "Пн",
      2     : "Вт",
      3     : "Ср",
      4     : "Чт",
      5     : "Пт",
      6     : "Сб",
    };

    var MONTH = {
      0     : "Янв",
      1     : "Фев",
      2     : "Мрт",
      3     : "Апр",
      4     : "Май",
      5     : "Июн",
      6     : "Июл",
      7     : "Авг",
      8     : "Сен",
      9     : "Окт",
      10    : "Нбр",
      11    : "Дек",

    }

    var BUILDING_DAYS = {'1': 0.4,'2': 0.75,'3': 1,'4': 1.2,'5': 2,'6': 2,'7': 4}

    CHOICES_AREA = (
        ('1', 'Периметр'),
        ('2', 'Свободная застройка (банкет, форум)'),
        ('3', 'Стандарт'),
        ('4', 'Стандарт + доп.оборудование'),
        ('5', 'Сложная застройка (форум, чемпионат)'),
        ('6', 'Стандарт + индивидуал'),
        ('7', 'Интерактив + мультимедия')
        )

    var STATUS = {
			'planning': 'Планирование',
			'working': 'В работе',
			'canceled': 'Отменен',
			'finished': 'Завершен'
	};

	var ADDDAY = 0

	var SITES = ['Аметист А','Аметист В','Аметист С', 'Выставочный павильон 1','Выставочный павильон 2','Открытая выставочная площадь фасад центр','Открытая выставочная площадь фасад юг']
    // var SITES = ['Выставочный павильон 1','Выставочный павильон 2','КВС Орион (2 эт)']

    var hexToRgb = function(hex, col) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? `rgb(${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}, ${col})` : null;
    }

    var editColumn =  function(){

            if($('#calendarDate').datetimepicker().data() && $('#calendarDate').datetimepicker().data().date != ''){
                d = new Date($('#calendarDate').datetimepicker().data().date + '-01');
            }else{
            d = new Date()
            ADDDAY=0}
            var col = []
            var countDay = 32 - new Date(d.getYear()+1900, d.getMonth(), 32).getDate();

            col.push({
                field: 'sites_name',
                title: 'Название',
                sortable: true,
                class:'calendarHead',
            })

            var date_minus = ADDDAY

            for(var i = 0+date_minus; i< 31+date_minus; i++){
            	if(i<countDay+date_minus){
                    var day = new Date(d.getYear()+1900, d.getMonth(), i-date_minus+1)
                    var day_nomer = new Date(day.setDate(day.getDate()+date_minus))
                    var month = day.getMonth()
                    var theBigDay = new Date();
                    theBigDay.setHours(0,0,0,0);
                    if(theBigDay.getTime() == day.getTime()){
                        day = day.getDay()
                        col.push({
                            field: 'date'+i,
                            title:  `${day_nomer.getDate()}\n ${DAY[day]} \n ${MONTH[month]}`,
                            sortable: true,
                            class: 'calendarBody nowDay',
                        })
                    }
                    else{
                        day = day.getDay()
                        col.push({
                            field: 'date'+i,
                            title:  `${day_nomer.getDate()}\n ${MONTH[month]} \n ${DAY[day]} `,
                            sortable: true,
                            class: (day==0 || day==6)?'calendarBody weekEnd':'calendarBody',
                        })
                    }
                }
                else{
                    col.push({
                        field: 'date'+i,
                        title:  ' - ',
                        sortable: true,
                        class:'calendarBody',
                    })
                }
            }
            return col
	}

	window.allHalls = function(){
        for(var i in $('#CalendarSites')[0].selectize.options){
            $('#CalendarSites')[0].selectize.addItem(parseInt(i))
        }

	}

	window.approveProject = function(val){
            $('#'+val.id).attr("disabled","disabled");
            var data =  {
                id: val.id,
                status:window.contents[val.value].status
            }

            var modalTemplateCreate = function(){
                var html = "Проект имеет пересечение. Вы действительно хотите запустить его в работу? <br>"
                html += `<div class="modal-footer"><button type="button" id = 'no_button' class="btn btn-ok" data-dismiss="modal">Отмена</button>`
                html += `<button type="button" id = 'ok_button' class="btn btn-ok" data-dismiss="modal">Подтвердить</button></div>`
                return html
            }

            var ModalView = Backbone.View.extend({
                events: {
                    'click #ok_button': 'okClicked',
                    'click #no_button': 'noClicked',
                },
                id:'modal_adm',
                tagName: 'div',
                template: modalTemplateCreate(),
                render: function() {
                    this.$el.html(this.template);
                    return this;
                },
                okClicked: function (modal) {
                    var temp = data.id
                    $.ajax({
				        type: 'PATCH',
                        url: '/apiprojects/projects/set_status/',
                        data: data,

                        success: function(response){
                            temp
                            $( "#cal_modal_error_"+temp )[0].innerHTML = 'Проект утвержден'
                            buttonSwitchingDate(0)

                        },
                        error: function(response){
                            temp
                            $( "#cal_modal_error_"+temp )[0].innerHTML = 'Ошибка при сохранении'
                            $('#'+val.id).removeAttr("disabled");

                        }
				    })
				},
				noClicked: function (modal) {
                    $('#'+val.id).removeAttr("disabled");
				},
            });

            $(document).ready(function() {
                var view = new ModalView();
                var modal = new Backbone.BootstrapModal({
                    content: view,
                    title: 'Подтверждение действия',
                    animate: true,
                    showFooter:false,
                });
                modal.open(function(){  });
            });
	}

	window.conformProject = function(val){
	    // debugger
       $.ajax({
            type: 'PATCH',
            url: 'apiprojects/projects/send_noty/?id='+val.id,
            data: {id:val.id},

            success: function(response){
            // debugger
                temp
                $( "#cal_modal_error_"+temp )[0].innerHTML = 'Проект утвержден'
                buttonSwitchingDate(0)

            },
            error: function(response){
            // debugger
                temp
                $( "#cal_modal_error_"+temp )[0].innerHTML = 'Ошибка при сохранении'
                $('#'+val.id).removeAttr("disabled");

            }
        })


	}

	window.buttonSwitchingDate =  function(count){
	    var init_date
		if($('#calendarDate').datetimepicker().data().date == ''){
            init_date = new Date();
        }else{
            init_date = new Date($('#calendarDate').datetimepicker().data().date + '-01');
            init_date.setMonth(init_date.getMonth() + count);
        }
        if (!isNaN(parseInt(count))){
            if (count!=0){
                ADDDAY = 0
            }
                $('#calendarDate').data("DateTimePicker").date(`${init_date.getYear()+1900}-${(init_date.getMonth()>=9)? init_date.getMonth()+1: '0'+(init_date.getMonth()+1)}`)
//            $('#calendarDate').datetimepicker().data().date = `${init_date.getYear()+1900}-${(init_date.getMonth()>=9)? init_date.getMonth()+1: '0'+(init_date.getMonth()+1)}`
        }else{
            if (count == 'del'){
                ADDDAY -=1
            }
            else if(count=='new'){
                ADDDAY = 0
            }
            else{
                ADDDAY += 1
            }

        }

        $('#tableCalendar').bootstrapTable('refresh')
        $('#tableCapacity').bootstrapTable('refresh')
	}

	var getDateStartAndStop =  function(date1, date2){

	    if (!date1 && !date2){
	        var d = new Date();
	        var countCol = 32 - new Date(d.getYear()+1900, d.getMonth(), 32).getDate();
	        var date_start = new Date(d.getYear()+1900,d.getMonth(),2)
	        var date_stop = new Date(d.getYear()+1900,d.getMonth(),countCol+1)
	        date_start.setMonth(date_start.getMonth() - 4)
	        date_start = date_start.toJSON()
	        date_stop.setMonth(date_stop.getMonth() + 4)
	        date_stop = date_stop.toJSON()
	        return '?calendar=true&date_start__gt='+date_start.slice(0,date_start.indexOf('T'))+'&date_end__lt='+date_stop.slice(0,date_stop.indexOf('T'))
	    }
	}

	var CalendarView = Backbone.Marionette.View.extend({
		template:$(html).filter('#CalendarView')[0].outerHTML,
		className: 'displayNone',
		defaultUrl: true,

		onRender: function(){
//		    debugger
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			function getUrlVars() {
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                    vars[key] = value;
                });
                return vars;
            }

            var date_project = new Date(getUrlVars()["date"]);

            this.$el.find('#calendarDate').datetimepicker({
				locale: 'ru',
				format: 'YYYY-MM',
				viewMode : 'months',
				toolbarPlacement: "top",
                allowInputToggle: true,
//				date: new Date()
                date: date_project == "Invalid Date"? new Date() : new Date(date_project)
			});
			this.$el.find('#calendarDate').on("dp.show", function(e) {
               $(e.target).data("DateTimePicker").viewMode("months");
            });
			this.CalendarSites = this.$el.find('#CalendarSites').selectize({
				plugins: ['remove_button'],
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				placeholder: 30,
				fetch:true,
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/sites/?orderby=name&sort=name',
						async: false,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i])

								if(SITES.indexOf(response[i].name)>=0){
									_this.addItem(response[i].id);
								}
							}
							return response;
						}
					});
				},
				render:{
					option: function(item, escape){
						return Handlebars.compile($(html).filter('#selectizeTemplate')[0].innerHTML)(item);
					}
				}
			});

			this.Table = this.$el.find('#tableCalendar').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				sortable: false,
				classes: 'table table-striped table-bordered calendar_table',
				iconSize: 'sm',
				url: '/apiprojects/calendar/table_data/?addday='+ADDDAY+'&sites='+this.$el.find('#CalendarSites').val(),
				newQuery: {},
				onRefresh: function(options){
					if(this.query){
						this.newQuery = this.query;
					}
					else{
						this.newQuery = {};
					};
				},
				onClickRow: function(row, e, index){
                    // debugger
					var CHOICES_AREA = {
                                '1': 'Периметр',
                                '2': 'Свободная застройка (банкет, форум)',
                                '3': 'Стандарт',
                                '4': 'Стандарт + доп.оборудование',
                                '5': 'Сложная застройка (форум, чемпионат)',
                                '6': 'Стандарт + индивидуал',
                                '7': 'Интерактив + мультимедия'
                            }
					if(row[index] != '' && row[index].length>100){
					    var modalTemplateCreate = function(){
					        var html = "", html_content = ''

					        date = parseInt(index.replace('date',''))
					        var content = row[index.replace('date','date_text')]
					        window.contents = content

					        html+=  '<script> $(function(){ $("#myTab a").click(function(e){e.preventDefault();$(this).tab("show"); });});</script>'
	                        html+= '<ul id="myTab" class="nav">'
	                        html_content+= '<div class="tab-content">'
					        for(i in content){

					        var qwer = ''
					            if(content[i].status == 'working'){
                                    qwer = `<img style="height: 20px; float: left;" src="/static/img/icon_ok.png">`
	                            }else if(content[i].status =='finished'){
                                    qwer = `<img style="height: 20px; float: left;" src="/static/img/icon_ok_end.png">`
	                            }

					            if (i==0){
					                html+=  `<li style = " background:${content[i].project_type.color}"class="active calendar"><a href="#panel${i}"><div></div>${qwer}${content[i].name}</a></li>`
					                html_content+=  ' <div id="panel'+i+'" class="tab-pane fade in active">'

					            }else{
					                html+=  `<li style = "background:${content[i].project_type.color}"class="calendar"><a href="#panel${i}"><div></div>${qwer}${content[i].name}</a></li>`
					                html_content+= ' <div id="panel'+i+'" class="tab-pane fade">'
					            }

					            var convert_date=function(date){
					                return `${('0' + new Date(date).getDate()).slice(-2)}.${('0' + (new Date(date).getMonth() + 1)).slice(-2)}.${new Date(date).getYear()+1900}`
					            }

					            var date_temp =  `c ${convert_date(content[i].date_start)} по ${convert_date(content[i].date_end)} `
					            var date_temp_event = `c ${convert_date(content[i].event_date_start?content[i].event_date_start:content[i].date_start)} по ${convert_date(content[i].event_date_end?content[i].event_date_end:content[i].date_end)} `

					            var date_i = new Date($('#calendarDate').datetimepicker().data().date + '-'+(parseInt(index.replace('date',''))+1)).getTime();

                                if (!window.user.isSuperuser && content[i].status == 'planning'){
	                                html_content+= `<button type="button" id=${content[i].id} value=${i} style="float: right; margin: 10px;" class="btn btn-danger" onClick="conformProject(this)"><span class="glyphicon glyphicon-exclamation-sign"></span> Отправить на согласование</button>`
	                            }
                                // debugger

                                if (window.user.isSuperuser && content[i].status == 'planning'){
	                                html_content+= `<button type="button" id=${content[i].id} value=${i} style="float: right; margin: 10px;" class="btn btn-danger" onClick="approveProject(this)"><span class="glyphicon glyphicon-exclamation-sign"></span> Утвердить</button>`
                                    html_content+=`<div style='float: left; margin: 20px; color: red;' id=${'cal_modal_error_' + content[i].id}></div>`
	                            }

                                var head_user = null
                                var user_find = null

	                            if(content[i].user_id != window.user.id){
	                                $.get({
                                        url: '/apiworkers/managers/',
                                        async: false,
                                        success: function(response) {
                                        debugger
                                            content[i]
                                            for(var res in response){
                                                if(response[res].id == user.manager_id){
                                                    head_user = response[res].id
                                                    break
                                                }
                                            }

                                            var find_user = function(temp_user){

                                                var us = null

                                                for(var res in response){
                                                    if(response[res].user.id == temp_user){
                                                        for(var res1 in response){
                                                            if(response[res1].user.id == response[res].head){
                                                                us = response[res1].user.id
                                                            }
                                                        }
                                                    }
                                                    if(temp_user == user.id){
                                                        return [us, temp_user]
                                                    }
                                                }
                                                return [us, null]
                                            }

                                            var temp_user = content[i].user_id
                                            while(!user_find && temp_user){
                                                [temp_user, user_find]= find_user(temp_user)
                                            }
                                        }
							        });
	                            }
	                             debugger
	                            if(window.user.isSuperuser || content[i].user_id == window.user.id || user_find){
	                                html_content+= `<button type="button" id = "close_b" value=${content[i].id} style="float: right; margin: 10px;" class="btn btn-success"> К проекту</button>`
	                            }

					            html_content+='<table class="table table-bordered">'
					            html_content+=    `<tr><th>Название проекта</th><td>${content[i].name.replace("'", '') }</td></tr>`
	                            html_content+=    `<tr><th>Вид проекта</th><td>${content[i].project_type.type}</td></tr>`
	                            html_content+=    `<tr><th>Организатор</th><td>${content[i].buisness_unit.name}</td></tr>`
	                            html_content+=    `<tr><th>Ответственный руководитель</th><td>${content[i].user}</td></tr>`
	                            html_content+=    `<tr><th>Период проведения проекта</th><td>${date_temp}</td></tr>`
	                            html_content+=    `<tr><th>Период проведения мероприятия</th><td>${date_temp_event}</td></tr>`
	                            html_content+=    `<tr><th>Время проекта</th><td> с ${content[i].time_start.slice(0,5)} до ${date_i==new Date(content[i].date_end).getTime()? content[i].time_last.slice(0,5) : content[i].time_end.slice(0,5)}</td></tr>`
	                            html_content+=    `<tr><th>Статус</th><td>${STATUS[content[i].status]}</td></tr>`
	                            html_content+=    `<tr><th>Сложность застройки</th><td>${CHOICES_AREA[content[i].area_size]}</td></tr>`

                                if(content[i].project_type.type_project_type){
                                    html_content+=    `<tr><th>№/сумма договора</th><td>`
                                    for(var contract in content[i].contract){
                                        html_content+=    `${content[i].contract[contract]['Номер']} / ${content[i].contract[contract]['СуммаCчета']}<br>`
                                    }
                                    html_content+=    `</td></tr>`
                                }


	                            if(content[i].client_name){
	                                html_content+=    `<tr><th>Клиент</th><td>${content[i].client_name}</td></tr>`
	                            }
	                            if(content[i].catering_service && content[i].catering_service.length ){
	                               html_content+=    `<tr><th>Услуги питания</th><td>`
	                               for(var catering in content[i].catering_service){
	                                html_content+=    `${content[i].catering_service[0].catering_type_display} \
	                                c ${content[i].catering_service[0].time_start} \
	                                до ${content[i].catering_service[0].time_end} <br>`

	                               }
	                               html_content+=    `</td></tr>`
	                            }
	                            html_content+='</table>'

	                            html_content+=   '</div>'

					        }

					        html+= '</ul>'
					        html += html_content
	                        return html
					    }

	                    var ModalView = Backbone.View.extend({
	                        id:'mymod',
	                        events: {
                                'click #close_b': 'close',
                            },
	                        tagName: 'div',
	                        template: modalTemplateCreate(),
	                        render: function() {
	                            this.$el.html(this.template);
	                            return this;
	                        },
	                        close: function(elem) {
	                            // debugger

	                            _view.modalCalendar.close();
	                            router.navigate('project/'+elem.target.value+'/edit', {trigger: true});
                            },
	                    });
	                    var select_date_temp = new Date($('#calendarDate').datetimepicker().data().date + "-" + (parseInt(index.replace('date',''))+1));
	                    var select_date =  `${('0' + select_date_temp.getDate()).slice(-2)}.${('0' + (select_date_temp.getMonth() + 1)).slice(-2)}.${select_date_temp.getYear()+1900} `

	                    $(document).ready(function() {
                            var view = new ModalView();
                            _view.modalCalendar = new Backbone.BootstrapModal({
                                id:'calendarModalBody',
                                content: view,
                                title: select_date,
                                animate: true,
                                showFooter:false,
                                escape: false,
								modalOptions: {
									backdrop: 'static',
									keyboard: false
								},
                            }).open();
	                    });
	                }
				},
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer", "padding": '0px'}
					};
				},

				columns:editColumn(),

				responseHandler: function(response){

                    window.response_project = response.results
				    var temp_width_div = Math.floor(($('#main_content')[0].clientWidth - 200)/31)

                    var width_div1 = temp_width_div>25 ? temp_width_div  : 25
                    width_div1 = temp_width_div<40 ? width_div1+1  : 40

                     $('.calendar_table tbody').css('width', '100%')

                     $('.calendar_table thead th').css("height", '54px');
                     $('.calendar_table tbody').scroll(function(e) {

                        $('.calendar_table thead').css("left", -$("tbody").scrollLeft());
                        $('.calendar_table thead th:nth-child(1)').css("left", $("tbody").scrollLeft());
                        $('.calendar_table tbody td:nth-child(1)').css("left", $("tbody").scrollLeft());
                     });

				    if($('#calendarDate').datetimepicker().data().date == ''){
					    ADDDAY = 0
						var init_date = new Date();
						$('#calendarDate').datetimepicker().data().datee = `${init_date.getYear()+1900}-${(init_date.getMonth()>=9)? init_date.getMonth()+1: '0'+(init_date.getMonth()+1)}`
					}
                    var col = editColumn()

					$( "#tableCalendar th" ).each(function( index ) {
                        if(col[index]){
                            $( this ).text(col[index].title)
                            $( this ).removeClass("weekEnd");
                            $( this ).removeClass("nowDay");

                        $( this ).addClass(col[index].class);
                        }
                        else{ $( this ).text('')}
                    });

                    var results = response;
                    var d = new Date($('#calendarDate').datetimepicker().data().date + '-01');

                    var countCol = new Date(d.getFullYear(), d.getMonth()+1, 0).getDate()

                    var sites= [],res = {}, sites_data = {}

                    $.get({
                        url: '/apiprojects/sites/?orderby=name&sort=name',
                        async: false,
                        success: function(resp) {
                            var sites_param = $('#CalendarSites').val().split(',')
                            var temp = []
                            for (var i = 0; i < resp.length; i++) {

                                if (sites_param.indexOf(resp[i].id.toString())>=0){
                                     sites.push(resp[i].id)
                                     sites_data[resp[i].id] = resp[i]
                                }
                            };
                            return temp;
                        }
                    });

                    for(var i in sites){

                        res[sites[i]] = {}
                        for(var n=0; n<countCol; n++){
                            res[sites[i]][n] = []
                            var date_calendar= new Date($('#calendarDate').datetimepicker().data().date + (n<9 ? '-0'+(n+1) : '-'+(n+1)));
                            date_calendar.setDate(date_calendar.getDate() + ADDDAY);
                            for(var j in results[sites[i]]){

                                var start_project = ''
                                var end_project = ''

                                if(sites_data[sites[i]].building_time){
                                    if(results[sites[i]][j].rooms.length){
                                        for(var room in results[sites[i]][j].rooms){
//                                            debugger
                                            if (results[sites[i]][j].rooms[room].site ==sites[i] ){
                                                start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_start.slice(0,10)))
                                                end_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_end.slice(0,10)))
                                                if(results[sites[i]][j].rooms[room].start_building){
                                                    start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].start_building.slice(0,10)))
                                                }
                                                if(results[sites[i]][j].rooms[room].day_arrival && new Date(Date.parse(results[sites[i]][j].rooms[room].start_building.slice(0,10))) > start_project){
                                                    start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].day_arrival.slice(0,10)))
                                                }
                                                if(results[sites[i]][j].rooms[room].end_building){
                                                    end_project = new Date(Date.parse(results[sites[i]][j].rooms[room].end_building.slice(0,10)))
                                                }
                                                if(date_calendar>=start_project && date_calendar<=end_project){
                                                    res[sites[i]][n].push(results[sites[i]][j])
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        start_project = new Date(Date.parse(results[sites[i]][j].start_building))
                                        end_project = new Date(Date.parse(results[sites[i]][j].end_building))
                                        if(date_calendar>=start_project && date_calendar<=end_project){
                                            res[sites[i]][n].push(results[sites[i]][j])
                                        }
                                    }
                                }else{
                                    if(results[sites[i]][j].rooms.length){
                                        for(var room in results[sites[i]][j].rooms){
                                            if (results[sites[i]][j].rooms[room].site ==sites[i] ){
                                                start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_start.slice(0,10)))
                                                end_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_end.slice(0,10)))
                                                if(results[sites[i]][j].rooms[room].start_building){
                                                    start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].start_building.slice(0,10)))
                                                }
                                                if(results[sites[i]][j].rooms[room].day_arrival && new Date(Date.parse(results[sites[i]][j].rooms[room].start_building.slice(0,10))) > start_project){
                                                    start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].day_arrival.slice(0,10)))
                                                }
                                                if(results[sites[i]][j].rooms[room].end_building){
                                                    end_project = new Date(Date.parse(results[sites[i]][j].rooms[room].end_building.slice(0,10)))
                                                }
                                                if(date_calendar>=start_project && date_calendar<=end_project){
                                                    res[sites[i]][n].push(results[sites[i]][j])
                                                }
                                            }
                                        }
                                    }else{
                                        start_project = new Date(Date.parse(results[sites[i]][j].date_start))
                                        end_project = new Date(Date.parse(results[sites[i]][j].date_end))

                                        if(date_calendar>=start_project && date_calendar<=end_project){
                                            res[sites[i]][n].push(results[sites[i]][j])
                                        }
                                    }

                                }
                            }

                        }
                    }

                    debugger

                    // отображение клеток

                    var CHOICES_AREA = {
                        '1': 'Периметр',
                        '2': 'Свободная застройка (банкет, форум)',
                        '3': 'Стандарт',
                        '4': 'Стандарт + доп.оборудование',
                        '5': 'Сложная застройка (форум, чемпионат)',
                        '6': 'Стандарт + индивидуал',
                        '7': 'Интерактив + мультимедия'
                    }

                    var result = []

                    for(var i in res){
                        temp_result = {
                            sites_name:sites_data[i].name
                        }
                        for(var j in res[i]){
                            temp_result['date'+j] =''
                            temp_result['date_text'+j] =[]

                                for(var k in res[i][j]){
                                    var style_div = ''
                                    var border = '; border: 1px solid #a9a6a6;'

                                    if(res[i][j].length>1){
                                        border = '; border: 2px solid #FF5722;'
                                    }

                                    var html_content=''

                                    html_content+='<table class="table table-bordered" >'
                                    html_content+=    `<tr><th>Название проекта</th><td>${res[i][j][k].name.replace("'", '') }</td></tr>`
                                    html_content+=    `<tr><th>Вид проекта</th><td>${res[i][j][k].project_type ? res[i][j][k].project_type.type : ''}</td></tr>`
                                    html_content+=    `<tr><th>Организатор</th><td>${res[i][j][k].buisness_unit ? res[i][j][k].buisness_unit.name: ''}</td></tr>`
                                    html_content+=    `<tr><th>Сложность застройки</th><td>${res[i][j][k].area_size ? CHOICES_AREA[res[i][j][k].area_size]: ''}</td></tr>`

                                    html_content+='</table>'


                                    var qwer = ''

                                    if(res[i][j][k].status == 'working'){
                                        qwer = `<span style="line-height:${1}"><img style="margin-bottom: 1px; max-height:100%;  max-width: 100%; " src="/static/img/icon_ok.png"><span>`
                                    }else if(res[i][j][k].status =='finished'){
                                        qwer = `<span style="line-height:${1}"><img style="margin-bottom: 1px; max-height:100%;  max-width: 100%; " src="/static/img/icon_ok_end.png"<span>`
                                    }

                                    if(sites_data[i].building_time){

                                        var start_b = new Date(Date.parse(res[i][j][k].start_building))
                                        var end_b = new Date(Date.parse(res[i][j][k].end_building))
                                        var start_p = new Date(Date.parse(res[i][j][k].date_start))
                                        var end_p = new Date(Date.parse(res[i][j][k].date_end))
                                        var day_arrival = null

                                        if (res[i][j][k].rooms.length){
                                            for(var room_local in res[i][j][k].rooms){
                                                if(res[i][j][k].rooms[room_local].site == i){
                                                    if(res[i][j][k].rooms[room_local].date_start){
                                                        var start_p = new Date(Date.parse(res[i][j][k].rooms[room_local].date_start.split('T')[0]))
                                                    }
                                                    if(res[i][j][k].rooms[room_local].date_start){
                                                        var end_p = new Date(Date.parse(res[i][j][k].rooms[room_local].date_end.split('T')[0]))
                                                    }
                                                    if(res[i][j][k].rooms[room_local].start_building){
                                                        var start_b = new Date(Date.parse(res[i][j][k].rooms[room_local].start_building.split('T')[0]))
                                                    }
                                                    if(res[i][j][k].rooms[room_local].end_building){
                                                        var end_b = new Date(Date.parse(res[i][j][k].rooms[room_local].end_building.split('T')[0]))
                                                    }
                                                    if(res[i][j][k].rooms[room_local].day_arrival){
                                                        var day_arrival = new Date(Date.parse(res[i][j][k].rooms[room_local].day_arrival.split('T')[0]))
                                                    }
                                                }
                                            }

                                        }

                                        day = parseInt(j)
                                        var date= new Date($('#calendarDate').datetimepicker().data().date + (day<9 ? '-0'+(day+1) : '-'+(day+1)));
                                        date.setDate(date.getDate() + ADDDAY);
                                        if (date>=start_b && date<start_p){
                                            var start_e = new Date(res[i][j][k].event_date_start)
                                            if(res[i][j][k].event_date_start && date>=new Date(res[i][j][k].event_date_start) && !res[i][j][k].rooms.length){

                                                style_div = `"background : ${res[i][j][k].project_type.color} linear-gradient(rgba(0, 0, 0, 0) 50%, rgba(255, 255, 255, .2) 50% ) center center / 100% 1em;
                                                            height:${Math.round(100/(res[i][j].length))}% ;
                                                            max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Заезд</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }else if(day_arrival && date>=day_arrival){

                                               style_div = `"background : ${res[i][j][k].project_type.color} linear-gradient(rgba(0, 0, 0, 0) 50%, rgba(255, 255, 255, .2) 50% ) center center / 100% 1em;
                                                            height:${Math.round(100/(res[i][j].length))}% ;
                                                            max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Заезд</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }else{

                                               style_div = `"background : ${hexToRgb(res[i][j][k].project_type.color, 0.7)};
                                                height:${Math.round(100/(res[i][j].length))}% ;
                                                max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Монтаж</h4>${html_content}' style= ${style_div}>${qwer}</div>`
                                            }

                                        }else if(date>end_p && date<=end_b){

                                            if(res[i][j][k].event_date_end && date<=new Date(res[i][j][k].event_date_end) && !res[i][j][k].rooms.length){
                                                style_div = `"background : ${res[i][j][k].project_type.color} linear-gradient(rgba(0, 0, 0, 0) 50%, rgba(255, 255, 255, .2) 50% ) center center / 100% 1em;
                                                            height:${Math.round(100/(res[i][j].length))}% ;
                                                            max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Выезд</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }else{
                                                var style_div = `"background : ${hexToRgb(res[i][j][k].project_type.color, 0.4)};
                                                    height:${Math.round(100/(res[i][j].length))}% ;
                                                    max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Демонтаж</h4>${html_content}' style= ${style_div}>${qwer}</div>`
                                            }
                                        }else{
                                            if(res[i][j][k].event_date_start && date<new Date(res[i][j][k].event_date_start) && !res[i][j][k].rooms.length){
                                                style_div = `"background : ${res[i][j][k].project_type.color} linear-gradient(rgba(0, 0, 0, 0) 50%, rgba(255, 255, 255, .2) 50% ) center center / 100% 1em;
                                                            height:${Math.round(100/(res[i][j].length))}% ;
                                                            max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Выезд</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }else if(day_arrival && date<day_arrival){

                                                style_div = `"background : ${res[i][j][k].project_type.color} linear-gradient(rgba(0, 0, 0, 0) 50%, rgba(255, 255, 255, .2) 50% ) center center / 100% 1em;
                                                            height:${Math.round(100/(res[i][j].length))}% ;
                                                            max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Выезд</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }
                                            else if(res[i][j][k].event_date_end && date>new Date(res[i][j][k].event_date_end) && !res[i][j][k].rooms.length){
                                                style_div = `"background : ${res[i][j][k].project_type.color} linear-gradient(rgba(0, 0, 0, 0) 50%, rgba(255, 255, 255, .2) 50% ) center center / 100% 1em;
                                                            height:${Math.round(100/(res[i][j].length))}% ;
                                                            max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Выезд</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }else{
                                                var style_div = `"background : ${res[i][j][k].project_type.color};
                                                                height:${Math.round(100/(res[i][j].length))}% ;
                                                                max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                                temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Проект</h4>${html_content}' style= ${style_div}>${qwer}</div>`

                                            }
                                        }

                                    }else{
                                        var style_div = `"background : ${res[i][j][k].project_type["color"]};
                                            height:${Math.round(100/(res[i][j].length))}% ;max-height:${Math.round(100/(res[i][j].length))}px ${border}"`
                                            temp_result['date'+j] += `<div class = "calendar_div" data-toggle="tooltip" rel='tooltip' title='<h4>Проект</h4>${html_content}' style= ${style_div}>${qwer}</div>`
                                    }

                                    temp_result['date_text'+j].push(res[i][j][k])
                                }
                            }
                        result.push(temp_result)
                    }
                    // debugger
                    response.results = result

                    $( document ).ready(function() {
                         $("[rel=tooltip]").tooltip({
                             html:true,
                             container: 'body',});
                    });
                    return response

				},
				queryParams: function(params){

				if($('#calendarDate').datetimepicker().data() && $('#calendarDate').datetimepicker().data().date != ''){
                    this.url = '/apiprojects/calendar/table_data/?adday='+ADDDAY+'&sites='+$('#CalendarSites').val()+'&date='+$('#calendarDate').datetimepicker().data().date

				}
				params.limit = 1000
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {

				}
			});

			this.Table = this.$el.find('#tableConventions').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: false,
				search: false,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: false,
				sortable: false,
				showColumns: false,
				classes: 'table  table-bordered info_table',
				iconSize: 'sm',
				url: '/apiprojects/projects_types/',
				newQuery: {},
				onRefresh: function(options){
					if(this.query){
						this.newQuery = this.query;
					}
					else{
						this.newQuery = {};
					};
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer", "padding": '0px',"height": "25px", "vertical-align": "middle"}
					};
				},

				columns:[{
                    field: 'type',
                    title: 'Тип проекта',
                    sortable: true,
                },{
                    field: 'color_m',
                    title: 'Монтаж',
                    sortable: true,

                    class:'one_coll_info',
                },{
                    field: 'color',
                    title: 'Проект',
                    sortable: true,

                    class:'one_coll_info',
                },{
                    field: 'color_d',
                    title: 'Демонтаж',
                    sortable: true,

                    class:'one_coll_info',
                }],

				responseHandler: function(response){
				    var res = []

				    for(i in response.results){
				        if (response.results[i].color){
				        var color = response.results[i].color
				            response.results[i].color = `<div style="height: 100%; border: 1px solid #a9a6a6; background:${color}"></div>`;
				            response.results[i].color_m = `<div style="height: 100%; border: 1px solid #a9a6a6; background:${hexToRgb(color, 0.7)}"></div>`
				            response.results[i].color_d = `<div style="height: 100%; border: 1px solid #a9a6a6; background:${hexToRgb(color, 0.4)}"></div>`
				            res.push(response.results[i])
				        }
				    }
				    res.push({'type':'Пересечение проектов',
				                'color':`<div style="height: 100%; border: 2px solid #D50000;"  ></div>`,
				                'color_m':`<div style="height: 100%; border: 2px solid #D50000;"  ></div>`,
				                'color_d':`<div style="height: 100%; border: 2px solid #D50000;"  ></div>`})
				    res.push({'type':'Пересечение  утв. проектов',
				                'color':`<div style="height: 100%; border: 2px solid #FF5722;"  ></div>`,
				                'color_m':`<div style="height: 100%; border: 2px solid #FF5722;"  ></div>`,
				                'color_d':`<div style="height: 100%; border: 2px solid #FF5722;"  ></div>`})

                    response.results = res

					return response
				},
				queryParams: function(params){


				params.limit = 1000
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {

				}
			});

			this.Table = this.$el.find('#tableAreaSize').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				sortable: false,
				classes: 'table  table-bordered info_table',
				iconSize: 'sm',
				method:"OPTIONS",
				url: '/apiprojects/projects/',
				newQuery: {},
				onRefresh: function(options){
					if(this.query){
						this.newQuery = this.query;
					}
					else{
						this.newQuery = {};
					};
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {

					return {
						css: {"cursor": "pointer", "padding": '0px',"height": "25px", "vertical-align": "middle"}
					};
				},
				columns:[{
                    field: 'display_name',
                    title: 'Сложность застройки',
                    sortable: true,
                },{
                    field: 'value',
                    title: 'Коэффициент',
                    sortable: true,
                    class:'one_coll_info',
                }],
				responseHandler: function(response){

				    var res = []
                    res.count = response.actions.POST.area_size.choices.length
                    res.next = null
                    res.previous = null
                    res.results = response.actions.POST.area_size.choices


                    for(var r in res.results){
                        res.results[r].value = BUILDING_DAYS[res.results[r].value]
                    }
					return res
				},
				queryParams: function(params){

				    params.limit = 1000
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				}
			});

			this.Table = this.$el.find('#tableCapacity').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				sortable: false,
				classes: 'table  table-bordered info_table',
				iconSize: 'sm',
				newQuery: {},
				onRefresh: function(options){

					if(this.query){
						this.newQuery = this.query;
					}
					else{
						this.newQuery = {};
					};
				},
                url: '/apiprojects/calendar/table_data/?report=true&adday='+ADDDAY+($('#calendarDate').datetimepicker().data()?'&date='+$('#calendarDate').datetimepicker().data().date:''),
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				columns:[
				{
                    field: 'type',
                    title: 'Вид расчета',
                    sortable: true,
                    },
				{
                    field: 'all',
                    title: 'Процент загрузки залов',
                    sortable: true,
                    class:'one_coll_info'
                    },
                {
                    field: 'congress',
                    title: 'Конгрессные',
                    sortable: true,
                    class:'one_coll_info'},
                {
                    field: 'ametist',
                    title: 'Аметист',
                    sortable: true,
                    class:'one_coll_info',},
                {
                    field: 'exhibition',
                    title: 'Выставочные',
                    sortable: true,
                    class:'one_coll_info',
                }],
                responseHandler: function(response){

                    var sites= [],sites_data = {}, res={}, res_not_build={}
                    var results = response

                    $.get({
                        url: '/apiprojects/sites/?orderby=name&sort=name',
                        async: false,
                        success: function(resp) {

                            for (var i = 0; i < resp.length; i++) {
                                sites.push(resp[i].id)
                                sites_data[resp[i].id] = resp[i]
                            };
                        }
                    });

                    var d = new Date($('#calendarDate').datetimepicker().data().date + '-01');
                    var countCol = new Date(d.getFullYear(), d.getMonth()+1, 0).getDate()
                    // debugger

                    for(var i in sites){
                        res[sites[i]] = {}
                        res_not_build[sites[i]] = {}
                        for(var n=0; n<countCol; n++){
                            res[sites[i]][n] = []
                            res_not_build[sites[i]][n] = []
                            var date_calendar= new Date($('#calendarDate').datetimepicker().data().date + (n<9 ? '-0'+(n+1) : '-'+(n+1)));
                            date_calendar.setDate(date_calendar.getDate() + ADDDAY);
                            for(var j in results[sites[i]]){
                                var start_project = ''
                                var end_project = ''

                                var start_project2 = ''
                                var end_project2 = ''


                                if(sites_data[sites[i]].building_time){
                                    if(results[sites[i]][j].rooms.length){
                                        for(var room in results[sites[i]][j].rooms){
                                            if (results[sites[i]][j].rooms[room].site ==sites[i] ){
                                                start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_start.slice(0,10)))
                                                end_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_end.slice(0,10)))
                                                start_project2 = new Date(Date.parse(results[sites[i]][j].rooms[room].date_start.slice(0,10)))
                                                end_project2 = new Date(Date.parse(results[sites[i]][j].rooms[room].date_end.slice(0,10)))
                                            }
                                        }
                                    }
                                    else{
                                        start_project = new Date(Date.parse(results[sites[i]][j].start_building))
                                        end_project = new Date(Date.parse(results[sites[i]][j].end_building))
                                        start_project2 = new Date(Date.parse(results[sites[i]][j].date_start))
                                        end_project2 = new Date(Date.parse(results[sites[i]][j].date_end))
                                    }
                                }else{
                                    if(results[sites[i]][j].rooms.length){
                                        for(var room in results[sites[i]][j].rooms){
                                            if (results[sites[i]][j].rooms[room].site ==sites[i] ){
                                                start_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_start.slice(0,10)))
                                                end_project = new Date(Date.parse(results[sites[i]][j].rooms[room].date_end.slice(0,10)))
                                                start_project2 = new Date(Date.parse(results[sites[i]][j].rooms[room].date_start.slice(0,10)))
                                                end_project2 = new Date(Date.parse(results[sites[i]][j].rooms[room].date_end.slice(0,10)))

                                            }
                                        }
                                    }else{
                                        start_project = new Date(Date.parse(results[sites[i]][j].date_start))
                                        end_project = new Date(Date.parse(results[sites[i]][j].date_end))
                                        start_project2 = new Date(Date.parse(results[sites[i]][j].date_start))
                                        end_project2 = new Date(Date.parse(results[sites[i]][j].date_end))
                                    }

                                }

                                if(date_calendar>=start_project && date_calendar<=end_project){
                                    res[sites[i]][n].push(results[sites[i]][j])
                                }
                                if(date_calendar>=start_project2 && date_calendar<=end_project2){
                                    res_not_build[sites[i]][n].push(results[sites[i]][j])

                                }
                            }

                        }
                    }

                    var result_sites = [{
                        all:{
                            count:0,
                            day:0,
                            count_buy:0,
                            day_buy:0,
                        },
                        ametist:{
                            count:0,
                            day:0,
                            count_buy:0,
                            day_buy:0,

                        },
                        congress:{
                            count:0,
                            day:0,
                            count_buy:0,
                            day_buy:0,
                        },
                        exhibition:{
                            count:0,
                            day:0,
                            count_buy:0,
                            day_buy:0,
                        },
                    }]

                    for(var i in res){
                        site = sites_data[i]
                        if(site.ametist || site.congress || site.exhibition){
                            for(var j in res[i]){
                                // debugger
                                result_sites[0].all.count+=(res[i][j].length ? 1:0)
                                result_sites[0].all.day+=1
                                result_sites[0].all.count_buy+=(res_not_build[i][j].length ? 1:0)
                                result_sites[0].all.day_buy+=1
                                if(site.ametist){
                                    result_sites[0].ametist.count+=(res[i][j].length ? 1:0)
                                    result_sites[0].ametist.day+=1
                                    result_sites[0].ametist.count_buy+=(res_not_build[i][j].length ? 1:0)
                                    result_sites[0].ametist.day_buy+=1

                                }
                                if(site.congress){
                                    result_sites[0].congress.count+=(res[i][j].length ? 1:0)
                                    result_sites[0].congress.day+=1
                                    result_sites[0].congress.count_buy+=(res_not_build[i][j].length ? 1:0)
                                    result_sites[0].congress.day_buy+=1

                                }
                                if(site.exhibition){
                                    result_sites[0].exhibition.count+=(res[i][j].length ? 1:0)
                                    result_sites[0].exhibition.day+=1
                                    result_sites[0].exhibition.count_buy+=(res_not_build[i][j].length ? 1:0)
                                    result_sites[0].exhibition.day_buy+=1

                                }
                            }
                        }
                    }

                    var result = [{
                            type:'С учетом застройки',
                            all:(result_sites[0].all.count/result_sites[0].all.day*100).toFixed()+"%",
                            ametist:(result_sites[0].ametist.count/result_sites[0].ametist.day*100).toFixed()+"%",
                            congress:(result_sites[0].congress.count/result_sites[0].congress.day*100).toFixed()+"%",
                            exhibition:(result_sites[0].exhibition.count/result_sites[0].exhibition.day*100).toFixed()+"%",
                        },{
                            type:'Без учета застройки',
                            all:(result_sites[0].all.count_buy/result_sites[0].all.day_buy*100).toFixed()+"%",
                            ametist:(result_sites[0].ametist.count_buy/result_sites[0].ametist.day_buy*100).toFixed()+"%",
                            congress:(result_sites[0].congress.count_buy/result_sites[0].congress.day_buy*100).toFixed()+"%",
                            exhibition:(result_sites[0].exhibition.count_buy/result_sites[0].exhibition.day_buy*100).toFixed()+"%",
                    }]

                    response.count = 2
                    response.results = result

				    return response


				},
				queryParams: function(params){

                    if($('#calendarDate').datetimepicker().data() && $('#calendarDate').datetimepicker().data().date != ''){

                        this.url =  '/apiprojects/calendar/table_data/?adday='+ADDDAY+($('#calendarDate').datetimepicker().data()?'&date='+$('#calendarDate').datetimepicker().data().date:'')

                    }
                    params.limit = 1000
                        if(params.sort){
                            params.sort = params.sort.replace('.', '__');
                            if(params.sort == 'user'){
                                params.sort = 'user__last_name';
                            };
                        };
                        if(params.order == 'desc'){
                            params.sort = '-' + params.sort;
                            return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
                        };
                        return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {

				}


			});

			$('#tableCapacity').bootstrapTable('load', {});
		},
	});

return CalendarView

})


