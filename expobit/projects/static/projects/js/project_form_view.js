define(['text!projects/html/main_view.html', 'Noty', 'utils', 'selectize', 'bootstrap-modal', 'datetimepicker'], function(html, Noty, utils){
	var ProjectFormView = Backbone.Marionette.View.extend({
		statusView:{
			'planning':'Планирование',
			'working': 'В работе',
			'finished': 'Завершен',
			'canceled': 'Отменен'
		},
		events:{
			'click .openCollapse': 'openCollapse',
			'click #create_section': 'createSection',
			'click .delete': 'removeRow',
			'click .edit': 'editRow',
			'click .delete_catering': 'removeCatering',
			'click .edit_catering': 'editCatering',
			'click #PreSave': 'PreSave',
			'click #SaveButton': 'save',
			'click #CancelButton': 'cancel',
			'change .roomReservation': 'changeRoomReservation',
			'change .roomReservationNotNec': 'changeRoomReservationNotNec',
			'click #addCatering': 'addCatering',
			'click .addSites': 'addSites',
			'click .deleteSites': 'deleteSites',
		},
		cancel: function(e){
			history.back();
			// mainView.GetProjectView();
		},
		preSaveBool: false,
		removeCatering: function(e){
			var _this=this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить услугу?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				var row = _this.TableCatering.bootstrapTable('getRowByUniqueId', $(e.currentTarget).data().id);
				$('.loading').fadeIn();
				$.ajax({
					type: 'DELETE', 
					url: '/apiprojects/catering/' + row.id,
					success: function(response){
						utils.notySuccess.show('Удалено');
					},
					error: function(){
						utils.notyError().show();
					}
			});
			});
		},
		editCatering: function(e){
			var row = this.TableCatering.bootstrapTable('getRowByUniqueId', $(e.currentTarget).data().id);
			this.addCatering(e, new Backbone.Model(row));
		},
		addCatering: function(e, model){
			var _view = this;
			require(['projects/js/catering_view'], function(View){
				var modal = new Backbone.BootstrapModal({
				title: 'Услуга питания', 
				content: new View({model: model}),
				okText: 'Сохранить',
				cancelText: 'Отмена',
				animate: true, 
				okCloses: false
				}).open();
				modal.on('ok', function(){
					if(!modal.options.content.validate()){
						return;
					}
					var type = 'POST';
					var url = '/apiprojects/catering/';
					if(model){
						type = 'PATCH'
						url += model.id + '/'; 
					}
					var data = new FormData(modal.options.content.$el.find('form')[0]);
					data.append('project', _view.model.id);
					$.ajax({
						url: url,
						type: type,
						contentType: false,
						processData: false,
						data: data,
						success: function(response){
							utils.notySuccess().show();
							_view.TableCatering.bootstrapTable('refresh', {url: '/apiprojects/catering/?project='+_view.model.id});
						},
						error: function(){
							utils.notyError().show();
						}
					});
				});
			});
		},
		addSites: function(e, model){
			var _view = this;
            if(_view.$el.find('#complexity_area_date_'+e.target.getAttribute('name'))[0]){
                var options = {}
//                this.$el.find('#SelectSites')["0"].selectize.options
                for(var i in this.$el.find('#SelectSites')["0"].selectize.options){
                    if(this.$el.find('#SelectSites')["0"].selectize.options[i].id == e.target.getAttribute('name')){
                        options = this.$el.find('#SelectSites')["0"].selectize.options[i]
                    }
                }

                var html_tmp1 = Handlebars.compile($(html).filter('#complexity_area_date')[0].innerHTML)({
                    sites_option: options,
                    reserv_options: {},
                    index:_view.$el.find('#complexity_area_date_'+e.target.getAttribute('name'))[0].children.length + 1,
                });

                _view.$el.find('#complexity_area_date_'+e.target.getAttribute('name')).append(html_tmp1);
                _view.SitesSelectizes['SelectArea_'+e.target.getAttribute('name') + '_'+ (
                    _view.$el.find('#complexity_area_date_'+e.target.getAttribute('name'))[0].children.length )] = _view.$el.find('#SelectArea_'+e.target.getAttribute('name') + '_'+
                  (_view.$el.find('#complexity_area_date_'+e.target.getAttribute('name'))[0].children.length )).selectize({
                    valueField: 'value',
                    labelField: 'display_name',
                    onInitialize: function(){
                        var _this = this;
                        $.ajax({
                            type: 'OPTIONS',
                            url: '/apiprojects/room_reservation/',
                            success: function(response){

                                for(var i in response.actions.POST.type_build.choices){
                                    _this.addOption(response.actions.POST.type_build.choices[i]);
                                }
                                var rooms = _view.model.get('rooms');
                                for(var i in rooms){
                                    if(rooms[i].site == e.target.getAttribute('name')){
                                        _this.setValue(rooms[i].type_build);
                                    }
                                }
                                _view.update_complexity = false;
                            }
                        });
                    },
                    onChange: function(value){
                        _view.update_complexity = true;
                    }
                });
            }
		},
		deleteSites:function(e, model){
		    var _view = this;
		    _view.$el.find('#site_'+e.target.getAttribute('name')).remove();
			_view.DisableData(_view.$el.find('.roomReservation').serializeArray())
			_view.DisableData(_view.$el.find('.roomReservationNotNec').serializeArray())
		},
		DisableData(arr){
		    var date = false
		    for(var i in arr){
		        if(arr[i].date_start || arr[i].date_end){
		            if(!isNaN(new Date(arr[i].date_start).getTime())){date = true}
		            if(!isNaN(new Date(arr[i].date_end).getTime())){date = true}
		        }else{
		            if(!isNaN(new Date(arr[i].value).getTime())){date = true}
		        }
			}
			if(date){
               this.$el.find('#datetimepicker1').data("DateTimePicker").disable()
               this.$el.find('#datetimepicker2').data("DateTimePicker").disable()
               this.$el.find('input[name="time_start"]').prop('disabled', true);
			   this.$el.find('input[name="time_end"]').prop('disabled', true);
			   this.$el.find('input[name="time_last"]').prop('disabled', true);
			   this.$el.find('input[name="event_date_start"]').prop('disabled', true);
			   this.$el.find('input[name="event_date_end"]').prop('disabled', true);
            }else{
               this.$el.find('#datetimepicker1').data("DateTimePicker").enable()
               this.$el.find('#datetimepicker2').data("DateTimePicker").enable()
               this.$el.find('input[name="time_start"]').prop('disabled', false);
			   this.$el.find('input[name="time_end"]').prop('disabled', false);
			   this.$el.find('input[name="time_last"]').prop('disabled', false);
			   this.$el.find('input[name="event_date_start"]').prop('disabled', false);
			   this.$el.find('input[name="event_date_end"]').prop('disabled', false);
            }
		},

		changeRoomReservationNotNec:function(e){
//		    TODO: сделать ограничения по вводу и блокировку полей
		    this.update_complexity = true;

			var dates_s = this.$el.find('.roomReservationArriv').serializeArray();
			var date_arr_s = [];

			for(var i in dates_s){
			    if(dates_s[i].value){
				    date_arr_s.push(new Date(dates_s[i].value));
				}
			}
			date_arr_s.sort(function(a,b){
				return a - b;
			});

			this.DisableData(this.$el.find('.roomReservationNotNec').serializeArray())
			if(date_arr_s.length){
                this.$el.find('#datetimepicker6').data("DateTimePicker").date(date_arr_s[0]);
			}


		},
		changeRoomReservation: function(e){

			this.update_complexity = true;
			var dates = this.$el.find('.roomReservation').serializeArray();
			var date_arr = [];
			for(var i in dates){
				date_arr.push(new Date(dates[i].value));
			}
			date_arr.sort(function(a,b){
				return a - b;
			});

			this.DisableData(dates)

			this.$el.find('#datetimepicker2').data("DateTimePicker").date(date_arr[date_arr.length-1]);
			this.$el.find('#datetimepicker7').data("DateTimePicker").date(date_arr[date_arr.length-1]);
			this.$el.find('#datetimepicker1').data("DateTimePicker").date(date_arr[0]);
			this.$el.find('input[name="time_start"]').val(date_arr[0].toLocaleTimeString());
			this.$el.find('input[name="time_end"]').val(date_arr[date_arr.length-1].toLocaleTimeString());
			this.$el.find('input[name="time_last"]').val(date_arr[date_arr.length-1].toLocaleTimeString());

		},
		save: function(){
			var _this = this;
			var form = this.$el.find('form');
			var data = new FormData(form[0]);
			var sites = data.get('sites').split(',');
			data.delete('sites');
			for(i in sites){
				data.append('sites', sites[i])
			};
			if(this.update_complexity){
				data.append('update_complexity', true);
			}
			data.set('date_start', this.$el.find('#datetimepicker1').data("DateTimePicker").date() ? this.$el.find('#datetimepicker1').data("DateTimePicker").date().format('YYYY-MM-DD'): '');
			data.set('date_end', this.$el.find('#datetimepicker2').data("DateTimePicker").date()? this.$el.find('#datetimepicker2').data("DateTimePicker").date().format('YYYY-MM-DD'):'');
			data.set('event_date_start', this.$el.find('#datetimepicker6').data("DateTimePicker").date() ?
			                             this.$el.find('#datetimepicker6').data("DateTimePicker").date().format('YYYY-MM-DD'):'');
			data.set('event_date_end',  this.$el.find('#datetimepicker7').data("DateTimePicker").date() ?
			                             this.$el.find('#datetimepicker7').data("DateTimePicker").date().format('YYYY-MM-DD'):'');
			data.set('time_start', this.$el.find('input[name="time_start"]').val());
			data.set('time_end', this.$el.find('input[name="time_end"]').val());
			data.set('time_last', this.$el.find('input[name="time_last"]').val());


            for(var i in this.$el.find('#SelectProjectType')[0].selectize.sifter.items){
                if(data.get('project_type') == this.$el.find('#SelectProjectType')[0].selectize.sifter.items[i].value && this.$el.find('#SelectProjectType')[0].selectize.sifter.items[i].addit == 'promo'){

                    for(var j in this.$el.find('#SelectSites')[0].selectize.sifter.items){
                        if(this.$el.find('#SelectSites')[0].selectize.sifter.items[j].name == 'Сторонняя площадка'){

                            data.delete('sites');
                            data.append('sites', this.$el.find('#SelectSites')[0].selectize.sifter.items[j].id)
                        }
                    }

                }
            }
			$.ajax({
				type: 'PATCH',
				url: '/apiprojects/projects/'+this.project_id+'/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					if(!_this.preSaveBool){
						utils.notySuccess().show();
						if(response.alert){
							utils.notyError('Нарушены сроки подготовки залов, проект не будет запущен в работу').show();
						}
					}
					_this.$el.find('#calendar_show').attr('href', '/#calendar/?date='+response.date_start);
					_this.update_complexity = false;
					_this.preSaveBool = false;
					_this.model.fetch({
						success: function(response){
							_this.changeThematic();
							if(response.get('project_type') && (response.get('project_type').type_project_type == 'expo' || response.get('project_type').type_project_type == 'ad')){
							mainView.getRegion('MainContent').currentView.$el.find('#ProjectReportsView').removeClass('disabled');
							}
							else{
								mainView.getRegion('MainContent').currentView.$el.find('#ProjectReportsView').addClass('disabled');	
							}
						}
					});
					_this.triggerMethod('closeModal', {id: response.id, name: response.name});
				},
				error: function(response){
					if(response.responseJSON){
						utils.notyError(response.responseJSON.alert).show();
					}
					else{
						utils.notyError().show();	
					}
				}
			})
		},
		initialize: function(){
			if(this.model){
				this.project_id = this.model.id;
			}
			this.name = 'project';
			this.count_sites = 0;
			this.SitesSelectizes = {};
			this.update_complexity = false;
			this.arr_w = [];
		},
		changeThematic: function(){
			if(this.model && this.model.get('subjects')){
				this.$el.find('#create_section').tooltip('destroy');
				this.$el.find('#create_section').attr('disabled', false);
			}
			else{
				this.$el.find('#create_section').tooltip({title: "Для добавлния сохраните проект с выбранной тематикой"});
				this.$el.find('#create_section').attr('disabled', true);
			}
		},
		PreSave: function(e){
			this.preSaveBool = false;
			var _this = this;
			$(e.currentTarget).prop('disabled', true);
			var form = new FormData();
			
			form.append('name', this.$el.find('input[name="name"]').val()) ;
			$.ajax({
				url: '/apiprojects/projects/',
				type: 'POST',
				contentType: false,
				processData: false,
				// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
				data: form,
				success: function(response){
					_this.project_id = response.id;
					_this.changeThematic();
					var model = Backbone.Model.extend({url:this.url + response.id});
					if(!_this.options.client){
						router.navigate('project/'+response.id+'/edit', {replace: true});
						window.mainView.getRegion('MainContent').currentView.enabledTabs();
						window.mainView.getRegion('MainContent').currentView.$el.find('#ProjectReportsView').addClass('disabled');
					}
					// замена потестить
					// _this.model = new model(response);
					_this.model = new model();
					_this.model.fetch();
					$(e.currentTarget).fadeOut(1);
					$(e.currentTarget).parent().parent().removeClass();
					_this.$el.find('#ProjectInfo').collapse("show");
					utils.notySuccess().show();
					mainView.getRegion('MainContent').currentView.model = _this.model;

				},
				error: function(response) {
					$(e.currentTarget).prop('disabled', false);
					utils.notyError().show();
					/* Act on the event */
				}

			});
			
		},
		questionSave: function(){
			var _this=this;
			var modal = new Backbone.BootstrapModal({
				content: 'Сохранить изменения', 
				title: 'Сохранить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				_view.save();
			});
		},
		removeRow: function(e){
			var _this=this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить раздел?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				var row = _this.Table.bootstrapTable('getRowByUniqueId', $(e.currentTarget).data().id);
				$('.loading').fadeIn();
				$.ajax({
					type: 'DELETE', 
					url: '/apiprojects/project_section/' + row.id,
					contentType: false,
					processData: false,
					success: function(response){
						_this.model.fetch({
							success: function(response){
								_this.Table.bootstrapTable('refresh');
								$('.loading').fadeOut();
								utils.notySuccess().show();
							}
						});
				}
			});
			});
		},
		// Переводит данные таблицы в объект для формы редактирования
		editRow: function(e){
			var TableData = {};
			var row = this.Table.bootstrapTable('getRowByUniqueId', $(e.currentTarget).data().id);
			row.subsection = [];
			var html_cell = $(row.html);
			row.name = html_cell.filter('.SectionName').text();
			row.subsection = html_cell.filter('.TableDiv').map(function(){
               return $.trim($(this).text());
            }).get();
			this.createSection(undefined, row);
		},
		// Вытаскивает число из любой строки
		getNum: function(str){
			return parseFloat(str.replace(/\.(?=.*\.)|[^\d\.-]/g, ''));
		},
		// Функция счтатет сумму в столбцах принимает массиы номеров столбцов, начиная с нуля 
		getSummRow(RowNum){
			var cont = 0;
			var SummRow = {};
			for (i in RowNum){
				SummRow[RowNum[i]] = {count:0};
			};
			var Table = this.$el.find('tr').toArray();
			delete Table[0];
			for (col in Table){
				if(Table[col].id != 'infoRow'){
					var col = $(Table[col]).children().toArray();
					for(row in col){
						if(RowNum.indexOf(parseInt(row))!=-1){
							SummRow[row].count += this.getNum(col[row].innerHTML);
						}
					}	
				}else{
					var LastCol = $(Table[col]).children().toArray();
					for(i in SummRow){
						// LastCol[i].id=='percent_conxept_all'
						
						if($(LastCol[i]).hasClass('percent')){
							$(LastCol[i]).replaceWith('<td id="percent_conxept_all" class="infoRow percent">' + Math.round(SummRow[i].count) + ' %</td>');
						}
						else{
							$(LastCol[i]).replaceWith('<td class="infoRow">' + SummRow[i].count + '</td>');	
						}
					}

				}
				
			}
		},
		//formdata из объекта
		getFormData(obj, exclude, arrayField){
			var data = new FormData();
			for(i in obj){
				if(exclude.indexOf(i)==-1 && arrayField.indexOf(i)==-1){
					data.append(i, obj[i]);
				}
				if(arrayField.indexOf(i)!=-1){
					obj[i] = obj[i].split(',');
					if(obj[i].length>0){
						for(j in obj[i]){
							data.append(i, obj[i][j]);
						};
					}
					else{
						if(data.has(i)){
							data.delete(i);
						}
						else{
							exclude.push(i);
						}
					}
				}
			};
			data.append('project', this.project_id);
			return data
		},

		createSection: function(e, Data){
			var _this=this;
			var edit = false;
			var okText = 'Добавить';
			var title = 'Добавить раздел';
			var type = 'POST';
			var html;
			var id = '';
			if(!e){
				id = Data.id;
				type = 'PATCH'
				title = 'Редактировать раздел'
				okText = 'Сохранить'
				edit = true;
			};
			if(this.preSaveBool){
				this.save();
				this.preSaveBool = false;
			};
			if(!Data){
				var Data = {};
			}
			// Data.percent = 100 - this.getNum(this.$el.find('#percent_conxept_all')[0].innerHTML);
			var count_rows = this.Table.bootstrapTable('getData').length;
			if(this.Table.bootstrapTable('getData').length == 0){
				Data.percent = 100;
			}
			else{
				Data.percent = 100 - this.Table.bootstrapTable('getData')[count_rows-1].percent_conxept;
			}
			require(['projects/js/create_section_view', 'workers/js/modals/modal_templates'], function(CreateSectionView, tmp){
				var modal = new Backbone.BootstrapModal({ 
					content: new CreateSectionView({
						htmlContext:Data,
						project_id:_this.project_id,
						sectionCollection: new Backbone.Collection(_this.Table.bootstrapTable('getData')),
						section_id: id
					}),
					title: title,
					template: tmp.full_screen,
					okText: okText,
					cancelText: 'Отмена',
					okCloses: false,
					escape: false,
					modalOptions: {
						backdrop: 'static',
						keyboard: false
					},
					animate: true, 
				}).open();
				modal.on('ok', function(){
					var _modal = this;
					if(this.options.content.validate()){
						var form = this.$content.find('form');
						form.find(':input:disabled').removeAttr('disabled');
						var html = _this.getHTMLRowTable(this.$content.find('form').serializeArray());
						var RowTable = _this.getRowTable(this.$content.find('form').serializeArray());
						this.close();
						$('.loading').fadeIn();

						var formData = _this.getFormData(RowTable, ['section'], ['activities']);
						formData.append('html', $(html).children()[0].innerHTML);
						$.ajax({
							url: id==''?'/apiprojects/project_section/':'/apiprojects/project_section/'+id+'/',
							contentType: false,
							processData: false,
							type: type,
							data: formData,
							success: function(response){
								_this.$el.find('#empty').attr('id', response.id);
								if(_modal.options.content.SelectActivities[0].selectize.ItemsRemoved.length>0){
									var data = new FormData();
									data.append('removed_id', _modal.options.content.SelectActivities[0].selectize.ItemsRemoved.toString());
									data.append('project_id', _this.project_id);
									$.ajax({
										url:'/apiworkers/managers/remove_related/',
										type:'POST', 
										contentType: false,
										processData: false,
										data: data,
										success: function(response){
											_this.model.fetch({
												success:function(){
													_this.Table.bootstrapTable('refresh', {url: '/apiprojects/project_section/?project='+_this.model.id});
													// _this.render();
													$('.loading').fadeOut();
													utils.notySuccess().show();
												}
											});
										},
										error: function(response){
											utils.notyError().show();
										}
									});
								}
								else{
									_this.model.fetch({
										success:function(){
											// _this.render();
											_this.Table.bootstrapTable('refresh', {url: '/apiprojects/project_section/?project='+_this.model.id});
											$('.loading').fadeOut();
											utils.notySuccess().show();
										},
										error: function(response){
											utils.notyError().show();
										}
									});
								}


							},
							error: function(response){
								// _modal.open();
								utils.notyError().show();
								$('.loading').fadeOut();
							}
						});
				};
			});
			});
		},
		getRowTable: function(data){
			var htmlContext = {};
			var section = [];
			for (i in data){
				if (data[i].name.split('.')[0]=='subsection'){
					section.push(data[i].value)
				}
				else{
					htmlContext[data[i].name]=data[i].value;
				}

			}
			htmlContext.section = section;
			return htmlContext;
		},
		guid: function(){
		  function s4() {
		    return Math.floor((1 + Math.random()) * 0x10000)
		      .toString(16)
		      .substring(1);
		  }
		  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		    s4() + '-' + s4() + s4() + s4();
		},
		getHTMLRowTable:function(data){
			var htmlContext = this.getRowTable(data);
			htmlContext.guid = this.guid();
			return Handlebars.compile($(html).filter('#newRow')[0].innerHTML)(htmlContext);
		},
		selectizeSites: function(){
			var _view = this;
			this.$el.find('#SelectSites').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/sites/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i])
							}
							if(_view.model){
								var sites = _view.model.get('sites');
								for(i in sites){
									_this.addItem(sites[i].id);
								}
								_view.update_complexity = false;
							}
							return response;
						}
					});
				},
				onItemAdd: function(value, $item){
					if(_view.$el.find('#SelectProjectType')[0].selectize.options[_view.$el.find('#SelectProjectType')[0].selectize.getValue()].room_reservation){
						if(this.options[value].congress || this.options[value].ametist || this.options[value].exhibition){
							_view.update_complexity = true;
							var room_reserv = _view.model.get('rooms');
							var reserv_options = [];
							for(var i in room_reserv){
								if(room_reserv[i].site == value){
									reserv_options.push(room_reserv[i]);
								}
							}
							var html_tmp = Handlebars.compile($(html).filter('#complexity_area')[0].innerHTML)({
								sites_option: this.options[value],
//								reserv_options: reserv_options
							});


							_view.$el.find('#complexity').append(html_tmp);


						}
					}
					_view.DisableData(_view.$el.find('.roomReservation').serializeArray())
					_view.DisableData(_view.$el.find('.roomReservationNotNec').serializeArray())

					    if(_view.$el.find('#complexity_area_date_'+this.options[value].id)[0]){
					        if(!reserv_options.length){

					            var html_tmp1 = Handlebars.compile($(html).filter('#complexity_area_date')[0].innerHTML)({
                                    sites_option: this.options[value],
                                    reserv_options: {},
                                    index:1,
                                });

                                _view.$el.find('#complexity_area_date_'+this.options[value].id).append(html_tmp1);
                                _view.SitesSelectizes['SelectArea_'+this.options[value].id + '_'+ 1] = _view.$el.find('#SelectArea_'+this.options[value].id + '_'+1).selectize({
                                    valueField: 'value',
                                    labelField: 'display_name',
                                    onInitialize: function(){
                                        var _this = this;
                                        $.ajax({
                                            type: 'OPTIONS',
                                            url: '/apiprojects/room_reservation/',
                                            success: function(response){
                                                for(var i in response.actions.POST.type_build.choices){
                                                    _this.addOption(response.actions.POST.type_build.choices[i]);
                                                }
                                                var rooms = _view.model.get('rooms');
                                                for(var i in rooms){
                                                    if(rooms[i].site == value){
                                                        _this.setValue(rooms[i].type_build);

                                                    }
                                                }
                                                _view.update_complexity = false;
                                            }
                                        });
                                    },
                                    onChange: function(value){
                                        _view.update_complexity = true;
                                    }
                                });
					        }

					        for(var i in reserv_options){
                                var html_tmp1 = Handlebars.compile($(html).filter('#complexity_area_date')[0].innerHTML)({
                                    sites_option: this.options[value],
                                    reserv_options: reserv_options[i],
                                    index:parseInt(i)+1,
                                });

                                _view.$el.find('#complexity_area_date_'+this.options[value].id).append(html_tmp1);
                                _view.SitesSelectizes['SelectArea_'+reserv_options[i].site + '_'+ (parseInt(i)+1)] = _view.$el.find('#SelectArea_'+reserv_options[i].site + '_'+ (parseInt(i)+1)).selectize({
                                    valueField: 'value',
                                    labelField: 'display_name',
                                    onInitialize: function(){
                                        var _this = this;
                                        $.ajax({
                                            type: 'OPTIONS',
                                            url: '/apiprojects/room_reservation/',
                                            success: function(response){
                                                for(var i in response.actions.POST.type_build.choices){
                                                    _this.addOption(response.actions.POST.type_build.choices[i]);
                                                }
                                                var rooms = _view.model.get('rooms');
                                                for(var i in rooms){
                                                    if(rooms[i].site == value){
                                                        _this.setValue(rooms[i].type_build);

                                                    }
                                                }
                                                _view.update_complexity = false;
                                            }
                                        });
                                    },
                                    onChange: function(value){
                                        _view.update_complexity = true;
                                    }
                                });
							}

						}
				},
				onItemRemove: function(value){
					delete _view.SitesSelectizes['SelectArea_'+value];
					_view.$el.find('#site_'+value).remove();
					_view.DisableData(_view.$el.find('.roomReservation').serializeArray())
					_view.DisableData(_view.$el.find('.roomReservationNotNec').serializeArray())
				},
				render:{
					option: function(item, escape){
						return Handlebars.compile($(html).filter('#selectizeTemplate')[0].innerHTML)(item);
					}
				}
			});
		},
		resizeTable: function(){
			// var arr_w = [];
			var _view = this;
			if(this.arr_w.length == 0){
				var arr = this.$el.find('tbody tr[data-index="0"]').find('td').toArray();
				for(var i in arr){
					this.arr_w.push(arr[i].offsetWidth);
				}
			}
			$(this.$el.find('thead')[0]).find('th').each(function(e){$(this).find('.fht-cell').width(_view.arr_w[e])});
			$(this.$el.find('thead')[1]).find('th').each(function(e){$(this).find('.fht-cell').width(_view.arr_w[e])});
		},
		onRender: function(){
			$('.loading').fadeOut();
			var _this = this;
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
				
			});
			this.changeThematic();
			_view.$el.on("hide.bs.collapse", '.listCollapse',function(){
				$(this).parent().find('span').removeClass();
				$(this).parent().find('span').addClass('glyphicon glyphicon-chevron-right menuSpan');
			});
			_view.$el.on("show.bs.collapse",'.listCollapse',  function(){
				$(this).parent().find('span').removeClass();
				$(this).parent().find('span').addClass('glyphicon glyphicon-chevron-down menuSpan');
			});
			this.$el.find('#datetimepicker1').datetimepicker({
				locale: 'ru',
				format: 'DD.MM.YYYY'
			});
			this.$el.find('#datetimepicker2').datetimepicker({
				useCurrent: false,
				locale: 'ru',
				format: 'DD.MM.YYYY'
			});
			this.$el.find('#datetimepicker3').datetimepicker({
                format: 'LT',
                locale: 'ru',
            });
			this.$el.find('#datetimepicker4').datetimepicker({
                format: 'LT',
                locale: 'ru',
            });
            this.$el.find('#datetimepicker5').datetimepicker({
                format: 'LT',
                locale: 'ru',
            });

            this.$el.find('#datetimepicker6').datetimepicker({
				locale: 'ru',
				format: 'DD.MM.YYYY'
			});
            this.$el.find('#datetimepicker7').datetimepicker({
				locale: 'ru',
				format: 'DD.MM.YYYY',
			});

			this.$el.find("#datetimepicker1").on("dp.change", function (e) {
				_view.$el.find('#datetimepicker2').data("DateTimePicker").minDate(e.date);
				var newDate = new Date(e.date);
                newDate.setDate(newDate.getDate() - 14);
                _view.$el.find('#datetimepicker6').data("DateTimePicker").minDate(newDate);
                _view.$el.find('#datetimepicker7').data("DateTimePicker").minDate(newDate);
			});
			this.$el.find("#datetimepicker2").on("dp.change", function (e) {
				_view.$el.find('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
				var newDate = new Date(e.date);
                newDate.setDate(newDate.getDate() + 14);
                _view.$el.find('#datetimepicker6').data("DateTimePicker").maxDate(newDate);
                _view.$el.find('#datetimepicker7').data("DateTimePicker").maxDate(newDate);
			});

			if(this.model){


//			    this.$el.find('#plaseEvent').collapse("show");
//			    this.$el.find('#complexityBuildingEvent').collapse("show");
//				this.$el.find('#BuildingEvent').collapse("show");
//				this.$el.find('#timeStartEvent').collapse("show");
//				this.$el.find('#timeEndEvent').collapse("show");
//				this.$el.find('#timeEndLastEvent').collapse("show");
//				this.$el.find('#DateProgramStartEvent').collapse("show");
//				this.$el.find('#DateProgramEndEvent').collapse("show");

				this.getSummRow([1,2,3,4,5]);
				if(this.model.get)
				this.$el.find('#datetimepicker1').data("DateTimePicker").date(this.model.get('date_start')?new Date(this.model.get('date_start')):new Date());
				this.$el.find('#datetimepicker2').data("DateTimePicker").date(this.model.get('date_end')?new Date(this.model.get('date_end')):new Date());

				if(this.model.get('event_date_start')){
				    this.$el.find('#datetimepicker6').data("DateTimePicker").date(this.model.get('event_date_start')?new Date(this.model.get('event_date_start')):new Date())
				}else if(this.model.get('date_start')){
				    this.$el.find('#datetimepicker6').data("DateTimePicker").date(this.model.get('date_start')? new Date(this.model.get('date_start')):new Date())
				}else{
				     this.$el.find('#datetimepicker6').data("DateTimePicker").date(new Date())
				}

				if(this.model.get('event_date_end')){
				    this.$el.find('#datetimepicker7').data("DateTimePicker").date(this.model.get('event_date_end') ? new Date(this.model.get('event_date_end')):new Date())
				}else if(this.model.get('date_end')){
				    this.$el.find('#datetimepicker7').data("DateTimePicker").date(this.model.get('date_end')? new Date(this.model.get('date_end')):new Date())
				}else{
				     this.$el.find('#datetimepicker7').data("DateTimePicker").date(new Date())
				}

				if (this.model.get('project_type') && this.model.get('project_type').type_project_type == 'kvs'){
                    this.$el.find('#datetimepicker6_label').html('Дата начала мероприятия')
                    this.$el.find('#datetimepicker7_label').html('Дата окончания мероприятия')
                }
			};


			this.$el.find('#SelectSubject').selectize({
				valueField: 'id',
				labelField: 'subject_name',
				searchField: 'subject_name',
				class:'SelectizeUser',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiprojects/subjects/',
						success: function(response){
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i]);
							};
							if(_view.model){
								_this.setValue(_view.model.get('subjects'));
							}
						}
					})
				},
			});


			this.SelectizeBuisnessUnit = this.$el.find('#SelectBuisnessUnit').selectize({
				onInitialize: function(){
					var _this=this;
					$.get({
						url: '/apiprojects/business_units/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption({
									value:response[i].id,
									text:response[i].name})
							}
							_this.setValue(_this.def_b_u);
						}
					});
				},
			});

			this.SelectUser = this.$el.find('#SelectUser').selectize({
				valueField: 'id',
				labelField: 'last_name',
				searchField: 'last_name',
				onInitialize: function(){
					var _this=this;
					$.get({
						url: '/apiworkers/users/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i]);
							}
							if(_view.model){
								_this.setValue(_view.model.get('user_id'));
								return;
							}
							_this.setValue(window.user.id);
						}
					});
				},
			});

			if(!window.user.isSuperuser && ['kvs_head', 'head_ad', 'head_expo', 'head_direction'].filter(element => window.user.groups.includes(element)).length==0){
			    var selectize = this.SelectUser[0].selectize;
                selectize.disable();
			}

			var filter_project_type = '';
			if(user.groups.indexOf('project_managers') != -1){
				filter_project_type = 'expo';
			}
			if(user.groups.indexOf('kvs') != -1){
				filter_project_type = 'kvs';
			}
			if(user.groups.indexOf('head_ad') != -1){
				filter_project_type = 'ad';
			}
			this.$el.find('#SelectProjectType').selectize({
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/projects_types/?type_project_type=' + filter_project_type,
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption({
									value:response[i].id,
									text:response[i].type,
									def_b_u:response[i].default.id,
									type_project_type:response[i].type_project_type,
									room_reservation: response[i].room_reservation,
									addit:response[i].addit_parameter,
								});
							}
							if(_view.model && _view.model.get('project_type')){
								_this.setValue(_view.model.get('project_type').id);
							}
							_view.selectizeSites();
							return response;
						}
					});
				},
				options:[],
				load: function(input) {
				},
				onChange: function(value){
				    if (this.options[value].type_project_type == 'kvs' && ['promo', 'booking'].indexOf(this.options[value].addit)>=0){
				        if(this.options[value].addit == 'promo'){
						    //если заявка на рекламу
						    _this.$el.find('#numberGuestsEvent').collapse("hide");
						}else{
						    _this.$el.find('#numberGuestsEvent').collapse("show");
						}

                        _this.$el.find('#formatEventCollapse').collapse("hide");
                        _this.$el.find('#plaseEvent').collapse("hide");
                        _this.$el.find('#complexityBuildingEvent').collapse("hide");
                        _this.$el.find('#BuildingEvent').collapse("hide");

                        _this.$el.find('#timeStartEvent').collapse("hide");
                        _this.$el.find('#timeEndEvent').collapse("hide");
                        _this.$el.find('#timeEndLastEvent').collapse("hide");
                        _this.$el.find('#DateProgramStartEvent').collapse("hide");
                        _this.$el.find('#DateProgramEndEvent').collapse("hide");

				    }else{
				        _this.$el.find('#plaseEvent').collapse("show");
                        _this.$el.find('#complexityBuildingEvent').collapse("show");
                        _this.$el.find('#BuildingEvent').collapse("show");
                        _this.$el.find('#timeStartEvent').collapse("show");
                        _this.$el.find('#timeEndEvent').collapse("show");
                        _this.$el.find('#timeEndLastEvent').collapse("show");
                        _this.$el.find('#DateProgramStartEvent').collapse("show");
                        _this.$el.find('#DateProgramEndEvent').collapse("show");
                        _this.$el.find('#numberGuestsEvent').collapse("show");
				    }



					_this.SelectizeBuisnessUnit[0].selectize.def_b_u = this.options[value].def_b_u;
					_this.SelectizeBuisnessUnit[0].selectize.setValue(this.options[value].def_b_u);
					if (this.options[value].type_project_type == 'expo'){
						_this.$el.find('#ThematicProject').collapse("show");
						_this.$el.find('#clientCollapse').collapse("hide");
						_this.$el.find('#formatEventCollapse').collapse("hide");
					}
					if (this.options[value].type_project_type == 'kvs'){
                        _this.$el.find('#datetimepicker6_label').html('Дата начала мероприятия')
                        _this.$el.find('#datetimepicker7_label').html('Дата окончания мероприятия')
                        _this.$el.find('#clientCollapse').collapse("show");
						_this.$el.find('#formatEventCollapse').collapse("show");



                    }
					else{
						_this.$el.find('#ThematicProject').collapse("hide");
						switch (this.options[value].type_project_type) {
							case 'expo':
								_this.$el.find('#clientCollapse').collapse("hide");
								_this.$el.find('#formatEventCollapse').collapse("hide");
								break;
							case 'ad':
								_this.$el.find('#clientCollapse').collapse("hide");
								_this.$el.find('#formatEventCollapse').collapse("show");
								break;
							case 'kvs':
								_this.$el.find('#clientCollapse').collapse("show");
								_this.$el.find('#formatEventCollapse').collapse("show");
								break;
						}
					}
				}

			});
			this.$el.find('#SelectClient').selectize({
				labelField: 'name',
				valueField: 'id',
				searchField: 'name',
				loadingClass: '',
				onInitialize: function(){
					if(_view.options.client){
						this.addOption(_view.options.client);
						this.setValue(_view.options.client.id);
					}
					if(_view.model && _view.model.get('client')){
						this.addOption(_view.model.get('client'));
						this.setValue(_view.model.get('client').id);
					}
				},
				load: function(input){
					var _this = this;
					if(input!=''){
						this.clearOptions();
						clearTimeout(this.timeout_id);
						this.timeout_id = setTimeout(function(){
							$.get({
								url: '/apiclients/clients/?offset=0&limit=15&search='+input,
								success: function(response){
									for(i in response.results){
										_this.addOption(response.results[i]);
									}
									_this.focus();
								}
							});		
						}, 100);
					}
				},
				onChange: function(value){
					_view.$el.find('#href_client').attr('href', '/#client/'+value);
				},
				render:{
					option: function(item, escape){	
						return Handlebars.compile($(html).filter('#selectizeClientTemplate')[0].innerHTML)(item);
					}
				}
			});
			this.$el.find('#SelectStatus').selectize({
				options:[
				{text:'Планирование', value: 'planning'},
				{text:'В работе', value: 'working'},
				{text:'Завершен', value:'finished'},
				{text:'Отменен', value: 'canceled'}],
				items:['planning'],
				onInitialize: function(){
					if(_view.model)
						this.setValue(_view.model.get('status'));
				}
			});
			// this.$el.find('#SelectExhibitionType').selectize({
			// 	options: [
			// 		{text: 'ДВП1', value: 'dvp1'},
			// 		{text: 'ДВП2', value: 'dvp2'}
			// 	],
			// 	onInitialize: function(){
			// 		if(_view.model){
			// 			this.setValue(_view.model.get('exhibition_type'));
			// 		}
			// 	}
			// });
			this.$el.find('#SelectFormatEvent').selectize({
				options: [
					{text: 'Выставка', value: 'exhibition'},
					{text: 'Форум', value: 'forum'},
					{text: 'Корпоратив', value: 'corporate'},
					{text: 'Конкурс/чемпионат', value: 'competition'},
					{text: 'Тренинг/семинар', value: 'training'},
					{text: 'Банкет', value: 'banquet'},
					{text: 'Фуршет', value: 'reception'},
					{text: 'Кофе-брейк', value: 'coffee_break'},
					{text: 'Обед-ужин', value: 'supper'},
					{text: 'Кейтеринг', value: 'catering'},
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('format_event'));
					}
				}
			});
			this.$el.find('#SelectArea').selectize({
				options: [
					{text: 'Периметр', value: '1'},
					{text: 'Свободная застройка (банкет, семинар)', value: '2'},
					{text: 'Стандарт', value: '3'},
					{text: 'Стандарт + доп.оборудование', value: '4'},
					{text: 'Сложная застройка (форум, чемпионат)', value: '5'},
					{text: 'Стандарт + индивидуал', value: '6'},
					{text: 'Интерактив + мультимедия', value: '7'},
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('area_size'));
					}
				}
			});

			this.TableCatering = this.$el.find('#table_catering').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// showColumns: true,
				dataField: 'results',
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				toolbar: $(html).filter('#toolbar_catering')[0].innerHTML,
				url: this.model?'/apiprojects/catering/?project='+this.model.id:'',
				columns: [
					{
						field: 'catering_type_display',
						title: 'Тип',
					},
					{
						field: 'time_start',
						title: 'Начало',
					},
					{
						field: 'time_end',
						title: 'Конец',
					},
					{
						field: 'edit',
						title: '',
						width: '30px',
						formatter: function(value, row, index){
							var last_index = _view.Table.bootstrapTable('getData').length - 1;
							if(index == last_index){
								return '';
							}
							return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil edit_catering" \
							data-id='+row.id+'></span><span class="glyphicon glyphicon-trash trash delete_catering" data-id='+row.id+'></span></div>';
						},
						align: 'center',
						valign: 'middle'
					}
				]
			});
			
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				// pagination: true,
				// search: true,
				// totalField: 'count',
				showColumns: true,
				// sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				height: 700,
				pageList: [10,25,50,100,250],
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				// toolbar: $(html).filter('#toolbar')[0].innerHTML,
				url: this.model?'/apiprojects/project_section/?project='+this.model.id:'',
				columns: [
					{
						field: 'html',
						title: 'Раздел/подраздел',
						// align: 'center',
						valign: 'middle'
						// escape: 
					},
					{
						field: 'all_activities',
						title: 'Виды деятельности',
						align: 'center',
						valign: 'middle',
						
						// escape: 
					},
					{
						field: 'percent_conxept',
						title: '% компаний по концепции(план)',
						align: 'center',
						valign: 'middle',
						formatter: function(value, row, index){
							return value + ' %'
						}
					},
					{
						field: 'count_comp_plan',
						title: 'Количество компаний по плану',
						align: 'center',
						valign: 'middle'
					},
					{
						field: 'count_clients',
						title: 'Количество компаний в разделах',
						align: 'center',
						valign: 'middle',
						cellStyle: function(value, row, index){
							if(row.change){
								return {
									css:{
										cursor: 'pointer'
									},
									classes: 'warning'
								}	
							}
							return {css:{
								cursor: 'pointer'
							}}
						}
					},
					{
						field: 'count_managers',
						title: 'Количество компаний, назначенных на менеджеров',
						align: 'center',
						valign: 'middle'
					},
					{
						field: 'percent',
						title: '% компаний по базе Клиентов(факт)',
						align: 'center',
						valign: 'middle'
						// formatter: function(value, row, index, field){
						// 	debugger;
						// 	return 1
						// },
					},
					{
						field: 'all_apps',
						title: 'Количество заявок',
						align: 'center',
						valign: 'middle'
					},
					{
						field: 'edit',
						title: '',
						formatter: function(value, row, index){
							var last_index = _view.Table.bootstrapTable('getData').length - 1;
							if(index == last_index){
								return '';
							}
							return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil edit" \
							data-id='+row.id+'></span><span class="glyphicon glyphicon-trash trash delete" data-id='+row.id+'></span></div>';
						},
						align: 'center',
						valign: 'middle'
					}				
				],
				onClickRow: function(row, e, index){

				},
				responseHandler: function(response){

					return response
				},
				queryParams: function(params){
					params.project_id = _view.options.project_id;
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return params
					}
					return params
				},
				onLoadSuccess: function(data){
					setTimeout(function(){
						_view.resizeTable();
					}, 1);
				},
				// rowStyle: function(row, index) {
				// 	if(row.change){
				// 		return {
				// 			css:{
				// 				cursor: 'pointer'
				// 			},
				// 			classes: 'warning'
				// 		}	
				// 	}
				// 	return {css:{
				// 		cursor: 'pointer'
				// 	}}
				// },
				onPostBody: function () {

				}
			});
		},
		afterRender: function() {
           debugger
//           this.$el.find('#SelectUser').data("DateTimePicker").disable()
           this.$el.find('#SelectUser').prop('disabled', true);
        },
		openCollapse: function(e){
			this.$el.find('selector')
		},
		template:$(html).filter('#FormProjectViewTest')[0].outerHTML,
		className: function(){
			if(this.options.client){
				return ''
			}
			return 'panel panel-default panelTabs displayNone'
		},
		templateContext: function() {
			if(this.options.client){
				return{show: false, isSuperuser: window.user.isSuperuser};
			}
			return {show: true, isSuperuser: window.user.isSuperuser};
		},
		
	});
			return ProjectFormView

		})

