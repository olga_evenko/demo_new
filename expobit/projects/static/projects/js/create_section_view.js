define(['text!projects/html/main_view.html', 'utils', 
	'bootstrap-table-editable', 'bootstrap-table-locale', 'spinner'], function(html, utils){
	var CreateSectionView = Backbone.Marionette.View.extend({
		initialize: function(){
			this.subsectionCount = 0;
			this.group_filter = '';
		},
		events:{
			'click #addSubSection': 'addSubSection',
			'click .glyphicon-trash': 'destroyInput',
			'click .filter_btn': 'setFilter',
			'click .filter_group_btn': 'setGroupFilter',
			'click .reset_btn': 'resetFilter',
			'click #not_call': 'setNotCallFilter'
		},
		setNotCallFilter: function(e){
			this.$el.find('#not_call').removeClass('btn-default').addClass('btn-warning');
			if(!this.set_not_call_bool){
				this.url_table = this.url_table + '&not_call=true';
				this.set_not_call_bool = true;
				this.Table.bootstrapTable('refresh', {url: this.url_table});
				return;
			}
		},
		setFilter: function(e){
			var filter_url = $(e.currentTarget).data().filter;
			this.Table.bootstrapTable('refresh', {url: this.url_table+filter_url});
		},
		setGroupFilter: function(e){
			var filter_url = $(e.currentTarget).data().filter;
			var subjects = mainView.getRegion('MainContent').currentView.model.get('subjects');
			if(!subjects){
				utils.notyAlert('Не сохранена тематика проекта в сервере, фильтрация невозможна');
				return;
			}
			this.group_filter = filter_url;
			this.Table.bootstrapTable('refresh', {
				url: this.url_table+filter_url+'&client_group__subjects='+subjects+'&membership__manager='+this.SelectManager[0].selectize.getValue()
			});
		},
		resetFilter: function(e){
			this.url_table = this.current_url;
			this.set_not_call_bool = false;
			this.Table.bootstrapTable('refresh', {url: this.url_table});
			this.$el.find('#not_call').removeClass('btn-warning').addClass('btn-default');
			this.group_filter = ';'
			this.SelectManager[0].selectize.clear();
		},
		destroyInput: function(e){
			$(e.currentTarget).parent().remove();
			this.subsectionCount-=1;
		},
		templateContext: function(){
			if(this.options.htmlContext){
				// this.options.htmlContext.edit = this.options.edit;
				return this.options.htmlContext
			}
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		addSubSection: function(){
			this.subsectionCount += 1;
			this.$el.find('#subsection').append('<div class="DivSubSection"><input type="text" class="form-control subsec input-sm" data-validate="True" name="subsection.'
				+this.subsectionCount.toString()
				+'"><span class="glyphicon glyphicon-trash trash"></span></div>');
			this.$el.find('input[name="subsection.'+this.subsectionCount.toString()+'"]').focus();
		},
		onRender: function(){
			var _view = this;
			var max_value = this.options.htmlContext.percent;
			if(this.options.htmlContext.percent_conxept)
				max_value = parseInt(max_value)+parseInt(this.options.htmlContext.percent_conxept);	
			this.$el.find('#spinner').spinner({ min: 1, max: max_value, step: 1, precision:0 });
			this.$el.find('input[name="percent_conxept"]').click(function(event) {
				this.select();
			});
			this.SelectActivities = this.$el.find('#SelectActivities').selectize({
				valueField: 'id',
				labelField: 'subcategories',
				searchField: ['subcategories', 'value'],
				optgroupField: 'category',
				optgroupLabelField: 'display_name',
				onInitialize: function(){
					var _this=this;
					this.ItemsRemoved = [];
					this.isLoad = false;
					$.ajax({
						url: '/apiprojects/activities/',
						type: 'OPTIONS',
						cache: true,
						success: function(response){
							var optGr = response.actions.POST.category.choices;
							for (i in optGr){
								_this.addOptionGroup(optGr[i].value, optGr[i]);
							}
							$.get({
								url: '/apiprojects/activities/?project='+_view.options.project_id+'&selected='+_view.options.htmlContext.activities,
								async: true,
								success: function(response) {
									for(var i=0; i<response.length; i++){
										_this.addOption(response[i]);
									}
									if(_view.options.htmlContext.activities){
										_this.addItems(typeof(_view.options.htmlContext.activities)=='string'?_view.options.htmlContext.activities.split(','):_view.options.htmlContext.activities);
										_view.getClientCount(_this.getValue());
										_view.getTableData(_this.getValue());
										_view.getManagerCount(_this.getValue());
									}
									_this.isLoad = true;
									return response;
								}
							});
						}
					})
				},
				onItemRemove: function(value){
					this.ItemsRemoved.push(value);
				},
				onItemAdd(value, $item){
					if(this.isLoad){
						if(this.ItemsRemoved.indexOf(value)!=-1){
							this.ItemsRemoved.splice(this.ItemsRemoved.indexOf(value), 1);
						}
					}
				},
				onChange: function(value){
					var h = _view.$el.find('.selectize-input.items.not-full.has-options.has-items')[0];
					_view.$el.find('.selectize-control.form-control.multi').css('height', h.offsetHeight+10);
				},
				onBlur: function(){
					// _view.getClientCount(this.getValue());
					_view.getTableData(this.getValue());
					_view.getManagerCount(this.getValue());
				},
			});
			
			// Таблица с данными 
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				height: 500,
				pageList: [10,25,50,100,250],
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				checkedRow:{},
				toolbar: $(html).filter('#toolbar_section')[0].innerHTML,
				onLoadSuccess: function(data){
					_view.SelectManager = _view.$el.find('#SelectManager').selectize({
						onInitialize: function(){
							if($(document).data()["/apiworkers/managers/forselect"]){
								var managers = 	$(document).data()["/apiworkers/managers/forselect"].sourceData;
								for(var i in managers){
									this.addOption(managers[i]);
								}
							}
						},
						onChange: function(value){
							_view.Table.bootstrapTable('refresh', {
								url: _view.url_table + '&membership__manager=' + value + _view.group_filter
							});
						}
					});
				},
				// url: '/apiclients/clients/?id='+_view.options.htmlContext.activities,
				columns: [
				{
					checkbox: true
				},	
				{
					field: 'name',
					title: 'Клиент',
					sortable: true,
					cellStyle: function(value, row, index, field) {
					  return {
					    // classes: 'text-nowrap another-class',
					    css: {"cursor": "pointer"}
					  };
					}
				}, 
				{
					field: 'group',
					title: 'Группа',
					sortable: true,
					editable: {
						source: [{text: 'A', value: 'a'},{text: 'B', value: 'b'},
						{text: 'C', value: 'c'},{text: 'VIP', value: 'vip'}],
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
							// contentType: false,
						    // processData: false,
						    beforeSend: function(jqXHR,settings){
						    	// settings.url += 'replace/';
								// debugger;
							}
						},
						// sourceOptions: {
						// 	type: 'get',
						// 	dataType: 'json'
						// },
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						// prepend: '',
						emptytext: 'Назначить',
						url: '/apiclients/clients_group/set_group_from_section/',
						params: function(params){
//							 debugger;
							params.project = _view.options.project_id;
							// params.many = false;
							var values_data = [];
							var check_row = _view.Table.bootstrapTable('getSelections');
							_view.check_row = check_row;
							var tableRow = _view.Table.bootstrapTable('getRowByUniqueId', params.pk);
//							 debugger;
							if(check_row.length>0 && user.isSuperuser){
								for(i in check_row){
									values_data.push(check_row[i].id);
								};
								return {values:values_data, project: _view.options.project_id, group:params.value, many: true}
								};
							return params
						},
						success: function(response){
							if(response.update){
								_view.Table.bootstrapTable('refresh', {silent: true});
							};
							if(_view.check_row){
								_view.Table.bootstrapTable('checkBy', _view.check_row);
								_view.check_row = null;
							};
							// if(response.update)
							// debugger;
							// _view.Table.bootstrapTable('refresh', {silent: false});
						}
					}
				}, 
				{
					field: 'manager',
					title: 'Менеджер',
					editable: {
						source: '/apiworkers/managers/forselect',
						ajaxOptions: {
							type: 'PUT',
							dataType: 'json', 
							// contentType: false,
						    // processData: false,
						    beforeSend: function(jqXHR,settings){
						    	settings.url += 'replace/';
								// debugger;
							},
						},
						sourceOptions: {
							type: 'get',
							dataType: 'json',
							complete: function(response, event){
								var managers = response.responseJSON;
								for(var i in managers){
									_view.SelectManager[0].selectize.addOption(managers[i]);
								}
							}
						},
						inputclass: 'input-sm',
						type: 'select',
						mode: 'inline',
						prepend: '',
						emptytext: 'Назначить',
						url: '/apiworkers/managers/',
						params: function(params){
							var obj = {};
							var arr_manager = [];
							var arr_client = [];
							var check_row = _view.$el.find('#table').bootstrapTable('getSelections');
							_view.check_row = check_row;
							var tableRow = _view.$el.find('#table').bootstrapTable('getRowByUniqueId', params.pk);
							if(check_row.length>0){
								for(i in check_row){
									if(typeof(check_row[i].manager)=='object'){
										check_row[i].manager='';
									}
									arr_manager.push(check_row[i].manager);
									arr_client.push(check_row[i].id)
								}
								return {old_manager:arr_manager,
									new_manager:params.value, client: arr_client, project: _view.options.project_id}
								}
								if(tableRow.manager!=params.value){
									if(typeof(tableRow.manager)=='object'){
										tableRow.manager='';
									}
									return {old_manager:tableRow.manager,
										new_manager:params.value, client: params.pk, project: _view.options.project_id}
									};
									return params
								},
								success: function(response){
									if(response.update){
										_view.$el.find('#table').bootstrapTable('refresh', {silent: true});
									};
									if(_view.check_row){
										_view.Table.bootstrapTable('checkBy', _view.check_row);
										_view.check_row = null;
									};
									_view.getManagerCount(_view.SelectActivities[0].selectize.getValue());
								}
							}
						},
						{
							field: 'change_activities',
							title: 'Дата изменения ВД',
							sortable: true,
						},
						{
							field: 'address',
							title: 'Адрес'
						},
						{
							field: 'date_last_contact',
							title: 'Дата последнего контакта',
							sortable: true,
						}, 
						// {
						// 	field: 'date_created',
						// 	title: 'Дата добавления',
						// 	sortable: true,
						// }
						],
						rowAttributes: function(row, index){
							if(this.checkedRow[row.id]){
								row[0] = this.checkedRow[row.id];
							};
						},
						onUncheckAll: function(rows){
							this.checkedRow = {};
						},
						onCheckAll: function(rows){
							for(var i in rows){
								this.checkedRow[rows[i].id] = true;
							}
						},
						onClickRow: function(row, e, index){
							if(index == 'name'){
								var model = new Backbone.Model();
									model.url = '/apiclients/clients/'+row.id+'/';
									model.fetch({
										success: function(response){
											require(['clients/js/one_client_view', 'projects/js/modal_template'], function(View, tmp){
												var view = new View({model:model});
												view.$el.find('.displayNone').css('display', 'block');
												var modal = new Backbone.BootstrapModal({
													content: view, 
													title: 'Клиент',
													template: tmp,
													okText: 'Закрыть',
													cancelText: '',
													escape: false,
													modalOptions: {
														backdrop: 'static',
														keyboard: false
													},
													animate: true, 
												}).open();
												mainView.modalView = modal;
												modal.on('ok', function(){
													mainView.modalView = undefined;
													$('.modal.fade.in').css('overflow-y', 'auto');
												});
												modal.on('cancel', function(){
													mainView.modalView = undefined;
													$('.modal.fade.in').css('overflow-y', 'auto');
												});
											});
										}
								});	
							}
						},
						onCheck: function(row, $element){
							this.checkedRow[row.id] = true;
						},
						onUncheck: function(row, $element){
							delete this.checkedRow[row.id];
						},
						responseHandler: function(response){
							_view.$el.find('.LoadClientCountInActivities').fadeOut(500);
							_view.$el.find('input[name="count_clients"]').val(response.count);
							_view.showClientData();
							var results = response.results;
							var text = '';
							for (i in results){
								if(results[i].manager.length!=0){
									results[i].manager = results[i].manager[0].manager 
								};
							};
							response.results = results;
							return response
						},
						queryParams: function(params){
							params.project_id = _view.options.project_id;
							if(params.order == 'desc'){
								params.sort = '-' + params.sort;
								return params
							}
							return params
						},
						rowStyle: function(row, index) {
							if(_view.options.section_id!='' && row.change_activities!=null){
								var change_activities = row.change_activities.match(/\d{2,4}/g);
								var time_change = _view.options.sectionCollection.get(_view.options.section_id).get('time_change').match(/\d{2,4}/g);
								change_activities = new Date(change_activities[1]+'-'+change_activities[0]+'-'+change_activities[2]+' '+change_activities[3]+':'+change_activities[4]);
								time_change = new Date(time_change[1]+'-'+time_change[0]+'-'+time_change[2]+' '+time_change[3]+':'+time_change[4]);
								if(change_activities>time_change){
									return {
										classes: 'danger',
									};
								}
							}
							return ''
						},
				// 		onColumnSwitch: function () {
				// 	// $('#table').editableTableWidget({editor: $('<textarea>')});
				// }
			});
			_view.$el.on('DOMSubtreeModified','.editable-empty',function(e){
				_view.Table.bootstrapTable('resetWidth');
			});
			// this.$el.find('#table').
		},
		getManagerCount: function(id){
			var _this = this; 
			var data = {project: this.options.project_id};
			if(id != ''){
				data = {project: this.options.project_id, client_id:id};
			}
			$.ajax({
				url: '/apiworkers/managers/count/',
				type: 'GET',
				data: data,
				success: function(response){
					_this.$el.find('input[name="count_managers"]').val(response.count);
				}

			});
		},
		getTableData: function(id){
			this.url_table = '/apiclients/clients/?id='+id;
			this.current_url = '/apiclients/clients/?id='+id;
			this.Table.bootstrapTable('refresh', {url:'/apiclients/clients/?id='+id})
		},
		getClientCount: function(id){
			var _this = this;
			$.ajax({
				url: '/apiclients/clients/count_activities/',
				type: 'GET',
				data: {id_act: id},
			})
			.done(function(response) {
				_this.$el.find('.LoadClientCountInActivities').fadeOut(500);
				_this.$el.find('input[name="count_clients"]').val(response.count);
				_this.showClientData();
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			

		},
		showClientData: function(){
			this.$el.find('#ClientData').collapse("show");
		},
		template:$(html).filter('#addSection')[0].outerHTML,
		// className: 'panel panel-default',
		// templateContext: function() {
		// 	return {
		// 		statusView: this.statusView[this.model.get('status')]
		// 	}
		// },

	});
return CreateSectionView

})