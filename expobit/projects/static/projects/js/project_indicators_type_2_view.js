define(['text!projects/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var ProjectIndicatorsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_indicators_view')[0].outerHTML,
		events: {
		},
		regions: {
		},
		className: 'panel panel-default panelTabs displayNone form',
		initialize: function(){
		},
		onRender: function(){
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/project_indicators/?project='+_this.model.id,
				columns: [
				{
					field: 'name',
					title: 'Показатели',
				},
				{
					field: 'value',
					title: 'План',
					editable: {
						type: 'text',
						emptytext: 'Установить',
						url: '/apiprojects/project_indicators/',
						ajaxOptions: {
							type: 'PATCH',
							dataType: 'json',
							beforeSend: function(jqXHR,settings){
								var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
								settings.url += row.id_field+'/';
								settings.data = row.field+'='+settings.data.match(/value=\d+/g)[0].match(/\d+/g)[0];
							}
						},
						params: function(params){
							delete params['name'];
							return params;
						},
						success: function(response){
							_this.Table.bootstrapTable('refresh', {silent: true});
						}
					},
					cellStyle: function(value, row, index, field){
						if(row.field){
							return{
								classes: '',
							};
						};
						return {
							classes: 'no-editble',
						};
					}
				},
				{
					field: 'fact',
					title: 'Факт',
				},
				{
					field: 'percent_complete',
					title: '% выполнения',
				},
				],
				responseHandler: function(response){
					var arr = [];
					var count = 1;
					for(i in response[0]){
						response[0][i].id = count;
						arr.push(response[0][i]);
						count += 1;
					};
					arr.sort(function(a, b) {
						var nameA = a.name.toUpperCase(); 
						var nameB = b.name.toUpperCase(); 
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						return 0;
					});
					return arr
				},
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				}
			});

		}
	});
	return ProjectIndicatorsView
})
