define(['text!projects/html/main_view.html', 'Noty', 'utils', 'selectize', 'bootstrap-modal', 'datetimepicker'], function(html, Noty, utils){
	var ProjectFormView = Backbone.Marionette.View.extend({
		statusView:{
			'planning':'Планирование',
			'working': 'В работе',
			'finished': 'Завершен',
			'canceled': 'Отменен'
		},
		events:{
			'click .openCollapse': 'openCollapse',
			'click #create_section': 'createSection',
			'click .glyphicon-trash': 'removeRow',
			'click .glyphicon-pencil': 'editRow',
			'click #PreSave': 'PreSave',
			'click #SaveButton': 'save',
			'click #CancelButton': 'cancel'
		},
		cancel: function(e){
			history.back();
			// mainView.GetProjectView();
		},
		preSaveBool: false,
		save: function(){
			var _this = this;
			var form = this.$el.find('form');
			var data = new FormData(form[0]);
			var sites = data.get('sites').split(',');
			data.delete('sites');
			for(i in sites){
				data.append('sites', sites[i])
			};
			data.set('date_start', this.$el.find('#datetimepicker1').data("DateTimePicker").date().format('YYYY-MM-DD'));
			data.set('date_end', this.$el.find('#datetimepicker2').data("DateTimePicker").date().format('YYYY-MM-DD'));
			$.ajax({
				type: 'PATCH',
				url: '/apiprojects/projects/'+this.project_id+'/',
				data: data,
				contentType: false,
				processData: false,
				success: function(response){
					if(!_this.preSaveBool){
						utils.notySuccess().show();
						if(response.alert){
							utils.notyError('Нарушены сроки подготовки залов, проект не будет запущен в работу').show();
						}
					};
					_this.preSaveBool = false;
					_this.model.fetch({
						success: function(response){
							if(response.get('project_type') && (response.get('project_type').type_project_type == 'expo' || response.get('project_type').type_project_type == 'ad')){
							mainView.getRegion('MainContent').currentView.$el.find('#ProjectReportsView').removeClass('disabled');
							}
							else{
								mainView.getRegion('MainContent').currentView.$el.find('#ProjectReportsView').addClass('disabled');
							}
						}
					});
					_this.triggerMethod('closeModal', {id: response.id, name: response.name});
				},
				error: function(response){
					if(response.responseJSON){
						utils.notyError(response.responseJSON.alert).show();
					}
					else{
						utils.notyError().show();
					}
				}
			})
		},
		initialize: function(){
			if(this.model){
				this.project_id = this.model.id;
			}
			this.name = 'project';
		},
		PreSave: function(e){
			this.preSaveBool = false;
			var _this = this;
			$(e.currentTarget).prop('disabled', true);
			var form = new FormData();

			form.append('name', this.$el.find('input[name="name"]').val()) ;
			$.ajax({
				url: '/apiprojects/projects/',
				type: 'POST',
				contentType: false,
				processData: false,
				// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
				data: form,
				success: function(response){
					_this.project_id = response.id;
					var model = Backbone.Model.extend({url:this.url + response.id});
					if(!_this.options.client){
						router.navigate('project/'+response.id+'/edit', {replace: true});
						window.mainView.getRegion('MainContent').currentView.enabledTabs();
						window.mainView.getRegion('MainContent').currentView.$el.find('#ProjectReportsView').addClass('disabled');
					}
					// замена потестить
					// _this.model = new model(response);
					_this.model = new model();
					_this.model.fetch();
					$(e.currentTarget).fadeOut(1);
					$(e.currentTarget).parent().parent().removeClass();
					_this.$el.find('#ProjectInfo').collapse("show");
					utils.notySuccess().show();
					mainView.getRegion('MainContent').currentView.model = _this.model;

				},
				error: function(response) {
					$(e.currentTarget).prop('disabled', false);
					utils.notyError().show();
					/* Act on the event */
				}

			});

		},
		questionSave: function(){
			var _this=this;
			var modal = new Backbone.BootstrapModal({
				content: 'Сохранить изменения',
				title: 'Сохранить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true,
			}).open();
			modal.on('ok', function(){
				_view.save();
			});
		},
		removeRow: function(e){
			var _this=this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить раздел?',
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true,
			}).open();
			modal.on('ok', function(){
				var row = $(e.target).parent().parent();
				$('.loading').fadeIn();
				$.ajax({
					type: 'DELETE',
					url: '/apiprojects/project_section/' + row[0].id,
					contentType: false,
					processData: false,
					success: function(response){
						_this.model.fetch({
							success: function(response){
								_this.render();
								$('.loading').fadeOut();
								utils.notySuccess().show();
							}
						});
					// _this.getSummRow([1,2,3,4,5]);
				}
			});
			});
		},
		// Переводит данные таблицы в объект для формы редактирования
		editRow: function(e){
			var TableData = {};
			TableData.subsection = [];
			var TableRow = $(e.target).parent().parent().find('td').toArray();
			TableRow.pop();
			for (row in TableRow){
				if(TableRow[row].className!='tdCenter'){
					var Inner = $(TableRow[row]).find('div').toArray();
					for( i in Inner){
						if (Inner[i].className=='SectionName'){
							TableData.name = Inner[i].innerText;
						}
						else{
							TableData.subsection.push(Inner[i].innerText);
						}
					}
				}
				else{
					TableData[$(TableRow[row]).attr('uname')] = this.getNum(TableRow[row].innerText);
					for (i in $(TableRow[row]).data()){
						TableData[i] = $(TableRow[row]).data()[i];
					}
				}

			}
			this.createSection(TableData, e.currentTarget);
		},
		// Вытаскивает число из любой строки
		getNum: function(str){
			return parseFloat(str.replace(/\.(?=.*\.)|[^\d\.-]/g, ''));
		},
		// Функция счтатет сумму в столбцах принимает массиы номеров столбцов, начиная с нуля
		getSummRow(RowNum){
			var cont = 0;
			var SummRow = {};
			for (i in RowNum){
				SummRow[RowNum[i]] = {count:0};
			};
			var Table = this.$el.find('tr').toArray();
			delete Table[0];
			for (col in Table){
				if(Table[col].id != 'infoRow'){
					var col = $(Table[col]).children().toArray();
					for(row in col){
						if(RowNum.indexOf(parseInt(row))!=-1){
							SummRow[row].count += this.getNum(col[row].innerHTML);
						}
					}
				}else{
					var LastCol = $(Table[col]).children().toArray();
					for(i in SummRow){
						// LastCol[i].id=='percent_conxept_all'

						if($(LastCol[i]).hasClass('percent')){
							$(LastCol[i]).replaceWith('<td id="percent_conxept_all" class="infoRow percent">' + Math.round(SummRow[i].count) + ' %</td>');
						}
						else{
							$(LastCol[i]).replaceWith('<td class="infoRow">' + SummRow[i].count + '</td>');
						}
					}

				}

			}
		},
		//formdata из объекта
		getFormData(obj, exclude, arrayField){
			var data = new FormData();
			for(i in obj){
				if(exclude.indexOf(i)==-1 && arrayField.indexOf(i)==-1){
					data.append(i, obj[i]);
				}
				if(arrayField.indexOf(i)!=-1){
					obj[i] = obj[i].split(',');
					if(obj[i].length>0){
						for(j in obj[i]){
							data.append(i, obj[i][j]);
						};
					}
					else{
						if(data.has(i)){
							data.delete(i);
						}
						else{
							exclude.push(i);
						}
					}
				}
			};
			data.append('project', this.project_id);
			return data
		},

		createSection: function(Data, row){
			var _this=this;
			var edit = false;
			var okText = 'Добавить';
			var title = 'Добавить раздел';
			var type = 'POST';
			var html;
			var id = '';
			if(row){
				id = $(row).parent().parent()[0].id;
				type = 'PATCH'
				title = 'Редактировать раздел'
				okText = 'Сохранить'
				edit = true;
			};
			if(this.preSaveBool){
				this.save();
				this.preSaveBool = false;
			};
			Data.percent = 100 - this.getNum(this.$el.find('#percent_conxept_all')[0].innerHTML);
			require(['projects/js/create_section_view', 'projects/js/modal_template'], function(CreateSectionView, tmp){
				var modal = new Backbone.BootstrapModal({
					content: new CreateSectionView({
						htmlContext:Data,
						project_id:_this.project_id,
						sectionCollection: new Backbone.Collection(_this.model.get('project_section')),
						section_id: id
					}),
					title: title,
					template: tmp,
					okText: okText,
					cancelText: 'Отмена',
					okCloses: false,
					escape: false,
					modalOptions: {
						backdrop: 'static',
						keyboard: false
					},
					animate: true,
				}).open();
				modal.on('ok', function(){
					var _modal = this;
					if(this.options.content.validate()){
						var form = this.$content.find('form');
						form.find(':input:disabled').removeAttr('disabled');
						var html = _this.getHTMLRowTable(this.$content.find('form').serializeArray());
						var RowTable = _this.getRowTable(this.$content.find('form').serializeArray());
						this.close();
						$('.loading').fadeIn();

						var formData = _this.getFormData(RowTable, ['section'], ['activities']);
						formData.append('html', $(html).children()[0].innerHTML);
						$.ajax({
							url: id==''?'/apiprojects/project_section/':'/apiprojects/project_section/'+id+'/',
							contentType: false,
							processData: false,
							type: type,
							data: formData,
							success: function(response){
								_this.$el.find('#empty').attr('id', response.id);
								if(_modal.options.content.SelectActivities[0].selectize.ItemsRemoved.length>0){
									var data = new FormData();
									data.append('removed_id', _modal.options.content.SelectActivities[0].selectize.ItemsRemoved.toString());
									data.append('project_id', _this.project_id);
									$.ajax({
										url:'/apiworkers/managers/remove_related/',
										type:'POST',
										contentType: false,
										processData: false,
										data: data,
										success: function(response){
											_this.model.fetch({
												success:function(){
													_this.render();
													$('.loading').fadeOut();
													utils.notySuccess().show();
												}
											});
										},
										error: function(response){
											utils.notyError().show();
										}
									});
								}
								else{
									_this.model.fetch({
										success:function(){
											_this.render();
											$('.loading').fadeOut();
											utils.notySuccess().show();
										},
										error: function(response){
											utils.notyError().show();
										}
									});
								}


							},
							error: function(response){
								// _modal.open();
								utils.notyError().show();
								$('.loading').fadeOut();
							}
						});

					// if(row){
					// 	_this.model.fetch({
					// 		success:function(){
					// 			_this.render();
					// 		}
					// 	});
					// 	// html = $(row).parent().parent().replaceWith(html);
					// 	// _this.getSummRow([1,2,3,4]);

					// }else {
					// 	_this.model.fetch({
					// 		success:function(){
					// 			_this.render();
					// 		}
					// 	});
					// 	// html = _this.$el.find('tbody').prepend(html);
					// 	// _this.getSummRow([1,2,3,4]);
					// }
				};
			});
			});
		},
		getRowTable: function(data){
			var htmlContext = {};
			var section = [];
			for (i in data){
				if (data[i].name.split('.')[0]=='subsection'){
					section.push(data[i].value)
				}
				else{
					htmlContext[data[i].name]=data[i].value;
				}

			}
			htmlContext.section = section;
			return htmlContext;
		},
		guid: function(){
		  function s4() {
		    return Math.floor((1 + Math.random()) * 0x10000)
		      .toString(16)
		      .substring(1);
		  }
		  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		    s4() + '-' + s4() + s4() + s4();
		},
		getHTMLRowTable:function(data){
			var htmlContext = this.getRowTable(data);
			htmlContext.guid = this.guid();
			return Handlebars.compile($(html).filter('#newRow')[0].innerHTML)(htmlContext);
		},
		onRender: function(){
			$('.loading').fadeOut();
			var _this = this;
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();

			});
			_view.$el.on("hide.bs.collapse", '.listCollapse',function(){
				$(this).parent().find('span').removeClass();
				$(this).parent().find('span').addClass('glyphicon glyphicon-chevron-right menuSpan');
			});
			_view.$el.on("show.bs.collapse",'.listCollapse',  function(){
				$(this).parent().find('span').removeClass();
				$(this).parent().find('span').addClass('glyphicon glyphicon-chevron-down menuSpan');
			});
			this.$el.find('#datetimepicker1').datetimepicker({
				locale: 'ru',
				format: 'DD.MM.YYYY'
			});
			this.$el.find('#datetimepicker2').datetimepicker({
				useCurrent: false,
				locale: 'ru',
				format: 'DD.MM.YYYY'
			});
			this.$el.find('#datetimepicker3').datetimepicker({
                format: 'LT',
                locale: 'ru',
            });
			this.$el.find('#datetimepicker4').datetimepicker({
                format: 'LT',
                locale: 'ru',
            });
            this.$el.find('#datetimepicker5').datetimepicker({
                format: 'LT',
                locale: 'ru',
            });
			this.$el.find("#datetimepicker1").on("dp.change", function (e) {
				_view.$el.find('#datetimepicker2').data("DateTimePicker").minDate(e.date);
			});
			this.$el.find("#datetimepicker2").on("dp.change", function (e) {
				_view.$el.find('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
			});
			if(this.model){
				this.getSummRow([1,2,3,4,5]);
				if(this.model.get)
				this.$el.find('#datetimepicker1').data("DateTimePicker").date(this.model.get('date_start')?new Date(this.model.get('date_start')):new Date());
				this.$el.find('#datetimepicker2').data("DateTimePicker").date(this.model.get('date_end')?new Date(this.model.get('date_end')):new Date());
			};


			this.$el.find('#SelectSubject').selectize({
				valueField: 'id',
				labelField: 'subject_name',
				searchField: 'subject_name',
				onInitialize: function(){
					var _this = this;
					$.get({
						url: '/apiprojects/subjects/',
						success: function(response){
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i]);
							};
							if(_view.model){
								_this.setValue(_view.model.get('subjects'));
							}
						}
					})
				}
			});


			this.SelectizeBuisnessUnit = this.$el.find('#SelectBuisnessUnit').selectize({
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/business_units/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption({
									value:response[i].id,
									text:response[i].name})
							}
							return response;
						}
					});
				},
			});
			this.$el.find('#SelectProjectType').selectize({
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/projects_types/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption({
									value:response[i].id,
									text:response[i].type,
									def_b_u:response[i].default.id,
									type_project_type:response[i].type_project_type
								})
							}
							if(_view.model){
								_this.setValue(_view.model.get('project_type').id);
							}
							return response;
						}
					});
				},
				options:[],
				load: function(input) {
				},
				onChange: function(value){
					_this.SelectizeBuisnessUnit[0].selectize.setValue(this.options[value].def_b_u);
					if (this.options[value].type_project_type == 'expo'){
						_this.$el.find('#ThematicProject').collapse("show");
						_this.$el.find('#clientCollapse').collapse("hide");
						_this.$el.find('#formatEventCollapse').collapse("hide");
					}
					else{
						_this.$el.find('#ThematicProject').collapse("hide");
						switch (this.options[value].type_project_type) {
							case 'expo':
								_this.$el.find('#clientCollapse').collapse("hide");
								_this.$el.find('#formatEventCollapse').collapse("hide");
								break;
							case 'ad':
								_this.$el.find('#clientCollapse').collapse("hide");
								_this.$el.find('#formatEventCollapse').collapse("show");
								break;
							case 'kvs':
								_this.$el.find('#clientCollapse').collapse("show");
								_this.$el.find('#formatEventCollapse').collapse("show");
								break;
						}
					}
				}

			});
			this.$el.find('#SelectClient').selectize({
				labelField: 'name',
				valueField: 'id',
				searchField: 'name',
				loadingClass: '',
				onInitialize: function(){
					if(_view.options.client){
						this.addOption(_view.options.client);
						this.setValue(_view.options.client.id);
					}
					if(_view.model && _view.model.get('client')){
						this.addOption(_view.model.get('client'));
						this.setValue(_view.model.get('client').id);
					}
				},
				load: function(input){
					var _this = this;
					if(input!=''){
						this.clearOptions();
						clearTimeout(this.timeout_id);
						this.timeout_id = setTimeout(function(){
							$.get({
								url: '/apiclients/clients/?data=min&search='+input,
								success: function(response){
									for(i in response){
										_this.addOption(response[i]);
									}
									_this.focus();
								}
							});
						}, 500);
					}
				}
			});
			this.$el.find('#SelectStatus').selectize({
				options:[
				{text:'Планирование', value: 'planning'},
				{text:'В работе', value: 'working'},
				{text:'Завершен', value:'finished'},
				{text:'Отменен', value: 'canceled'}],
				items:['planning'],
				onInitialize: function(){
					if(_view.model)
						this.setValue(_view.model.get('status'));
				}
			});
			this.$el.find('#SelectExhibitionType').selectize({
				options: [
					{text: 'ДВП1', value: 'dvp1'},
					{text: 'ДВП2', value: 'dvp2'}
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('exhibition_type'));
					}
				}
			});
			this.$el.find('#SelectFormatEvent').selectize({
				options: [
					{text: 'Выставка', value: 'exhibition'},
					{text: 'Форум', value: 'forum'},
					{text: 'Корпоратив', value: 'corporate'},
					{text: 'Конкурс/чемпионат', value: 'competition'},
					{text: 'Тренинг/семинар', value: 'training'},
					{text: 'Банкет', value: 'banquet'},
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('format_event'));
					}
				}
			});
			this.$el.find('#SelectArea').selectize({
				options: [
					{text: 'До 500 кв.м.', value: '1'},
					{text: 'От 501 кв.м. до 1000 кв.м.', value: '2'},
					{text: 'От 1001 кв.м. до 2000 кв.м.', value: '3'},
					{text: 'От 2001 кв.м. до 3000 кв.м.', value: '4'},
					{text: 'От 3001 кв.м. до 4000 кв.м.', value: '5'},
					{text: 'От 4001 кв.м.', value: '6'},
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('area_size'));
					}
				}
			});
			this.$el.find('#SelectSites').selectize({
				valueField: 'id',
				labelField: 'name',
				searchField: 'name',
				onInitialize: function(){
					var _this=this;
					var response = $.get({
						url: '/apiprojects/sites/',
						async: true,
						success: function(response) {
							for(var i=0; i<response.length; i++){
								_this.addOption(response[i])
							}
							if(_view.model){
								var sites = _view.model.get('sites');
								for(i in sites){
									_this.addItem(sites[i].id);
								};
							}
							return response;
						}
					});
				},
				render:{
					option: function(item, escape){
						return Handlebars.compile($(html).filter('#selectizeTemplate')[0].innerHTML)(item);
					}
				}
			});
		},
		openCollapse: function(e){
			debugger;
			this.$el.find('selector')
		},
		template:$(html).filter('#FormProjectView')[0].outerHTML,
		className: function(){
			if(this.options.client){
				return ''
			}
			return 'panel panel-default panelTabs displayNone'
		},
		templateContext: function() {
			if(this.options.client){
				return{show: false}
			}
			return {show: true}
			// return {
			// 	statusView: this.statusView[this.model.get('status')]
			// }
		},
		
	});
			return ProjectFormView

		})

