define(['text!projects/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale', 'selectize'], function(html){

	var View = Backbone.Marionette.View.extend({
		template:$(html).filter('#catering_view')[0].outerHTML,
		events: {
		},
		validate: function(){
			var valid = true;
			var inputs = this.$el.find('[data-validate="True"]').toArray();
			for(i in inputs){
				if($(inputs[i]).parent().hasClass('has-error')){
					$(inputs[i]).parent().removeClass('has-error')
				};
				if($(inputs[i]).val()==""){
					$(inputs[i]).parent().addClass('has-error');
					valid = false;
				};
			};
			return valid;
		},
		initialize: function(){

		},
		onRender: function(){
			var _view = this;
			this.SelectType = this.$el.find('#SelectType').selectize({
				options: [
					{
						value: 'coffee_break',
						text: 'Кофе-брейк'
					},
					{
						value: 'launch',
						text: 'Обед'
					},
					{
						value: 'dinner',
						text: 'Ужин'
					},
				],
				onInitialize: function(){
					if(_view.model){
						this.setValue(_view.model.get('catering_type'));
					}
				}
			});
		},
	});
	return View;
})
