define(['text!projects/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var ProjectIndicatorsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#project_indicators_view')[0].outerHTML,
		events: {
		'click #oldProjectIndicator': 'getListOldProject',
		},
		regions: {
		},
		className: 'panel panel-default panelTabs displayNone form',
		getListOldProject: function(e, model){
		    var _this = this
		    $.get({
                url: '/apiprojects/projects/get_old_projects/?project='+_this.model.id,
                async: true,
                success: function(response) {
                    response = response.slice(0,3)
                    _this
                    var table = _this.$el.find('#table')

                    var columns= [
                        {
                            field: 'name',
                            title: 'Показатели',
                            cellStyle: function(value, row, index, field){
                                if(row.right){
                                    return {
                                        classes: '',
                                        css: {'padding-left': '40px'}
                                    }
                                }
                                return {
                                    classes: ''
                                }
                            }
                        },
                        {
                            field: 'value',
                            title: 'План',
                            align: 'right',
                            formatter: function(value, row, index){
                                if(value){
                                    value = value.toString().replace(/ /g,'');
                                    var number = value;
                                    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                                }
                                return value;
                            },
                            editable: {
                                type: 'text',
                                emptytext: 'Установить',
                                url: '/apiprojects/project_indicators/',
                                ajaxOptions: {
                                    type: 'PATCH',
                                    dataType: 'json',
                                    beforeSend: function(jqXHR,settings){
                                        var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
                                        settings.url += row.id_field+'/'+'?type='+_this.model.get('project_type').type_project_type;
                                        settings.data = settings.data.replace(/\+/gi,'');
                                        settings.data = row.field+'='+settings.data.match(/value=\d+/g)[0].match(/\d+/g)[0];
                                    }
                                },
                                params: function(params){
                                    delete params['name'];
                                    return params;
                                },
                                success: function(response){
                                    _this.Table.bootstrapTable('refresh', {silent: true});
                                }
                            },
                            cellStyle: function(value, row, index, field){
                                if(row.field && user.isSuperuser){
                                    return{
                                        classes: '',
                                    };
                                };
                                return {
                                    classes: 'no-editble',
                                };
                            }
                        },
                        {
                            field: 'fact',
                            title: 'Факт',
                            align: 'center',
                            formatter: function(value, row, index){
                                if(value){
                                    value = value.toString().replace(/ /g,'');
                                    var number = value;
                                    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                                }
                                return value;
                            },
                        },
                        {
                            field: 'percent_complete',
                            title: '% выполнения',
                            align: 'center'
                        },
                    ]


                    for(var i in response){
                        var type = _this.model.get('project_type')?_this.model.get('project_type').type_project_type:undefined;
                        columns.push({
                            field: 'fact'+i,
                            title: 'Факт ' + response[i].date_start.slice(0, 4),
                            formatter: function(value, row, index){
                                if(value){
                                    value = value.toString().replace(/ /g,'');
                                    var number = value;
                                    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                                }
                                return value;
                            },
                        })
                    }

                    table.bootstrapTable('refreshOptions',{
                        async: false,
                        columns:columns,
                        onLoadSuccess: function(resp) {

                            $('#table').bootstrapTable({
                              data: temp
                            });
                            $('#table').bootstrapTable('load', temp);
                        }
                    })

                    var temp = JSON.parse(JSON.stringify(table.bootstrapTable('getData')))

                    for(var k in response){
                        var type = _this.model.get('project_type')?_this.model.get('project_type').type_project_type:undefined;

                        $.get({
                            url: '/apiprojects/project_indicators/?project='+response[k].id+'&type='+type,
                            param:k,
                            async: false,
                            success: function(resp) {
                                response
                                for(var i in temp){
                                    for(var j in resp[0]){
                                        if(temp[i].name == resp[0][j].name){
                                            temp[i][`fact${this.param}`] = resp[0][j].fact
                                            continue
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            })
		},
		initialize: function(){
		},
		onRender: function(){
			$('.loading').fadeIn();
			var _this = this;
			this.$el.find(document).ready(function($) {
				_this.$el.fadeIn(1000);
			});
			var type = _this.model.get('project_type')?_this.model.get('project_type').type_project_type:undefined;
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				classes: 'table table-hover table-bordered',
				url: '/apiprojects/project_indicators/?project='+_this.model.id+'&type='+type,
				columns: [
				{
					field: 'name',
					title: 'Показатели',
					cellStyle: function(value, row, index, field){
						if(row.right){
							return {
								classes: '',
								css: {'padding-left': '40px'}
							}
						}
						return {
							classes: ''
						}
					}
				},
				{
					field: 'value',
					title: 'План',
					align: 'right',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
					editable: {
						type: 'text',
						emptytext: 'Установить',
						url: '/apiprojects/project_indicators/',
						ajaxOptions: {
							type: 'PATCH',
							dataType: 'json',
							beforeSend: function(jqXHR,settings){
								var row = _this.Table.bootstrapTable('getRowByUniqueId', settings.data.match(/pk=\d+/g)[0].match(/\d+/g)[0]);
								settings.url += row.id_field+'/'+'?type='+_this.model.get('project_type').type_project_type;
								settings.data = settings.data.replace(/\+/gi,'');
						    	settings.data = row.field+'='+settings.data.match(/value=\d+/g)[0].match(/\d+/g)[0];
							}
						},
						params: function(params){
							delete params['name'];
							return params;
						},
						success: function(response){
							_this.Table.bootstrapTable('refresh', {silent: true});
						}
					},
					cellStyle: function(value, row, index, field){
						if(row.field && user.isSuperuser){
							return{
								classes: '',
							};
						};
						return {
							classes: 'no-editble',
						};
					}
				},
				{
					field: 'fact',
					title: 'Факт',
					align: 'center',
					formatter: function(value, row, index){
						if(value){
							value = value.toString().replace(/ /g,'');
						    var number = value;
						    value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						}
						return value;
					},
				},
				{
					field: 'percent_complete',
					title: '% выполнения',
					align: 'center'
				},
//				{
//                            field: 'fact2019',
//                            title: '% выполнения',
//                            align: 'center'
//                        },
				],
				rowStyle: function(row){
					if(row.bold){
						return {
							classes: '',
							css: {
								'font-weight': 'bolder'
							}
						}
					}
					return {
						classes: ''
					}
				},
				responseHandler: function(response){
					var arr = [];
					var count = 1;
					$('.loading').fadeOut();
					for(i in response[0]){
						response[0][i].id = count;
						if(response[0][i].remove_zero && (!response[0][i].fact || response[0][i].fact == "0")){
							continue;
						}
						arr.push(response[0][i]);
						count += 1;
					};

					arr.sort(function(a, b) {
						var nameA = a.sort;
						var nameB = b.sort;
						if (nameA < nameB) {
							return -1;
						}
						if (nameA > nameB) {
							return 1;
						}
						return 0;
					});
					return arr
				},
				onLoadSuccess: function () {
					var no_edit = _this.$el.find('.no-editble a');
					no_edit = no_edit.toArray();
					for(i in no_edit){
						no_edit[i].outerHTML = no_edit[i].innerHTML
					};
				}
			});
			this.$el.on('keyup', '.form-control.input-sm', function(){
			    this.value = this.value.replace(/ /g,'');
			    var number = this.value;
			    this.value = number.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			});
		}
	});
	return ProjectIndicatorsView
})
