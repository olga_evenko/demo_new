define(['text!projects/html/main_view.html','libs/backbone.paginator/js/paginator', 
	'paginator-view', 'js/filters_view', 'utils',
	'bootstrap-table-locale', 'bootstrap-modal'], function(html, Paginator, PaginatorView, FilterView, utils){

	var ProjectsView = Backbone.Marionette.View.extend({
		template:$(html).filter('#ProjectView')[0].outerHTML,
		events: {
			'click #addProject': 'addProject',
			'click .edit': 'editIndividual',
			'click .copy': 'copyProject',
			'click #filterButton': 'setFilter',
			'click #resetfilterButton': 'resetFilter',
			'click .trash': 'removeProject'
		},
		regions: {
			filter: '#filterModal'
		},
		className: 'displayNone',
		defaultUrl: true,
		addProject: function(){
			router.navigate('project/create', {trigger: true});
		},
		removeProject: function(e){
			var id = $(e.currentTarget).data().id;
			var _view = this;
			var modal = new Backbone.BootstrapModal({
				content: 'Удалить проект?', 
				title: 'Удалить?',
				okText: 'Да',
				cancelText: 'Отмена',
				animate: true, 
			}).open();
			modal.on('ok', function(){
				$.ajax({
					type: 'DELETE',
					url: '/apiprojects/projects/'+id+'/',
					success: function(response){
						utils.notySuccess('Удалено').show();
						_view.Table.bootstrapTable('refresh');
					},
					error: function(){
						utils.notyError().show();
					}
				});
			});
		},
		setFilter: function(){
			var _this = this;
			if(!viewscache.filters.projects){
				viewscache.filters.projects = new Backbone.BootstrapModal({
					content: new FilterView({
						filter_fields:[
							{
								date: {
									name: 'date_start',
									gr: true,
									ls: true,
									cn: true,
									label: 'Дата начала'
								}
							},
							{
								date: {
									name: 'date_end',
									gr: true,
									ls: true,
									cn: true,
									label: 'Дата окончания'
								}
							},
							{
								select: {
									name: 'status',
									label: 'Статус',
									url: '/apiprojects/projects/',
									type_get: 'OPTIONS',
									labelField: 'display_name'
								}
							},
							{
								select: {
									name: 'user',
									label: 'Руководитель',
									url: '/apiworkers/managers/forselect/',
									type_get: 'GET',
									labelField: 'text',
									valueField: 'user_id'
								}
							},
							{
								multiselect: {
									name: 'project_type',
									label: 'Вид проекта',
									url: '/apiprojects/projects_types/',
									type_get: 'GET',
									labelField: 'type',
									searchField: 'type'
								}
							},
							{
								multiselect: {
									name: 'sites',
									label: 'Площадки',
									url: '/apiprojects/sites/',
									type_get: 'GET',
									labelField: 'name',
									searchField: 'name'
								}
							}
						]
					}), 
					title: 'Фильтр',
					okText: 'Применить',
					cancelText: 'Сбросить',
					animate: true,
					okCloses: false
				}).open();
			viewscache.filters.projects.on('ok', function(){
				this.preventClose();
				_this.defaultUrl = false;
				$('#table').bootstrapTable('refresh', {
					silent: true, 
					query: this.options.content.getQuery(),
					pageNumber: 1
				});
				this.$el.modal('hide');
			});
			viewscache.filters.projects.preventClose();
			viewscache.filters.projects.on('cancel', function(e){
				this.preventClose();
				if(e){
					this.options.content.resetForm();
				}
				else{
					this.$el.modal('hide');
				}
			});
		}
		else{
			viewscache.filters.projects.$el.modal('show');
		};
			// this.$el.find('#FiltersModal').modal();
		},
		resetFilter: function(){
			// this.defaultUrl = true;
			if(viewscache.filters.projects){
				viewscache.filters.projects.options.content.resetForm();
			};
			this.$el.find('#table').bootstrapTable('refresh', {
				silent: true,
			}); 
		},
		copyProject: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			require(['projects/js/modal_extend_project_view'], function(ModalExtendProjectView){
				var _this=this;
				var modal = new Backbone.BootstrapModal({
					content: new ModalExtendProjectView({name:row.name}), 
					title: 'Создать проект по выбранному',
					okText: 'Создать',
					cancelText: 'Отмена',
					animate: true, 
				}).open();
				modal.on('ok', function(){
					$('.loading').fadeIn();
					var data = new FormData(this.$content.find('form')[0]);
					data.append('id', row.id);
					$.ajax({
						type: 'POST',
						url: 'apiprojects/projects/extend/',
						contentType: false,
						processData: false,
						data: data,
						success: function(response){
							require(['projects/js/project_form_view'], function(CreateClientView){
								window.mainView.showChildView('MainContent', new CreateClientView({model: new Backbone.Model(response)}));
							});
						}
					});
				});
			});
		},
		initialize: function(){
			
			// this.filterView = new FilterView({
			// 	filter_fields:[
			// 		{
			// 			date: {
			// 				name: 'date_start',
			// 				gr: true,
			// 				ls: true,
			// 				cn: true,
			// 				label: 'Дата начала'
			// 			}
			// 		},
			// 		{
			// 			date: {
			// 				name: 'date_end',
			// 				gr: true,
			// 				ls: true,
			// 				cn: true,
			// 				label: 'Дата окончания'
			// 			}
			// 		},
			// 		{
			// 			select: {
			// 				name: 'status',
			// 				label: 'Статус',
			// 				url: '/apiprojects/projects/',
			// 				type_get: 'OPTIONS',
			// 				labelField: 'display_name'
			// 			}
			// 		},
			// 		{
			// 			multiselect: {
			// 				name: 'project_type',
			// 				label: 'Вид проекта',
			// 				url: '/apiprojects/projects_types/',
			// 				type_get: 'GET',
			// 				labelField: 'type'
			// 			}
			// 		}
			// 	]
			// });
			// this.filterView.on('filtered', function(){
			// 	_this.defaultUrl = false;
			// 	_this.$el.find('#table').bootstrapTable('refresh', {
			// 		silent: true, 
			// 		query: _this.filterView.getQuery(),
			// 		pageNumber: 1
			// 	});
			// });
			// this.showChildView('filter', this.filterView);
		},
		editIndividual: function(e){
			var row = this.Table.bootstrapTable('getRowByUniqueId',$(e.currentTarget).data().id);
			router.navigate('project/'+row.id+'/edit', {trigger: true});
		},
		onRender: function(){
			var _view = this;
			this.$el.find(document).ready(function($) {
				_view.$el.fadeIn();
			});
			this.Table = this.$el.find('#table').bootstrapTable({
				idField: 'id',
				uniqueId: 'id',
				pagination: true,
				search: true,
				totalField: 'count',
				sidePagination: 'server',
				dataField: 'results',
				checkbox: true,
				sortable: true,
				showColumns: true,
				// height: 500,
				// maintainSelected: true,
				// clickToSelect: true,
				// checkboxHeader: true,
				classes: 'table table-hover table-bordered',
				iconSize: 'sm',
				url: '/apiprojects/projects/',
				newQuery: {},
				onRefresh: function(options){
					if(options.query){
						this.newQuery = options.query;	
					}
					else{
						this.newQuery = {};
					};
				},
				onClickRow: function(row, e, index){
					if(index!='edit'){
						router.navigate('project/'+row.id+'/edit', {trigger: true});
						// router.navigate('project/'+row.id, {trigger: true});
					}
				},
				onPageChange: function(num, size){
					router.navigate('projects/p'+num+'-'+size, {replace: true});
				},
				pageSize: _view.options.page_size,
				pageNumber: _view.options.page_num,
				rowStyle: function(row, index) {
					return {
						css: {"cursor": "pointer"}
					};
				},
				toolbar: $(html).filter('#toolbar')[0].innerHTML,
				columns: [
				{
					field: 'created_date',
					title: 'Дата создания',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'name',
					title: 'Название',
					sortable: true,
				},
				{
					field: 'user',
					title: 'Руководитель проекта',
					sortable: true,
				},
				{
					field: 'project_type.type',
					title: 'Вид проекта',
					sortable: true,
				},
				{
					field: 'buisness_unit.name',
					title: 'Бизнес-единица',
					sortable: true,
				},
				{
					field: 'date_start',
					title: 'Дата начала',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						}
						return value;
					}
				},
				{
					field: 'date_end',
					title: 'Дата окончания',
					sortable: true,
					formatter: function(value, row, index){
						if(value!=null){
							var D = value.split('-')
							value = D[2]+'-'+D[1]+'-'+D[0];
							return value
						};
						return value
					}
				},
				{
					field: 'sites_all',
					title: 'Место проведения',
					// sortable: true,
				},
				{
					field: 'count_guests',
					title: 'Количество гостей',
					sortable: true
				},
				{
					field: 'status',
					title: 'Статус',
					sortable: true,
					formatter: function(value, row, index){
						var status = {
							'planning': 'Планирование',
							'working': 'В работе',
							'canceled': 'Отменен',
							'finished': 'Завершен'
						};
						return status[value]
					}
				},
				{
					field: 'edit',
					title: '',
					formatter: function(value, row, index){
						return '<div style = "vertical-align: middle; text-align: center;"><span class="glyphicon glyphicon-pencil pencil edit" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-copy pencil copy" data-id ='+
						row.id+'></span><span class="glyphicon glyphicon-trash trash" data-id ='+
						row.id+'></span></div>';
					}
				}],
				rowAttributes: function(row, index, e){
					// var status = {
					// 	'planning': 'Планирование',
					// 	'working': 'В работе',
					// 	'canceled': 'Отменен',
					// 	'finished': 'Завершен'
					// };
					// row.status = status[row.status];
					// var DS, DE;
					// if(row.date_start!=null || row.date_end!=null){
						// DS = row.date_start.split('-');
						// DE = row.date_end.split('-');
						// row.date_start = DS[2]+'-'+DS[1]+'-'+DS[0];
						// row.date_end = DE[2]+'-'+DE[1]+'-'+DE[0];
					// };
				},
				responseHandler: function(response){
					var results = response.results;
					var sites_arr = [];
					var s = 0;
					var sum_cap = 0;
					for(i in results){
						for(j in results[i].sites){
							 sites_arr.push(results[i].sites[j].name);
							 s += parseInt(results[i].sites[j].area);
							 sum_cap += results[i].sites[j].capacity;
						}
						results[i].sites_all = sites_arr.join(', ');
						results[i].all_area = s;
						results[i].all_capacity = sum_cap;
						sites_arr = [];
						s = 0; 
						sum_cap = 0;
					};
					return response
				},
				queryParams: function(params){
					if(params.sort){
						params.sort = params.sort.replace('.', '__');
						if(params.sort == 'user'){
							params.sort = 'user__last_name';
						};
					};
					if(params.order == 'desc'){
						params.sort = '-' + params.sort;
						return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
					};
					return Object.assign(params, viewscache.filters.projects?viewscache.filters.projects.options.content.getQuery():{});
				},
				onPostBody: function () {
					// $('#table').editableTableWidget({editor: $('<textarea>')});
				}
			});
		},
	});


return ProjectsView

})
