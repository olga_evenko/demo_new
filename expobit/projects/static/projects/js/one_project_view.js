define(['text!projects/html/main_view.html', 'bootstrap-table-editable', 'bootstrap-table-locale'], function(html){

	var ItemProjectView = Backbone.Marionette.View.extend({
		template:$(html).filter('#one_project_view')[0].outerHTML,
		events: {
			'click #ProjectView': 'openProjectView',
			'click #ProjectIndicatorsView': 'openProjectIndicatorsView',
			'click #ProjectReportsView': 'openProjectReports',
			'click #ProjectReportBuilder': 'openProjectInfo'
		},
		regions: {
			navMenu: "#navMenu",
			MenuContent: "#MenuContent"
		},
		// className: 'displayNone',
		initialize: function(){
			this.change_project = false;
		},
		templateContext: function(){
			var show_report_bool = true;
			// if(this.model && this.model.get('project_type') && this.model.get('project_type').type_project_type != 'expo'){
			// 	show_report_bool = false;
			// }
			return {show_report: show_report_bool}
		},
		disabledTabs: function(){
			this.$el.find('li').not('#ProjectView').addClass('disabled');
		},
		enabledTabs: function(){
			this.$el.find('li').not('#ProjectView').removeClass('disabled');
		},
		openProjectInfo: function(e){
			if(!window.location.hash.match('create') && !$(e.currentTarget).hasClass('disabled')){
				var _this = this;
				this.changeMenuActive(e);
				require(['projects/js/info_project_view'], function(InfoProjectView) {
					_this.showChildView('MenuContent', new InfoProjectView({model:_this.model}))
				});
			}
		},
		openProjectReports: function(e){
			if(!window.location.hash.match('create') && !$(e.currentTarget).hasClass('disabled')){
				var _this = this;
				this.changeMenuActive(e);
				if(this.model.get('project_type').type_project_type == 'expo'){
					require(['projects/js/project_reports_view'], function(tabProjectView) {
						_this.showChildView('MenuContent', new tabProjectView({model:_this.model}))
					});
				}
				if(this.model.get('project_type').type_project_type == 'ad'){
					require(['projects/js/project_reports_type2_view'], function(tabProjectView) {
						_this.showChildView('MenuContent', new tabProjectView({model:_this.model}))
					});
				}
			}
		},
		onRender: function(){
			$('.loading').fadeOut();
			var _this = this;
			if(window.location.hash.match('create')){
				this.disabledTabs();
			}
			this.$el.find('#ProjectReportsView').addClass('disabled');
			if(this.model && this.model.get('project_type') && 
				(this.model.get('project_type').type_project_type == 'expo' || this.model.get('project_type').type_project_type == 'ad')){
				this.$el.find('#ProjectReportsView').removeClass('disabled');
			}
			// this.$el.find(document).ready(function($) {
			// 	_this.$el.fadeIn(1000);
			// });
			this.menu = this.$el.find('li').toArray();
			require(['projects/js/project_form_view'], function(tabProjectView) {
				_this.showChildView('MenuContent', new tabProjectView({model:_this.model}))
				if(user.groups.indexOf("head_expo") != -1){
					$('#ProjectReportsView').click();
				}
			});
		},
		openProjectView: function(e){
			if(!window.location.hash.match('create')){
				var _this = this;
				this.changeMenuActive(e);
				if(viewscache.project){
					_this.showChildView('MenuContent', viewscache.project);
					viewscache.project.$el.fadeIn();

				}
				else{
					require(['projects/js/project_form_view'], function(tabProjectView) {
						_this.showChildView('MenuContent', new tabProjectView({model:_this.model}));
					});
				};
			}
		},
		changeMenuActive: function(e){
			if(this.getRegion('MenuContent').currentView.name=='project'){
				this.getRegion('MenuContent').currentView.$el.fadeOut();
				viewscache.project = this.detachChildView('MenuContent');
			};
			for(i in this.menu){
				$(this.menu[i]).removeClass('active');
			};
			$(e.currentTarget).addClass('active');
		},
		openProjectIndicatorsView: function(e){
			if(!window.location.hash.match('create') && !$(e.currentTarget).hasClass('disabled')){
				var _this = this;
				this.changeMenuActive(e);
				require(['projects/js/project_indicators_view'], function(ProjectIndicatorsView) {
					_this.showChildView('MenuContent', new ProjectIndicatorsView({model:_this.model}))
				});
			}
		},
		onShow: function(){
			debugger;
		},

	});
	return ItemProjectView
})
