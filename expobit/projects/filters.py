import django_filters
from .models import *
from workers.models import Manager

class ListFilter(django_filters.Filter):
    def filter(self, qs, value):
        if not value:
            return qs
        self.lookup_expr = 'in'
        values = value.split(',')
        return super(ListFilter, self).filter(qs, values)

class HeadFilter(django_filters.Filter):
    def filter(self, qs, value):
        if not value:
            return qs
        users=[]
        str_users = 'head'
        while True:
            us = Manager.objects.filter(**{str_users:value})
            str_users += '__managers__head'
            if us:
                if users:
                    users = (users|us).distinct()
                    continue
                users=us
                continue
            break

        users_list = list(users.values_list('user_id', flat=True))
        users_list.append(value)
        types_project = Project_type.objects.filter(director__in = users_list).values_list('id', flat=True)

        result = qs.filter(project_type__in = types_project, user__in=users_list)

        return result


class ProjectFilter(django_filters.FilterSet):
    project_type = ListFilter(name='project_type')
    sites = ListFilter(name='sites', distinct=True)
    date_start__lt = django_filters.DateFilter(name='date_start', lookup_expr='lt')
    date_start__gt = django_filters.DateFilter(name='date_start', lookup_expr='gt')
    date_start__contain = django_filters.DateFilter(name='date_start')
    status = ListFilter(name='status')
    manager = django_filters.Filter(name='membership', lookup_expr='manager', distinct=True)
    section_activities = ListFilter(name='project_section__activities', distinct=True)
    client = django_filters.Filter(name='client', distinct=True)
    project_type__head_managers__name = django_filters.CharFilter(name='project_type',
                                                                  lookup_expr='head_managers__name')
    project_type__director = django_filters.Filter(name='project_type', lookup_expr='director', distinct=True)
    user = django_filters.Filter(name='user')
    attr_visitors_isnull = django_filters.BooleanFilter(name='attr_visitors', lookup_expr='isnull')

    user__managers__head__id = HeadFilter()

    class Meta:
        model = Project
        fields = ['project_type', 'date_start__lt', 'date_start__gt']