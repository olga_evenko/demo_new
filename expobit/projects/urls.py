from django.conf.urls import url, include
from rest_framework import routers
from .views import *
router = routers.DefaultRouter()
router.register(r'projects', ApiSetProjectView, 'Project')
router.register(r'catering', ApiSetCateringView, 'Catering')
router.register(r'complexity', ApiSetComplexityView, 'Complexity')
router.register(r'room_reservation', ApiSetRoomReservation, 'ApiSetRoomReservation')
router.register(r'projects_in_client', ApiSetProjectInClientView, 'ApiSetProjectInClientView')
router.register(r'projects_types', ApiSetProjectType, 'Projects_Types')
router.register(r'business_units', ApiSetBuisnessUnit, 'BusinessUnits')
router.register(r'sites', ApiSetSites, 'Sites')
router.register(r'approval', ApiSetApproval, 'ApiSetApproval')
router.register(r'activities', ApiSetActivities, 'Activities')
router.register(r'project_section', ApiSetProjectSection, 'ProjectSection')
router.register(r'sub_sections', ApiSetSubSection, 'SubSections')
router.register(r'subjects', ApiSetSubjects, 'Subjects')
router.register(r'project_indicators', ApiSetProjectIndicators, 'ProjectIndicators')
router.register(r'project_reports', ApiSetProjectReportView, 'ApiSetProjectReportView')
router.register(r'project_reports_type2', ApiSetProjectReportTypeTwoView, 'ApiSetProjectReportTypeTwoView')
router.register(r'report_managers', ApiSetProjectReportManagerView, 'ApiSetProjectReportManagerView')
router.register(r'report_managers_type2', ApiSetProjectReportManagerTypeTwoView, 'ApiSetProjectReportManagerTypeTwoView')
router.register(r'project_type_reports', ApiSetProjectTypeReportView, 'ApiSetProjectTypeReportView')
router.register(r'report_values', ApiSetReportValuesView, 'ApiSetReportValuesView')
router.register(r'calendar', ApiSetCalendarView, 'ApiSetReportCalendarView')

urlpatterns = [
    url(r'^', include(router.urls)),
]