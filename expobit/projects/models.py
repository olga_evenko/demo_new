from django.db import models
from django.contrib.auth.models import User, Group
import json
from simple_history.models import HistoricalRecords
from django.core.cache import cache
from django.db.models.signals import post_save
import os


# def update_project(sender, instance, created, **kwargs):
#     if instance and not created and instance.status == 'working':
#         cache.incr('count_projects')
#         pass

with open('projects/activites.json', 'r') as activites:
    CHOICES_CATEGORY = json.load(activites)
    CHOICES_CATEGORY = map(lambda x: (x['activites'], x['nameactivites']), CHOICES_CATEGORY)


class Project_type(models.Model):
    CHOICES_TYPE_VIEW = (
        ('expo', 'Выставочный'),
        ('kvs', 'КВС и прочее'),
        ('ad', 'Интерактивные выставки, открытые мероприятия')
    )
    type = models.CharField(max_length=64, verbose_name='Вид проекта')
    default = models.ForeignKey('Buisness_unit', verbose_name='Значение по умолчанию', related_name='default')
    type_app_client = models.CharField(max_length=64, verbose_name='Вид заявки от Клиента')
    type_project_type = models.CharField(choices=CHOICES_TYPE_VIEW, max_length=44)
    manager_group = models.ForeignKey(Group, blank=True, null=True, verbose_name='Группа менеджеров',
                                      related_name='Project_type_manager')
    head_managers = models.ForeignKey(Group, blank=True, null=True, verbose_name='Группа руководителей менеджеров',
                                      related_name='Project_type_head_managers')
    head_direction = models.ForeignKey(Group, blank=True, null=True, verbose_name='Группа руководителей дирекции',
                                       related_name='Project_type_head_direction')
    director = models.ForeignKey(User, verbose_name='Руководитель дирекции', related_name='direction', blank=True,
                                 null=True)
    color = models.CharField(max_length=9, verbose_name='Цвет', blank=True)
    addit_parameter = models.CharField(max_length=10, verbose_name='Дополнительный параметр', null=True, blank=True)
    room_reservation = models.BooleanField(default=False)
    class Meta:
        verbose_name = 'Вид прокта'
        verbose_name_plural = 'Виды проектов'
    def __str__(self):
        return self.type

def get_default_id():
    return Project_type.objects.filter(type='Выставочный проект')


class Subjects(models.Model):
    CHOICES_FORMAT = (
        ('B2B', 'b2b'),
        ('B2C', 'b2c'),
        ('B2B+B2C', 'b2b+b2c')
    )
    subject_name = models.CharField(verbose_name='Тематика мероприятия', max_length=64)
    description = models.CharField(max_length=64, verbose_name='Площадь', blank=True, null=True)
    format = models.CharField(max_length=64, choices=CHOICES_FORMAT, verbose_name='Формат')
    per_im_plan = models.FloatField(default=1, verbose_name='% за выполнение плана')
    per_over_plan = models.FloatField(default=1, verbose_name='% за перевыполнение плана')
    per_im_plan_head = models.FloatField(default=1, verbose_name='% за выполнение плана (рук.)')
    per_over_plan_head = models.FloatField(default=1, verbose_name='% за перевыполнение плана (рук.)')
    per_im_plan_group = models.FloatField(default=1, verbose_name='% за выполнение плана (груп.)')
    per_over_plan_group = models.FloatField(default=1, verbose_name='% за перевыполнение плана (груп.)')
    # project_type = models.OneToOneField(Project)

    class Meta:
        verbose_name = 'Тематика проекта'
        verbose_name_plural = 'Тематики проекта'

    def __str__(self):
        return self.subject_name


class Buisness_unit(models.Model):
    name = models.CharField(max_length=64, verbose_name='Бизнес-единица')
    head_direction = models.ForeignKey(Group, blank=True, null=True, verbose_name='Группа руководителей дирекции',
                                       related_name='business_unit_head_direction')

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Бизнес-единица'
        verbose_name_plural = 'Бизнес-единицы'


class Sites(models.Model):
    name = models.CharField(max_length=64, verbose_name='Наименование площадки')
    area = models.CharField(max_length=8, verbose_name='Площадь', blank=True, null=True)
    capacity = models.IntegerField(verbose_name='Вместимость', blank=True, null=True)
    ordering = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name='Сортировка')
    congress = models.BooleanField(default=False, verbose_name='Конгрессный')
    ametist = models.BooleanField(default=False, verbose_name='Аметист')
    exhibition = models.BooleanField(default=False, verbose_name='Выставочный')
    open_area = models.BooleanField(default=False, verbose_name='Открытая площадь')
    building_time = models.BooleanField(default=False)
    check_head = models.BooleanField(default=False)
    class Meta:
        verbose_name = 'Площадка'
        verbose_name_plural = 'Площадки'
    def __str__(self):
        return self.name


# def default_buisness_unit_id(id):
#     return 1

class ComplexityBuild(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название сложности')
    coefficient = models.FloatField(verbose_name='коэффициент')


class ComplexityBuildProject(models.Model):
    site = models.ForeignKey(Sites, verbose_name='Площадка')
    complexity = models.ForeignKey(ComplexityBuild, verbose_name='Сложность')
    project = models.ForeignKey('Project', verbose_name='Проект', related_name='complexity_build')
    start_building = models.DateField()
    end_building = models.DateField()


class CateringService(models.Model):
    CHOICES_CATERING_TYPE = (
        ('coffee_break', 'Кофе-брейк'),
        ('launch', 'Обед'),
        ('dinner', 'Ужин'),
    )
    time_start = models.TimeField()
    time_end = models.TimeField()
    catering_type = models.CharField(max_length=12, choices=CHOICES_CATERING_TYPE)
    project = models.ForeignKey('Project', related_name='catering_service')


class Project(models.Model):
    user = models.ForeignKey(User)
    CHOICES_STATUS = (
        ('planning', 'Планирование'),
        ('working', 'В работе'),
        ('finished', 'Завершен'),
        ('canceled', 'Отменен')
    )
    CHOICES_EXHIBITION_PROJECT = (
        ('dvp1', 'ДВП1'),
        ('dvp2', 'ДВП2')
    )
    CHOICES_FORMAT_EVENT = (
        ('exhibition', 'Выставка'),
        ('forum', 'Форум'),
        ('corporate', 'Корпоратив'),
        ('competition', 'Конкурс/чемпионат'),
        ('training', 'Тренинг/семинар'),
        ('banquet', 'Банкет'),
        ('reception', 'Фуршет'),
        ('coffee_break', 'Кофе-брейк'),
        ('supper', 'Обед-ужин'),
        ('catering', 'Кейтеринг'),
    )
    CHOICES_AREA = (
        ('1', 'Периметр'),
        ('2', 'Свободная застройка (банкет, форум)'),
        ('3', 'Стандарт'),
        ('4', 'Стандарт + доп.оборудование'),
        ('5', 'Сложная застройка (форум, чемпионат)'),
        ('6', 'Стандарт + индивидуал'),
        ('7', 'Интерактив + мультимедия')
    )
    name = models.CharField(max_length=256)
    created_date = models.DateField(auto_now_add=True, blank=True, null=True)
    date_start = models.DateField(verbose_name='Дата начала', blank=True, null=True)
    date_end = models.DateField(verbose_name='Дата окончания', blank=True, null=True)
    event_date_start = models.DateField(verbose_name='Дата начала заезда', blank=True, null=True)
    event_date_end = models.DateField(verbose_name='Дата окончания выезда', blank=True, null=True)
    subjects = models.ForeignKey(Subjects, verbose_name='Тематика проекта', blank=True, null=True,
                                 related_name='projects')
    max_discount = models.PositiveSmallIntegerField(blank=True, null=True)
    sites = models.ManyToManyField(Sites, verbose_name='Место проведения', related_name='sites', blank=True)
    project_type = models.ForeignKey(Project_type, verbose_name='Тип проекта', related_name='project_type',
                                     blank=True, null=True)
    exhibition_type = models.CharField(choices=CHOICES_EXHIBITION_PROJECT, blank=True, max_length=4)
    format_event = models.CharField(choices=CHOICES_FORMAT_EVENT, blank=True, max_length=32)
    count_guests = models.SmallIntegerField(blank=True, null=True)
    buisness_unit = models.ForeignKey(Buisness_unit, verbose_name='Бизнес-единица', blank=True, null=True)
    area = models.IntegerField('Занимаемая площадь', blank=True, null=True)
    area_size = models.CharField(choices=CHOICES_AREA, blank=True, max_length=1, null=True)
    status = models.CharField(verbose_name='Статус', choices=CHOICES_STATUS, max_length=64, default='planning',
                              blank=True)
    time_start = models.TimeField(blank=True, null=True)
    time_end = models.TimeField(blank=True, null=True)
    event_time_start = models.TimeField(verbose_name='Время начала мероприятия', blank=True, null=True)
    event_time_end= models.TimeField(verbose_name='Время окончания мероприятия', blank=True, null=True)
    time_last = models.TimeField(blank=True, null=True)
    start_building = models.DateField(blank=True, null=True)
    end_building = models.DateField(blank=True, null=True)
    client = models.ForeignKey('clients.Client', null=True, blank=True)
    address = models.CharField(max_length=1000, blank=True)
    history = HistoricalRecords()

    @property
    def default_choice(self):
        return 'planning'
    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'
        permissions = (
            ("view_project", "Can see available projects"),
        )

    def __str__(self):
        return self.name


class ProjectIndicators(models.Model):
    project = models.OneToOneField(Project, related_name='project_indicators')
    cash_receipts_exp = models.IntegerField(verbose_name='Поступление денежных средств от экспонентов',
                                            blank=True, null=True)
    count_app = models.SmallIntegerField(verbose_name='Количество заявок', blank=True, null=True)
    cash_receipts_vis = models.IntegerField(verbose_name='Поступление денежных средств от посетителей',
                                            blank=True, null=True)
    count_vis = models.IntegerField(verbose_name='Количество посетителей', blank=True, null=True)
    area_netto = models.SmallIntegerField(verbose_name='Площадь нетто', blank=True, null=True)
    history = HistoricalRecords()


class ProjectIndicatorsTypeOne(models.Model):
    project = models.OneToOneField(Project, related_name='project_indicators_type_one')
    incomes = models.IntegerField(blank=True, null=True, verbose_name='Доходы')
    costs = models.IntegerField(blank=True, null=True, verbose_name='Расходы')
    marginal_profit = models.IntegerField(blank=True, null=True, verbose_name='Маржинальная прибыль')
    profitability_project = models.IntegerField(blank=True, null=True, verbose_name='Рентабельность прокта')
    customer_satisfaction = models.IntegerField(blank=True, null=True, verbose_name='Удовлетвореннось клиента')
    history = HistoricalRecords()


class ProjectIndicatorsTypeTwo(models.Model):
    project = models.OneToOneField(Project, related_name='project_indicators_type_two')
    incomes = models.IntegerField(blank=True, null=True, verbose_name='Доходы')
    costs = models.IntegerField(blank=True, null=True, verbose_name='Расходы')
    marginal_profit = models.IntegerField(blank=True, null=True, verbose_name='Маржинальная прибыль')
    profitability_project = models.IntegerField(blank=True, null=True, verbose_name='Рентабельность проекта')
    customer_satisfaction = models.IntegerField(blank=True, null=True, verbose_name='Удовлетвореннось клиента')
    count_visitors = models.SmallIntegerField(blank=True, null=True, verbose_name='Количество посетителей')
    history = HistoricalRecords()


class ProjectReport(models.Model):
    CHOICES_APPROVAL = (
        ('on_approval', 'На утверждении'),
        ('approved_by', 'Утвержден'),
        ('return', 'Возврат на доработку')
    )
    month_name = models.CharField(max_length=8, verbose_name='Название месяца')
    month_date = models.DateField(verbose_name='Дата периода')
    project_indicators = models.ForeignKey(ProjectIndicators, verbose_name='Показатели проекта',
                                           related_name='project_report')
    fin_plan = models.IntegerField(verbose_name='Финансовый план экономист', blank=True, null=True)
    fin_plan_month = models.IntegerField(verbose_name='Финансовый план руководителя проекта', blank=True, null=True)
    app_plan = models.IntegerField(verbose_name='План поступления заявок на весь период', blank=True, null=True)
    app_plan_month = models.IntegerField(verbose_name='Ежемесячный план поступления заявок', blank=True, null=True)
    plan_processing_db = models.IntegerField(verbose_name='План обработки базы клиентов', blank=True, null=True)
    approval_status = models.CharField(max_length=12, blank=True,
                                       verbose_name='Статус утверждения', choices=CHOICES_APPROVAL)
    history = HistoricalRecords()


# План маркетинга для 3 блока 1 тип Открытые Ивент мероприятия
class ProjectReportTypeTwo(models.Model):
    month_name = models.CharField(max_length=8, verbose_name='Название месяца')
    month_date = models.DateField(verbose_name='Дата периода')
    project_indicators = models.ForeignKey(ProjectIndicatorsTypeTwo, verbose_name='Показатели проекта',
                                           related_name='project_report')
    fin_plan = models.IntegerField(verbose_name='Финансовый план экономист', blank=True, null=True)
    fin_plan_month = models.IntegerField(verbose_name='Финансовый план руководителя проекта', blank=True, null=True)
    ticket_plan = models.IntegerField(verbose_name='План покупки билетов', blank=True, null=True)
    plan_processing_db = models.IntegerField(verbose_name='План обработки базы клиентов', blank=True, null=True)
    history = HistoricalRecords()


class ProjectReportManager(models.Model):
    manager = models.ForeignKey('workers.Manager')
    project_report = models.ForeignKey(ProjectReport, verbose_name='Менеджеры на месяц',
                                           related_name='project_report_manager')
    fin_plan_month = models.IntegerField(verbose_name='Финансовый план руководителя проекта', blank=True, null=True)
    app_plan_month = models.IntegerField(verbose_name='Ежемесячный план поступления заявок', blank=True, null=True)
    plan_processing_db = models.IntegerField(verbose_name='План обработки базы клиентов', blank=True, null=True)
    history = HistoricalRecords()

    class Meta:
        unique_together = (("manager", "project_report"),)


# План маркетинга для 3 блока 1 тип Открытые Ивент мероприятия назначение менеджера
class ProjectReportTypeTwoManager(models.Model):
    manager = models.ForeignKey('workers.Manager', related_name='project_reports')
    # month_date = models.DateField(verbose_name='Дата периода')
    project_report = models.ForeignKey(ProjectReportTypeTwo, verbose_name='Менеджеры на месяц',
                                           related_name='project_report_manager')
    # fin_plan = models.IntegerField(verbose_name='Финансовый план экономист', blank=True, null=True)
    fin_plan_month = models.IntegerField(verbose_name='Финансовый план руководителя проекта', blank=True, null=True)
    # app_plan = models.IntegerField(verbose_name='План поступления заявок на весь период', blank=True, null=True)
    ticket_plan_month = models.IntegerField(verbose_name='Ежемесячный план поступления заявок', blank=True, null=True)
    plan_processing_db = models.IntegerField(verbose_name='План обработки базы клиентов', blank=True, null=True)
    history = HistoricalRecords()

    class Meta:
        unique_together = (("manager", "project_report"),)


class Approval(models.Model):
    CHOICES_STATUS_APPROVAL = (
        ('e', 'error'),
        ('s', 'success'),
        ('w', 'warning')
    )
    created_user = models.ForeignKey(User)
    created_date = models.DateTimeField(auto_now=True)
    comment = models.CharField(max_length=4096, verbose_name='Комментарий')
    project = models.ForeignKey(Project, related_name='approvals')
    report = models.ForeignKey(ProjectReport, related_name='approvals')
    status = models.CharField(max_length=1, choices=CHOICES_STATUS_APPROVAL)


# TODO Решить с таблицей или чойсом, добавить ссылку на Юр или Физ лицо
# TODO исправить слово бизнес везде
class Category(models.Model):
    name = models.CharField(max_length=64, verbose_name='Название категории')


class Activities(models.Model):
    # CHOICES_CATEGORY = (
    #     ('', ''),
    #     ('', ''),
    #     ('', ''),
    #     ('', '')
    # )
    subcategories = models.CharField(verbose_name='Подкатегория', max_length=100)
    category = models.CharField(verbose_name='Категория', max_length=32, choices=CHOICES_CATEGORY)
    # test = models.ForeignKey('Test', verbose_name='К форме назначения видов', related_name='test')

    def __str__(self):
        return self.subcategories


class Test(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название раздел')
    project = models.ForeignKey(Project, related_name='project_section')
    percent_conxept = models.IntegerField(verbose_name='% компаний по концепции (план)')
    count_comp_plan = models.IntegerField(verbose_name='Количество компаний по плану', blank=True, null=True)
    activities = models.ManyToManyField('Activities', verbose_name="Виды деятельности",
                                        blank=True, related_name='project_section')
    # parent = models.ForeignKey('self',  blank=True, null=True, related_name='children', verbose_name='Отношение к разделу')
    html = models.TextField(verbose_name='html')
    time_change = models.DateTimeField(verbose_name='Изменено', blank=True, null=True)
    change = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Раздел проекта'
        verbose_name_plural = 'Разделы проектов'
        ordering = ('percent_conxept',)

    def __str__(self):
        return self.name


class Subsection(models.Model):
    name = models.CharField(max_length=64, verbose_name='Название')
    test = models.ForeignKey(Test, related_name='subsection')


class RoomReservation(models.Model):
    CHOICES_TYPE = (
        ('1', 'Класс'),
        ('2', 'Театр'),
        ('3', 'Подкова'),
        ('4', 'Произвольная планировка')
    )
    project = models.ForeignKey(Project, verbose_name='Проект', related_name='rooms')
    site = models.ForeignKey(Sites)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    type_build = models.CharField(max_length=32, blank=True, choices=CHOICES_TYPE)
    start_building = models.DateTimeField(null=True)
    end_building = models.DateTimeField(null=True)
    day_arrival = models.DateTimeField(null=True)

    class Meta:
        verbose_name = 'Бронирование зала'
        verbose_name_plural = 'Бронирование залов'


class ReportTypes(models.Model):
    name = models.CharField(max_length=128)
    one_c_property = models.CharField(max_length=128, blank=True, null=True)
    project_type = models.ForeignKey(Project_type)

    class Meta:
        verbose_name = 'Тип отчета'
        verbose_name_plural = 'Типы отчетов'

    def __str__(self):
        return self.name


class ReportValue(models.Model):
    type_report = models.ForeignKey(ReportTypes)
    plan = models.FloatField(blank=True, null=True)
    fact = models.FloatField(blank=True, null=True)
    working_out = models.FloatField(blank=True, null=True)
    count = models.PositiveIntegerField(blank=True, null=True)
    count_people = models.PositiveIntegerField(blank=True, null=True)
    average_check = models.FloatField(blank=True, null=True)
    satisfaction = models.SmallIntegerField(blank=True, null=True)
    date = models.DateField()


class Contract(models.Model):
    subject = models.ForeignKey(Subjects, blank=True, null=True, verbose_name='Тематика мероприятия',
                                related_name='contracts')
    business_unit = models.ForeignKey(Buisness_unit, blank=True, null=True, verbose_name='Бизнес единица',
                                      related_name='contracts')
    name = models.CharField(max_length=512)

    def __str__(self):
        return self.name


class TypeContract(models.Model):
    CHOICES_FORMAT = (
        ('pdf', 'PDF'),
        ('docx', 'DOCX'),
        ('xlsx', 'XLSX')
    )
    format_file = models.CharField(choices=CHOICES_FORMAT, max_length=4)
    url_name = models.CharField(max_length=128)
    contract = models.ForeignKey(Contract, related_name='type_contracts')

# Create your models here.