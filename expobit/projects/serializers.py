from rest_framework import serializers
from .models import *
# from clients.serializers import EntitySerializer
from clients.models import *
from workers.models import *
from django.core.cache import cache
from django.db import connection
import json
from dateutil.relativedelta import relativedelta
from django.db.models import Count, Sum
import timeit
import time
from one_c import manage_data

cache.set('count', 0, None)
cache.set('validate', False, None)


class CustomDateTimeField(serializers.DateTimeField):
    def to_representation(self, value):
        tz = timezone.get_default_timezone()
        value = timezone.localtime(value, timezone=tz)
        return super().to_representation(value)


class SubSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subsection
        fields = '__all__'


class TestSerializer(serializers.ModelSerializer):
    # subsection = SubSectionSerializer(read_only=False, many=False)
    count_clients = serializers.SerializerMethodField()
    count_managers = serializers.SerializerMethodField()
    time_change = CustomDateTimeField(default=timezone.now())

    # count_percent_manager = serializers.IntegerField()

    def get_count_managers(self, project_section):
        array_id = []
        activities_obj = project_section.activities.all().values_list('id')
        array_id = [x[0] for x in activities_obj]
        # for obj in activities_obj:
        #     array_id.append(obj.id)
        query_client = Client.objects.filter(entitys__activities__in=array_id).distinct()
        counter = Membership.objects.filter(project=project_section.project_id, client__in=query_client).count()
        # if not cache.get('validate'):
        #     cache.incr('count', counter)
        # else:
        #     cache.set('validate', False, None)
        return counter

    #
    # def validate(self, attrs):
    #     cache.set('validate', True, None)
    #     return attrs

    # def get_count_percent_manager(self, project_section):
    #     all_manager = Membership.objects.filter(project=project_section.project).count()
    #     if all_manager != 0:
    #         return self.counter/(Membership.objects.filter(project=project_section.project).count()/100)
    #     else:
    #         return 0

    def get_count_clients(self, project_section):
        array_id = []
        activities_obj = project_section.activities.all().values_list('id')
        array_id = [x[0] for x in activities_obj]
        # for obj in activities_obj:
        #     array_id.append(obj.id)
        return Client.objects.filter(entitys__activities__in=array_id).distinct().count()

    class Meta:
        model = Test
        fields = '__all__'


class TestSerializerRO(serializers.ModelSerializer):
    # subsection = SubSectionSerializer(many=True)
    count_clients = serializers.SerializerMethodField()
    count_managers = serializers.SerializerMethodField()
    all_activities = serializers.SerializerMethodField()
    all_apps = serializers.SerializerMethodField()

    def get_all_activities(self, obj):
        return ', '.join(list(obj.activities.all().values_list('subcategories', flat=True)))

    # count_percent_manager = serializers.IntegerField()

    def get_count_managers(self, project_section):
        array_id = []
        activities_obj = project_section.activities.all().values_list('id', flat=True)
        # for obj in activities_obj:
        #     array_id.append(obj.id)
        array_id = list(activities_obj)
        self.__dict__['activities_list'] = array_id
        # query_client = Client.objects.filter(entitys__activities__in=array_id).distinct()
        counter = Membership.objects.filter(project=project_section.project, client__entitys__activities__in=array_id).distinct().count()
        # if not cache.get('validate'):
        #     cache.incr('count', counter)
        # else:
        #     cache.set('validate', False, None)
        return counter

    def get_all_apps(self, obj):
        return ApplicationExpo.objects.filter(
            client__entitys__activities__in=self.__dict__['activities_list'], project=obj.project).distinct().count()

    #
    # def validate(self, attrs):
    #     cache.set('validate', True, None)
    #     return attrs

    # def get_count_percent_manager(self, project_section):
    #     all_manager = Membership.objects.filter(project=project_section.project).count()
    #     if all_manager != 0:
    #         return self.counter/(Membership.objects.filter(project=project_section.project).count()/100)
    #     else:
    #         return 0

    def get_count_clients(self, project_section):
        array_id = []
        activities_obj = project_section.activities.all()
        for obj in activities_obj:
            array_id.append(obj.id)
        return Client.objects.filter(entitys__activities__in=array_id).distinct().count()

    class Meta:
        model = Test
        fields = '__all__'


class RoomReservationSerializer(serializers.ModelSerializer):
    date_start = serializers.SerializerMethodField()
    date_end = serializers.SerializerMethodField()
    start_building = serializers.SerializerMethodField()
    end_building = serializers.SerializerMethodField()
    day_arrival = serializers.SerializerMethodField()

    def get_date_start(self, obj):
        date = obj.date_start
        date = date + relativedelta(hours=3)
        return datetime.datetime.strftime(date, '%Y-%m-%dT%H:%M')

    def get_date_end(self, obj):
        date = obj.date_end
        date = date + relativedelta(hours=3)
        return datetime.datetime.strftime(date, '%Y-%m-%dT%H:%M')

    def get_start_building(self, obj):
        date = obj.start_building
        if date:
            date = date + relativedelta(hours=3)
            return datetime.datetime.strftime(date, '%Y-%m-%dT%H:%M')
        return ''

    def get_end_building(self, obj):
        date = obj.end_building
        if date:
            date = date + relativedelta(hours=3)
            return datetime.datetime.strftime(date, '%Y-%m-%dT%H:%M')
        return ''

    def get_day_arrival(self, obj):
        date = obj.day_arrival
        if date:
            date = date + relativedelta(hours=3)
            return datetime.datetime.strftime(date, '%Y-%m-%dT%H:%M')
        return ''

    class Meta:
        model = RoomReservation
        fields = '__all__'


class BuisnessUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Buisness_unit
        fields = "__all__"


class SitesSerializers(serializers.ModelSerializer):
    class Meta:
        model = Sites
        fields = "__all__"


class ProjectTypeSerializer(serializers.ModelSerializer):
    default = BuisnessUnitSerializer()

    class Meta:
        model = Project_type
        fields = "__all__"


class ProjectSerializerROForArchive(serializers.ModelSerializer):
    # project_section = serializers.SerializerMethodField()
    project_section = TestSerializer(many=True)
    project_type = ProjectTypeSerializer(read_only=True)
    sites = SitesSerializers(read_only=True, many=True)
    buisness_unit = BuisnessUnitSerializer(read_only=True)
    # all_manager_percent = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()
    #user = serializers.SerializerMethodField()

    def get_client(self, project):
        if project.client:
            return {'id': project.client.id, 'name': project.client.name}
        else:
            return None

    #def get_user(self, project):
     #   return project.user.last_name + ' ' + project.user.first_name

    # def get_all_manager_percent(self, project):
    #     count = cache.get('count')
    #     cache.set('count', 0, None)
    #     return count

    class Meta:
        model = Project
        exclude = ['user']


class ComplexitySerializer(serializers.ModelSerializer):
    class Meta:
        model = ComplexityBuild
        fields = '__all__'


class ProjectSerializerRO(serializers.ModelSerializer):
    # project_section = serializers.SerializerMethodField()
    # project_section = TestSerializer(many=True)
    rooms = RoomReservationSerializer(many=True, read_only=True)
    project_type = ProjectTypeSerializer(read_only=True)
    sites = SitesSerializers(read_only=True, many=True)
    buisness_unit = BuisnessUnitSerializer(read_only=True)
    # all_manager_percent = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField(read_only=True)
    сomplexity = serializers.SerializerMethodField()

    def get_user_id(self, obj):
        return obj.user_id

    def get_сomplexity(self, project):
        return ComplexityBuildProject.objects.filter(project=project).values()

    def get_client(self, project):
        if project.client:
            return {'id': project.client.id, 'name': project.client.name}
        else:
            return None

    def get_user(self, project):
        return project.user.last_name + ' ' + project.user.first_name

    # def get_all_manager_percent(self, project):
    #     count = cache.get('count')
    #     cache.set('count', 0, None)
    #     return count

    class Meta:
        model = Project
        fields = '__all__'
        #exclude = ['user']


class CateringSerializer(serializers.ModelSerializer):
    catering_type_display = serializers.SerializerMethodField()

    def get_catering_type_display(self, obj):
        return obj.get_catering_type_display()

    class Meta:
        model = CateringService
        fields = '__all__'


class ProjectCalendarSerializer(serializers.ModelSerializer):
    sites = SitesSerializers(read_only=True, many=True)
    project_type = ProjectTypeSerializer(read_only=True)
    buisness_unit = BuisnessUnitSerializer(read_only=True)
    rooms = RoomReservationSerializer(many=True, read_only=True)
    catering_service = CateringSerializer(many=True, read_only=True)
    user = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField()
    contract = serializers.SerializerMethodField()
    client_name = serializers.CharField()

    def get_user_id(self, obj):
        return obj.user_id

    def get_user(self, obj):
        return '%s %s' % (obj.user.last_name, obj.user.first_name)

    def get_contract(self, obj):
        if obj.project_type.type_project_type == 'kvs' and obj.client_id:
            bills = json.loads(manage_data.getSumTable(project_id=obj.id))
            contract = []
            for i in bills:
                if i:
                    contract.append({
                        'Номер':i['Номер'],
                        'СуммаCчета':i['СуммаСчета'],
                    })
            return contract
        return ''

    class Meta:
        model = Project
        fields = ['id', 'name',
                  'date_start', 'date_end',
                  'event_date_start', 'event_date_end',
                  'start_building', 'end_building',
                  'time_start', 'event_time_start',
                  'time_end', 'event_time_end',
                  'time_last', 'user',
                  'project_type', 'sites',
                  'buisness_unit',
                  'client_name',
                  'status',
                  'area_size',
                  'client',
                  'rooms',
                  'catering_service', 'user_id', 'contract']


class ProjectCalendarSerializerReport(serializers.ModelSerializer):
    sites = SitesSerializers(read_only=True, many=True)
    project_type = ProjectTypeSerializer(read_only=True)
    buisness_unit = BuisnessUnitSerializer(read_only=True)
    rooms = RoomReservationSerializer(many=True, read_only=True)
    catering_service = CateringSerializer(many=True, read_only=True)
    user = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField()
    client_name = serializers.CharField()

    def get_user_id(self, obj):
        return obj.user_id

    def get_user(self, obj):
        return '%s %s' % (obj.user.last_name, obj.user.first_name)

    class Meta:
        model = Project
        fields = ['id', 'name',
                  'date_start', 'date_end',
                  'event_date_start', 'event_date_end',
                  'start_building', 'end_building',
                  'time_start', 'event_time_start',
                  'time_end', 'event_time_end',
                  'time_last', 'user',
                  'project_type', 'sites',
                  'buisness_unit',
                  'client_name',
                  'status',
                  'area_size',
                  'client',
                  'rooms',
                  'catering_service', 'user_id',]


class ProjectSerializerROMin(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['name', 'id', 'date_start', 'date_end', 'subjects', 'count_guests']


class ProjectSerializerForTable(serializers.ModelSerializer):
    project_type = ProjectTypeSerializer(read_only=True)
    sites = SitesSerializers(read_only=True, many=True)
    buisness_unit = BuisnessUnitSerializer(read_only=True)
    user = serializers.SerializerMethodField()
    сomplexity = serializers.SerializerMethodField()

    def get_сomplexity(self, project):
        return ComplexityBuildProject.objects.filter(project=project).values()

    def get_user(self, project):
        return project.user.last_name + ' ' + project.user.first_name

    class Meta:
        model = Project
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'
        # exclude = ['user']


class ActivitiesSerializer(serializers.ModelSerializer):
    # entitys = EntitySerializer(many=True)
    class Meta:
        model = Activities
        fields = '__all__'


class SubjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subjects
        fields = '__all__'


class ProjectInClientSerializer(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField()
    contentment = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    project_type = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField()

    def get_user_id(self, obj):
        return obj.user_id

    def get_date(self, obj):
        return '%s - %s' % (obj.date_start.strftime('%d-%m-%Y'), obj.date_end.strftime('%d-%m-%Y'))

    def get_user(self, obj):
        return '%s %s' % (obj.user.last_name, obj.user.first_name)

    def get_project_type(self, obj):
        return obj.project_type.type

    def get_status(self, obj):
        return obj.get_status_display()

    def get_manager(self, obj):
        manager = obj.client.personal_manager_type_one
        if manager:
            return '%s %s' % (obj.client.personal_manager_type_one.user.last_name,
                              obj.client.personal_manager_type_one.user.first_name)
        return None

    def get_contentment(self, obj):
        return None

    class Meta:
        model = Project
        fields = ['id', 'name', 'project_type', 'status', 'manager', 'contentment', 'user', 'date', 'user_id']


# ProjectIndicators Показатели проекта сериалайзер


class ProjectIndicatorsSerializerRO(serializers.ModelSerializer):
    cash_receipts_exp = serializers.SerializerMethodField()
    count_app = serializers.SerializerMethodField()
    cash_receipts_vis = serializers.SerializerMethodField()
    count_vis = serializers.SerializerMethodField()
    average_cost = serializers.SerializerMethodField()
    area_brutto = serializers.SerializerMethodField()
    area_netto = serializers.SerializerMethodField()
    count_days = serializers.SerializerMethodField()
    count_hour = serializers.SerializerMethodField()
    size_average_stand = serializers.SerializerMethodField()
    coefficient_loading = serializers.SerializerMethodField()
    ###
    reg = serializers.SerializerMethodField()
    area = serializers.SerializerMethodField()
    equipment = serializers.SerializerMethodField()
    multimedia = serializers.SerializerMethodField()
    participation = serializers.SerializerMethodField()
    ad = serializers.SerializerMethodField()
    service = serializers.SerializerMethodField()
    accredit = serializers.SerializerMethodField()
    barter = serializers.SerializerMethodField()
    demo = serializers.SerializerMethodField()
    sponsor = serializers.SerializerMethodField()
    perc_db = serializers.SerializerMethodField()
    conv_db = serializers.SerializerMethodField()

    def get_perc_db(self, project_indicators):
        clients = Membership.objects.filter(project_id=project_indicators.project_id).values_list('client', flat=True)
        contacts = Contact.objects.filter(project_id=project_indicators.project_id,
                                          type_contact='out_phone', client__in=clients).distinct('client').count()
        self.__dict__['contacts'] = contacts
        # plan = ProjectReport.objects.filter(project_indicators=project_indicators).\
        #     annotate(Sum('plan_processing_db')).values('plan_processing_db')
        count = clients.count()
        try:
            perc = str(round((contacts / count) * 100)) + '%'
        except Exception as e:
            perc = None
        return {'name': 'Обработка базы', 'remove_zero': True, 'sort': 16,
                'value': count, 'fact': contacts,
                'percent_complete': perc}

    def get_conv_db(self, project_indicators):
        apps = ApplicationExpo.objects.filter(
            project_id=project_indicators.project_id).exclude(status='canceled').count()
        try:
            perc = str(round((apps / self.__dict__['contacts']) * 100)) + '%'
        except Exception as e:
            perc = None
        return {'name': 'Конвертация звонков', 'remove_zero': False, 'sort': 16,
                'value': None, 'fact': None,
                'percent_complete': perc}

    def get_reg(self, project_indicators):
        return {'name': 'Регистрационный взнос', 'remove_zero': True, 'sort': 2, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('РегистрационныйВзнос'),
                'percent_complete': None}

    def get_area(self, project_indicators):
        return {'name': 'Аренда площади', 'remove_zero': True, 'sort': 3, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('АрендаПлощади'),
                'percent_complete': None}

    def get_equipment(self, project_indicators):
        return {'name': 'Доп оборудование', 'remove_zero': True, 'sort': 4, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('ДопОборудование'),
                'percent_complete': None}

    def get_multimedia(self, project_indicators):
        return {'name': 'Мультимедийное оборудование', 'remove_zero': True, 'sort': 5, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('МультимедийноеОборудование'),
                'percent_complete': None}

    def get_participation(self, project_indicators):
        return {'name': 'Участие в программе мероприятия', 'remove_zero': True, 'sort': 6, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('УчастиеВПрограммеМероприятия'),
                'percent_complete': None}

    def get_ad(self, project_indicators):
        return {'name': 'Рекламные услуги', 'remove_zero': True, 'sort': 8, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('РекламныеУслуги'),
                'percent_complete': None}

    def get_service(self, project_indicators):
        return {'name': 'Серв.услуги и тех.обеспечние', 'remove_zero': True, 'sort': 7, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('СервисныеУслугиИТО'),
                'percent_complete': None}

    def get_accredit(self, project_indicators):
        return {'name': 'Аккредитация и тех.контроль', 'remove_zero': True, 'sort': 9, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('АкредитацияИТК'),
                'percent_complete': None}

    def get_barter(self, project_indicators):
        return {'name': 'Бартер', 'remove_zero': True, 'sort': 2, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('Бартер'),
                'percent_complete': None}

    def get_demo(self, project_indicators):
        return {'name': 'Демпоказы', 'remove_zero': True,'right': True,
                'value': None, 'fact': self.context.get('performance').get('Демпоказы'),
                'percent_complete': None}

    def get_sponsor(self, project_indicators):
        return {'name': 'Спонсорство', 'remove_zero': True, 'sort': 2, 'right': True,
                'value': None, 'fact': self.context.get('performance').get('Спонсорство'),
                'percent_complete': None}

    def get_cash_receipts_exp(self, project_indicators):
        one_c_data = self.context.get('one_c')
        cash_receipts_exp = project_indicators.cash_receipts_exp
        s = 0
        if one_c_data[0] != {}:
            for i in one_c_data:
                s += int(float(i['ОплаченнаяСумма']))
        self.__dict__['fact_money'] = s
        percent_complete = 0
        if cash_receipts_exp and cash_receipts_exp != 0:
            percent_complete = s / cash_receipts_exp * 100
        return {'name': project_indicators._meta.get_field('cash_receipts_exp').verbose_name,
                'value': cash_receipts_exp, 'id_field': project_indicators.id, 'sort': 1, 'bold': True,
                'field': 'cash_receipts_exp', 'fact': s,
                'percent_complete': str(round(percent_complete)) + ' %'}

    def get_count_app(self, project_indicators):
        count_app = 0
        percent_complete = None
        if project_indicators.project.project_type.default.name == 'Дирекция выставочных проектов':
            count_app = ApplicationExpo.objects.filter(project=project_indicators.project, main__isnull=True).\
                exclude(status='canceled').count()
        if project_indicators.count_app:
            percent_complete = str(round(count_app / project_indicators.count_app * 100)) + ' %'
        self.__dict__['count_app'] = count_app
        return {'name': project_indicators._meta.get_field('count_app').verbose_name,
                'value': project_indicators.count_app, 'sort': 11,
                'id_field': project_indicators.id, 'fact': count_app,
                'field': 'count_app', 'percent_complete': percent_complete}

    def get_cash_receipts_vis(self, project_indicators):
        percent_complete = 0
        if project_indicators.cash_receipts_vis and project_indicators.sum_amount_fact is not None:
            percent_complete = (project_indicators.sum_amount_fact / project_indicators.cash_receipts_vis) * 100
        return {'name': project_indicators._meta.get_field('cash_receipts_vis').verbose_name,
                'value': project_indicators.cash_receipts_vis, 'id_field': project_indicators.id,
                'field': 'cash_receipts_vis', 'sort': 10,
                'fact': project_indicators.sum_amount_fact,
                'percent_complete': str(round(percent_complete)) + ' %'}

    def get_count_vis(self, project_indicators):
        percent_complete = 0
        if project_indicators.count_vis:
            percent_complete = (project_indicators.count_vis_fact / project_indicators.count_vis) * 100
        return {'name': project_indicators._meta.get_field('count_vis').verbose_name,
                'value': project_indicators.count_vis, 'id_field': project_indicators.id,
                'field': 'count_vis',
                'sort': 12,
                'fact': project_indicators.count_vis_fact,
                'percent_complete': str(round(percent_complete)) + ' %'}

    def get_average_cost(self, project_indicators):
        average_cost = 0
        fact_average_cost = 0
        if project_indicators.cash_receipts_exp and project_indicators.count_app:
            average_cost = project_indicators.cash_receipts_exp / project_indicators.count_app
        # qs_expo = ApplicationExpo.objects.filter(project_id=project_indicators.project_id)
        # qs_ad = ApplicationAd.objects.filter(project_id=project_indicators.project_id)
        # qs_lease = ApplicationLeaseKVS.objects.filter(project_id=project_indicators.project_id)
        # s = 0
        # for i in qs_expo:
        #     for j in i.ordered_service.all():
        #         s += j.price * j.count * (1 - ((100 - j.discount_rate) / 100))
        #     if i.barter_sum and i.barter_sum != 0:
        #         s -= i.barter_sum
        # for i in qs_ad:
        #     for j in i.ordered_service.all():
        #         s += j.price * j.count * (1 - ((100 - j.discount_rate) / 100))
        #     if i.barter_sum and i.barter_sum != 0:
        #         s -= i.barter_sum
        # for i in qs_lease:
        #     for j in i.ordered_service.all():
        #         s += j.price * j.count * (1 - ((100 - j.discount_rate) / 100))
        #     if i.barter_sum and i.barter_sum != 0:
        #         s -= i.barter_sum
        one_c_data = self.context.get('one_c_bill')
        # cash_receipts_exp = project_indicators.cash_receipts_exp
        s = 0
        if one_c_data[0] != {}:
            for i in one_c_data:
                s += int(float(i['СуммаСчета']))
        # self.__dict__['fact_money'] = s
        if self.__dict__['count_app'] != 0:
            fact_average_cost = s / self.__dict__['count_app']
        percent_complete = 0
        if average_cost != 0:
            percent_complete = fact_average_cost / average_cost * 100
        return {'name': 'Средняя стоимость заявки', 'value': str(round(average_cost)),
                'id_field': project_indicators.id,
                'fact': str(round(fact_average_cost)),
                'sort': 14,
                'percent_complete': str(round(percent_complete)) + ' %'}

    def get_area_brutto(self, project_indicators):
        sites = project_indicators.project.sites.all()
        area = 0
        for i in sites:
            if i.area:
                area += int(i.area)
        percent_complete = '0 %'
        if area != 0:
            percent_complete = str(area / area * 100) + ' %'
        return {'name': 'Площадь выставки БРУТТО',
                'sort': 15,
                'value': area, 'id_field': project_indicators.id, 'fact': area,
                'percent_complete': percent_complete}

    def get_area_netto(self, project_indicators):
        places = project_indicators.project.places.all()
        s = 0
        for i in places:
            if i.area != '':
                s += float(i.area)
        area_netto = project_indicators.area_netto
        percent_complete = None
        if area_netto:
            percent_complete = str(round(s / area_netto * 100)) + ' %'
        return {'name': 'Площадь выставки НЕТТО', 'value': area_netto,
                'sort': 16,
                'id_field': project_indicators.id, 'fact': s,
                'percent_complete': percent_complete, 'field': 'area_netto'}

    def get_count_days(self, project_indicators):
        difference = project_indicators.project.date_end - project_indicators.project.date_start
        return {'name': 'Количество дней',
                'sort': 17,
                'value': difference.days + 1, 'id_field': project_indicators.id}

    def get_count_hour(self, project_indicators):
        difference_days = project_indicators.project.date_end - project_indicators.project.date_start + relativedelta(
            days=1)
        difference_clock = datetime.datetime.combine(datetime.date.today(),
                                                     project_indicators.project.time_end) - \
                           datetime.datetime.combine(datetime.date.today(), project_indicators.project.time_start)
        difference_clock_last = datetime.datetime.combine(datetime.date.today(),
                                                          project_indicators.project.time_last) - \
                                datetime.datetime.combine(datetime.date.today(), project_indicators.project.time_start)

        all_clock = (difference_days.days - 1) * int(difference_clock.seconds / 3600) + \
                    int(difference_clock_last.seconds / 3600)
        self.__dict__['all_clock'] = all_clock
        return {'name': 'Количество часов',
                'sort': 18,
                'value': all_clock, 'id_field': project_indicators.id}

    def get_size_average_stand(self, project_indicators):
        places = project_indicators.project.places.all()
        s = 0
        count = 0
        for i in places:
            if i.area != '':
                s += float(i.area)
                count += 1
        fact = None
        # if count != 0:
        #     fact = round(s / count, 2)
        # Временное решение
        if self.context.get('one_c_bill') != [{}]:
            fact = round(sum(x['Площадь'] for x in self.context.get('one_c_bill'))/len(self.context.get('one_c_bill')))
        return {'name': 'Средний размер стенда', 'value': '-', 'sort': 13,
                'id_field': project_indicators.id, 'fact': fact,
                'percent_complete': None}

    def get_coefficient_loading(self, project_indicators):
        thematic = project_indicators.project.subjects.format
        if thematic == 'B2B+B2C':
            k = 20
        elif thematic == 'B2C':
            k = 30
        elif thematic == 'B2B':
            k = 11
        else:
            return {'name': 'Коэффицент "загрузки" экспонента посетителями выставки в один час',
                    'value': None, 'id_field': project_indicators.id}
        if not project_indicators.count_app or not project_indicators.count_vis:
            return {'name': 'Коэффицент "загрузки" экспонента посетителями выставки в один час',
                    'value': None, 'id_field': project_indicators.id}
        coefficient_loading = (project_indicators.count_vis / (project_indicators.count_app *
                                                               self.__dict__['all_clock'])) * k
        return {'name': 'Коэффицент "загрузки" экспонента посетителями выставки в один час',
                'value': round(coefficient_loading), 'id_field': project_indicators.id}

    class Meta:
        model = ProjectIndicators
        # fields = '__all__'
        exclude = ('project', 'id',)


class ProjectIndicatorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicators
        fields = '__all__'


class ProjectIndicatorsTypeOneSerializerRO(serializers.ModelSerializer):
    incomes = serializers.SerializerMethodField()
    costs = serializers.SerializerMethodField()
    marginal_profit = serializers.SerializerMethodField()
    profitability_project = serializers.SerializerMethodField()
    customer_satisfaction = serializers.SerializerMethodField()

    def get_incomes(self, project_indicators):
        return {'name': project_indicators._meta.get_field('incomes').verbose_name,
                'value': project_indicators.incomes, 'id_field': project_indicators.id,
                'field': 'incomes'}

    def get_costs(self, project_indicators):
        return {'name': project_indicators._meta.get_field('costs').verbose_name,
                'value': project_indicators.costs, 'id_field': project_indicators.id,
                'field': 'costs'}

    def get_marginal_profit(self, project_indicators):
        return {'name': project_indicators._meta.get_field('marginal_profit').verbose_name,
                'value': project_indicators.marginal_profit, 'id_field': project_indicators.id,
                'field': 'marginal_profit'}

    def get_profitability_project(self, project_indicators):
        return {'name': project_indicators._meta.get_field('profitability_project').verbose_name,
                'value': project_indicators.profitability_project, 'id_field': project_indicators.id,
                'field': 'profitability_project'}

    def get_customer_satisfaction(self, project_indicators):
        return {'name': project_indicators._meta.get_field('customer_satisfaction').verbose_name,
                'value': project_indicators.customer_satisfaction, 'id_field': project_indicators.id,
                'field': 'customer_satisfaction'}

    class Meta:
        model = ProjectIndicatorsTypeOne
        exclude = ('project', 'id',)


class ProjectIndicatorsTypeOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicatorsTypeOne
        fields = '__all__'


class ProjectIndicatorsTypeTwoSerializerRO(serializers.ModelSerializer):
    incomes = serializers.SerializerMethodField()
    costs = serializers.SerializerMethodField()
    marginal_profit = serializers.SerializerMethodField()
    profitability_project = serializers.SerializerMethodField()
    customer_satisfaction = serializers.SerializerMethodField()
    count_visitors = serializers.SerializerMethodField()

    def get_count_visitors(self, project_indicators):
        return {'name': project_indicators._meta.get_field('count_visitors').verbose_name,
                'value': project_indicators.count_visitors, 'id_field': project_indicators.id,
                'field': 'count_visitors'}

    def get_incomes(self, project_indicators):
        one_c_data = self.context
        incomes = project_indicators.incomes
        s = 0
        if one_c_data.get('one_c')[0] != {}:
            for i in one_c_data.get('one_c'):
                s += int(i['ОплаченнаяСумма'])
        self.__dict__['fact_money'] = s
        percent_complete = 0
        if incomes and incomes != 0:
            percent_complete = (s / incomes) * 100
        return {'name': project_indicators._meta.get_field('incomes').verbose_name,
                'value': incomes, 'id_field': project_indicators.id,
                'field': 'incomes', 'fact': s,
                'percent_complete': str(round(percent_complete, 2)) + ' %'}

    def get_costs(self, project_indicators):
        return {'name': project_indicators._meta.get_field('costs').verbose_name,
                'value': project_indicators.costs, 'id_field': project_indicators.id,
                'field': 'costs'}

    def get_marginal_profit(self, project_indicators):
        return {'name': project_indicators._meta.get_field('marginal_profit').verbose_name,
                'value': project_indicators.marginal_profit, 'id_field': project_indicators.id,
                'field': 'marginal_profit'}

    def get_profitability_project(self, project_indicators):
        return {'name': project_indicators._meta.get_field('profitability_project').verbose_name,
                'value': project_indicators.profitability_project, 'id_field': project_indicators.id,
                'field': 'profitability_project'}

    def get_customer_satisfaction(self, project_indicators):
        return {'name': project_indicators._meta.get_field('customer_satisfaction').verbose_name,
                'value': project_indicators.customer_satisfaction, 'id_field': project_indicators.id,
                'field': 'customer_satisfaction'}

    class Meta:
        model = ProjectIndicatorsTypeTwo
        exclude = ('project', 'id',)


class ProjectIndicatorsTypeTwoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicatorsTypeTwo
        fields = '__all__'


class ProjectReportSerializer(serializers.ModelSerializer):
    fact_app = serializers.SerializerMethodField()
    app_diff = serializers.SerializerMethodField()
    perc_comp = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    perc_comp_db = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    fact_bill = serializers.SerializerMethodField()
    diff = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()
    comment_return = serializers.SerializerMethodField()
    plan_app_money = serializers.SerializerMethodField()
    project_subject = serializers.SerializerMethodField()

    def get_plan_app_money(self, obj):
        project_id = self.context.get('project_id', obj.project_indicators.project_id)
        s = PayApp.objects.filter(date__month=obj.month_date.month,
                              date__year=obj.month_date.year,
                              expo__project_id=project_id).exclude(expo__status='canceled', expo__barter=True).aggregate(Sum('sum'))
        return s.get('sum__sum') if s.get('sum__sum') else 0

    def get_comment_return(self, obj):
        if hasattr(obj, 'comment_return'):
            return obj.comment_return
        return None

    def get_fact(self, report):
        self.__dict__['fact'] = 0
        if not self.context.get('one_c'):
            return 0
        key = '%s.%s' % (report.month_date.year, report.month_date.month)
        if key in self.context['one_c']:
            self.__dict__['fact'] = sum(self.context['one_c'][key])
            return sum(self.context['one_c'][key])
        key = str(report.project_indicators.project_id)
        if key in self.context['one_c']:
            self.__dict__['fact'] = sum(self.context['one_c'][key])
            return sum(self.context['one_c'][key])
        return 0

    def get_fact_bill(self, report):
        self.__dict__['fact_bill'] = 0
        if not self.context.get('one_c_bill'):
            return 0
        key = '%s.%s' % (report.month_date.year, report.month_date.month)
        if key in self.context['one_c_bill']:
            self.__dict__['fact_bill'] = sum(self.context['one_c_bill'][key])
            return sum(self.context['one_c_bill'][key])
        key = str(report.project_indicators.project_id)
        if key in self.context['one_c_bill']:
            self.__dict__['fact_bill'] = sum(self.context['one_c_bill'][key])
            return sum(self.context['one_c_bill'][key])
        return 0

    def get_diff(self, report):
        # if report.fin_plan:
        #     return self.__dict__['fact'] - report.fin_plan
        return None

    def get_perc(self, report):
        if report.fin_plan_month:
            return str(round((self.__dict__['fact'] / report.fin_plan_month) * 100)) + ' %'
        return None

    def get_project(self, report):
        project = report.project_indicators.project
        return {'name': project.name,
                'id': project.id,
                'user_name': '%s %s' % (project.user.last_name, project.user.first_name)}

    def get_perc_comp_db(self, report):
        return str(round(self.percent_db, 2)) + ' %'

    def get_fact_processing_db(self, report):
        # if not self.__dict__.get('project_id'):
        self.__dict__['project_id'] = report.project_indicators.project_id
        project_id = self.__dict__['project_id']
        if self.context.get('arr_qs'):
            arr_qs = self.context.get('arr_qs')
        else:
            qs_client = Membership.objects.filter(project=project_id).exclude(client__status__in=['black_list', 'inactive']).values('client_id')
            arr_qs = [x['client_id'] for x in qs_client]
        # arr_qs = self.context.get('arr_qs')
        date = report.month_date
        if date.month == 12:
            date_end = datetime.date(date.year + 1, 1, date.day)
        else:
            date_end = datetime.date(date.year, date.month + 1, date.day)
        s = 0
        # TODO Тут поменял проверить
        # qs = Contact.objects.filter(project=project_id,
        #                             type_contact='out_phone',
        #                             client__in=arr_qs, date__gt=date, date__lt=date_end).values('client_id').distinct()


        qs_old = Contact.objects.filter(type_contact='out_phone',
                                    client__in=arr_qs, project=project_id,
                                    date__lt=date).values_list('client_id', flat=True).distinct()
        qs = Contact.objects.filter(type_contact='out_phone',
                                    client__in=arr_qs, project=project_id,
                                    date__gt=date, date__lt=date_end).values_list('client_id', flat=True).distinct()
        # for i in qs_client:
        #     qs = Contact.objects.filter(project=project_id,
        #                                 type_contact='out_phone',
        #                                 client=i['client_id'], date__gt=date, date__lt=date_end)
        #     if qs.exists():
        #         s += 1
        s = len(set(qs) - set(qs_old))
        self.__dict__['percent_db'] = 0
        if report.plan_processing_db:
            self.percent_db = (s / report.plan_processing_db) * 100
        return s

    def get_perc_comp(self, report):
        return str(round(self.percent, 2)) + ' %'

    def get_app_diff(self, report):
        self.__dict__['percent'] = 0
        if report.app_plan and report.app_plan_month:
            self.percent = (self.s / report.app_plan_month) * 100
            return self.s - report.app_plan
        else:
            return 0

    def get_fact_app(self, report):
        date = report.month_date

        # if date.month == 12:
        #     date_end = datetime.date(date.year + 1, 1, date.day)
        # else:
        #     date_end = datetime.date(date.year, date.month + 1, date.day)
        date_end = date + relativedelta(months=1)
        project_id = report.project_indicators.project_id
        app_expo = ApplicationExpo.objects.filter(date__gte=date, date__lt=date_end,
                                                  project=project_id, main__isnull=True).exclude(status='canceled').count()
        app_kvs = ApplicationLeaseKVS.objects.filter(date__gte=date, date__lt=date_end,
                                                     project=project_id, main__isnull=True).exclude(status='canceled').count()
        app_ad = ApplicationAd.objects.filter(date__gte=date, date__lt=date_end,
                                              project=project_id, main__isnull=True).exclude(status='canceled').count()
        self.__dict__['s'] = 0
        self.s = app_expo + app_ad + app_kvs
        return app_expo + app_ad + app_kvs

    def get_project_subject(self, obj):
        if obj.project_indicators.project.subjects:
            return obj.project_indicators.project.subjects.id
        return ''

    class Meta:
        model = ProjectReport
        fields = '__all__'


class ProjectReportTypeTwoSerializer(serializers.ModelSerializer):
    fact_money = serializers.SerializerMethodField()
    diff_money = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()
    fact_ticket = serializers.SerializerMethodField()
    diff_ticket = serializers.SerializerMethodField()
    diff_perc_ticket = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    perc_processing_db = serializers.SerializerMethodField()

    def get_project(self, report):
        project = report.project_indicators.project
        return {'name': project.name,
                'id': project.id,
                'user_name': '%s %s' % (project.user.last_name, project.user.first_name)}

    def get_perc_processing_db(self, obj):
        if obj.plan_processing_db:
            return str(round((self.count / obj.plan_processing_db) * 100, 2)) + ' %'
        return None

    def get_fact_processing_db(self, obj):
        # id_obj = obj.id
        # TODO Тут переделал, проверить, старый вариант закомментирован
        qs_project_report = obj.project_report_manager.all().values_list('id')
        project_report_id = [x[0] for x in qs_project_report]
        month = obj.month_date.month
        qs_managers = Manager.objects.filter(project_reports__in=project_report_id).values_list('id')
        managers_id = [x[0] for x in qs_managers]
        qs_clients = Client.objects.filter(visitor_manager__in=managers_id).values_list('id')
        project = obj.project_indicators.project_id
        clients_id = [x[0] for x in qs_clients]
        # qs = Contact.objects.filter(client__in=clients_id, date__month=month, type_contact='out_phone',
        #                             project=project,
        #                             manager__in=managers_id)
        qs = Contact.objects.filter(client__in=clients_id, date__month=month, type_contact='out_phone',
                                    project=project,
                                    manager__in=managers_id).values('client_id').distinct()
        count = 0
        # for i in qs:
        #     c = i.client_id
        #     if c in clients_id:
        #         count += 1
        #         clients_id.remove(c)
        count += len(qs)
        # Еще дописать проверку на Физ лица
        qs_individuals = Individual.objects.filter(visitor_manager__in=managers_id).values_list('id')
        individuals_id = [x[0] for x in qs_individuals]
        # qs_ind = IndividualContact.objects.filter(individual__in=individuals_id, date__month=month,
        #                                           type_contact='out_phone', project=project, manager__in=managers_id)
        qs_ind = IndividualContact.objects.filter(individual__in=individuals_id, date__month=month,
                                                  type_contact='out_phone',
                                                  project=project,
                                                  manager__in=managers_id).values('individual_id').distinct()
        count += len(qs_ind)
        # for i in qs_ind:
        #     c = i.individual_id
        #     if c in individuals_id:
        #         count += 1
        #         individuals_id.remove(c)
        self.__dict__['count'] = count
        return count

        # return 0

    def diff_perc_ticket(self, obj):
        return 0

    def get_diff_ticket(self, obj):
        return 0

    def get_fact_ticket(self, obj):
        if not self.context.get('first_month'):
            return 0
        with connection.cursor() as cursor:
            cursor.execute("""SELECT COUNT(*)
                    FROM "TICKETS_Tickets", "PROJECT_projects", "TICKETS_Order"
                    WHERE "TICKETS_Tickets".order_id="TICKETS_Order".id
                    AND "TICKETS_Order".project_id="PROJECT_projects".id
                    AND "PROJECT_projects".p_expobit=%s 
                    AND "TICKETS_Tickets".date_create::date >= %s 
                    AND "TICKETS_Tickets".date_create::date <= %s 
                    AND "TICKETS_Tickets".status=true""", [str(obj.project_indicators.project_id),
                                                           self.context.get('first_month'),
                                                           self.context.get('last_month')])
            result = cursor.fetchone()
        if result:
            return result[0]
        return 0

    def get_perc(self, report):
        if report.fin_plan_month:
            return str(round((self.__dict__['fact'] / report.fin_plan_month) * 100)) + ' %'
        return None

    def get_diff_money(self, obj):
        return 0

    def get_fact_money(self, report):
        self.__dict__['fact'] = 0
        if not self.context.get('one_c'):
            return 0
        key = '%s.%s' % (report.month_date.year, report.month_date.month)
        if key in self.context['one_c']:
            self.__dict__['fact'] = sum(self.context['one_c'][key])
            return sum(self.context['one_c'][key])
        key = str(report.project_indicators.project_id)
        if key in self.context['one_c']:
            self.__dict__['fact'] = sum(self.context['one_c'][key])
            return sum(self.context['one_c'][key])
        return 0

    class Meta:
        model = ProjectReportTypeTwo
        fields = '__all__'


class ProjectReportManagerSerializerRO(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField()
    fact_app = serializers.SerializerMethodField()
    perc_comp = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    perc_comp_db = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()
    plan_app_money = serializers.SerializerMethodField()

    def get_plan_app_money(self, obj):
        project_id = self.__dict__['project_id']
        s = PayApp.objects.filter(date__month=obj.project_report.month_date.month,
                                  date__year=obj.project_report.month_date.year,
                                  expo__project_id=project_id,
                                  expo__manager_id=obj.manager_id).aggregate(Sum('sum'))
        return s.get('sum__sum') if s.get('sum__sum') else 0

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.manager_id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        return 0

    def get_perc(self, report):
        if report.fin_plan_month:
            return str(round((self.__dict__['fact'] / report.fin_plan_month) * 100)) + ' %'
        return None

    def get_perc_comp_db(self, obj):
        return str(round(self.percent_db, 2)) + ' %'

    # def get_fact_processing_db(self, obj):
    #     # start = time.time()
    #     if not self.__dict__.get('project_id'):
    #         self.__dict__['project_id'] = obj.project_report.project_indicators.project_id
    #     project_id = self.__dict__['project_id']
    #     qs_client = Membership.objects.filter(project=project_id,
    #                                           manager=obj.manager_id).values_list('client_id')
    #     clients_id = [x[0] for x in qs_client]
    #     date = obj.project_report.month_date
    #     if date.month == 12:
    #         date_end = datetime.date(date.year + 1, 1, date.day)
    #     else:
    #         date_end = datetime.date(date.year, date.month + 1, date.day)
    #     qs = Contact.objects.filter(project=project_id,
    #                                 manager=obj.manager_id, type_contact='out_phone',
    #                                 client__in=clients_id, date__gt=date,
    #                                 date__lt=date_end).values('client_id').distinct()
    #     s = len(qs)
    #     self.__dict__['percent_db'] = 0
    #     if obj.plan_processing_db:
    #         self.percent_db = (s / obj.plan_processing_db) * 100
    #     return s

    def get_fact_processing_db(self, report):
        self.__dict__['project_id'] = report.project_report.project_indicators.project_id
        project_id = self.__dict__['project_id']
        if self.context.get('arr_qs'):
            arr_qs = self.context.get('arr_qs')
        else:
            qs_client = Membership.objects.filter(manager=report.manager_id,
                                                  project=project_id).exclude(
                client__status__in=['black_list', 'inactive']).values('client_id')
            arr_qs = [x['client_id'] for x in qs_client]
        date = report.project_report.month_date
        if date.month == 12:
            date_end = datetime.date(date.year + 1, 1, date.day)
        else:
            date_end = datetime.date(date.year, date.month + 1, date.day)
        s = 0
        # TODO Обдумать по фильтрации
        qs_old = Contact.objects.filter(type_contact='out_phone',
                                        client__in=arr_qs, project=project_id,
                                        # manager=report.manager_id,
                                        date__lt=date).values_list('client_id', flat=True).distinct()
        qs = Contact.objects.filter(type_contact='out_phone',
                                    # manager=report.manager_id,
                                    client__in=arr_qs, project=project_id,
                                    date__gt=date, date__lt=date_end).values_list('client_id', flat=True).distinct()
        s = len(set(qs) - set(qs_old))
        self.__dict__['percent_db'] = 0
        if report.plan_processing_db:
            self.percent_db = (s / report.plan_processing_db) * 100
        return s

    def get_perc_comp(self, obj):
        if obj.app_plan_month != 0:
            return str(round((self.s / obj.app_plan_month) * 100, 2)) + ' %'
        return None

    def get_fact_app(self, obj):
        date = obj.project_report.month_date
        if date.month == 12:
            date_end = datetime.date(date.year + 1, 1, date.day)
        else:
            date_end = datetime.date(date.year, date.month + 1, date.day)
        app_expo = ApplicationExpo.objects.filter(date__gt=date, date__lt=date_end,
                                                  project=obj.project_report.project_indicators.project,
                                                  manager=obj.manager, main__isnull=True).exclude(status='canceled').count()
        app_kvs = ApplicationLeaseKVS.objects.filter(date__gt=date, date__lt=date_end,
                                                     project=obj.project_report.project_indicators.project,
                                                     manager=obj.manager, main__isnull=True).exclude(status='canceled').count()
        app_ad = ApplicationAd.objects.filter(date__gt=date, date__lt=date_end,
                                              project=obj.project_report.project_indicators.project,
                                              manager=obj.manager, main__isnull=True).exclude(status='canceled').count()
        self.__dict__['s'] = 0
        self.s = app_expo + app_ad + app_kvs
        return app_expo + app_ad + app_kvs

    def get_manager(self, obj):
        self.__dict__['count_client_manager'] = 0
        return {'manager': '%s %s' % (obj.manager.user.last_name, obj.manager.user.first_name),
                'id': obj.manager.id}

    class Meta:
        model = ProjectReportManager
        fields = '__all__'


# Для отчетов по всем ДВП


class ProjectReportManagerFullSerializerRO(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField()
    fact_app = serializers.SerializerMethodField()
    perc_comp = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    perc_comp_db = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()
    fin_plan_month_sum = serializers.SerializerMethodField()
    app_plan_month_sum = serializers.SerializerMethodField()
    plan_processing_db_sum = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.get('manager'))
        one_c_context = self.context.get('one_c_context')
        if key in one_c_context:
            self.__dict__['fact'] = sum(one_c_context[key])
            return sum(one_c_context[key])
        return 0

    def get_perc(self, report):
        if report.get('fin_plan_month_sum'):
            return str(round((self.__dict__['fact'] / report.get('fin_plan_month_sum')) * 100)) + ' %'
        return None

    def get_perc_comp_db(self, obj):
        return str(round(self.percent_db, 2)) + ' %'

    def get_fact_processing_db(self, obj):
        project_id = self.context.get('projects_id')
        qs_client = Membership.objects.filter(project__in=project_id,
                                              manager=obj.get('manager')).distinct('client').values_list('client_id')
        clients_id = [x[0] for x in qs_client]
        date = obj.get('project_report__month_date')
        if date.month == 12:
            date_end = datetime.date(date.year + 1, 1, date.day)
        else:
            date_end = datetime.date(date.year, date.month + 1, date.day)
        qs = Contact.objects.filter(project__in=project_id,
                                    manager=obj.get('manager'), type_contact='out_phone',
                                    client__in=clients_id, date__gt=date,
                                    date__lt=date_end).values('client_id').distinct()
        s = len(qs)
        self.__dict__['percent_db'] = 0
        if obj.get('plan_processing_db_sum'):
            self.percent_db = (s / obj.get('plan_processing_db_sum')) * 100
        return s

    def get_perc_comp(self, obj):
        if obj.get('app_plan_month_sum') != 0:
            return str(round((self.s / obj.get('app_plan_month_sum')) * 100, 2)) + ' %'
        return None

    def get_fact_app(self, obj):
        date = obj.get('project_report__month_date')
        if date.month == 12:
            date_end = datetime.date(date.year + 1, 1, date.day)
        else:
            date_end = datetime.date(date.year, date.month + 1, date.day)
        app_expo = ApplicationExpo.objects.filter(date__gt=date, date__lt=date_end,
                                                  project__in=self.context.get('projects_id'),
                                                  manager=obj.get('manager'), main__isnull=True).exclude(status='canceled').count()
        app_kvs = ApplicationLeaseKVS.objects.filter(date__gt=date, date__lt=date_end,
                                                     project__in=self.context.get('projects_id'),
                                                     manager=obj.get('manager'), main__isnull=True).exclude(status='canceled').count()
        app_ad = ApplicationAd.objects.filter(date__gt=date, date__lt=date_end,
                                              project__in=self.context.get('projects_id'),
                                              manager=obj.get('manager'), main__isnull=True).exclude(status='canceled').count()
        self.__dict__['s'] = 0
        self.s = app_expo + app_ad + app_kvs
        return app_expo + app_ad + app_kvs

    def get_manager(self, obj):
        self.__dict__['count_client_manager'] = 0
        return {'manager': '%s %s' % (obj.get('manager__user__last_name'),
                                      obj.get('manager__user__first_name')),
                'id': obj.get('manager')}

    def get_fin_plan_month_sum(self, obj):
        return obj.get('fin_plan_month_sum')

    def get_app_plan_month_sum(self, obj):
        return obj.get('app_plan_month_sum')

    def get_plan_processing_db_sum(self, obj):
        return obj.get('plan_processing_db_sum')

    class Meta:
        model = ProjectReportManager
        fields = ['manager', 'fact_app', 'perc_comp', 'fact_processing_db', 'perc_comp_db', 'fact', 'perc',
                  'plan_processing_db_sum', 'app_plan_month_sum', 'fin_plan_month_sum']


class ProjectReportManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectReportManager
        fields = '__all__'


class ProjectReportTypeTwoManagerSerializerRO(serializers.ModelSerializer):
    manager = serializers.SerializerMethodField()
    fact_processing_db = serializers.SerializerMethodField()
    perc_proccessing_db = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    perc = serializers.SerializerMethodField()

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.manager_id)
        if key in self.context:
            self.__dict__['fact'] = sum(self.context[key])
            return sum(self.context[key])
        return 0

    def get_perc(self, report):
        if report.fin_plan_month:
            return str(round((self.__dict__['fact'] / report.fin_plan_month) * 100)) + ' %'
        return None

    def get_fact_processing_db(self, obj):
        # start = time.time()
        month = obj.project_report.month_date.month
        manager = obj.manager_id
        qs_clients = Client.objects.filter(visitor_manager=manager).values_list('id')
        if not self.__dict__.get('project_id'):
            self.__dict__['project_id'] = obj.project_report.project_indicators.project_id
        project = self.__dict__['project_id']
        # project = obj.project_report.project_indicators.project_id
        clients_id = [x[0] for x in qs_clients]
        qs = Contact.objects.filter(client__in=clients_id, date__month=month, type_contact='out_phone',
                                    project=project,
                                    manager=manager).values('client_id').distinct()
        count = 0
        # for i in qs:
        #     c = i.client_id
        #     if c in clients_id:
        #         count += 1
        #         clients_id.remove(c)
        count += len(qs)
        # Еще дописать проверку на Физ лица
        qs_individuals = Individual.objects.filter(visitor_manager=manager).values_list('id')
        individuals_id = [x[0] for x in qs_individuals]
        qs_ind = IndividualContact.objects.filter(individual__in=individuals_id, date__month=month,
                                                  type_contact='out_phone',
                                                  project=project, manager=manager).values('individual_id').distinct()
        # for i in qs_ind:
        #     c = i.individual_id
        #     if c in individuals_id:
        #         count += 1
        #         individuals_id.remove(c)
        count += len(qs_ind)
        self.__dict__['count'] = count
        return count

    def get_perc_proccessing_db(self, obj):
        if obj.plan_processing_db:
            return str(round((self.count / obj.plan_processing_db) * 100, 2)) + ' %'
        return None

    def get_manager(self, obj):
        return {'manager': '%s %s' % (obj.manager.user.last_name, obj.manager.user.first_name),
                'id': obj.manager_id}

    class Meta:
        model = ProjectReportTypeTwoManager
        fields = '__all__'


class ProjectReportTypeTwoManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectReportTypeTwoManager
        fields = '__all__'


class ApprovalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Approval
        fields = '__all__'


class ProjectTypeReportSerializer(serializers.ModelSerializer):
    director_name = serializers.SerializerMethodField()
    fin_plan = serializers.SerializerMethodField()
    fin_plan_month = serializers.SerializerMethodField()
    plan_app_money = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    percent = serializers.SerializerMethodField()
    #diff = serializers.SerializerMethodField()
    manager_detail = serializers.SerializerMethodField()
    #plan_app_money = serializers.SerializerMethodField()

    def get_manager_report(self, obj, manager):
        detail_dict = {}
        filter_dict = {
            'date__month': self.context.get('date').month,
            'date__year': self.context.get('date').year
        }
        if obj.type_project_type == 'expo':
            filter_dict['expo__project__project_type'] = obj
            filter_dict['expo__manager_id'] = manager
        elif obj.type_project_type == 'kvs':
            filter_dict['lease_kvs__project__project_type'] = obj
            filter_dict['lease_kvs__manager_id'] = manager
        elif obj.type_project_type == 'ad':
            filter_dict['ad__project__project_type'] = obj
            filter_dict['ad__manager_id'] = manager
        s = PayApp.objects.filter(**filter_dict).aggregate(Sum('sum'))
        detail_dict['plan_money'] = round(s.get('sum__sum', 0) if s.get('sum__sum', 0) else 0, 2)
        if obj.type_project_type == 'expo':
            s = ProjectReportManager.objects.filter(project_report__project_indicators__project__project_type=obj,
                                                    manager_id=manager,
                                                    project_report__month_date=self.context.get('date')).\
                aggregate(Sum('fin_plan_month'), Sum('app_plan_month'), Sum('plan_processing_db'))
            detail_dict['plan_app'] = s.get('app_plan_month__sum')
            detail_dict['fact_app'] = ApplicationExpo.objects.filter(manager_id=manager,
                                                                     project__project_type=obj,
                                                                     date__year=self.context['date'].year,
                                                                     date__month=self.context['date'].month,
                                                                     main__isnull=True).\
                exclude(status='canceled').count()
            detail_dict['plan_processing_db'] = s.get('plan_processing_db__sum')
        elif obj.type_project_type == 'kvs':
            s = ManagersPlanTypeOne.objects.filter(fin_plan__head_id=obj.director_id,
                                                   manager_id=manager,
                                                   fin_plan__month=self.context.get('date')).\
                aggregate(Sum('month_plan'), Sum('plan_app_count'), Sum('plan_processing_db'))
            detail_dict['plan_app'] = s.get('plan_app_count__sum')
            detail_dict['fact_app'] = ApplicationLeaseKVS.objects.filter(manager_id=manager,
                                                                     project__project_type=obj,
                                                                         date__year=self.context['date'].year,
                                                                         date__month=self.context['date'].month,
                                                                     main__isnull=True). \
                exclude(status='canceled').count()
            detail_dict['plan_processing_db'] = s.get('plan_processing_db__sum')
        elif obj.type_project_type == 'ad':
            s = ProjectReportTypeTwoManager.objects.filter(project_report__project_indicators__project__project_type=obj,
                                                           manager_id=manager,
                                                           project_report__month_date=self.context.get('date')).\
                aggregate(Sum('fin_plan_month'), Sum('plan_processing_db'))
            detail_dict['fact_app'] = ApplicationAd.objects.filter(manager_id=manager,
                                                                         project__project_type=obj,
                                                                         date__year=self.context['date'].year,
                                                                         date__month=self.context['date'].month,
                                                                         main__isnull=True). \
                exclude(status='canceled').count()
            detail_dict['plan_app'] = s.get('plan_app_count__sum', 0)
            detail_dict['plan_processing_db'] = s.get('plan_processing_db__sum')
        try:
            detail_dict['percent_app'] = round(detail_dict['fact_app'] / detail_dict['plan_app'] * 100, 2)
        except:
            detail_dict['percent_app'] = None
        detail_dict['fin_plan_month'] = s.get('fin_plan_month__sum', s.get('month_plan__sum', 0))
        detail_dict['fact'] = 0
        key = str(manager)
        if key in self.context['manager_money_dict']:
            s = sum(self.context['manager_money_dict'][key])
            detail_dict['fact'] = s
        try:
            detail_dict['percent'] = round(detail_dict['fact'] / detail_dict['fin_plan_month'] * 100, 2)
        except:
            detail_dict['percent'] = None
        try:
            user = User.objects.get(managers=manager)
            detail_dict['name'] = '%s %s' % (user.last_name, user.first_name)
        except:
            detail_dict['name'] = None
        detail_dict['id'] = int(manager)
        return detail_dict


    def get_manager_detail(self, obj):
        sub_dict = self.context.get('sub_dir_dict')
        managers = [x for x in sub_dict if sub_dict[x] == str(obj.director_id)]
        return [self.get_manager_report(obj, x) for x in managers]


    def get_percent(self, obj):
        try:
            return round(self.__dict__['fact']/self.__dict__['fin_plan_month']*100, 2)
        except Exception as e:
            return None

    def get_plan_app_money(self, obj):
        filter_dict = {
            'date__month': self.context.get('date').month,
            'date__year': self.context.get('date').year
        }
        if obj.type_project_type == 'expo':
            filter_dict['expo__project__project_type'] = obj
        elif obj.type_project_type == 'kvs':
            filter_dict['lease_kvs__project__project_type'] = obj
        elif obj.type_project_type == 'ad':
            filter_dict['ad__project__project_type'] = obj
        s = PayApp.objects.filter(**filter_dict).aggregate(Sum('sum'))
        return round(s.get('sum__sum'), 2) if s.get('sum__sum') else 0

    def get_fact(self, obj):
        self.__dict__['fact'] = 0
        key = str(obj.id)
        if key in self.context['sub_money_dict']:
            self.__dict__['fact'] = sum(self.context['sub_money_dict'][key])
            return sum(self.context['sub_money_dict'][key])
        return 0

    def get_fin_plan(self, obj):
        if obj.type_project_type == 'expo':
            s = ProjectReport.objects.filter(project_indicators__project__project_type=obj,
                                             month_date=self.context.get('date')).exclude(
                project_indicators__project__status__in=['canceled', 'planning']).aggregate(Sum('fin_plan'))
        elif obj.type_project_type == 'kvs':
            s = FinPlanTypeOne.objects.filter(head_id=obj.director_id, month=self.context.get('date')).aggregate(Sum('fin_plan'))
        elif obj.type_project_type == 'ad':
            s = ProjectReportTypeTwo.objects.filter(project_indicators__project__project_type=obj,
                                             month_date=self.context.get('date')).exclude(
                project_indicators__project__status__in=['canceled', 'planning']).aggregate(Sum('fin_plan'))
        return s.get('fin_plan__sum', 0)

    def get_fin_plan_month(self, obj):
        if obj.type_project_type == 'expo':
            s = ProjectReport.objects.filter(project_indicators__project__project_type=obj,
                                             month_date=self.context.get('date')).exclude(
                project_indicators__project__status__in=['canceled', 'planning']).aggregate(Sum('fin_plan_month'))
        elif obj.type_project_type == 'kvs':
            s = FinPlanTypeOne.objects.filter(head_id=obj.director_id, month=self.context.get('date')).aggregate(Sum('month_plan'))
        elif obj.type_project_type == 'ad':
            s = ProjectReportTypeTwo.objects.filter(project_indicators__project__project_type=obj,
                                             month_date=self.context.get('date')).exclude(
                project_indicators__project__status__in=['canceled', 'planning']).aggregate(Sum('fin_plan_month'))
        self.__dict__['fin_plan_month'] = s.get('sum__fin_plan_month') if s.get('sum__fin_plan_month') else s.get('sum__month_plan', 0)
        return s.get('fin_plan_month__sum') if s.get('fin_plan_month__sum') else s.get('month_plan__sum', 0)

    def get_director_name(self, obj):
        user = obj.director
        return '%s %s' % (user.last_name, user.first_name)

    class Meta:
        model = Project_type
        fields = '__all__'


class ReportValuesSerializer(serializers.ModelSerializer):
    name = serializers.CharField(read_only=True)
    perc = serializers.SerializerMethodField()

    def get_perc(self, obj):
        try:
            return '{value} %'.format(value=str(round((obj.fact/obj.plan) * 100, 2)))
        except:
            return None

    class Meta:
        model = ReportValue
        fields = '__all__'

