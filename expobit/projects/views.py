import math

from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from rest_framework import viewsets, filters
from rest_framework.permissions import *
from rest_framework.response import Response
from django.db.models import Sum
# from  rest_framework.renderers import JSONRenderer
from dateutil.relativedelta import relativedelta
from rest_framework.decorators import list_route
from django_filters.rest_framework import DjangoFilterBackend
from .models import *
from .serializers import *
from django.conf import settings
from .metadata import *
from .permissions import DjangoModelPermissionsOrNotView
from django.db.models import Q, F
from restExt.viewsets import ModelViewSetClientCascadeDestroy
from django.utils import timezone
from archive.models import *
from one_c.models import OneCSession
from one_c import manage_data
from .filters import ProjectFilter
from collections import OrderedDict
import requests
import uuid
import calendar
from .process_data import set_or_create_project_report_type_two, \
    get_serialized_data_project_report_type_two, \
    get_serialized_data_project_report_managers_type_two

MONTH_NAME = ['Январь', 'Февраль', 'Март',
              'Апрель', 'Май', 'Июнь',
              'Июль', 'Август', 'Сентябрь',
              'Октябрь', 'Ноябрь', 'Декабрь']

# BUILDING_DAYS = {'1': {'dismantling': 1, 'building': 1},
#                  '2': {'dismantling': 2, 'building': 2},
#                  '3': {'dismantling': 3, 'building': 3},
#                  '4': {'dismantling': 4, 'building': 4},
#                  '5': {'dismantling': 5, 'building': 6},
#                  '6': {'dismantling': 6, 'building': 7}}


BUILDING_DAYS = {'1': 0.4,
                 '2': 0.75,
                 '3': 1,
                 '4': 1.2,
                 '5': 2,
                 '6': 2,
                 '7': 4}


def update_session():
    if not cache.get('update_session'+settings.MEMCACHED_NAME):
        cache.set('update_session'+settings.MEMCACHED_NAME, True, None)
        rq = requests.get("http://%s/hs/Expobit/Empty" % (settings.ONE_C_URL,), auth=settings.ONE_C_AUTH,
                          headers={'IBSession': 'start'})
        if rq.status_code == 200:
            qs = OneCSession.objects.create(session_key=rq.cookies.get('ibsession'))
            cache.set('one_c_session_key'+settings.MEMCACHED_NAME, rq.cookies.get('ibsession'), None)
            cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
        else:
            cache.set('update_session'+settings.MEMCACHED_NAME, False, None)
    else:
        while cache.get('update_session'+settings.MEMCACHED_NAME):
            time.sleep(0.01)
        return 200
    return rq.status_code


def get_inc(qs):
    s = 0
    for i in qs:
        if i.area:
            s += int(i.area)
    return s


class ApiSetComplexityView(viewsets.ModelViewSet):
    serializer_class = ComplexitySerializer
    queryset = ComplexityBuild.objects.all()
    permission_classes = (DjangoModelPermissions, )


class ApiSetProjectView(viewsets.ModelViewSet):
    @list_route(methods=['get'])
    def count(self, request):
        Project_count = Project.objects.count()
        content = {'count': Project_count}
        return JsonResponse(content)

    @list_route(methods=['patch'])
    def set_status(self, request):
        if request.user.is_superuser:
            project_id = request.data.get('id')
            Project.objects.filter(id=project_id).update(status='working')
            project = Project.objects.get(pk=project_id)
            date = '%s-%s' % (str(project.date_start.year), str(project.date_start.month))
            message = 'Обратите внимание! Согласованы сжатые сроки застройки ' \
                      '(<a href="#calendar/%s/">%s</a>)' % (date, project.name,)
            Channel('send_notification').send({
                'users': str(project.user_id),
                'message': message,
                'block_working': None
            })
            return Response(status=200, data={'status': 'working'})
        return Response(status=403, data={'permissions_error': 'not permission'})

    @list_route(methods=['patch'])
    def send_noty(self, request):
        if request.user.is_superuser:
            project_id = request.data.get('id')
            project = Project.objects.get(pk=project_id)
            send_user = User.objects.filter(groups__name='executive_director')
            date = '%s-%s' % (str(project.date_start.year), str(project.date_start.month))
            message = 'Согласование сроков конкурентных проектов по дате проведения' \
                      '(<a href="#calendar/%s/">%s</a>)' % (date, project.name,)
            Channel('send_notification').send({
                'users': str(send_user[0].id),
                'message': message,
                'block_working': None
            })
            return Response(status=200, data={'status': 'working'})
        return Response(status=403, data={'permissions_error': 'not permission'})

    @list_route(methods=['post'])
    def extend(self, request):
        name = request.data.get('name')
        project_id = request.data.get('id')
        qs = Project.objects.get(id=project_id)
        qs.name = name
        qs.pk = None
        qs.save()
        qsMs = Membership.objects.filter(project=project_id)
        qsT = Test.objects.filter(project=project_id)
        qsAct = Activities.objects.none()
        new_qsMs = []
        new_qsT = []
        for i in qsT:
            qsAct = i.activities.all()
            i.project = qs
            i.pk = None
            i.save()
            i.activities.add(*qsAct)

        for i in qsMs:
            i.project = qs
            i.pk = None
            new_qsMs.append(i)
        Membership.objects.bulk_create(new_qsMs)
        NewContact.objects.filter(project_id=project_id, date_new_contact__gt=qs.date_end).update(project=qs)
        return Response(data=ProjectSerializerRO(instance=qs).data, content_type='application/json')

    permission_classes = (DjangoModelPermissionsOrNotView,)
    queryset = Project.objects.all()
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    search_fields = ('name', 'project_type__type')
    ordering = ('-created_date',)
    ordering_fields = ('name', 'buisness_unit__name', 'project_type__type', 'date_start', 'date_end', 'sites__name',
                       'user__last_name', 'status', 'created_date', 'count_guests')
    filter_class = ProjectFilter
    filter_fields = {
        'date_start': ['gte', 'lt', 'contains'],
    }

    # metadata_class = MinimalMetadata

    def get_queryset(self):
        if self.request.GET.get('calendar'):
            return Project.objects.all().annotate(client_name=F('client__name')).prefetch_related('sites', 'project_type', 'buisness_unit', 'user',
                                                          'catering_service')
        return Project.objects.all()

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            if self.request.GET.get('data', None) == 'min':
                return ProjectSerializerROMin
            elif 'pk' in self.kwargs:
                return ProjectSerializerRO
            elif self.request.GET.get('calendar'):
                return ProjectCalendarSerializer
            else:
                return ProjectSerializerForTable
        else:
            return ProjectSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        ProjectIndicators.objects.create(project=serializer.instance)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if self.__dict__.get('err_400'):
            return Response(status=400, data={'alert': 'Сохранение невозможно, неверные даты застройки'})
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        data = serializer.data
        data['alert'] = self.__dict__['alert_error']

        return Response(data)

    def check_sites(self, sites_list):
        for i in sites_list:
            if i.building_time:
                return True
        return False

    def send_notification(self, project):
        message = '<p>Внимание: на даты Вашего проекта претендует другой Проект<p><a href="#calendar/%s/">%s</a>' % (
                '%s-%s' % (str(project.date_start.year), str(project.date_start.month)),
                project.name
            )
        if not Notification.objects.filter(message=message, user_id=project.user_id).exists() \
                and project.status == 'planning':
            Channel('send_notification').send({
                'users': str(project.user_id),
                'message': message,
                'block_working': None
            })

    def perform_update(self, serializer):
        flag = False
        complexity_dict = {}
        complexity_id = []
        for key, value in self.request.data.items():
            if key.startswith('area_id')  and value:
                if not complexity_dict.get(key.split('_')[2]+'_'+key.split('_')[3]):
                    complexity_dict[key.split('_')[2]+'_'+key.split('_')[3]] = {'type_build': value}
                else:
                    complexity_dict[key.split('_')[2]+'_'+key.split('_')[3]]['type_build'] = value


            if key.startswith('date_start_')  and value:
                if not complexity_dict.get(key.split('_')[3]+'_'+key.split('_')[4]):
                    complexity_dict[key.split('_')[3]+'_'+key.split('_')[4]] = {'date_start': value}
                else:
                    complexity_dict[key.split('_')[3]+'_'+key.split('_')[4]]['date_start'] = value

                if value not in complexity_id:
                    complexity_id.append(value)
            if key.startswith('date_end_')  and value:

                if not complexity_dict.get(key.split('_')[3]+'_'+key.split('_')[4]):
                    complexity_dict[key.split('_')[3]+'_'+key.split('_')[4]] = {'date_end': value}
                else:
                    complexity_dict[key.split('_')[3]+'_'+key.split('_')[4]]['date_end'] = value

                if value not in complexity_id:
                    complexity_id.append(value)

            if key.startswith('start_building_')  and value:

                if not complexity_dict.get(key.split('_')[3] + '_' + key.split('_')[4]):
                    complexity_dict[key.split('_')[3] + '_' + key.split('_')[4]] = {'start_building': value}
                else:
                    complexity_dict[key.split('_')[3] + '_' + key.split('_')[4]]['start_building'] = value

                if value not in complexity_id:
                    complexity_id.append(value)

            if key.startswith('end_building_') and value:

                if not complexity_dict.get(key.split('_')[3] + '_' + key.split('_')[4]):
                    complexity_dict[key.split('_')[3] + '_' + key.split('_')[4]] = {'end_building': value}
                else:
                    complexity_dict[key.split('_')[3] + '_' + key.split('_')[4]]['end_building'] = value

                if value not in complexity_id:
                    complexity_id.append(value)

            if key.startswith('day_arrival_') and value:

                if not complexity_dict.get(key.split('_')[3] + '_' + key.split('_')[4]):
                    complexity_dict[key.split('_')[3] + '_' + key.split('_')[4]] = {'day_arrival': value}
                else:
                    complexity_dict[key.split('_')[3] + '_' + key.split('_')[4]]['day_arrival'] = value

                if value not in complexity_id:
                    complexity_id.append(value)

        complexity_dict_temp = {}
        for key in complexity_dict:
            key_i = key.split('_')[0]
            if not key_i in complexity_dict_temp:
                complexity_dict_temp[key_i] = []

            complexity_dict_temp[key_i].append(complexity_dict[key])

        complexity_dict = complexity_dict_temp

        if serializer.validated_data.get('date_start') and serializer.validated_data.get('date_end') \
                and serializer.validated_data.get('area_size') and \
                self.check_sites(serializer.validated_data.get('sites')):
                # serializer.instance.sites.filter(building_time=True).exists():
            building_date = {}
            if serializer.validated_data.get('date_start') != serializer.instance.date_start \
                    or not serializer.instance.start_building or \
                            serializer.instance.area_size != serializer.validated_data.get('area_size'):
                building_date['start_building'] = serializer.validated_data.get('date_start') - relativedelta(
                    days=round(BUILDING_DAYS[serializer.validated_data.get('area_size')] * get_inc(
                        serializer.validated_data.get('sites')) * 0.0007))
            else:
                building_date['start_building'] = serializer.instance.start_building
            if serializer.validated_data.get('date_end') != serializer.instance.date_end \
                    or not serializer.instance.end_building \
                    or serializer.instance.area_size != serializer.validated_data.get('area_size'):
                building_date['end_building'] = serializer.validated_data.get('date_end') + relativedelta(
                    days=round(BUILDING_DAYS[serializer.validated_data.get('area_size')] * get_inc(
                        serializer.validated_data.get('sites')) * 0.0007 * 0.6))
            else:
                building_date['end_building'] = serializer.instance.end_building
            qs = Project.objects.filter(Q(start_building__lt=building_date['start_building'],
                                          end_building__gt=building_date['start_building']) |
                                        Q(start_building__lt=building_date['end_building'],
                                          end_building__gt=building_date['end_building']),
                                        sites__in=serializer.validated_data.get('sites'),
                                        status__in=['planning', 'working']).distinct()
            for i in qs:
                lt = i.end_building - building_date['start_building']
                gt = i.start_building - building_date['end_building']
                if (lt.days > 0 or gt.days > 0) and i != serializer.instance:
                    self.send_notification(i)
                    flag = True
                    # break
            self.__dict__['err_400'] = False
            if serializer.validated_data.get('status') == 'working' and flag:
                self.__dict__['err_400'] = True
                return None
        self.__dict__['alert_error'] = flag
        status_from_request = serializer.validated_data.get('status', None)
        status_in_project = serializer.instance.status

        if status_from_request == 'working' or status_from_request == 'planning':
        # перезапись заявок
            if serializer.validated_data.get('project_type', None).type_project_type == 'kvs':
                aplication = ApplicationLeaseKVS.objects.filter(status='working',
                                                                project=serializer.instance.id)
            if serializer.validated_data.get('project_type', None).type_project_type == 'expo':
                aplication = ApplicationExpo.objects.filter(status='working',
                                                            project=serializer.instance.id)
            if serializer.validated_data.get('project_type', None).type_project_type == 'ad':
                aplication = ApplicationAd.objects.filter(status='working',
                                                          project=serializer.instance.id)
            aplication.update(manager=Manager.objects.get(user_id = serializer.validated_data.get('user', None).id))


        if status_from_request == 'working' and status_in_project == 'planning':
            qsArchive = SectionArchive.objects.filter(project=serializer.instance)
            if qsArchive.count != 0:
                qsArchive.delete()
            data = ProjectSerializerROForArchive(instance=serializer.instance).data
            qsSec = serializer.instance.project_section.all()
            for index, i in enumerate(qsSec):
                qsAct = i.activities.all()
                save_params = dict([(field.name, getattr(i, field.name)) for field in i._meta.fields])
                save_params.pop('change')
                obj_archive = SectionArchive(**save_params)
                obj_archive.pk = None
                # obj_archive.activities = None
                obj_archive.count_managers = data.get('project_section')[index].get('count_managers')
                obj_archive.count_clients = data.get('project_section')[index].get('count_clients')
                obj_archive.save()
                obj_archive.activities.add(*qsAct)
                # for i in data.get('project_section'):
                #     i['project'] = serializer.instance
                #     act = i.get('activities')
                #     i.pop('activities')
                #     obj_archive = SectionArchive(**i)
                #     obj_archive.pk = None
                #     obj_archive.save()
                #     obj_archive.activities.add(act)
        change_date = False
        if serializer.validated_data.get('date_start') != serializer.instance.date_start:
            change_date = True
        if serializer.validated_data.get('date_start') and serializer.validated_data.get('date_end') \
                and serializer.validated_data.get('area_size') \
                or serializer.validated_data.get('area_size') != serializer.instance.area_size:

            if not serializer.validated_data.get('area_size') and \
                    serializer.validated_data.get('project_type').addit_parameter == 'promo':
                serializer.save()
            else:
                building_date = {}
                if serializer.validated_data.get('date_start') != serializer.instance.date_start \
                        or not serializer.instance.start_building or \
                                serializer.instance.area_size != serializer.validated_data.get('area_size'):
                    building_date['start_building'] = serializer.validated_data.get('date_start') - relativedelta(
                        days=round(BUILDING_DAYS[serializer.validated_data.get('area_size')] * get_inc(
                            serializer.validated_data.get('sites')) * 0.0007))
                if serializer.validated_data.get('date_end') != serializer.instance.date_end \
                        or not serializer.instance.end_building or \
                                serializer.instance.area_size != serializer.validated_data.get('area_size'):
                    building_date['end_building'] = serializer.validated_data.get('date_end') + relativedelta(
                        days=round(BUILDING_DAYS[serializer.validated_data.get('area_size')] * get_inc(
                            serializer.validated_data.get('sites')) * 0.0007 * 0.4))
                serializer.save(**building_date)

        else:
            serializer.save()
        complexity_array = []
        for i in complexity_dict:
            for j in complexity_dict[i]:
                temp = j
                temp['site_id'] = i
                temp['project_id'] = serializer.instance.id
                complexity_array.append(temp)
        if self.request.data.get('update_complexity'):
            RoomReservation.objects.filter(project_id=serializer.instance.id).delete()
            RoomReservation.objects.bulk_create([RoomReservation(**x) for x in complexity_array])
        if not ProjectIndicators.objects.filter(project=serializer.instance) and \
                        serializer.instance.project_type.type_project_type == 'expo':
            ProjectIndicators.objects.create(project=serializer.instance)
        if not ProjectIndicatorsTypeOne.objects.filter(project=serializer.instance) and \
                        serializer.instance.project_type.type_project_type == 'kvs':
            ProjectIndicatorsTypeOne.objects.create(project=serializer.instance)
        if not ProjectIndicatorsTypeTwo.objects.filter(project=serializer.instance) and \
                        serializer.instance.project_type.type_project_type == 'ad':
            ProjectIndicatorsTypeTwo.objects.create(project=serializer.instance)
        if serializer.instance.project_type.type_project_type == 'expo' and \
                serializer.instance.subjects and serializer.instance.date_start:
            if not serializer.instance.project_indicators.project_report.all():
                sub_format = serializer.instance.subjects.format
                date_month = serializer.instance.date_start.month
                date_year = serializer.instance.date_start.year
                # diff = 0
                count = 0
                if sub_format == 'В2В' or sub_format == 'В2В+В2С':
                    count = 13
                    # diff = date_month - count
                else:
                    count = 13
                    # diff = date_month - count
                # if diff < 0:
                #     diff = diff % 12
                #     date_year = date_year - 1
                #     date_month = diff
                # elif diff == 0:
                #     date_year = date_year - 1
                #     date_month = 12
                # else:
                #     date_month = diff
                date = datetime.date(date_year, date_month, 1) + relativedelta(months=1)
                date = date - relativedelta(months=count)
                for i in range(count + 1):
                    ProjectReport.objects.create(month_name=MONTH_NAME[date.month - 1],
                                                 month_date=date,
                                                 project_indicators=serializer.instance.project_indicators)
                    date = date + relativedelta(months=1)
            else:
                if change_date:
                    sub_format = serializer.instance.subjects.format
                    date_month = serializer.instance.date_start.month
                    date_year = serializer.instance.date_start.year
                    if sub_format == 'В2В' or sub_format == 'В2В+В2С':
                        count = 13
                    else:
                        count = 13
                    qs = serializer.instance.project_indicators.project_report.all()
                    date = datetime.date(date_year, date_month, 1) + relativedelta(months=1)
                    date = date - relativedelta(months=count)
                    for i in qs:
                        i.month_name = MONTH_NAME[date.month - 1]
                        i.month_date = date
                        i.save()
                        date = date + relativedelta(months=1)
        # Изменение дат плана маркетинга при изменении дат проекта третьего блока
        if serializer.instance.project_type.type_project_type == 'ad' and \
                serializer.instance.subjects and serializer.instance.date_start:
            count_project_report = ProjectReportTypeTwo.objects.filter(project=serializer.instance.id).count()
            if change_date and count_project_report > 0:
                set_or_create_project_report_type_two(count_project_report, serializer.instance.id)

    @list_route(methods=['get'])
    def get_old_projects(self, request):
        if self.request.GET.get('project'):
            project = Project.objects.get(id=self.request.GET.get('project'))
            projects = Project.objects.filter(subjects = project.subjects, date_start__lte = project.date_start).exclude(
                id = project.id).order_by('-date_start').values(*['id', 'date_start'])
            return Response(projects)
        return Response([])


    # def perform_update(self, serializer):
    #     flag = False
    #     complexity_dict = {}
    #     complexity_array = []
    #     complexity_id = []
    #     sites_id = [x.id for x in serializer.validated_data.get('sites')]
    #     for key, value in self.request.data.items():
    #         if key.startswith('area_id'):
    #             complexity_dict[int(key.split('_')[2])] = int(value)
    #             complexity_id.append(value)
    #     if serializer.validated_data.get('date_start') and serializer.validated_data.get('date_end') \
    #             and complexity_dict and \
    #             Sites.objects.filter(building_time=True, id__in=sites_id).exists():
    #         building_date = {}
    #         if serializer.validated_data.get('date_start') != serializer.instance.date_start or \
    #                         serializer.validated_data.get('date_end') != serializer.instance.date_end:
    #             # sites_dict = {}
    #             qs_complexity = ComplexityBuild.objects.filter(id__in=complexity_id).values('id', 'coefficient')
    #             qs_complexity = {a['id']: a['coefficient'] for a in qs_complexity}
    #             qs_sites = Sites.objects.filter(
    #                 id__in=[x.id for x in serializer.validated_data.get('sites') if x.building_time]).\
    #                 values('id', 'area')
    #             qs_sites = {a['id']: int(a['area']) for a in qs_sites}
    #             for k, v in qs_sites.items():
    #                 complexity_array.append({
    #                     'start_building': serializer.validated_data.get('date_start') - relativedelta(
    #                     days=round(qs_complexity[complexity_dict[k]] * int(v) * 0.0007)),
    #                     'end_building': serializer.validated_data.get('date_end') + relativedelta(
    #                     days=round(qs_complexity[complexity_dict[k]] * int(v) * 0.0007 * 0.6)),
    #                     'project_id': serializer.instance.id,
    #                     'complexity_id': complexity_dict[k],
    #                     'site_id': k
    #                 })
    #         else:
    #             complexity_array = serializer.instance.complexity_build.all().values()
    #         min_start_building = min(complexity_array, key=(lambda x: x['start_building']))['start_building']
    #         max_end_building = max(complexity_array, key=(lambda x: x['end_building']))['end_building']
    #         qs = Project.objects.filter(Q(complexity_build__start_building__lt=min_start_building,
    #                                       complexity_build__end_building__gt=min_start_building) |
    #                                     Q(complexity_build__start_building__lt=max_end_building,
    #                                       complexity_build__end_building__gt=max_end_building),
    #                                     complexity_build__site__in=serializer.instance.sites.all().values_list('id')).\
    #             distinct()
    #         for i in qs:
    #             for j in i.complexity_build.all():
    #                 lt = j.end_building - min_start_building
    #                 gt = j.start_building - max_end_building
    #                 if (lt.days > 0 or gt.days > 0) and i != serializer.instance:
    #                     flag = True
    #                     break
    #         self.__dict__['err_400'] = False
    #         if serializer.validated_data.get('status') == 'working' and flag:
    #             self.__dict__['err_400'] = True
    #             return None
    #     self.__dict__['alert_error'] = flag
    #     status_from_request = serializer.validated_data.get('status', None)
    #     status_in_project = serializer.instance.status
    #     if status_from_request == 'working' and status_in_project == 'planning':
    #         qsArchive = SectionArchive.objects.filter(project=serializer.instance)
    #         if qsArchive.count != 0:
    #             qsArchive.delete()
    #         data = ProjectSerializerROForArchive(instance=serializer.instance).data
    #         qsSec = serializer.instance.project_section.all()
    #         for index, i in enumerate(qsSec):
    #             qsAct = i.activities.all()
    #             save_params = dict([(field.name, getattr(i, field.name)) for field in i._meta.fields])
    #             save_params.pop('change')
    #             obj_archive = SectionArchive(**save_params)
    #             obj_archive.pk = None
    #             # obj_archive.activities = None
    #             obj_archive.count_managers = data.get('project_section')[index].get('count_managers')
    #             obj_archive.count_clients = data.get('project_section')[index].get('count_clients')
    #             obj_archive.save()
    #             obj_archive.activities.add(*qsAct)
    #             # for i in data.get('project_section'):
    #             #     i['project'] = serializer.instance
    #             #     act = i.get('activities')
    #             #     i.pop('activities')
    #             #     obj_archive = SectionArchive(**i)
    #             #     obj_archive.pk = None
    #             #     obj_archive.save()
    #             #     obj_archive.activities.add(act)
    #     change_date = False
    #     if serializer.validated_data.get('date_start') != serializer.instance.date_start:
    #         change_date = True
    #     # if serializer.validated_data.get('date_start') and serializer.validated_data.get('date_end') \
    #     #         and serializer.validated_data.get('area_size') \
    #     #         or serializer.validated_data.get('area_size') != serializers.instance.area_size:
    #     #     building_date = {}
    #     #     if serializer.validated_data.get('date_start') != serializer.instance.date_start \
    #     #             or not serializer.instance.start_building or \
    #     #                     serializer.instance.area_size != serializer.validated_data.get('area_size'):
    #     #         building_date['start_building'] = serializer.validated_data.get('date_start') - relativedelta(
    #     #             days=round(BUILDING_DAYS[serializer.validated_data.get('area_size')] * get_inc(
    #     #                 serializer.validated_data.get('sites')) * 0.0007))
    #     #     if serializer.validated_data.get('date_end') != serializer.instance.date_end \
    #     #             or not serializer.instance.end_building or \
    #     #                     serializer.instance.area_size != serializer.validated_data.get('area_size'):
    #     #         building_date['end_building'] = serializer.validated_data.get('date_end') + relativedelta(
    #     #             days=round(BUILDING_DAYS[serializer.validated_data.get('area_size')] * get_inc(
    #     #                 serializer.validated_data.get('sites')) * 0.0007 * 0.6))
    #     #     serializer.save(**building_date)
    #     # else:
    #     serializer.save()
    #     if self.request.data.get('update_complexity'):
    #         ComplexityBuildProject.objects.filter(project_id=serializer.instance.id).delete()
    #         ComplexityBuildProject.objects.bulk_create([ComplexityBuildProject(**x) for x in complexity_array])
    #     if not ProjectIndicators.objects.filter(project=serializer.instance) and \
    #                     serializer.instance.project_type.type_project_type == 'expo':
    #         ProjectIndicators.objects.create(project=serializer.instance)
    #     if not ProjectIndicatorsTypeOne.objects.filter(project=serializer.instance) and \
    #                     serializer.instance.project_type.type_project_type == 'kvs':
    #         ProjectIndicatorsTypeOne.objects.create(project=serializer.instance)
    #     if not ProjectIndicatorsTypeTwo.objects.filter(project=serializer.instance) and \
    #                     serializer.instance.project_type.type_project_type == 'ad':
    #         ProjectIndicatorsTypeTwo.objects.create(project=serializer.instance)
    #     if serializer.instance.project_type.type_project_type == 'expo' and \
    #             serializer.instance.subjects and serializer.instance.date_start:
    #         if not serializer.instance.project_indicators.project_report.all():
    #             sub_format = serializer.instance.subjects.format
    #             date_month = serializer.instance.date_start.month
    #             date_year = serializer.instance.date_start.year
    #             # diff = 0
    #             count = 0
    #             if sub_format == 'В2В' or sub_format == 'В2В+В2С':
    #                 count = 12
    #                 # diff = date_month - count
    #             else:
    #                 count = 12
    #                 # diff = date_month - count
    #             # if diff < 0:
    #             #     diff = diff % 12
    #             #     date_year = date_year - 1
    #             #     date_month = diff
    #             # elif diff == 0:
    #             #     date_year = date_year - 1
    #             #     date_month = 12
    #             # else:
    #             #     date_month = diff
    #
    #             date = datetime.date(date_year, date_month, 1) - relativedelta(months=count)
    #             for i in range(count + 1):
    #                 ProjectReport.objects.create(month_name=MONTH_NAME[date.month - 1],
    #                                              month_date=date,
    #                                              project_indicators=serializer.instance.project_indicators)
    #                 date = date + relativedelta(months=1)
    #         else:
    #             if change_date:
    #                 sub_format = serializer.instance.subjects.format
    #                 date_month = serializer.instance.date_start.month
    #                 date_year = serializer.instance.date_start.year
    #                 if sub_format == 'В2В' or sub_format == 'В2В+В2С':
    #                     count = 12
    #                 else:
    #                     count = 12
    #                 qs = serializer.instance.project_indicators.project_report.all()
    #                 date = datetime.date(date_year, date_month, 1) - relativedelta(months=count)
    #                 for i in qs:
    #                     i.month_name = MONTH_NAME[date.month - 1]
    #                     i.month_date = date
    #                     i.save()
    #                     date = date + relativedelta(months=1)
    #     # Изменение дат плана маркетинга при изменении дат проекта третьего блока
    #     if serializer.instance.project_type.type_project_type == 'ad' and \
    #             serializer.instance.subjects and serializer.instance.date_start:
    #         count_project_report = ProjectReportTypeTwo.objects.filter(project=serializer.instance.id).count()
    #         if change_date and count_project_report > 0:
    #             set_or_create_project_report_type_two(count_project_report, serializer.instance.id)


class ApiSetProjectInClientView(viewsets.ModelViewSet):
    queryset = Project.objects.all().prefetch_related('user')
    serializer_class = ProjectInClientSerializer
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('client', 'status')
    ordering_fields = ('name',)
    ordering = ('-date_start',)


class ApiSetProjectType(viewsets.ModelViewSet):
    queryset = Project_type.objects.all()
    serializer_class = ProjectTypeSerializer
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    filter_fields = ('type_project_type','director')
    ordering = ('type',)
    ordering_fields = ('type',)


class ApiSetBuisnessUnit(viewsets.ModelViewSet):
    queryset = Buisness_unit.objects.all()
    serializer_class = BuisnessUnitSerializer


class ApiSetSites(viewsets.ModelViewSet):
    queryset = Sites.objects.all()
    serializer_class = SitesSerializers
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    ordering = ('ordering',)
    filter_fields = ('sites', )


class ApiSetActivities(viewsets.ModelViewSet):
    queryset = Activities.objects.all()
    serializer_class = ActivitiesSerializer
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id',)

    def get_queryset(self):
        if self.request.GET.get('project'):
            if self.request.GET.get('selected') != 'undefined':
                return Activities.objects.filter(Q(id__in=self.request.GET.get('selected').split(',')) |
                                                 ~Q(project_section__project=self.request.GET.get('project')))
            else:
                return Activities.objects.filter(~Q(project_section__project=self.request.GET.get('project')))
        if self.request.GET.get('project_clients') and self.request.GET.get('project_clients', '') != 'all':
            return Activities.objects.filter(Q(project_section__project=self.request.GET.get('project_clients')))
        return Activities.objects.all()


class ApiSetProjectSection(ModelViewSetClientCascadeDestroy):
    queryset = Test.objects.all()
    serializer_class = TestSerializerRO
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('project',)

    def perform_create(self, serializer):
        serializer.save(time_change=timezone.now())
        count = serializer.instance.project.project_indicators.count_app
        if count:
            serializer.instance.count_comp_plan = int((count / 100) * serializer.instance.percent_conxept)
            serializer.instance.save()

    def perform_update(self, serializer):
        count = serializer.instance.project.project_indicators.count_app
        percent = serializer.instance.percent_conxept
        if count and int(serializer.validated_data.get('percent_conxept')) != percent:
            count_comp_plan = int((count / 100) * int(serializer.validated_data.get('percent_conxept')))
            serializer.save(time_change=timezone.now(), change=False, count_comp_plan=count_comp_plan)
        else:
            serializer.save(time_change=timezone.now(), change=False)

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return TestSerializerRO
        else:
            return TestSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        summ_fields = {'count_clients': 0, 'count_managers': 0, 'percent_conxept': 0, 'count_comp_plan': 0}
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        for i in data:
            i['percent'] = None
            for k, v in i.items():
                if k in summ_fields and v:
                    summ_fields[k] += v
        set_total = False
        for i in data:
            if summ_fields['count_managers'] != 0:
                set_total = True
                i['percent'] = str(round((round(i['count_managers'] / summ_fields['count_managers'], 2) * 100))) + '%'
        if set_total:
            summ_fields['percent'] = str(
                round(summ_fields['count_managers'] / summ_fields['count_clients'], 2) * 100) + '%'
        summ_fields['html'] = 'Итого'
        summ_fields['id'] = uuid.uuid4()
        data.append(OrderedDict(summ_fields))
        return Response(data)


class ApiSetSubSection(viewsets.ModelViewSet):
    serializer_class = SubSectionSerializer
    queryset = Subsection.objects.all()
    permission_classes = (DjangoModelPermissions,)


class ApiSetSubjects(viewsets.ModelViewSet):
    serializer_class = SubjectsSerializer
    queryset = Subjects.objects.all()
    permission_classes = (DjangoModelPermissions,)


class ApiSetProjectIndicators(viewsets.ModelViewSet):
    serializer_class = ProjectIndicatorsSerializerRO
    queryset = ProjectIndicators.objects.all()
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('project',)

    def get_serializer_context(self):
        project_id = self.request.GET.get('project')
        if not project_id and self.lookup_field == 'pk':
            project_id = ProjectIndicators.objects.get(pk=self.kwargs.get('pk')).project_id
        return {
            'one_c_bill': json.loads(manage_data.getSumTableBill(project_id=project_id)),
            'one_c': json.loads(manage_data.getSumTable(project_id=project_id)),
            'performance': manage_data.getProjectPerformance(project_id)[0]
        }

    def perform_update(self, serializer):
        if self.request.GET.get('type') == 'expo':
            if serializer.validated_data.get('count_app'):
                count = int(serializer.validated_data.get('count_app'))
                qs = serializer.instance.project.project_section.all()
                for i in qs:
                    i.count_comp_plan = int(count * (i.percent_conxept / 100))
                    i.save()
        serializer.save()

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            if self.request.GET.get('type') == 'expo':
                return ProjectIndicatorsSerializerRO
            if self.request.GET.get('type') == 'kvs':
                return ProjectIndicatorsTypeOneSerializerRO
            if self.request.GET.get('type') == 'ad':
                return ProjectIndicatorsTypeTwoSerializerRO
        else:
            if self.request.GET.get('type') == 'expo':
                return ProjectIndicatorsSerializer
            if self.request.GET.get('type') == 'kvs':
                return ProjectIndicatorsTypeOneSerializer
            if self.request.GET.get('type') == 'ad':
                return ProjectIndicatorsTypeTwoSerializer

    def get_queryset(self):
        if self.request.GET.get('type') == 'expo':
            return ProjectIndicators.objects.all().extra(select={
            'count_vis_fact': """SELECT COUNT(*)
            FROM "TICKETS_Tickets", "PROJECT_projects", "TICKETS_Order"
            WHERE "TICKETS_Tickets".order_id="TICKETS_Order".id
            AND "TICKETS_Order".project_id="PROJECT_projects".id
            AND "PROJECT_projects".p_expobit=projects_projectindicators.project_id::text 
            AND "TICKETS_Tickets".state=true""",
            'sum_amount_fact': """SELECT SUM(amount)
            FROM "TICKETS_Order", "PROJECT_projects"
            WHERE "TICKETS_Order".project_id="PROJECT_projects".id
            AND "PROJECT_projects".p_expobit=projects_projectindicators.project_id::text 
            AND "TICKETS_Order".state=1"""
        })
        if self.request.GET.get('type') == 'kvs':
            return ProjectIndicatorsTypeOne.objects.all()
        if self.request.GET.get('type') == 'ad':
            return ProjectIndicatorsTypeTwo.objects.all()


class ApiSetProjectReportView(viewsets.ModelViewSet):
    queryset = ProjectReport.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = ProjectReportSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('project_indicators__project__user', 'project_indicators__project__project_type__director')

    def perform_update(self, serializer):
        if self.request.data.get('approval_status') == 'return':
            project = serializer.instance.project_indicators.project
            Approval.objects.create(
                created_user=self.request.user,
                comment=self.request.data.get('comment'),
                project=project,
                report=serializer.instance,
                status='e'
            )
            Channel('chat-messages').send({'user': project.user_id,
                                           'push': 'Возврат на доработку плана по проекту',
                                           'username': project.user.username,
                                           'message': '<a href="#project/%s/edit">%s</a><p>%s</p>' % (
                                               project.id,
                                               project.name,
                                               self.request.data.get('comment')
                                           ),
                                           'sender': self.request.user.pk})
            Channel('send_notification').send({
                'users': ','.join([str(project.user_id), str(project.project_type.director_id)]),
                'username': project.user.username,
                'message': '<p>Возврат на доработку плана по проекту<p><a href="#project/%s/edit">%s</a><p>%s</p>' % (
                    project.id,
                    project.name,
                    self.request.data.get('comment')
                ),
                'block_working': None
            })
            Channel('send-email').send({
                'user': project.project_type.director_id,
                'subject': 'Возврат на доработку плана по проекту %s' % (project.name,),
                'message': '<a href="#project/%s/edit">%s</a><p>%s</p>' % (
                                               project.id,
                                               project.name,
                                               self.request.data.get('comment')
                                           ),
            })
        if self.request.data.get('approval_status') == 'on_approval':
            project = serializer.instance.project_indicators.project
            send_user = User.objects.filter(groups__name='executive_director')
            if send_user:
                Channel('send-email').send({
                    'user': send_user[0].id,
                    'subject': 'Утвердить план на месяц по проекту %s от %s %s' % (project.name,
                                                                              self.request.user.last_name,
                                                                              self.request.user.first_name),
                    'message': '<a href="http://expobit/#year_report_dvp">Перейти для просмотра</a>'
                })
                Channel('chat-messages').send({'user': send_user[0].id,
                                               'push': 'Запрос на утверждение',
                                               'username': send_user[0].username,
                                               'message': '<a href="/#year_report_dvp">'
                                                          'Перейти для просмотра</a><p>'
                                                          'Запрос на утверждение плана на месяц по проекту %s</p>' % (
                                                   project.name,
                                               ),
                                               'sender': self.request.user.pk})
        serializer.save()

    def get_queryset(self):
        month = self.request.GET.get('month')
        months = self.request.GET.get('months')
        project_status = ['working']
        if self.request.GET.get('all'):
            project_status.append('finished')
        if month:
            return ProjectReport.objects.filter(month_date__month=datetime.datetime.now().month,
                                                month_date__year=datetime.date.today().year,
                                                project_indicators__project__status__in=project_status).extra(select={
            'comment_return': """SELECT comment 
            FROM projects_approval
            WHERE projects_approval.report_id=projects_projectreport.id 
            ORDER BY created_date DESC LIMIT 1"""
        })
        if months:
            return ProjectReport.objects.filter(month_date__month=months.split('-')[1],
                                                month_date__year=months.split('-')[0],
                                                project_indicators__project__status__in=project_status).extra(select={
            'comment_return': """SELECT comment 
            FROM projects_approval
            WHERE projects_approval.report_id=projects_projectreport.id 
            ORDER BY created_date DESC LIMIT 1"""
        })
        return ProjectReport.objects.all()

    def get_serializer_context(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            if self.request.GET.get('months'):
                first_month = datetime.datetime.strptime(self.request.GET.get('months'), '%Y-%m').date()
            else:
                first_month = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month, 1)
            last_month = first_month + relativedelta(months=1) - relativedelta(days=1)
            qs = self.filter_queryset(self.get_queryset())
            projects_id = []
            for i in qs:
                project = i.project_indicators.project_id
                if project not in projects_id:
                    projects_id.append(str(project))
            # TODO Подсчет расхождения с фин планом доходов
            if len(projects_id) != 0:
                one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, ','.join(projects_id)))
                one_c_context = {}
                if one_c_data[0] != {}:
                    for bill in one_c_data:
                        if bill['ПроектИД'] in one_c_context:
                            one_c_context[bill['ПроектИД']].append(int(float(bill['ОплаченнаяСумма'])))
                        else:
                            one_c_context[bill['ПроектИД']] = [int(float(bill['ОплаченнаяСумма']))]
                return {'one_c': one_c_context}
        return {}

    @list_route(methods=['get'])
    def table(self, request):
        project_id = request.GET.get('project')
        project_reports = ProjectReport.objects.filter(project_indicators__project=project_id).order_by(
            'month_date').extra(select={
            'comment_return': """SELECT comment 
            FROM projects_approval
            WHERE projects_approval.report_id=projects_projectreport.id 
            ORDER BY created_date DESC LIMIT 1"""
        })
        first_month = project_reports.first().month_date
        last_month = project_reports.last().month_date + relativedelta(months=1) - relativedelta(days=1)
        one_c_context = {}
        one_c_context_bill = None
        # TODO Подсчет расхождения с фин планом доходов
        qs_client = Membership.objects.filter(project=project_id).exclude(client__status__in=['black_list', 'inactive']).values('client_id')
        arr_qs = [x['client_id'] for x in qs_client]
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, project_id))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
                key = '%s.%s' % (str(date.year), str(date.month))
                if key in one_c_context:
                    one_c_context[key].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    one_c_context[key] = [int(float(bill['ОплаченнаяСумма']))]
        if request.GET.get('show_bill'):
            one_c_context_bill = {}
            one_c_data = json.loads(manage_data.getSumTableBill(first_month, last_month, project_id))
            if one_c_data[0] != {}:
                for bill in one_c_data:
                    date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
                    key = '%s.%s' % (str(date.year), str(date.month))
                    if key in one_c_context_bill:
                        one_c_context_bill[key].append(int(float(bill['СуммаСчета'])))
                    else:
                        one_c_context_bill[key] = [int(float(bill['СуммаСчета']))]
        data = ProjectReportSerializer(instance=project_reports, many=True, context={
            'one_c': one_c_context,
            'arr_qs': arr_qs,
            'one_c_bill': one_c_context_bill,
            'project_id': project_id
        }).data
        s_dict = {
            'fact_app': 0,
            'fact_processing_db': 0,
            'fact': 0,
            'diff': 0,
            'fact_bill': 0,
            'plan_app_money': 0
        }
        sum_fin = 0
        sum_fact_bill = 0
        sum_fact = 0
        sum_app_fact = 0
        summ_app_plan = 0
        for i in data:
            if i.get('fact'):
                sum_fact += i.get('fact')
            else:
                sum_fact += 0
            if i.get('fact_bill'):
                sum_fact_bill += i.get('fact_bill')
            else:
                sum_fact_bill += 0
            if i.get('fin_plan'):
                sum_fin += i.get('fin_plan')
            else:
                sum_fin += 0
            if i.get('fact_app'):
                sum_app_fact += i.get('fact_app')
            else:
                sum_app_fact += 0
            if i.get('app_plan'):
                summ_app_plan += i.get('app_plan')
            else:
                summ_app_plan += 0
            i['app_diff'] = sum_app_fact - summ_app_plan
            i['diff'] = sum_fact - sum_fin
        for i in data:
            s_dict['fact_app'] += int(i.get('fact_app'))
            s_dict['plan_app_money'] += int(i.get('plan_app_money'))
            s_dict['fact_processing_db'] += int(i.get('fact_processing_db'))
            s_dict['fact'] += int(i.get('fact'))
            s_dict['fact_bill'] += int(i.get('fact_bill'))
            s_dict['diff'] += int(i.get('diff'))
        if not project_reports:
            return Response(status=400)
        project_indicator = project_reports[0].project_indicators
        first_row = data[0].copy()
        for i in first_row:
            first_row[i] = None
        # first_row = OrderedDict()
        first_row['fin_plan'] = project_indicator.cash_receipts_exp
        first_row['app_plan'] = project_indicator.count_app
        first_row['plan_app_money'] = s_dict['plan_app_money']
        first_row['fact_app'] = s_dict['fact_app']
        first_row['fact'] = s_dict['fact']
        first_row['fact_bill'] = s_dict['fact_bill']
        try:
            first_row['diff'] = first_row['fact'] - first_row['fin_plan']
        except:
            return JsonResponse(data={'error': 'not full data'}, status=400, safe=False)
        first_row['fin_plan_month'] = project_indicator.cash_receipts_exp
        if first_row['fact_app'] is None or first_row['app_plan'] is None:
            first_row['app_diff'] = None
        else:
            first_row['app_diff'] = first_row['fact_app'] - first_row['app_plan']
        first_row['app_plan_month'] = project_indicator.count_app
        try:
            first_row['perc_comp'] = str(round((first_row['fact_app'] / first_row['app_plan_month']) * 100, 2)) + ' %'
        except:
            first_row['perc_comp'] = 0
        try:
            first_row['perc'] = str(round((first_row['fact'] / first_row['fin_plan_month']) * 100, 2)) + ' %'
        except:
            first_row['perc'] = 0
        first_row['plan_processing_db'] = Membership.objects.filter(project=project_id).exclude(
            client__status__in=['inactive', 'black_list']).count()
        first_row['fact_processing_db'] = s_dict['fact_processing_db']
        if first_row['plan_processing_db'] != 0:
            first_row['perc_comp_db'] = str(
                round((first_row['fact_processing_db'] / first_row['plan_processing_db']) * 100, 2)) + ' %'
        else:
            first_row['perc_comp_db'] = 0
        first_row['month_name'] = 'Итого'
        first_row['id'] = str(uuid.uuid4())
        data.insert(0, first_row)
        return JsonResponse(data, safe=False)


class ApiSetProjectReportTypeTwoView(viewsets.ModelViewSet):
    queryset = ProjectReportTypeTwo.objects.all()
    serializer_class = ProjectReportTypeTwoSerializer
    permission_classes = (DjangoModelPermissions,)

    def get_queryset(self):
        month = self.request.GET.get('month')
        months = self.request.GET.get('months')
        project_status = ['working']
        if self.request.GET.get('all'):
            project_status.append('finished')
        if month:
            return ProjectReportTypeTwo.objects.filter(month_date__month=datetime.datetime.now().month,
                                                       month_date__year=datetime.date.today().year,
                                                       project_indicators__project__status__in=project_status)
        if months and not self.request.GET.get('project'):
            return ProjectReportTypeTwo.objects.filter(month_date__month=months.split('-')[1],
                                                       month_date__year=months.split('-')[0],
                                                       project_indicators__project__status__in=project_status)
        return ProjectReportTypeTwo.objects.all()

    def get_serializer_context(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            if self.request.GET.get('months'):
                first_month = datetime.datetime.strptime(self.request.GET.get('months'), '%Y-%m').date()
            else:
                first_month = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month, 1)
            last_month = first_month + relativedelta(months=1) - relativedelta(days=1)
            qs = self.filter_queryset(self.get_queryset())
            projects_id = []
            for i in qs:
                project = i.project_indicators.project_id
                if project not in projects_id:
                    projects_id.append(str(project))
            # TODO Подсчет расхождения с фин планом доходов
            if len(projects_id) != 0:
                one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, ','.join(projects_id)))
                one_c_context = {}
                if one_c_data[0] != {}:
                    for bill in one_c_data:
                        if bill['ПроектИД'] in one_c_context:
                            one_c_context[bill['ПроектИД']].append(int(float(bill['ОплаченнаяСумма'])))
                        else:
                            one_c_context[bill['ПроектИД']] = [int(float(bill['ОплаченнаяСумма']))]
                return {'one_c': one_c_context, 'first_month': first_month, 'last_month': last_month}
        return {}

    @list_route(methods=['post'])
    def create_plan(self, request):
        project_id = request.GET.get('project')
        count_month = int(request.GET.get('months'))
        error, status = set_or_create_project_report_type_two(count_month, project_id)
        if error:
            Response(status=status)
        return Response(data={'result': 'ok'})

    @list_route(methods=['get'])
    def table(self, request):
        project_id = request.GET.get('project')
        return JsonResponse(get_serialized_data_project_report_type_two(project_id), safe=False)


class ApiSetProjectReportManagerView(viewsets.ModelViewSet):
    queryset = ProjectReportManager.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = ProjectReportManagerSerializer

    # filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    # ordering = ('month_date',)
    # ordering_fields = ('month_date',)

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return ProjectReportManagerSerializerRO
        else:
            return ProjectReportManagerSerializer

    @list_route(methods=['get'])
    def table(self, request):
        report_id = request.GET.get('report')
        project_reports_managers = ProjectReportManager.objects.filter(project_report=report_id)
        project_report = ProjectReport.objects.get(pk=report_id)
        project_id = project_report.project_indicators.project_id
        first_month = project_report.month_date
        last_month = first_month + relativedelta(months=1) - relativedelta(days=1)
        one_c_context = {}
        # TODO Подсчет расхождения с фин планом доходов.
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, project_id))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                if bill['МенеджерИД'] in one_c_context:
                    one_c_context[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    one_c_context[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
        data = ProjectReportManagerSerializerRO(instance=project_reports_managers,
                                                many=True, context=one_c_context).data
        if not project_reports_managers:
            project_report = ProjectReport.objects.get(pk=report_id)
            first_row = OrderedDict()
            first_row['app_plan_month'] = project_report.app_plan_month
            first_row['fin_plan_month'] = project_report.fin_plan_month
            first_row['plan_processing_db'] = project_report.plan_processing_db
            first_row['manager'] = 'Итого'
            first_row['id'] = str(uuid.uuid4())
            return JsonResponse([first_row, ], safe=False)
        project_report = project_reports_managers[0].project_report
        s_dict = {'fact_app': 0, 'fact_processing_db': 0, 'fact': 0, 'plan_app_money': 0}
        for i in data:
            s_dict['fact_app'] += int(i.get('fact_app'))
            s_dict['fact_processing_db'] += int(i.get('fact_processing_db'))
            s_dict['fact'] += int(i.get('fact'))
            s_dict['plan_app_money'] += int(i.get('plan_app_money'))
        first_row = data[0].copy()
        for i in first_row:
            first_row[i] = None
        # first_row = OrderedDict()
        # first_row['fin_plan'] = project_indicator.cash_receipts_exp
        # first_row['app_plan'] = project_indicator.count_app
        first_row['fact_app'] = s_dict['fact_app']
        first_row['fact'] = s_dict['fact']
        first_row['plan_app_money'] = s_dict['plan_app_money']
        first_row['app_plan_month'] = project_report.app_plan_month
        first_row['fin_plan_month'] = project_report.fin_plan_month
        first_row['fact_processing_db'] = s_dict['fact_processing_db']
        first_row['plan_processing_db'] = project_report.plan_processing_db
        try:
            first_row['perc_comp_db'] = str(
                round((first_row['fact_processing_db'] / first_row['plan_processing_db']) * 100, 2)) + ' %'
        except:
            first_row['perc_comp_db'] = None
        first_row['manager'] = 'Итого'
        try:
            first_row['perc_comp'] = str(round((first_row['fact_app'] / first_row['app_plan_month']) * 100, 2)) + ' %'
        except:
            first_row['perc_comp'] = 0
        try:
            first_row['perc'] = str(round((first_row['fact'] / first_row['fin_plan_month']) * 100, 2)) + ' %'
        except:
            first_row['perc'] = 0
        first_row['id'] = str(uuid.uuid4())
        data.insert(0, first_row)
        return JsonResponse(data, safe=False)

    @list_route(methods=['get'])
    def full_table(self, request):
        date = request.GET.get('date')
        date = date.split('-')
        filter_field = {
            'project_report__month_date__month': date[1],
            'project_report__month_date__year': date[0]
        }
        proj_filter = {}
        if request.GET.get('project__project_type__director'):
            filter_field['project_report__project_indicators__project__project_type__director'] = request.\
                GET.get('project__project_type__director')
            proj_filter['project_type__director'] = request.GET.get('project__project_type__director')
        if request.GET.get('project_indicators__project__user'):
            filter_field['project_report__project_indicators__project__user'] = request.\
                GET.get('project_indicators__project__user')
            proj_filter['user'] = request.GET.get('project_indicators__project__user')
        project_reports_managers = ProjectReportManager.objects.filter(
            **filter_field
        ).values('manager__user__last_name',
                 'manager__user__first_name',
                 'project_report__month_date',
                 'manager').annotate(
            fin_plan_month_sum=Sum('fin_plan_month')).annotate(app_plan_month_sum=Sum('app_plan_month')).annotate(
            plan_processing_db_sum=Sum('plan_processing_db'))
        projects_id = Project.objects.filter(
            **proj_filter,
            project_indicators__project_report__project_report_manager__in=project_reports_managers.values_list(
                'id', flat=True)).\
            values_list('id', flat=True).distinct()
        first_month = datetime.date(int(date[0]),
                                    int(date[1]), 1)
        last_month = first_month + relativedelta(months=1) - relativedelta(days=1)
        one_c_context = {}
        # TODO Подсчет расхождения с фин планом доходов.
        projects_str_id = [str(x) for x in list(projects_id)]
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, ','.join(projects_str_id)))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                if bill['МенеджерИД'] in one_c_context:
                    one_c_context[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    one_c_context[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
        data = ProjectReportManagerFullSerializerRO(
            instance=project_reports_managers,
            many=True, context={
                'one_c_context': one_c_context,
                'projects_id': list(projects_id)
            }).data
        last_row = OrderedDict()
        last_row['fin_plan_month_sum'] = 0
        last_row['app_plan_month_sum'] = 0
        last_row['plan_processing_db_sum'] = 0
        last_row['fact'] = 0
        last_row['fact_processing_db'] = 0
        last_row['fact_app'] = 0
        last_row['manager'] = {'manager': 'Итого'}
        for i in data:
            last_row['fin_plan_month_sum'] += i['fin_plan_month_sum']
            last_row['app_plan_month_sum'] += i['app_plan_month_sum']
            last_row['plan_processing_db_sum'] += i['plan_processing_db_sum']
            last_row['fact'] += i['fact']
            last_row['fact_processing_db'] += i['fact_processing_db']
            last_row['fact_app'] += i['fact_app']
        if last_row['fin_plan_month_sum']:
            last_row['perc'] = str(round((last_row['fact']/last_row['fin_plan_month_sum'])*100, 2))+' %'
        if last_row['app_plan_month_sum']:
            last_row['perc_comp'] = str(round((last_row['fact_app'] / last_row['app_plan_month_sum']) * 100, 2))+' %'
        if last_row['plan_processing_db_sum']:
            last_row['perc_comp_db'] = str(round(
                (last_row['fact_processing_db'] / last_row['plan_processing_db_sum']) * 100, 2))+' %'
        data.append(last_row)
        # project_report = project_reports_managers[0].project_report

        return JsonResponse(data, safe=False)


class ApiSetProjectReportManagerTypeTwoView(viewsets.ModelViewSet):
    queryset = ProjectReportTypeTwoManager.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = ProjectReportTypeTwoManagerSerializer

    # filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    # ordering = ('month_date',)
    # ordering_fields = ('month_date',)

    def get_serializer_class(self):
        if self.request.method.upper() in settings.SAFE_METHODS:
            return ProjectReportTypeTwoManagerSerializerRO
        else:
            return ProjectReportTypeTwoManagerSerializer

    @list_route(methods=['get'])
    def table(self, request):
        report_id = request.GET.get('report')
        return JsonResponse(data=get_serialized_data_project_report_managers_type_two(report_id), safe=False)
        # Create your views here.


class ApiSetRoomReservation(viewsets.ModelViewSet):
    queryset = RoomReservation.objects.all()
    serializer_class = RoomReservationSerializer
    permission_classes = (DjangoModelPermissions,)


class ApiSetApproval(viewsets.ModelViewSet):
    queryset = Approval.objects.all()
    serializer_class = ApprovalSerializer
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('report', )


class ApiSetCateringView(viewsets.ModelViewSet):
    queryset = CateringService.objects.all()
    serializer_class = CateringSerializer
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('project',)


class ApiSetProjectTypeReportView(viewsets.ModelViewSet):
    queryset = Project_type.objects.all()
    serializer_class = ProjectTypeReportSerializer
    permission_classes = (DjangoModelPermissions,)

    def get_queryset(self):
        qs = super().get_queryset().exclude(director_id__isnull=True)
        if self.request.user and 'head_direction' in self.request.user.groups.values_list('name',  flat=True):
            user = list(Manager.objects.filter(Q(user_id = self.request.user.id) | Q(head_id = self.request.user.id)).distinct().values_list('user_id', flat=True))
            qs = qs.filter(director__in = user)
        return qs

    def get_serializer_context(self):
        month = self.request.GET.get('month')
        first_month = datetime.datetime.strptime(month, '%Y-%m').date()
        last_month = first_month + relativedelta(months=1) - relativedelta(days=1)


        dirs = Project_type.objects.all().values_list('director', flat=True)
        p_t = Project_type.objects.all()
        sub_dict = {}
        for i in p_t:
            proj_type = i.project_type.all()
            sub_dict = {**sub_dict, **{str(x): str(i.id) for x in i.project_type.all().values_list('id', flat=True)}}
        sub_dir_dict = {}
        for i in dirs:
            qs = Manager.objects.filter(Q(head_id=i) | Q(head__managers__head_id=i)).values('id')
            sub_dir_dict = {**sub_dir_dict, **{str(x['id']): str(i) for x in qs}}
            sub_dir_dict[str(i)] = str(i)
        #sub_dict = {str(x['user']): str(x['head']) for x in qs}
        manager_money_dict = {}
        sub_money_dict = {}
        one_c_data = json.loads(manage_data.getSumTable(first_month, last_month))
        if one_c_data[0] != {}:
            for bill in one_c_data:
                if bill['МенеджерИД'] in manager_money_dict:
                    manager_money_dict[bill['МенеджерИД']].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    manager_money_dict[bill['МенеджерИД']] = [int(float(bill['ОплаченнаяСумма']))]
                if sub_dict.get(bill['ПроектИД']) in sub_money_dict:
                    sub_money_dict[sub_dict.get(bill['ПроектИД'])].append(int(float(bill['ОплаченнаяСумма'])))
                else:
                    sub_money_dict[sub_dict.get(bill['ПроектИД'])] = [int(float(bill['ОплаченнаяСумма']))]
        return {
            'sub_money_dict': sub_money_dict,
            'manager_money_dict': manager_money_dict,
            'sub_dict': sub_dict,
            'sub_dir_dict': sub_dir_dict,
            'date': first_month
        }


class ApiSetReportValuesView(viewsets.ModelViewSet):
    serializer_class = ReportValuesSerializer
    permission_classes = (DjangoModelPermissions,)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('type_report__project_type__type',)
    queryset = ReportValue.objects.all()

    def get_queryset(self):
        if self.request.method == 'PATCH':
            return ReportValue.objects.all()
        date = self.request.GET.get('month')

        date = datetime.date(int(date.split('-')[0]), int(date.split('-')[1]), 1)
        name = self.request.GET.get('type_report__project_type__type')
        now = datetime.date.today()
        if not ReportValue.objects.filter(date__month=now.month, date__year=now.year, type_report__project_type__type=name).exists():
            qs = ReportTypes.objects.filter(project_type__type=name)
            curr_date = datetime.date(now.year, now.month, 1)
            new_reports = [ReportValue(date=curr_date, type_report=i) for i in qs]
            ReportValue.objects.bulk_create(new_reports)
        return ReportValue.objects.filter(date__month=date.month, date__year=date.year).annotate(
            name=F('type_report__name')).order_by('name')


class ApiSetCalendarView(viewsets.ModelViewSet):
    queryset = Project.objects.all()

    @list_route(methods=['get'])
    def table_data(self, request):
        from datetime import timedelta

        data = request.GET

        content={}

        if 'date' in data:
            start = datetime.date(int(data['date'].split('-')[0]), int(data['date'].split('-')[1]), 1)
            end = datetime.date(int(data['date'].split('-')[0]), int(data['date'].split('-')[1]),
                                calendar.monthrange(int(data['date'].split('-')[0]), int(data['date'].split('-')[1]))[1])

        else:
            start = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month, 1)
            end = datetime.date(datetime.datetime.now().year, datetime.datetime.now().month,
                                calendar.monthrange(datetime.datetime.now().year, datetime.datetime.now().month)[1])

        if 'adday' in data:
            start = start +(1 if int(data['adday'])>0 else -1)*timedelta(days = math.fabs(int(data['adday'])))
            end = end +(1 if int(data['adday'])>0 else -1)*timedelta(days = math.fabs(int(data['adday'])))

        sites = []
        if 'sites' in data:
            if data['sites']:
                sites = data['sites'].split(',')
        else:
            for site in Sites.objects.all():
                sites.append(site.id)

        for s in sites:
            site = Sites.objects.filter(id=s)

            projects = Project.objects.exclude(status='canceled').filter( sites=site).filter(
                    Q(start_building__gte=start, end_building__lte=end) |
                    Q(start_building__lte=start, end_building__lte=end, end_building__gte=start) |
                    Q(start_building__gte=start, end_building__gte=end, start_building__lte=end) |
                    Q(start_building__lte=start, end_building__gte=end)
            ).distinct('id').annotate(client_name=F('client__name')).prefetch_related('sites', 'project_type', 'buisness_unit', 'user',
                                                          'catering_service')

            if 'report' in data:
                content[s] = ProjectCalendarSerializerReport(projects, many=True).data
            else:
                content[s] = ProjectCalendarSerializer(projects, many=True).data

        return Response(content)


    @list_route(methods=['get'])
    def report(self, request):
        import time
        start_time = time.time()
        content = []
        data = request.GET
        project = Project.objects.exclude(status='canceled')

        if data['extend_val'] == 'true':
            sites_db = Sites.objects.all()
            sites = []
            for i in sites_db:
                sites.append({
                    'name' : i.name,
                    'params':{"id":i.id}
                })
        else:
            sites = [
                {'name' : 'Конгрессные','params':{"congress":True}, 'params2':{"congress":True}},
                {'name' : 'Аметист','params':{"ametist":True},'params2':{"ametist":True}},
                {'name' : 'Выставочные','params':{"exhibition":True},'params2':{"exhibition":True}},
                {'name' : 'Открытая площадь','params':{"open_area":True},'params2':{"open_area":True}}
            ]

        for site in sites:

            site_db = Sites.objects.filter(**site['params'])
            temp = {
                'plase': site['name']
            }
            for month in range(1, 13):
                temp[month] = 0

                if data['extend_val'] == 'true':
                    site_db_one = site_db[0]
                    count_day = calendar.monthrange(int(data['year']), month)[1]+1

                    day_range1 = datetime.date(int(data['year']), month, 1)
                    day_range2 = datetime.date(int(data['year']), month, count_day-1)

                    if data['building'] == 'true':
                        project_date = project.filter(Q(sites = site_db_one) &(
                            Q(start_building__gte=day_range1, end_building__lte=day_range2) |
                            Q(start_building__lte=day_range1, end_building__lte=day_range2, end_building__gte=day_range1) |
                            Q(start_building__gte=day_range1, end_building__gte=day_range2, start_building__lte=day_range2) |
                            Q(start_building__lte=day_range1, end_building__gte=day_range2)))


                    else:
                        project_date = project.filter(Q(sites = site_db_one) &(
                            Q(date_start__gte=day_range1, date_end__lte=day_range2) |
                            Q(date_start__lte=day_range1, date_end__lte=day_range2, date_end__gte=day_range1) |
                            Q(date_start__gte=day_range1, date_end__gte=day_range2, date_start__lte=day_range2) |
                            Q(date_start__lte=day_range1, date_end__gte=day_range2)))


                    if project_date.count()>0:



                        for day in range(1, count_day):
                            day_range = datetime.date(int(data['year']), month, day)

                            if data['building'] == 'true':
                                project_date_day = project.filter(sites = site_db_one, start_building__lte=day_range, end_building__gte=day_range)

                            else:
                                project_date_day = project.filter(sites = site_db_one, date_start__lte=day_range,
                                                                           date_end__gte=day_range)
                            if project_date_day.count():
                                temp_month_add = 0
                                for proj in project_date_day:
                                    if proj.rooms.count() and proj.rooms.filter(site=site['params']['id']).count():
                                        if day_range >= proj.rooms.get(
                                                site=site['params']['id']).date_start.date() and day_range <= proj.rooms.get(
                                                site=site['params']['id']).date_end.date():
                                            temp_month_add = 1
                                    else:
                                        temp_month_add = 1
                                temp[month] += temp_month_add
                        if temp[month]>0:
                            temp[month] =str(round(temp[month]/calendar.monthrange(int(data['year']), month)[1] *100))+'%'
                        else:
                            temp[month] = '0%'
                    else:
                        temp[month] = '0%'

                else:
                    # sites_local = Sites.objects.filter(**site['params'])
                    for s in site_db:
                        for day in range(1, calendar.monthrange(int(data['year']), month)[1] + 1):
                            day_range = datetime.date(int(data['year']), month, day)

                            if data['building'] == 'true':

                                project_date_day = project.filter(sites=s, start_building__lte=day_range,
                                                                   end_building__gte=day_range)
                            else:
                                project_date_day = project.filter(sites=s, date_start__lte=day_range,
                                                                           date_end__gte=day_range)
                            if project_date_day.count():
                                temp_month_add = 0
                                for proj in project_date_day:
                                    if proj.rooms.count() and proj.rooms.filter(site=s.id).count():
                                        if day_range>=proj.rooms.get(site=s.id).date_start.date() and day_range<=proj.rooms.get(site=s.id).date_end.date():
                                            temp_month_add = 1
                                    else:
                                        if not s.building_time:
                                            if day_range >= proj.date_start and day_range <= proj.date_end:
                                                temp_month_add = 1
                                        else:
                                            temp_month_add = 1
                                temp[month] += temp_month_add

                    temp[month] = str(round(temp[month] / (calendar.monthrange(int(data['year']), month)[1]*site_db.count()) * 100))+'%'
                    # print("--- %s seconds ---" % (time.time() - start_time))

            content.append(temp)
        return Response(content)