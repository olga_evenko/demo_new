from .models import ProjectReportTypeTwo, ProjectIndicatorsTypeTwo, Project, ProjectReportTypeTwoManager, ProjectReport
from .serializers import ProjectReportTypeTwoSerializer, ProjectReportTypeTwoManagerSerializerRO
from dateutil.relativedelta import relativedelta
import uuid
import datetime
from collections import OrderedDict
import requests
import json
# from .views import update_session as update_session
from django.conf import settings
from django.core.cache import cache
from one_c import manage_data

MONTH_NAME = ['Январь', 'Февраль', 'Март',
              'Апрель', 'Май', 'Июнь',
              'Июль', 'Август', 'Сентябрь',
              'Октябрь', 'Ноябрь', 'Декабрь']


def set_or_create_project_report_type_two(count_month, project_id):
    error = False
    status = 200
    if not count_month:
        error = True
        status = 400
    project = Project.objects.get(pk=project_id)
    date_start = project.date_end
    date = datetime.date(date_start.year, date_start.month, 1)
    date_start_report = date - relativedelta(months=count_month-1)
    project_indicators = ProjectIndicatorsTypeTwo.objects.filter(project=project)[:1]
    months_in_db = ProjectReportTypeTwo.objects.filter(project_indicators=project_indicators[0]).order_by('month_date')
    if months_in_db.count() > count_month:
        arr_delete = months_in_db[:months_in_db.count() - count_month]
        for i in arr_delete:
            i.delete()
    for i in range(count_month):
        ProjectReportTypeTwo.objects.update_or_create(month_name=MONTH_NAME[date_start_report.month - 1],
                                                      month_date=date_start_report,
                                                      project_indicators=project_indicators[0])
        date_start_report = date_start_report + relativedelta(months=1)
    return error, status


def get_serialized_data_project_report_type_two(project_id):
    error = False
    project_reports = ProjectReportTypeTwo.objects.filter(project_indicators__project=project_id).order_by('month_date')
    first_month = project_reports.first().month_date
    last_month = project_reports.last().month_date
    one_c_context = {}
    # TODO Подсчет расхождения с фин планом доходов
    one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, project_id))

    if one_c_data[0] != {}:
        for bill in one_c_data:
            date = datetime.datetime.strptime(bill['Дата'], '%d.%m.%Y %H:%M:%S').date()
            key = '%s.%s' % (str(date.year), str(date.month))
            if key in one_c_context:
                one_c_context[key].append(int(bill['ОплаченнаяСумма']))
            else:
                one_c_context[key] = [int(bill['ОплаченнаяСумма'])]

    data = ProjectReportTypeTwoSerializer(instance=project_reports, many=True, context={'one_c': one_c_context}).data

    s_dict = {'fact_money': 0, 'diff_money': 0, 'diff_perc_money': 0,
              'fact_ticket': 0, 'diff_ticket': 0, 'diff_perc_ticket': 0,
              'fact_processing_db': 0, 'perc_processing_db': 0, 'fin_plan': 0,
              'fin_plan_month': 0, 'ticket_plan': 0, 'plan_processing_db': 0}
    sum_fin = 0
    sum_fact = 0
    for i in data:
        if i.get('fact_money'):
            sum_fact += i.get('fact_money')
        else:
            sum_fact += 0
        if i.get('fin_plan'):
            sum_fin += i.get('fin_plan')
        else:
            sum_fin += 0
        i['diff_money'] = sum_fact - sum_fin

    for i in data:
        if i.get('ticket_plan'):
            s_dict['ticket_plan'] += int(i.get('ticket_plan'))
        if i.get('fact_processing_db'):
            s_dict['fact_processing_db'] += int(i.get('fact_processing_db'))
        if i.get('plan_processing_db'):
            s_dict['plan_processing_db'] += int(i.get('plan_processing_db'))
        if i.get('fin_plan_month'):
            s_dict['fin_plan_month'] += int(i.get('fin_plan_month'))
        if i.get('fact_money'):
            s_dict['fact_money'] += int(i.get('fact_money'))

    if not project_reports:
        error = True

    project_indicator = project_reports[0].project_indicators
    first_row = data[0].copy()
    for i in first_row:
        first_row[i] = None
    first_row['fin_plan'] = project_indicator.incomes
    first_row['ticket_plan'] = s_dict['ticket_plan']
    first_row['fact_processing_db'] = s_dict['fact_processing_db']
    first_row['plan_processing_db'] = s_dict['plan_processing_db']
    first_row['ticket_plan'] = s_dict['ticket_plan']
    first_row['fin_plan_month'] = s_dict['fin_plan_month']
    first_row['fact_money'] = s_dict['fact_money']
    first_row['diff_money'] = s_dict['diff_money']
    try:
        first_row['diff_money'] = first_row['fact_money'] - first_row['fin_plan']
    except:
        first_row['diff_money'] = 0
    try:
        first_row['perc'] = str(round((first_row['fact_money'] / first_row['fin_plan_month']) * 100, 2)) + ' %'
    except:
        first_row['perc'] = 0
    first_row['month_name'] = 'Итого'
    first_row['id'] = str(uuid.uuid4())
    data.insert(0, first_row)
    return data


def get_serialized_data_project_report_managers_type_two(report_id):
    project_reports_managers = ProjectReportTypeTwoManager.objects.filter(project_report=report_id)
    project_report = ProjectReportTypeTwo.objects.get(pk=report_id)
    project_id = project_report.project_indicators.project_id
    first_month = project_report.month_date
    last_month = first_month + relativedelta(months=1)
    one_c_context = {}
    # TODO Подсчет расхождения с фин планом доходов.
    one_c_data = json.loads(manage_data.getSumTable(first_month, last_month, project_id))
    if one_c_data[0] != {}:
        for bill in one_c_data:
            if bill['МенеджерИД'] in one_c_context:
                one_c_context[bill['МенеджерИД']].append(int(bill['ОплаченнаяСумма']))
            else:
                one_c_context[bill['МенеджерИД']] = [int(bill['ОплаченнаяСумма'])]
    # data = ProjectReportManagerSerializerRO(instance=project_reports_managers,
    #                                         many=True, context=one_c_context).data
    data = ProjectReportTypeTwoManagerSerializerRO(instance=project_reports_managers,
                                                   many=True, context=one_c_context).data
    if not project_reports_managers:
        project_report = ProjectReportTypeTwo.objects.get(pk=report_id)
        first_row = OrderedDict()
        first_row['ticket_plan_month'] = project_report.ticket_plan
        first_row['fin_plan_month'] = project_report.fin_plan_month
        first_row['plan_processing_db'] = project_report.plan_processing_db
        first_row['manager'] = 'Итого'
        first_row['id'] = str(uuid.uuid4())
        return [first_row, ]
    project_report = project_reports_managers[0].project_report
    s_dict = {'fact_ticket': 0, 'fact_processing_db': 0, 'fact': 0}
    for i in data:
        if i.get('fact_ticket'):
            s_dict['fact_ticket'] += int(i.get('fact_ticket'))
        if i.get('fact_processing_db'):
            s_dict['fact_processing_db'] += int(i.get('fact'))
        if i.get('fact'):
            s_dict['fact'] += int(i.get('fact'))
    first_row = data[0].copy()
    for i in first_row:
        first_row[i] = None
    # first_row = OrderedDict()
    # first_row['fin_plan'] = project_indicator.cash_receipts_exp
    # first_row['app_plan'] = project_indicator.count_app
    first_row['fact'] = s_dict['fact']
    first_row['ticket_plan_month'] = project_report.ticket_plan
    first_row['fin_plan_month'] = project_report.fin_plan_month
    first_row['fact_processing_db'] = s_dict['fact_processing_db']
    first_row['plan_processing_db'] = project_report.plan_processing_db
    try:
        first_row['perc'] = str(
            round((first_row['fact'] / first_row['fin_plan_month']) * 100, 2)) + ' %'
    except:
        first_row['perc'] = None
    try:
        first_row['perc_proccessing_db'] = str(
            round((first_row['fact_processing_db'] / first_row['plan_processing_db']) * 100, 2)) + ' %'
    except:
        first_row['perc_proccessing_db'] = None
    # first_row['manager'] = 'Итого'
    # first_row['perc_comp'] = str(round((first_row['fact_app'] / first_row['app_plan_month']) * 100, 2)) + ' %'
    first_row['id'] = str(uuid.uuid4())
    data.insert(0, first_row)
    return data
