# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2019-09-19 08:17
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('workers', '0001_initial'),
        ('auth', '0008_alter_user_username_max_length'),
        ('clients', '0002_auto_20190919_1117'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectreporttypetwomanager',
            name='manager',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_reports', to='workers.Manager'),
        ),
        migrations.AddField(
            model_name='projectreporttypetwomanager',
            name='project_report',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_report_manager', to='projects.ProjectReportTypeTwo', verbose_name='Менеджеры на месяц'),
        ),
        migrations.AddField(
            model_name='projectreporttypetwo',
            name='project_indicators',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_report', to='projects.ProjectIndicatorsTypeTwo', verbose_name='Показатели проекта'),
        ),
        migrations.AddField(
            model_name='projectreportmanager',
            name='manager',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workers.Manager'),
        ),
        migrations.AddField(
            model_name='projectreportmanager',
            name='project_report',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_report_manager', to='projects.ProjectReport', verbose_name='Менеджеры на месяц'),
        ),
        migrations.AddField(
            model_name='projectreport',
            name='project_indicators',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_report', to='projects.ProjectIndicators', verbose_name='Показатели проекта'),
        ),
        migrations.AddField(
            model_name='projectindicatorstypetwo',
            name='project',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='project_indicators_type_two', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='projectindicatorstypeone',
            name='project',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='project_indicators_type_one', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='projectindicators',
            name='project',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='project_indicators', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='project_type',
            name='default',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='default', to='projects.Buisness_unit', verbose_name='Значение по умолчанию'),
        ),
        migrations.AddField(
            model_name='project_type',
            name='director',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='direction', to=settings.AUTH_USER_MODEL, verbose_name='Руководитель дирекции'),
        ),
        migrations.AddField(
            model_name='project_type',
            name='head_direction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Project_type_head_direction', to='auth.Group', verbose_name='Группа руководителей дирекции'),
        ),
        migrations.AddField(
            model_name='project_type',
            name='head_managers',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Project_type_head_managers', to='auth.Group', verbose_name='Группа руководителей менеджеров'),
        ),
        migrations.AddField(
            model_name='project_type',
            name='manager_group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Project_type_manager', to='auth.Group', verbose_name='Группа менеджеров'),
        ),
        migrations.AddField(
            model_name='project',
            name='buisness_unit',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='projects.Buisness_unit', verbose_name='Бизнес-единица'),
        ),
        migrations.AddField(
            model_name='project',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='clients.Client'),
        ),
        migrations.AddField(
            model_name='project',
            name='project_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='project_type', to='projects.Project_type', verbose_name='Тип проекта'),
        ),
        migrations.AddField(
            model_name='project',
            name='sites',
            field=models.ManyToManyField(blank=True, related_name='sites', to='projects.Sites', verbose_name='Место проведения'),
        ),
        migrations.AddField(
            model_name='project',
            name='subjects',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='projects', to='projects.Subjects', verbose_name='Тематика проекта'),
        ),
        migrations.AddField(
            model_name='project',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectreporttypetwomanager',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectreporttypetwomanager',
            name='manager',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='workers.Manager'),
        ),
        migrations.AddField(
            model_name='historicalprojectreporttypetwomanager',
            name='project_report',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.ProjectReportTypeTwo'),
        ),
        migrations.AddField(
            model_name='historicalprojectreporttypetwo',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectreporttypetwo',
            name='project_indicators',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.ProjectIndicatorsTypeTwo'),
        ),
        migrations.AddField(
            model_name='historicalprojectreportmanager',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectreportmanager',
            name='manager',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='workers.Manager'),
        ),
        migrations.AddField(
            model_name='historicalprojectreportmanager',
            name='project_report',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.ProjectReport'),
        ),
        migrations.AddField(
            model_name='historicalprojectreport',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectreport',
            name='project_indicators',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.ProjectIndicators'),
        ),
        migrations.AddField(
            model_name='historicalprojectindicatorstypetwo',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectindicatorstypetwo',
            name='project',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='historicalprojectindicatorstypeone',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectindicatorstypeone',
            name='project',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='historicalprojectindicators',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalprojectindicators',
            name='project',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='historicalproject',
            name='buisness_unit',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.Buisness_unit'),
        ),
        migrations.AddField(
            model_name='historicalproject',
            name='client',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='clients.Client'),
        ),
        migrations.AddField(
            model_name='historicalproject',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalproject',
            name='project_type',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.Project_type'),
        ),
        migrations.AddField(
            model_name='historicalproject',
            name='subjects',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='projects.Subjects'),
        ),
        migrations.AddField(
            model_name='historicalproject',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contract',
            name='business_unit',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to='projects.Buisness_unit', verbose_name='Бизнес единица'),
        ),
        migrations.AddField(
            model_name='contract',
            name='subject',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to='projects.Subjects', verbose_name='Тематика мероприятия'),
        ),
        migrations.AddField(
            model_name='complexitybuildproject',
            name='complexity',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.ComplexityBuild', verbose_name='Сложность'),
        ),
        migrations.AddField(
            model_name='complexitybuildproject',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='complexity_build', to='projects.Project', verbose_name='Проект'),
        ),
        migrations.AddField(
            model_name='complexitybuildproject',
            name='site',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.Sites', verbose_name='Площадка'),
        ),
        migrations.AddField(
            model_name='cateringservice',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='catering_service', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='buisness_unit',
            name='head_direction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='business_unit_head_direction', to='auth.Group', verbose_name='Группа руководителей дирекции'),
        ),
        migrations.AddField(
            model_name='approval',
            name='created_user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='approval',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='approvals', to='projects.Project'),
        ),
        migrations.AddField(
            model_name='approval',
            name='report',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='approvals', to='projects.ProjectReport'),
        ),
        migrations.AlterUniqueTogether(
            name='projectreporttypetwomanager',
            unique_together=set([('manager', 'project_report')]),
        ),
        migrations.AlterUniqueTogether(
            name='projectreportmanager',
            unique_together=set([('manager', 'project_report')]),
        ),
    ]
