from django.contrib import admin
from django.apps import apps
from .models import *


class ProjectAdmin(admin.ModelAdmin):
    search_fields = ('name',)


for model in apps.get_app_config('projects').models.values():
    admin.site.register(model)
admin.site.unregister(Project)
admin.site.register(Project, ProjectAdmin)
# Register your models here.
