from rest_framework.permissions import *
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from projects.models import Project_type, Sites
from django.db.models import Q


class DjangoModelPermissionsOrNotView(DjangoModelPermissions):
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

    def has_object_permission(self, request, view, obj):
        if request.method.upper() not in settings.SAFE_METHODS and not request.user.is_superuser:
            is_manager = True
            try:
                request.user.managers
            except ObjectDoesNotExist:
                is_manager = False
            if obj._meta.model_name == 'project' and request.data.get('project_type'):
                project_type_id = int(request.data.get('project_type'))
                # if is_manager and (project_type_id,) \
                #         not in request.user.managers.head.direction.all().values_list('id'):
                #     return False
                project_type = Project_type.objects.get(pk=project_type_id)
                if project_type.type_project_type == 'kvs' and \
                        request.user.groups.filter(name='kvs').exists():
                    # if request.user == obj.user:
                    #     return True
                    if obj.status != 'working' and not request.user.groups.filter(
                            name='kvs_head').exists() and \
                            request.data.get('status') == 'working' and \
                            Sites.objects.filter(id__in=request.data.getlist('sites'),
                                                 check_head=True).exists():
                        return False

                    if is_manager and request.user.managers.head == project_type.director:
                        return True
                    elif not is_manager and request.user == project_type.director:
                        return True
                    if request.user.groups.filter(name='kvs_head').exists():
                        tmp = []
                        my_managers = request.user.my_managers.all()
                        tmp += list(my_managers)
                        perm_list = []
                        index = 0
                        if tmp:
                            while len(tmp) - 1 != index:
                                for i in range(index, len(tmp) - 1):
                                    if tmp[i].user.my_managers.all():
                                        tmp += tmp[i].user.my_managers.all()
                                    index += 1
                            perm_list = [x.user_id for x in tmp]
                        perm_list.append(request.user.id)
                        if obj.user_id in perm_list:
                            return True
                        return False
                if project_type.type_project_type == 'expo' and \
                        request.user.groups.filter(name='project_managers').exists():
                    return request.user == obj.user
                if project_type.type_project_type == 'ad' and request.user.groups.filter(name='head_ad').exists():
                    return True
                return False
        return True

    # def has_permission(self, request, view):
    #     # Workaround to ensure DjangoModelPermissions are not applied
    #     # to the root view when using DefaultRouter.
    #     if getattr(view, '_ignore_model_permissions', False):
    #         return True
    #
    #     if hasattr(view, 'get_queryset'):
    #         queryset = view.get_queryset()
    #     else:
    #         queryset = getattr(view, 'queryset', None)
    #
    #     assert queryset is not None, (
    #         'Cannot apply DjangoModelPermissions on a view that '
    #         'does not set `.queryset` or have a `.get_queryset()` method.'
    #     )
    #
    #     perms = self.get_required_permissions(request.method, queryset.model)
    #
    #     return (
    #         request.user and
    #         (is_authenticated(request.user) or not self.authenticated_users_only) and
    #         not set(perms).isdisjoint(request.user.get_group_permissions())
    #     )