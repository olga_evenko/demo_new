class WidgetConfiguratorModel {
  constructor({
    name,
    title,
    thumbnail,
    defaultData,
    customizableConfigs,
    defaultConfig,
  }) {
    this.name = name;
    this.title = title;
    this.thumbnail = thumbnail;
    this.defaultData = defaultData;
    this.customizableConfigs = customizableConfigs;
    this.defaultConfig = defaultConfig;
  }

  static fromArray(interfaceArr) {
    const widgetConfiguratorModels = interfaceArr.map(
      configuratorData => new WidgetConfiguratorModel(configuratorData)
    );
    return widgetConfiguratorModels;
  }
}

export default WidgetConfiguratorModel;
