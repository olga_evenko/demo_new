export default class {
  constructor({
    id, username, email, phoneNumber, firstName, lastName,
    middleName, organisation, position, isSuperuser, avatar, permissions, isActive, language,
  }) {
    this.id = id;
    this.username = username;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.firstName = firstName;
    this.lastName = lastName;
    this.middleName = middleName;
    this.organisation = organisation;
    if (!this.organisation) {
      this.organisation = {
        name: '',
        description: '',
      };
    }
    this.position = position;
    this.isSuperuse = isSuperuser;
    this.avatar = avatar;
    this.permissions = permissions;
    this.isActive = isActive;
    this.language = language;
  }

  get fullName() {
    if (!this.firstName || !this.middleName) {
      return `<a>this.username</a>`;
    }
    return `<a style='color: #2196f3'>${this.lastName} ${this.firstName[0]}. ${this.middleName[0]}.</a>`;
  }
}
