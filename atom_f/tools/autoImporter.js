function generateStoreName(fileName) {
  return fileName.slice(2, fileName.length - 3);
}

function collectFiles(requireModule) {
  const stores = {};
  requireModule.keys().forEach((fileName) => {
    if (fileName === './index.js') return;
    const storeName = generateStoreName(fileName);
    stores[storeName] = requireModule(fileName).default;
  });
  return stores;
}

export default collectFiles;
