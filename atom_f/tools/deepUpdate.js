function deepUpdate(o1, o2) {
  Object.keys(o2).forEach((p) => {
    if (typeof o2[p] === 'object' && typeof o1[p] === 'object') {
      deepUpdate(o1[p], o2[p]);
    }
    o1[p] = o2[p];
  });
  return o1;
}

export default deepUpdate;
