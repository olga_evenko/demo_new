function isObject(value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

function deepCopy(o1, o2) {
  Object.keys(o1).forEach((p) => {
    if (isObject(o1[p]) && isObject(o2[p])) {
      deepCopy(o1[p], o2[p]);
    } else if (o2[p] !== undefined) {
      o1[p] = o2[p];
    } else {
      o1[p] = '';
    }
  });
  return o1;
}

export default deepCopy;
