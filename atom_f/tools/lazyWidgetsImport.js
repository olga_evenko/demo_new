function importLazyComponent(ignoreFiles) {
  /**
   * Динамическая подгрузка компонентов
   * @param {String} path Путь до папки с компонентами от корневой директории
   * @param {Array} ignoreFiles, опциональный аргумент
   * @param {Array} requireModule список модулей относительно импортируемого пакета
   * @return {Array} Список компонентов для ленивого импорта
   */
  const lazyComponents = {};
  const requireModule = require.context(
    '@/components/widgets/widget/',
    false,
    /\.vue$/,
  );
  ignoreFiles = ignoreFiles || [];
  requireModule.keys().filter((fileName) => {
    if (fileName in ignoreFiles) {
      return;
    }
    const slicedFilename = fileName.slice(2, fileName.length - 4);
    // TODO: сделать импорт с динамическим путем.
    lazyComponents[slicedFilename] = () => import(`@/components/widgets/widget/${slicedFilename}`);
  });
  return lazyComponents;
}

export default importLazyComponent;
