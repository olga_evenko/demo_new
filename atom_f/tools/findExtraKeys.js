function _findAllExtraKeys() {}

function _findExtraKeys(o1, o2) {
  const extraKeys = [];

  function searchExtraKeys(o1, o2) {
    o2 = o2 || {};
    Object.keys(o1).forEach((k) => {
      if (o2[k] === undefined) {
        extraKeys.push(k);
      }
      if (typeof o1[k] === 'object') {
        searchExtraKeys(o1[k], o2[k]);
      }
    });
  }

  searchExtraKeys(o1, o2);
  return extraKeys;
}

function findExtraKeys(o1, o2, mode) {
  /**
   * Глубокий поиск ключей, которые присутствуют в 1 объекте
   * но отсутствуют во 2.
   * @param  {o1} object 1 объект
   * @param  {o2} object 2 объект
   * @param {mode} number режим операции:
   *                      1 - ищет лишние свойста в 1 объекте
   *                      2 - ищет лишние свойства во 2 объекте
   *                      * - ищет свйоства которые есть либо в 1 либо во 2 объекте
   */
  mode = mode || 1;
  let extraKeys = null;
  switch (mode) {
    case 1:
      extraKeys = _findExtraKeys(o1, o2);
      break;
    case 2:
      extraKeys = _findExtraKeys(o2, o1);
      break;
    case '*':
      extraKeys = _findAllExtraKeys(o1, o2);
      break;
    default:
      throw Error('Not supported mode.');
  }
  return extraKeys;
}

export default findExtraKeys;
