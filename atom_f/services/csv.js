export default {
  export(rows, columns, title) {
    let csvContent = 'data:text/csv;charset=utf-8,';
    for (const row of Array.from(rows)) {
      let content = '';
      if (!columns) {
        content = Object.keys(row)
          .map(k => row[k])
          .join(';');
      } else {
        Object.keys(row).map((k) => {
          if (columns.indexOf(k) >= 0) {
            content += `${row[k]};`;
          }
        });
      }
      csvContent += `${content}\r\n`;
    }
    const encodedUri = encodeURI(csvContent);
    const link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', `${title}.csv`);
    link.click();
  },
};
