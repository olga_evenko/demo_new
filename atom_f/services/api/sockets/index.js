import incidents from './incidents';
import events from './events';
import system from './system';

class Socket {
  constructor(path, params) {
    this.params = params;
    this.socket = new WebSocket(`${process.env.VUE_APP_WS_SERVER}${path}`);
  }

  map(onMessage, onClose, onError) {
    this.socket.onmessage = (event) => {
      onMessage(JSON.parse(event.data), this.socket);
    };
    this.socket.onopen = () => {
      const params = this.params ? JSON.stringify(this.params) : true;
      this.socket.send(params);
    };
    this.socket.onerror = (error) => {
      if (onError) {
        onError(error);
      }
    };
    this.socket.onclose = (event) => {
      if (onClose) {
        onClose(event);
      }
    };
  }
}

export default {
  incidents: incidents(Socket),
  events: events(Socket),
  system: system(Socket),
};
