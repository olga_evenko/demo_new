export default Socket => ({
  loadScatterData: () => new Socket('/ws/incidents/scatter/'),
});
