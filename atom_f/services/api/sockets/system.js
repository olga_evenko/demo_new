export default Socket => ({
  subscribeToSystem: () => new Socket('/ws/system/'),
});
