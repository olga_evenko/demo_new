export default Socket => ({
  search: params => new Socket('/ws/events/search/', params),
});
