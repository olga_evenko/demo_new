import axios from 'axios';
import cookies from 'vue-cookies';
import { converters } from './index';

axios.interceptors.request.use((config) => {
  const lang = cookies.get('lang');
  if (typeof lang !== 'undefined') {
    config.headers.common['Accept-Language'] = lang;
  }
  return config;
});

export default {
  auth(data) {
    return axios({
      url: `${process.env.VUE_APP_API_URL}/api/token-auth/`,
      method: 'post',
      data,
      withCredentials: true,
    });
  },
  getProjectStatus() {
    return axios({
      url: `${process.env.VUE_APP_API_URL}/api/init-project/`,
      method: 'get',
    });
  },
  initProject(superuserInfo) {
    return axios({
      url: `${process.env.VUE_APP_API_URL}/api/init-project/`,
      method: 'post',
      data: superuserInfo,
    });
  },
  axios(axiosData) {
    const token = cookies.get('token');
    if (!token) {
      // eslint-disable-next-line no-throw-literal
      throw 'TokenNotFound';
    }
    axiosData.headers = {
      Authorization: `JWT ${token}`,
      ...axiosData.headers,
    };
    return axios(axiosData);
  },
  getPersonalInfo() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/personal-info/`,
      method: 'get',
    });
  },
  getUsers(offset, limit) {
    limit = limit || 10;
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/users/`,
      method: 'get',
      params: {
        offset,
        limit,
      },
    });
  },
  getUserDetail(userId) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/users/${userId}/`,
      method: 'get',
    });
  },
  tryRefreshToken(token) {
    return axios({
      url: `${process.env.VUE_APP_API_URL}/api/token-refresh/`,
      method: 'post',
      data: {
        token,
      },
    });
  },
  generatePdf(usersId) {
    let query = '?';
    usersId.forEach((id) => {
      query += `users_id=${id}&`;
    });
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/users-pdf/${query}`,
      method: 'get',
    });
  },
  async createDashboard(data) {
    const dashboard = JSON.parse(JSON.stringify(data));
    const convertedDashboard = converters.forApi.convertDashboard(dashboard);
    const rspns = await this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/dashboards/`,
      method: 'post',
      data: convertedDashboard,
    });
    const recievedDashboard = converters.forClient.convertDashboard(rspns.data);
    return recievedDashboard;
  },
  getDashboardTypes() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/dashboard-types/`,
      method: 'get',
    });
  },
  deleteDashboard(id) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/dashboards/${id}/`,
      method: 'delete',
    });
  },
  async getDashboards() {
    const data = await this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/dashboards/`,
      method: 'get',
    });
    const convertedData = converters.forClient.convertDashboards(
      data.data.results,
    );
    return convertedData;
  },
  updateDashboard(dashboard) {
    const data = converters.forApi.convertDashboard(dashboard);
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/dashboards/${data.id}/`,
      method: 'patch',
      data,
    });
  },
  async createWidget(widget, dashboardId) {
    let convertedWidget = converters.forApi.convertWidget(widget);
    const rspns = await this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/widgets/`,
      method: 'post',
      data: {
        ...convertedWidget,
        dashboardId,
      },
    });

    convertedWidget = converters.forClient.convertWidget(rspns.data);
    return convertedWidget;
  },
  deleteWidget(id) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/widgets/${id}/`,
      method: 'delete',
    });
  },
  updateWidget(widget) {
    const convertedWidget = converters.forApi.convertWidget(widget);
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/widgets/${widget.id}/`,
      method: 'patch',
      data: convertedWidget,
    });
  },
  swapDashboards(dashboard1, dashboard2) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/dashboards/${
        dashboard1.id
      }/swap_positions/`,
      method: 'patch',
      data: {
        id: dashboard2.id,
      },
    });
  },
  getTickets() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/tickets/`,
      method: 'get',
    });
  },
  resetPassword(data) {
    return axios({
      url: `${process.env.VUE_APP_API_URL}/api/reset-password/`,
      method: 'post',
      data,
    });
  },
  createUserProfile(data) {
    const formData = new FormData();
    const temp_user = { ...data };

    Object.keys(temp_user).forEach((k) => {
      if (temp_user[k] && temp_user[k].constructor === Object) {
        temp_user[k] = JSON.stringify(temp_user[k]);
      }
    });

    Object.keys(temp_user).forEach((k) => {
      formData.append(k, temp_user[k]);
    });
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/personal-info/`,
      method: 'post',
      data: formData,
      header: { 'Content-Type': 'multipart/form-data' },
    });
  },
  updateUserProfile(data) {
    const formData = new FormData();
    const temp_user = { ...data };

    Object.keys(temp_user).forEach((k) => {
      if (temp_user[k] && temp_user[k].constructor === Object) {
        temp_user[k] = JSON.stringify(temp_user[k]);
      }
    });

    Object.keys(temp_user).forEach((k) => {
      formData.append(k, temp_user[k]);
    });

    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/personal-info/`,
      method: 'put',
      data: formData,
      header: { 'Content-Type': 'multipart/form-data' },
    });
  },
  deleteUserProfile(data) {
    const formData = new FormData();
    formData.user_id = data.arr;
    formData.action = data.action;
    debugger
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/personal-info/`,
      method: 'delete',
      data,
    });
  },
  changePassword(data) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/personal-info/change-password/`,
      data,
      method: 'patch',
    });
  },
  updateUser(data) {
    const formData = new FormData();
    const temp_user = { ...data };

    Object.keys(temp_user).forEach((k) => {
      if (temp_user[k] && temp_user[k].constructor === Object) {
        temp_user[k] = JSON.stringify(temp_user[k]);
      }
    });

    Object.keys(temp_user).forEach((k) => {
      formData.append(k, temp_user[k]);
    });
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/users/${data.id}/`,
      data: formData,
      method: 'put',
    });
  },
  deactivateUser(userId) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/users/${userId}/`,
      method: 'delete',
    });
  },
  updateUserPermissions(data) {
    const { userId, permissions } = data;
    return this.axios({
      url: `${
        process.env.VUE_APP_API_URL
      }/api/users/${userId}/change-permissions/`,
      method: 'patch',
      data: {
        permissions,
      },
    });
  },
  getAvailablePermissions() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/user-permissions/`,
      method: 'get',
    });
  },
  initEventsSearch(data) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/search/`,
      method: 'post',
      data,
    });
  },
  stopEventsSearch(task) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/stop-search/`,
      method: 'post',
      data: { task },
    });
  },
  fetchEvents(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/`,
      method: 'get',
      params,
    });
  },
  fetchCachedSearchResults() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/cached-search/`,
      method: 'get',
    });
  },
  extractCachedChunks(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/cached-chunks/`,
      method: 'get',
      params,
    });
  },
  createOrganisationPreset(data) {
    const formData = new FormData();
    Object.keys(data).forEach(fieldName => formData.append(fieldName, data[fieldName]));
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/organisation-presets/`,
      method: 'post',
      data: formData,
    });
  },
  async getOrganisationPresets() {
    const data = await this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/organisation-presets/`,
      method: 'get',
    });
    const convertedData = converters.forClient.convertPresets(
      data.data.results,
    );
    return convertedData;
  },
  changePresetRating(id, rating) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/users-presets-rating/`,
      method: 'post',
      data: {
        preset: id,
        rating,
      },
    });
  },
  uploadOrganisationImage(file) {
    const data = new FormData();
    data.append('file', file);
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/upload-organisation-file/`,
      method: 'post',
      data,
    });
  },
  loadAssets({ offset, limit }) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/`,
      method: 'get',
      params: {
        offset,
        limit,
      },
    });
  },
  searchAssets(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/`,
      method: 'get',
      params,
    });
  },
  loadAssetsHistory() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/search-history/`,
      method: 'get',
    });
  },
  resetAssetsHistory() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/search-history/`,
      method: 'delete',
    });
  },
  loadAssetsMetaInfo() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/meta-info/`,
      method: 'get',
    });
  },
  loadPopularAssetsQueries() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/popular-queries/`,
      method: 'get',
    });
  },
  getAssetById(id) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/assets/${id}/`,
      method: 'get',
    });
  },
  loadPersonalVisualConfig() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/user-web-config/`,
      method: 'get',
    });
  },
  savePersonalVisualConfig(webConfig) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/user-web-config/`,
      method: 'post',
      data: {
        webConfig,
      },
    });
  },
  loadTop10DeviceByEvents() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/top-activity/`,
      method: 'get',
    });
  },
  updateWidgetPreset(id, preset) {
    const formData = new FormData();
    Object.keys(preset).forEach(fieldName => formData.append(fieldName, preset[fieldName]));
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/organisation-presets/${id}/`,
      method: 'patch',
      data: formData,
    });
  },
  loadDbTables() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/database-management/db-tables/`,
      method: 'get',
    });
  },
  loadBackupTasks() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/database-management/`,
      method: 'get',
    });
  },
  loadBackups(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/database-backups/`,
      method: 'get',
      params,
    });
  },
  loadBackupTasks(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/database-management/`,
      methods: 'get',
      params,
    });
  },
  createBackupTask(data) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/database-management/`,
      method: 'post',
      data,
    });
  },
  deleteBackups(data) {
    return this.axios({
      url: `${
        process.env.VUE_APP_API_URL
      }/api/database-backups/delete-backups/`,
      method: 'delete',
      data,
    });
  },
  deleteBackupTasks(data) {
    return this.axios({
      url: `${
        process.env.VUE_APP_API_URL
      }/api/database-management/delete-tasks/`,
      method: 'delete',
      data,
    });
  },
  loadIncidents({ offset, limit }, params) {
    limit = limit || 10;
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/incidents/`,
      // url: 'http://127.0.0.1:8083/incidents/',
      method: 'GET',
      params: {
        offset,
        limit,
        ...params,
      },
    });
  },
  loadActivity({ offset, limit }, params) {
    limit = limit || 10;
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/incidents/?type=activity`,
      method: 'GET',
      params: {
        offset,
        limit,
        ...params,
      },
    });
  },
  loadIncidentScatterData() {
    return this.axios({
      method: 'get',
      url: `${process.env.VUE_APP_API_URL}/api/incidents/scatter_data/`,
    });
  },
  loadActivityScatterData() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/incidents/scatter_data/`,
      method: 'get',
      params: {
        type: 'activity',
      },
    });
  },
  loadEventsByIncidentId(id, { limit, offset }) {
    limit = limit || 10;
    offset = offset || 0;
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/events/by-incident/`,
      params: {
        id,
        limit,
        offset,
      },
    });
  },
  loadUpdates(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/system/`,
      params,
    });
  },
  applyUpdate() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/system/apply-update/`,
      method: 'post',
    });
  },
  loadAttackMatrix() {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/attacks-matrix/`,
      method: 'get',
    });
  },
  loadPatterns(params) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/patterns/`,
      method: 'get',
      params,
    });
  },
  updatePattern(pattern) {
    return this.axios({
      url: `${process.env.VUE_APP_API_URL}/api/patterns/${pattern.id}/`,
      method: 'put',
      data: pattern,
    });
  },
};
