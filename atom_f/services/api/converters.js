const converters = {
  forClient: {
    convertDashboards(dashboards) {
      dashboards.forEach((dasboard, i) => {
        dashboards[i] = this.convertDashboard(dasboard);
      });
      return dashboards;
    },
    convertDashboard(dashboard) {
      /*
      * Приведение полученных данных с АПИ сервера к валидной структуре.
      */
      dashboard.widgets.forEach((w, i) => {
        dashboard.widgets[i] = this.convertWidget(w);
      });
      return dashboard;
    },
    convertPresets(widgets) {
      const convertedWidgets = widgets.map((p => this.convertPreset(p)));
      return convertedWidgets;
    },
    convertPreset(preset) {
      return {
        id: preset.id,
        dataWidget: preset.config,
        title: preset.title,
        description: preset.description,
        rating: preset.rating,
        owner: preset.owner,
        type: preset.type,
        thumbnail: preset.thumbnail,
        userRating: preset.userRating,
        name: preset.name,
      };
    },
    convertWidget(widget) {
      const newWidget = JSON.parse(widget.config);
      newWidget.i = widget.id;
      Object.keys(widget).forEach((w) => {
        if (w !== 'config') {
          newWidget[w] = widget[w];
        }
      });
      return newWidget;
    },
  },
  forApi: {
    convertDashboard(dashboard) {
      if (!dashboard.widgets) {
        return dashboard;
      }
      const copiedDshb = dashboard;
      copiedDshb.widgets.forEach((w, i) => {
        copiedDshb.widgets[i] = this.convertWidget(w);
      });
      return copiedDshb;
    },
    convertWidget(widget) {
      const newWidget = {
        title: widget.title,
        description: widget.description,
        icon: widget.icon,
        type: widget.type,
        apiUrl: widget.apiUrl,
        id: widget.id,
        presetId: widget.presetId,
        config: {},
      };
      Object.keys(widget).forEach((k) => {
        if (!(k in newWidget)) {
          newWidget.config[k] = widget[k];
        }
      });
      // Дабы избежать конвертации на сервере
      newWidget.config = JSON.stringify(newWidget.config);
      return newWidget;
    },
  },
};
export { converters };
