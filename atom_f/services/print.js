class Print {
  constructor(html, style, title = '') {
    this.html = html;
    this.style = style;
    this.title = title;
  }

  print() {
    const mywindow = window.open('', 'PRINT', 'height=400,width=600');
    mywindow.document.write(`<html><head><title>${document.title}</title>`);
    mywindow.document.write(this.style);
    mywindow.document.write(this.title);
    mywindow.document.write(this.html);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
  }
}

export default class {
  constructor(rows, header = [], columns = null) {
    this.rows = rows;
    this.columns = columns;
    this.header = header;
  }

  print() {
    const html = this.generateHtml();
    const style = `<style type="text/css">
                    table { width: 100%; }
                    .sorting { text-align: left; }
                    th, tr { border-bottom: 1px solid #ddd }
                    * { font-family: arial }
                   </style>`;

    const print = new Print(html, style);
    print.print();
  }

  generateHtml() {
    let html = '<table><tr>';
    for (const headerColumn of this.header) {
      html += `<th class="sorting">${headerColumn}</th>`;
    }
    html += '</tr>';
    for (const row of Array.from(this.rows)) {
      html += '<tr>';
      for (const column of this.columns) {
        html += `<td>${row[column] ? row[column] : ''}</td>`;
      }
      html += '</tr>';
    }
    html += '</table>';
    return html;
  }
}
