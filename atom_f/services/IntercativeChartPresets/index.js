const requireMoodule = require.context('.', false, /\w+\.js/);

const interactivePresets = [];
const initializers = {};

requireMoodule.keys().forEach((m) => {
  if (m !== './index.js') {
    interactivePresets.push(requireMoodule(m).default);
    const key = m.slice(2, m.length - 3);
    initializers[key] = requireMoodule(m).initData;
  }
});
export default interactivePresets;
export { initializers };
