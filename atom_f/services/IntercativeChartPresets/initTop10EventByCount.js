import api from '@/services/api';

const widget = {
  title: 'events',
  type: 'Echart',
  dataLoader: 'initTop10EventByCount',
  updateInterval: 5000,
  description: 'Топ 10 device по количеству событиям',
  dataWidget: {
    // Define colors
    color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

    // Global text styles
    textStyle: {
      fontFamily: 'Roboto, Arial, Verdana, sans-serif',
      fontSize: 13,
    },

    // Chart animation duration
    animationDuration: 750,

    // Setup grid
    grid: {
      left: 0,
      right: 40,
      top: 35,
      bottom: 0,
      containLabel: true,
    },

    // Add legend
    // legend: {
    //   data: ['Evaporation', 'Precipitation'],
    //   itemHeight: 8,
    //   itemGap: 20,
    //   textStyle: {
    //     padding: [0, 5],
    //   },
    // },

    // Add tooltip
    tooltip: {
      trigger: 'axis',
      backgroundColor: 'rgba(0,0,0,0.75)',
      padding: [10, 15],
      textStyle: {
        fontSize: 13,
        fontFamily: 'Roboto, sans-serif',
      },
    },

    // Horizontal axis
    xAxis: {
      type: 'category',
      data: new Array(10).fill(''),
    },

    // Vertical axis
    yAxis: [
      {
        type: 'value',
        axisLabel: {
          color: '#333',
        },
        axisLine: {
          lineStyle: {
            color: '#999',
          },
        },
        splitLine: {
          lineStyle: {
            color: ['#eee'],
          },
        },
        splitArea: {
          show: true,
          areaStyle: {
            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)'],
          },
        },
      },
    ],

    // Add series
    series: {
      name: 'Evaporation',
      type: 'bar',
      data: new Array(10).fill({ value: 0, itemStyle: {} }),
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'top',
            textStyle: {
              fontWeight: 500,
            },
          },
        },
      },
      markLine: {
        data: [{ type: 'average', name: 'Average' }],
      },
    },
  },
};

export default widget;

export const initData = async (data) => {
  const rspns = await api.loadTop10DeviceByEvents();
  const option = { ...data };

  option.series.data = [];

  rspns.data.forEach((v, i) => {
    const { count } = v;
    const { device } = v;
    option.xAxis.data[i] = device;
    let color = '#2ec7c9';
    if (i % 2) {
      color = '#b6a2de';
    }
    option.series.data[i] = {
      value: count,
      itemStyle: {
        color,
      },
    };
  });
};
