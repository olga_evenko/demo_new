const apiAudio = {
  get success() {
    return new Audio(`${process.env.VUE_APP_API_URL}/static/audio/login.mp3`);
  },
  get error() {
    return new Audio(`${process.env.VUE_APP_API_URL}/static/audio/error.mp3`);
  },
};

export default apiAudio;
