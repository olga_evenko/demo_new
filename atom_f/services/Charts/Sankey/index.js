import presets from './presets';

export default {
  name: 'Sankey',
  icon: 'sankey.svg',
  description: 'sankey chart',
  title: 'sankey',
  presets,
};
