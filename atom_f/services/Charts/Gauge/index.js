import presets from './presets';

export default {
  name: 'Gauge',
  icon: 'gauge.svg',
  description: 'gauge chart',
  title: 'gauge',
  presets,
};
