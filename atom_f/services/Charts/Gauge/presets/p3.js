const option = {
  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Add title
  title: {
    text: 'Server resources usage',
    subtext: 'Random demo data',
    left: 'center',
    textStyle: {
      fontSize: 17,
      fontWeight: 500,
      color: '#008acd',
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'item',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    formatter: '{a} <br/>{b} : {c}%',
  },

  // Add series
  series: [
    {
      name: 'Memory usage',
      type: 'gauge',
      center: ['50%', '57.5%'],
      radius: '80%',
      startAngle: 150,
      endAngle: -150,
      axisLine: {
        lineStyle: {
          color: [
            [0.2, 'lightgreen'],
            [0.4, 'orange'],
            [0.8, 'skyblue'],
            [1, '#ff4500'],
          ],
          width: 30,
        },
      },
      axisTick: {
        splitNumber: 5,
        length: 5,
        lineStyle: {
          color: '#fff',
        },
      },
      axisLabel: {
        formatter(v) {
          switch (`${v}`) {
            case '10':
              return 'Idle';
            case '30':
              return 'Low';
            case '60':
              return 'Normal';
            case '90':
              return 'High';
            default:
              return '';
          }
        },
      },
      splitLine: {
        length: 35,
        lineStyle: {
          color: '#fff',
        },
      },
      pointer: {
        width: 5,
      },
      title: {
        offsetCenter: ['-81%', -20],
        textStyle: {
          fontSize: 13,
        },
      },
      detail: {
        offsetCenter: ['-80%', 10],
        formatter: '{value}%',
        textStyle: {
          fontSize: 25,
          fontWeight: 500,
        },
      },
      data: [{ value: 50, name: 'Memory usage' }],
    },
  ],
};
export default option;
