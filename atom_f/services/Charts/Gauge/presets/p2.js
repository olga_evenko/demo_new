const option = {
  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Add title
  title: {
    text: 'Server resources usage',
    subtext: 'Random demo data',
    left: 'center',
    textStyle: {
      fontSize: 17,
      fontWeight: 500,
      color: '#008acd',
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'item',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    formatter: '{a} <br/>{b}: {c}%',
  },

  // Add series
  series: [
    {
      name: 'Memory usage',
      type: 'gauge',
      center: ['50%', '62%'],
      radius: '90%',
      detail: { formatter: '{value}%' },
      axisLine: {
        lineStyle: {
          color: [[0.2, '#2ec7c9'], [0.8, '#5ab1ef'], [1, '#d87a80']],
          width: 15,
        },
      },
      axisTick: {
        splitNumber: 10,
        length: 20,
        lineStyle: {
          color: 'auto',
        },
      },
      splitLine: {
        length: 22,
        lineStyle: {
          color: 'auto',
        },
      },
      title: {
        offsetCenter: [0, '60%'],
        textStyle: {
          fontSize: 13,
        },
      },
      detail: {
        offsetCenter: [0, '45%'],
        formatter: '{value}%',
        textStyle: {
          fontSize: 27,
          fontWeight: 500,
        },
      },
      pointer: {
        width: 5,
      },
      data: [{ value: 50, name: 'Memory usage' }],
    },
  ],
};
export default option;
