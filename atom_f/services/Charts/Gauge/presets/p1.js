import echarts from 'echarts';

const option = {
  tooltip: {
    // mode: 'average',
    intersect: true,
    // position: 'nearest',
    // position: [10, 10],
    formatter: '{a} <br/>{b} : {c}%',
  },
  toolbox: {
    feature: {
      restore: { title: 'Обновить' },
      saveAsImage: { title: 'Сохранить' },
    },
  },
  series: [
    {
      name: 'Пример',
      type: 'gauge',
      radius: '98%',
      detail: { formatter: '{value}%' },
      data: [{ value: 50, name: 'Тест' }],
    },
  ],
};
export default option;
