import echarts from 'echarts';

const option = {

  // Define colors
  color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  animationDuration: 750,
  grid: {
    left: 0,
    right: 40,
    top: 35,
    bottom: 0,
    containLabel: true,
  },

  // Add legend
  legend: {
    data: ['New orders', 'In progress', 'Closed deals'],
    itemHeight: 8,
    itemGap: 20,
  },

  // Add tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [{
    type: 'value',
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      lineStyle: {
        color: '#eee',
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)'],
      },
    },
  }],

  // Add series
  series: [
    {
      name: 'Closed deals',
      type: 'line',
      data: [10, 12, 21, 54, 260, 830, 710],
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
    },
    {
      name: 'In progress',
      type: 'line',
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      data: [30, 182, 434, 791, 390, 30, 10],
    },
    {
      name: 'New orders',
      type: 'line',
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      data: [1320, 1132, 601, 234, 120, 90, 20],
    },
  ],
};

export default option;
