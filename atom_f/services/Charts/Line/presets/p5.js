import echarts from 'echarts';

const option = {

  // Define colors
  color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Chart animation duration
  animationDuration: 750,

  // Setup grid
  grid: {
    left: 0,
    right: 40,
    top: 35,
    bottom: 0,
    containLabel: true,
  },

  // Add legend
  legend: {
    data: ['Internet Explorer', 'Safari', 'Firefox', 'Chrome'],
    itemHeight: 8,
    itemGap: 20,
  },

  // Add tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [{
    type: 'value',
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      lineStyle: {
        color: '#eee',
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)'],
      },
    },
  }],

  // Add series
  series: [
    {
      name: 'Internet Explorer',
      type: 'line',
      stack: 'Total',
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
      data: [120, 132, 101, 134, 490, 230, 210],
    },
    {
      name: 'Safari',
      type: 'line',
      stack: 'Total',
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
      data: [150, 1232, 901, 154, 190, 330, 810],
    },
    {
      name: 'Firefox',
      type: 'line',
      stack: 'Total',
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
      data: [320, 1332, 1801, 1334, 590, 830, 1220],
    },
    {
      name: 'Chrome',
      type: 'line',
      stack: 'Total',
      areaStyle: {
        normal: {
          opacity: 0.25,
        },
      },
      smooth: true,
      symbolSize: 7,
      itemStyle: {
        normal: {
          borderWidth: 2,
        },
      },
      data: [820, 1632, 1901, 2234, 1290, 1330, 1320],
    },
  ],
};

export default option;
