import presets from './presets';

export default {
  name: 'Line',
  icon: 'line.svg',
  description: 'line chart',
  title: 'eline',
  presets,
};
