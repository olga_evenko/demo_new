import presets from './presets';

export default {
  name: 'Calendar',
  icon: 'calendar.png',
  description: 'calendar chart',
  title: 'calendar',
  presets,
};
