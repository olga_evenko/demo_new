import echarts from 'echarts';

function getVirtulData(year) {
  year = year || '2017';
  const date = +echarts.number.parseDate(`${year}-01-01`);
  const end = +echarts.number.parseDate(`${+year + 1}-01-01`);
  const dayTime = 3600 * 24 * 1000;
  const data = [];
  for (let time = date; time < end; time += dayTime) {
    data.push([
      echarts.format.formatTime('yyyy-MM-dd', time),
      Math.floor(Math.random() * 10000),
    ]);
  }
  return data;
}

const option = {
  // Add title
  title: {
    text: 'Github commits',
    subtext: 'Open source information',
    left: 'center',
    textStyle: {
      fontSize: 17,
      fontWeight: 500,
    },
    subtextStyle: {
      fontSize: 12,
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'item',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
  },

  // Add legend
  legend: {
    orient: 'vertical',
    top: 'center',
    left: 0,
    data: ['IE', 'Opera', 'Safari', 'Firefox', 'Chrome'],
    itemHeight: 8,
    itemWidth: 8,
  },

  // Visual map
  visualMap: {
    min: 0,
    max: 10000,
    type: 'piecewise',
    orient: 'horizontal',
    left: 'center',
    bottom: 0,
    textStyle: {
      fontSize: 12,
    },
  },

  // Calendar
  calendar: {
    top: 80,
    left: 30,
    right: 5,
    cellSize: ['auto', 25],
    range: '2016',
    itemStyle: {
      normal: {
        borderWidth: 1,
        borderColor: '#fff',
      },
    },
    splitLine: {
      lineStyle: {
        color: '#333',
        width: 2,
      },
    },
    yearLabel: { show: false },
  },

  // Add series
  series: [
    {
      type: 'heatmap',
      coordinateSystem: 'calendar',
      data: getVirtulData(2016),
    },
  ],
};

export default option;
