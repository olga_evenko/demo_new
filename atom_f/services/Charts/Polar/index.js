import presets from './presets';

export default {
  name: 'Polar',
  icon: 'sunburst.svg',
  description: 'polar chart',
  title: 'polar',
  presets,
};
