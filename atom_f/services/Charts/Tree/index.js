import presets from './presets';

export default {
  name: 'Tree',
  icon: 'tree.svg',
  description: 'tree chart',
  title: 'tree',
  presets,
};
