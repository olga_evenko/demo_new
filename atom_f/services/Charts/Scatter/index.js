import presets from './presets';

export default {
  name: 'Scatter',
  icon: 'scatter.svg',
  description: 'scatter chart',
  title: 'scatter',
  presets,
};
