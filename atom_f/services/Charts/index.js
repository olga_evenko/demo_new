import abstractChart from './abstractChart';

export { isChartValid } from './utils';

const charts = {};

const requireModule = require.context('.', true);
requireModule.keys().forEach((k) => {
  const r = k.match(/\.\/(\w*)\/index/);
  if (r) {
    const chart = {
      ...abstractChart,
      ...requireModule(k).default,
      type: requireModule(k).default.type ? requireModule(k).default.type : 'Echart',
    };
    const name = r[1];
    charts[name] = chart;
  }
});
export default charts;
