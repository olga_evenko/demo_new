import presets from './presets';

export default {
  name: 'Table',
  icon: 'table.png',
  description: 'table',
  title: 'table',
  type: 'Table',
  presets,
};
