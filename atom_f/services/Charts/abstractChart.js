export default {
  icon: 'line.svg',
  type: 'Echarts',
  getFirstPreset() {
    return this.presets ? this.presets[0] : null;
  },
};
