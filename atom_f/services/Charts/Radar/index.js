import presets from './presets';

export default {
  name: 'Radar',
  icon: 'radar.svg',
  description: 'radar chart',
  title: 'radar',
  presets,
};
