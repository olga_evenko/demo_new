const option = {
  title: {
    text: '多雷达图'
  },
  tooltip: {
    trigger: 'axis'
  },
  legend: {
    x: 'center',
    data: ['某软件', '某主食手机', '某水果手机', '降水量', '蒸发量']
  },
  radar: [
    {
      indicator: [
        { text: '品牌', max: 100 },
        { text: '内容', max: 100 },
        { text: '可用性', max: 100 },
        { text: '功能', max: 100 }
      ],
      center: ['50%', '50%'],
      radius: 80
    },
  ],
  series: [
    {
      type: 'radar',
      tooltip: {
        trigger: 'item'
      },
      itemStyle: { normal: { areaStyle: { type: 'default' } } },
      data: [
        {
          value: [60, 73, 85, 40],
          name: '某软件'
        }
      ]
    },
  ]
};

export default option;
