import p1 from './presets/p1';
import p2 from './presets/p2';
import p3 from './presets/p3';

export default [p1, p2, p3];
