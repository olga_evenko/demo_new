import presets from './presets';

export default {
  name: 'Graph',
  icon: 'graph.svg',
  description: 'graph chart',
  title: 'graph',
  presets,
};
