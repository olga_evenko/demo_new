const option = {

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Chart animation duration
  animationDuration: 750,

  // Setup grid
  grid: {
    left: 0,
    right: 5,
    top: 55,
    bottom: 0,
    containLabel: true,
  },

  // Add legend
  legend: {
    data: [
      'Version 1.7 - 2k data', 'Version 1.7 - 2w data', 'Version 1.7 - 20w data', '',
      'Version 2.0 - 2k data', 'Version 2.0 - 2w data', 'Version 2.0 - 20w data',
    ],
    itemHeight: 2,
    itemGap: 8,
    textStyle: {
      padding: [0, 10],
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    axisPointer: {
      type: 'shadow',
      shadowStyle: {
        color: 'rgba(0,0,0,0.025)',
      },
    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'value',
    axisLabel: {
      color: '#333',
      formatter: '{value} ms',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [
    {
      type: 'category',
      data: ['Line', 'Bar', 'Scatter', 'Pies'],
      axisLabel: {
        color: '#333',
      },
      axisLine: {
        lineStyle: {
          color: '#999',
        },
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: '#eee',
        },
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)'],
        },
      },
    },
    {
      type: 'category',
      axisLine: { show: false },
      axisTick: { show: false },
      axisLabel: { show: false },
      splitArea: { show: false },
      splitLine: { show: false },
      data: ['Line', 'Bar', 'Scatter', 'Pies'],
    },
  ],

  // Add series
  series: [
    {
      name: 'Version 2.0 - 2k data',
      type: 'bar',
      z: 2,
      yAxisIndex: 1,
      itemStyle: {
        normal: {
          color: '#F44336',
          label: {
            show: true,
            padding: 5,
            position: 'right',
            textStyle: {
              color: '#fff',
              fontSize: 11,
            },
          },
        },
      },
      data: [247, 187, 95, 175],
    },
    {
      name: 'Version 2.0 - 2w data',
      type: 'bar',
      z: 2,
      yAxisIndex: 1,
      itemStyle: {
        normal: {
          color: '#4CAF50',
          label: {
            show: true,
            padding: 5,
            position: 'right',
            textStyle: {
              color: '#fff',
              fontSize: 11,
            },
          },
        },
      },
      data: [488, 415, 405, 340],
    },
    {
      name: 'Version 2.0 - 20w data',
      type: 'bar',
      z: 2,
      yAxisIndex: 1,
      itemStyle: {
        normal: {
          color: '#2196F3',
          label: {
            show: true,
            padding: 5,
            position: 'right',
            textStyle: {
              color: '#fff',
              fontSize: 11,
            },
          },
        },
      },
      data: [906, 911, 908, 778],
    },
    {
      name: 'Version 1.7 - 2k data',
      type: 'bar',
      z: 1,
      itemStyle: {
        normal: {
          color: '#E57373',
          label: {
            show: true,
            padding: 5,
            position: 'right',
            textStyle: {
              fontSize: 11,
            },
          },
        },
      },
      data: [680, 819, 564, 724],
    },
    {
      name: 'Version 1.7 - 2w data',
      type: 'bar',
      z: 1,
      itemStyle: {
        normal: {
          color: '#81C784',
          label: {
            show: true,
            padding: 5,
            position: 'right',
            textStyle: {
              fontSize: 11,
            },
          },
        },
      },
      data: [1212, 2035, 1620, 955],
    },
    {
      name: 'Version 1.7 - 20w data',
      type: 'bar',
      z: 1,
      itemStyle: {
        normal: {
          color: '#64B5F6',
          label: {
            show: true,
            padding: 5,
            position: 'right',
            textStyle: {
              fontSize: 11,
            },
          },
        },
      },
      data: [2200, 3000, 2500, 3000],
    },
  ],
};
export default option;
