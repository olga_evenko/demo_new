const option = {

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Chart animation duration
  animationDuration: 750,

  // Setup grid
  grid: {
    left: 0,
    right: 30,
    top: 30,
    bottom: 0,
    containLabel: true,
  },

  // Add legend
  legend: {
    data: ['Customers', 'Returned'],
    itemHeight: 8,
    itemGap: 20,
    textStyle: {
      padding: [0, 5],
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    axisPointer: {
      type: 'shadow',
      shadowStyle: {
        color: 'rgba(0,0,0,0.025)',
      },
    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'value',
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [{
    type: 'category',
    data: ['Oct', 'Sep', 'Aug', 'July', 'June', 'May', 'Apr', 'Mar', 'Feb', 'Jan'],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: ['#eee'],
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)'],
      },
    },
  }],

  // Add series
  series: [
    {
      name: 'Customers',
      type: 'bar',
      barCategoryGap: '40%',
      label: {
        normal: {
          textStyle: {
            color: '#682d19',
          },
          position: 'left',
          show: false,
          formatter: '{b}',
        },
      },
      itemStyle: {
        normal: {
          color: '#6bca6f',
        },
      },
      data: [1900, 1029, 1602, 2004, 1100, 1800, 2800, 1407, 2200, 900],
    },
    {
      name: 'Returned',
      type: 'line',
      symbolSize: 7,
      silent: true,
      data: [100, 1000, 800, 1070, 900, 300, 1200, 900, 1200, 200],
      itemStyle: {
        normal: {
          color: '#2f4553',
          borderWidth: 2,
        },
      },
    },
  ],
};
export default option;
