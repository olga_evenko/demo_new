const option = {

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Chart animation duration
  animationDuration: 750,

  // Setup grid
  grid: {
    left: 0,
    right: 30,
    top: 35,
    bottom: 0,
    containLabel: true,
  },

  // Add legend
  legend: {
    data: ['Internet Explorer', 'Opera', 'Safari', 'Firefox', 'Chrome'],
    itemHeight: 8,
    itemGap: 20,
    textStyle: {
      padding: [0, 5],
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    axisPointer: {
      type: 'shadow',
      shadowStyle: {
        color: 'rgba(0,0,0,0.025)',
      },
    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'value',
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [{
    type: 'category',
    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: ['#eee'],
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)'],
      },
    },
  }],

  // Add series
  series: [
    {
      name: 'Internet Explorer',
      type: 'bar',
      stack: 'Total',
      barWidth: 36,
      itemStyle: {
        normal: {
          color: '#42A5F5',
          label: {
            show: true,
            position: 'insideRight',
            padding: [0, 10],
            fontSize: 12,
          },
        },
      },
      data: [320, 302, 301, 334, 390, 330, 320],
    },
    {
      name: 'Opera',
      type: 'bar',
      stack: 'Total',
      itemStyle: {
        normal: {
          color: '#ef5350',
          label: {
            show: true,
            position: 'insideRight',
            padding: [0, 10],
            fontSize: 12,
          },
        },
      },
      data: [120, 132, 101, 134, 120, 230, 210],
    },
    {
      name: 'Safari',
      type: 'bar',
      stack: 'Total',
      itemStyle: {
        normal: {
          color: '#66bb6a',
          label: {
            show: true,
            position: 'insideRight',
            padding: [0, 10],
            fontSize: 12,
          },
        },
      },
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: 'Firefox',
      type: 'bar',
      stack: 'Total',
      itemStyle: {
        normal: {
          color: '#ff7043',
          label: {
            show: true,
            position: 'insideRight',
            padding: [0, 10],
            fontSize: 12,
          },
        },
      },
      data: [150, 212, 201, 154, 190, 330, 410],
    },
    {
      name: 'Chrome',
      type: 'bar',
      stack: 'Total',
      itemStyle: {
        normal: {
          color: '#26a69a',
          label: {
            show: true,
            position: 'insideRight',
            padding: [0, 10],
            fontSize: 12,
          },
        },
      },
      data: [820, 832, 901, 934, 1290, 1330, 1320],
    },
  ],
};
export default option;
