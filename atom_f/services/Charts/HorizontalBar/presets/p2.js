import echarts from 'echarts';

const option = {

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Chart animation duration
  animationDuration: 750,

  // Setup grid
  grid: {
    left: 0,
    right: 30,
    top: 35,
    bottom: 0,
    containLabel: true,
  },

  // Add legend
  legend: {
    data: ['Year 2013', 'Year 2014'],
    itemHeight: 8,
    itemGap: 20,
    textStyle: {
      padding: [0, 5],
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    axisPointer: {
      type: 'shadow',
      shadowStyle: {
        color: 'rgba(0,0,0,0.025)',
      },
    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'value',
    boundaryGap: [0, 0.01],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [{
    type: 'category',
    data: ['Germany', 'France', 'Spain', 'Netherlands', 'Belgium'],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: ['#eee'],
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)'],
      },
    },
  }],

  // Add series
  series: [
    {
      name: 'Year 2013',
      type: 'bar',
      itemStyle: {
        normal: {
          color: '#EF5350',
        },
      },
      data: [38203, 73489, 129034, 204970, 331744],
    },
    {
      name: 'Year 2014',
      type: 'bar',
      itemStyle: {
        normal: {
          color: '#66BB6A',
        },
      },
      data: [39325, 83438, 131000, 221594, 334141],
    },
  ],
};
export default option;
