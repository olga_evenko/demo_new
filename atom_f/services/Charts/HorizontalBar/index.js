import presets from './presets';

export default {
  name: 'HorizontalBar',
  icon: 'bar.svg',
  description: 'bar chart',
  title: 'horizontalBar',
  presets,
};
