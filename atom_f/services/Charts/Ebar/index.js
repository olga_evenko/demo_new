import presets from './presets';

export default {
  name: 'Ebar',
  icon: 'bar.svg',
  description: 'bar chart',
  title: 'bar',
  presets,
};
