const option = {

  // Define colors
  color: ['#f17a52', '#03A9F4'],

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Chart animation duration
  animationDuration: 750,

  // Setup grid
  grid: {
    left: 10,
    right: 10,
    top: 35,
    bottom: 0,
    containLabel: true,
  },

  // Tooltip
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    axisPointer: {
      type: 'shadow',
      shadowStyle: {
        color: 'rgba(0,0,0,0.025)',
      },
    },
    formatter (params) {
                        var tar = params[0];
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    },
  },

  // Horizontal axis
  xAxis: [{
    type: 'category',
    data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  }],

  // Vertical axis
  yAxis: [{
    type: 'value',
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      lineStyle: {
        color: '#eee',
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)'],
      },
    },
  }],

  // Add series
  series: [
    {
      name: 'Aid',
      type: 'bar',
      stack: 'Total',
      itemStyle: {
        normal: {
          barBorderColor: 'rgba(0,0,0,0)',
          color: 'rgba(0,0,0,0)',
        },
        emphasis: {
          barBorderColor: 'rgba(0,0,0,0)',
          color: 'rgba(0,0,0,0)',
        },
      },
      data: [0, 3500, 3000, 2300, 1700, 900, 400, 0],
    },
    {
      name: 'Cost of living',
      type: 'bar',
      stack: 'Total',
      itemStyle: {
        normal: {
          barBorderRadius: 3,
          color: '#42A5F5',
          label: {
            show: true,
            position: 'inside',
          },
        },
        emphasis: {
          color: '#42A5F5',
        },
      },
      data: [4500, 1000, 500, 700, 600, 800, 500, 400],
    },
  ],
};
export default option;
