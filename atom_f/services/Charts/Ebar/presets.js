import p6 from './presets/p6';
import p7 from './presets/p7';
import p8 from './presets/p8';
import p9 from './presets/p9';
import p10 from './presets/p10';

export default [p6, p7, p8, p9, p10];
