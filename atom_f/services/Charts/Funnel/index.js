import presets from './presets';

export default {
  name: 'Funnel',
  icon: 'funnel.svg',
  description: 'bar chart',
  title: 'funnel',
  presets,
};
