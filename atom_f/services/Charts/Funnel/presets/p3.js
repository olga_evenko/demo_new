const option = {
  // Colors
  color: [
    '#2ec7c9',
    '#b6a2de',
    '#5ab1ef',
    '#ffb980',
    '#d87a80',
    '#8d98b3',
    '#e5cf0d',
    '#97b552',
    '#95706d',
    '#dc69aa',
    '#07a2a4',
    '#9a7fd1',
    '#588dd5',
    '#f5994e',
    '#c05050',
    '#59678c',
    '#c9ab00',
    '#7eb00a',
    '#6f5553',
    '#c14089',
  ],

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },

  // Add title
  title: {
    text: 'Browser popularity',
    subtext: 'Open source information',
    left: 'center',
    textStyle: {
      fontSize: 17,
      fontWeight: 500,
    },
    subtextStyle: {
      fontSize: 12,
    },
  },

  // Add tooltip
  tooltip: {
    trigger: 'item',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
    formatter: '{a} <br/>{b}: {c}%',
  },

  // Add legend
  legend: {
    orient: 'vertical',
    top: 'center',
    left: 0,
    data: ['IE', 'Opera', 'Safari', 'Firefox', 'Chrome'],
    itemHeight: 8,
    itemWidth: 8,
  },

  // Add series
  series: [
    {
      name: 'Statistics',
      type: 'funnel',
      left: '25%',
      right: '25%',
      top: '16%',
      height: '84%',
      sort: 'ascending',
      itemStyle: {
        normal: {
          borderColor: '#fff',
          borderWidth: 1,
          label: {
            position: 'right',
          },
        },
      },
      data: [
        { value: 60, name: 'Safari' },
        { value: 40, name: 'Firefox' },
        { value: 20, name: 'Chrome' },
        { value: 80, name: 'Opera' },
        { value: 100, name: 'IE' },
      ],
    },
  ],
};

export default option;
