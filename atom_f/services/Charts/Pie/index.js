import presets from './presets';

export default {
  name: 'Pie',
  icon: 'pie.svg',
  description: 'pie chart',
  title: 'pie',
  presets,
};
