import p1 from './presets/p1';
import p2 from './presets/p2';
import p3 from './presets/p3';
import p4 from './presets/p4';
import p5 from './presets/p5';
import p6 from './presets/p6';
import p7 from './presets/p7';

export default [p1, p2, p3, p4, p5, p6, p7];
