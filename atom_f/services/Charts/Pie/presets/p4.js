const option = {

  // Colors
  color: [
    '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
  ],

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13
  },

  // Add tooltip
  tooltip: {
    trigger: 'item',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif'
    },
    formatter: '{a} <br/>{b}: {c} ({d}%)'
  },

  // Add legend
  legend: {
    orient: 'vertical',
    top: 'center',
    left: 0,
    data: ['Italy', 'Spain', 'Austria', 'Germany', 'Poland', 'Denmark', 'Hungary', 'Portugal', 'France', 'Netherlands'],
    itemHeight: 8,
    itemWidth: 8
  },

  // Add series
  series: [

    // Inner
    {
      name: 'Countries',
      type: 'pie',
      selectedMode: 'single',
      radius: [0, '50%'],
      itemStyle: {
        normal: {
          borderWidth: 1,
          borderColor: '#fff',
          label: {
            position: 'inner'
          },
          labelLine: {
            show: false
          }
        }
      },
      data: [
        { value: 535, name: 'Italy' },
        { value: 679, name: 'Spain' },
        { value: 1548, name: 'Austria' }
      ]
    },

    // Outer
    {
      name: 'Countries',
      type: 'pie',
      radius: ['60%', '85%'],
      itemStyle: {
        normal: {
          borderWidth: 1,
          borderColor: '#fff'
        }
      },
      data: [
        { value: 535, name: 'Italy' },
        { value: 310, name: 'Germany' },
        { value: 234, name: 'Poland' },
        { value: 135, name: 'Denmark' },
        { value: 948, name: 'Hungary' },
        { value: 251, name: 'Portugal' },
        { value: 147, name: 'France' },
        { value: 202, name: 'Netherlands' }
      ]
    }
  ]
};
export default option;
