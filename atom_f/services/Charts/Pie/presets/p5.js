const dataStyle = {
  normal: {
    borderWidth: 1,
    borderColor: '#fff',
    label: { show: false },
    labelLine: { show: false }
  }
};
const placeHolderStyle = {
  normal: {
    color: 'transparent',
    borderWidth: 0
  }
};

const option = {

  // Colors
  color: [
    '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
  ],

  // Global text styles
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13
  },

  // Add title
  title: {
    text: 'Are you happy?',
    subtext: 'Utrecht, Netherlands',
    left: 'center',
    top: '45%',
    textStyle: {
      color: 'rgba(30,144,255,0.8)',
      fontSize: 19,
      fontWeight: 500
    },
    subtextStyle: {
      fontSize: 12
    }
  },

  // Add tooltip
  tooltip: {
    trigger: 'item',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif'
    },
    formatter: function (params) {
      if (params.color == "transparent") return;
      return params.percent + '%' + ' - ' + params.seriesName;
    }
  },

  // Add legend
  legend: {
    orient: 'vertical',
    top: '5%',
    left:  '52%',
    data: ['60% Definitely yes', '30% Could be better', '10% Not at the moment'],
    itemHeight: 8,
    itemWidth: 8,
    itemGap: 15
  },

  // Add series
  series: [
    {
      name: 'Definitely yes',
      type: 'pie',
      cursor: 'default',
      clockWise: false,
      radius: ['75%', '90%'],
      hoverOffset: 0,
      itemStyle: dataStyle,
      data: [
        {
          value: 60,
          name: '60% Definitely yes'
        },
        {
          value: 40,
          name: '',
          itemStyle: placeHolderStyle
        }
      ]
    },

    {
      name: 'Could be better',
      type: 'pie',
      cursor: 'default',
      clockWise: false,
      radius: ['60%', '75%'],
      hoverOffset: 0,
      itemStyle: dataStyle,
      data: [
        {
          value: 30,
          name: '30% Could be better'
        },
        {
          value: 70,
          name: 'invisible',
          silent: false,
          itemStyle: placeHolderStyle
        }
      ]
    },

    {
      name: 'Not at the moment',
      type: 'pie',
      cursor: 'default',
      clockWise: false,
      radius: ['45%', '60%'],
      hoverOffset: 0,
      itemStyle: dataStyle,
      data: [
        {
          value: 10,
          name: '10% Not at the moment'
        },
        {
          value: 90,
          name: 'invisible',
          itemStyle: placeHolderStyle
        }
      ]
    }
  ]
};
export default option;
