import echarts from 'echarts';

const o1 = {
  title: {
    text: 'Заголовок',
    subtext: 'From d3.js',
    x: 'right',
    y: 'bottom',
  },
  tooltip: {
    trigger: 'item',
    formatter(params) {
      if (params.indicator2) {
        // is edge
        return params.value.weight;
      } // is node
      return params.name;
    },
  },
  toolbox: {
    show: true,
    feature: {
      restore: { show: true },
      magicType: { show: true, type: ['force', 'chord'] },
      saveAsImage: { show: true },
    },
  },
  legend: {
    x: 'left',
    data: ['group1', 'group2', 'group3', 'group4'],
  },
  series: [
    {
      type: 'chord',
      sort: 'ascending',
      sortSub: 'descending',
      showScale: true,
      showScaleText: true,
      data: [
        { name: 'group1' },
        { name: 'group2' },
        { name: 'group3' },
        { name: 'group4' },
      ],
      itemStyle: {
        normal: {
          label: {
            show: false,
          },
        },
      },
      matrix: [
        [11975, 5871, 8916, 2868],
        [1951, 10048, 2060, 6171],
        [8010, 16145, 8090, 8045],
        [1013, 990, 940, 6907],
      ],
    },
  ],
};

export default o1;
