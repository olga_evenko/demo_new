import presets from './presets';

export default {
  name: 'Sunburst',
  icon: 'sunburst.svg',
  description: 'sunburst chart',
  title: 'sunburst',
  presets,
};
