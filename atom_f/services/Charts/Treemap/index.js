import presets from './presets';

export default {
  name: 'Treemap',
  icon: 'treemap.svg',
  description: 'treemap chart',
  title: 'treemap',
  presets,
};
