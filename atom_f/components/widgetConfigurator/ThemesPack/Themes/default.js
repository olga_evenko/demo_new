// eslint-disable-next-line
export const theme = {
  color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],
  textStyle: {
    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
    fontSize: 13,
  },
  animationDuration: 750,
  grid: {
    left: 0,
    right: 40,
    top: 35,
    bottom: 0,
    containLabel: true,
  },
  legend: {
    itemHeight: 8,
    itemGap: 20,
  },
  tooltip: {
    trigger: 'axis',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: [10, 15],
    textStyle: {
      fontSize: 13,
      fontFamily: 'Roboto, sans-serif',
    },
  },
  categoryAxis: {
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
  },
  valueAxis: {
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)'],
      },
    },
  },
  logAxis: {
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)'],
      },
    },
  },
  timeAxis: {
    axisLabel: {
      color: '#333',
    },
    axisLine: {
      lineStyle: {
        color: '#999',
      },
    },
    splitLine: {
      show: true,
      lineStyle: {
        color: '#eee',
        type: 'dashed',
      },
    },
    splitArea: {
      show: true,
      areaStyle: {
        color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)'],
      },
    },
  },
  radar: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  bar: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  pie: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  scatter: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  boxplot: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  parallel: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  sankey: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  funnel: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  gauge: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  candlestick: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  graph: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  map: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },
  geo: {
    itemStyle: {
      normal: {
        borderWidth: 2,
      },
    },
    areaStyle: {
      normal: {
        opacity: 0.25,
      },
    },
  },

};
