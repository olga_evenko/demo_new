export default {
  navigationPosition: 0,
  maxCompletedTab: 0,
  activationKey: null,
  user: {
    username: null,
    email: null,
    firstName: null,
    lastName: null,
    middleName: null,
    position: null,
    password: null,
    password2: null,
    phoneNumber: null,
  },
  organization: {
    name: null,
    description: null,
  },
  initialLoading: false,
  initialError: false,
};
