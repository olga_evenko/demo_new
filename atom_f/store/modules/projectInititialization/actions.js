import * as actionTypes from './actionTypes';
import API from '@/services/api';
import globalActionTypes from '@/store/actionTypes';

export default {
  [actionTypes.INIT_PROJECT](context) {
    this.commit(actionTypes.SET_LOADING, false);
    this.commit(actionTypes.SET_ERORR, null);
    API.initProject({
      ...context.state.user,
      organization: context.state.organization,
    })
      .then(() => {
        this.commit(actionTypes.SET_LOADING, false);
        this.dispatch(globalActionTypes.auth.TRY_REMOTE_AUTH, {
          username: context.state.user.username,
          password: context.state.user.password,
        });
      })
      .catch((e) => {
        console.log(e);
        this.commit(actionTypes.SET_ERORR, `Произошла ошибка ${e.statusCode}.`);
      });
  },
};
