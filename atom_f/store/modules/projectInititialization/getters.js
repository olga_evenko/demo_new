export default {
  getInitialActivationKey: state => state.activationKey,
  getInitialUser: state => state.user,
  getInitialOrgranization: state => state.organization,
  getInitialNavigationPosition: state => state.navigationPosition,
  getCountInitialCompletedTab: state => state.maxCompletedTab,
  getInitialLoading: state => state.initialLoading,
  getInitialError: state => state.initialError,
};
