import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_INITIAL_USER](state, userInfo) {
    state.user = userInfo;
  },
  [actionTypes.SET_ACTIVATION_KEY](state, key) {
    state.activationKey = key;
  },
  [actionTypes.SET_INITIAL_OPGANISATION](state, orgranizationInfo) {
    state.organization = orgranizationInfo;
  },
  [actionTypes.TO_PREVIOUS_TAB](state) {
    state.navigationPosition -= 1;
  },
  [actionTypes.TO_NEXT_TAB](state) {
    state.navigationPosition += 1;
    if (state.navigationPosition > state.maxCompletedTab) {
      state.maxCompletedTab = state.navigationPosition;
    }
  },
  [actionTypes.SET_LOADING](state, status) {
    state.initialLoading = status;
  },
  [actionTypes.SET_ERORR](state, error) {
    state.initialError = error;
  },
};
