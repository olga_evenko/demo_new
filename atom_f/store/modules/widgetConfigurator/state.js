export default {
  // Mode: editor/visual
  widgetData: {},
  widgetVersions: [],
  widgetConfMap: {},
  configuratorError: null,
  configuratorLoading: false,
  configuratorSuccess: false,
  widgetPresets: [],
  editableData: null, // if object - edit mode
  editablePreset: null,
};
