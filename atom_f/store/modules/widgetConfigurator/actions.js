import * as actionTypes from './actionTypes';
import api, { converters } from '@/services/api';

export default {
  async [actionTypes.SAVE_ORGANISATION_PRESET](_, presetMetaData) {
    this.dispatch(actionTypes.RESET_CONFIGURATOR);
    const data = { ...presetMetaData };
    data.config = JSON.stringify(this.getters.getWidgetConf);
    try {
      const rspns = await api.createOrganisationPreset(data);
      const preset = converters.forClient.convertPreset(rspns.data);
      this.commit(actionTypes.SET_CONFIGURATOR_SUCCESS, true);
      this.commit(actionTypes.SET_ORGANISATION_PRESETS, [
        ...this.getters.getWidgetPresets,
        preset,
      ]);
    } catch (e) {
      console.log('TCL: saveOrganisationPreset -> e', e);
      this.commit(actionTypes.SET_ERROR, 'unexpectedError');
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
  async [actionTypes.LOAD_ORGANISATION_PRESETS]() {
    this.dispatch(actionTypes.RESET_CONFIGURATOR);
    try {
      const presets = await api.getOrganisationPresets();
      this.commit(actionTypes.SET_ORGANISATION_PRESETS, presets);
    } catch (e) {
      console.log('TCL: e', e);
      this.commit(actionTypes.SET_ERROR, 'unexpectedError');
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
  async [actionTypes.CHANGE_PRESET_RATING](_, presetData) {
    const { id, rating } = presetData;
    try {
      const rspns = await api.changePresetRating(id, rating);
      this.commit(actionTypes.CHANGE_PRESET_RATING, {
        id: rspns.data.preset,
        userRating: rspns.data.rating,
        rating: rspns.data.presetRating,
      });
    } catch (e) {
      console.log('TCL: changePresetRating -> e', e);
      // TODO: вывод ошибки в сервис уведомлений
    }
  },
  async [actionTypes.EDIT_PRESET]({ state, commit }, updatedPreset) {
    try {
      const rspns = await api.updateWidgetPreset(
        updatedPreset.id,
        updatedPreset,
      );
      const presets = [...state.widgetPresets];
      const updatedPresetIndex = presets.findIndex(
        p => p.id === updatedPreset.id,
      );
      presets[updatedPresetIndex] = updatedPreset;
      commit(actionTypes.SET_ORGANISATION_PRESETS, presets);
      this.commit(actionTypes.SET_CONFIGURATOR_SUCCESS, true);
    } catch (e) {
      console.log(e);
    }
  },
  [actionTypes.RESET_CONFIGURATOR]() {
    this.commit(actionTypes.SET_ERROR, null);
    this.commit(actionTypes.SET_LOADING, false);
    this.commit(actionTypes.SET_CONFIGURATOR_SUCCESS, false);
    this.commit(actionTypes.SET_EDITABLE_DATA, null);
    this.commit(actionTypes.SET_EDITABLE_PRESET, null);
  },
};
