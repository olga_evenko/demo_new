export default {
  getWidgetConfVersion: state => (state.widgetVersions.length
    ? state.widgetVersions[state.widgetVersions.length] - 1
    : 0),
  getWidgetConf: state => state.widgetData,
  getConfiguratorError: state => state.configuratorError,
  getConfiguratorLoading: state => state.configuratorLoading,
  getConfiguratorSuccess: state => state.configuratorSuccess,
  getWidgetPresets: state => state.widgetPresets,
  getSearchQuery: state => state.searchQuery,
  getEditableData: state => state.editableData,
  getChartsPresets: state => state.widgetPresets,
  getEditablePreset: state => state.editablePreset,
  // getChartsPresets: state => state.widgetPresets.filter(
  //   w => [
  //     'Eline',
  //     'Gauge',
  //     'Graph',
  //     'Pie',
  //     'Polar',
  //     'Radar',
  //     'Scatter',

  //   ].indexOf(w.type) >= 0,
  // ),
};
