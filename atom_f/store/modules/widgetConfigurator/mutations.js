import * as actionTypes from './actionTypes';
import deepUpdate from '@/tools/deepUpdate';

export default {
  [actionTypes.SET_WIDGET_CONF](state, widgetData) {
    state.widgetData = widgetData;
  },
  [actionTypes.UPDATE_WIDGET_CONF](state, code) {
    // const updatedWidgetData = deepUpdate({ ...state.widgetData }, code);
    state.widgetData = code;
  },
  [actionTypes.SET_ERROR](state, error) {
    state.configuratorError = error;
  },
  [actionTypes.SET_LOADING](state, loading) {
    state.configuratorLoading = loading;
  },
  [actionTypes.SET_CONFIGURATOR_SUCCESS](state, success) {
    state.configuratorSuccess = success;
  },
  [actionTypes.SET_ORGANISATION_PRESETS](state, presets) {
    state.widgetPresets = presets;
  },
  [actionTypes.CHANGE_PRESET_RATING](state, presetData) {
    const presetIndex = state.widgetPresets.findIndex(
      p => p.id === presetData.id,
    );
    const presets = [...state.widgetPresets];
    presets[presetIndex].userRating = presetData.userRating;
    presets[presetIndex].rating = presetData.rating
      ? presetData.rating
      : presets[presetIndex].rating;
    state.widgetPresets = presets;
  },
  [actionTypes.SET_EDITABLE_DATA](state, data) {
    state.editableData = data;
  },
  [actionTypes.SET_EDITABLE_PRESET](state, preset) {
    state.editablePreset = preset;
  },
};
