export default {
  getIncidentsLoading: state => state.incidentsLoading,
  getIncidentsError: state => state.incidentsError,
  getIncidents: state => state.incidents,
  getIncidentsPagination: state => state.incidentsPagination,
  getIncidentsFilters: state => state.incidentsFilters,
  getIncidentsScatterData: state => state.incidentsScatterData,
  getActivities: state => state.activities,
  getIncidentsScatterLoaded: state => state.incidentsScatterLoaded,
  getSelectedIncident: state => state.selectedIncident,
  getSelectedIncidentEventsPagination: state => state.selectedIncidentEventsPagination,
};
