import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_INCIDENTS](state, incidents) {
    state.incidents = incidents;
  },
  [actionTypes.SET_ERROR](state, error) {
    state.incidentsError = error;
  },
  [actionTypes.SET_LOADING](state, loading) {
    state.incidentsLoading = loading;
  },
  [actionTypes.SET_PAGINATION](state, pagination) {
    state.incidentsPagination = pagination;
  },
  [actionTypes.SET_FILTERS](state, filters) {
    state.incidentsFilters = filters;
  },
  [actionTypes.SET_SCATTER_DATA](state, data) {
    state.incidentsScatterData = data;
  },
  [actionTypes.SET_ACTIVITIES](state, activities) {
    state.activities = activities;
  },
  [actionTypes.SET_INCIDENTS_SCATTER_LOADING](state, loaded) {
    state.incidentsScatterLoaded = loaded;
  },
  [actionTypes.SELECT_INCIDENT](state, incident) {
    state.selectedIncident = incident;
  },
  [actionTypes.SET_SELECTED_INCIDENT_EVENTS_PAGINATION](state, pagination) {
    state.selectedIncidentEventsPagination = pagination;
  },
};
