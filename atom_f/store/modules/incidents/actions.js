import { get } from 'https';
import * as actionTypes from './actionTypes';
import api from '@/services/api';
import socket from '@/services/api/sockets';

export default {
  async [actionTypes.LOAD_INCIDENTS]({ state, commit }) {
    try {
      const rspns = await api.loadIncidents(
        state.incidentsPagination,
        state.incidentsFilters,
      );
      const updatedPagination = {
        ...state.incidentsPagination,
        count: rspns.data.count,
        next: rspns.data.next,
        previous: rspns.data.previous,
      };
      commit(actionTypes.SET_PAGINATION, updatedPagination);
      commit(actionTypes.SET_INCIDENTS, rspns.data.results);
    } catch (e) {
      console.log(e);
    }
  },
  [actionTypes.LOAD_SCATTER_DATA]({ state, commit }) {
    const loadedData = [];
    socket.incidents.loadScatterData().map(
      (data) => {
        loadedData.push(...data.chart);
      },
      () => {
        commit(actionTypes.SET_SCATTER_DATA, loadedData);
        commit(actionTypes.SET_INCIDENTS_SCATTER_LOADING, true);
      },
    );
  },
  async [actionTypes.LOAD_ACTIVITIES]({ state, commit }) {
    try {
      const rspns = await api.loadIncidents(
        state.incidentsPagination,
        state.incidentsFilters,
      );
      const updatedPagination = {
        ...state.incidentsPagination,
        count: rspns.data.count,
        next: rspns.data.next,
        previous: rspns.data.previous,
      };
      commit(actionTypes.SET_PAGINATION, updatedPagination);
      commit(actionTypes.SET_ACTIVITIES, rspns.data.results);
    } catch (e) {
      console.log(e);
    }
  },
  async [actionTypes.LOAD_SELECTED_INCIDENTS_META]({ state, commit }) {
    try {
      const [eventRspns, assetRspns] = await Promise.all([
        api.loadEventsByIncidentId(
          state.selectedIncident.id,
          state.selectedIncidentEventsPagination,
        ),
        api.getAssetById(state.selectedIncident.assetId),
      ]);
      commit(actionTypes.SELECT_INCIDENT, {
        ...state.selectedIncident,
        events: eventRspns.data.results,
        assetMeta: assetRspns.data,
      });
      commit(actionTypes.SET_SELECTED_INCIDENT_EVENTS_PAGINATION, {
        ...state.selectedIncidentEventsPagination,
        next: eventRspns.data.next,
        previous: eventRspns.data.previous,
        total: eventRspns.data.count,
      });
    } catch (e) {
      console.log(e);
    }
  },
};
