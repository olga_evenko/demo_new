import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_USERS](state, users) {
    state.users = users;
  },
  [actionTypes.SET_ERROR](state, error) {
    state.usersError = error;
  },
  [actionTypes.SET_LOADING](state, status) {
    state.usersLoading = status;
  },
  [actionTypes.SELECT_USER](state, user) {
    state.selectedUsers.push(user);
  },
  [actionTypes.DESELECT_USER](state, user) {
    const index = state.selectedUsers.indexOf(user);
    if (index !== -1) state.selectedUsers.splice(index, 1);
  },
  [actionTypes.SET_PAGINATION](state, pagination) {
    state.pagination = pagination;
  },
  [actionTypes.UPDATE_USER](state, user) {
    if (!state.users) {
      return;
    }
    const users = [...state.users];
    const oldUserIndex = users.findIndex(u => u.id === user.id);
    if (oldUserIndex < 0) {
      return;
    }
    // users[oldUserIndex] = new UserModel(user);
    state.users = users;
  },
  [actionTypes.SET_AVAILABLE_PERMISSIONS](state, permissions) {
    state.availablePermissions = permissions;
  },
};
