export default {
  getUsers: state => state.users,
  getUsersError: state => state.usersError,
  getUsersLoading: state => state.usersLoading,
  getSelectedUsers: state => state.selectedUsers,
  getUsersPagination: state => state.pagination,
  getAvailablePermissions: state => state.availablePermissions,
  getUserById: state => (userId) => {
    if (!state.users) {
      return;
    }
    // eslint-disable-next-line
    for (const u of state.users) {
      if (u.id === userId) {
        return u;
      }
    }
  },
};
