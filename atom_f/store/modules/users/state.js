export default {
  users: null,
  usersError: null,
  usersLoading: false,
  selectedUsers: [],
  pagination: {
    next: null,
    previous: null,
    usersOffset: 5,
    usersLimit: 5,
  },
  availablePermissions: [],
};
