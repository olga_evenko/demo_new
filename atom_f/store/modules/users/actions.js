import API from '@/services/api';
import UserModel from '@/models/user';
import * as actionTypes from './actionTypes';

export default {
  async [actionTypes.LOAD_USERS](context, data) {
    this.commit(actionTypes.SET_LOADING, true);
    const limit = data ? data.limit : 10;
    const offset = data ? data.offset : 0;
    try {
      const rspns = await API.getUsers(offset, limit);
      const users = [];
      const pagination = {
        next: rspns.data.next,
        previous: rspns.data.previous,
        offset,
        limit,
      };
      this.commit(actionTypes.SET_PAGINATION, pagination);
      rspns.data.results.forEach((user) => {
        const userModel = new UserModel(user);
        users.push(userModel);
        this.commit(actionTypes.SET_USERS, users);
      });
    } catch (e) {
      this.commit(
        actionTypes.SET_ERROR,
        'Произошла ошибка при запросе на удаленный сервер.',
      );
    } finally {
      this.commit(actionTypes.SET_LOADING, false);
    }
  },
  async [actionTypes.UPDATE_REMOTE_USER](_, user) {
    this.commit(actionTypes.SET_ERROR, null);
    this.commit(actionTypes.SET_LOADING, true);
    try {
      const rspns = await API.updateUser(user);
      this.commit(actionTypes.UPDATE_USER, rspns.data);
    } catch (e) {
      console.log('TCL: }catch -> e', e);
      this.commit(
        actionTypes.SET_ERROR,
        'Произошла ошибка при запросе на удаленный сервер',
      );
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
  async [actionTypes.DELETE_USERS](_, userData) {
    this.commit(actionTypes.SET_ERROR, null);
    this.commit(actionTypes.SET_LOADING, true);
    try {
      if (userData.length) {
        const rspns = await API.deleteUserProfile(userData);
      }
    } catch (e) {
      console.log('TCL: e', e);
      this.commit(
        actionTypes.SET_ERROR,
        'Ошибка на удаленном сервере.',
      );
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
  async [actionTypes.LOAD_AVAILABLE_PERMISSIONS]() {
    try {
      const rspns = await API.getAvailablePermissions();
      this.commit(actionTypes.SET_AVAILABLE_PERMISSIONS, rspns.data);
    } catch (e) { }
  },
  async [actionTypes.UPDATE_USER_PERMISSIONS](_, data) {
    this.commit(actionTypes.SET_LOADING, true);
    this.commit(actionTypes.SET_ERROR, null);
    try {
      await API.updateUserPermissions(data);
    } catch (e) {
      this.commit(
        actionTypes.SET_ERROR,
        'Произошла ошибка при запросе на удаленный сервер',
      );
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
  async [actionTypes.DEACTIVATE_USER](_, userId) {
    this.commit(actionTypes.SET_ERROR, null);
    this.commit(actionTypes.SET_LOADING, true);
    try {
      await API.deactivateUser(userId);
    } catch (e) {
      this.commit(
        actionTypes.SET_ERROR,
        'Произошла ошибка при запросе на удаленный сервер',
      );
    }
    this.commit(actionTypes.SET_LOADING, true);
  },
};
