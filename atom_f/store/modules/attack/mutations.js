import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_ATTACK_MATRIX](state, matrix) {
    state.matrix = matrix;
  },
};
