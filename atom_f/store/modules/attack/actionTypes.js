export const LOAD_ATTACK_MATRIX = '[ATTACK] LOAD ATTACK MATRIX';
export const SET_ERROR = '[ATTACK] SET USER ERROR';
export const SET_LOADING = '[ATTACK] SET LOADING';
export const SET_ATTACK_MATRIX = '[ATTACK] SET ATTACK MATRIX';
