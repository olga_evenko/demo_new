import API from '@/services/api';
import * as actionTypes from './actionTypes';

export default {
  async [actionTypes.LOAD_ATTACK_MATRIX]() {
    try {
      const response = await API.loadAttackMatrix();
      this.commit(actionTypes.SET_ATTACK_MATRIX, response.data);
    } catch (e) {
      this.commit(
        actionTypes.SET_ERROR,
        'Произошла ошибка при запросе на удаленный сервер',
      );
    }
  },
};
