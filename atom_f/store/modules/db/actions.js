import api from '@/services/api';
import * as actionTypes from './actionTypes';

export default {
  async [actionTypes.LOAD_DATABASE_TABLE_NAMES]({ commit }) {
    try {
      const rspns = await api.loadDbTables();
      commit(actionTypes.SET_DB_TABLES, rspns.data);
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.LOAD_BACKUP_TASKS]({ commit }) {
    try {
      const rspns = await api.loadBackupTasks();
      commit(actionTypes.SET_BACKUP_TASKS, rspns.data);
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.LOAD_BACKUPS]({ commit, state }) {
    try {
      const params = {
        limit: state.backupsPagination.limit,
        offset: state.backupsPagination.offset,
      };
      const rspns = await api.loadBackups(params);
      commit(actionTypes.SET_BACKUPS, rspns.data.results);
      commit(actionTypes.SET_BACKUPS_PAGINATION, {
        ...state.backupsPagination,
        next: rspns.data.next,
        previous: rspns.data.previous,
        count: rspns.data.count,
      });
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.LOAD_BACKUP_TASKS]({ commit, state }) {
    try {
      const params = {
        limit: state.backupTasksPagination.limit,
        offset: state.backupTasksPagination.offset,
      };
      const rspns = await api.loadBackupTasks(params);
      commit(actionTypes.SET_BACKUP_TASKS, rspns.data.results);
      commit(actionTypes.SET_BACKUP_TASKS_PAGINATION, {
        ...state.backupTasksPagination,
        next: rspns.data.next,
        previous: rspns.data.previous,
        count: rspns.data.count,
      });
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.CREATE_BACKUP_TASK]({ commit }, data) {
    try {
      const rspns = await api.createBackupTask(data);
      commit(actionTypes.ADD_BACKUP_TASK, rspns.data);
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.DELETE_BACKUPS]({ dispatch }, data) {
    try {
      const rspns = await api.deleteBackups(data);
      dispatch(actionTypes.LOAD_BACKUPS);
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.DELETE_BACKUP_TASKS]({ dispatch }, data) {
    try {
      const rspns = await api.deleteBackupTasks(data);
      dispatch(actionTypes.LOAD_BACKUP_TASKS);
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
};
