export default {
  getDbTables: state => state.dbTables,
  getBackupTasks: state => state.backupTasks,
  getBackups: state => state.backups,
  getDbError: state => state.dbError,
  getDbLoading: state => state.dbLoading,
  getbBackupsPagination: state => state.backupsPagination,
  getBackupsTasksPagination: state => state.backupTasksPagination,
};
