import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_DB_TABLES](state, tables) {
    state.dbTables = tables;
  },
  [actionTypes.SET_BACKUP_TASKS](state, tasks) {
    state.backupTasks = tasks;
  },
  [actionTypes.SET_BACKUPS](state, backups) {
    state.backups = backups;
  },
  [actionTypes.ADD_BACKUP_TASK](state, backupTask) {
    state.backupTasks = [backupTask, ...state.backupTasks.slice(0, state.backupTasks.length - 1)];
  },
  [actionTypes.SET_BACKUPS_PAGINATION](state, pagination) {
    state.backupsPagination = pagination;
  },
  [actionTypes.SET_BACKUP_TASKS_PAGINATION](state, pagination) {
    state.backupTasksPagination = pagination;
  },
};
