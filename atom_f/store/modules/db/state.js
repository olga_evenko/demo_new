export default {
  dbLoading: false,
  dbError: null,
  dbTables: null,
  backupTasks: null,
  backups: null,
  backupsPagination: {
    limit: 20,
    count: 0,
    previus: null,
    next: null,
    offset: 0,
  },
  backupTasksPagination: {
    limit: 5,
    count: 0,
    previus: null,
    next: null,
    offset: 0,
  },
};
