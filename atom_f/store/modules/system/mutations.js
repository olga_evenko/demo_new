import * as types from './actionTypes';

export default {
  [types.SET_ERROR](state, error) {
    state.updatesError = error;
  },
  [types.SET_LOADING](state, loading) {
    state.updatesLoading = loading;
  },
  [types.SET_PAGINATION](state, pagination) {
    state.updatesPagination = pagination;
  },
  [types.SET_UPDATES](state, updates) {
    state.updates = updates;
  },
  [types.SET_UPDATE_INFO](state, info) {
    state.updateInfo = info;
  },
};
