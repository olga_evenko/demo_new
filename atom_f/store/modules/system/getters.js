export default {
  getUpdates: state => state.updates,
  getUpdatesPagination: state => state.updatesPagination,
  getUpdateInfo: state => state.updateInfo,
};
