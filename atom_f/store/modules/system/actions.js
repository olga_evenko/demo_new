import * as types from './actionTypes';
import globalActionTypes from '@/store/actionTypes';
import api from '@/services/api';
import sockets from '@/services/api/sockets';

export default {
  async [types.LOAD_UPDATES]({ state, commit }) {
    commit(types.SET_LOADING, true);
    try {
      const rspns = await api.loadUpdates(state.updatePagination);
      commit(types.SET_UPDATES, rspns.data.results);
      commit(types.SET_PAGINATION, {
        count: rspns.data.count,
        next: rspns.data.next,
        previus: rspns.data.previus,
      });
    } catch (e) {
      console.log('updates: ', e);
      commit(types.SET_ERROR, 'Что-то пошло не так');
    }
    commit(types.SET_LOADING, false);
  },
  async [types.APPLY_UPDATE]({ commit }) {
    commit(types.SET_LOADING, true);
    try {
      await api.applyUpdate();
    } catch (e) {
      let errMsg = 'Что-то пошло не так.';
      if (e.response && e.response.status === 429) {
        errMsg = 'Процесс обновления уже запущен.';
      }
      if (e.response && e.response.status === 404) {
        errMsg = 'Все обновления уже применены.';
      }
      this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, {
        type: 'ModalError',
        data: {
          title: 'Ошибка',
          error: errMsg,
        },
      });
    }
    commit(types.SET_LOADING, false);
  },
  async [types.SUBSCRIBE_TO_SYSTEM]({ commit, dispatch }) {
    sockets.system.subscribeToSystem().map(
      (res) => {
        if (res) {
          commit(types.SET_UPDATE_INFO, res);
        }
      },
      () => {},
      () => {
        dispatch(types.SUBSCRIBE_TO_SYSTEM);
      },
    );
  },
};
