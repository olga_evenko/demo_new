export default {
  updatesPagination: {
    limit: 25,
    offset: 0,
    next: null,
    previus: null,
  },
  updates: [],
  updatesError: null,
  updatesLoading: null,
  updateInfo: null,
};
