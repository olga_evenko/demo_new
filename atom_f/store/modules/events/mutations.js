import * as actionTypes from './actionTypes';
import { defaultFilters } from './state';

export default {
  [actionTypes.RESET_EVENTS_PAGINATION](state) {
    state.eventsPagination = {
      limit: 100,
      offset: 0,
      next: null,
      previous: null,
    };
  },
  [actionTypes.RESET_EVENTS_FILTERS](state) {
    state.eventsFilters = defaultFilters;
  },
  [actionTypes.RESET_EVENTS_SEARCH_DATA](state) {
    state.searchData = {
      progress: 0,
      task: null,
      total: 0,
      chunks: [],
    };
  },
  [actionTypes.RESET_EVENTS_CACHE](state) {
    state.eventsCache = {
      historyTasks: [],
      selectedTask: null,
    };
  },
  [actionTypes.SET_EVENTS_SEARCH_DATA](state, searchData) {
    state.searchData = searchData;
  },
  [actionTypes.SET_EVENTS_PAGINATION](state, pagination) {
    state.eventsPagination = pagination;
  },
  [actionTypes.SET_EVENTS_CACHE](state, cache) {
    state.eventsCache = cache;
  },
  [actionTypes.SET_EVENTS_LOADING](state, loadingStatus) {
    state.eventsLoading = loadingStatus;
  },
  [actionTypes.SET_EVENTS_ERROR](state, error) {
    state.eventsError = error;
  },
  [actionTypes.SELECT_EVENTS_CHUNK](state, chunk) {
    state.eventsSelectedChunk = chunk;
  },
  [actionTypes.SET_EVENTS_DATA](state, eventsData) {
    const { sortBy } = state.eventsSortConfig;
    if (eventsData && sortBy) {
      eventsData.sort((o1, o2) => {
        if (o1[sortBy] < o2[sortBy]) { return -1 * state.eventsSortConfig.forward; }
        if (o1[sortBy] > o2[sortBy]) { return 1 * state.eventsSortConfig.forward; }
        return 0;
      });
    }
    state.eventsData = eventsData;
  },
  [actionTypes.SET_EVENTS_FILTERS](state, filters) {
    state.eventsFilters = filters;
  },
  [actionTypes.SET_EVENTS_ALREADE_SEARCHED](state, searchedStatus) {
    state.eventsAlreadySearched = searchedStatus;
  },
  [actionTypes.SET_EVENTS_SORT_PARAM](state, sortBy) {
    let { forward } = state.eventsSortConfig;
    if (sortBy === state.eventsSortConfig.sortBy) {
      forward *= -1;
    }
    state.eventsSortConfig = {
      sortBy,
      forward,
    };
    this.commit(actionTypes.SET_EVENTS_DATA, state.eventsData);
  },
};
