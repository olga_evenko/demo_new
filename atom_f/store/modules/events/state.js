export const defaultFilters = {
  begin: '2017-01-01T00:00',
  end: '2018-01-01T00:00',
  search: 'ERROR COREDEV NetScreen',
  srcip: '',
  device_ip: '',
  protocol: '',
  status: '',
  srcuser: '',
  action: '',
  zoom: 60 * 60 * 24 * 7,
  raw: false,
};

export default {
  eventsPagination: {
    limit: 100,
    offset: 0,
    next: null,
    previous: null,
  },
  eventsFilters: { ...defaultFilters },
  searchData: {
    progress: 0,
    task: null,
    total: 0,
    chunks: [],
  },
  eventsCache: {
    historyTasks: [],
    selectedTask: null,
  },
  eventsError: null,
  eventsLoading: false,
  eventsSelectedChunk: null,
  eventsData: null,
  eventsAlreadySearched: false,
  eventsSortConfig: {
    sortBy: null,
    forward: 1,
  },
};
