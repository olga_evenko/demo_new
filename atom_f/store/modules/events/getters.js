export default {
  getEventsPagination: state => state.eventsPagination,
  getEventsFilters: state => state.eventsFilters,
  getEventsSearchData: state => state.searchData,
  getEventsCache: state => state.eventsCache,
  getEventsError: state => state.eventsError,
  getEventsLoading: state => state.eventsLoading,
  getEventsSelectedChunk: state => state.eventsSelectedChunk,
  getEventsData: state => state.eventsData,
  getEventsAlreadySearched: state => state.eventsAlreadySearched,
};
