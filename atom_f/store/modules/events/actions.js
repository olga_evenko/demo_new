import * as actionTypes from './actionTypes';
import API from '@/services/api';
import socket from '@/services/api/sockets';

export default {
  [actionTypes.RESET_EVENTS]() {
    this.commit(actionTypes.RESET_EVENTS_FILTERS);
    this.commit(actionTypes.RESET_EVENTS_PAGINATION);
    this.commit(actionTypes.RESET_EVENTS_SEARCH_DATA);
    this.commit(actionTypes.SET_EVENTS_ERROR, null);
    this.commit(actionTypes.SET_EVENTS_LOADING, false);
    this.commit(actionTypes.SELECT_EVENTS_CHUNK, null);
    this.commit(actionTypes.SET_EVENTS_DATA, null);
    this.commit(actionTypes.SET_EVENTS_ALREADE_SEARCHED, false);
  },
  async [actionTypes.SEARCH_EVENTS]({ state, commit }) {
    await this.dispatch(actionTypes.STOP_SEARCH);
    this.commit(actionTypes.RESET_EVENTS_SEARCH_DATA);
    this.commit(actionTypes.SET_EVENTS_ALREADE_SEARCHED, true);
    this.commit(actionTypes.SET_EVENTS_CACHE, {
      ...state.eventsCache,
      selectedTask: null,
    });
    this.commit(actionTypes.SET_EVENTS_SEARCH_DATA, state.searchData);
    socket.events.search(state.eventsFilters).map((data) => {
      if (data.data.task === state.searchData.task || state.searchData.task === null) {
        commit(actionTypes.SET_EVENTS_SEARCH_DATA, {
          ...state.searchData,
          task: data.data.task,
          total: data.data.total,
          progress: data.progress,
          elapsedTime: data.elapsed_time,
          chunks: [...state.searchData.chunks, data.data.chunk],
        });
      }
    });
  },
  async [actionTypes.STOP_SEARCH]({ state }) {
    if (state.searchData.task !== null) {
      try {
        // TODO: закрыть текущий сокет.
        this.dispatch('closeSocket');
        state.searchData = {
          ...state.searchData,
          task: null,
        };
      } catch (error) {
        console.log('TCL: error', error);
        // TODO: конвертировать оишбку
        this.commit(actionTypes.SET_EVENTS_ERROR, error);
      }
    }
  },
  async [actionTypes.FETCH_SELECTED_EVENTS]({ state }) {
    this.commit(actionTypes.SET_EVENTS_LOADING, true);
    if (state.eventsSelectedChunk !== null) {
      try {
        const params = {
          limit: state.eventsPagination.limit,
          offset: state.eventsPagination.offset,
          begin: state.eventsSelectedChunk.begin,
          end: state.eventsSelectedChunk.end,
          search: state.eventsFilters.search,
          srcip: state.eventsFilters.srcip,
          device_ip: state.eventsFilters.device_ip,
          protocol: state.eventsFilters.protocol,
          status: state.eventsFilters.status,
          srcuser: state.eventsFilters.srcuser,
          action: state.eventsFilters.action,
        };
        const rspns = await API.fetchEvents(params);
        this.commit(actionTypes.SET_EVENTS_PAGINATION, {
          ...state.eventsPagination,
          next: rspns.data.next,
          previous: rspns.data.previous,
          count: rspns.data.count,
        });
        this.commit(actionTypes.SET_EVENTS_DATA, rspns.data.results);
      } catch (error) {
        console.log('TCL: error', error);
        // TODO: конвертировать оишбку
        this.commit(actionTypes.SET_EVENTS_ERROR, error);
      }
      this.commit(actionTypes.SET_EVENTS_LOADING, false);
    }
  },
  async [actionTypes.LOAD_EVENTS_CHUNK_INFO]({ state }) {
    if (state.eventsCache.selectedTask !== null) {
      this.commit(actionTypes.SET_EVENTS_LOADING, true);
      try {
        const rspns = await API.extractCachedChunks({
          task: state.eventsCache.selectedTask,
        });
        this.commit(actionTypes.SET_EVENTS_SEARCH_DATA, {
          ...state.getEventsSearchData,
          chunks: rspns.data,
        });
      } catch (error) {
        console.log('TCL: error', error);
        // TODO: конвертировать оишбку
        this.commit(actionTypes.SET_EVENTS_ERROR, error);
      }
      this.commit(actionTypes.SET_EVENTS_LOADING, false);
    }
  },
  async [actionTypes.LOAD_EVENTS_HISTORY]({ state }) {
    try {
      const rspns = await API.fetchCachedSearchResults();
      this.commit(actionTypes.SET_EVENTS_CACHE, {
        ...state.eventsCache,
        historyTasks: rspns.data,
      });
    } catch (error) {
      console.log('TCL: error', error);
      // TODO: конвертировать оишбку
      this.commit(actionTypes.SET_EVENTS_ERROR, error);
    }
  },
};
