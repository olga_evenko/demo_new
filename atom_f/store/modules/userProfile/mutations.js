import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_ERROR](state, error) {
    state.userProfileError = error;
  },
  [actionTypes.SET_LOADING](state, loading) {
    state.userProfileLoading = loading;
  },
  [actionTypes.SET_PASSWORD_CHANGING](state, status) {
    state.passwordChanging = status;
  },
  [actionTypes.SET_USER_PROFILE](state, profile) {
    state.userProfile = profile;
  },
};
