export default {
  userProfileError: null,
  userProfileLoading: false,
  userProfileSuccess: null,
  passwordChanging: false,
  userProfile: null,
};
