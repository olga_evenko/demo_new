export default {
  getUserProfileError: state => state.userProfileError,
  getUserProfileLoading: state => state.userProfileLoading,
  getPasswordChangingStatus: state => state.passwordChanging,
  getuserProfile: state => state.userProfile,
};