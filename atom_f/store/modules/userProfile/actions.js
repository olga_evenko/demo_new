import * as actionTypes from './actionTypes';
import api from '@/services/api';
import globalActionTypes from '@/store/actionTypes';
import store from '@/store';

export default {
  async [actionTypes.UPDATE_USER_PROFILE](_, userData) {
    this.commit(actionTypes.SET_ERROR, null);
    this.commit(actionTypes.SET_LOADING, true);
    try {
      if(userData.id){
        const rspns = await api.updateUserProfile(userData);
      }else{
        const rspns = await api.createUserProfile(userData);
      }
    } catch (e) {
      console.log('TCL: e', e);
      this.commit(
        actionTypes.SET_ERROR,
        'Ошибка на удаленном сервере.',
      );
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
  async [actionTypes.CHANGE_USER_PASSWORD](_, data) {
    this.commit(actionTypes.SET_LOADING, true);
    try {
      const rspns = await api.changePassword(data);
    } catch (e) {
      this.commit(
        actionTypes.SET_ERROR,
        'Ошибка на удаленном сервере',
      );
    }
    this.commit(actionTypes.SET_LOADING, false);
  },
};
