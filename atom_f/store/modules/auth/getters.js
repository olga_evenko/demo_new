export default {
  getUser: state => state.userPofile,
  getUserLoading: state => state.loading,
  getUserToken: state => state.token,
  getAuthError: state => state.authError,
  getOtp: state => state.otp,
  getBlockTime: state => state.blockTime,
  getBlockResendTime: state => state.blockResendTime,
  getPasswordReset: state => state.passwordReset,
  getPasswordResetBlock: state => state.passwordResetBlock,
  getBanTime: state => state.banTime,
};
