import * as actionTypes from './actionTypes';
import cookies from 'vue-cookies';
import API from '@/services/api';
import globalActionTypes from '@/store/actionTypes';

export default {
  [actionTypes.TRY_REMOTE_AUTH](_, userData) {
    this.commit(actionTypes.SET_LOADING, true);
    this.dispatch(actionTypes.RESET);
    API.auth(userData)
      .then((rspns) => {
        if (rspns.data.otp) {
          this.commit(actionTypes.SET_OTP, rspns.data.messageBroker);
          this.commit(actionTypes.SET_BLOCK_TIME, rspns.blockTime);
          return;
        }
        this.commit(actionTypes.SET_USER_TOKEN, rspns.data.token);
        this.commit(actionTypes.SET_USER_DATA, rspns.data.user);
      })
      .catch((e) => {
        // TODO: сервис для конвертации ошибки в читабельный формат
        console.log('TCL: e', e);
        if (!e.response.data) {
          return;
        }
        if (e.response.status === 429) {
          this.commit(actionTypes.SET_ERROR, 'ManyAttempts');
          this.commit(actionTypes.SET_BAN_TIME, +e.response.data.banTime);
          return;
        }
        this.commit(actionTypes.SET_ERROR, e.response.data.error);
        if (this.getters.getAuthError === 'BlockResend') {
          this.commit(
            actionTypes.SET_BLOCK_RESEND_TIME,
            e.response.data.blockTime,
          );
          return;
        }
        this.commit(actionTypes.SET_BLOCK_TIME, e.response.data.blockTime);
      })
      .finally(() => {
        this.commit(actionTypes.SET_LOADING, false);
      });
  },
  async [actionTypes.CHECK_TOKEN](_, sliceMode) {
    this.commit(actionTypes.SET_LOADING, true);
    try {
      const rspns = await API.getPersonalInfo();
      this.commit(actionTypes.SET_USER_DATA, rspns.data);
    } catch (e) {
      if (!sliceMode) {
        if (e === 'TokenNotFound') {
          this.commit(actionTypes.SET_TOKEN_NOT_FOUND);
          return;
        }
        this.commit(
          'setUserError',
          'Ваша сессия истекла. Войдите повторно в систему.',
        );
      }
    } finally {
      this.commit(actionTypes.SET_LOADING, false);
    }
  },
  async [actionTypes.TRY_REFRESH_TOKEN]() {
    const token = cookies.get('token');
    if (!token) {
      return;
    }
    try {
      const rspns = await API.tryRefreshToken(token);
      this.commit(actionTypes.SET_USER_TOKEN, rspns.data.token);
    } catch (e) {
      this.commit(
        'setUserError',
        'Ваша сессия истекла. Войдите повторно в систему.',
      );
    } finally {
      this.commit(actionTypes.SET_LOADING, false);
    }
  },
  async [actionTypes.TRY_RESET_PASSWORD](_, resetingData) {
    this.commit(actionTypes.SET_LOADING, true);
    this.commit('setAuthError', null);
    try {
      const rspns = await API.resetPassword(resetingData);
      if (rspns.data.otp) {
        this.commit(actionTypes.SET_OTP, rspns.data.messageBroker);
        return;
      }
      this.commit(actionTypes.SET_PASSWORD_RESET_STATUS, true);
    } catch (e) {
      if (!e.response) {
        return;
      }
      switch (e.response.data.error) {
        case 'IncorrectCode':
          if (this.getters.getOtp) {
            this.commit(actionTypes.SET_ERROR, 'Неверный код.');
            return;
          }
          this.commit(actionTypes.SET_OTP, {});
          break;
        case 'ManyAttempts':
          this.commit(actionTypes.SET_ERROR, 'Слишком много попыток входа.');
          this.commit(
            actionTypes.SET_PASSWORD_RESET_BLOCK,
            e.response.data.blockTime,
          );
          break;
        case 'BlockResend':
          this.commit(
            actionTypes.SET_BLOCK_RESEND_TIME,
            e.response.data.blockTime,
          );
          break;
        default:
          break;
      }
    } finally {
      this.commit(actionTypes.SET_LOADING, false);
    }
  },
  async [actionTypes.RESET]({ dispatch }) {
    this.commit(actionTypes.SET_BLOCK_TIME, 0);
    this.commit(actionTypes.SET_ERROR, null);
    this.commit(actionTypes.SET_PASSWORD_RESET_BLOCK, null);
    this.commit(actionTypes.SET_BLOCK_RESEND_TIME, null);
    this.commit(actionTypes.SET_PASSWORD_RESET_STATUS, false);
  },
  // eslint-disable-next-line
  async [actionTypes.LOGOUT]({ state, dispatch }) {
    await dispatch(globalActionTypes.events.RESET_EVENTS);
    state.userPofile = {
      isAuth: false,
    };
    state.token = null;
    state.error = null;
    state.loading = null;
    state.otp = false;
    this.commit(globalActionTypes.users.SET_USERS, null);
    this.commit(actionTypes.LOGOUT);
  },
};
