import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_USER_TOKEN](state, token) {
    state.token = token;
  },
  [actionTypes.SET_USER_DATA](state, userData) {
    state.userPofile = {
      ...state.userProfile,
      ...userData,
      isAuth: true,
    };
  },
  [actionTypes.SET_ERROR](state, error) {
    state.authError = error;
  },
  [actionTypes.SET_LOADING](state, loadingStatus) {
    state.loading = loadingStatus;
  },
  [actionTypes.SET_TOKEN_NOT_FOUND]() {
    // Simple state for interrupt auth from cookies token
  },
  [actionTypes.SET_OTP](state, otpSign) {
    /**
     * @param {Object} otpSign сигнатура метода отправки одноразового пароля
     * { broker: String (метод отправки сообщения), payload: String (путь назначения) }
     */
    state.otp = otpSign;
  },
  [actionTypes.SET_BLOCK_TIME](state, blockTime) {
    state.blockTime = blockTime;
  },
  [actionTypes.SET_BLOCK_RESEND_TIME](state, blockTime) {
    state.blockResendTime = blockTime;
  },
  [actionTypes.SET_PASSWORD_RESET_STATUS](state, status) {
    state.passwordReset = status;
  },
  [actionTypes.SET_PASSWORD_RESET_BLOCK](state, blockTime) {
    /**
     * Блокировка по времени
     * @param {Number} blockTime время блокировки в секундах.
     */
    state.passwordResetBlock = blockTime;
  },
  [actionTypes.SET_BAN_TIME](state, time) {
    state.banTime = time;
  },
  [actionTypes.LOGOUT]() {

  },
};
