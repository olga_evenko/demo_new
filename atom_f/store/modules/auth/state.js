export const initialState = {
  userPofile: {
    username: null,
    firstName: null,
    lastName: null,
    middleName: null,
    isAuth: false,
  },
  otp: null,
  token: null,
  loading: null,
  blockTime: null,
  banTime: null,
  passwordResetBlock: null,
  authError: null,
  blockResendTime: null,
  passwordReset: false,
  otp_reset: false,
};

export default {
  ...initialState,
};
