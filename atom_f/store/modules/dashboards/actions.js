import * as actionTypes from './actionTypes';
import globalActionTypes from '@/store/actionTypes';
import API from '@/services/api';

export default {
  async [actionTypes.ADD_DASHBOARD](_, dashboard) {
    try {
      const createdDashboard = await API.createDashboard(dashboard);
      this.commit(actionTypes.ADD_DASHBOARD, createdDashboard);
    } catch (e) {
      const modalData = { type: 'ModalError' };
      if (e.response) {
        modalData.data = {
          error: `Произошла ошибка с кодом: ${e.response.status}`,
        };
      }
      this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, modalData);
    }
  },
  async [actionTypes.ADD_WIDGET](_, metaDataWidget) {
    const dashboard = { ...this.getters.getSelectedDashboard };

    let maxY = 0;
    dashboard.widgets.forEach((pos) => {
      if (pos.y >= maxY) {
        maxY = pos.y + pos.h;
      }
    });
    const newWidget = {
      x: 0,
      y: maxY,
      w: 12,
      h: 13,
      i: dashboard.widgets.length + 1,
      // minH: 13,
      ...metaDataWidget,
    };
    try {
      const widget = await API.createWidget(newWidget, dashboard.id);
      dashboard.widgets.push(widget);
      this.commit(actionTypes.EDIT_DASHBOARD, dashboard);
    } catch (e) {
      // TODO: сделай наконец экщены для перехвата ошибок извне.
      console.log(e);
      const modalData = { type: 'ModalError' };
      if (e.response) {
        modalData.data = {
          error: `Произошла ошибка с кодом: ${e.response.status}`,
        };
      }
      this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, modalData);
    }

    this.commit(actionTypes.EDIT_DASHBOARD, dashboard);
  },
  async [actionTypes.ADD_WIDGET_PICTURE](_, widgetMetaData) {
    // Обработка предварительной загрузки изображения.
    this.dispatch(actionTypes.SET_WIDGET_PICTURE, [
      widgetMetaData,
      actionTypes.ADD_WIDGET,
    ]);
  },
  async [actionTypes.UPDATE_WIDGET_PICTURE](_, widgetMetaData) {
    // Обработка предварительной загрузки изображения.
    this.dispatch(actionTypes.SET_WIDGET_PICTURE, [
      widgetMetaData,
      actionTypes.EDIT_WIDGET,
    ]);
  },
  async [actionTypes.SET_WIDGET_PICTURE](_, [widgetMetaData, dispatchAction]) {
    try {
      const rspns = await API.uploadOrganisationImage(
        widgetMetaData.dataWidget.image,
      );
      const metaDataWidget = {
        ...widgetMetaData,
        dataWidget: {
          src: rspns.data.path,
        },
      };
      this.dispatch(dispatchAction, metaDataWidget);
    } catch (e) {
      console.log('TCL: e', e);
      const modalData = { type: 'ModalError' };
      if (e.response) {
        modalData.data = {
          error: `Произошла ошибка с кодом: ${e.response.status}`,
        };
      }
      this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, modalData);
    }
  },
  async [actionTypes.EDIT_WIDGET](_, widget) {
    const dashboard = { ...this.getters.getSelectedDashboard };
    try {
      await API.updateWidget(widget);
      // eslint-disable-next-line
      for (const i in dashboard.widgets) {
        if (dashboard.widgets[i].i === widget.i) {
          dashboard.widgets[i] = widget;
          this.commit(actionTypes.EDIT_DASHBOARD, dashboard);
          break;
        }
      }
    } catch (e) {
      // TODO: сервис обработки ошибок
      console.log(e);
    }
  },
  async [actionTypes.DELETE_WIDGET](context, widget) {
    const dashboard = context.state.dashboards[context.state.selectedDashboardId];
    try {
      await API.deleteWidget(widget.id);
      // eslint-disable-next-line
      for (const i in dashboard.widgets) {
        if (dashboard.widgets[i].i === widget.i) {
          dashboard.widgets.splice(i, 1);
          this.commit(actionTypes.EDIT_DASHBOARD, dashboard);
          break;
        }
      }
    } catch (e) {
      // TODO: реализовать через слой ошибок.
      console.log(e);
    }

    // TODO: перенести в проперти дашбордов. => сделать дашборд классом.
  },
  // eslint-disable-next-line
  async [actionTypes.LOAD_DASHBOARDS]() {
    try {
      const dashboards = await API.getDashboards();
      this.commit(actionTypes.SET_DASHBOARDS, dashboards);
    } catch (e) {
      console.log('TCL: e', e);

      if (e.response) {
        const modalData = { type: 'ModalError' };
        modalData.data = {
          error: `При загрузки списка панелей управления произошла ошибка со статусом ${
            e.response.status
          }`,
        };
        this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, modalData);
      }
    }
  },
  async [actionTypes.DELETE_SELECTED_DASHBOARD]() {
    const selectedDashboard = this.getters.getSelectedDashboard;
    try {
      await API.deleteDashboard(selectedDashboard.id);
      const modalData = {
        type: 'ModalSuccess',
        data: { title: `Панель ${selectedDashboard.title} успешно удалена.` },
      };
      this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, modalData);
      this.commit(actionTypes.DELETE_SELECTED_DASHBOARD);
    } catch (e) {
      const modalData = { type: 'modalError' };
      if (e.response) {
        modalData.data = {
          error: `Произошла ошибка с кодом: ${e.response.status}`,
        };
      }
      this.commit(globalActionTypes.ui.SET_MODAL_WINDOW_TYPE, modalData);
    }
  },
  async [actionTypes.LOAD_DASHBOARDS_TYPES](context) {
    if (context.state.dashboardTypes) {
      this.commit(actionTypes.SET_DASHBOARD_TYPE, context.state.dashboardTypes);
      return;
    }
    try {
      const rspns = await API.getDashboardTypes();
      this.commit(actionTypes.SET_DASHBOARD_TYPE, rspns.data);
      // eslint-disable-next-line
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.EDIT_DASHBOARD](_, dashboard) {
    const data = { ...dashboard };
    delete data.widgets;
    try {
      await API.updateDashboard(data);
      this.commit(actionTypes.EDIT_DASHBOARD, dashboard);
    } catch (e) {
      if (e.response) {
        console.log(e.response);
      } else {
        console.log(e);
      }
    }
  },
  async [actionTypes.UPDATE_WIDGETS_POSITIONS](_, dashboard) {
    try {
      const data = JSON.parse(JSON.stringify(dashboard));
      await API.updateDashboard(data);
      this.commit(actionTypes.EDIT_DASHBOARD, dashboard);
    } catch (e) {
      if (e.response) {
        console.log(e.response);
      } else {
        console.log(e);
      }
    }
  },
  async [actionTypes.SWAP_DASHBOARDS_POSITIONS](
    { state, commit },
    { oldIndex, newIndex },
  ) {
    // Проверка на коллизии дашбордов, ну мало ли
    const tmpDashboards = [...state.dashboards];
    const oldDasbhoard = state.dashboards[oldIndex];
    const newDashboard = state.dashboards[newIndex];
    tmpDashboards[oldIndex] = newDashboard;
    tmpDashboards[newIndex] = oldDasbhoard;
    commit(actionTypes.SET_DASHBOARDS, tmpDashboards);
    try {
      await API.swapDashboards(oldDasbhoard, newDashboard);
    } catch (e) {
      console.log(e);
    }
  },
};
