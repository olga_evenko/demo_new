import * as actionTypes from './actionTypes';

const collapsedHeight = 2;

const collapseWidgetInDashboard = (id, dashboard) => {
  if (!dashboard.widgets) {
    return -1;
  }
  const widgetIndex = findWidgetIndex(id, dashboard);
  const widget = dashboard.widgets[widgetIndex];

  if (!widget.collapse) {
    widget.actualH = widget.h;
    widget.minH = collapsedHeight;
    widget.h = collapsedHeight;
  } else {
    widget.minH = 6;
    widget.h = widget.actualH;
  }
  widget.collapse = !widget.collapse;
  dashboard.widgets[widgetIndex] = widget;
  return dashboard;
};
const findWidgetIndex = (id, dashboard) => {
  if (!dashboard.widgets) {
    return -1;
  }
  let widgetIndex = -1;
  try {
    dashboard.widgets.forEach((w, i) => {
      if (+w.i === +id) {
        widgetIndex = i;
        throw Error('Break');
      }
    });
  } catch (e) {
    if (!(e.message === 'Break')) {
      throw e;
    }
  }

  return widgetIndex;
};

export default {
  [actionTypes.SET_DASHBOARDS](state, dashboards) {
    state.dashboards = dashboards;
  },
  [actionTypes.ADD_DASHBOARD](state, dashboard) {
    state.dashboards.push(dashboard);
    this.commit(actionTypes.SELECT_DASHBOARD, state.dashboards.length - 1);
  },
  [actionTypes.SELECT_DASHBOARD](state, id) {
    state.selectedDashboardId = id;
  },
  [actionTypes.DELETE_SELECTED_DASHBOARD](state) {
    state.dashboards.splice(state.selectedDashboardId, 1);
    if (state.dashboards) {
      this.commit(actionTypes.SELECT_DASHBOARD, 0);
      return;
    }
    this.commit(actionTypes.SELECT_DASHBOARD, -1);
  },
  [actionTypes.EDIT_DASHBOARD](state, dashboard) {
    state.dashboards.splice(state.selectedDashboardId, 1, dashboard);
  },
  [actionTypes.SET_ERROR](state, error) {
    state.dashboardsError = error;
  },
  [actionTypes.SET_LOADING](state, status) {
    state.isDashboardsLoading = status;
  },
  [actionTypes.SET_DASHBOARD_TYPE](state, types) {
    state.dashboardTypes = types;
  },
  [actionTypes.TOGGLE_WIDGET](state, widget) {
    const dashboard = this.getters.getSelectedDashboard;
    const collapsedDashboard = collapseWidgetInDashboard(widget.i, dashboard);
    console.log(state);
    state.dashboards.splice(
      this.getters.getSelectedDashboardId,
      1,
      collapsedDashboard,
    );
  },
};
