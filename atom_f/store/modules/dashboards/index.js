import state from './state';
import actions from './actions';
import mutations from './mutations';
import getters from './getters';


export default {
  // TODO: сделать класс для дашбордов после формирования единой структуры данных. (づ｡◕‿‿◕｡)づ
  state,
  getters,
  mutations,
  actions,
};
