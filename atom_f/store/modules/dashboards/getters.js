export default {
  getDashboards: state => state.dashboards,
  getDashboardsLoading: state => state.isDashboardsLoading,
  getDashboardsError: state => state.dashboardsError,
  // eslint-disable-next-line
  getSelectedDashboard: state => state.dashboards[state.selectedDashboardId],
  getSelectedDashboardId: state => state.selectedDashboardId,
  getDashboardTypes: state => state.dashboardTypes,
  getDashboardLoading: state => state.dashboardsLoading,
}
;
