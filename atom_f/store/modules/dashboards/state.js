export default {
  dashboards: [],
  dashboardLoading: false,
  dashboardsError: null,
  dashboardTypes: null,
  selectedDashboardId: 0,
};
