import { defaultPagination } from './constants';
import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_ASSETS](state, assets) {
    const { sortBy } = state.assetsSortConfig;
    if (sortBy) {
      assets.sort((o1, o2) => {
        if (o1[sortBy] < o2[sortBy]) { return -1 * state.assetsSortConfig.forward; }
        if (o1[sortBy] > o2[sortBy]) { return 1 * state.assetsSortConfig.forward; }
        return 0;
      });
    }
    state.assets = assets;
  },
  [actionTypes.SET_ASSETS_PAGINATION](state, pagination) {
    state.assetsPagination = pagination;
  },
  [actionTypes.SET_ASSETS_LOADING](state, loadingStatus) {
    state.assetsLoading = loadingStatus;
  },
  [actionTypes.SET_ASSETS_ERROR](state, error) {
    state.assetsError = error;
  },
  [actionTypes.RESET_ASSETS_PAGINATION](state) {
    state.assetsPagination = { ...defaultPagination };
  },
  [actionTypes.SET_ASSETS_SEARCH_PARAMS](state, params) {
    state.assetsSearchParams = params;
  },
  [actionTypes.SET_ASSETS_COUNT](state, count) {
    state.assetsCount = count;
  },
  [actionTypes.SET_ASSETS_SEARCH_HISTORY](state, history) {
    state.assetsSearchHistory = history;
  },
  [actionTypes.SET_ASSETS_COUNT_META_INFO](state, meta) {
    state.assetsCountMetaInfo = meta;
  },
  [actionTypes.SET_POPULAR_ASSET_QUERIES](state, queris) {
    state.popularAssetsQueries = queris;
  },
  [actionTypes.SET_SELECTED_ASSET](state, asset) {
    state.selectedAsset = asset;
  },
  [actionTypes.SET_ASSETS_SORT_PARAM](state, sortBy) {
    let forward = state.assetsSortConfig.forward;
    if (sortBy === state.assetsSortConfig.sortBy) {
      forward *= -1;
    }
    state.assetsSortConfig = {
      sortBy,
      forward,
    };
    this.commit(actionTypes.SET_ASSETS, state.assets);
  },
};
