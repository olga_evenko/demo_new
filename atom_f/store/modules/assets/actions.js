import API from '@/services/api';
import * as actionTypes from './actionTypes';

export default {
  [actionTypes.RESET_ASSETS]({ commit }) {
    commit(actionTypes.SET_ASSETS_PAGINATION, { ...defaultPagination });
    commit(actionTypes.SET_ASSETS, null);
    commit(actionTypes.SET_ASSETS_ERROR, null);
    commit(actionTypes.SET_ASSETS_LOADING, false);
  },
  async [actionTypes.LOAD_ASSETS]({ state, commit }) {
    commit(actionTypes.SET_ASSETS_ERROR, null);
    commit(actionTypes.SET_ASSETS_LOADING, true);
    try {
      const rspns = await API.loadAssets(state.assetsPagination);
      commit(actionTypes.SET_ASSETS, rspns.data.results);
      commit(actionTypes.SET_ASSETS_COUNT, rspns.data.count);
      commit(actionTypes.SET_ASSETS_PAGINATION, {
        ...state.assetsPagination,
        next: rspns.data.next,
        previous: rspns.data.previous,
        count: rspns.data.count,
      });
    } catch (e) {
      console.log('TCL: e', e);
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
    commit(actionTypes.SET_ASSETS_LOADING, false);
  },
  async [actionTypes.SEARCH_ASSETS]({ state, commit }) {
    commit(actionTypes.SET_ASSETS_LOADING, true);
    commit(actionTypes.SET_ASSETS_ERROR, null);
    try {
      const rspns = await API.searchAssets(state.assetsSearchParams);
      commit(actionTypes.SET_ASSETS_PAGINATION, {
        ...state.assetsPagination,
        next: rspns.data.next,
        previous: rspns.data.previous,
        count: rspns.data.count,
      });
      commit(actionTypes.SET_ASSETS_COUNT, rspns.data.count);
      commit(actionTypes.SET_ASSETS, rspns.data.results);
      let updatedHistory = [...state.assetsSearchHistory];
      updatedHistory.unshift({ ...state.assetsSearchParams });
      if (updatedHistory.length > 10) {
        updatedHistory = updatedHistory.slice(0, 10);
      }
      commit(actionTypes.SET_ASSETS_SEARCH_HISTORY, updatedHistory);
    } catch (e) {
      console.log('TCL: e', e);
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
    commit(actionTypes.SET_ASSETS_LOADING, false);
  },
  async [actionTypes.LOAD_ASSETS_SEARCH_HISTORY]({ state, commit }) {
    commit(actionTypes.SET_ASSETS_ERROR, null);
    try {
      const rspns = await API.loadAssetsHistory();
      commit(actionTypes.SET_ASSETS_SEARCH_HISTORY, rspns.data || []);
    } catch (e) {
      console.log('TCL: e', e);
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
  },
  async [actionTypes.RESET_ASSETS_SEARCH_HISTORY]({ commit }) {
    commit(actionTypes.SET_ASSETS_ERROR, null);
    try {
      await API.resetAssetsHistory();
      commit(actionTypes.SET_ASSETS_SEARCH_HISTORY, []);
    } catch (e) {
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
  },
  async [actionTypes.LOAD_ASSETS_COUNT_META_INFO]({ commit }) {
    try {
      const rspns = await API.loadAssetsMetaInfo();
      commit(actionTypes.SET_ASSETS_COUNT_META_INFO, rspns.data);
    } catch (e) {
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
  },
  async [actionTypes.LOAD_POPULAR_ASSETS_QUERIES]({ commit }) {
    commit(actionTypes.SET_ASSETS_ERROR, null);
    try {
      const rspns = await API.loadPopularAssetsQueries();
      commit(actionTypes.SET_POPULAR_ASSET_QUERIES, rspns.data);
    } catch (e) {
      console.log('TCL: e', e);
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
  },
  async [actionTypes.LOAD_ASSET_BY_ID]({ state, commit }, id) {
    if (state.assets) {
      const selectedAsset = state.assets.find(e => +e.id === +id);

      if (selectedAsset) {
        commit(actionTypes.SET_SELECTED_ASSET, selectedAsset);
        return;
      }
      commit(actionTypes.SET_ASSETS_ERROR, null);
      commit(actionTypes.SET_ASSETS_LOADING, true);
      return;
    }

    // TODO: загрузка записи с сервера.
    try {
      const rspns = await API.getAssetById(id);
      commit(actionTypes.SET_SELECTED_ASSET, rspns.data);
    } catch (e) {
      console.log('TCL: e', e);
      commit(actionTypes.SET_ASSETS_ERROR, e);
    }
    commit(actionTypes.SET_ASSETS_LOADING, false);
  },
};
