export const defaultPagination = {
  limit: 50,
  offset: 0,
  next: null,
  previous: null,
  count: 0,
};
