export default {
  getAssets: state => state.assets,
  getAssetsError: state => state.assetsError,
  getAssetsLoading: state => state.assetsLoading,
  getAssetsPagination: state => state.assetsPagination,
  getAssetsSearchHistory: state => state.assetsSearchHistory,
  getAssetsSearchParams: state => state.assetsSearchParams,
  getAssetsCount: state => state.assetsCount,
  getAssetsCountMetaInfo: state => state.assetsCountMetaInfo,
  getPopularAssetsQueries: state => state.popularAssetsQueries,
  getSelectedAsset: state => state.selectedAsset,
};
