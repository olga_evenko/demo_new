import { defaultPagination } from './constants';

export default {
  assets: null,
  assetsError: null,
  assetsLoading: false,
  assetsPagination: { ...defaultPagination },
  assetsSearchHistory: [],
  popularAssetsQueries: [],
  assetsSearchParams: null,
  assetsCount: 0,
  assetsCountMetaInfo: null,
  selectedAsset: null,
  assetsSortConfig: {
    sortBy: null,
    forward: 1,
  },
};
