import * as actionTypes from './actionTypes';

export default {
  state() {
    return {
      locale: 'ru',
      locales: ['en', 'ru', 'uk', 'es', 'fr', 'ar', 'de', 'kk', 'be'],
    };
  },
  getters: {
    getLocale: state => state.locale,
    getLocales: state => state.locales,
  },
  mutations: {
    [actionTypes.SET_LOCALE](state, locale) {
      if (state.locales.indexOf(locale) !== -1) {
        state.locale = locale;
      } else {
        console.warn('Unsupported language');
      }
    },
  },
  actions: {},
};
