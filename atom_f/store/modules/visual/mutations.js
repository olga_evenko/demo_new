import * as actionTypes from './actionTypes';

export default {
  [actionTypes.TOGGLE_SIDEBAR](state) {
    state.sideBarActive = !state.sideBarActive;
  },
  [actionTypes.TOGGLE_SECOND_SIDEBAR](state) {
    state.secondSideBarStatus = !state.secondSideBarStatus;
  },
  [actionTypes.SET_SIDEBAR_BUTTON_STATUS](state, status) {
    state.sideBarButtonStatus = status;
  },
  [actionTypes.SET_SIDEBAR_STATUS](state, status) {
    state.sideBarActive = status;
  },
  [actionTypes.SET_SUB_NAVBAR_STATUS](state, status) {
    state.subNavBarActiveStatus = status;
  },
  [actionTypes.SET_SUB_NAVBAR_FIXED](state, status) {
    state.subNavBarFixed = status;
  },
  [actionTypes.SET_SUB_NAVBAR_TYPE](state, type) {
    state.subNavBarType = type;
  },
  [actionTypes.SET_BLACKOUT_STATUS](state, status) {
    state.blackout = status;
  },
  [actionTypes.SET_DASHBOARD_INFO_STATUS](state, status) {
    state.showDashboardInfo = status;
  },
  [actionTypes.SET_SUB_NAVBAR_HEADER_STATUS](state, status) {
    state.subNavBarHeaderStatus = status;
  },
  [actionTypes.TOGGLE_SUB_NAVBAR_HEADER](state) {
    state.subNavBarHeaderStatus = !state.subNavBarHeaderStatus;
  },
  [actionTypes.SET_MODAL_WINDOW_TYPE](state, data) {
    data = data || {};
    state.modalWindowType = data.type;
    state.modalWindowData = data.data;
  },
  [actionTypes.CLOSE_MODAL_WINDOW](state) {
    this.commit(actionTypes.SET_MODAL_WINDOW_TYPE, null);
  },
  [actionTypes.SET_DOCUMENT_META](state, meta) {
    state.documentMeta = meta;
  },
  [actionTypes.SET_SUB_NAVBAR_HEADER_COLLAPSE](state, collapseStatus) {
    state.collapseSubNavBarHeader = collapseStatus;
  },
  [actionTypes.SET_PERSONAL_VISUAL_CONFIG](state, personalVisualConfig) {
    state.personalVisualConfig = personalVisualConfig;
  },
  [actionTypes.SET_PERSONAL_CONFIG_LOADED](state, loaded) {
    state.personalConfigLoaded = loaded;
  },
  [actionTypes.SET_TITLE_PAGE](state, value) {
    if (value.action === 'add') {
      if (!state.titlePage.includes(value.data)) {
        state.titlePage.push(value.data);
      }
    }
    if (value.action === 'remove') {
      if (state.titlePage.includes(value.data)) {
        state.titlePage.splice(state.titlePage.indexOf(value.data), 1);
      }
    }
  },
};
