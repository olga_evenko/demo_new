import * as actionTypes from './actionTypes';
import API from '@/services/api';

export default {
  async [actionTypes.LOAD_PERSONAL_VISUAL_CONFIG]({ commit, dispatch }) {
    try {
      const rspns = await API.loadPersonalVisualConfig();
      commit(actionTypes.SET_PERSONAL_VISUAL_CONFIG, rspns.data);
    } catch (e) {
      if (e.response.status === 404) {
        dispatch(actionTypes.SAVE_PERSONAL_VISUAL_CONFIG);
      } else {
        console.log('TCL: e', e);
      }
    }
    commit(actionTypes.SET_PERSONAL_CONFIG_LOADED, true);
  },
  async [actionTypes.SAVE_PERSONAL_VISUAL_CONFIG]({ state }) {
    try {
      await API.savePersonalVisualConfig(state.personalVisualConfig);
    } catch (e) {
      console.log(e);
    }
  },
};
