import * as actionTypes from './actionTypes';

export default {
  [actionTypes.SET_ERROR](state, error) {
    state.patternsError = error;
  },
  [actionTypes.SET_LOADING](state, loading) {
    state.patternsLoading = loading;
  },
  [actionTypes.SET_PATTERNS](state, patterns) {
    state.patterns = patterns;
  },
  [actionTypes.SET_PAGINATION](state, pagination) {
    state.patternsPagination = pagination;
  },
};
