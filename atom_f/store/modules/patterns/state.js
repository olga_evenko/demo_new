export default {
  patterns: null,
  patternsLoading: false,
  patternsError: null,
  patternsPagination: {
    next: null,
    previous: null,
    offset: 0,
    limit: 10,
  },
};
