import api from '@/services/api';
import * as actionTypes from './actionTypes';

export default {
  async [actionTypes.LOAD_PATTERNS]({ state, commit }) {
    try {
      const { limit, offset } = state.patternsPagination;
      const rspns = await api.loadPatterns({ limit, offset });
      commit(actionTypes.SET_PATTERNS, rspns.data.results);
      commit(actionTypes.SET_PAGINATION, {
        ...state.patternsPagination,
        count: rspns.data.count,
        next: rspns.data.next,
        previous: rspns.data.previous,
      });
    } catch (e) {
      console.log('TCL: e', e);
    }
  },
  async [actionTypes.UPDATE_PATTERN]({ state, commit }, pattern) {
    try {
      const rspns = await api.updatePattern(pattern);
      const patterns = [...state.patterns];
      const patternIndex = patterns.findIndex(p => p.id === pattern.id);
      patterns[patternIndex] = pattern;
      commit(actionTypes.SET_PATTERNS, patterns);
    } catch (e) {
      console.log(e);
    }
  },
};
