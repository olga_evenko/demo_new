export default {
  getPatterns: state => state.patterns,
  getPatternsLoading: state => state.patternsLoading,
  getPatternsError: state => state.patternsError,
  getPatternsPagination: state => state.patternsPagination,
};
