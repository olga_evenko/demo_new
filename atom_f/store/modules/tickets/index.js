import * as actionTypes from './actionTypes';

export default {
  state: {
    tickets: [
      {
        id: '1',
        name: 'ticket 1',
        description: 'lorem ipsum',
        date: new Date(),
      },
      {
        id: '2',
        name: 'ticket 2',
        description: 'lorem ipsum',
        date: new Date(),
      },
      {
        id: '3',
        name: 'ticket 3',
        description: 'lorem ipsum',
        date: new Date(),
      },
    ],
  },
  getters: {
    getTickets: state => state.tickets,
  },
};
