import Vue from 'vue';
import Vuex from 'vuex';
// import VuexPersistence from 'vuex-persist';
import createMutationsSharer from 'vuex-shared-mutations';
import actionTypes from './actionTypes';

Vue.use(Vuex);

function generateStoreName(fileName) {
  return fileName.slice(2, fileName.length - 3);
}

function collectStore() {
  const requireModule = require.context('./modules', true);

  const stores = {};

  requireModule.keys().forEach((path) => {
    const r = path.match(/\.\/(\w*)\/index/);
    if (r) {
      const storeName = r[1];
      stores[storeName] = requireModule(path).default;
    }
  });
  return stores;
}

const collectedStores = collectStore();

export default new Vuex.Store({
  plugins: [
    createMutationsSharer({
      predicate: [
        actionTypes.dashboards.ADD_DASHBOARD,
        actionTypes.dashboards.DELETE_SELECTED_DASHBOARD,
        actionTypes.dashboards.EDIT_DASHBOARD,
        actionTypes.dashboards.TOGGLE_WIDGET,
        actionTypes.dashboards.SET_WIDGET_PICTURE,
        actionTypes.dashboards.ADD_WIDGET,
        actionTypes.dashboards.ADD_WIDGET_PICTURE,
        actionTypes.dashboards.DELETE_WIDGET,
        actionTypes.dashboards.EDIT_DASHBOARD,
        actionTypes.dashboards.UPDATE_WIDGET_PICTURE,
        actionTypes.dashboards.UPDATE_WIDGETS_POSITIONS,
        actionTypes.dashboards.SWAP_DASHBOARDS_POSITIONS,
      ],
    }),
  ],
  modules: {
    ...collectedStores,
  },
});
