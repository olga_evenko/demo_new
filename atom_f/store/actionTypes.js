// Профиль пользователя
import * as authActionTypes from './modules/auth/actionTypes';
import * as uiActionTypes from './modules/visual/actionTypes';
import * as eventsActionTypes from './modules/events/actionTypes';
import * as assetsActionTypes from './modules/assets/actionTypes';
import * as dashboardsActionTypes from './modules/dashboards/actionTypes';
import * as localeActionTypes from './modules/locale/actionTypes';
import * as ticketsActionTypes from './modules/tickets/actionTypes';
import * as userProfileActionTypes from './modules/userProfile/actionTypes';
import * as usersActionTypes from './modules/users/actionTypes';
import * as widgetConfiguratorActionTypes from './modules/widgetConfigurator/actionTypes';
import * as projectInitializationActionTypes from './modules/projectInititialization/actionTypes';
import * as dbActionTypes from './modules/db/actionTypes';
import * as incidentsActionTypes from './modules/incidents/actionTypes';
import * as systemActionTypes from './modules/system/actionTypes';
import * as attackActionTypes from './modules/attack/actionTypes';
import * as patternsActionTypes from './modules/patterns/actionTypes';

function addPrefix(prefix, module) {
  let updatedModule = {};
  Object.keys(module).forEach((n) => {
    updatedModule[n] = `${prefix}/${module[n]}`;
  });
  updatedModule = Object.freeze({ ...module, ...updatedModule });
  return updatedModule;
}

export default {
  assets: assetsActionTypes,
  auth: authActionTypes,
  dashboards: dashboardsActionTypes,
  events: eventsActionTypes,
  locale: localeActionTypes,
  projectInitialization: projectInitializationActionTypes,
  tickets: ticketsActionTypes,
  userProfile: userProfileActionTypes,
  users: usersActionTypes,
  ui: uiActionTypes,
  widgetConfigurator: widgetConfiguratorActionTypes,
  db: dbActionTypes,
  incidents: incidentsActionTypes,
  system: systemActionTypes,
  attack: attackActionTypes,
  patterns: patternsActionTypes,
};
