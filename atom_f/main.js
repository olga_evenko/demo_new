import Vue from 'vue';
import VueRx from 'vue-rx';
import VueProgressBar from 'vue-progressbar';
import ECharts from 'vue-echarts';
import VueLocalStorage from 'vue-localstorage';
import VeeValidate from 'vee-validate';
import VueCookies from 'vue-cookies';
import BootstrapVue from 'bootstrap-vue';
import VueParticles from 'vue-particles';
import VueI18n from 'vue-i18n';
import App from './App.vue';
import router from './routers';
import store from './store';
import DateFilter from './filters/date.js';

import * as config from '@/config';

import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import en from '@/locale/en';
import ru from '@/locale/ru';
import VTooltip from 'v-tooltip';
import UUID from 'vue-uuid';

Vue.use(VueRx);
Vue.use(VeeValidate);
Vue.use(VueCookies);
Vue.use(BootstrapVue);
Vue.use(VueParticles);
Vue.use(VueLocalStorage);
Vue.use(VueI18n);
Vue.use(VTooltip);
Vue.use(UUID);

Vue.component('v-chart', ECharts);
Vue.filter('date', DateFilter);
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px',
});

const i18n = new VueI18n({
  locale: 'ru',
  fallbackLocale: 'ru',
  messages: { ru, en },
  silentTranslationWarn: true,
});

Vue.prototype.$config = config;
new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
