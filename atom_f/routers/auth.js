import store from '@/store';
import API from '@/services/api';
import actionTypes from '@/store/actionTypes';

const routes = [
  {
    path: '/auth',
    name: 'auth',
    component: () => import('@/views/Auth/Auth.vue'),
    beforeEnter(to, from, next) {
      const user = store.getters.getUser;
      if (user.isAuth) {
        next('/');
        return;
      }
      next();
    },
    children: [
      {
        path: 'login',
        name: 'login',
        meta: { title: 'Авторизация' },
        component: () => import('@/views/Auth/Login.vue'),
      },
      {
        path: 'reset-password',
        name: 'reset-password',
        component: () => import('@/views/Auth/PasswordReseting'),
      },
    ],
  },
  {
    path: '/logout',
    name: 'logout',
    component: () => import('@/views/Logout'),
  },
  {
    name: 'init-project',
    path: '/init-project',
    meta: { title: 'view.title.initProject' },
    component: () => import('@/views/InitProject.vue'),
    async beforeEnter(to, from, next) {
      const user = store.getters.getUser;
      if (user.isAuth) {
        next('/');
        return;
      }
      const rspns = await API.getProjectStatus();
      if (rspns.data.projectStatus) {
        next('/');
        return;
      }
      next();
    },
  },
];

export default routes;
