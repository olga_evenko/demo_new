import { isUserAuth } from '../context';

const route = {
  path: '/zoom-dashboard',
  name: 'zoom-dashboard',
  component: () => import('@/views/ModalDashboards.vue'),
  async beforeEnter(to, from, next) {
    const hasAccess = await isUserAuth();
    if (hasAccess) {
      next();
      return;
    }
    next('/login');
  },
};

export default route;
