import { isUserAuth, hasPermission } from '@/routers/context';

/*
  TODO: обдумать функцию для проверки прав. Возможно стоит сделать динамическую генерацию гуардов
  для каждого роута, автоматически генерирую гуарды для каждого роута в соответствии с метатегом
  отвечающим за доступ к правам.
*/
const route = {
  path: '/',
  name: 'home',
  component: () => import('@/views/MainContent.vue'),
  async beforeEnter(to, from, next) {
    const hasAccess = await isUserAuth();
    if (hasAccess) {
      if (to.fullPath === '/') {
        next('/analytics');
        return;
      }
      next();
      return;
    }
    next('/auth/login');
  },
  children: [
    {
      path: '/patterns',
      name: 'patterns',
      component: () => import('@/views/patterns/Patterns.vue'),
    },
    {
      path: '/configuration/users',
      name: 'users',
      component: () => import('@/views/Users/UserManagement'),
      meta: { title: 'users' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations') && hasPermission('viewUsers')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: ':id',
          name: 'user-detail',
          meta: { title: 'users' },
          component: () => import('@/views/Users/Username'),
        },
        {
          path: ':id',
          name: 'my-profile',
          meta: { title: 'users' },
          component: () => import('@/views/Users/Username'),
        },
        {
          path: '',
          name: 'user-list',
          meta: { title: 'users' },
          component: () => import('@/views/Users/Users.vue'),
        },
      ],
    },
    {
      path: '/configuration/users/username',
      name: 'users.username',
      component: () => import('@/views/Users/Username.vue'),
      meta: { title: 'users' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
    },
    {
      path: '/configuration/users/management',
      name: 'users.management',
      component: () => import('@/views/Users/Management.vue'),
      meta: { title: 'management' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
    },
    {
      path: '/configuration/users/permissions',
      name: 'users.permissions',
      component: () => import('@/views/Users/Permissions.vue'),
      meta: { title: 'users' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
    },
    {
      path: '/configuration/system',
      name: 'configurationSystem',
      component: () => import('@/views/configuration/system/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'primary',
          name: 'systemPrimary',
          component: () => import('@/views/configuration/system/Primary.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'primary',
          },
        },
        {
          path: 'authorization',
          name: 'systemAuthorization',
          component: () => import('@/views/configuration/system/Authorization.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'auth',
          },
        },
        {
          path: 'performance',
          name: 'systemPerformance',
          component: () => import('@/views/configuration/system/Performance.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'performance',
          },
        },
        {
          path: 'notifications',
          name: 'systemNotifications',
          component: () => import('@/views/configuration/system/Notifications.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'notifications',
          },
        },
        {
          path: 'ldap',
          name: 'Ldap',
          component: () => import('@/views/configuration/system/Ldap.vue'),
          meta: {
            title: 'Ldap',
          },
        },
        {
          path: 'backups',
          name: 'Backups',
          component: () => import('@/views/configuration/system/Backups.vue'),
          meta: {
            title: 'Backups',
          },
        },
      ],
    },
    {
      path: '/configuration/analysis',
      name: 'configurationAnalysis',
      component: () => import('@/views/configuration/analysis/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'search',
          name: 'analysisSearch',
          component: () => import('@/views/configuration/analysis/Search.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'search',
          },
        },
        {
          path: 'indexing',
          name: 'analysisIndexing',
          component: () => import('@/views/configuration/analysis/Indexing.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'indexing',
          },
        },
        {
          path: 'copying',
          name: 'analysisCopying',
          component: () => import('@/views/configuration/analysis/Copying.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'copying',
          },
        },
        {
          path: 'manage',
          name: 'analysisManage',
          component: () => import('@/views/configuration/analysis/Manage.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'management',
          },
        },
      ],
    },
    {
      path: '/configuration/assets',
      name: 'configurationAssets',
      component: () => import('@/views/configuration/assets/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'scan',
          name: 'assestScan',
          component: () => import('@/views/configuration/assets/Scan.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'scanning',
          },
        },
        {
          path: 'agents',
          name: 'assestAgents',
          component: () => import('@/views/configuration/assets/Agents.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'agents',
          },
        },
        {
          path: 'reaction',
          name: 'assestReaction',
          component: () => import('@/views/configuration/assets/Reaction.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'reaction',
          },
        },
        {
          path: 'events',
          name: 'assestEvents',
          component: () => import('@/views/configuration/assets/Events.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'events',
          },
        },
      ],
    },
    {
      path: '/configuration/correlation',
      name: 'configurationCorrelation',
      component: () => import('@/views/configuration/correlation/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'rules',
          name: 'correlationRules',
          component: () => import('@/views/configuration/correlation/Rules.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'rules',
          },
        },
        {
          path: 'logs',
          name: 'correlationLogs',
          component: () => import('@/views/configuration/correlation/Logs.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'logs',
          },
        },
        {
          path: 'manage',
          name: 'correlationManage',
          component: () => import('@/views/configuration/correlation/Manage.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'management',
          },
        },
        {
          path: 'events',
          name: 'correlationEvents',
          component: () => import('@/views/configuration/correlation/Events.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'events',
          },
        },
      ],
    },
    {
      path: '/configuration/intelligence',
      name: 'configurationIntelligence',
      component: () => import('@/views/configuration/intelligence/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'subscriptions',
          name: 'intelligenceSubscriptions',
          component: () => import('@/views/configuration/intelligence/Subscriptions.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'subscriptions',
          },
        },
        {
          path: 'indicators',
          name: 'intelligenceIndicators',
          component: () => import('@/views/configuration/intelligence/Indicators.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'indicators',
          },
        },
        {
          path: 'intergration',
          name: 'intelligenceIntergration',
          component: () => import('@/views/configuration/intelligence/Integration.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'integration',
          },
        },
        {
          path: 'manage',
          name: 'intelligenceManage',
          component: () => import('@/views/configuration/intelligence/Manage.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'management',
          },
        },
      ],
    },
    {
      path: '/configuration/standardization',
      name: 'configurationStandardization',
      component: () => import('@/views/configuration/standardization/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewConfigurations')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'standarts',
          name: 'standartizationStandarts',
          component: () => import('@/views/configuration/standardization/Standarts.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'standarts',
          },
        },
        {
          path: 'policies',
          name: 'standartizationPolicies',
          component: () => import('@/views/configuration/standardization/Policies.vue'),
          meta: {
            title: 'configuration',
            subTitle: 'policies',
          },
        },
      ],
    },
    {
      path: '/configuration/changelog',
      name: 'configurationChangelog',
      component: () => import('@/views/configuration/Changelog/Changelog.vue'),
      meta: {
        title: 'configuration',
        subTitle: 'updates',
      },
    },
    {
      path: 'analytics',
      name: 'analytics',
      component: () => import('@/views/analytics/Analytics.vue'),
      meta: { title: 'analytics' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewAnalytics')) {
          next();
          return;
        }
        next('/my-profile');
      },
    },
    {
      path: 'analytics/configurator/:name/:id',
      name: 'widgetConfigurator',
      component: () => import('@/views/analytics/widgetConfigurator/Configurator'),
      meta: { title: 'analyticsCenter' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewAnalytics')) {
          next();
          return;
        }
        next('/');
      },
    },
    {
      path: 'analytics/gallery',
      name: 'analyticsGallery',
      component: () => import('@/views/analytics/widgetConfigurator/Gallery'),
      meta: { title: 'gallery' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewAnalytics')) {
          next();
          return;
        }
        next('/');
      },
    },
    {
      path: 'analytics/open-analytics-exchange',
      name: 'analyticsGallery',
      component: () => import('@/views/analytics/widgetConfigurator/Gallery'),
      meta: { title: 'gallery' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewAnalytics')) {
          next();
          return;
        }
        next('/');
      },
    },
    {
      path: 'analytics',
      name: 'analytics',
      component: () => import('@/views/analytics/Analytics.vue'),
      children: [
        {
          path: ':dashboardUrl',
          name: 'AnalyticsDashboard',
          meta: { title: 'analytics' },
          component: () => import('@/views/analytics/Dashboard.vue'),
        },
      ],
      meta: { title: 'analytics' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewAnalytics')) {
          next();
          return;
        }
        next('/user-profile');
      },
    },
    {
      path: 'data-analysis',
      name: 'analysis',
      component: () => import('@/views/analysis/Index.vue'),
      meta: { title: 'analysisData' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewAnalysis')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'events-search',
          name: 'analysisEvents',
          component: () => import('@/views/analysis/Events.vue'),
          meta: {
            title: 'analysisData',
            subTitle: 'eventSearch',
          },
        },
        {
          path: 'patterns',
          name: 'analysisPatterns',
          component: () => import('@/views/analysis/Patterns.vue'),
          meta: {
            title: 'analysisData',
            subTitle: 'patterns',
          },
        },
        {
          path: 'partitioning',
          name: 'analysisPartitions',
          component: () => import('@/views/analysis/Partitions.vue'),
          meta: {
            title: 'analysisData',
            subTitle: 'dataPartitions',
          },
        },
        {
          path: 'archieving',
          name: 'analysisArchiving',
          component: () => import('@/views/analysis/Archiving.vue'),
          meta: {
            title: 'analysisData',
            subTitle: 'archiving',
          },
        },
        {
          path: 'data-sources',
          name: 'dataSources',
          component: () => import('@/views/analysis/DataSources.vue'),
          meta: {
            title: 'analysisData',
            subTitle: 'dataSources',
          },
        },
      ],
    },
    {
      path: 'reaction',
      name: 'reaction',
      component: () => import('@/views/Reaction/Index.vue'),
      meta: { title: 'reaction' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewReactions')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'tab1',
          name: 'reactionTab1',
          component: () => import('@/views/Reaction/Tab1.vue'),
          meta: {
            title: 'reaction',
            subTitle: 'tab1',
          },
        },
      ],
    },
    {
      path: 'incidents',
      name: 'incidents',
      component: () => import('@/views/Incidents/Index.vue'),
      meta: { title: 'incidents' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewIncidents')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'incidents',
          name: 'incidentsList',
          component: () => import('@/views/Incidents/Incidents.vue'),
          meta: {
            title: 'incidents',
            subTitle: 'incidentsList',
          },
        },
        {
          path: 'details',
          name: 'incidetsDetail',
          component: () => import('@/views/Incidents/Details.vue'),
          meta: {
            title: 'incidents',
            subTitle: 'incidentsDetail',
          },
        },
        {
          path: 'active-attacks',
          name: 'incidentAttack',
          component: () => import('@/views/Incidents/Details.vue'),
          meta: {
            title: 'incidents',
            subTitle: 'incidentAttack',
          },
        },
        {
          path: 'wiki',
          name: 'wiki',
          component: () => import('@/views/Incidents/AppWiki.vue'),
          meta: {
            title: 'incidents',
            subTitle: 'appWiki',
          },
        },
      ],
    },
    {
      path: 'infrastructure',
      name: 'infractructure',
      component: () => import('@/views/Infrastructures/Index.vue'),
      meta: { title: 'infractructure' },
      beforeEnter(to, from, next) {
        if (hasPermission('viewInfrastructure')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'assets',
          name: 'infractructureAssets',
          component: () => import('@/views/Infrastructures/Assets.vue'),
          meta: {
            title: 'infractructure',
            subTitle: 'infractructureAssets',
          },
        },
        {
          path: 'details/:id',
          name: 'infractructureDetail',
          component: () => import('@/views/Infrastructures/Details.vue'),
          meta: {
            title: 'infractructure',
            subTitle: 'infractructureDetail',
          },
        },
        {
          path: 'groups',
          name: 'infractructureGroups',
          component: () => import('@/views/Infrastructures/Groups.vue'),
          meta: {
            title: 'infractructure',
            subTitle: 'infractructureGroups',
          },
        },
        {
          path: 'topology',
          name: 'infractructureTopology',
          component: () => import('@/views/Infrastructures/Topology.vue'),
          meta: {
            title: 'infractructure',
            subTitle: 'infractructureTopology',
          },
        },
        {
          path: 'scanning',
          name: 'infractructureScanning',
          component: () => import('@/views/Infrastructures/Scanning.vue'),
          meta: {
            title: 'infractructure',
            subTitle: 'scanning',
          },
        },
        {
          path: 'instruments',
          name: 'infractructureInstruments',
          component: () => import('@/views/Infrastructures/Instruments.vue'),
          meta: {
            title: 'infractructure',
            subTitle: 'infractructureInstruments',
          },
        },
      ],
    },
    {
      path: 'attack-center',
      name: 'attackCenter',
      component: () => import('@/views/AttackCenter/Index.vue'),
      meta: { title: 'attackCenter' },
      // beforeEnter(to, from, next) {
      //   if (hasPermission('viewInfrastructure')) {
      //     next();
      //     return;
      //   }
      //   next('/');
      // },
      children: [
        {
          path: 'source-management',
          name: 'source-management',
          component: () => import('@/views/AttackCenter/SourceManagement.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'sourceManagement',
          },
        },
        {
          path: 'attack-matrix',
          name: 'attack-matrix',
          component: () => import('@/views/AttackCenter/AttackMatrix.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'attackMatrix',
          },
        },
        {
          path: 'coverage-matrix',
          name: 'coverage-matrix',
          component: () => import('@/views/AttackCenter/CoverageMatrix.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'coverageMatrinx',
          },
        },
        {
          path: 'detect-matrix',
          name: 'detect-matrix',
          component: () => import('@/views/AttackCenter/DetectMatrix.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'detectMatrix',
          },
        },
        {
          path: 'group-matrix',
          name: 'group-matrix',
          component: () => import('@/views/AttackCenter/GroupMatrix.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'groupMatrix',
          },
        },
        {
          path: 'attack-tecknology',
          name: 'attack-tecknology',
          component: () => import('@/views/AttackCenter/AttackTecknology.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'attackTecknology',
          },
        },
        {
          path: 'attack-tactics',
          name: 'attack-tactics',
          component: () => import('@/views/AttackCenter/AttackTactics.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'attackTactics',
          },
        },
        {
          path: 'management',
          name: 'mittre-management',
          component: () => import('@/views/AttackCenter/Management.vue'),
          meta: {
            title: 'attackCenter',
            subTitle: 'management',
          },
        },
      ],
    },
    {
      path: 'tickets',
      name: 'tickets',
      meta: {
        title: 'investigations',
      },
      component: () => import('@/views/tickets/Index.vue'),
      beforeEnter(to, from, next) {
        if (hasPermission('viewTickets')) {
          next();
          return;
        }
        next('/');
      },
      children: [
        {
          path: 'tickets',
          name: 'ticketsList',
          component: () => import('@/views/tickets/Tickets.vue'),
          meta: {
            title: 'investigations',
          },
        },
        {
          path: 'details',
          name: 'ticketsDetail',
          component: () => import('@/views/tickets/Details.vue'),
          meta: {
            title: 'investigations',
            subTitle: 'details',
          },
        },
        {
          path: 'groups',
          name: 'ticketsGroups',
          component: () => import('@/views/tickets/Groups.vue'),
          meta: {
            title: 'investigations',
            subTitle: 'groups',
          },
        },
        {
          path: 'topology',
          name: 'ticketsTopology',
          component: () => import('@/views/tickets/Topology.vue'),
          meta: {
            title: 'investigations',
            subTitle: 'topology',
          },
        },
        {
          path: 'SLA',
          name: 'ticetsSLA',
          component: () => import('@/views/tickets/Sla.vue'),
          meta: {
            title: 'investigations',
            subTitle: 'SLA',
          },
        },
        {
          path: 'instruments',
          name: 'ticketsInstruments',
          component: () => import('@/views/tickets/Instruments.vue'),
          meta: {
            title: 'investigations',
            subTitle: 'instruments',
          },
        },
        {
          path: 'ticket/:id',
          name: 'ticketView',
          component: () => import('@/views/tickets/TicketDetailPage.vue'),
        },
      ],
    },
    {
      path: 'sitemap',
      name: 'sitemap',
      component: () => import('@/views/Sitemap/Sitemap.vue'),
      meta: {
        title: 'sitemap',
      },
    },
    {
      path: 'my-profile',
      name: 'my-profile',
      component: () => import('@/views/UserProfile/UserProfile.vue'),
      meta: { title: 'users' },
    },
    {
      path: 'docs',
      name: 'docs',
      component: () => import('@/views/docs/Docs.vue'),
      meta: { title: 'docs' },
    },
  ],
};

export default route;
