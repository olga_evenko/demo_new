import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';
// eslint-disable-next-line
import root from './mainContent';
import zoomDashboard from './zoomDashboard';
import authRoutes from './auth';
import actionTypes from '@/store/actionTypes';


Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.VUE_APP_BASE_URL,
  routes: [
    ...authRoutes,
    root,
    zoomDashboard,

    {
      path: '*',
      name: 'page-not-found',
      // component: () => import('@/views/PageNotFound'),
      async beforeEnter(to, from, next) {
        next('/');
      },
    },
  ],
});
router.beforeEach((to, from, next) => {
  if (to.meta) {
    // eslint-disable-next-line
    store.commit(actionTypes.ui.SET_DOCUMENT_META, to.meta);
  }
  next();
});

export default router;
