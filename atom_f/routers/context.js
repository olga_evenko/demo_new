import store from '@/store';
import cookies from 'vue-cookies';
import API from '@/services/api';
import actionTypes from '@/store/actionTypes';

async function isUserAuth() {
  const user = store.getters.getUser;
  if (user.isAuth) {
    return true;
  }
  const token = cookies.get('token');
  if (token) {
    try {
      const userData = await API.getPersonalInfo();
      store.commit(actionTypes.auth.SET_USER_DATA, userData.data);
      return true;
    } catch (e) {
      console.log(e);
    }
  }
  return false;
}

const hasPermission = (perm) => {
  const user = store.getters.getUser;
  if (user.isSuperuser) {
    return true;
  }
  if (user && user.permissions && user.permissions[perm]) {
    return true;
  }
  return false;
};

export { isUserAuth, hasPermission };
